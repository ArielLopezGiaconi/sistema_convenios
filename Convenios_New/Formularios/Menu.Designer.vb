﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Menu
    Inherits MetroFramework.Forms.MetroForm    'Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Menu))
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProductosAlternativosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CostoFuturoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditarConveniosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReversarPropuestasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CostoEspecialToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FichaProductoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CargaMasivaCostoEspecialToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MantenedorLIstasDePreciosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InputsToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CargaMaestraToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CargaCompetenciaToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CargaAporteYXdeltaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConveniosLicitacionesNuevasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReajusteAutomáticoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReajustePorLíneaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultaReajustePorClienteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultaProximosReajustesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CopiaConveniosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CargaMercadoPublicoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CargaOfertasMercadoPublicoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReportabilidadToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FichaClienteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultarRelacionesPorRutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultaReajustesPorDiaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultaMaestraProductosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReportesDelDíaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AuditoriaProductosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MotorDeBusquedaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReportesVariosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReporteConveniosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ElasticidadToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProductosMercadoPublicoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReporteVentaTissueToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AtributosTissueToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SETUDToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CompetenciasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DescargarActualesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CargaMasivaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ScrapingToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CorrerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrisaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RicardoRodriguezToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PCFactoryToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LiderToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.JumboToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RedOfficeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LapizLopezToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CorrerFlexibleToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DescargarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SalirToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btnPantallas = New System.Windows.Forms.Button()
        Me.btnFichaProducto = New System.Windows.Forms.Button()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.btnColor = New System.Windows.Forms.Button()
        Me.RadialMenuItem1 = New DevComponents.DotNetBar.RadialMenuItem()
        Me.RadialMenuItem4 = New DevComponents.DotNetBar.RadialMenuItem()
        Me.RadialMenuItem5 = New DevComponents.DotNetBar.RadialMenuItem()
        Me.DockContainerItem1 = New DevComponents.DotNetBar.DockContainerItem()
        Me.Command1 = New DevComponents.DotNetBar.Command(Me.components)
        Me.DiarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.APedidoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.Color.White
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem1, Me.ToolStripMenuItem2, Me.ReportabilidadToolStripMenuItem, Me.SETUDToolStripMenuItem, Me.CompetenciasToolStripMenuItem, Me.SalirToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(20, 60)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(879, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ProductosAlternativosToolStripMenuItem, Me.CostoFuturoToolStripMenuItem, Me.EditarConveniosToolStripMenuItem, Me.ReversarPropuestasToolStripMenuItem, Me.CostoEspecialToolStripMenuItem, Me.FichaProductoToolStripMenuItem, Me.CargaMasivaCostoEspecialToolStripMenuItem, Me.MantenedorLIstasDePreciosToolStripMenuItem, Me.InputsToolStripMenuItem1, Me.CargaAporteYXdeltaToolStripMenuItem})
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(95, 20)
        Me.ToolStripMenuItem1.Text = "Mantenedores"
        '
        'ProductosAlternativosToolStripMenuItem
        '
        Me.ProductosAlternativosToolStripMenuItem.Name = "ProductosAlternativosToolStripMenuItem"
        Me.ProductosAlternativosToolStripMenuItem.Size = New System.Drawing.Size(257, 22)
        Me.ProductosAlternativosToolStripMenuItem.Text = "Productos Alternativos (144)"
        '
        'CostoFuturoToolStripMenuItem
        '
        Me.CostoFuturoToolStripMenuItem.Name = "CostoFuturoToolStripMenuItem"
        Me.CostoFuturoToolStripMenuItem.Size = New System.Drawing.Size(257, 22)
        Me.CostoFuturoToolStripMenuItem.Text = "Costo Futuro"
        '
        'EditarConveniosToolStripMenuItem
        '
        Me.EditarConveniosToolStripMenuItem.Name = "EditarConveniosToolStripMenuItem"
        Me.EditarConveniosToolStripMenuItem.Size = New System.Drawing.Size(257, 22)
        Me.EditarConveniosToolStripMenuItem.Text = "Editar Convenios (145)"
        '
        'ReversarPropuestasToolStripMenuItem
        '
        Me.ReversarPropuestasToolStripMenuItem.Name = "ReversarPropuestasToolStripMenuItem"
        Me.ReversarPropuestasToolStripMenuItem.Size = New System.Drawing.Size(257, 22)
        Me.ReversarPropuestasToolStripMenuItem.Text = "Reversar Propuestas (146)"
        '
        'CostoEspecialToolStripMenuItem
        '
        Me.CostoEspecialToolStripMenuItem.Name = "CostoEspecialToolStripMenuItem"
        Me.CostoEspecialToolStripMenuItem.Size = New System.Drawing.Size(257, 22)
        Me.CostoEspecialToolStripMenuItem.Text = "Costo Especial (147)"
        '
        'FichaProductoToolStripMenuItem
        '
        Me.FichaProductoToolStripMenuItem.Name = "FichaProductoToolStripMenuItem"
        Me.FichaProductoToolStripMenuItem.Size = New System.Drawing.Size(257, 22)
        Me.FichaProductoToolStripMenuItem.Text = "Ficha Producto (143)"
        '
        'CargaMasivaCostoEspecialToolStripMenuItem
        '
        Me.CargaMasivaCostoEspecialToolStripMenuItem.Name = "CargaMasivaCostoEspecialToolStripMenuItem"
        Me.CargaMasivaCostoEspecialToolStripMenuItem.Size = New System.Drawing.Size(257, 22)
        Me.CargaMasivaCostoEspecialToolStripMenuItem.Text = "Carga Masiva Costo Especial (148)"
        '
        'MantenedorLIstasDePreciosToolStripMenuItem
        '
        Me.MantenedorLIstasDePreciosToolStripMenuItem.Name = "MantenedorLIstasDePreciosToolStripMenuItem"
        Me.MantenedorLIstasDePreciosToolStripMenuItem.Size = New System.Drawing.Size(257, 22)
        Me.MantenedorLIstasDePreciosToolStripMenuItem.Text = "Mantenedor Listas de Precios (149)"
        '
        'InputsToolStripMenuItem1
        '
        Me.InputsToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CargaMaestraToolStripMenuItem1, Me.CargaCompetenciaToolStripMenuItem1})
        Me.InputsToolStripMenuItem1.Name = "InputsToolStripMenuItem1"
        Me.InputsToolStripMenuItem1.Size = New System.Drawing.Size(257, 22)
        Me.InputsToolStripMenuItem1.Text = "Inputs"
        '
        'CargaMaestraToolStripMenuItem1
        '
        Me.CargaMaestraToolStripMenuItem1.Name = "CargaMaestraToolStripMenuItem1"
        Me.CargaMaestraToolStripMenuItem1.Size = New System.Drawing.Size(179, 22)
        Me.CargaMaestraToolStripMenuItem1.Text = "Carga Maestra"
        '
        'CargaCompetenciaToolStripMenuItem1
        '
        Me.CargaCompetenciaToolStripMenuItem1.Name = "CargaCompetenciaToolStripMenuItem1"
        Me.CargaCompetenciaToolStripMenuItem1.Size = New System.Drawing.Size(179, 22)
        Me.CargaCompetenciaToolStripMenuItem1.Text = "Carga Competencia"
        '
        'CargaAporteYXdeltaToolStripMenuItem
        '
        Me.CargaAporteYXdeltaToolStripMenuItem.Name = "CargaAporteYXdeltaToolStripMenuItem"
        Me.CargaAporteYXdeltaToolStripMenuItem.Size = New System.Drawing.Size(257, 22)
        Me.CargaAporteYXdeltaToolStripMenuItem.Text = "Carga Aporte y Xdelta"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ConveniosLicitacionesNuevasToolStripMenuItem, Me.ReajusteAutomáticoToolStripMenuItem, Me.ReajustePorLíneaToolStripMenuItem, Me.ConsultaReajustePorClienteToolStripMenuItem, Me.ConsultaProximosReajustesToolStripMenuItem, Me.CopiaConveniosToolStripMenuItem, Me.CargaMercadoPublicoToolStripMenuItem, Me.CargaOfertasMercadoPublicoToolStripMenuItem})
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(75, 20)
        Me.ToolStripMenuItem2.Text = "Convenios"
        '
        'ConveniosLicitacionesNuevasToolStripMenuItem
        '
        Me.ConveniosLicitacionesNuevasToolStripMenuItem.Name = "ConveniosLicitacionesNuevasToolStripMenuItem"
        Me.ConveniosLicitacionesNuevasToolStripMenuItem.Size = New System.Drawing.Size(315, 22)
        Me.ConveniosLicitacionesNuevasToolStripMenuItem.Text = "Propuesta Nueva (160)"
        '
        'ReajusteAutomáticoToolStripMenuItem
        '
        Me.ReajusteAutomáticoToolStripMenuItem.Name = "ReajusteAutomáticoToolStripMenuItem"
        Me.ReajusteAutomáticoToolStripMenuItem.Size = New System.Drawing.Size(315, 22)
        Me.ReajusteAutomáticoToolStripMenuItem.Text = "Reajuste Automático (161)"
        '
        'ReajustePorLíneaToolStripMenuItem
        '
        Me.ReajustePorLíneaToolStripMenuItem.Name = "ReajustePorLíneaToolStripMenuItem"
        Me.ReajustePorLíneaToolStripMenuItem.Size = New System.Drawing.Size(315, 22)
        Me.ReajustePorLíneaToolStripMenuItem.Text = "Reajuste Por Línea (162)"
        '
        'ConsultaReajustePorClienteToolStripMenuItem
        '
        Me.ConsultaReajustePorClienteToolStripMenuItem.Name = "ConsultaReajustePorClienteToolStripMenuItem"
        Me.ConsultaReajustePorClienteToolStripMenuItem.Size = New System.Drawing.Size(315, 22)
        Me.ConsultaReajustePorClienteToolStripMenuItem.Text = "Aprobar Convenios Enviados por Correo (163)"
        '
        'ConsultaProximosReajustesToolStripMenuItem
        '
        Me.ConsultaProximosReajustesToolStripMenuItem.Name = "ConsultaProximosReajustesToolStripMenuItem"
        Me.ConsultaProximosReajustesToolStripMenuItem.Size = New System.Drawing.Size(315, 22)
        Me.ConsultaProximosReajustesToolStripMenuItem.Text = "Consulta Proximos Reajustes (154)"
        '
        'CopiaConveniosToolStripMenuItem
        '
        Me.CopiaConveniosToolStripMenuItem.Name = "CopiaConveniosToolStripMenuItem"
        Me.CopiaConveniosToolStripMenuItem.Size = New System.Drawing.Size(315, 22)
        Me.CopiaConveniosToolStripMenuItem.Text = "Copia Convenios (164)"
        '
        'CargaMercadoPublicoToolStripMenuItem
        '
        Me.CargaMercadoPublicoToolStripMenuItem.Name = "CargaMercadoPublicoToolStripMenuItem"
        Me.CargaMercadoPublicoToolStripMenuItem.Size = New System.Drawing.Size(315, 22)
        Me.CargaMercadoPublicoToolStripMenuItem.Text = "Carga Mercado Publico (140)"
        '
        'CargaOfertasMercadoPublicoToolStripMenuItem
        '
        Me.CargaOfertasMercadoPublicoToolStripMenuItem.Name = "CargaOfertasMercadoPublicoToolStripMenuItem"
        Me.CargaOfertasMercadoPublicoToolStripMenuItem.Size = New System.Drawing.Size(315, 22)
        Me.CargaOfertasMercadoPublicoToolStripMenuItem.Text = "Carga Ofertas Mercado Publico (140)"
        '
        'ReportabilidadToolStripMenuItem
        '
        Me.ReportabilidadToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FichaClienteToolStripMenuItem, Me.ConsultarRelacionesPorRutToolStripMenuItem, Me.ConsultaReajustesPorDiaToolStripMenuItem, Me.ConsultaMaestraProductosToolStripMenuItem, Me.ReportesDelDíaToolStripMenuItem, Me.ProductosMercadoPublicoToolStripMenuItem, Me.ReporteVentaTissueToolStripMenuItem, Me.AtributosTissueToolStripMenuItem})
        Me.ReportabilidadToolStripMenuItem.Name = "ReportabilidadToolStripMenuItem"
        Me.ReportabilidadToolStripMenuItem.Size = New System.Drawing.Size(96, 20)
        Me.ReportabilidadToolStripMenuItem.Text = "Reportabilidad"
        '
        'FichaClienteToolStripMenuItem
        '
        Me.FichaClienteToolStripMenuItem.Name = "FichaClienteToolStripMenuItem"
        Me.FichaClienteToolStripMenuItem.Size = New System.Drawing.Size(226, 22)
        Me.FichaClienteToolStripMenuItem.Text = "Ficha Cliente Convenio"
        '
        'ConsultarRelacionesPorRutToolStripMenuItem
        '
        Me.ConsultarRelacionesPorRutToolStripMenuItem.Name = "ConsultarRelacionesPorRutToolStripMenuItem"
        Me.ConsultarRelacionesPorRutToolStripMenuItem.Size = New System.Drawing.Size(226, 22)
        Me.ConsultarRelacionesPorRutToolStripMenuItem.Text = "Consultar Relaciones por Rut"
        '
        'ConsultaReajustesPorDiaToolStripMenuItem
        '
        Me.ConsultaReajustesPorDiaToolStripMenuItem.Name = "ConsultaReajustesPorDiaToolStripMenuItem"
        Me.ConsultaReajustesPorDiaToolStripMenuItem.Size = New System.Drawing.Size(226, 22)
        Me.ConsultaReajustesPorDiaToolStripMenuItem.Text = "Consulta Reajustes por Dia"
        '
        'ConsultaMaestraProductosToolStripMenuItem
        '
        Me.ConsultaMaestraProductosToolStripMenuItem.Name = "ConsultaMaestraProductosToolStripMenuItem"
        Me.ConsultaMaestraProductosToolStripMenuItem.Size = New System.Drawing.Size(226, 22)
        Me.ConsultaMaestraProductosToolStripMenuItem.Text = "Consulta Maestra Productos"
        '
        'ReportesDelDíaToolStripMenuItem
        '
        Me.ReportesDelDíaToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AuditoriaProductosToolStripMenuItem, Me.MotorDeBusquedaToolStripMenuItem, Me.ReportesVariosToolStripMenuItem, Me.ReporteConveniosToolStripMenuItem, Me.ElasticidadToolStripMenuItem})
        Me.ReportesDelDíaToolStripMenuItem.Name = "ReportesDelDíaToolStripMenuItem"
        Me.ReportesDelDíaToolStripMenuItem.Size = New System.Drawing.Size(226, 22)
        Me.ReportesDelDíaToolStripMenuItem.Text = "Reportes del Día"
        '
        'AuditoriaProductosToolStripMenuItem
        '
        Me.AuditoriaProductosToolStripMenuItem.Name = "AuditoriaProductosToolStripMenuItem"
        Me.AuditoriaProductosToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.AuditoriaProductosToolStripMenuItem.Text = "Auditoria Productos"
        '
        'MotorDeBusquedaToolStripMenuItem
        '
        Me.MotorDeBusquedaToolStripMenuItem.Name = "MotorDeBusquedaToolStripMenuItem"
        Me.MotorDeBusquedaToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.MotorDeBusquedaToolStripMenuItem.Text = "Motor de Busqueda"
        '
        'ReportesVariosToolStripMenuItem
        '
        Me.ReportesVariosToolStripMenuItem.Name = "ReportesVariosToolStripMenuItem"
        Me.ReportesVariosToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.ReportesVariosToolStripMenuItem.Text = "Reportes Varios"
        '
        'ReporteConveniosToolStripMenuItem
        '
        Me.ReporteConveniosToolStripMenuItem.Name = "ReporteConveniosToolStripMenuItem"
        Me.ReporteConveniosToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.ReporteConveniosToolStripMenuItem.Text = "Reporte Convenios"
        '
        'ElasticidadToolStripMenuItem
        '
        Me.ElasticidadToolStripMenuItem.Name = "ElasticidadToolStripMenuItem"
        Me.ElasticidadToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.ElasticidadToolStripMenuItem.Text = "Elasticidad"
        '
        'ProductosMercadoPublicoToolStripMenuItem
        '
        Me.ProductosMercadoPublicoToolStripMenuItem.Name = "ProductosMercadoPublicoToolStripMenuItem"
        Me.ProductosMercadoPublicoToolStripMenuItem.Size = New System.Drawing.Size(226, 22)
        Me.ProductosMercadoPublicoToolStripMenuItem.Text = "Productos Mercado Publico"
        '
        'ReporteVentaTissueToolStripMenuItem
        '
        Me.ReporteVentaTissueToolStripMenuItem.Name = "ReporteVentaTissueToolStripMenuItem"
        Me.ReporteVentaTissueToolStripMenuItem.Size = New System.Drawing.Size(226, 22)
        Me.ReporteVentaTissueToolStripMenuItem.Text = "Reporte Venta Tissue"
        '
        'AtributosTissueToolStripMenuItem
        '
        Me.AtributosTissueToolStripMenuItem.Name = "AtributosTissueToolStripMenuItem"
        Me.AtributosTissueToolStripMenuItem.Size = New System.Drawing.Size(226, 22)
        Me.AtributosTissueToolStripMenuItem.Text = "Atributos Tissue"
        '
        'SETUDToolStripMenuItem
        '
        Me.SETUDToolStripMenuItem.Name = "SETUDToolStripMenuItem"
        Me.SETUDToolStripMenuItem.Size = New System.Drawing.Size(53, 20)
        Me.SETUDToolStripMenuItem.Text = "SETUD"
        '
        'CompetenciasToolStripMenuItem
        '
        Me.CompetenciasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DescargarActualesToolStripMenuItem, Me.CargaMasivaToolStripMenuItem, Me.ScrapingToolStripMenuItem})
        Me.CompetenciasToolStripMenuItem.Name = "CompetenciasToolStripMenuItem"
        Me.CompetenciasToolStripMenuItem.Size = New System.Drawing.Size(95, 20)
        Me.CompetenciasToolStripMenuItem.Text = "Competencias"
        '
        'DescargarActualesToolStripMenuItem
        '
        Me.DescargarActualesToolStripMenuItem.Name = "DescargarActualesToolStripMenuItem"
        Me.DescargarActualesToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.DescargarActualesToolStripMenuItem.Text = "Descargar Actuales"
        '
        'CargaMasivaToolStripMenuItem
        '
        Me.CargaMasivaToolStripMenuItem.Name = "CargaMasivaToolStripMenuItem"
        Me.CargaMasivaToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.CargaMasivaToolStripMenuItem.Text = "Carga Masiva"
        '
        'ScrapingToolStripMenuItem
        '
        Me.ScrapingToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CorrerToolStripMenuItem, Me.DescargarToolStripMenuItem})
        Me.ScrapingToolStripMenuItem.Name = "ScrapingToolStripMenuItem"
        Me.ScrapingToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.ScrapingToolStripMenuItem.Text = "Scraping"
        '
        'CorrerToolStripMenuItem
        '
        Me.CorrerToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PrisaToolStripMenuItem, Me.RicardoRodriguezToolStripMenuItem, Me.PCFactoryToolStripMenuItem, Me.LiderToolStripMenuItem, Me.JumboToolStripMenuItem, Me.RedOfficeToolStripMenuItem, Me.LapizLopezToolStripMenuItem, Me.CorrerFlexibleToolStripMenuItem})
        Me.CorrerToolStripMenuItem.Name = "CorrerToolStripMenuItem"
        Me.CorrerToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.CorrerToolStripMenuItem.Text = "Correr"
        '
        'PrisaToolStripMenuItem
        '
        Me.PrisaToolStripMenuItem.Name = "PrisaToolStripMenuItem"
        Me.PrisaToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.PrisaToolStripMenuItem.Text = "Prisa"
        '
        'RicardoRodriguezToolStripMenuItem
        '
        Me.RicardoRodriguezToolStripMenuItem.Name = "RicardoRodriguezToolStripMenuItem"
        Me.RicardoRodriguezToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.RicardoRodriguezToolStripMenuItem.Text = "Ricardo Rodriguez"
        '
        'PCFactoryToolStripMenuItem
        '
        Me.PCFactoryToolStripMenuItem.Name = "PCFactoryToolStripMenuItem"
        Me.PCFactoryToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.PCFactoryToolStripMenuItem.Text = "PC Factory"
        '
        'LiderToolStripMenuItem
        '
        Me.LiderToolStripMenuItem.Name = "LiderToolStripMenuItem"
        Me.LiderToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.LiderToolStripMenuItem.Text = "Lider"
        '
        'JumboToolStripMenuItem
        '
        Me.JumboToolStripMenuItem.Name = "JumboToolStripMenuItem"
        Me.JumboToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.JumboToolStripMenuItem.Text = "Jumbo"
        '
        'RedOfficeToolStripMenuItem
        '
        Me.RedOfficeToolStripMenuItem.Name = "RedOfficeToolStripMenuItem"
        Me.RedOfficeToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.RedOfficeToolStripMenuItem.Text = "RedOffice"
        '
        'LapizLopezToolStripMenuItem
        '
        Me.LapizLopezToolStripMenuItem.Name = "LapizLopezToolStripMenuItem"
        Me.LapizLopezToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.LapizLopezToolStripMenuItem.Text = "Lapiz Lopez"
        '
        'CorrerFlexibleToolStripMenuItem
        '
        Me.CorrerFlexibleToolStripMenuItem.Name = "CorrerFlexibleToolStripMenuItem"
        Me.CorrerFlexibleToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.CorrerFlexibleToolStripMenuItem.Text = "Correr Flexible"
        '
        'DescargarToolStripMenuItem
        '
        Me.DescargarToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DiarioToolStripMenuItem, Me.APedidoToolStripMenuItem})
        Me.DescargarToolStripMenuItem.Name = "DescargarToolStripMenuItem"
        Me.DescargarToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.DescargarToolStripMenuItem.Text = "Descargar"
        '
        'SalirToolStripMenuItem
        '
        Me.SalirToolStripMenuItem.Name = "SalirToolStripMenuItem"
        Me.SalirToolStripMenuItem.Size = New System.Drawing.Size(41, 20)
        Me.SalirToolStripMenuItem.Text = "Salir"
        '
        'Panel1
        '
        Me.Panel1.AutoSize = True
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel1.Location = New System.Drawing.Point(20, 154)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(911, 319)
        Me.Panel1.TabIndex = 1
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.Button1)
        Me.Panel2.Controls.Add(Me.btnPantallas)
        Me.Panel2.Controls.Add(Me.btnFichaProducto)
        Me.Panel2.Location = New System.Drawing.Point(20, 87)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(891, 61)
        Me.Panel2.TabIndex = 2
        '
        'Button1
        '
        Me.Button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.Button1.Image = CType(resources.GetObject("Button1.Image"), System.Drawing.Image)
        Me.Button1.Location = New System.Drawing.Point(129, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(58, 54)
        Me.Button1.TabIndex = 2
        Me.Button1.UseVisualStyleBackColor = True
        Me.Button1.Visible = False
        '
        'btnPantallas
        '
        Me.btnPantallas.BackgroundImage = CType(resources.GetObject("btnPantallas.BackgroundImage"), System.Drawing.Image)
        Me.btnPantallas.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnPantallas.Location = New System.Drawing.Point(66, 4)
        Me.btnPantallas.Name = "btnPantallas"
        Me.btnPantallas.Size = New System.Drawing.Size(57, 54)
        Me.btnPantallas.TabIndex = 1
        Me.btnPantallas.UseVisualStyleBackColor = True
        Me.btnPantallas.Visible = False
        '
        'btnFichaProducto
        '
        Me.btnFichaProducto.BackgroundImage = CType(resources.GetObject("btnFichaProducto.BackgroundImage"), System.Drawing.Image)
        Me.btnFichaProducto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnFichaProducto.Location = New System.Drawing.Point(3, 3)
        Me.btnFichaProducto.Name = "btnFichaProducto"
        Me.btnFichaProducto.Size = New System.Drawing.Size(57, 54)
        Me.btnFichaProducto.TabIndex = 0
        Me.btnFichaProducto.UseVisualStyleBackColor = True
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 600000
        '
        'btnColor
        '
        Me.btnColor.BackgroundImage = CType(resources.GetObject("btnColor.BackgroundImage"), System.Drawing.Image)
        Me.btnColor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnColor.Location = New System.Drawing.Point(335, 13)
        Me.btnColor.Name = "btnColor"
        Me.btnColor.Size = New System.Drawing.Size(42, 44)
        Me.btnColor.TabIndex = 2
        Me.btnColor.UseVisualStyleBackColor = True
        Me.btnColor.Visible = False
        '
        'RadialMenuItem1
        '
        Me.RadialMenuItem1.Image = Global.Convenios_New.My.Resources.Resources.Excel
        Me.RadialMenuItem1.Name = "RadialMenuItem1"
        Me.RadialMenuItem1.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.RadialMenuItem4})
        Me.RadialMenuItem1.Text = "Item 1.1"
        '
        'RadialMenuItem4
        '
        Me.RadialMenuItem4.Image = Global.Convenios_New.My.Resources.Resources.menos
        Me.RadialMenuItem4.Name = "RadialMenuItem4"
        Me.RadialMenuItem4.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.RadialMenuItem5})
        Me.RadialMenuItem4.Text = "Item 1.2"
        '
        'RadialMenuItem5
        '
        Me.RadialMenuItem5.Image = Global.Convenios_New.My.Resources.Resources.search
        Me.RadialMenuItem5.Name = "RadialMenuItem5"
        Me.RadialMenuItem5.Text = "Item 1.3"
        '
        'DockContainerItem1
        '
        Me.DockContainerItem1.Name = "DockContainerItem1"
        Me.DockContainerItem1.Text = "DockContainerItem1"
        '
        'Command1
        '
        Me.Command1.Name = "Command1"
        '
        'DiarioToolStripMenuItem
        '
        Me.DiarioToolStripMenuItem.Name = "DiarioToolStripMenuItem"
        Me.DiarioToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.DiarioToolStripMenuItem.Text = "Diario"
        '
        'APedidoToolStripMenuItem
        '
        Me.APedidoToolStripMenuItem.Name = "APedidoToolStripMenuItem"
        Me.APedidoToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.APedidoToolStripMenuItem.Text = "A Pedido"
        '
        'Menu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(919, 427)
        Me.Controls.Add(Me.btnColor)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "Menu"
        Me.ShowIcon = False
        Me.Text = "Sistema de Convenios Dimerc"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents Panel1 As Panel
    Friend WithEvents SalirToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents ProductosAlternativosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As ToolStripMenuItem
    Friend WithEvents ReajusteAutomáticoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ReajustePorLíneaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ConsultaReajustePorClienteToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ConsultaProximosReajustesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CostoFuturoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ReportabilidadToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents FichaClienteToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ConveniosLicitacionesNuevasToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ConsultarRelacionesPorRutToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ConsultaReajustesPorDiaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents EditarConveniosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ReversarPropuestasToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CostoEspecialToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents FichaProductoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents Panel2 As Panel
    Friend WithEvents btnFichaProducto As Button
    Friend WithEvents ConsultaMaestraProductosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SETUDToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CopiaConveniosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ReportesDelDíaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AuditoriaProductosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents MotorDeBusquedaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ReportesVariosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ReporteConveniosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CargaMercadoPublicoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CargaOfertasMercadoPublicoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ProductosMercadoPublicoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ElasticidadToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents Timer1 As Timer
    Friend WithEvents btnPantallas As Button
    Friend WithEvents btnColor As Button
    Friend WithEvents CargaMasivaCostoEspecialToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents Command1 As DevComponents.DotNetBar.Command
    Friend WithEvents MantenedorLIstasDePreciosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents Button1 As Button
    Friend WithEvents ReporteVentaTissueToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AtributosTissueToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents InputsToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents CargaMaestraToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents CargaCompetenciaToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents CompetenciasToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DescargarActualesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CargaMasivaToolStripMenuItem As ToolStripMenuItem
    Private WithEvents RadialMenuItem1 As DevComponents.DotNetBar.RadialMenuItem
    Private WithEvents RadialMenuItem4 As DevComponents.DotNetBar.RadialMenuItem
    Private WithEvents RadialMenuItem5 As DevComponents.DotNetBar.RadialMenuItem
    Private WithEvents DockContainerItem1 As DevComponents.DotNetBar.DockContainerItem
    Friend WithEvents CargaAporteYXdeltaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ScrapingToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CorrerToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PrisaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RicardoRodriguezToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PCFactoryToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents LiderToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents JumboToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RedOfficeToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents LapizLopezToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DescargarToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CorrerFlexibleToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DiarioToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents APedidoToolStripMenuItem As ToolStripMenuItem
End Class
