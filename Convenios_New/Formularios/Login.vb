﻿Imports System.Deployment.Application

Public Class Login

    ' TODO: inserte el código para realizar autenticación personalizada usando el nombre de usuario y la contraseña proporcionada 
    ' (Consulte http://go.microsoft.com/fwlink/?LinkId=35339).  
    ' El objeto principal personalizado se puede adjuntar al objeto principal del subproceso actual como se indica a continuación: 
    '     My.User.CurrentPrincipal = CustomPrincipal
    ' donde CustomPrincipal es la implementación de IPrincipal utilizada para realizar la autenticación. 
    ' Posteriormente, My.User devolverá la información de identidad encapsulada en el objeto CustomPrincipal
    ' como el nombre de usuario, nombre para mostrar, etc.

    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click
        If Globales.Login(UsernameTextBox.Text, PasswordTextBox.Text) Then
            Globales.user = UsernameTextBox.Text
            If chkPrueba.Checked Then
                Globales.basePrueba = True
            End If
            If Globales.detDerUsu(Globales.user, 141) = True Then
                Dim frmMenu As New Menu
                limpiar()
                Me.Hide()
                Globales.empresa = 3
                frmMenu.ShowDialog()
                PuntoControl.RegistroUso("1")
            Else
                MessageBox.Show("No tienes Permiso para acceder al Sistema. Comunicarse con Inteligencia Dimerc.")
                Application.Exit()
            End If
        Else
            MessageBox.Show("Error de usuario o contraseña, revisar")
            limpiar()
        End If
    End Sub

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click
        Application.Exit()
    End Sub

    Private Sub Login_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Location = New Point(50, 100)


        If (ApplicationDeployment.IsNetworkDeployed) Then
            ToolStripStatusLabel1.Text = "Versión " & ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString()
        Else
            ToolStripStatusLabel1.Text = "Versión " + Globales.revision
        End If
    End Sub

    Private Sub PasswordTextBox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PasswordTextBox.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If UsernameTextBox.Text.Length > 0 And PasswordTextBox.Text.Length > 0 Then
                OK.PerformClick()
            End If
        End If
    End Sub

    Private Sub limpiar()
        PasswordTextBox.Text = ""
        UsernameTextBox.Text = ""
        UsernameTextBox.Focus()
    End Sub

    Private Sub UsernameTextBox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles UsernameTextBox.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            PasswordTextBox.Focus()
        End If
    End Sub
End Class
