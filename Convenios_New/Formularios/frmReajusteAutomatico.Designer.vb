﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmReajusteAutomatico
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.txtPorRebate = New System.Windows.Forms.TextBox()
        Me.txtPorGastoOp = New System.Windows.Forms.TextBox()
        Me.txtPorVenta = New System.Windows.Forms.TextBox()
        Me.txtPorTransporte = New System.Windows.Forms.TextBox()
        Me.dgvResumenLínea = New System.Windows.Forms.DataGridView()
        Me.cmbMarcaPropia = New System.Windows.Forms.ComboBox()
        Me.dgvDatosMP = New System.Windows.Forms.DataGridView()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtTotMargenFinal = New System.Windows.Forms.TextBox()
        Me.txtTotGasto = New System.Windows.Forms.TextBox()
        Me.txtTotRebate = New System.Windows.Forms.TextBox()
        Me.txtGastoOp = New System.Windows.Forms.TextBox()
        Me.txtTotVenta = New System.Windows.Forms.TextBox()
        Me.txtTotTrans = New System.Windows.Forms.TextBox()
        Me.txtTotContr = New System.Windows.Forms.TextBox()
        Me.txtPorTotCon = New System.Windows.Forms.TextBox()
        Me.txtTotIngreso = New System.Windows.Forms.TextBox()
        Me.txtRazons = New System.Windows.Forms.TextBox()
        Me.txtNumcot = New System.Windows.Forms.TextBox()
        Me.txtRutcli = New System.Windows.Forms.TextBox()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.btn_Confirmar = New System.Windows.Forms.Button()
        Me.txtCostoEspecial = New System.Windows.Forms.TextBox()
        Me.txtVariacion = New System.Windows.Forms.TextBox()
        Me.txtPrecioNuevo = New System.Windows.Forms.TextBox()
        Me.txtPrecio = New System.Windows.Forms.TextBox()
        Me.txtDescripcion = New System.Windows.Forms.TextBox()
        Me.txtCodpro = New System.Windows.Forms.TextBox()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.dgvDetalleLineaProductos = New System.Windows.Forms.DataGridView()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.txtPorRebateFinal = New System.Windows.Forms.TextBox()
        Me.txtPorGastoOpFinal = New System.Windows.Forms.TextBox()
        Me.txtPorVentaFinal = New System.Windows.Forms.TextBox()
        Me.txtPorTransFinal = New System.Windows.Forms.TextBox()
        Me.btnExcel = New System.Windows.Forms.Button()
        Me.dgvLineaFinal = New System.Windows.Forms.DataGridView()
        Me.cmbMarcaPropiaFinal = New System.Windows.Forms.ComboBox()
        Me.dgvMpFinal = New System.Windows.Forms.DataGridView()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.txtMgFinal = New System.Windows.Forms.TextBox()
        Me.txtTotGastoFinal = New System.Windows.Forms.TextBox()
        Me.txtTotRebateFinal = New System.Windows.Forms.TextBox()
        Me.txtTotGastOpFinal = New System.Windows.Forms.TextBox()
        Me.txtTotVentaFinal = New System.Windows.Forms.TextBox()
        Me.txtTotTransFinal = New System.Windows.Forms.TextBox()
        Me.txtTotContriFinal = New System.Windows.Forms.TextBox()
        Me.txtPorContriFinal = New System.Windows.Forms.TextBox()
        Me.txtTotIngresiFinal = New System.Windows.Forms.TextBox()
        Me.txtRazonsFinal = New System.Windows.Forms.TextBox()
        Me.txtNumcotFinal = New System.Windows.Forms.TextBox()
        Me.txtRutCliFinal = New System.Windows.Forms.TextBox()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.txtFechaIni = New System.Windows.Forms.TextBox()
        Me.txtFechaFin = New System.Windows.Forms.TextBox()
        Me.txtValAnt = New System.Windows.Forms.TextBox()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.txtValAct = New System.Windows.Forms.TextBox()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.txtPorVariacion = New System.Windows.Forms.TextBox()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.dgvResumenLínea, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvDatosMP, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.dgvDetalleLineaProductos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        CType(Me.dgvLineaFinal, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvMpFinal, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.Button1)
        Me.Panel1.Location = New System.Drawing.Point(8, 14)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1250, 47)
        Me.Panel1.TabIndex = 1
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(1181, 3)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(62, 40)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Salir"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Location = New System.Drawing.Point(12, 68)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1250, 470)
        Me.TabControl1.TabIndex = 4
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.Label52)
        Me.TabPage1.Controls.Add(Me.Label51)
        Me.TabPage1.Controls.Add(Me.txtPorRebate)
        Me.TabPage1.Controls.Add(Me.txtPorGastoOp)
        Me.TabPage1.Controls.Add(Me.txtPorVenta)
        Me.TabPage1.Controls.Add(Me.txtPorTransporte)
        Me.TabPage1.Controls.Add(Me.dgvResumenLínea)
        Me.TabPage1.Controls.Add(Me.cmbMarcaPropia)
        Me.TabPage1.Controls.Add(Me.dgvDatosMP)
        Me.TabPage1.Controls.Add(Me.Label15)
        Me.TabPage1.Controls.Add(Me.Label14)
        Me.TabPage1.Controls.Add(Me.Label13)
        Me.TabPage1.Controls.Add(Me.Label12)
        Me.TabPage1.Controls.Add(Me.Label20)
        Me.TabPage1.Controls.Add(Me.Label19)
        Me.TabPage1.Controls.Add(Me.Label18)
        Me.TabPage1.Controls.Add(Me.Label17)
        Me.TabPage1.Controls.Add(Me.Label16)
        Me.TabPage1.Controls.Add(Me.Label11)
        Me.TabPage1.Controls.Add(Me.Label10)
        Me.TabPage1.Controls.Add(Me.Label9)
        Me.TabPage1.Controls.Add(Me.Label8)
        Me.TabPage1.Controls.Add(Me.Label7)
        Me.TabPage1.Controls.Add(Me.Label6)
        Me.TabPage1.Controls.Add(Me.Label5)
        Me.TabPage1.Controls.Add(Me.Label4)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.Label3)
        Me.TabPage1.Controls.Add(Me.Label22)
        Me.TabPage1.Controls.Add(Me.Label21)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Controls.Add(Me.txtTotMargenFinal)
        Me.TabPage1.Controls.Add(Me.txtTotGasto)
        Me.TabPage1.Controls.Add(Me.txtTotRebate)
        Me.TabPage1.Controls.Add(Me.txtGastoOp)
        Me.TabPage1.Controls.Add(Me.txtTotVenta)
        Me.TabPage1.Controls.Add(Me.txtTotTrans)
        Me.TabPage1.Controls.Add(Me.txtTotContr)
        Me.TabPage1.Controls.Add(Me.txtPorTotCon)
        Me.TabPage1.Controls.Add(Me.txtFechaFin)
        Me.TabPage1.Controls.Add(Me.txtFechaIni)
        Me.TabPage1.Controls.Add(Me.txtTotIngreso)
        Me.TabPage1.Controls.Add(Me.txtRazons)
        Me.TabPage1.Controls.Add(Me.txtNumcot)
        Me.TabPage1.Controls.Add(Me.txtRutcli)
        Me.TabPage1.Location = New System.Drawing.Point(4, 23)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1242, 443)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Datos Reajuste"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'txtPorRebate
        '
        Me.txtPorRebate.Location = New System.Drawing.Point(145, 240)
        Me.txtPorRebate.Name = "txtPorRebate"
        Me.txtPorRebate.Size = New System.Drawing.Size(52, 22)
        Me.txtPorRebate.TabIndex = 44
        Me.txtPorRebate.Text = "0"
        Me.txtPorRebate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPorGastoOp
        '
        Me.txtPorGastoOp.Location = New System.Drawing.Point(145, 212)
        Me.txtPorGastoOp.Name = "txtPorGastoOp"
        Me.txtPorGastoOp.Size = New System.Drawing.Size(52, 22)
        Me.txtPorGastoOp.TabIndex = 44
        Me.txtPorGastoOp.Text = "0"
        Me.txtPorGastoOp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPorVenta
        '
        Me.txtPorVenta.Location = New System.Drawing.Point(145, 184)
        Me.txtPorVenta.Name = "txtPorVenta"
        Me.txtPorVenta.Size = New System.Drawing.Size(52, 22)
        Me.txtPorVenta.TabIndex = 44
        Me.txtPorVenta.Text = "0"
        Me.txtPorVenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPorTransporte
        '
        Me.txtPorTransporte.Location = New System.Drawing.Point(145, 157)
        Me.txtPorTransporte.Name = "txtPorTransporte"
        Me.txtPorTransporte.Size = New System.Drawing.Size(52, 22)
        Me.txtPorTransporte.TabIndex = 44
        Me.txtPorTransporte.Text = "0"
        Me.txtPorTransporte.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'dgvResumenLínea
        '
        Me.dgvResumenLínea.AllowUserToAddRows = False
        Me.dgvResumenLínea.AllowUserToDeleteRows = False
        Me.dgvResumenLínea.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvResumenLínea.Location = New System.Drawing.Point(539, 204)
        Me.dgvResumenLínea.Name = "dgvResumenLínea"
        Me.dgvResumenLínea.ReadOnly = True
        Me.dgvResumenLínea.RowHeadersVisible = False
        Me.dgvResumenLínea.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvResumenLínea.Size = New System.Drawing.Size(677, 223)
        Me.dgvResumenLínea.TabIndex = 43
        '
        'cmbMarcaPropia
        '
        Me.cmbMarcaPropia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMarcaPropia.FormattingEnabled = True
        Me.cmbMarcaPropia.Location = New System.Drawing.Point(913, 16)
        Me.cmbMarcaPropia.Name = "cmbMarcaPropia"
        Me.cmbMarcaPropia.Size = New System.Drawing.Size(197, 22)
        Me.cmbMarcaPropia.TabIndex = 41
        '
        'dgvDatosMP
        '
        Me.dgvDatosMP.AllowUserToAddRows = False
        Me.dgvDatosMP.AllowUserToDeleteRows = False
        Me.dgvDatosMP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDatosMP.Location = New System.Drawing.Point(539, 47)
        Me.dgvDatosMP.Name = "dgvDatosMP"
        Me.dgvDatosMP.ReadOnly = True
        Me.dgvDatosMP.RowHeadersVisible = False
        Me.dgvDatosMP.Size = New System.Drawing.Size(677, 151)
        Me.dgvDatosMP.TabIndex = 39
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(204, 243)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(19, 14)
        Me.Label15.TabIndex = 38
        Me.Label15.Text = "%"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(204, 215)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(19, 14)
        Me.Label14.TabIndex = 29
        Me.Label14.Text = "%"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(204, 187)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(19, 14)
        Me.Label13.TabIndex = 30
        Me.Label13.Text = "%"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(204, 159)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(19, 14)
        Me.Label12.TabIndex = 31
        Me.Label12.Text = "%"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(230, 243)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(16, 14)
        Me.Label20.TabIndex = 32
        Me.Label20.Text = "="
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(230, 215)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(16, 14)
        Me.Label19.TabIndex = 37
        Me.Label19.Text = "="
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(230, 187)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(16, 14)
        Me.Label18.TabIndex = 34
        Me.Label18.Text = "="
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(230, 159)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(16, 14)
        Me.Label17.TabIndex = 33
        Me.Label17.Text = "="
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(230, 134)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(16, 14)
        Me.Label16.TabIndex = 35
        Me.Label16.Text = "="
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(205, 134)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(19, 14)
        Me.Label11.TabIndex = 36
        Me.Label11.Text = "%"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(8, 299)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(126, 14)
        Me.Label10.TabIndex = 27
        Me.Label10.Text = "Mg. Final Contribución"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(8, 271)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(75, 14)
        Me.Label9.TabIndex = 26
        Me.Label9.Text = "Total Gastos"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(8, 215)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(102, 14)
        Me.Label8.TabIndex = 25
        Me.Label8.Text = "Gastos Operación"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(8, 187)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(40, 14)
        Me.Label7.TabIndex = 24
        Me.Label7.Text = "Venta"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(8, 159)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(67, 14)
        Me.Label6.TabIndex = 23
        Me.Label6.Text = "Transporte"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(8, 131)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(107, 14)
        Me.Label5.TabIndex = 22
        Me.Label5.Text = "Total Contribución"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(8, 103)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(85, 14)
        Me.Label4.TabIndex = 28
        Me.Label4.Text = "Total Ingresos"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(8, 75)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(73, 14)
        Me.Label2.TabIndex = 21
        Me.Label2.Text = "Razón Social"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(8, 243)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(46, 14)
        Me.Label3.TabIndex = 19
        Me.Label3.Text = "Rebate"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(783, 19)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(75, 14)
        Me.Label22.TabIndex = 20
        Me.Label22.Text = "Marca Propia"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(8, 19)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(108, 14)
        Me.Label21.TabIndex = 20
        Me.Label21.Text = "Número Cotización"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 47)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(67, 14)
        Me.Label1.TabIndex = 20
        Me.Label1.Text = "Rut Cliente"
        '
        'txtTotMargenFinal
        '
        Me.txtTotMargenFinal.BackColor = System.Drawing.SystemColors.Info
        Me.txtTotMargenFinal.Location = New System.Drawing.Point(259, 296)
        Me.txtTotMargenFinal.Name = "txtTotMargenFinal"
        Me.txtTotMargenFinal.Size = New System.Drawing.Size(138, 22)
        Me.txtTotMargenFinal.TabIndex = 5
        Me.txtTotMargenFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotGasto
        '
        Me.txtTotGasto.BackColor = System.Drawing.SystemColors.Info
        Me.txtTotGasto.Location = New System.Drawing.Point(259, 268)
        Me.txtTotGasto.Name = "txtTotGasto"
        Me.txtTotGasto.Size = New System.Drawing.Size(138, 22)
        Me.txtTotGasto.TabIndex = 6
        Me.txtTotGasto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotRebate
        '
        Me.txtTotRebate.BackColor = System.Drawing.SystemColors.Info
        Me.txtTotRebate.Location = New System.Drawing.Point(259, 240)
        Me.txtTotRebate.Name = "txtTotRebate"
        Me.txtTotRebate.Size = New System.Drawing.Size(138, 22)
        Me.txtTotRebate.TabIndex = 7
        Me.txtTotRebate.Text = "0"
        Me.txtTotRebate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtGastoOp
        '
        Me.txtGastoOp.BackColor = System.Drawing.SystemColors.Info
        Me.txtGastoOp.Location = New System.Drawing.Point(259, 212)
        Me.txtGastoOp.Name = "txtGastoOp"
        Me.txtGastoOp.Size = New System.Drawing.Size(138, 22)
        Me.txtGastoOp.TabIndex = 9
        Me.txtGastoOp.Text = "0"
        Me.txtGastoOp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotVenta
        '
        Me.txtTotVenta.BackColor = System.Drawing.SystemColors.Info
        Me.txtTotVenta.Location = New System.Drawing.Point(259, 184)
        Me.txtTotVenta.Name = "txtTotVenta"
        Me.txtTotVenta.Size = New System.Drawing.Size(138, 22)
        Me.txtTotVenta.TabIndex = 10
        Me.txtTotVenta.Text = "0"
        Me.txtTotVenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotTrans
        '
        Me.txtTotTrans.BackColor = System.Drawing.SystemColors.Info
        Me.txtTotTrans.Location = New System.Drawing.Point(259, 156)
        Me.txtTotTrans.Name = "txtTotTrans"
        Me.txtTotTrans.Size = New System.Drawing.Size(138, 22)
        Me.txtTotTrans.TabIndex = 12
        Me.txtTotTrans.Text = "0"
        Me.txtTotTrans.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotContr
        '
        Me.txtTotContr.BackColor = System.Drawing.SystemColors.Info
        Me.txtTotContr.Location = New System.Drawing.Point(259, 128)
        Me.txtTotContr.Name = "txtTotContr"
        Me.txtTotContr.Size = New System.Drawing.Size(138, 22)
        Me.txtTotContr.TabIndex = 14
        Me.txtTotContr.Text = "0"
        Me.txtTotContr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPorTotCon
        '
        Me.txtPorTotCon.BackColor = System.Drawing.SystemColors.Info
        Me.txtPorTotCon.Location = New System.Drawing.Point(145, 128)
        Me.txtPorTotCon.Name = "txtPorTotCon"
        Me.txtPorTotCon.Size = New System.Drawing.Size(52, 22)
        Me.txtPorTotCon.TabIndex = 15
        Me.txtPorTotCon.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotIngreso
        '
        Me.txtTotIngreso.BackColor = System.Drawing.SystemColors.Info
        Me.txtTotIngreso.Location = New System.Drawing.Point(145, 100)
        Me.txtTotIngreso.Name = "txtTotIngreso"
        Me.txtTotIngreso.Size = New System.Drawing.Size(116, 22)
        Me.txtTotIngreso.TabIndex = 16
        Me.txtTotIngreso.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtRazons
        '
        Me.txtRazons.BackColor = System.Drawing.SystemColors.Info
        Me.txtRazons.Location = New System.Drawing.Point(145, 72)
        Me.txtRazons.Name = "txtRazons"
        Me.txtRazons.Size = New System.Drawing.Size(388, 22)
        Me.txtRazons.TabIndex = 18
        '
        'txtNumcot
        '
        Me.txtNumcot.Location = New System.Drawing.Point(145, 16)
        Me.txtNumcot.Name = "txtNumcot"
        Me.txtNumcot.Size = New System.Drawing.Size(116, 22)
        Me.txtNumcot.TabIndex = 4
        '
        'txtRutcli
        '
        Me.txtRutcli.BackColor = System.Drawing.SystemColors.Info
        Me.txtRutcli.Location = New System.Drawing.Point(145, 44)
        Me.txtRutcli.Name = "txtRutcli"
        Me.txtRutcli.Size = New System.Drawing.Size(116, 22)
        Me.txtRutcli.TabIndex = 4
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.btn_Confirmar)
        Me.TabPage2.Controls.Add(Me.txtCostoEspecial)
        Me.TabPage2.Controls.Add(Me.txtVariacion)
        Me.TabPage2.Controls.Add(Me.txtPrecioNuevo)
        Me.TabPage2.Controls.Add(Me.txtPrecio)
        Me.TabPage2.Controls.Add(Me.txtDescripcion)
        Me.TabPage2.Controls.Add(Me.txtCodpro)
        Me.TabPage2.Controls.Add(Me.Label50)
        Me.TabPage2.Controls.Add(Me.Label27)
        Me.TabPage2.Controls.Add(Me.Label26)
        Me.TabPage2.Controls.Add(Me.Label25)
        Me.TabPage2.Controls.Add(Me.Label24)
        Me.TabPage2.Controls.Add(Me.Label23)
        Me.TabPage2.Controls.Add(Me.dgvDetalleLineaProductos)
        Me.TabPage2.Location = New System.Drawing.Point(4, 23)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1242, 443)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Cuadros de Resumen"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'btn_Confirmar
        '
        Me.btn_Confirmar.Location = New System.Drawing.Point(1091, 16)
        Me.btn_Confirmar.Name = "btn_Confirmar"
        Me.btn_Confirmar.Size = New System.Drawing.Size(145, 42)
        Me.btn_Confirmar.TabIndex = 43
        Me.btn_Confirmar.Text = "Confirmar Cambios"
        Me.btn_Confirmar.UseVisualStyleBackColor = True
        '
        'txtCostoEspecial
        '
        Me.txtCostoEspecial.Location = New System.Drawing.Point(941, 36)
        Me.txtCostoEspecial.Name = "txtCostoEspecial"
        Me.txtCostoEspecial.Size = New System.Drawing.Size(121, 22)
        Me.txtCostoEspecial.TabIndex = 42
        Me.txtCostoEspecial.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtVariacion
        '
        Me.txtVariacion.Location = New System.Drawing.Point(658, 36)
        Me.txtVariacion.Name = "txtVariacion"
        Me.txtVariacion.Size = New System.Drawing.Size(121, 22)
        Me.txtVariacion.TabIndex = 42
        Me.txtVariacion.Text = "0"
        Me.txtVariacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPrecioNuevo
        '
        Me.txtPrecioNuevo.Location = New System.Drawing.Point(790, 36)
        Me.txtPrecioNuevo.Name = "txtPrecioNuevo"
        Me.txtPrecioNuevo.Size = New System.Drawing.Size(121, 22)
        Me.txtPrecioNuevo.TabIndex = 42
        Me.txtPrecioNuevo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPrecio
        '
        Me.txtPrecio.BackColor = System.Drawing.SystemColors.Info
        Me.txtPrecio.Location = New System.Drawing.Point(531, 36)
        Me.txtPrecio.Name = "txtPrecio"
        Me.txtPrecio.Size = New System.Drawing.Size(121, 22)
        Me.txtPrecio.TabIndex = 42
        Me.txtPrecio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtDescripcion
        '
        Me.txtDescripcion.BackColor = System.Drawing.SystemColors.Info
        Me.txtDescripcion.Location = New System.Drawing.Point(174, 36)
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(351, 22)
        Me.txtDescripcion.TabIndex = 42
        '
        'txtCodpro
        '
        Me.txtCodpro.BackColor = System.Drawing.SystemColors.Info
        Me.txtCodpro.Location = New System.Drawing.Point(58, 36)
        Me.txtCodpro.Name = "txtCodpro"
        Me.txtCodpro.Size = New System.Drawing.Size(100, 22)
        Me.txtCodpro.TabIndex = 42
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Location = New System.Drawing.Point(941, 16)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(123, 14)
        Me.Label50.TabIndex = 41
        Me.Label50.Text = "Costo Especial Nuevo"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(813, 16)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(79, 14)
        Me.Label27.TabIndex = 41
        Me.Label27.Text = "Precio Nuevo"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(663, 17)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(118, 14)
        Me.Label26.TabIndex = 41
        Me.Label26.Text = "Variación Precio (%)"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(554, 16)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(78, 14)
        Me.Label25.TabIndex = 41
        Me.Label25.Text = "Precio Actual"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(317, 16)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(68, 14)
        Me.Label24.TabIndex = 41
        Me.Label24.Text = "Descripción"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(75, 16)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(57, 14)
        Me.Label23.TabIndex = 41
        Me.Label23.Text = "Producto"
        '
        'dgvDetalleLineaProductos
        '
        Me.dgvDetalleLineaProductos.AllowUserToAddRows = False
        Me.dgvDetalleLineaProductos.AllowUserToDeleteRows = False
        Me.dgvDetalleLineaProductos.AllowUserToResizeRows = False
        Me.dgvDetalleLineaProductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetalleLineaProductos.Location = New System.Drawing.Point(16, 67)
        Me.dgvDetalleLineaProductos.Name = "dgvDetalleLineaProductos"
        Me.dgvDetalleLineaProductos.ReadOnly = True
        Me.dgvDetalleLineaProductos.RowHeadersVisible = False
        Me.dgvDetalleLineaProductos.Size = New System.Drawing.Size(1220, 358)
        Me.dgvDetalleLineaProductos.TabIndex = 40
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.txtPorRebateFinal)
        Me.TabPage3.Controls.Add(Me.txtPorGastoOpFinal)
        Me.TabPage3.Controls.Add(Me.txtPorVentaFinal)
        Me.TabPage3.Controls.Add(Me.txtPorTransFinal)
        Me.TabPage3.Controls.Add(Me.btnExcel)
        Me.TabPage3.Controls.Add(Me.dgvLineaFinal)
        Me.TabPage3.Controls.Add(Me.cmbMarcaPropiaFinal)
        Me.TabPage3.Controls.Add(Me.dgvMpFinal)
        Me.TabPage3.Controls.Add(Me.Label49)
        Me.TabPage3.Controls.Add(Me.Label28)
        Me.TabPage3.Controls.Add(Me.Label29)
        Me.TabPage3.Controls.Add(Me.Label30)
        Me.TabPage3.Controls.Add(Me.Label31)
        Me.TabPage3.Controls.Add(Me.Label32)
        Me.TabPage3.Controls.Add(Me.Label33)
        Me.TabPage3.Controls.Add(Me.Label34)
        Me.TabPage3.Controls.Add(Me.Label35)
        Me.TabPage3.Controls.Add(Me.Label36)
        Me.TabPage3.Controls.Add(Me.Label37)
        Me.TabPage3.Controls.Add(Me.Label38)
        Me.TabPage3.Controls.Add(Me.Label39)
        Me.TabPage3.Controls.Add(Me.Label40)
        Me.TabPage3.Controls.Add(Me.Label41)
        Me.TabPage3.Controls.Add(Me.Label42)
        Me.TabPage3.Controls.Add(Me.Label43)
        Me.TabPage3.Controls.Add(Me.Label55)
        Me.TabPage3.Controls.Add(Me.Label54)
        Me.TabPage3.Controls.Add(Me.Label53)
        Me.TabPage3.Controls.Add(Me.Label44)
        Me.TabPage3.Controls.Add(Me.Label45)
        Me.TabPage3.Controls.Add(Me.Label46)
        Me.TabPage3.Controls.Add(Me.Label47)
        Me.TabPage3.Controls.Add(Me.Label48)
        Me.TabPage3.Controls.Add(Me.txtMgFinal)
        Me.TabPage3.Controls.Add(Me.txtTotGastoFinal)
        Me.TabPage3.Controls.Add(Me.txtTotRebateFinal)
        Me.TabPage3.Controls.Add(Me.txtTotGastOpFinal)
        Me.TabPage3.Controls.Add(Me.txtTotVentaFinal)
        Me.TabPage3.Controls.Add(Me.txtTotTransFinal)
        Me.TabPage3.Controls.Add(Me.txtTotContriFinal)
        Me.TabPage3.Controls.Add(Me.txtPorContriFinal)
        Me.TabPage3.Controls.Add(Me.txtPorVariacion)
        Me.TabPage3.Controls.Add(Me.txtValAct)
        Me.TabPage3.Controls.Add(Me.txtValAnt)
        Me.TabPage3.Controls.Add(Me.txtTotIngresiFinal)
        Me.TabPage3.Controls.Add(Me.txtRazonsFinal)
        Me.TabPage3.Controls.Add(Me.txtNumcotFinal)
        Me.TabPage3.Controls.Add(Me.txtRutCliFinal)
        Me.TabPage3.Location = New System.Drawing.Point(4, 23)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(1242, 443)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Estado Resultado Final"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'txtPorRebateFinal
        '
        Me.txtPorRebateFinal.Location = New System.Drawing.Point(157, 244)
        Me.txtPorRebateFinal.Name = "txtPorRebateFinal"
        Me.txtPorRebateFinal.Size = New System.Drawing.Size(52, 22)
        Me.txtPorRebateFinal.TabIndex = 85
        Me.txtPorRebateFinal.Text = "0"
        Me.txtPorRebateFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPorGastoOpFinal
        '
        Me.txtPorGastoOpFinal.Location = New System.Drawing.Point(157, 216)
        Me.txtPorGastoOpFinal.Name = "txtPorGastoOpFinal"
        Me.txtPorGastoOpFinal.Size = New System.Drawing.Size(52, 22)
        Me.txtPorGastoOpFinal.TabIndex = 86
        Me.txtPorGastoOpFinal.Text = "0"
        Me.txtPorGastoOpFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPorVentaFinal
        '
        Me.txtPorVentaFinal.Location = New System.Drawing.Point(157, 188)
        Me.txtPorVentaFinal.Name = "txtPorVentaFinal"
        Me.txtPorVentaFinal.Size = New System.Drawing.Size(52, 22)
        Me.txtPorVentaFinal.TabIndex = 87
        Me.txtPorVentaFinal.Text = "0"
        Me.txtPorVentaFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPorTransFinal
        '
        Me.txtPorTransFinal.Location = New System.Drawing.Point(157, 161)
        Me.txtPorTransFinal.Name = "txtPorTransFinal"
        Me.txtPorTransFinal.Size = New System.Drawing.Size(52, 22)
        Me.txtPorTransFinal.TabIndex = 88
        Me.txtPorTransFinal.Text = "0"
        Me.txtPorTransFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'btnExcel
        '
        Me.btnExcel.Location = New System.Drawing.Point(406, 377)
        Me.btnExcel.Name = "btnExcel"
        Me.btnExcel.Size = New System.Drawing.Size(138, 52)
        Me.btnExcel.TabIndex = 84
        Me.btnExcel.Text = "Exportar a Excel"
        Me.btnExcel.UseVisualStyleBackColor = True
        '
        'dgvLineaFinal
        '
        Me.dgvLineaFinal.AllowUserToAddRows = False
        Me.dgvLineaFinal.AllowUserToDeleteRows = False
        Me.dgvLineaFinal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLineaFinal.Location = New System.Drawing.Point(550, 206)
        Me.dgvLineaFinal.Name = "dgvLineaFinal"
        Me.dgvLineaFinal.ReadOnly = True
        Me.dgvLineaFinal.RowHeadersVisible = False
        Me.dgvLineaFinal.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLineaFinal.Size = New System.Drawing.Size(672, 223)
        Me.dgvLineaFinal.TabIndex = 83
        '
        'cmbMarcaPropiaFinal
        '
        Me.cmbMarcaPropiaFinal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMarcaPropiaFinal.FormattingEnabled = True
        Me.cmbMarcaPropiaFinal.Location = New System.Drawing.Point(919, 18)
        Me.cmbMarcaPropiaFinal.Name = "cmbMarcaPropiaFinal"
        Me.cmbMarcaPropiaFinal.Size = New System.Drawing.Size(197, 22)
        Me.cmbMarcaPropiaFinal.TabIndex = 82
        '
        'dgvMpFinal
        '
        Me.dgvMpFinal.AllowUserToAddRows = False
        Me.dgvMpFinal.AllowUserToDeleteRows = False
        Me.dgvMpFinal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvMpFinal.Location = New System.Drawing.Point(550, 49)
        Me.dgvMpFinal.Name = "dgvMpFinal"
        Me.dgvMpFinal.ReadOnly = True
        Me.dgvMpFinal.RowHeadersVisible = False
        Me.dgvMpFinal.Size = New System.Drawing.Size(672, 151)
        Me.dgvMpFinal.TabIndex = 81
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Location = New System.Drawing.Point(789, 21)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(75, 14)
        Me.Label49.TabIndex = 80
        Me.Label49.Text = "Marca Propia"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(215, 248)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(19, 14)
        Me.Label28.TabIndex = 75
        Me.Label28.Text = "%"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(215, 220)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(19, 14)
        Me.Label29.TabIndex = 66
        Me.Label29.Text = "%"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(215, 192)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(19, 14)
        Me.Label30.TabIndex = 67
        Me.Label30.Text = "%"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(215, 164)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(19, 14)
        Me.Label31.TabIndex = 68
        Me.Label31.Text = "%"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(241, 248)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(16, 14)
        Me.Label32.TabIndex = 69
        Me.Label32.Text = "="
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(241, 220)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(16, 14)
        Me.Label33.TabIndex = 74
        Me.Label33.Text = "="
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(241, 192)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(16, 14)
        Me.Label34.TabIndex = 71
        Me.Label34.Text = "="
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(241, 164)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(16, 14)
        Me.Label35.TabIndex = 70
        Me.Label35.Text = "="
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(241, 139)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(16, 14)
        Me.Label36.TabIndex = 72
        Me.Label36.Text = "="
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(216, 139)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(19, 14)
        Me.Label37.TabIndex = 73
        Me.Label37.Text = "%"
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(19, 304)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(126, 14)
        Me.Label38.TabIndex = 64
        Me.Label38.Text = "Mg. Final Contribución"
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Location = New System.Drawing.Point(19, 276)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(75, 14)
        Me.Label39.TabIndex = 63
        Me.Label39.Text = "Total Gastos"
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(19, 220)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(102, 14)
        Me.Label40.TabIndex = 62
        Me.Label40.Text = "Gastos Operación"
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Location = New System.Drawing.Point(19, 192)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(40, 14)
        Me.Label41.TabIndex = 61
        Me.Label41.Text = "Venta"
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Location = New System.Drawing.Point(19, 164)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(67, 14)
        Me.Label42.TabIndex = 60
        Me.Label42.Text = "Transporte"
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Location = New System.Drawing.Point(19, 136)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(107, 14)
        Me.Label43.TabIndex = 59
        Me.Label43.Text = "Total Contribución"
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Location = New System.Drawing.Point(19, 108)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(85, 14)
        Me.Label44.TabIndex = 65
        Me.Label44.Text = "Total Ingresos"
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Location = New System.Drawing.Point(19, 80)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(73, 14)
        Me.Label45.TabIndex = 58
        Me.Label45.Text = "Razón Social"
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Location = New System.Drawing.Point(19, 248)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(46, 14)
        Me.Label46.TabIndex = 55
        Me.Label46.Text = "Rebate"
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Location = New System.Drawing.Point(19, 24)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(108, 14)
        Me.Label47.TabIndex = 57
        Me.Label47.Text = "Número Cotización"
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Location = New System.Drawing.Point(19, 52)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(67, 14)
        Me.Label48.TabIndex = 56
        Me.Label48.Text = "Rut Cliente"
        '
        'txtMgFinal
        '
        Me.txtMgFinal.BackColor = System.Drawing.SystemColors.Info
        Me.txtMgFinal.Location = New System.Drawing.Point(270, 301)
        Me.txtMgFinal.Name = "txtMgFinal"
        Me.txtMgFinal.Size = New System.Drawing.Size(138, 22)
        Me.txtMgFinal.TabIndex = 45
        Me.txtMgFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotGastoFinal
        '
        Me.txtTotGastoFinal.BackColor = System.Drawing.SystemColors.Info
        Me.txtTotGastoFinal.Location = New System.Drawing.Point(270, 273)
        Me.txtTotGastoFinal.Name = "txtTotGastoFinal"
        Me.txtTotGastoFinal.Size = New System.Drawing.Size(138, 22)
        Me.txtTotGastoFinal.TabIndex = 46
        Me.txtTotGastoFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotRebateFinal
        '
        Me.txtTotRebateFinal.BackColor = System.Drawing.SystemColors.Info
        Me.txtTotRebateFinal.Location = New System.Drawing.Point(270, 245)
        Me.txtTotRebateFinal.Name = "txtTotRebateFinal"
        Me.txtTotRebateFinal.Size = New System.Drawing.Size(138, 22)
        Me.txtTotRebateFinal.TabIndex = 47
        Me.txtTotRebateFinal.Text = "0"
        Me.txtTotRebateFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotGastOpFinal
        '
        Me.txtTotGastOpFinal.BackColor = System.Drawing.SystemColors.Info
        Me.txtTotGastOpFinal.Location = New System.Drawing.Point(270, 217)
        Me.txtTotGastOpFinal.Name = "txtTotGastOpFinal"
        Me.txtTotGastOpFinal.Size = New System.Drawing.Size(138, 22)
        Me.txtTotGastOpFinal.TabIndex = 48
        Me.txtTotGastOpFinal.Text = "0"
        Me.txtTotGastOpFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotVentaFinal
        '
        Me.txtTotVentaFinal.BackColor = System.Drawing.SystemColors.Info
        Me.txtTotVentaFinal.Location = New System.Drawing.Point(270, 189)
        Me.txtTotVentaFinal.Name = "txtTotVentaFinal"
        Me.txtTotVentaFinal.Size = New System.Drawing.Size(138, 22)
        Me.txtTotVentaFinal.TabIndex = 49
        Me.txtTotVentaFinal.Text = "0"
        Me.txtTotVentaFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotTransFinal
        '
        Me.txtTotTransFinal.BackColor = System.Drawing.SystemColors.Info
        Me.txtTotTransFinal.Location = New System.Drawing.Point(270, 161)
        Me.txtTotTransFinal.Name = "txtTotTransFinal"
        Me.txtTotTransFinal.Size = New System.Drawing.Size(138, 22)
        Me.txtTotTransFinal.TabIndex = 50
        Me.txtTotTransFinal.Text = "0"
        Me.txtTotTransFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotContriFinal
        '
        Me.txtTotContriFinal.BackColor = System.Drawing.SystemColors.Info
        Me.txtTotContriFinal.Location = New System.Drawing.Point(270, 133)
        Me.txtTotContriFinal.Name = "txtTotContriFinal"
        Me.txtTotContriFinal.Size = New System.Drawing.Size(138, 22)
        Me.txtTotContriFinal.TabIndex = 51
        Me.txtTotContriFinal.Text = "0"
        Me.txtTotContriFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPorContriFinal
        '
        Me.txtPorContriFinal.BackColor = System.Drawing.SystemColors.Info
        Me.txtPorContriFinal.Location = New System.Drawing.Point(156, 133)
        Me.txtPorContriFinal.Name = "txtPorContriFinal"
        Me.txtPorContriFinal.Size = New System.Drawing.Size(52, 22)
        Me.txtPorContriFinal.TabIndex = 52
        Me.txtPorContriFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotIngresiFinal
        '
        Me.txtTotIngresiFinal.BackColor = System.Drawing.SystemColors.Info
        Me.txtTotIngresiFinal.Location = New System.Drawing.Point(156, 105)
        Me.txtTotIngresiFinal.Name = "txtTotIngresiFinal"
        Me.txtTotIngresiFinal.Size = New System.Drawing.Size(116, 22)
        Me.txtTotIngresiFinal.TabIndex = 53
        Me.txtTotIngresiFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtRazonsFinal
        '
        Me.txtRazonsFinal.BackColor = System.Drawing.SystemColors.Info
        Me.txtRazonsFinal.Location = New System.Drawing.Point(156, 77)
        Me.txtRazonsFinal.Name = "txtRazonsFinal"
        Me.txtRazonsFinal.Size = New System.Drawing.Size(388, 22)
        Me.txtRazonsFinal.TabIndex = 54
        '
        'txtNumcotFinal
        '
        Me.txtNumcotFinal.BackColor = System.Drawing.SystemColors.Info
        Me.txtNumcotFinal.Location = New System.Drawing.Point(156, 21)
        Me.txtNumcotFinal.Name = "txtNumcotFinal"
        Me.txtNumcotFinal.Size = New System.Drawing.Size(116, 22)
        Me.txtNumcotFinal.TabIndex = 44
        '
        'txtRutCliFinal
        '
        Me.txtRutCliFinal.BackColor = System.Drawing.SystemColors.Info
        Me.txtRutCliFinal.Location = New System.Drawing.Point(156, 49)
        Me.txtRutCliFinal.Name = "txtRutCliFinal"
        Me.txtRutCliFinal.Size = New System.Drawing.Size(116, 22)
        Me.txtRutCliFinal.TabIndex = 43
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.Location = New System.Drawing.Point(35, 345)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(99, 14)
        Me.Label51.TabIndex = 45
        Me.Label51.Text = "Venta Desde el :"
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.Location = New System.Drawing.Point(76, 373)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(58, 14)
        Me.Label52.TabIndex = 45
        Me.Label52.Text = "Hasta el :"
        '
        'txtFechaIni
        '
        Me.txtFechaIni.BackColor = System.Drawing.SystemColors.Info
        Me.txtFechaIni.Location = New System.Drawing.Point(145, 342)
        Me.txtFechaIni.Name = "txtFechaIni"
        Me.txtFechaIni.Size = New System.Drawing.Size(116, 22)
        Me.txtFechaIni.TabIndex = 16
        Me.txtFechaIni.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtFechaFin
        '
        Me.txtFechaFin.BackColor = System.Drawing.SystemColors.Info
        Me.txtFechaFin.Location = New System.Drawing.Point(145, 370)
        Me.txtFechaFin.Name = "txtFechaFin"
        Me.txtFechaFin.Size = New System.Drawing.Size(116, 22)
        Me.txtFechaFin.TabIndex = 16
        Me.txtFechaFin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtValAnt
        '
        Me.txtValAnt.BackColor = System.Drawing.SystemColors.Info
        Me.txtValAnt.Location = New System.Drawing.Point(157, 351)
        Me.txtValAnt.Name = "txtValAnt"
        Me.txtValAnt.Size = New System.Drawing.Size(116, 22)
        Me.txtValAnt.TabIndex = 53
        Me.txtValAnt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.Location = New System.Drawing.Point(42, 354)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(109, 14)
        Me.Label53.TabIndex = 65
        Me.Label53.Text = "Valorizado Anterior"
        '
        'txtValAct
        '
        Me.txtValAct.BackColor = System.Drawing.SystemColors.Info
        Me.txtValAct.Location = New System.Drawing.Point(157, 379)
        Me.txtValAct.Name = "txtValAct"
        Me.txtValAct.Size = New System.Drawing.Size(116, 22)
        Me.txtValAct.TabIndex = 53
        Me.txtValAct.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Location = New System.Drawing.Point(42, 382)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(99, 14)
        Me.Label54.TabIndex = 65
        Me.Label54.Text = "Valorizado Actual"
        '
        'txtPorVariacion
        '
        Me.txtPorVariacion.BackColor = System.Drawing.SystemColors.Info
        Me.txtPorVariacion.Location = New System.Drawing.Point(157, 407)
        Me.txtPorVariacion.Name = "txtPorVariacion"
        Me.txtPorVariacion.Size = New System.Drawing.Size(116, 22)
        Me.txtPorVariacion.TabIndex = 53
        Me.txtPorVariacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Location = New System.Drawing.Point(42, 410)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(55, 14)
        Me.Label55.TabIndex = 65
        Me.Label55.Text = "Variación"
        '
        'frmReajusteAutomatico
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1269, 548)
        Me.ControlBox = False
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmReajusteAutomatico"
        Me.Text = "Reajuste Automatico de Convenio"
        Me.Panel1.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.dgvResumenLínea, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvDatosMP, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        CType(Me.dgvDetalleLineaProductos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        CType(Me.dgvLineaFinal, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvMpFinal, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Button1 As Button
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents Label15 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents txtTotMargenFinal As TextBox
    Friend WithEvents txtTotGasto As TextBox
    Friend WithEvents txtTotRebate As TextBox
    Friend WithEvents txtGastoOp As TextBox
    Friend WithEvents txtTotVenta As TextBox
    Friend WithEvents txtTotTrans As TextBox
    Friend WithEvents txtTotContr As TextBox
    Friend WithEvents txtPorTotCon As TextBox
    Friend WithEvents txtTotIngreso As TextBox
    Friend WithEvents txtRazons As TextBox
    Friend WithEvents txtRutcli As TextBox
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents dgvDatosMP As DataGridView
    Friend WithEvents dgvDetalleLineaProductos As DataGridView
    Friend WithEvents Label21 As Label
    Friend WithEvents txtNumcot As TextBox
    Friend WithEvents cmbMarcaPropia As ComboBox
    Friend WithEvents Label22 As Label
    Friend WithEvents txtDescripcion As TextBox
    Friend WithEvents txtCodpro As TextBox
    Friend WithEvents Label24 As Label
    Friend WithEvents Label23 As Label
    Friend WithEvents txtPrecioNuevo As TextBox
    Friend WithEvents txtPrecio As TextBox
    Friend WithEvents Label27 As Label
    Friend WithEvents Label26 As Label
    Friend WithEvents Label25 As Label
    Friend WithEvents btn_Confirmar As Button
    Friend WithEvents dgvResumenLínea As DataGridView
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents dgvLineaFinal As DataGridView
    Friend WithEvents cmbMarcaPropiaFinal As ComboBox
    Friend WithEvents dgvMpFinal As DataGridView
    Friend WithEvents Label49 As Label
    Friend WithEvents Label28 As Label
    Friend WithEvents Label29 As Label
    Friend WithEvents Label30 As Label
    Friend WithEvents Label31 As Label
    Friend WithEvents Label32 As Label
    Friend WithEvents Label33 As Label
    Friend WithEvents Label34 As Label
    Friend WithEvents Label35 As Label
    Friend WithEvents Label36 As Label
    Friend WithEvents Label37 As Label
    Friend WithEvents Label38 As Label
    Friend WithEvents Label39 As Label
    Friend WithEvents Label40 As Label
    Friend WithEvents Label41 As Label
    Friend WithEvents Label42 As Label
    Friend WithEvents Label43 As Label
    Friend WithEvents Label44 As Label
    Friend WithEvents Label45 As Label
    Friend WithEvents Label46 As Label
    Friend WithEvents Label47 As Label
    Friend WithEvents Label48 As Label
    Friend WithEvents txtMgFinal As TextBox
    Friend WithEvents txtTotGastoFinal As TextBox
    Friend WithEvents txtTotRebateFinal As TextBox
    Friend WithEvents txtTotGastOpFinal As TextBox
    Friend WithEvents txtTotVentaFinal As TextBox
    Friend WithEvents txtTotTransFinal As TextBox
    Friend WithEvents txtTotContriFinal As TextBox
    Friend WithEvents txtPorContriFinal As TextBox
    Friend WithEvents txtTotIngresiFinal As TextBox
    Friend WithEvents txtRazonsFinal As TextBox
    Friend WithEvents txtNumcotFinal As TextBox
    Friend WithEvents txtRutCliFinal As TextBox
    Friend WithEvents txtCostoEspecial As TextBox
    Friend WithEvents Label50 As Label
    Friend WithEvents btnExcel As Button
    Friend WithEvents txtPorRebate As TextBox
    Friend WithEvents txtPorGastoOp As TextBox
    Friend WithEvents txtPorVenta As TextBox
    Friend WithEvents txtPorTransporte As TextBox
    Friend WithEvents txtVariacion As TextBox
    Friend WithEvents txtPorRebateFinal As TextBox
    Friend WithEvents txtPorGastoOpFinal As TextBox
    Friend WithEvents txtPorVentaFinal As TextBox
    Friend WithEvents txtPorTransFinal As TextBox
    Friend WithEvents Label52 As Label
    Friend WithEvents Label51 As Label
    Friend WithEvents txtFechaFin As TextBox
    Friend WithEvents txtFechaIni As TextBox
    Friend WithEvents Label54 As Label
    Friend WithEvents Label53 As Label
    Friend WithEvents txtValAct As TextBox
    Friend WithEvents txtValAnt As TextBox
    Friend WithEvents Label55 As Label
    Friend WithEvents txtPorVariacion As TextBox
End Class
