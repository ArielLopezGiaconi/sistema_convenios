﻿Public Class frmBuscaCliente
    Dim bd As New Conexion
    Dim datos As String
    Dim PuntoControl As New PuntoControl
    Private Sub frmBuscaCliente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Location = New Point(100, 85)
        PuntoControl.RegistroUso("235")
        buscar()
    End Sub
    Public Sub New(dato As String)
        ' Llamada necesaria para el diseñador.
        InitializeComponent()
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        datos = dato
    End Sub
    Private Sub buscar()
        Try
            bd.open_dimerc()
            Dim SQL = "SELECT  "
            SQL = SQL & "a.rutcli ""Rut"", "
            SQL = SQL & "a.razons ""Razon Social"" "
            SQL = SQL & " FROM en_cliente a "
            SQL = SQL & "  WHERE a.razons like '%" & datos & "%'"
            SQL = SQL & "    and a.codemp = 3"
            SQL = SQL & " order by a.razons"
            dgvDatos.DataSource = bd.sqlSelect(Sql)

            dgvDatos.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dgvDatos.Columns("Rut").Width = 150
            dgvDatos.Columns("Rut").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvDatos.Columns("Razon Social").Width = 480
            dgvDatos.Columns("Razon Social").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub frmBuscaCliente_KeyPress(sender As Object, e As KeyPressEventArgs) Handles MyBase.KeyPress
        Close()
    End Sub

    Private Sub frmBuscaCliente_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.Handled = True
            Me.Close()
        End If
    End Sub
End Class