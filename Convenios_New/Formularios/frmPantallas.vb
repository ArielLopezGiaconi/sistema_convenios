﻿Imports System.Data.OracleClient
Public Class frmPantallas
    Dim bd As New Conexion
    Dim id_numero As String
    Dim PuntoControl As New PuntoControl
    Private Sub corrigeNotas()
        Using con = New OracleConnection(Conexion.retornaConexion)
            con.Open()
            Dim comando As OracleCommand
            Dim dataAdapter As OracleDataAdapter
            Dim sql As String
            Dim dt As New DataTable
            Using transs = con.BeginTransaction
                Try
                    sql = "select b.numnvt, b.codpro, b.costos, c.cosprom, b.codneg from en_notavta a, de_notavta b, ma_product c
                            where a.numnvt = b.numnvt 
                              and a.fecemi between to_Date('" & Date.Now.ToShortDateString & " 00:00:00','dd/mm/yyyy hh24:mi:ss') and to_Date('" & Date.Now.ToShortDateString & " 23:59:59','dd/mm/yyyy hh24:mi:ss')
                              and b.codpro = c.codpro
                              and b.cosprom is null"
                    comando = New OracleCommand(sql, con)
                    dataAdapter = New OracleDataAdapter(comando)
                    dataAdapter.Fill(dt)

                    For i = 0 To dt.Rows.Count - 1
                        sql = "UPDATE DE_NOTAVTA SET COSPROM = " & dt.Rows(0).Item("COSPROM") & "  
                                WHERE NUMNVT = D.NUMNVT
                                  AND CODPRO = D.CODPRO"
                        comando = New OracleCommand(sql, con, transs)
                        comando.ExecuteNonQuery()
                    Next

                    transs.Commit()

                Catch ex As Exception
                    PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                    MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Finally
                    transs.Rollback()
                    con.Close()
                End Try
            End Using
        End Using
    End Sub
    Private Sub frmPantallas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        PuntoControl.RegistroUso("238")
    End Sub

    Private Sub ConsultaPrecioNota()
        Try
            bd.open_dimerc()
            Dim dt As New DataTable
            Dim sql, rut, codlin, filtro As String

            sql = "select a.rutcli, b.userid from en_notavta a, ma_usuario b
               where a.numnvt = " & txtNota.Text & " and a.codven = b.rutusu "
            dt = bd.sqlSelect(sql)

            If dt.Rows.Count = 0 Then
                MessageBox.Show("Esta nota " & txtProducto.Text & ", no existe.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            End If

            lblEjecutivo.Text = dt.Rows(0).Item("userid").ToString
            rut = dt.Rows(0).Item("rutcli").ToString
            lblRutcli.Text = dt.Rows(0).Item("rutcli").ToString

            sql = "Select a.codpro, b.codlin, getlinea(a.codpro) || ' (' || b.codlin || ')' linea, 
               a.precio, a.costos, a.cosprom, a.costocte, b.costo costo_ma, b.cosprom cosprom_ma 
               from de_notavta a, ma_product b
                where a.numnvt = " & txtNota.Text & "
                  and a.codpro = '" & txtProducto.Text & "'
                  and b.codpro = a.codpro "
            dt = bd.sqlSelect(sql)

            If dt.Rows.Count = 0 Then
                MessageBox.Show("Este Producto " & txtProducto.Text & ",  no existe en la nota de venta.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            End If

            lblCosprom.Text = "$ " & dt.Rows(0).Item("cosprom").ToString
            lblCosto.Text = "$ " & dt.Rows(0).Item("costos").ToString
            lblCostocte.Text = "$ " & dt.Rows(0).Item("costocte").ToString
            lblCostoComercial.Text = "$ " & dt.Rows(0).Item("costo_ma").ToString
            lblCostoPromedio.Text = "$ " & dt.Rows(0).Item("cosprom_ma").ToString
            codlin = dt.Rows(0).Item("codlin").ToString
            lblPrecioNota.Text = "$ " & dt.Rows(0).Item("precio").ToString
            lblLinea.Text = dt.Rows(0).Item("linea").ToString

            sql = "select distinct codcnl from re_cliente_lista_linea
                where rutcli = " & rut & "
                  and codlin = " & codlin
            dt = bd.sqlSelect(sql)

            For i = 0 To dt.Rows.Count - 1
                filtro = filtro & dt.Rows(0).Item("codcnl").ToString & ", "
            Next
            filtro = filtro.Substring(0, filtro.Length - 2)

            lblListasPrecio.Text = filtro

            sql = "select max(precio) precio from re_canprod
                where codpro = '" & txtProducto.Text & "'
                  and codemp = 3
                  and codcnl in(" & filtro & ")"
            dt = bd.sqlSelect(sql)

            lblPrecioMax.Text = "$ " & dt.Rows(0).Item("precio").ToString

            sql = "select min(prefij) precio from re_canprod
                where codpro = '" & txtProducto.Text & "'
                  and codemp = 3
                  and codcnl in(" & filtro & ")"
            dt = bd.sqlSelect(sql)

            lblPrecioMin.Text = "$ " & dt.Rows(0).Item("precio").ToString

            sql = "select id_numero, precio, costo, margen, usr_Creac AnalistaCosto from en_cliente_costo
                where rutcli = " & rut & "
                  and codpro = '" & txtProducto.Text & "'
                  and estado = 'V'"
            dt = bd.sqlSelect(sql)

            If dt.Rows.Count > 0 Then
                lblPrecioEspecial.Text = "$ " & dt.Rows(0).Item("precio").ToString
                lblCostoEspecial.Text = "$ " & dt.Rows(0).Item("costo").ToString
                lblAPCosto.Text = dt.Rows(0).Item("AnalistaCosto").ToString
                lblmargen.Text = dt.Rows(0).Item("margen").ToString & " %"
                id_numero = dt.Rows(0).Item("id_numero").ToString
            Else
                lblPrecioEspecial.Text = "$ 0"
                lblCostoEspecial.Text = "$ 0"
                lblAPCosto.Text = "..."
                lblmargen.Text = "0 %"
                id_numero = ""
            End If

            sql = "select b.precio, b.costos, a.usr_creac AnalistaConvenio from en_conveni a, de_conveni b
                where rutcli = " & rut & "
                  and trunc(sysdate) between a.fecemi and a.fecven
                  and a.numcot = b.numcot
                  and b.codpro = '" & txtProducto.Text & "'"
            dt = bd.sqlSelect(sql)

            If dt.Rows.Count > 0 Then
                lblPrecioConvenio.Text = "$ " & dt.Rows(0).Item("precio").ToString
                lblCostoConvenio.Text = "$ " & dt.Rows(0).Item("costos").ToString
                lblAPConvenio.Text = dt.Rows(0).Item("AnalistaConvenio").ToString
            Else
                lblPrecioConvenio.Text = "$ 0"
                lblCostoConvenio.Text = "$ 0"
                lblAPConvenio.Text = "..."
            End If

            sql = "select b.precio, b.costos from en_conveni a, de_conveni b
                where rutcli = 78912345
                  and trunc(sysdate) between a.fecemi and a.fecven
                  and a.numcot = b.numcot
                  and b.codpro = '" & txtProducto.Text & "'"
            dt = bd.sqlSelect(sql)

            If dt.Rows.Count > 0 Then
                lblPrecioMarco.Text = "$ " & dt.Rows(0).Item("precio").ToString
                lblCostoMarco.Text = "$ " & dt.Rows(0).Item("costos").ToString
            Else
                lblPrecioMarco.Text = "$ 0"
                lblCostoMarco.Text = "$ 0"
            End If

            sql = "select a.precio, a.costos, a.numcot from de_cotizac a, (
                select numcot from en_notavta a, re_cotnota b
                where a.numnvt = " & txtNota.Text & "
                  and a.numnvt = b.numnvt) b
                where a.numcot = b.numcot
                  and codpro = '" & txtProducto.Text & "'"
            dt = bd.sqlSelect(sql)

            If dt.Rows.Count > 0 Then
                lblPRecioCoti.Text = "$ " & dt.Rows(0).Item("precio").ToString
                lblCostoCoti.Text = "$ " & dt.Rows(0).Item("costos").ToString
                lblCotizacion.Text = dt.Rows(0).Item("numcot").ToString
            Else
                lblPRecioCoti.Text = " $ 0"
                lblCostoCoti.Text = "$ 0"
                lblCotizacion.Text = "..."
            End If
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        ConsultaPrecioNota()
        PuntoControl.RegistroUso("239")
    End Sub

    Private Sub txtNota_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNota.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            txtProducto.Focus()
        End If
    End Sub

    Private Sub txtProducto_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtProducto.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            btnBuscar.PerformClick()
        End If
    End Sub

    Private Sub TextBox1_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCostoModif.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            AlteraNota()
            PuntoControl.RegistroUso("240")
        End If
    End Sub

    Private Sub AlteraNota()
        Using con = New OracleConnection(Conexion.retornaConexion)
            con.Open()
            Dim comando As OracleCommand
            'Dim dataAdapter As OracleDataAdapter
            Dim sql As String
            Dim dt As New DataTable
            Using transs = con.BeginTransaction
                Try

                    'If rbtCosprom.Checked = True Then 
                    sql = "UPDATE DE_NOTAVTA SET COSTOS = " & txtCostoModif.Text & ", COSTOCTE = " & Replace(lblCostoComercial.Text, "$", "") & ", MARGEN = " &
                         Replace(Math.Round((((Replace(lblPrecioEspecial.Text, "$", "") - txtCostoModif.Text) / Replace(lblPrecioEspecial.Text, "$", "")) * 100), 2), ",", ".") & " 
                        WHERE NUMNVT = " & txtNota.Text & " 
                          AND CODPRO = '" & txtProducto.Text & "'"
                    comando = New OracleCommand(sql, con, transs)
                    comando.ExecuteNonQuery()

                    'ElseIf rbtCosto.Checked = True Then
                    '    sql = "UPDATE DE_NOTAVTA SET COSTOCTE = " & txtCostoModif.Text & "  
                    '    WHERE NUMNVT = " & txtNota.Text & "
                    '      AND CODPRO = " & txtNota.Text
                    '    comando = New OracleCommand(sql, con, transs)
                    '    comando.ExecuteNonQuery()
                    'ElseIf rbtCostocte.Checked = True Then
                    '    sql = "UPDATE DE_NOTAVTA SET COSPROM = " & txtCostoModif.Text & "  
                    '    WHERE NUMNVT = " & txtNota.Text & "
                    '      AND CODPRO = " & txtNota.Text
                    '    comando = New OracleCommand(sql, con, transs)
                    '    comando.ExecuteNonQuery()
                    'End If

                    If lblPrecioEspecial.Text = "$ 0" Then
                        sql = "INSERT INTO EN_CLIENTE_COSTO
                           Select EN_CLIENTE_COSTO_SEQ.nextval,
                                  " & lblRutcli.Text & ", '" & txtProducto.Text & "', " & Replace(lblPrecioNota.Text, "$", "") & ", " & Replace(txtCostoModif.Text, "$", "") & ", 
                                    ROUND(((" & Replace(lblPrecioNota.Text, "$", "") & " - " & Replace(txtCostoModif.Text, "$", "") & ") / " & Replace(lblPrecioNota.Text, "$", "") & ") * 100,2) MARGEN, sysdate, sysdate, 1, 'ModifKK',
                                  3, 0, 0, SYSDATE, SYSDATE, 'V'
                            From DUAL"
                        comando = New OracleCommand(sql, con, transs)
                        comando.ExecuteNonQuery()
                    Else
                        sql = "update en_cliente_costo set estado = 'C', fecfin = trunc(sysdate - 1)
                               where id_numero = " & id_numero & "
                                 and rutcli = " & lblRutcli.Text & "
                                 and codpro = '" & txtProducto.Text & "'"
                        comando = New OracleCommand(sql, con, transs)
                        comando.ExecuteNonQuery()

                        sql = "INSERT INTO EN_CLIENTE_COSTO
                           Select EN_CLIENTE_COSTO_SEQ.nextval,
                                  " & lblRutcli.Text & ", '" & txtProducto.Text & "', " & Replace(lblPrecioNota.Text, "$", "") & ", " & Replace(txtCostoModif.Text, "$", "") & ", 
                                    ROUND(((" & Replace(lblPrecioNota.Text, "$", "") & " - " & Replace(txtCostoModif.Text, "$", "") & ") / " & Replace(lblPrecioNota.Text, "$", "") & ") * 100,2) MARGEN, sysdate, sysdate, 1, 'ModifKK',
                                  3, 0, 0, trunc(SYSDATE), trunc(SYSDATE), 'V'
                            From DUAL"
                        comando = New OracleCommand(sql, con, transs)
                        comando.ExecuteNonQuery()
                    End If

                    transs.Commit()

                    MessageBox.Show("Costo modificado correctamente", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)

                    btnBuscar.PerformClick()

                Catch ex As Exception
                    PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                    MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Finally
                    con.Close()
                End Try
            End Using
        End Using
    End Sub

    Private Sub corrigeNota()
        Using con = New OracleConnection(Conexion.retornaConexion)
            con.Open()
            Dim comando As OracleCommand
            Dim dataAdapter As OracleDataAdapter
            Dim sql As String
            Dim dt As New DataTable
            Using transs = con.BeginTransaction
                Try
                    sql = "select numgui from re_notagui where numnvt = " & txtNota.Text
                    comando = New OracleCommand(sql, con)
                    dataAdapter = New OracleDataAdapter(comando)
                    dataAdapter.Fill(dt)

                    If dt.Rows.Count > 0 Then
                        MessageBox.Show("Nota pasada a bodega con la guia N°" & dt.Rows(0).Item("numgui").ToString, "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End If

                    sql = "select numfac from re_notafac where numnvt = " & txtNota.Text
                    comando = New OracleCommand(sql, con)
                    dataAdapter = New OracleDataAdapter(comando)
                    dataAdapter.Fill(dt)

                    If dt.Rows.Count > 0 Then
                        MessageBox.Show("Nota pasada a bodega con la factura N°" & dt.Rows(0).Item("numfac").ToString, "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End If

                    If rbtCosprom.Checked = True Then
                        sql = "UPDATE DE_NOTAVTA SET COSTO = " & txtCostoModif.Text & "  
                        WHERE NUMNVT = D.NUMNVT
                          AND CODPRO = D.CODPRO"
                        comando = New OracleCommand(sql, con, transs)
                        comando.ExecuteNonQuery()
                    ElseIf rbtCosto.Checked = True Then
                        sql = "UPDATE DE_NOTAVTA SET COSTOCTE = " & txtCostoModif.Text & "  
                        WHERE NUMNVT = D.NUMNVT
                          AND CODPRO = D.CODPRO"
                        comando = New OracleCommand(sql, con, transs)
                        comando.ExecuteNonQuery()
                    ElseIf rbtCostocte.Checked = True Then
                        sql = "UPDATE DE_NOTAVTA SET COSPROM = " & txtCostoModif.Text & "  
                        WHERE NUMNVT = D.NUMNVT
                          AND CODPRO = D.CODPRO"
                        comando = New OracleCommand(sql, con, transs)
                        comando.ExecuteNonQuery()
                    End If

                    transs.Commit()

                    btnBuscar.PerformClick()

                Catch ex As Exception
                    PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                    MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Finally
                    transs.Rollback()
                    con.Close()
                End Try
            End Using
        End Using
    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles Button1.Click
        ConsultaCostosBajoInventario()
    End Sub

    Private Sub ConsultaCostosBajoInventario()
        Try
            Dim sql, cod As String
            Dim dt As New DataTable
            bd.open_dimerc()

            sql = "select codpro, despro, getlinea(codpro) linea, costo, cosprom, fecha_modif fecha, getdominio(20, estpro) estado, userid, maquina_usr, modulo from ma_product_cambios
                    where fecha_modif > to_Date('" & dtpFecha.Value.ToShortDateString & " 00:00:00','dd/mm/yyyy hh24:mi:ss')
                      and costo < cosprom "
            If txtCodigo.Text.Trim = "" Then
                sql = sql & " group by codpro, despro, estpro, costo, cosprom, userid, maquina_usr, modulo, fecha_modif"
            Else
                sql = sql & " and codpro = '" & txtCodigo.Text.Trim & "'
                    group by codpro, despro, estpro, costo, cosprom, userid, maquina_usr, modulo, fecha_modif"
            End If

            dt = bd.sqlSelect(sql)

            dgvDatosSku.DataSource = dt
            dgvDatosSku.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells

        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub btnImportar_Click(sender As Object, e As EventArgs) Handles btnImportar.Click
        Dim save As New SaveFileDialog
        If dgvDatosSku.RowCount = 0 Then
            MessageBox.Show("No hay datos consultados en la grilla", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        Else
            save.Filter = "Archivo Excel | *.xlsx"
            If save.ShowDialog = System.Windows.Forms.DialogResult.OK Then
                Globales.exporta(save.FileName, dgvDatosSku)
            End If
        End If
    End Sub
End Class