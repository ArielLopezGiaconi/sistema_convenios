﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmClientesxLista
    Inherits MetroFramework.Forms.MetroForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Line1 = New DevComponents.DotNetBar.Controls.Line()
        Me.dgvClientesxListas = New System.Windows.Forms.DataGridView()
        Me.dgvClientesxListasNew = New System.Windows.Forms.DataGridView()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.cmbListas = New System.Windows.Forms.ComboBox()
        Me.cmbLinea = New System.Windows.Forms.ComboBox()
        Me.cbxLinea = New System.Windows.Forms.CheckBox()
        Me.btnMover = New System.Windows.Forms.Button()
        Me.cmbListaNueva = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblcargando = New System.Windows.Forms.Label()
        Me.lblProcesando = New System.Windows.Forms.Label()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        CType(Me.dgvClientesxListas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvClientesxListasNew, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Line1
        '
        Me.Line1.Location = New System.Drawing.Point(514, 7)
        Me.Line1.Name = "Line1"
        Me.Line1.Size = New System.Drawing.Size(24, 603)
        Me.Line1.TabIndex = 0
        Me.Line1.Text = "Line1"
        Me.Line1.VerticalLine = True
        '
        'dgvClientesxListas
        '
        Me.dgvClientesxListas.AllowUserToAddRows = False
        Me.dgvClientesxListas.AllowUserToDeleteRows = False
        Me.dgvClientesxListas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvClientesxListas.Location = New System.Drawing.Point(15, 191)
        Me.dgvClientesxListas.Name = "dgvClientesxListas"
        Me.dgvClientesxListas.ReadOnly = True
        Me.dgvClientesxListas.RowHeadersVisible = False
        Me.dgvClientesxListas.Size = New System.Drawing.Size(493, 412)
        Me.dgvClientesxListas.TabIndex = 1
        '
        'dgvClientesxListasNew
        '
        Me.dgvClientesxListasNew.AllowUserToAddRows = False
        Me.dgvClientesxListasNew.AllowUserToDeleteRows = False
        Me.dgvClientesxListasNew.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvClientesxListasNew.Location = New System.Drawing.Point(544, 191)
        Me.dgvClientesxListasNew.Name = "dgvClientesxListasNew"
        Me.dgvClientesxListasNew.ReadOnly = True
        Me.dgvClientesxListasNew.RowHeadersVisible = False
        Me.dgvClientesxListasNew.Size = New System.Drawing.Size(505, 412)
        Me.dgvClientesxListasNew.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(20, 91)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(77, 13)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Lista de Precio"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(20, 137)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(94, 13)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Linea de Producto"
        '
        'btnBuscar
        '
        Me.btnBuscar.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBuscar.Location = New System.Drawing.Point(23, 44)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(70, 37)
        Me.btnBuscar.TabIndex = 9
        Me.btnBuscar.Text = "Buscar"
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'cmbListas
        '
        Me.cmbListas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbListas.FormattingEnabled = True
        Me.cmbListas.Location = New System.Drawing.Point(23, 107)
        Me.cmbListas.Name = "cmbListas"
        Me.cmbListas.Size = New System.Drawing.Size(226, 21)
        Me.cmbListas.TabIndex = 24
        '
        'cmbLinea
        '
        Me.cmbLinea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbLinea.FormattingEnabled = True
        Me.cmbLinea.Location = New System.Drawing.Point(23, 153)
        Me.cmbLinea.Name = "cmbLinea"
        Me.cmbLinea.Size = New System.Drawing.Size(226, 21)
        Me.cmbLinea.TabIndex = 26
        '
        'cbxLinea
        '
        Me.cbxLinea.AutoSize = True
        Me.cbxLinea.Location = New System.Drawing.Point(493, 156)
        Me.cbxLinea.Name = "cbxLinea"
        Me.cbxLinea.Size = New System.Drawing.Size(15, 14)
        Me.cbxLinea.TabIndex = 29
        Me.cbxLinea.UseVisualStyleBackColor = True
        '
        'btnMover
        '
        Me.btnMover.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMover.Location = New System.Drawing.Point(468, 44)
        Me.btnMover.Name = "btnMover"
        Me.btnMover.Size = New System.Drawing.Size(121, 37)
        Me.btnMover.TabIndex = 30
        Me.btnMover.Text = "Mover Clientes"
        Me.btnMover.UseVisualStyleBackColor = True
        '
        'cmbListaNueva
        '
        Me.cmbListaNueva.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbListaNueva.FormattingEnabled = True
        Me.cmbListaNueva.Location = New System.Drawing.Point(823, 153)
        Me.cmbListaNueva.Name = "cmbListaNueva"
        Me.cmbListaNueva.Size = New System.Drawing.Size(226, 21)
        Me.cmbListaNueva.TabIndex = 33
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(820, 137)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(112, 13)
        Me.Label4.TabIndex = 31
        Me.Label4.Text = "Lista de Precio Nueva"
        '
        'lblcargando
        '
        Me.lblcargando.AutoSize = True
        Me.lblcargando.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblcargando.Location = New System.Drawing.Point(94, 363)
        Me.lblcargando.Name = "lblcargando"
        Me.lblcargando.Size = New System.Drawing.Size(330, 46)
        Me.lblcargando.TabIndex = 34
        Me.lblcargando.Text = "Cargando listas..."
        Me.lblcargando.Visible = False
        '
        'lblProcesando
        '
        Me.lblProcesando.AutoSize = True
        Me.lblProcesando.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProcesando.Location = New System.Drawing.Point(639, 363)
        Me.lblProcesando.Name = "lblProcesando"
        Me.lblProcesando.Size = New System.Drawing.Size(350, 46)
        Me.lblProcesando.TabIndex = 35
        Me.lblProcesando.Text = "Procesando Listas"
        Me.lblProcesando.Visible = False
        '
        'ComboBox1
        '
        Me.ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(255, 107)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(226, 21)
        Me.ComboBox1.TabIndex = 37
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(252, 91)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(81, 13)
        Me.Label2.TabIndex = 36
        Me.Label2.Text = "Cliente Registro"
        '
        'ComboBox2
        '
        Me.ComboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(255, 153)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(226, 21)
        Me.ComboBox2.TabIndex = 39
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(252, 137)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(77, 13)
        Me.Label5.TabIndex = 38
        Me.Label5.Text = "Lista de Precio"
        '
        'frmClientesxLista
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1064, 614)
        Me.Controls.Add(Me.ComboBox2)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblProcesando)
        Me.Controls.Add(Me.lblcargando)
        Me.Controls.Add(Me.cmbListaNueva)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.btnMover)
        Me.Controls.Add(Me.cbxLinea)
        Me.Controls.Add(Me.cmbLinea)
        Me.Controls.Add(Me.cmbListas)
        Me.Controls.Add(Me.btnBuscar)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dgvClientesxListasNew)
        Me.Controls.Add(Me.dgvClientesxListas)
        Me.Controls.Add(Me.Line1)
        Me.Name = "frmClientesxLista"
        CType(Me.dgvClientesxListas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvClientesxListasNew, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Line1 As DevComponents.DotNetBar.Controls.Line
    Friend WithEvents dgvClientesxListas As DataGridView
    Friend WithEvents dgvClientesxListasNew As DataGridView
    Friend WithEvents Label1 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents btnBuscar As Button
    Friend WithEvents cmbListas As ComboBox
    Friend WithEvents cmbLinea As ComboBox
    Friend WithEvents cbxLinea As CheckBox
    Friend WithEvents btnMover As Button
    Friend WithEvents cmbListaNueva As ComboBox
    Friend WithEvents Label4 As Label
    Friend WithEvents lblcargando As Label
    Friend WithEvents lblProcesando As Label
    Friend WithEvents ComboBox1 As ComboBox
    Friend WithEvents Label2 As Label
    Friend WithEvents ComboBox2 As ComboBox
    Friend WithEvents Label5 As Label
End Class
