﻿Public Class frmBusca
    Dim bd As New Conexion
    Dim datos As String
    Dim relacionprovpro As Boolean
    Dim codigo As String
    Dim PuntoControl As New PuntoControl
    Private Sub frmBusca_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        PuntoControl.RegistroUso("234")
        Me.Location = New Point(100, 85)
        buscar()
    End Sub
    Public Sub New(dato As String, relacion As Boolean, Optional cod As String = "")
        ' Llamada necesaria para el diseñador.
        InitializeComponent()
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        datos = dato
        relacionprovpro = relacion
        codigo = cod
    End Sub

    Private Sub frmBusca_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Close()
    End Sub

    Private Sub buscar()
        Try
            If datos = "0" Or datos = "," Then
                datos = ""
            End If
            bd.open_dimerc()
            Dim Sql = "SELECT  "
            Sql = Sql & "a.rutprv ""Rut"","
            Sql = Sql & "a.digprv ""Digito"","
            Sql = Sql & "a.nombre ""Nombre Proveedor"","
            Sql = Sql & "a.direcc ""Dirección"","
            Sql = Sql & "a.fono01 ""Teléfono"","
            Sql = Sql & "a.contac ""Contacto"","
            Sql = Sql & "a.codprv, "
            Sql = Sql & "'-' fldCliente "
            Sql = Sql & "FROM ma_proveed a  "
            If relacionprovpro = True Then
                Sql = Sql & ", re_provpro d "
            End If
            Select Case Tag
                Case "RazonSocial"
                    If codigo.ToString.Trim <> "" Then
                        Sql = Sql & " WHERE a.nombre >= '%" & datos & "%' "
                        Sql = Sql & " and d.codpro = '" & codigo & "'"
                        Sql = Sql & " and d.rutprv = a.rutprv "
                    Else
                        Sql = Sql & " WHERE a.nombre like '%" & datos & "%' "
                    End If
                    'SQL = SQL & "   and a.codemp = " & GstrCodemp
                    'Sql = Sql & " order by a.nombre"
            End Select
            Sql = Sql & " order by a.nombre"
            dgvDatos.DataSource = bd.sqlSelect(Sql)
            If dgvDatos.RowCount = 0 Then
                MessageBox.Show("La busqueda no arrojo resultados,intente con otro nombre por favor.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            End If

            dgvDatos.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dgvDatos.Columns("Rut").Width = 80
            dgvDatos.Columns("Rut").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvDatos.Columns("Digito").Width = 20
            dgvDatos.Columns("Nombre Proveedor").Width = 220
            dgvDatos.Columns("Nombre Proveedor").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            dgvDatos.Columns("Dirección").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            dgvDatos.Columns("Dirección").Width = 220
            dgvDatos.Columns("Teléfono").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            dgvDatos.Columns("Teléfono").Width = 100
            dgvDatos.Columns("Contacto").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            dgvDatos.Columns("Contacto").Width = 100
            dgvDatos.Columns("codprv").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvDatos.Columns("codprv").Width = 80
            dgvDatos.Columns("fldCliente").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            dgvDatos.Columns("fldCliente").Width = 80

        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub


    Private Sub frmBusca_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.Handled = True
            Me.Close()
        End If
    End Sub

    Private Sub dgvDatos_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvDatos.CellDoubleClick
        Me.Close()
    End Sub
End Class