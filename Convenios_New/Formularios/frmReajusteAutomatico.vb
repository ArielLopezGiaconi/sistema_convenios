﻿Imports System.Data.OracleClient
Imports Microsoft.Office.Interop

Public Class frmReajusteAutomatico
    Dim bd As New Conexion
    Dim flag As Boolean = False
    Dim flaggrilla As Boolean = False
    Dim indice As Integer = 0
    Dim sumacosto, sumacostoFinal As Integer
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub frmReajusteAutomatico_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.StartPosition = FormStartPosition.Manual
        Me.Location = New Point(0, 100)

        cargacombo()
    End Sub
    Private Sub cargacombo()
        Try
            Dim dtMP As DataTable = New DataTable

            dtMP.Columns.Add("Codigo")
            dtMP.Columns.Add("Descripcion")

            dtMP.Rows.Add("TODO", "TODO")
            dtMP.Rows.Add("NACIONAL", "NACIONAL")
            dtMP.Rows.Add("IMPORTADO", "IMPORTADO")
            dtMP.Rows.Add("NO MP", "NO MP")

            cmbMarcaPropia.DataSource = dtMP
            cmbMarcaPropia.SelectedIndex = -1
            cmbMarcaPropia.ValueMember = "Codigo"
            cmbMarcaPropia.DisplayMember = "Descripcion"
            cmbMarcaPropia.Refresh()

            cmbMarcaPropiaFinal.DataSource = dtMP
            cmbMarcaPropiaFinal.SelectedIndex = -1
            cmbMarcaPropiaFinal.ValueMember = "Codigo"
            cmbMarcaPropiaFinal.DisplayMember = "Descripcion"
            cmbMarcaPropiaFinal.Refresh()

            flag = True
        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)

        End Try
    End Sub

    Private Sub txtRutcli_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtTotVenta.KeyPress, txtTotTrans.KeyPress, txtTotRebate.KeyPress, txtTotMargenFinal.KeyPress, txtTotIngreso.KeyPress, txtTotGasto.KeyPress, txtTotContr.KeyPress, txtRutcli.KeyPress, txtRazons.KeyPress, txtPorTotCon.KeyPress, txtGastoOp.KeyPress, txtFechaIni.KeyPress, txtFechaFin.KeyPress
        e.Handled = True
    End Sub

    Private Sub txtPorTrans_KeyPress(sender As Object, e As KeyPressEventArgs)
        Globales.soloNumeros(sender, e)
    End Sub

    Private Sub txtNumcot_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNumcot.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If txtNumcot.Text <> "" Then
                buscacotizacion()
            End If

        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub
    Private Sub buscacotizacion()
        Try
            bd.open_dimerc()
            Dim sql As String = "select rutcli,get_cliente(codemp,rutcli) razons,fecemi,fecven from en_cotizac where codemp=3 and numcot=" + txtNumcot.Text
            Dim dt = bd.sqlSelect(sql)
            If dt.Rows.Count = 0 Then
                MessageBox.Show("Número de de cotización no encontrado", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            Else
                txtRutcli.Text = dt.Rows(0).Item("rutcli").ToString
                txtRutCliFinal.Text = dt.Rows(0).Item("rutcli").ToString
                txtRazons.Text = dt.Rows(0).Item("razons").ToString
                txtRazonsFinal.Text = dt.Rows(0).Item("razons").ToString
                txtNumcotFinal.Text = txtNumcot.Text


                Dim fechaini As String = dt.Rows(0).Item("fecemi")
                Dim fechafin As String = dt.Rows(0).Item("fecven")
                txtFechaIni.Text = dt.Rows(0).Item("fecemi")
                txtFechaFin.Text = dt.Rows(0).Item("fecven")


                CargaTablaTemporal(fechaini, fechafin)



                flaggrilla = False
                sql = "select * from tmp_proceso_convenio"
                dgvDetalleLineaProductos.DataSource = bd.sqlSelect(sql)
                formatogrilla()
                flaggrilla = True
                recalculaEstadoResultado()

                sql = " select Linea,sum(""Valorizado Precio"") Valorizado, round((sum(""Valorizado Precio"")-sum(""Valorizado Analisis""))/ sum(""Valorizado Precio"")*100,2) ""MG. Lista"", "
                sql = sql & "round((sum(""Contribucion Comercial"")+ sum(""Contrib. Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comercial"", "
                sql = sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comer + Aporte MP"", "
                sql = sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Rebate""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Global"" "
                sql = sql & " from tmp_proceso_convenio "
                sql = sql & " where ""Valorizado Precio"">0"
                sql = sql & "group by linea order by sum(""Valorizado Precio"") desc"
                Dim dtResumenLinea As New DataTable
                dtResumenLinea = bd.sqlSelect(sql)

                Dim dtjuntar As New DataTable
                sql = " select sum(""Valorizado Precio"") Valorizado, round((sum(""Valorizado Precio"")-sum(""Valorizado Analisis""))/ sum(""Valorizado Precio"")*100,2) ""MG. Lista"", "
                sql = sql & "round((sum(""Contribucion Comercial"")+ sum(""Contrib. Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comercial"", "
                sql = sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comer + Aporte MP"", "
                sql = sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Rebate""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Global"" "
                sql = sql & " from tmp_proceso_convenio "
                sql = sql & " where ""Valorizado Precio"">0"
                dtjuntar = bd.sqlSelect(sql)

                dtResumenLinea.Merge(dtjuntar)
                dgvLineaFinal.DataSource = dtResumenLinea



                dgvLineaFinal.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
                dgvLineaFinal.Columns("Valorizado").DefaultCellStyle.Format = "n0"
                dgvLineaFinal.Columns("Valorizado").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgvLineaFinal.Columns("Mg. Lista").DefaultCellStyle.Format = "n2"
                dgvLineaFinal.Columns("Mg. Lista").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgvLineaFinal.Columns("Mg. Comercial").DefaultCellStyle.Format = "n2"
                dgvLineaFinal.Columns("Mg. Comercial").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgvLineaFinal.Columns("Mg. Comer + Aporte MP").DefaultCellStyle.Format = "n2"
                dgvLineaFinal.Columns("Mg. Comer + Aporte MP").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgvLineaFinal.Columns("Mg. Global").DefaultCellStyle.Format = "n2"
                dgvLineaFinal.Columns("Mg. Global").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight



                sql = " select Linea, sum(""Valorizado Precio"") Valorizado,round((sum(""Valorizado Precio"")-sum(""Valorizado Analisis""))/ sum(""Valorizado Precio"")*100,2) ""MG. Lista"", "
                sql = sql & "round((sum(""Contribucion Comercial"")+ sum(""Contrib. Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comercial"", "
                sql = sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comer + Aporte MP"", "
                sql = sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Rebate""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Global"" "
                sql = sql & " from tmp_proceso_convenio "
                sql = sql & " where ""Valorizado Precio"">0"
                sql = sql & "group by linea order by sum(""Valorizado Precio"") desc"
                Dim dtDatosMp As New DataTable
                dtDatosMp = bd.sqlSelect(sql)


                Dim dtjuntar2 As New DataTable
                sql = " select sum(""Valorizado Precio"") Valorizado,round((sum(""Valorizado Precio"")-sum(""Valorizado Analisis""))/ sum(""Valorizado Precio"")*100,2) ""MG. Lista"", "
                sql = sql & "round((sum(""Contribucion Comercial"")+ sum(""Contrib. Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comercial"", "
                sql = sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comer + Aporte MP"", "
                sql = sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Rebate""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Global"" "
                sql = sql & " from tmp_proceso_convenio "
                sql = sql & " where ""Valorizado Precio"">0"
                dtjuntar2 = bd.sqlSelect(sql)
                dtDatosMp.Merge(dtjuntar2)
                dgvMpFinal.DataSource = dtDatosMp


                dgvMpFinal.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
                dgvMpFinal.Columns("Valorizado").DefaultCellStyle.Format = "n0"
                dgvMpFinal.Columns("Valorizado").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgvMpFinal.Columns("Mg. Lista").DefaultCellStyle.Format = "n2"
                dgvMpFinal.Columns("Mg. Lista").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgvMpFinal.Columns("Mg. Comercial").DefaultCellStyle.Format = "n2"
                dgvMpFinal.Columns("Mg. Comercial").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgvMpFinal.Columns("Mg. Comer + Aporte MP").DefaultCellStyle.Format = "n2"
                dgvMpFinal.Columns("Mg. Comer + Aporte MP").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgvMpFinal.Columns("Mg. Global").DefaultCellStyle.Format = "n2"
                dgvMpFinal.Columns("Mg. Global").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight







            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub CargaTablaTemporal(fechaini, fechafin)
        Using conn = New OracleConnection(Conexion.retornaConexion)
            conn.Open()
            Dim comando As OracleCommand
            Dim dataadapter As OracleDataAdapter
            Dim dt As New DataTable
            Using transaccion = conn.BeginTransaction
                Try

                    Dim sql = "select tname from tab where tname = 'TMP_PROCESO_CONVENIO'"
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    dataadapter = New OracleDataAdapter(comando)
                    dataadapter.Fill(dt)
                    If dt.Rows.Count > 0 Then
                        sql = "drop table tmp_proceso_convenio"
                        comando = New OracleCommand(sql, conn)
                        comando.Transaction = transaccion
                        comando.ExecuteNonQuery()
                    End If



                    sql = " create table tmp_proceso_convenio as ( "
                    sql = sql & "select c.codpro,c.despro,nvl(e.tipo,'NO MP') mpropia,getlinea(c.codpro) linea,getmarca(c.codpro) marca, "
                    sql = sql & "nvl(d.cantidad,0) cantidad ,b.precio, "
                    sql = sql & "decode(f.costo,'',C.COSTO,f.costo) ""Costo Analisis"",  "
                    sql = sql & "decode(f.costo,'',C.COSTO,f.costo) ""Costo Comercial"", "
                    sql = sql & "decode(f.costo,'',C.cosprom,f.costo) ""Costo Promedio"", "
                    sql = sql & "0 ""Costo Especial"", "
                    sql = sql & "nvl(f.costo,0) ""Costo Especial PM"", "
                    sql = sql & "nvl(f.fecini,'') ""FecIni Costo Especial"", "
                    sql = sql & "nvl(f.fecfin,'') ""FecFIn Costo Especial"", "
                    sql = sql & "round(decode(f.costo,'',C.cosprom,f.costo) * (1-(nvl(g.aporte,0)/100)),2) ""Costo Rebate"", "
                    sql = sql & "nvl(g.aporte,0) aporte, "
                    sql = sql & "0 PESO, "
                    sql = sql & "round(((b.precio-decode(f.costo,'',C.COSTO,f.costo))/b.precio)*100,2) ""Mg. Comercial"", "
                    sql = sql & "round((1-decode(f.costo,'',C.cosprom,f.costo)/b.precio)*100,2) ""Mg. Inventario"", "
                    sql = sql & "round((1-(decode(f.costo,'',C.cosprom,f.costo) * (1-(nvl(g.aporte,0)/100)))/b.precio)*100,2) ""Mg. Rebate"", "
                    sql = sql & "nvl(d.cantidad,0) * b.precio ""Valorizado Precio"", "
                    sql = sql & "nvl(d.cantidad,0) * decode(f.costo,'',c.costo,f.costo) ""Valorizado Analisis"", "
                    sql = sql & "nvl(d.cantidad,0) * decode(f.costo,'',C.cosprom,f.costo) ""Valorizado Inventario"", "
                    sql = sql & "nvl(nvl(d.cantidad,0) * round(decode(f.costo,'',C.cosprom,f.costo) * (1-(nvl(g.aporte,0)/100)),2),0) ""Valorizado Rebate"", "
                    sql = sql & "(nvl(d.cantidad,0) * b.precio)  - (nvl(d.cantidad,0) * decode(f.costo,'',C.costo,f.costo)) ""Contribucion Comercial"", "
                    sql = sql & "decode(e.tipo,'IMPORTADO',0,DECODE(greatest(c.costo,c.cosprom),c.costo,(nvl(d.cantidad,0) * decode(f.costo,'',C.costo,f.costo))-nvl(d.cantidad,0) * decode(f.costo,'',C.cosprom,f.costo),0)) ""Contrib. Inventario"", "
                    sql = sql & "decode(e.tipo,'IMPORTADO',0,decode(nvl(g.aporte,0),0,0,decode(greatest(c.costo,c.cosprom),c.costo,(nvl(d.cantidad,0) * decode(f.costo,'',C.cosprom,f.costo)) -(nvl(d.cantidad,0) * round(decode(f.costo,'',C.cosprom,f.costo) * (1-(aporte/100)),2)),(nvl(d.cantidad,0) * decode(f.costo,'',C.costo,f.costo))- (nvl(d.cantidad,0) * round(decode(f.costo,'',C.cosprom,f.costo) * (1-(aporte/100)),2))))) ""Contrib. Rebate"", "
                    sql = sql & "decode(e.tipo,'IMPORTADO',DECODE(greatest(c.costo,c.cosprom),c.costo,(nvl(d.cantidad,0) * decode(f.costo,'',C.costo,f.costo))-nvl(d.cantidad,0) * decode(f.costo,'',C.cosprom,f.costo),0),0) ""Contrib Rebate Mp"", "
                    sql = sql & "b.precio ""Precio Cotización"", "
                    sql = sql & "nvl(d.cantidad,0) * b.precio ""Valorizado Anterior"", "
                    sql = sql & "getestadoprod(c.codpro) estado, "
                    sql = sql & "decode(getpedsto(c.codpro),'S','STOCK','P','PEDIDO','VACIO') pedsto, "
                    sql = sql & "0 variacion,GETCOSTOREPOSICION3(C.CODPRO) cosrep, nvl(GETULTIMACOMPRA('3',c.CODPRO,'V'),0) ultcom "
                    sql = sql & "from en_cotizac a,  "
                    sql = sql & "de_cotizac b,  "
                    sql = sql & "ma_product c, "
                    sql = sql & "(select codpro,mpropia_hst,nvl(sum(decode(tipo,'N',cantid*-1,cantid)),0) cantidad "
                    sql = sql & " from qv_control_margen a "
                    sql = sql & "where a.codemp = 3 "
                    sql = sql & "and a.rutcli= " + txtRutcli.Text
                    sql = sql & "and a.fecha between to_date('" + fechaini + "','dd/mm/yyyy')  "
                    sql = sql & "and to_date('" + fechafin + "','dd/mm/yyyy') "
                    sql = sql & "and ((tipo = 'F' and internet in (0,1)) or (tipo = 'G' and internet in (0,2)) or (tipo = 'N' and internet in (1,2,7,9))) "
                    sql = sql & "group by codpro,mpropia_hst "
                    sql = sql & ") d, "
                    'sql = sql & "--qv_mpropia_dimerc_hst e, "
                    sql = sql & "(select codpro,tipo from qv_mpropia_dimerc_hst where (fechavig,codpro) in( "
                    sql = sql & "select max(fechavig),codpro from qv_mpropia_dimerc_hst  "
                    sql = sql & "group by codpro)) e, "
                    sql = sql & "(select codpro,costo,FECINI,FECFIN from en_cliente_costo a where codemp=3 "
                    sql = sql & "    and rutcli= " + txtRutcli.Text
                    sql = sql & "     AND SYSDATE BETWEEN FECINI AND FECFIN "
                    sql = sql & "    and tipo=0) f, "
                    sql = sql & "ma_prod_aporte g "
                    sql = sql & "where a.codemp=b.codemp "
                    sql = sql & "and a.numcot=b.numcot "
                    sql = sql & "and b.codpro=c.codpro "
                    sql = sql & "and a.numcot= " + txtNumcot.Text
                    sql = sql & "and a.codemp=3 "
                    sql = sql & "and c.codpro=d.codpro(+) "
                    sql = sql & "and c.codpro=e.codpro(+) "
                    sql = sql & "and c.codpro=f.codpro(+) "
                    sql = sql & "and c.codpro=g.codpro(+) "
                    sql = sql & ") "
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()



                    'DESPUES DE CARGAR LA TABLA CALCULO ESTADO RESULTADO ACTUAL, LUEGO HARE LAS CORRECCIONES AUTOMATICAS Y PRESNETARE
                    'EL ESTADO RESULTADO NUEVO

                    sql = " select Linea,sum(""Valorizado Precio"") Valorizado, round((sum(""Valorizado Precio"")-sum(""Valorizado Analisis""))/ sum(""Valorizado Precio"")*100,2) ""MG. Lista"", "
                    sql = sql & "round((sum(""Contribucion Comercial"")+ sum(""Contrib. Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comercial"", "
                    sql = sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comer + Aporte MP"", "
                    sql = sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Rebate""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Global"" "
                    sql = sql & " from tmp_proceso_convenio "
                    sql = sql & " where ""Valorizado Precio"">0"
                    sql = sql & " group by linea order by sum(""Valorizado Precio"") desc"
                    Dim dtResumenLinea As New DataTable
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    dataadapter = New OracleDataAdapter(comando)
                    dataadapter.Fill(dtResumenLinea)

                    Dim dtjuntar As New DataTable
                    sql = " select sum(""Valorizado Precio"") Valorizado, round((sum(""Valorizado Precio"")-sum(""Valorizado Analisis""))/ sum(""Valorizado Precio"")*100,2) ""MG. Lista"", "
                    sql = sql & "round((sum(""Contribucion Comercial"")+ sum(""Contrib. Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comercial"", "
                    sql = sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comer + Aporte MP"", "
                    sql = sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Rebate""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Global"" "
                    sql = sql & " from tmp_proceso_convenio "
                    sql = sql & " where ""Valorizado Precio"">0"
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    dataadapter = New OracleDataAdapter(comando)
                    dataadapter.Fill(dtjuntar)

                    dtResumenLinea.Merge(dtjuntar)
                    dgvResumenLínea.DataSource = dtResumenLinea



                    dgvResumenLínea.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
                    dgvResumenLínea.Columns("Valorizado").DefaultCellStyle.Format = "n0"
                    dgvResumenLínea.Columns("Valorizado").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvResumenLínea.Columns("Mg. Lista").DefaultCellStyle.Format = "n2"
                    dgvResumenLínea.Columns("Mg. Lista").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvResumenLínea.Columns("Mg. Comercial").DefaultCellStyle.Format = "n2"
                    dgvResumenLínea.Columns("Mg. Comercial").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvResumenLínea.Columns("Mg. Comer + Aporte MP").DefaultCellStyle.Format = "n2"
                    dgvResumenLínea.Columns("Mg. Comer + Aporte MP").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvResumenLínea.Columns("Mg. Global").DefaultCellStyle.Format = "n2"
                    dgvResumenLínea.Columns("Mg. Global").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight



                    sql = " select Linea, sum(""Valorizado Precio"") Valorizado,round((sum(""Valorizado Precio"")-sum(""Valorizado Analisis""))/ sum(""Valorizado Precio"")*100,2) ""MG. Lista"", "
                    sql = sql & "round((sum(""Contribucion Comercial"")+ sum(""Contrib. Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comercial"", "
                    sql = sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comer + Aporte MP"", "
                    sql = sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Rebate""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Global"" "
                    sql = sql & "from tmp_proceso_convenio "
                    sql = sql & " where ""Valorizado Precio"">0"
                    sql = sql & "group by linea order by sum(""Valorizado Precio"") desc"
                    Dim dtDatosMp As New DataTable
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    dataadapter = New OracleDataAdapter(comando)
                    dataadapter.Fill(dtDatosMp)
                    dgvDatosMP.DataSource = dtDatosMp

                    Dim dtjuntar2 As New DataTable
                    sql = " select sum(""Valorizado Precio"") Valorizado,round((sum(""Valorizado Precio"")-sum(""Valorizado Analisis""))/ sum(""Valorizado Precio"")*100,2) ""MG. Lista"", "
                    sql = sql & "round((sum(""Contribucion Comercial"")+ sum(""Contrib. Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comercial"", "
                    sql = sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comer + Aporte MP"", "
                    sql = sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Rebate""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Global"" "
                    sql = sql & " from tmp_proceso_convenio "
                    sql = sql & " where ""Valorizado Precio"">0"
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    dataadapter = New OracleDataAdapter(comando)
                    dataadapter.Fill(dtjuntar2)
                    dtDatosMp.Merge(dtjuntar2)
                    dgvDatosMP.DataSource = dtDatosMp


                    dgvDatosMP.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
                    dgvDatosMP.Columns("Valorizado").DefaultCellStyle.Format = "n0"
                    dgvDatosMP.Columns("Valorizado").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvDatosMP.Columns("Mg. Lista").DefaultCellStyle.Format = "n2"
                    dgvDatosMP.Columns("Mg. Lista").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvDatosMP.Columns("Mg. Comercial").DefaultCellStyle.Format = "n2"
                    dgvDatosMP.Columns("Mg. Comercial").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvDatosMP.Columns("Mg. Comer + Aporte MP").DefaultCellStyle.Format = "n2"
                    dgvDatosMP.Columns("Mg. Comer + Aporte MP").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvDatosMP.Columns("Mg. Global").DefaultCellStyle.Format = "n2"
                    dgvDatosMP.Columns("Mg. Global").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight



                    sql = "select nvl(sum(""Valorizado Precio"") ,0)precio,sum(""Valorizado Analisis"") costo, "
                    sql += " round((sum(""Valorizado Precio"")-sum(""Valorizado Rebate""))/ sum(""Valorizado Precio"")*100,2) margen, round(avg(""Mg. Inventario""),2) margen2 from tmp_proceso_convenio where cantidad>0"
                    Dim dt2 As New DataTable
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    dataadapter = New OracleDataAdapter(comando)
                    dataadapter.Fill(dt2)
                    If dt2.Rows.Count > 0 Then

                        If Convert.ToDouble(dt2.Rows(0).Item("precio")) > 0 Then
                            txtTotIngreso.Text = dt2.Rows(0).Item("precio")
                            txtTotContr.Text = Math.Round(dt2.Rows(0).Item("Precio") * (dt2.Rows(0).Item("margen") / 100))
                            txtPorTotCon.Text = dt2.Rows(0).Item("margen")
                            txtPorTotCon.Text = FormatPercent(txtPorTotCon.Text / 100, 2)
                            txtTotMargenFinal.Text = (dt2.Rows(0).Item("margen"))
                            txtTotMargenFinal.Text = FormatPercent(txtTotMargenFinal.Text / 100, 2)
                            txtTotIngreso.Text = FormatNumber(txtTotIngreso.Text, 0)
                            txtTotContr.Text = FormatNumber(txtTotContr.Text, 0)
                            txtValAnt.Text = dt2.Rows(0).Item("precio")
                            txtValAct.Text = dt2.Rows(0).Item("precio")
                            txtValAnt.Text = FormatNumber(txtValAnt.Text, 0)
                            txtValAct.Text = FormatNumber(txtValAct.Text, 0)
                            txtPorVariacion.Text = "0"
                        Else
                            txtTotIngreso.Text = "0"
                            txtTotContr.Text = "0"
                            txtTotMargenFinal.Text = "0"
                            txtPorTotCon.Text = "0"
                            txtValAnt.Text = "0"
                            txtValAct.Text = "0"
                            txtValAnt.Text = FormatNumber(txtValAnt.Text, 0)
                            txtValAct.Text = FormatNumber(txtValAct.Text, 0)
                            txtPorVariacion.Text = "0"
                            txtPorTotCon.Text = FormatPercent(txtPorTotCon.Text / 100, 2)
                        End If

                    End If

                    'CORRIJO MARGEN DE INVENTARIO, TODOS DEBEN SER MAYOR A 12

                    sql = " update tmp_proceso_convenio  "
                    sql = sql & "set precio = round(""Costo Promedio"" /0.88), "
                    sql = sql & "variacion = round(((round((""Costo Promedio"" /0.88))- precio) / precio) * 100,2), "
                    sql = sql & """Valorizado Precio"" = round(""Costo Promedio"" /0.88) * cantidad, "
                    sql = sql & """Mg. Inventario"" = round((1-""Costo Promedio""/round(""Costo Promedio"" /0.88))*100,2), "
                    sql = sql & """Mg. Comercial"" = round((1-""Costo Analisis""/round(""Costo Promedio"" /0.88))*100,2), "
                    sql = sql & """Mg. Rebate"" = round((1-(""Costo Promedio"" * (1-(APORTE/100)))/round(""Costo Promedio"" /0.88))*100,2), "
                    sql = sql & """Contribucion Comercial"" = (CANTIDAD* round(""Costo Promedio"" /0.88))  - (CANTIDAD * ""Costo Analisis"") "
                    sql = sql & "where ""Mg. Inventario"" <12 "

                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    If Convert.ToDouble(txtTotIngreso.Text) > 0 Then
                        'CALCULA EL PESO
                        sql = " UPDATE TMP_PROCESO_CONVENIO C SET PESO = ( "
                        sql = sql & "SELECT ROUND(((A.CANTIDAD*A.PRECIO)/ B.TOTAL)*100,2)  PESO  "
                        sql = sql & "FROM TMP_PROCESO_CONVENIO A, "
                        sql = sql & "( "
                        sql = sql & "SELECT SUM(CANTIDAD * PRECIO) TOTAL  "
                        sql = sql & "FROM TMP_PROCESO_CONVENIO "
                        sql = sql & ") B "
                        sql = sql & "WHERE C.CODPRO=A.CODPRO)  "

                        comando = New OracleCommand(sql, conn)
                        comando.Transaction = transaccion
                        comando.ExecuteNonQuery()
                    End If

                    'COmienza a generar propuesta de costo especial
                    'SOLO PARA PRODUCTOS IMPORTADO
                    'Si margen Inventario > 60 y mg. Comercial <20, se corrige el mg. comercial y se deja en 20

                    sql = " update tmp_proceso_convenio  "
                    sql = sql & "set ""Costo Especial"" = round(precio * 0.8), "
                    sql = sql & " ""Costo Analisis"" = round(precio * 0.8), "
                    sql = sql & " ""Valorizado Analisis"" = round(precio * 0.8) * cantidad, "
                    sql = sql & """Mg. Comercial"" = round((1-round(precio * 0.8)/precio)*100,2) "
                    sql = sql & "where ""Mg. Inventario"" > 60 and ""Mg. Comercial"" <20 and mpropia='IMPORTADO' "

                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    sql = " update tmp_proceso_convenio  "
                    sql = sql & "set ""Costo Especial"" = round(precio * 0.85), "
                    sql = sql & " ""Costo Analisis"" = round(precio * 0.85), "
                    sql = sql & " ""Valorizado Analisis"" = round(precio * 0.85) * cantidad, "
                    sql = sql & """Mg. Comercial"" = round((1-round(precio * 0.85)/precio)*100,2) "
                    sql = sql & "where ""Mg. Inventario"" > 50 and ""Mg. Inventario"" < 60 and ""Mg. Comercial"" < 15 and mpropia='IMPORTADO' "

                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    sql = " update tmp_proceso_convenio  "
                    sql = sql & "set ""Costo Especial"" = round(precio * 0.88), "
                    sql = sql & " ""Costo Analisis"" = round(precio * 0.88), "
                    sql = sql & " ""Valorizado Analisis"" = round(precio * 0.88) * cantidad, "
                    sql = sql & """Mg. Comercial"" = round((1-round(precio * 0.88)/precio)*100,2) "
                    sql = sql & "where ""Mg. Inventario"" > 40 and ""Mg. Inventario"" < 50 and ""Mg. Comercial"" < 12 and mpropia='IMPORTADO' "

                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    sql = " update tmp_proceso_convenio  "
                    sql = sql & "set ""Costo Especial"" = round(precio * 0.90), "
                    sql = sql & " ""Costo Analisis"" = round(precio * 0.90), "
                    sql = sql & " ""Valorizado Analisis"" = round(precio * 0.90) * cantidad, "
                    sql = sql & """Mg. Comercial"" = round((1-round(precio * 0.90)/precio)*100,2) "
                    sql = sql & "where ""Mg. Inventario"" > 30 and ""Mg. Inventario"" < 40 and ""Mg. Comercial"" < 10 and mpropia='IMPORTADO' "

                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    'SOLO PARA PRODUCTOS QUE NO SEAN IMPORTADO
                    'Si margen Inventario >12 y <15 se generan costos para que margen comercial quede 1 punto debajo del margen inventario

                    sql = " update tmp_proceso_convenio "
                    sql = sql & "set ""Costo Especial"" = round(precio * ((100-(""Mg. Inventario""-1))/100)),  "
                    sql = sql & " ""Costo Analisis"" = round(precio * ((100-(""Mg. Inventario""-1))/100)), "
                    sql = sql & " ""Valorizado Analisis"" = round(precio * ((100-(""Mg. Inventario""-1))/100)) * cantidad, "
                    sql = sql & """Mg. Comercial"" = round((1-round(precio * ((100-(""Mg. Inventario""-1))/100))/precio) * 100,2)  "
                    sql = sql & "where ""Mg. Inventario"" > 11 and ""Mg. Inventario""<15 and ""Mg. Comercial"" < (""Mg. Inventario""-1) and not mpropia ='IMPORTADO'"

                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    'Si margen Inventario >15 y <20 se generan costos para que margen comercial quede 3 punto debajo del margen inventario

                    sql = " update tmp_proceso_convenio "
                    sql = sql & "set ""Costo Especial"" = round(precio * ((100-(""Mg. Inventario""-3))/100)),  "
                    sql = sql & " ""Costo Analisis"" = round(precio * ((100-(""Mg. Inventario""-3))/100)),  "
                    sql = sql & " ""Valorizado Analisis"" = round(precio * ((100-(""Mg. Inventario""-3))/100)) * cantidad, "
                    sql = sql & """Mg. Comercial"" = round((1-round(precio * ((100-(""Mg. Inventario""-3))/100))/precio) * 100,2)  "
                    sql = sql & "where ""Mg. Inventario"" > 15 and ""Mg. Inventario""<20 and ""Mg. Comercial"" < (""Mg. Inventario""-3) and not mpropia ='IMPORTADO'"

                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    'Si margen Inventario >20 y <30 se generan costos para que margen comercial quede 5 puntos debajo del margen inventario

                    sql = " update tmp_proceso_convenio "
                    sql = sql & "set ""Costo Especial"" = round(precio * ((100-(""Mg. Inventario""-5))/100)),  "
                    sql = sql & " ""Costo Analisis"" = round(precio * ((100-(""Mg. Inventario""-5))/100)),  "
                    sql = sql & " ""Valorizado Analisis"" = round(precio * ((100-(""Mg. Inventario""-5))/100)) * cantidad, "
                    sql = sql & """Mg. Comercial"" = round((1-round(precio * ((100-(""Mg. Inventario""-5))/100))/precio) * 100,2)  "
                    sql = sql & "where ""Mg. Inventario"" > 20 and ""Mg. Inventario""<30 and ""Mg. Comercial"" < (""Mg. Inventario""-5) and not mpropia ='IMPORTADO'"

                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    'Si margen Inventario >30 y <40 se generan costos para que margen comercial quede 10 puntos debajo del margen inventario

                    sql = " update tmp_proceso_convenio "
                    sql = sql & "set ""Costo Especial"" = round(precio * ((100-(""Mg. Inventario""-10))/100)),  "
                    sql = sql & " ""Costo Analisis"" = round(precio * ((100-(""Mg. Inventario""-10))/100)),  "
                    sql = sql & " ""Valorizado Analisis"" = round(precio * ((100-(""Mg. Inventario""-10))/100)) * cantidad, "
                    sql = sql & """Mg. Comercial"" = round((1-round(precio * ((100-(""Mg. Inventario""-10))/100))/precio) * 100,2)  "
                    sql = sql & "where ""Mg. Inventario"" > 30 and ""Mg. Inventario""<40 and ""Mg. Comercial"" < (""Mg. Inventario""-10) and not mpropia ='IMPORTADO'"

                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    'Si margen Inventario >40 y <60 se generan costos para que margen comercial quede 15 puntos debajo del margen inventario

                    sql = " update tmp_proceso_convenio "
                    sql = sql & "set ""Costo Especial"" = round(precio * ((100-(""Mg. Inventario""-15))/100)),  "
                    sql = sql & " ""Costo Analisis"" = round(precio * ((100-(""Mg. Inventario""-15))/100)),  "
                    sql = sql & " ""Valorizado Analisis"" = round(precio * ((100-(""Mg. Inventario""-15))/100)) * cantidad, "
                    sql = sql & """Mg. Comercial"" = round((1-round(precio * ((100-(""Mg. Inventario""-15))/100))/precio) * 100,2)  "
                    sql = sql & "where ""Mg. Inventario"" > 40 and ""Mg. Inventario""<60 and ""Mg. Comercial"" < (""Mg. Inventario""-15) and not mpropia ='IMPORTADO'"

                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    'Si margen Inventario >60  se generan costos para que margen comercial quede 20 puntos debajo del margen inventario

                    sql = " update tmp_proceso_convenio "
                    sql = sql & "set ""Costo Especial"" = round(precio * ((100-(""Mg. Inventario""-20))/100)),  "
                    sql = sql & " ""Costo Analisis"" = round(precio * ((100-(""Mg. Inventario""-20))/100)),  "
                    sql = sql & " ""Valorizado Analisis"" = round(precio * ((100-(""Mg. Inventario""-20))/100)) * cantidad, "
                    sql = sql & """Mg. Comercial"" = round((1-round(precio * ((100-(""Mg. Inventario""-20))/100))/precio) * 100,2)  "
                    sql = sql & "where ""Mg. Inventario"" > 40  and ""Mg. Comercial"" < (""Mg. Inventario""-20) and not mpropia ='IMPORTADO'"

                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    sql = " update tmp_proceso_convenio set  "
                    sql = sql & """Costo Rebate""= round(""Costo Promedio"" * (1-(nvl(aporte,0)/100)),2)  "
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    sql = " update tmp_proceso_convenio set  "
                    sql = sql & """Valorizado Analisis""=""Costo Analisis"" * cantidad, "
                    sql = sql & """Valorizado Precio""=PRECIO * cantidad, "
                    sql = sql & """Valorizado Rebate"" = round(""Costo Rebate""*cantidad), "
                    sql = sql & """Valorizado Inventario"" = round(""Costo Promedio""*cantidad) "
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    sql = " update tmp_proceso_convenio set  "
                    sql = sql & """Contribucion Comercial"" = ""Valorizado Precio""-""Valorizado Analisis"", "
                    sql = sql & """Contrib. Inventario"" =round(decode(mpropia,'IMPORTADO',0,DECODE(greatest(""Costo Analisis"",""Costo Promedio""),""Costo Analisis"",""Valorizado Analisis""-""Valorizado Inventario"",0))), "
                    sql = sql & """Contrib. Rebate""= decode(mpropia,'IMPORTADO',0,decode(aporte,0,0,Decode(greatest(""Costo Promedio"",""Costo Rebate""),""Costo Promedio"", greatest(""Costo Analisis"",""Costo Promedio""), ""Costo Analisis"", (""Valorizado Inventario""-""Valorizado Rebate""),(""Valorizado Analisis""-""Valorizado Rebate"")) )), "
                    sql = sql & """Contrib Rebate Mp"" = decode(mpropia,'IMPORTADO',""Valorizado Analisis""-""Valorizado Inventario"" ,0) "
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    transaccion.Commit()

                Catch ex As Exception
                    MessageBox.Show(ex.ToString, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    If Not transaccion.Equals(Nothing) Then
                        transaccion.Rollback()
                    End If
                Finally
                    conn.Close()
                End Try

            End Using
        End Using



    End Sub

    Private Sub cmbMarcaPropia_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbMarcaPropia.SelectedIndexChanged
        If flag = True Then
            Try

                bd.open_dimerc()
                Dim Sql = " select Linea, sum(""Valorizado Precio"") Valorizado,round((sum(""Valorizado Precio"")-sum(""Valorizado Analisis""))/ sum(""Valorizado Precio"")*100,2) ""MG. Lista"", "
                Sql = Sql & "round((sum(""Contribucion Comercial"")+ sum(""Contrib. Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comercial"", "
                Sql = Sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comer + Aporte MP"", "
                Sql = Sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Rebate""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Global"" "
                Sql = Sql & " from tmp_proceso_convenio "
                Sql = Sql & " where ""Valorizado Precio"">0"
                If cmbMarcaPropia.SelectedIndex > 0 Then
                    Sql = Sql & " and mpropia='" + cmbMarcaPropia.SelectedValue.ToString + "' "
                End If
                Sql = Sql & "group by linea order by sum(""Valorizado Precio"") desc"
                Dim dtDatosMp As New DataTable
                dtDatosMp = bd.sqlSelect(Sql)


                Dim dtjuntar2 As New DataTable
                Sql = " select sum(""Valorizado Precio"") Valorizado,round((sum(""Valorizado Precio"")-sum(""Valorizado Analisis""))/ sum(""Valorizado Precio"")*100,2) ""MG. Lista"", "
                Sql = Sql & "round((sum(""Contribucion Comercial"")+ sum(""Contrib. Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comercial"", "
                Sql = Sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comer + Aporte MP"", "
                Sql = Sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Rebate""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Global"" "
                Sql = Sql & "from tmp_proceso_convenio "
                Sql = Sql & " where ""Valorizado Precio"">0"
                If cmbMarcaPropia.SelectedIndex > 0 Then
                    Sql = Sql & "and mpropia='" + cmbMarcaPropia.SelectedValue.ToString + "' "
                End If
                dtjuntar2 = bd.sqlSelect(Sql)
                dtDatosMp.Merge(dtjuntar2)
                dgvDatosMP.DataSource = dtDatosMp


                dgvDatosMP.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
                dgvDatosMP.Columns("Valorizado").DefaultCellStyle.Format = "n0"
                dgvDatosMP.Columns("Valorizado").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgvDatosMP.Columns("Mg. Lista").DefaultCellStyle.Format = "n2"
                dgvDatosMP.Columns("Mg. Lista").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgvDatosMP.Columns("Mg. Comercial").DefaultCellStyle.Format = "n2"
                dgvDatosMP.Columns("Mg. Comercial").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgvDatosMP.Columns("Mg. Comer + Aporte MP").DefaultCellStyle.Format = "n2"
                dgvDatosMP.Columns("Mg. Comer + Aporte MP").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgvDatosMP.Columns("Mg. Global").DefaultCellStyle.Format = "n2"
                dgvDatosMP.Columns("Mg. Global").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight




            Catch ex As Exception
                MessageBox.Show(ex.ToString, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Finally
                bd.close()
            End Try
        End If
    End Sub
    Private Sub formatogrilla()
        dgvDetalleLineaProductos.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
        dgvDetalleLineaProductos.Columns("Cantidad").DefaultCellStyle.Format = "n0"
        dgvDetalleLineaProductos.Columns("Cantidad").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Precio").DefaultCellStyle.Format = "n0"
        dgvDetalleLineaProductos.Columns("Precio").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Costo Analisis").DefaultCellStyle.Format = "n0"
        dgvDetalleLineaProductos.Columns("Costo Analisis").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Costo Comercial").DefaultCellStyle.Format = "n0"
        dgvDetalleLineaProductos.Columns("Costo Comercial").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Costo Promedio").DefaultCellStyle.Format = "n0"
        dgvDetalleLineaProductos.Columns("Costo Promedio").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Costo Especial").DefaultCellStyle.Format = "n0"
        dgvDetalleLineaProductos.Columns("Costo Especial").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Costo Especial PM").DefaultCellStyle.Format = "n0"
        dgvDetalleLineaProductos.Columns("Costo Especial PM").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Costo Rebate").DefaultCellStyle.Format = "n0"
        dgvDetalleLineaProductos.Columns("Costo Rebate").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Aporte").DefaultCellStyle.Format = "n2"
        dgvDetalleLineaProductos.Columns("Aporte").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Peso").DefaultCellStyle.Format = "n2"
        dgvDetalleLineaProductos.Columns("Peso").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Mg. Comercial").DefaultCellStyle.Format = "n2"
        dgvDetalleLineaProductos.Columns("Mg. Comercial").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Mg. Inventario").DefaultCellStyle.Format = "n2"
        dgvDetalleLineaProductos.Columns("Mg. Inventario").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Mg. Rebate").DefaultCellStyle.Format = "n2"
        dgvDetalleLineaProductos.Columns("Mg. Rebate").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Valorizado Precio").DefaultCellStyle.Format = "n0"
        dgvDetalleLineaProductos.Columns("Valorizado Precio").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Valorizado Analisis").DefaultCellStyle.Format = "n0"
        dgvDetalleLineaProductos.Columns("Valorizado Analisis").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Valorizado Inventario").DefaultCellStyle.Format = "n0"
        dgvDetalleLineaProductos.Columns("Valorizado Inventario").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Valorizado Rebate").DefaultCellStyle.Format = "n0"
        dgvDetalleLineaProductos.Columns("Valorizado Rebate").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Contribucion Comercial").DefaultCellStyle.Format = "n0"
        dgvDetalleLineaProductos.Columns("Contribucion Comercial").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Contrib. Inventario").DefaultCellStyle.Format = "n0"
        dgvDetalleLineaProductos.Columns("Contrib. Inventario").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Contrib. Rebate").DefaultCellStyle.Format = "n0"
        dgvDetalleLineaProductos.Columns("Contrib. Rebate").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Contrib Rebate MP").DefaultCellStyle.Format = "n0"
        dgvDetalleLineaProductos.Columns("Contrib Rebate MP").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Precio Cotización").DefaultCellStyle.Format = "n0"
        dgvDetalleLineaProductos.Columns("Precio Cotización").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Valorizado Anterior").DefaultCellStyle.Format = "n0"
        dgvDetalleLineaProductos.Columns("Valorizado Anterior").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Variacion").DefaultCellStyle.Format = "n2"
        dgvDetalleLineaProductos.Columns("Variacion").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Cosrep").DefaultCellStyle.Format = "n2"
        dgvDetalleLineaProductos.Columns("Cosrep").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Ultcom").DefaultCellStyle.Format = "n2"
        dgvDetalleLineaProductos.Columns("Ultcom").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
    End Sub

    Private Sub txtCodpro_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPrecio.KeyPress, txtDescripcion.KeyPress, txtCodpro.KeyPress
        e.Handled = True
    End Sub

    Private Sub dgvDetalleLineaProductos_CurrentCellChanged(sender As Object, e As EventArgs) Handles dgvDetalleLineaProductos.CurrentCellChanged
        If flaggrilla = True And Not dgvDetalleLineaProductos.CurrentRow Is Nothing Then
            txtCodpro.Text = dgvDetalleLineaProductos.Item("Codpro", dgvDetalleLineaProductos.CurrentRow.Index).Value.ToString
            txtDescripcion.Text = dgvDetalleLineaProductos.Item("Despro", dgvDetalleLineaProductos.CurrentRow.Index).Value.ToString
            txtPrecio.Text = dgvDetalleLineaProductos.Item("Precio", dgvDetalleLineaProductos.CurrentRow.Index).Value.ToString
            txtPrecioNuevo.Text = dgvDetalleLineaProductos.Item("Precio", dgvDetalleLineaProductos.CurrentRow.Index).Value.ToString
            txtCostoEspecial.Text = dgvDetalleLineaProductos.Item("Costo Especial", dgvDetalleLineaProductos.CurrentRow.Index).Value.ToString
        End If
    End Sub

    Private Sub dgvDetalleLineaProductos_KeyDown(sender As Object, e As KeyEventArgs) Handles dgvDetalleLineaProductos.KeyDown
        If e.KeyValue = Keys.Enter Then
            txtVariacion.Focus()
            indice = dgvDetalleLineaProductos.CurrentRow.Index
            e.Handled = True
        End If

    End Sub

    Private Sub txtVariacion_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtVariacion.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Not Trim(txtVariacion.Text) = "," Then
                txtPrecioNuevo.Text = Math.Round(txtPrecio.Text * (1 + (txtVariacion.Text / 100)))
            End If
            txtCostoEspecial.Focus()
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If

    End Sub
    Private Sub txtCostoEspecial_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCostoEspecial.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            btn_Confirmar.Focus()
        End If
    End Sub
    Private Sub cambiaprecio()
        Using conn = New OracleConnection(Conexion.retornaConexion)
            conn.Open()
            Dim comando As OracleCommand
            Using transaccion = conn.BeginTransaction
                Try
                    If Trim(txtCostoEspecial.Text) = "" Then
                        txtCostoEspecial.Text = "0"
                    End If
                    Dim Sql = " update tmp_proceso_convenio  "
                    Sql = Sql & "set precio = " + txtPrecioNuevo.Text + ", "
                    Sql = Sql & "variacion = round(((" + txtPrecioNuevo.Text + "- precio) / precio) * 100,2), "
                    Sql = Sql & """Valorizado Precio"" = " + txtPrecioNuevo.Text + " * cantidad, "
                    If Convert.ToDouble(txtCostoEspecial.Text) > 0 Then
                        Sql = Sql & """Costo Especial""= " + txtCostoEspecial.Text + ","
                        Sql = Sql & """Costo Analisis""= " + txtCostoEspecial.Text + ","
                    End If
                    Sql = Sql & """Mg. Rebate"" = round((1-(""Costo Promedio"" * (1-(APORTE/100)))/" + txtPrecioNuevo.Text + ")*100,2) "
                    Sql = Sql & "where codpro='" + dgvDetalleLineaProductos.Item("Codpro", indice).Value.ToString + "' "

                    comando = New OracleCommand(Sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    Sql = " update tmp_proceso_convenio set  "
                    Sql = Sql & """Valorizado Analisis""=""Costo Analisis"" * cantidad, "
                    Sql = Sql & """Mg. Comercial"" = round((1-""Costo Analisis""/precio)*100,2), "
                    Sql = Sql & """Mg. Inventario"" = round((1-""Costo Promedio""/precio)*100,2), "
                    Sql = Sql & """Contribucion Comercial"" = ""Valorizado Precio""-""Valorizado Analisis"", "
                    Sql = Sql & """Contrib. Inventario"" =round(decode(mpropia,'IMPORTADO',0,DECODE(greatest(""Costo Analisis"",""Costo Promedio""),""Costo Analisis"",""Valorizado Analisis""-""Valorizado Inventario"",0))) "

                    comando = New OracleCommand(Sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    transaccion.Commit()

                    flaggrilla = False
                    Sql = "select * from tmp_proceso_convenio"
                    dgvDetalleLineaProductos.DataSource = bd.sqlSelect(Sql)
                    formatogrilla()
                    flaggrilla = True



                    MessageBox.Show("Cambio Listo")

                Catch ex As Exception
                    If Not transaccion.Equals(Nothing) Then
                        transaccion.Rollback()
                    End If
                    MessageBox.Show(ex.ToString, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Finally
                    conn.Close()
                End Try
            End Using
        End Using
    End Sub

    Private Sub recalculaEstadoResultado()
        Try
            bd.open_dimerc()

            Dim sql = "select nvl(sum(""Valorizado Precio""),0) Ingreso, sum(cantidad * ""Costo Promedio"") costo "
            sql += ", round((sum(""Valorizado Precio"")-sum(""Valorizado Rebate""))/ sum(""Valorizado Precio"")*100,2) margen from tmp_proceso_convenio where cantidad>0"
            Dim dt = bd.sqlSelect(sql)
            If dt.Rows.Count > 0 Then
                If dt.Rows(0).Item("Ingreso") > 0 Then
                    txtTotIngresiFinal.Text = dt.Rows(0).Item("Ingreso")
                    txtValAct.Text = dt.Rows(0).Item("Ingreso")
                    txtTotContriFinal.Text = Math.Round(dt.Rows(0).Item("Ingreso") * (dt.Rows(0).Item("margen") / 100))
                    txtPorContriFinal.Text = dt.Rows(0).Item("margen")
                    txtPorContriFinal.Text = FormatPercent(txtPorContriFinal.Text / 100, 2)
                    txtMgFinal.Text = (dt.Rows(0).Item("margen"))
                    txtMgFinal.Text = FormatPercent(txtMgFinal.Text / 100, 2)
                    txtTotIngresiFinal.Text = FormatNumber(txtTotIngresiFinal.Text, 0)
                    txtTotContriFinal.Text = FormatNumber(txtTotContriFinal.Text, 0)
                    txtValAct.Text = FormatNumber(txtValAct.Text, 0)
                    txtPorVariacion.Text = (txtValAct.Text - txtValAnt.Text) / txtValAct.Text
                    'txtPorContriFinal.Text = FormatPercent(txtPorContriFinal.Text, 2)
                    txtPorVariacion.Text = FormatPercent(txtPorVariacion.Text, 2)
                    calculaEstadoFinal()
                Else
                    txtTotIngresiFinal.Text = "0"
                    txtValAct.Text = "0"
                    txtTotContriFinal.Text = "0"
                    txtPorContriFinal.Text = "0"
                    txtMgFinal.Text = "0"
                    txtMgFinal.Text = FormatPercent(txtMgFinal.Text, 2)
                    txtTotIngresiFinal.Text = FormatNumber(txtTotIngresiFinal.Text, 0)
                    txtTotContriFinal.Text = FormatNumber(txtTotContriFinal.Text, 0)
                    txtValAct.Text = FormatNumber(txtValAct.Text, 0)
                    txtPorVariacion.Text = "0"
                    'txtPorContriFinal.Text = FormatPercent(txtPorContriFinal.Text, 2)
                    txtPorVariacion.Text = FormatPercent(txtPorVariacion.Text, 2)
                    calculaEstadoFinal 
                End If

            End If

        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub btn_Confirmar_Click(sender As Object, e As EventArgs) Handles btn_Confirmar.Click
        cambiaprecio()
        recalculaEstadoResultado()
    End Sub

    Private Sub btnExcel_Click(sender As Object, e As EventArgs)
        exportar()
    End Sub

    Private Sub btn_Grabar_Click(sender As Object, e As EventArgs)
        If MessageBox.Show("¿Desea grabar los datos de este reajuste?", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            grabareajuste()
        End If
    End Sub
    Private Sub grabareajuste()
        Using conn = New OracleConnection
            conn.Open()
            Dim comando As OracleCommand
            Using transaccion = conn.BeginTransaction
                Try


                    transaccion.Commit()
                Catch ex As Exception
                    If Not transaccion.Equals(Nothing) Then
                        transaccion.Rollback()
                    End If
                    MessageBox.Show(ex.ToString, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Finally
                    conn.Close()
                End Try
            End Using
        End Using
    End Sub

    Private Sub txtPorTransporte_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPorTransporte.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Trim(txtPorTransporte.Text) <> "," Then
                txtTotTrans.Text = Math.Round((txtPorTransporte.Text / 100) * txtTotIngreso.Text)
                txtPorTransFinal.Text = txtPorTransporte.Text
                txtTotTransFinal.Text = Math.Round((txtPorTransFinal.Text / 100) * txtTotIngresiFinal.Text)
                txtPorVenta.Focus()
                formatear()
                calculaEstado()
                calculaEstadoFinal()
            End If
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtPorTransFinal_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPorTransFinal.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Trim(txtPorTransFinal.Text) <> "," Then
                txtTotTransFinal.Text = Math.Round((txtPorTransFinal.Text / 100) * txtTotIngresiFinal.Text)
                formatear()
                txtPorVentaFinal.Focus()
                calculaEstadoFinal()
            End If
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtPorVenta_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPorVenta.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Trim(txtPorVenta.Text) <> "," Then
                Dim costo = (Convert.ToDouble(txtTotIngreso.Text) - sumacosto) / Convert.ToDouble(txtTotIngreso.Text)
                txtTotVenta.Text = txtTotIngreso.Text * getMargenComercial() * (txtPorVenta.Text / 100)
                txtPorVentaFinal.Text = txtPorVenta.Text
                txtTotVentaFinal.Text = txtTotIngresiFinal.Text * getMargenComercial() * (txtPorVentaFinal.Text / 100)
                txtPorGastoOp.Focus()
                formatear()
                calculaEstado()
                calculaEstadoFinal()
            End If
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtPorVentaFinal_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPorVentaFinal.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Trim(txtPorVentaFinal.Text) <> "," Then
                Dim costo = (Convert.ToDouble(txtTotIngresiFinal.Text) - sumacostoFinal) / Convert.ToDouble(txtTotIngresiFinal.Text)
                txtTotVentaFinal.Text = txtTotIngresiFinal.Text * getMargenComercial() * (txtPorVentaFinal.Text / 100)

                txtPorGastoOpFinal.Focus()
                formatear()
                calculaEstadoFinal()
            End If

        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub
    Private Function getMargenComercial()
        Try
            bd.open_dimerc()
            Dim sql = "select round(1-sum(""Valorizado Analisis"")/sum(""Valorizado Precio"") ,4) margen from tmp_proceso_convenio"
            Dim dt As DataTable = bd.sqlSelect(sql)
            If dt.Rows.Count > 0 Then
                Return Convert.ToDouble(dt.Rows(0).Item("margen"))
            End If
        Catch ex As Exception
            Return 0
        Finally
            bd.close()
        End Try
    End Function
    Private Sub txtPorGastoOp_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPorGastoOp.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then

            If Trim(txtPorGastoOp.Text) <> "," Then
                txtGastoOp.Text = Math.Round((txtPorGastoOp.Text / 100) * txtTotIngreso.Text)
                txtPorGastoOpFinal.Text = txtPorGastoOp.Text
                txtTotGastOpFinal.Text = Math.Round((txtPorGastoOpFinal.Text / 100) * txtTotIngresiFinal.Text)
                txtPorRebate.Focus()
                formatear()
                calculaEstado()
                calculaEstadoFinal()
            End If
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If

    End Sub

    Private Sub txtPorGastoOpFinal_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPorGastoOpFinal.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Trim(txtPorGastoOpFinal.Text) <> "," Then
                txtTotGastOpFinal.Text = Math.Round((txtPorGastoOpFinal.Text / 100) * txtTotIngresiFinal.Text)

                txtPorRebateFinal.Focus()
                formatear()
                calculaEstadoFinal()
            End If
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtPorRebate_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPorRebate.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Trim(txtPorRebate.Text) <> "," Then
                txtPorRebateFinal.Text = txtPorRebate.Text
                txtTotRebate.Text = Math.Round((Trim(txtPorRebate.Text) / 100) * txtTotIngreso.Text)
                txtTotRebateFinal.Text = Math.Round((Trim(txtPorRebateFinal.Text) / 100) * txtTotIngresiFinal.Text)
                formatear()
                calculaEstado()
                calculaEstadoFinal()
            End If
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If

    End Sub

    Private Sub txtPorRebateFinal_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPorRebateFinal.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Trim(txtPorRebateFinal.Text <> ",") Then
                txtTotRebateFinal.Text = Math.Round((Trim(txtPorRebateFinal.Text) / 100) * txtTotIngresiFinal.Text)
                formatear()
                calculaEstadoFinal()
            End If
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub calculaEstado()
        txtTotGasto.Text = ""
        Dim valor As Double = Convert.ToDouble(txtTotRebate.Text) + Convert.ToDouble(txtTotVenta.Text) + Convert.ToDouble(txtTotTrans.Text) + Convert.ToDouble(txtGastoOp.Text)
        txtTotGasto.Text = valor
        txtTotGasto.Text = FormatNumber(txtTotGasto.Text, 0)
        txtTotMargenFinal.Text = ""
        txtTotMargenFinal.Text = (Convert.ToDouble(txtTotContr.Text) - Convert.ToDouble(txtTotGasto.Text)) / Convert.ToDouble(txtTotIngreso.Text)
        txtTotMargenFinal.Text = FormatPercent(txtTotMargenFinal.Text, 2)
        txtTotRebate.Text = FormatNumber(txtTotRebate.Text, 0)
        txtGastoOp.Text = FormatNumber(txtGastoOp.Text, 0)
        txtTotVenta.Text = FormatNumber(txtTotVenta.Text, 0)
        txtTotTrans.Text = FormatNumber(txtTotTrans.Text, 0)
    End Sub

    Private Sub calculaEstadoFinal()
        txtTotTransFinal.Text = Math.Round((txtPorTransFinal.Text / 100) * txtTotIngresiFinal.Text)
        txtTotVentaFinal.Text = txtTotIngresiFinal.Text * getMargenComercial() * (txtPorVentaFinal.Text / 100)
        txtTotGastOpFinal.Text = Math.Round((txtPorGastoOpFinal.Text / 100) * txtTotIngresiFinal.Text)
        txtTotRebateFinal.Text = Math.Round((Trim(txtPorRebateFinal.Text) / 100) * txtTotIngresiFinal.Text)
        txtTotGastoFinal.Text = ""
        Dim valor As Double = Convert.ToDouble(txtTotRebateFinal.Text) + Convert.ToDouble(txtTotVentaFinal.Text) + Convert.ToDouble(txtTotTransFinal.Text) + Convert.ToDouble(txtTotGastOpFinal.Text)
        txtTotGastoFinal.Text = valor
        txtTotGastoFinal.Text = FormatNumber(txtTotGastoFinal.Text, 0)
        txtMgFinal.Text = ""
        txtMgFinal.Text = (Convert.ToDouble(txtTotContriFinal.Text) - Convert.ToDouble(txtTotGastoFinal.Text)) / Convert.ToDouble(txtTotIngresiFinal.Text)
        txtMgFinal.Text = FormatPercent(txtMgFinal.Text, 2)
        txtTotRebateFinal.Text = FormatNumber(txtTotRebateFinal.Text, 0)
        txtTotGastOpFinal.Text = FormatNumber(txtTotGastOpFinal.Text, 0)
        txtTotVentaFinal.Text = FormatNumber(txtTotVentaFinal.Text, 0)
        txtTotTransFinal.Text = FormatNumber(txtTotTransFinal.Text, 0)
    End Sub

    Private Sub exportar()

        Try
            Dim car As New frmCargando
            car.ProgressBar1.Minimum = 0
            car.ProgressBar1.Value = 0
            car.ProgressBar1.Maximum = dgvDetalleLineaProductos.RowCount * 2
            car.StartPosition = FormStartPosition.CenterScreen
            car.Show()
            Cursor.Current = Cursors.WaitCursor

            Dim xlApp As Excel.Application
            Dim xlWorkBook As Excel.Workbook = Nothing
            Dim xlWorkSheet As Excel.Worksheet

            'xlApp = New Excel.ApplicationClass
            xlApp = New Excel.Application
            'xlApp.Visible = True

            If IO.File.Exists("C:\Program Files\Convenios_licitaciones\base.xlsx") Then xlWorkBook = xlApp.Workbooks.Open("C:\Program Files\Convenios_licitaciones\base.xlsx")
            'xlWorkBook = xlApp.Workbooks.Open("C:\test1.xlsx")
            xlWorkSheet = xlWorkBook.Worksheets("Propuesta Original")

            xlApp.ScreenUpdating = False
            'edit the cell with new value
            xlWorkSheet.Cells(12, 4) = txtRazons.Text
            xlWorkSheet.Cells(13, 4) = txtRutcli.Text


            For i = 0 To dgvDetalleLineaProductos.RowCount - 1
                xlWorkSheet.Cells(i + 22, 2) = (i + 1).ToString
                xlWorkSheet.Cells(i + 22, 3) = dgvDetalleLineaProductos.Item("Codpro", i).Value.ToString
                xlWorkSheet.Cells(i + 22, 4) = dgvDetalleLineaProductos.Item("Despro", i).Value.ToString
                xlWorkSheet.Cells(i + 22, 5) = dgvDetalleLineaProductos.Item("Marca", i).Value.ToString
                car.ProgressBar1.Value += 1
            Next


            xlWorkSheet = xlWorkBook.Worksheets("Data Original")

            For i = 0 To dgvDetalleLineaProductos.RowCount - 1
                xlWorkSheet.Cells(i + 2, 1) = dgvDetalleLineaProductos.Item("Codpro", i).Value.ToString
                xlWorkSheet.Cells(i + 2, 2) = dgvDetalleLineaProductos.Item("Despro", i).Value.ToString
                xlWorkSheet.Cells(i + 2, 3) = dgvDetalleLineaProductos.Item("Mpropia", i).Value.ToString
                xlWorkSheet.Cells(i + 2, 4) = dgvDetalleLineaProductos.Item("Linea", i).Value.ToString
                xlWorkSheet.Cells(i + 2, 5) = dgvDetalleLineaProductos.Item("Marca", i).Value.ToString
                xlWorkSheet.Cells(i + 2, 6) = IIf(dgvDetalleLineaProductos.Item("Cantidad", i).Value > 0, dgvDetalleLineaProductos.Item("Cantidad", i).Value, 0)
                xlWorkSheet.Cells(i + 2, 7) = dgvDetalleLineaProductos.Item("Precio", i).Value.ToString
                xlWorkSheet.Cells(i + 2, 8) = dgvDetalleLineaProductos.Item("Costo Analisis", i).Value.ToString
                xlWorkSheet.Cells(i + 2, 9) = dgvDetalleLineaProductos.Item("Costo Promedio", i).Value.ToString
                xlWorkSheet.Cells(i + 2, 10) = dgvDetalleLineaProductos.Item("Costo Especial", i).Value.ToString
                xlWorkSheet.Cells(i + 2, 11) = dgvDetalleLineaProductos.Item("Costo Especial PM", i).Value.ToString
                xlWorkSheet.Cells(i + 2, 12) = dgvDetalleLineaProductos.Item("Aporte", i).Value / 100
                xlWorkSheet.Cells(i + 2, 13) = dgvDetalleLineaProductos.Item("Peso", i).Value / 100
                'xlWorkSheet.Cells(i + 2, 14) = dgvDetalleLineaProductos.Item("Mg. Comercial", i).Value / 100
                Dim oXLRange As Excel.Range
                oXLRange = CType(xlWorkSheet.Cells(i + 2, 14), Excel.Range)
                oXLRange.Formula = "=1-H" & i + 2 & "/G" & i + 2
                'xlWorkSheet.Cells(i + 2, 15) = dgvDetalleLineaProductos.Item("Mg. Inventario", i).Value / 100

                oXLRange = CType(xlWorkSheet.Cells(i + 2, 15), Excel.Range)
                oXLRange.Formula = "=SI(H" & i + 2 & "<I" & i + 2 & ";1-H" & i + 2 & "/G" & i + 2 & ";1-I" & i + 2 & "/G" & i + 2 & ")"

                'xlWorkSheet.Cells(i + 2, 16) = dgvDetalleLineaProductos.Item("Mg. Rebate", i).Value / 100
                oXLRange = CType(xlWorkSheet.Cells(i + 2, 16), Excel.Range)
                oXLRange.Formula = "=SI(H" & i + 2 & "<W" & i + 2 & ";1-H" & i + 2 & "/G" & i + 2 & ";1-W" & i + 2 & "/G" & i + 2 & ")"


                'xlWorkSheet.Cells(i + 2, 17) = dgvDetalleLineaProductos.Item("Valorizado Precio", i).Value.ToString
                oXLRange = CType(xlWorkSheet.Cells(i + 2, 17), Excel.Range)
                oXLRange.Formula = "=G" & i + 2 & "*F" & i + 2

                'xlWorkSheet.Cells(i + 2, 18) = dgvDetalleLineaProductos.Item("Valorizado Analisis", i).Value.ToString
                ''CALCULAR VALORIZADO ANALISIS
                oXLRange = CType(xlWorkSheet.Cells(i + 2, 18), Excel.Range)
                oXLRange.Formula = "=H" & i + 2 & "*F" & i + 2

                'CALCULAR VALORIZADO INVENTARIO
                'xlWorkSheet.Cells(i + 2, 19) = dgvDetalleLineaProductos.Item("Valorizado Inventario", i).Value.ToString
                oXLRange = CType(xlWorkSheet.Cells(i + 2, 19), Excel.Range)
                oXLRange.Formula = "=I" & i + 2 & "*F" & i + 2


                'deja con formula valorizado rebate
                'CALCULAR VALORIZADO REBATE
                'xlWorkSheet.Cells(i + 2, 20) = dgvDetalleLineaProductos.Item("Valorizado Rebate", i).Value.ToString
                oXLRange = CType(xlWorkSheet.Cells(i + 2, 20), Excel.Range)
                oXLRange.Formula = "=W" & i + 2 & "*F" & i + 2
                xlWorkSheet.Cells(i + 2, 22) = dgvDetalleLineaProductos.Item("Costo Comercial", i).Value.ToString
                'xlWorkSheet.Cells(i + 2, 23) = dgvDetalleLineaProductos.Item("Costo Rebate", i).Value.ToString
                oXLRange = CType(xlWorkSheet.Cells(i + 2, 23), Excel.Range)
                oXLRange.Formula = "=I" & i + 2 & "*(1-L" & i + 2 & ")"

                'xlWorkSheet.Cells(i + 2, 28) = dgvDetalleLineaProductos.Item("Contribucion Comercial", i).Value.ToString
                oXLRange = CType(xlWorkSheet.Cells(i + 2, 28), Excel.Range)
                oXLRange.Formula = "=(Q" & i + 2 & "-R" & i + 2 & ")"
                'xlWorkSheet.Cells(i + 2, 29) = dgvDetalleLineaProductos.Item("Contrib. Inventario", i).Value.ToString

                oXLRange = CType(xlWorkSheet.Cells(i + 2, 29), Excel.Range)
                oXLRange.Formula = "=SI(C" & i + 2 & "<>""IMPORTADO"";SI(R" & i + 2 & ">S" & i + 2 & ";(R" & i + 2 & "-S" & i + 2 & ");0);0)"
                'xlWorkSheet.Cells(i + 2, 30) = dgvDetalleLineaProductos.Item("Contrib. Rebate", i).Value.ToString

                oXLRange = CType(xlWorkSheet.Cells(i + 2, 30), Excel.Range)
                oXLRange.Formula = "=SI(C" & i + 2 & "<>""IMPORTADO"";SI(S" & i + 2 & ">T" & i + 2 & ";SI(R" & i + 2 & ">S" & i + 2 & ";(S" & i + 2 & "-T" & i + 2 & ");(R" & i + 2 & "-T" & i + 2 & "));0);0)"
                'xlWorkSheet.Cells(i + 2, 31) = dgvDetalleLineaProductos.Item("Contrib Rebate MP", i).Value.ToString

                oXLRange = CType(xlWorkSheet.Cells(i + 2, 31), Excel.Range)
                oXLRange.Formula = "=SI(C" & i + 2 & "=""IMPORTADO"";SI(R" & i + 2 & ">S" & i + 2 & ";(R" & i + 2 & "-S" & i + 2 & ");0);0)"
                xlWorkSheet.Cells(i + 2, 32) = dgvDetalleLineaProductos.Item("Precio Cotización", i).Value.ToString
                xlWorkSheet.Cells(i + 2, 39) = dgvDetalleLineaProductos.Item("Estado", i).Value.ToString
                xlWorkSheet.Cells(i + 2, 40) = dgvDetalleLineaProductos.Item("Pedsto", i).Value.ToString
                xlWorkSheet.Cells(i + 2, 41) = dgvDetalleLineaProductos.Item("UltCom", i).Value.ToString
                xlWorkSheet.Cells(i + 2, 42) = dgvDetalleLineaProductos.Item("Cosrep", i).Value.ToString
                xlWorkSheet.Cells(i + 2, 43) = dgvDetalleLineaProductos.Item("Variacion", i).Value / 100
                xlWorkSheet.Cells(i + 2, 44) = dgvDetalleLineaProductos.Item("FecIni Costo Especial", i).Value.ToString
                xlWorkSheet.Cells(i + 2, 45) = dgvDetalleLineaProductos.Item("FecFin Costo Especial", i).Value.ToString
                xlWorkSheet.Cells(i + 2, 46) = dgvDetalleLineaProductos.Item("Valorizado Anterior", i).Value.ToString
                car.ProgressBar1.Value += 1
            Next

            xlWorkSheet = xlWorkBook.Worksheets("Resumen Original")

            'edit the cell with new value
            'xlWorkSheet.Cells(6, 2) = txtRazons.Text
            'xlWorkSheet.Cells(7, 2) = txtRutcli.Text
            'xlWorkSheet.Cells(10, 3) = txtTotIngresiFinal.Text
            'xlWorkSheet.Cells(11, 2) = txtPorContriFinal.Text
            'xlWorkSheet.Cells(11, 3) = txtTotContriFinal.Text
            If Trim(txtPorTransFinal.Text) = "," Then
                xlWorkSheet.Cells(12, 2) = txtPorTransFinal.Text = "0,0"

            Else
                xlWorkSheet.Cells(12, 2) = txtPorTransFinal.Text / 100
            End If
            If Trim(txtPorVentaFinal.Text) = "," Then
                xlWorkSheet.Cells(13, 2) = "0,0"
            Else
                xlWorkSheet.Cells(13, 2) = txtPorVentaFinal.Text / 100
            End If
            If Trim(txtPorGastoOp.Text) = "," Then
                xlWorkSheet.Cells(14, 2) = "0,0"
            Else
                xlWorkSheet.Cells(14, 2) = txtPorGastoOp.Text / 100
            End If
            'xlWorkSheet.Cells(12, 3) = txtTotTransFinal.Text

            'xlWorkSheet.Cells(13, 3) = txtTotVentaFinal.Text
            If Trim(txtPorRebateFinal.Text) = "," Then
                xlWorkSheet.Cells(15, 2) = "0,0"
            Else
                xlWorkSheet.Cells(15, 2) = txtPorRebateFinal.Text / 100
            End If

            'xlWorkSheet.Cells(14, 3) = txtTotGastOpFinal.Text

            'xlWorkSheet.Cells(15, 3) = txtTotRebateFinal.Text
            'xlWorkSheet.Cells(16, 3) = txtTotGastoFinal.Text
            'xlWorkSheet.Cells(17, 3) = txtTotMargenFinal.Text


            xlWorkSheet.Activate()
            Dim pt As Excel.PivotTable
            pt = xlWorkSheet.PivotTables("Tabla dinámica3")
            pt.PivotCache.Refresh()

            pt = xlWorkSheet.PivotTables("Tabla dinámica1")
            pt.PivotCache.Refresh()



            xlApp.ScreenUpdating = True


            car.Close()
            xlApp.DisplayAlerts = False
            xlWorkBook.Close(SaveChanges:=True)

            xlWorkBook.SaveAs("C:\dimerc\propuesta " & txtRazons.Text & ".xlsx")
            xlWorkBook.Save()
            xlApp.DisplayAlerts = True
            xlApp.Quit()

            'xlWorkSheet.ReleaseComObject(pt)
            releaseObject(pt)
            releaseObject(xlApp)
            releaseObject(xlWorkBook)
            releaseObject(xlWorkSheet)

            MessageBox.Show("Listo")


            Cursor.Current = Cursors.Arrow
        Catch ex As Exception
            MessageBox.Show(ex.ToString())
            Exit Sub
        End Try


    End Sub

    Private Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub

    Private Sub cmbMarcaPropiaFinal_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbMarcaPropiaFinal.SelectedIndexChanged
        If flag = True Then
            Try
                bd.open_dimerc()
                Dim Sql = " select Linea, sum(""Valorizado Precio"") Valorizado,round((sum(""Valorizado Precio"")-sum(""Valorizado Analisis""))/ sum(""Valorizado Precio"")*100,2) ""MG. Lista"", "
                Sql = Sql & "round((sum(""Contribucion Comercial"")+ sum(""Contrib. Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comercial"", "
                Sql = Sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comer + Aporte MP"", "
                Sql = Sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Rebate""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Global"" "
                Sql = Sql & " from tmp_proceso_convenio "
                Sql = Sql & " where ""Valorizado Precio"">0 "
                If cmbMarcaPropia.SelectedIndex > 0 Then
                    Sql = Sql & " and mpropia='" + cmbMarcaPropia.SelectedValue.ToString + "' "
                End If
                Sql = Sql & "group by linea order by sum(""Valorizado Precio"") desc"
                Dim dtDatosMp As New DataTable
                dtDatosMp = bd.sqlSelect(Sql)


                Dim dtjuntar2 As New DataTable
                Sql = " select sum(""Valorizado Precio"") Valorizado,round((sum(""Valorizado Precio"")-sum(""Valorizado Analisis""))/ sum(""Valorizado Precio"")*100,2) ""MG. Lista"", "
                Sql = Sql & "round((sum(""Contribucion Comercial"")+ sum(""Contrib. Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comercial"", "
                Sql = Sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comer + Aporte MP"", "
                Sql = Sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Rebate""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Global"" "
                Sql = Sql & "from tmp_proceso_convenio "
                Sql = Sql & " where ""Valorizado Precio"">0 "
                If cmbMarcaPropia.SelectedIndex > 0 Then
                    Sql = Sql & " and  mpropia='" + cmbMarcaPropia.SelectedValue.ToString + "' "
                End If
                dtjuntar2 = bd.sqlSelect(Sql)
                dtDatosMp.Merge(dtjuntar2)
                dgvMpFinal.DataSource = dtDatosMp


                dgvMpFinal.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
                dgvMpFinal.Columns("Valorizado").DefaultCellStyle.Format = "n0"
                dgvMpFinal.Columns("Valorizado").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgvMpFinal.Columns("Mg. Lista").DefaultCellStyle.Format = "n2"
                dgvMpFinal.Columns("Mg. Lista").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgvMpFinal.Columns("Mg. Comercial").DefaultCellStyle.Format = "n2"
                dgvMpFinal.Columns("Mg. Comercial").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgvMpFinal.Columns("Mg. Comer + Aporte MP").DefaultCellStyle.Format = "n2"
                dgvMpFinal.Columns("Mg. Comer + Aporte MP").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgvMpFinal.Columns("Mg. Global").DefaultCellStyle.Format = "n2"
                dgvMpFinal.Columns("Mg. Global").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight




            Catch ex As Exception
                MessageBox.Show(ex.ToString, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Finally
                bd.close()
            End Try
        End If

    End Sub

    Private Sub btnExcel_Click_1(sender As Object, e As EventArgs) Handles btnExcel.Click
        exportar()
    End Sub

    Private Sub txtNumcotFinal_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtTotVentaFinal.KeyPress, txtTotTransFinal.KeyPress, txtTotRebateFinal.KeyPress, txtTotIngresiFinal.KeyPress, txtTotGastOpFinal.KeyPress, txtTotGastoFinal.KeyPress, txtTotContriFinal.KeyPress, txtRutCliFinal.KeyPress, txtRazonsFinal.KeyPress, txtPorContriFinal.KeyPress, txtNumcotFinal.KeyPress, txtMgFinal.KeyPress, txtValAct.KeyPress, txtValAnt.KeyPress, txtPorVariacion.KeyPress
        e.Handled = True
    End Sub
    Public Sub formatear()
        'transforma los enteror en numeros con 2 decimales
        txtPorTransporte.Text = Format(CDec(txtPorTransporte.Text), "N2")
        txtPorVenta.Text = Format(CDec(txtPorVenta.Text), "N2")
        txtPorGastoOp.Text = Format(CDec(txtPorGastoOp.Text), "N2")
        txtPorRebate.Text = Format(CDec(txtPorRebate.Text), "N2")
        txtVariacion.Text = Format(CDec(txtVariacion.Text), "N2")
        txtPorTransFinal.Text = Format(CDec(txtPorTransFinal.Text), "N2")
        txtPorVentaFinal.Text = Format(CDec(txtPorVentaFinal.Text), "N2")
        txtPorGastoOpFinal.Text = Format(CDec(txtPorGastoOpFinal.Text), "N2")
        txtPorRebateFinal.Text = Format(CDec(txtPorRebateFinal.Text), "N2")
    End Sub

End Class