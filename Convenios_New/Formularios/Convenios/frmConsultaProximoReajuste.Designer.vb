﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConsultaProximoReajuste
    Inherits MetroFramework.Forms.MetroForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label84 = New System.Windows.Forms.Label()
        Me.dtpFechaLineaReajuste = New System.Windows.Forms.DateTimePicker()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btn_Excel = New System.Windows.Forms.Button()
        Me.btn_Buscar = New System.Windows.Forms.Button()
        Me.btn_Salir = New System.Windows.Forms.Button()
        Me.dgvConsulta = New System.Windows.Forms.DataGridView()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvConsulta, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label84
        '
        Me.Label84.AutoSize = True
        Me.Label84.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label84.Location = New System.Drawing.Point(228, 125)
        Me.Label84.Name = "Label84"
        Me.Label84.Size = New System.Drawing.Size(122, 14)
        Me.Label84.TabIndex = 12
        Me.Label84.Text = "Fecha Prox. Reajuste"
        '
        'dtpFechaLineaReajuste
        '
        Me.dtpFechaLineaReajuste.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFechaLineaReajuste.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaLineaReajuste.Location = New System.Drawing.Point(356, 121)
        Me.dtpFechaLineaReajuste.Name = "dtpFechaLineaReajuste"
        Me.dtpFechaLineaReajuste.Size = New System.Drawing.Size(110, 22)
        Me.dtpFechaLineaReajuste.TabIndex = 11
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.btn_Excel)
        Me.Panel1.Controls.Add(Me.btn_Buscar)
        Me.Panel1.Controls.Add(Me.btn_Salir)
        Me.Panel1.Location = New System.Drawing.Point(13, 57)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(756, 52)
        Me.Panel1.TabIndex = 13
        '
        'btn_Excel
        '
        Me.btn_Excel.Image = Global.Convenios_New.My.Resources.Resources.Excel
        Me.btn_Excel.Location = New System.Drawing.Point(57, 4)
        Me.btn_Excel.Name = "btn_Excel"
        Me.btn_Excel.Size = New System.Drawing.Size(47, 42)
        Me.btn_Excel.TabIndex = 2
        Me.btn_Excel.UseVisualStyleBackColor = True
        '
        'btn_Buscar
        '
        Me.btn_Buscar.Image = Global.Convenios_New.My.Resources.Resources.search
        Me.btn_Buscar.Location = New System.Drawing.Point(4, 4)
        Me.btn_Buscar.Name = "btn_Buscar"
        Me.btn_Buscar.Size = New System.Drawing.Size(47, 42)
        Me.btn_Buscar.TabIndex = 2
        Me.btn_Buscar.UseVisualStyleBackColor = True
        '
        'btn_Salir
        '
        Me.btn_Salir.Image = Global.Convenios_New.My.Resources.Resources.salir
        Me.btn_Salir.Location = New System.Drawing.Point(710, 3)
        Me.btn_Salir.Name = "btn_Salir"
        Me.btn_Salir.Size = New System.Drawing.Size(41, 43)
        Me.btn_Salir.TabIndex = 1
        Me.btn_Salir.UseVisualStyleBackColor = True
        '
        'dgvConsulta
        '
        Me.dgvConsulta.AllowUserToAddRows = False
        Me.dgvConsulta.AllowUserToDeleteRows = False
        Me.dgvConsulta.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvConsulta.Location = New System.Drawing.Point(13, 156)
        Me.dgvConsulta.Name = "dgvConsulta"
        Me.dgvConsulta.ReadOnly = True
        Me.dgvConsulta.RowHeadersVisible = False
        Me.dgvConsulta.Size = New System.Drawing.Size(756, 248)
        Me.dgvConsulta.TabIndex = 14
        '
        'frmConsultaProximoReajuste
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(781, 413)
        Me.ControlBox = False
        Me.Controls.Add(Me.dgvConsulta)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label84)
        Me.Controls.Add(Me.dtpFechaLineaReajuste)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmConsultaProximoReajuste"
        Me.ShowIcon = False
        Me.Text = "Consulta de proximos reajustes"
        Me.Panel1.ResumeLayout(False)
        CType(Me.dgvConsulta, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label84 As Label
    Friend WithEvents dtpFechaLineaReajuste As DateTimePicker
    Friend WithEvents Panel1 As Panel
    Friend WithEvents btn_Salir As Button
    Friend WithEvents dgvConsulta As DataGridView
    Friend WithEvents btn_Buscar As Button
    Friend WithEvents btn_Excel As Button
End Class
