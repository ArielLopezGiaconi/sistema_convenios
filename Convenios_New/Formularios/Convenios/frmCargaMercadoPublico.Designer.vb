﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCargaMercadoPublico
    Inherits MetroFramework.Forms.MetroForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.dgvDatos = New System.Windows.Forms.DataGridView()
        Me.lblTotales = New MetroFramework.Controls.MetroLabel()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.cbxVisible = New System.Windows.Forms.CheckBox()
        Me.lblEstado = New MetroFramework.Controls.MetroLabel()
        Me.lblCuenta = New MetroFramework.Controls.MetroLabel()
        Me.MetroProgressSpinner1 = New MetroFramework.Controls.MetroProgressSpinner()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.btnCarga = New MetroFramework.Controls.MetroButton()
        Me.ProgressBar2 = New System.Windows.Forms.ProgressBar()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.MetroButton1 = New MetroFramework.Controls.MetroButton()
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.MetroButton2 = New MetroFramework.Controls.MetroButton()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.TabControl1.SuspendLayout()
        CType(Me.dgvDatos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        Me.SuspendLayout()
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Location = New System.Drawing.Point(6, 54)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(632, 496)
        Me.TabControl1.TabIndex = 14
        '
        'dgvDatos
        '
        Me.dgvDatos.AllowUserToAddRows = False
        Me.dgvDatos.AllowUserToDeleteRows = False
        Me.dgvDatos.AllowUserToResizeRows = False
        Me.dgvDatos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDatos.Location = New System.Drawing.Point(6, 224)
        Me.dgvDatos.Name = "dgvDatos"
        Me.dgvDatos.ReadOnly = True
        Me.dgvDatos.RowHeadersVisible = False
        Me.dgvDatos.Size = New System.Drawing.Size(566, 217)
        Me.dgvDatos.TabIndex = 9
        '
        'lblTotales
        '
        Me.lblTotales.AutoSize = True
        Me.lblTotales.Location = New System.Drawing.Point(434, 444)
        Me.lblTotales.Name = "lblTotales"
        Me.lblTotales.Size = New System.Drawing.Size(43, 19)
        Me.lblTotales.TabIndex = 12
        Me.lblTotales.Text = "Total: "
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.dgvDatos)
        Me.TabPage2.Controls.Add(Me.lblTotales)
        Me.TabPage2.Controls.Add(Me.cbxVisible)
        Me.TabPage2.Controls.Add(Me.lblEstado)
        Me.TabPage2.Controls.Add(Me.lblCuenta)
        Me.TabPage2.Controls.Add(Me.MetroProgressSpinner1)
        Me.TabPage2.Controls.Add(Me.Label2)
        Me.TabPage2.Controls.Add(Me.Label1)
        Me.TabPage2.Controls.Add(Me.MetroLabel1)
        Me.TabPage2.Controls.Add(Me.btnCarga)
        Me.TabPage2.Controls.Add(Me.ProgressBar2)
        Me.TabPage2.Controls.Add(Me.ProgressBar1)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(624, 470)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Procesa Cambios"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'cbxVisible
        '
        Me.cbxVisible.AutoSize = True
        Me.cbxVisible.Checked = True
        Me.cbxVisible.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbxVisible.Location = New System.Drawing.Point(542, 82)
        Me.cbxVisible.Name = "cbxVisible"
        Me.cbxVisible.Size = New System.Drawing.Size(82, 17)
        Me.cbxVisible.TabIndex = 11
        Me.cbxVisible.Text = "Ver Avance"
        Me.cbxVisible.UseVisualStyleBackColor = True
        '
        'lblEstado
        '
        Me.lblEstado.Location = New System.Drawing.Point(6, 172)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(550, 23)
        Me.lblEstado.TabIndex = 10
        '
        'lblCuenta
        '
        Me.lblCuenta.Location = New System.Drawing.Point(9, 198)
        Me.lblCuenta.Name = "lblCuenta"
        Me.lblCuenta.Size = New System.Drawing.Size(156, 23)
        Me.lblCuenta.TabIndex = 9
        '
        'MetroProgressSpinner1
        '
        Me.MetroProgressSpinner1.Location = New System.Drawing.Point(562, 176)
        Me.MetroProgressSpinner1.Maximum = 100
        Me.MetroProgressSpinner1.Name = "MetroProgressSpinner1"
        Me.MetroProgressSpinner1.Size = New System.Drawing.Size(56, 51)
        Me.MetroProgressSpinner1.TabIndex = 8
        Me.MetroProgressSpinner1.UseSelectable = True
        Me.MetroProgressSpinner1.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 130)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(55, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Productos"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 89)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(63, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Licitaciones"
        '
        'MetroLabel1
        '
        Me.MetroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.MetroLabel1.Location = New System.Drawing.Point(124, 17)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(497, 65)
        Me.MetroLabel1.TabIndex = 3
        Me.MetroLabel1.Text = "Es muy importante que el archivo Cargado tenga las siguientes columnas:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "IDLINEA " &
    "- IDPRODUCTO - STOCK - PRECIO" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Favor respetar el orden."
        '
        'btnCarga
        '
        Me.btnCarga.Location = New System.Drawing.Point(14, 17)
        Me.btnCarga.Name = "btnCarga"
        Me.btnCarga.Size = New System.Drawing.Size(104, 65)
        Me.btnCarga.TabIndex = 2
        Me.btnCarga.Text = "Procesa Excel"
        Me.btnCarga.UseSelectable = True
        '
        'ProgressBar2
        '
        Me.ProgressBar2.Location = New System.Drawing.Point(6, 146)
        Me.ProgressBar2.Name = "ProgressBar2"
        Me.ProgressBar2.Size = New System.Drawing.Size(612, 23)
        Me.ProgressBar2.TabIndex = 1
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(6, 105)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(612, 23)
        Me.ProgressBar1.TabIndex = 0
        '
        'MetroButton1
        '
        Me.MetroButton1.Location = New System.Drawing.Point(471, 36)
        Me.MetroButton1.Name = "MetroButton1"
        Me.MetroButton1.Size = New System.Drawing.Size(99, 23)
        Me.MetroButton1.TabIndex = 15
        Me.MetroButton1.Text = "Carga Excel"
        Me.MetroButton1.UseSelectable = True
        '
        'BackgroundWorker1
        '
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'MetroButton2
        '
        Me.MetroButton2.Location = New System.Drawing.Point(351, 36)
        Me.MetroButton2.Name = "MetroButton2"
        Me.MetroButton2.Size = New System.Drawing.Size(114, 23)
        Me.MetroButton2.TabIndex = 16
        Me.MetroButton2.Text = "Descarga catalogo"
        Me.MetroButton2.UseSelectable = True
        '
        'frmCargaMercadoPublico
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(642, 559)
        Me.Controls.Add(Me.MetroButton2)
        Me.Controls.Add(Me.MetroButton1)
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "frmCargaMercadoPublico"
        Me.Text = "Actualización Stock y Precio"
        Me.TabControl1.ResumeLayout(False)
        CType(Me.dgvDatos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents dgvDatos As DataGridView
    Friend WithEvents lblTotales As MetroFramework.Controls.MetroLabel
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents btnCarga As MetroFramework.Controls.MetroButton
    Friend WithEvents ProgressBar2 As ProgressBar
    Friend WithEvents ProgressBar1 As ProgressBar
    Friend WithEvents MetroButton1 As MetroFramework.Controls.MetroButton
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents Timer1 As Timer
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents MetroProgressSpinner1 As MetroFramework.Controls.MetroProgressSpinner
    Friend WithEvents lblCuenta As MetroFramework.Controls.MetroLabel
    Friend WithEvents lblEstado As MetroFramework.Controls.MetroLabel
    Friend WithEvents cbxVisible As CheckBox
    Friend WithEvents MetroButton2 As MetroFramework.Controls.MetroButton
    Friend WithEvents SaveFileDialog1 As SaveFileDialog
End Class
