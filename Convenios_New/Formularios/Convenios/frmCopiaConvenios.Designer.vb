﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCopiaConvenios
    Inherits MetroFramework.Forms.MetroForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.lblDesCco = New System.Windows.Forms.TextBox()
        Me.txtCencos = New System.Windows.Forms.TextBox()
        Me.lblRazons = New System.Windows.Forms.TextBox()
        Me.txtRutCli = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblItems = New System.Windows.Forms.TextBox()
        Me.lblTotNet = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtNumCot = New System.Windows.Forms.TextBox()
        Me.MetroTabControl1 = New MetroFramework.Controls.MetroTabControl()
        Me.MetroTabPage1 = New MetroFramework.Controls.MetroTabPage()
        Me.MetroTabPage2 = New MetroFramework.Controls.MetroTabPage()
        Me.MetroLabel5 = New MetroFramework.Controls.MetroLabel()
        Me.txtRut = New MetroFramework.Controls.MetroTextBox()
        Me.dtpFechaVencimiento = New MetroFramework.Controls.MetroDateTime()
        Me.MetroLabel4 = New MetroFramework.Controls.MetroLabel()
        Me.MetroButton1 = New MetroFramework.Controls.MetroButton()
        Me.txtClave = New System.Windows.Forms.TextBox()
        Me.dtpFechaModificar = New MetroFramework.Controls.MetroDateTime()
        Me.MetroLabel3 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.dtpFechaInicio = New MetroFramework.Controls.MetroDateTime()
        Me.txtCoti = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.GroupBox1.SuspendLayout()
        Me.MetroTabControl1.SuspendLayout()
        Me.MetroTabPage1.SuspendLayout()
        Me.MetroTabPage2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(21, 140)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(47, 13)
        Me.Label5.TabIndex = 15
        Me.Label5.Text = "C. Costo"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(18, 97)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(24, 13)
        Me.Label4.TabIndex = 14
        Me.Label4.Text = "Rut"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(243, 164)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 13
        Me.Button1.Text = "Copiar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'lblDesCco
        '
        Me.lblDesCco.Location = New System.Drawing.Point(203, 134)
        Me.lblDesCco.Name = "lblDesCco"
        Me.lblDesCco.Size = New System.Drawing.Size(290, 20)
        Me.lblDesCco.TabIndex = 12
        '
        'txtCencos
        '
        Me.txtCencos.Location = New System.Drawing.Point(84, 134)
        Me.txtCencos.Name = "txtCencos"
        Me.txtCencos.Size = New System.Drawing.Size(100, 20)
        Me.txtCencos.TabIndex = 11
        '
        'lblRazons
        '
        Me.lblRazons.Location = New System.Drawing.Point(203, 97)
        Me.lblRazons.Name = "lblRazons"
        Me.lblRazons.Size = New System.Drawing.Size(290, 20)
        Me.lblRazons.TabIndex = 10
        '
        'txtRutCli
        '
        Me.txtRutCli.Location = New System.Drawing.Point(84, 97)
        Me.txtRutCli.Name = "txtRutCli"
        Me.txtRutCli.Size = New System.Drawing.Size(100, 20)
        Me.txtRutCli.TabIndex = 9
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblItems)
        Me.GroupBox1.Controls.Add(Me.lblTotNet)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtNumCot)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 26)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(520, 47)
        Me.GroupBox1.TabIndex = 8
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Cotizacion"
        '
        'lblItems
        '
        Me.lblItems.Location = New System.Drawing.Point(399, 16)
        Me.lblItems.Name = "lblItems"
        Me.lblItems.Size = New System.Drawing.Size(100, 20)
        Me.lblItems.TabIndex = 5
        '
        'lblTotNet
        '
        Me.lblTotNet.Location = New System.Drawing.Point(255, 16)
        Me.lblTotNet.Name = "lblTotNet"
        Me.lblTotNet.Size = New System.Drawing.Size(100, 20)
        Me.lblTotNet.TabIndex = 4
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(361, 20)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(32, 13)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Items"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(191, 20)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(58, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Monto Coti"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(65, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Numero Coti"
        '
        'txtNumCot
        '
        Me.txtNumCot.Location = New System.Drawing.Point(75, 16)
        Me.txtNumCot.Name = "txtNumCot"
        Me.txtNumCot.Size = New System.Drawing.Size(100, 20)
        Me.txtNumCot.TabIndex = 0
        '
        'MetroTabControl1
        '
        Me.MetroTabControl1.Controls.Add(Me.MetroTabPage1)
        Me.MetroTabControl1.Controls.Add(Me.MetroTabPage2)
        Me.MetroTabControl1.Location = New System.Drawing.Point(3, 59)
        Me.MetroTabControl1.Name = "MetroTabControl1"
        Me.MetroTabControl1.SelectedIndex = 0
        Me.MetroTabControl1.Size = New System.Drawing.Size(645, 267)
        Me.MetroTabControl1.TabIndex = 16
        Me.MetroTabControl1.UseSelectable = True
        '
        'MetroTabPage1
        '
        Me.MetroTabPage1.Controls.Add(Me.Label5)
        Me.MetroTabPage1.Controls.Add(Me.Label4)
        Me.MetroTabPage1.Controls.Add(Me.GroupBox1)
        Me.MetroTabPage1.Controls.Add(Me.Button1)
        Me.MetroTabPage1.Controls.Add(Me.txtRutCli)
        Me.MetroTabPage1.Controls.Add(Me.lblDesCco)
        Me.MetroTabPage1.Controls.Add(Me.lblRazons)
        Me.MetroTabPage1.Controls.Add(Me.txtCencos)
        Me.MetroTabPage1.HorizontalScrollbarBarColor = True
        Me.MetroTabPage1.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroTabPage1.HorizontalScrollbarSize = 10
        Me.MetroTabPage1.Location = New System.Drawing.Point(4, 38)
        Me.MetroTabPage1.Name = "MetroTabPage1"
        Me.MetroTabPage1.Size = New System.Drawing.Size(637, 225)
        Me.MetroTabPage1.TabIndex = 0
        Me.MetroTabPage1.Text = "Copia Convenio"
        Me.MetroTabPage1.VerticalScrollbarBarColor = True
        Me.MetroTabPage1.VerticalScrollbarHighlightOnWheel = False
        Me.MetroTabPage1.VerticalScrollbarSize = 10
        '
        'MetroTabPage2
        '
        Me.MetroTabPage2.Controls.Add(Me.MetroLabel5)
        Me.MetroTabPage2.Controls.Add(Me.txtRut)
        Me.MetroTabPage2.Controls.Add(Me.dtpFechaVencimiento)
        Me.MetroTabPage2.Controls.Add(Me.MetroLabel4)
        Me.MetroTabPage2.Controls.Add(Me.MetroButton1)
        Me.MetroTabPage2.Controls.Add(Me.txtClave)
        Me.MetroTabPage2.Controls.Add(Me.dtpFechaModificar)
        Me.MetroTabPage2.Controls.Add(Me.MetroLabel3)
        Me.MetroTabPage2.Controls.Add(Me.MetroLabel2)
        Me.MetroTabPage2.Controls.Add(Me.dtpFechaInicio)
        Me.MetroTabPage2.Controls.Add(Me.txtCoti)
        Me.MetroTabPage2.Controls.Add(Me.MetroLabel1)
        Me.MetroTabPage2.HorizontalScrollbarBarColor = True
        Me.MetroTabPage2.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroTabPage2.HorizontalScrollbarSize = 10
        Me.MetroTabPage2.Location = New System.Drawing.Point(4, 38)
        Me.MetroTabPage2.Name = "MetroTabPage2"
        Me.MetroTabPage2.Size = New System.Drawing.Size(637, 225)
        Me.MetroTabPage2.TabIndex = 1
        Me.MetroTabPage2.Text = "Extender Cotizacion"
        Me.MetroTabPage2.VerticalScrollbarBarColor = True
        Me.MetroTabPage2.VerticalScrollbarHighlightOnWheel = False
        Me.MetroTabPage2.VerticalScrollbarSize = 10
        '
        'MetroLabel5
        '
        Me.MetroLabel5.AutoSize = True
        Me.MetroLabel5.Location = New System.Drawing.Point(328, 17)
        Me.MetroLabel5.Name = "MetroLabel5"
        Me.MetroLabel5.Size = New System.Drawing.Size(31, 19)
        Me.MetroLabel5.TabIndex = 13
        Me.MetroLabel5.Text = "Rut:"
        '
        'txtRut
        '
        '
        '
        '
        Me.txtRut.CustomButton.Image = Nothing
        Me.txtRut.CustomButton.Location = New System.Drawing.Point(92, 1)
        Me.txtRut.CustomButton.Name = ""
        Me.txtRut.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.txtRut.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txtRut.CustomButton.TabIndex = 1
        Me.txtRut.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txtRut.CustomButton.UseSelectable = True
        Me.txtRut.CustomButton.Visible = False
        Me.txtRut.Lines = New String(-1) {}
        Me.txtRut.Location = New System.Drawing.Point(365, 13)
        Me.txtRut.MaxLength = 32767
        Me.txtRut.Name = "txtRut"
        Me.txtRut.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtRut.ReadOnly = True
        Me.txtRut.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtRut.SelectedText = ""
        Me.txtRut.SelectionLength = 0
        Me.txtRut.SelectionStart = 0
        Me.txtRut.ShortcutsEnabled = True
        Me.txtRut.Size = New System.Drawing.Size(114, 23)
        Me.txtRut.TabIndex = 12
        Me.txtRut.UseSelectable = True
        Me.txtRut.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txtRut.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'dtpFechaVencimiento
        '
        Me.dtpFechaVencimiento.Location = New System.Drawing.Point(135, 91)
        Me.dtpFechaVencimiento.MinimumSize = New System.Drawing.Size(0, 29)
        Me.dtpFechaVencimiento.Name = "dtpFechaVencimiento"
        Me.dtpFechaVencimiento.Size = New System.Drawing.Size(257, 29)
        Me.dtpFechaVencimiento.TabIndex = 11
        Me.dtpFechaVencimiento.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'MetroLabel4
        '
        Me.MetroLabel4.AutoSize = True
        Me.MetroLabel4.Location = New System.Drawing.Point(16, 101)
        Me.MetroLabel4.Name = "MetroLabel4"
        Me.MetroLabel4.Size = New System.Drawing.Size(121, 19)
        Me.MetroLabel4.TabIndex = 10
        Me.MetroLabel4.Text = "Fecha Vencimiento:"
        '
        'MetroButton1
        '
        Me.MetroButton1.Location = New System.Drawing.Point(505, 172)
        Me.MetroButton1.Name = "MetroButton1"
        Me.MetroButton1.Size = New System.Drawing.Size(132, 23)
        Me.MetroButton1.TabIndex = 9
        Me.MetroButton1.Text = "Extender Convenio"
        Me.MetroButton1.UseSelectable = True
        '
        'txtClave
        '
        Me.txtClave.Location = New System.Drawing.Point(534, 13)
        Me.txtClave.Name = "txtClave"
        Me.txtClave.Size = New System.Drawing.Size(100, 20)
        Me.txtClave.TabIndex = 8
        Me.txtClave.Visible = False
        '
        'dtpFechaModificar
        '
        Me.dtpFechaModificar.FontSize = MetroFramework.MetroDateTimeSize.Tall
        Me.dtpFechaModificar.Location = New System.Drawing.Point(177, 160)
        Me.dtpFechaModificar.MinimumSize = New System.Drawing.Size(0, 35)
        Me.dtpFechaModificar.Name = "dtpFechaModificar"
        Me.dtpFechaModificar.Size = New System.Drawing.Size(302, 35)
        Me.dtpFechaModificar.TabIndex = 7
        '
        'MetroLabel3
        '
        Me.MetroLabel3.AutoSize = True
        Me.MetroLabel3.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel3.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.MetroLabel3.Location = New System.Drawing.Point(16, 170)
        Me.MetroLabel3.Name = "MetroLabel3"
        Me.MetroLabel3.Size = New System.Drawing.Size(155, 25)
        Me.MetroLabel3.TabIndex = 6
        Me.MetroLabel3.Text = "Fecha Modificada:"
        '
        'MetroLabel2
        '
        Me.MetroLabel2.AutoSize = True
        Me.MetroLabel2.Location = New System.Drawing.Point(16, 66)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(80, 19)
        Me.MetroLabel2.TabIndex = 5
        Me.MetroLabel2.Text = "Fecha inicio:"
        '
        'dtpFechaInicio
        '
        Me.dtpFechaInicio.Location = New System.Drawing.Point(135, 56)
        Me.dtpFechaInicio.MinimumSize = New System.Drawing.Size(0, 29)
        Me.dtpFechaInicio.Name = "dtpFechaInicio"
        Me.dtpFechaInicio.Size = New System.Drawing.Size(257, 29)
        Me.dtpFechaInicio.TabIndex = 4
        Me.dtpFechaInicio.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'txtCoti
        '
        '
        '
        '
        Me.txtCoti.CustomButton.Image = Nothing
        Me.txtCoti.CustomButton.Location = New System.Drawing.Point(165, 1)
        Me.txtCoti.CustomButton.Name = ""
        Me.txtCoti.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.txtCoti.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txtCoti.CustomButton.TabIndex = 1
        Me.txtCoti.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txtCoti.CustomButton.UseSelectable = True
        Me.txtCoti.CustomButton.Visible = False
        Me.txtCoti.Lines = New String(-1) {}
        Me.txtCoti.Location = New System.Drawing.Point(135, 13)
        Me.txtCoti.MaxLength = 32767
        Me.txtCoti.Name = "txtCoti"
        Me.txtCoti.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtCoti.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtCoti.SelectedText = ""
        Me.txtCoti.SelectionLength = 0
        Me.txtCoti.SelectionStart = 0
        Me.txtCoti.ShortcutsEnabled = True
        Me.txtCoti.Size = New System.Drawing.Size(187, 23)
        Me.txtCoti.TabIndex = 3
        Me.txtCoti.UseSelectable = True
        Me.txtCoti.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txtCoti.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.Location = New System.Drawing.Point(16, 17)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(109, 19)
        Me.MetroLabel1.TabIndex = 2
        Me.MetroLabel1.Text = "Num. Cotizacion:"
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(312, 30)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(332, 23)
        Me.ProgressBar1.TabIndex = 17
        Me.ProgressBar1.Visible = False
        '
        'frmCopiaConvenios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(659, 349)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.MetroTabControl1)
        Me.Name = "frmCopiaConvenios"
        Me.Text = "Convenios y Cotizaciones"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.MetroTabControl1.ResumeLayout(False)
        Me.MetroTabPage1.ResumeLayout(False)
        Me.MetroTabPage1.PerformLayout()
        Me.MetroTabPage2.ResumeLayout(False)
        Me.MetroTabPage2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Button1 As Button
    Friend WithEvents lblDesCco As TextBox
    Friend WithEvents txtCencos As TextBox
    Friend WithEvents lblRazons As TextBox
    Friend WithEvents txtRutCli As TextBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents lblItems As TextBox
    Friend WithEvents lblTotNet As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents txtNumCot As TextBox
    Friend WithEvents MetroTabControl1 As MetroFramework.Controls.MetroTabControl
    Friend WithEvents MetroTabPage1 As MetroFramework.Controls.MetroTabPage
    Friend WithEvents MetroTabPage2 As MetroFramework.Controls.MetroTabPage
    Friend WithEvents MetroButton1 As MetroFramework.Controls.MetroButton
    Friend WithEvents txtClave As TextBox
    Friend WithEvents dtpFechaModificar As MetroFramework.Controls.MetroDateTime
    Friend WithEvents MetroLabel3 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents dtpFechaInicio As MetroFramework.Controls.MetroDateTime
    Friend WithEvents txtCoti As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel5 As MetroFramework.Controls.MetroLabel
    Friend WithEvents txtRut As MetroFramework.Controls.MetroTextBox
    Friend WithEvents dtpFechaVencimiento As MetroFramework.Controls.MetroDateTime
    Friend WithEvents MetroLabel4 As MetroFramework.Controls.MetroLabel
    Friend WithEvents ProgressBar1 As ProgressBar
End Class
