﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConvenioProveedor
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnEnviar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblRutCli = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblNumRel = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblRazons = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblNumCot = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lblRegionDespacho = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.lblVigencia = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cmbDispensadores = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cmbAlternativas = New System.Windows.Forms.ComboBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtObserv = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.dgvProductos = New System.Windows.Forms.DataGridView()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txtMail = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtCC = New System.Windows.Forms.TextBox()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvProductos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(352, 308)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 0
        Me.btnGuardar.Text = "Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnEnviar
        '
        Me.btnEnviar.Location = New System.Drawing.Point(644, 325)
        Me.btnEnviar.Name = "btnEnviar"
        Me.btnEnviar.Size = New System.Drawing.Size(114, 27)
        Me.btnEnviar.TabIndex = 1
        Me.btnEnviar.Text = "Enviar Por Mail"
        Me.btnEnviar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(24, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Rut"
        '
        'lblRutCli
        '
        Me.lblRutCli.AutoSize = True
        Me.lblRutCli.Location = New System.Drawing.Point(144, 19)
        Me.lblRutCli.Name = "lblRutCli"
        Me.lblRutCli.Size = New System.Drawing.Size(39, 13)
        Me.lblRutCli.TabIndex = 3
        Me.lblRutCli.Text = "Label2"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 41)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(74, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Núm Relación"
        '
        'lblNumRel
        '
        Me.lblNumRel.AutoSize = True
        Me.lblNumRel.Location = New System.Drawing.Point(144, 41)
        Me.lblNumRel.Name = "lblNumRel"
        Me.lblNumRel.Size = New System.Drawing.Size(39, 13)
        Me.lblNumRel.TabIndex = 5
        Me.lblNumRel.Text = "Label3"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 68)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(70, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Razón Social"
        '
        'lblRazons
        '
        Me.lblRazons.AutoSize = True
        Me.lblRazons.Location = New System.Drawing.Point(144, 68)
        Me.lblRazons.Name = "lblRazons"
        Me.lblRazons.Size = New System.Drawing.Size(39, 13)
        Me.lblRazons.TabIndex = 7
        Me.lblRazons.Text = "Label4"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(12, 94)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(104, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Cotización Convenio"
        '
        'lblNumCot
        '
        Me.lblNumCot.AutoSize = True
        Me.lblNumCot.Location = New System.Drawing.Point(144, 94)
        Me.lblNumCot.Name = "lblNumCot"
        Me.lblNumCot.Size = New System.Drawing.Size(39, 13)
        Me.lblNumCot.TabIndex = 9
        Me.lblNumCot.Text = "Label5"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(12, 124)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(93, 13)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "Región Despacho"
        '
        'lblRegionDespacho
        '
        Me.lblRegionDespacho.AutoSize = True
        Me.lblRegionDespacho.Location = New System.Drawing.Point(144, 124)
        Me.lblRegionDespacho.Name = "lblRegionDespacho"
        Me.lblRegionDespacho.Size = New System.Drawing.Size(39, 13)
        Me.lblRegionDespacho.TabIndex = 11
        Me.lblRegionDespacho.Text = "Label6"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(12, 150)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(81, 26)
        Me.Label6.TabIndex = 12
        Me.Label6.Text = "Vigencia Precio" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "(Meses)"
        '
        'lblVigencia
        '
        Me.lblVigencia.AutoSize = True
        Me.lblVigencia.Location = New System.Drawing.Point(144, 150)
        Me.lblVigencia.Name = "lblVigencia"
        Me.lblVigencia.Size = New System.Drawing.Size(39, 13)
        Me.lblVigencia.TabIndex = 13
        Me.lblVigencia.Text = "Label7"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(9, 225)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(123, 13)
        Me.Label7.TabIndex = 14
        Me.Label7.Text = "Requiere Dispensadores"
        '
        'cmbDispensadores
        '
        Me.cmbDispensadores.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbDispensadores.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmbDispensadores.FormattingEnabled = True
        Me.cmbDispensadores.Items.AddRange(New Object() {"Sí", "No"})
        Me.cmbDispensadores.Location = New System.Drawing.Point(147, 222)
        Me.cmbDispensadores.Name = "cmbDispensadores"
        Me.cmbDispensadores.Size = New System.Drawing.Size(121, 21)
        Me.cmbDispensadores.TabIndex = 15
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(12, 189)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(99, 13)
        Me.Label8.TabIndex = 16
        Me.Label8.Text = "Acepta Alternativas"
        '
        'cmbAlternativas
        '
        Me.cmbAlternativas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbAlternativas.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmbAlternativas.FormattingEnabled = True
        Me.cmbAlternativas.Items.AddRange(New Object() {"Sí", "No"})
        Me.cmbAlternativas.Location = New System.Drawing.Point(147, 186)
        Me.cmbAlternativas.Name = "cmbAlternativas"
        Me.cmbAlternativas.Size = New System.Drawing.Size(121, 21)
        Me.cmbAlternativas.TabIndex = 17
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtObserv)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.cmbAlternativas)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.cmbDispensadores)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.lblVigencia)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.lblRegionDespacho)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.lblNumCot)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.lblRazons)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.lblNumRel)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.lblRutCli)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(18, 5)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(346, 290)
        Me.GroupBox1.TabIndex = 18
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos Cliente"
        '
        'txtObserv
        '
        Me.txtObserv.Location = New System.Drawing.Point(147, 250)
        Me.txtObserv.Multiline = True
        Me.txtObserv.Name = "txtObserv"
        Me.txtObserv.Size = New System.Drawing.Size(193, 34)
        Me.txtObserv.TabIndex = 19
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(12, 257)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(67, 13)
        Me.Label10.TabIndex = 18
        Me.Label10.Text = "Observación"
        '
        'dgvProductos
        '
        Me.dgvProductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvProductos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column2, Me.Column3, Me.Column4, Me.Column5, Me.Column6, Me.Column7, Me.Column8})
        Me.dgvProductos.Location = New System.Drawing.Point(389, 12)
        Me.dgvProductos.Name = "dgvProductos"
        Me.dgvProductos.Size = New System.Drawing.Size(711, 283)
        Me.dgvProductos.TabIndex = 19
        '
        'Column1
        '
        Me.Column1.HeaderText = "Codigo Dimerc"
        Me.Column1.Name = "Column1"
        '
        'Column2
        '
        Me.Column2.HeaderText = "Codigo Proveedor"
        Me.Column2.Name = "Column2"
        '
        'Column3
        '
        Me.Column3.HeaderText = "Descripción Dimerc"
        Me.Column3.Name = "Column3"
        '
        'Column4
        '
        Me.Column4.HeaderText = "Marca"
        Me.Column4.Name = "Column4"
        '
        'Column5
        '
        Me.Column5.HeaderText = "Consumo Convenio"
        Me.Column5.Name = "Column5"
        '
        'Column6
        '
        Me.Column6.HeaderText = "Consumo 6 meses"
        Me.Column6.Name = "Column6"
        '
        'Column7
        '
        Me.Column7.HeaderText = "Costo Especial Vigente"
        Me.Column7.Name = "Column7"
        '
        'Column8
        '
        Me.Column8.HeaderText = "Fecha Término"
        Me.Column8.Name = "Column8"
        '
        'txtMail
        '
        Me.txtMail.Location = New System.Drawing.Point(538, 312)
        Me.txtMail.Name = "txtMail"
        Me.txtMail.Size = New System.Drawing.Size(100, 20)
        Me.txtMail.TabIndex = 20
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(506, 315)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(26, 13)
        Me.Label9.TabIndex = 21
        Me.Label9.Text = "Mail"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(509, 348)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(21, 13)
        Me.Label11.TabIndex = 22
        Me.Label11.Text = "CC"
        '
        'txtCC
        '
        Me.txtCC.Location = New System.Drawing.Point(537, 348)
        Me.txtCC.Name = "txtCC"
        Me.txtCC.Size = New System.Drawing.Size(100, 20)
        Me.txtCC.TabIndex = 23
        '
        'frmConvenioProveedor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1119, 373)
        Me.Controls.Add(Me.txtCC)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txtMail)
        Me.Controls.Add(Me.dgvProductos)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnEnviar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Name = "frmConvenioProveedor"
        Me.Text = "Envío Convenio Proveedor"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgvProductos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnGuardar As Button
    Friend WithEvents btnEnviar As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents lblRutCli As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents lblNumRel As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents lblRazons As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents lblNumCot As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents lblRegionDespacho As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents lblVigencia As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents cmbDispensadores As ComboBox
    Friend WithEvents Label8 As Label
    Friend WithEvents cmbAlternativas As ComboBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents dgvProductos As DataGridView
    Friend WithEvents txtMail As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents txtObserv As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents Column1 As DataGridViewTextBoxColumn
    Friend WithEvents Column2 As DataGridViewTextBoxColumn
    Friend WithEvents Column3 As DataGridViewTextBoxColumn
    Friend WithEvents Column4 As DataGridViewTextBoxColumn
    Friend WithEvents Column5 As DataGridViewTextBoxColumn
    Friend WithEvents Column6 As DataGridViewTextBoxColumn
    Friend WithEvents Column7 As DataGridViewTextBoxColumn
    Friend WithEvents Column8 As DataGridViewTextBoxColumn
    Friend WithEvents Label11 As Label
    Friend WithEvents txtCC As TextBox
End Class
