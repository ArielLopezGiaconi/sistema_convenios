﻿Imports System.Data.OracleClient
Imports System.Net.Mail
Imports System.Runtime.InteropServices
Imports Microsoft.Office.Interop
Public Class frmApruebaConvenioCorreo
    Dim bd As New Conexion
    Dim flaggrilla, flagemprel As Boolean
    Dim reajuste As String
    Dim numcot As String
    Dim cliente As String
    Dim vendedor As String
    Dim indice As Integer
    Dim cambioConBoton As Boolean
    Dim cencos As String
    Dim PuntoControl As New PuntoControl
    Private mExcelProcesses() As Process
    Private Sub frmConsultaReajusteCliente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If Globales.tema = 1 Then
            Me.Theme = 2
            Globales.tema = 2
        Else
            Me.Theme = 1
            Globales.tema = 1
        End If
        Me.StartPosition = FormStartPosition.Manual
        Me.Location = New Point(0, 0)
        cmbTipoConvenio.SelectedIndex = 0

        cargacombo()
        PuntoControl.RegistroUso("2")
    End Sub
    Private Sub cargacombo()
        Try
            bd.open_dimerc()
            Dim sql = "select '<<<TODOS>>>' usr_creac from dual "
            sql += " union select distinct usr_creac from en_reajuste_convenio"
            Dim dt = bd.sqlSelect(sql)

            cmbUsuario.DataSource = dt
            cmbUsuario.SelectedIndex = -1
            cmbUsuario.ValueMember = "usr_creac"
            cmbUsuario.DisplayMember = "usr_creac"
            cmbUsuario.Refresh()


        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub
    Private Sub carganegociosrelacionados()
        Try
            bd.open_dimerc()
            Dim SQL = " select rutcli,getrazonsocial(codemp,rutcli) razons,decode(estado,1,'FINANCIERO',2,'COMERCIAL') RELACION "
            SQL = SQL & "FROM re_emprela  "
            SQL = SQL & "where numrel in(select numrel from re_emprela where rutcli=" + cliente + " and codemp=3)  "
            SQL = SQL & "and rutcli not in(select rutcli from re_emprela where rutcli=" + cliente + " and codemp=3)order by rutcli "

            dgvRelComercial.DataSource = bd.sqlSelect(SQL)

            If flagemprel = False Then
                Dim check2, check3 As New DataGridViewCheckBoxColumn

                check2.HeaderText = "SEL"
                check2.Name = "SEL"
                check3.HeaderText = "SEL"
                check3.Name = "SEL"

                dgvRelComercial.Columns.Insert(0, check2)
                dgvRelComercial.Columns(0).Visible = True
                dgvLineasReajuste.Columns.Insert(0, check3)
                dgvLineasReajuste.Columns(0).Visible = True
                flagemprel = True
            End If

            dgvRelComercial.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub
    Private Sub buscaCliente()
        Try
            bd.open_dimerc()
            Dim StrQuery = "select razons nombre from en_cliente "
            StrQuery = StrQuery & " where rutcli = '" & txtRutcli.Text & "'"
            StrQuery = StrQuery & "   and codemp = 3"
            Dim OdnCliente = bd.sqlSelect(StrQuery)
            If Not OdnCliente.Rows.Count = 0 Then
                txtRazons.Text = CStr(OdnCliente.Rows(0).Item("nombre"))
                buscaCoti()
            Else
                MessageBox.Show("Cliente No Existe. Debe ser Ingresado Previamente.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtRutcli.Text = ""
                txtRutcli.Focus()
                Exit Sub
            End If
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub
    Private Sub txtRutcli_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtRutcli.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Trim(txtRutcli.Text) <> "" Then
                buscaCliente()
            Else
                txtRazons.Focus()
            End If
        End If
    End Sub

    Private Sub txtRazons_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtRazons.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If txtRazons.Text.ToString.Trim <> "" And txtRutcli.Text.ToString.Trim = "" Then
                Dim buscador As New frmBuscaCliente(txtRazons.Text)
                buscador.ShowDialog()
                If buscador.dgvDatos.CurrentRow Is Nothing Then
                    Exit Sub
                End If
                txtRutcli.Text = buscador.dgvDatos.Item("Rut", buscador.dgvDatos.CurrentRow.Index).Value.ToString
                txtRazons.Text = buscador.dgvDatos.Item("Razon Social", buscador.dgvDatos.CurrentRow.Index).Value.ToString
                buscaCoti()
            Else
                If txtRutcli.Text = "" Then
                    txtRutcli.Focus()
                End If
            End If

        End If
    End Sub
    Private Sub buscaCoti()
        Try
            bd.open_dimerc()
            Dim Sql = " select a.rutcli ""Rutcli"",getrazonsocial(a.codemp,a.rutcli) ""Razon Social"",a.num_reajuste ""Num. Reajuste"", "
            Sql = Sql & " a.numcot ""Num. Cotizacion"",GET_NOMBRE(b.codven,1) ""Vendedor"",a.usr_creac ""Usuario"","
            Sql = Sql & " a.fecha_creac ""Fecha Creacion"",a.fecini_convenio ""Desde"",a.fecfin_convenio ""Vence"", a.observacion ""Observacion"", "
            Sql = Sql & " getdominio(278,a.estado) ""Estado"",nvl(c.numrel,0) numrel,b.cencos "
            Sql = Sql & " from en_reajuste_convenio a,en_cotizac b,re_emprela c "
            Sql = Sql & "where a.codemp=3 "
            If Not cmbUsuario.SelectedValue.ToString = "<<<TODOS>>>" Then
                Sql = Sql & " and  a.usr_creac='" + cmbUsuario.SelectedValue.ToString + "'"
            End If
            If Not txtRutcli.Text = "" Then
                Sql = Sql & " and a.rutcli=" + txtRutcli.Text
            End If
            Sql = Sql & " And a.estado=1 "
            Sql = Sql & " And a.codemp=b.codemp "
            Sql = Sql & " And a.numcot=b.numcot "
            Sql = Sql & " And a.codemp=c.codemp(+) "
            Sql = Sql & " And a.rutcli=c.rutcli(+) order by a.num_reajuste desc"
            Dim dt = bd.sqlSelect(Sql)
            If dt.Rows.Count = 0 Then
                MessageBox.Show("No hay reajustes hechos para este cliente", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            Else
                txtRelCom.Text = dt.Rows(0).Item("numrel").ToString
                dgvReajuste.DataSource = dt
                dgvReajuste.Columns("numrel").Visible = False
                dgvReajuste.Columns("cencos").Visible = False
                dgvReajuste.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
                If flaggrilla = False Then
                    Dim check As New DataGridViewCheckBoxColumn
                    check.HeaderText = "¿Aprueba?"
                    check.Name = "¿Aprueba?"
                    dgvReajuste.Columns.Insert(0, check)
                    dgvReajuste.Columns(0).Visible = True
                    flaggrilla = True
                End If
            End If

            PuntoControl.RegistroUso("3")
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub txtRelCom_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtRelCom.KeyPress
        e.Handled = True
    End Sub

    Private Sub btn_Salir_Click(sender As Object, e As EventArgs) Handles btn_Salir.Click
        Close()
    End Sub

    Private Sub dgvReajuste_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvReajuste.CellContentClick
        Try
            If e.RowIndex <> -1 Then
                If e.ColumnIndex = 0 Then
                    If dgvReajuste.Item("¿Aprueba?", e.RowIndex).Value = 0 Then
                        dgvReajuste.Item("¿Aprueba?", e.RowIndex).Value = 1
                    Else
                        dgvReajuste.Item("¿Aprueba?", e.RowIndex).Value = 0
                    End If
                End If
            End If
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

        End Try
    End Sub

    Private Sub cargaFechasLinea()
        Try
            bd.open_dimerc()
            Dim SQL = " select a.codlin ""Codigo"",getnombrelinea(a.codlin) ""Linea"",min(nvl(b.fecha_reajuste,'" + dtpFecVenConvenio.Value.Date + "')) ""Fecha""    "
            SQL = SQL & "   from de_reajuste_convenio a join de_fechas_reajuste_convenio b    "
            SQL = SQL & "   on b.codemp=3  "
            SQL = SQL & "   and a.num_reajuste= " + reajuste
            SQL = SQL & "   and a.codlin=b.codlin   "
            SQL = SQL & "   and a.codemp=b.codemp "
            SQL = SQL & "   and a.num_reajuste=b.num_reajuste  "
            SQL = SQL & "   group by a.codlin,getnombrelinea(a.codlin) "
            SQL = SQL & "   order by getnombrelinea(a.codlin)  "

            dgvLineasReajuste.DataSource = Nothing
            dgvLineasReajuste.DataSource = bd.sqlSelect(sql)

            dgvLineasReajuste.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells


            SQL = "select fecini_convenio,fecfin_convenio from en_reajuste_convenio where codemp=3 "
            SQL += " and num_reajuste=" + reajuste
            Dim dtFinal = bd.sqlSelect(SQL)
            If dtFinal.Rows.Count > 0 Then
                dtpFecVenConvenio.Value = dtFinal.Rows(0).Item("fecfin_convenio")
                dtpFeciniConvenio.Value = dtFinal.Rows(0).Item("fecini_convenio")
            End If


        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub


    Private Sub btn_CambiaPagina_Click(sender As Object, e As EventArgs) Handles btn_CambiaPagina.Click
        Dim cuento = 0
        For i = 0 To dgvReajuste.Rows.Count - 1
            If dgvReajuste.Item("¿Aprueba?", i).Value = 1 Then
                cuento += 1
                indice = i
            End If
        Next

        If cuento = 0 Then
            MessageBox.Show("Debe marcar una cotización", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            indice = Nothing
            Exit Sub
        End If
        If cuento > 1 Then
            MessageBox.Show("Debe marcar solo una cotización", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            indice = Nothing
            Exit Sub
        End If
        cambioConBoton = True
        tabReajuste.SelectedIndex = 1

        numcot = dgvReajuste.Item("Num. Cotizacion", indice).Value.ToString
        reajuste = dgvReajuste.Item("Num. Reajuste", indice).Value.ToString
        cliente = dgvReajuste.Item("Rutcli", indice).Value.ToString
        cencos = dgvReajuste.Item("cencos", indice).Value.ToString
        vendedor = dgvReajuste.Item("Vendedor", indice).Value.ToString
        carganegociosrelacionados()
        cargaFechasLinea()
        grbBotones.Enabled = True
        cambioConBoton = False
        PuntoControl.RegistroUso("5")
    End Sub

    Private Sub tabReajuste_Selecting(sender As Object, e As TabControlCancelEventArgs) Handles tabReajuste.Selecting
        If cambioConBoton = False Then
            e.Cancel = True
        End If
    End Sub

    Private Sub btn_Correo_Click(sender As Object, e As EventArgs) Handles btn_Correo.Click
        Try
            If Not IO.File.Exists("C:\dimerc\propuesta " & txtRazons.Text & " cliente.xlsx") Then
                'IO.File.Delete("C:\dimerc\propuesta nueva " & txtRazons.Text & " cliente.xlsx")
                preparaexcelcorreoCliente()
            End If
            If Not IO.File.Exists("C:\dimerc\propuesta " & txtRazons.Text & " vendedor.xlsx") Then
                'IO.File.Delete("C:\dimerc\propuesta nueva " & txtRazons.Text & " vendedor.xlsx")
                preparaexcelcorreoVendedor()
            End If


            'Exit Sub
            Dim cadenaCorreoPara = ""
            Dim cadenaCorreoCC = ""
            If txtMail1.Text = "" And txtMail2.Text = "" And txtMail3.Text = "" Then
                MessageBox.Show("No se enviara correo, no hay destinatarios ingresados", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            End If

            Dim fechaConvenio As String = ""
            Dim sql = "select fecini from en_reajuste_convenio where codemp=3 and num_reajuste=" + reajuste
            Dim dtFecha = bd.sqlSelect(sql)
            If dtFecha.Rows.Count > 0 Then
                fechaConvenio = dtFecha.Rows(0).Item("fecini")
            End If

            sql = "select mail01, psw_mail from ma_usuario"
            Sql += " where userid = '" + Globales.user + "'"
            Dim dt = bd.sqlSelect(sql)
            If dt.Rows.Count = 0 Then
                MessageBox.Show("No se encuentra correo de usuario, favor comunicarse con Sistemas.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            Else
                Dim correo = dt.Rows(0).Item("mail01").ToString
                Dim contraseña = dt.Rows(0).Item("psw_mail").ToString
                Dim Smtp_Server As New SmtpClient
                Dim e_mail As New MailMessage()
                Smtp_Server.UseDefaultCredentials = False
                Smtp_Server.Credentials = New Net.NetworkCredential(correo, contraseña)
                Smtp_Server.Host = "mail.dimerc.cl"

                e_mail = New MailMessage()
                e_mail.From = New MailAddress(correo)
                If txtMail1.Text <> "" Then
                    e_mail.To.Add(txtMail1.Text)
                End If
                If txtMail2.Text <> "" Then
                    e_mail.To.Add(txtMail2.Text)
                End If
                If txtMail3.Text <> "" Then
                    e_mail.To.Add(txtMail3.Text)
                End If


                If txtCC1.Text <> "" Then
                    e_mail.CC.Add(txtCC1.Text)
                End If
                If txtCC2.Text <> "" Then
                    e_mail.CC.Add(txtCC2.Text)
                End If
                If txtCC3.Text <> "" Then
                    e_mail.CC.Add(txtCC3.Text)
                End If


                e_mail.Subject = "Propuesta de Reajuste de Convenio " + txtRazons.Text
                e_mail.IsBodyHtml = False
                Dim mensaje As String = "Estimado ejecutivo : " + vbCrLf '"</br>"
                mensaje += "Le informo que a " + txtRazons.Text + " le corresponde un reajuste de su convenio con fecha " + Now.Date.ToShortDateString + "." + vbCrLf + vbCrLf '" </br></br>"
                mensaje += "Estamos enviando este correo con anticipación para que pueda informar a su cliente que con fecha " + fechaConvenio + " este reajuste será aplicado en forma automática en nuestros sistema por lo cual aparecerán los nuevos precios fijados." + vbCrLf + vbCrLf '"</br></br>"
                mensaje += "Dentro del periodo de una semana usted puede solicitar corregir algún código producto para evitar errores en la carga." + vbCrLf + vbCrLf '"</br></br>"
                mensaje += "Quedamos atentos a sus comentarios," + vbCrLf + vbCrLf + vbCrLf '"</br></br></br> "
                mensaje += "Saluda atentamente," + vbCrLf + vbCrLf
                mensaje += "Equipo de Convenios y Licitaciones Dimerc."
                e_mail.Body = mensaje
                Dim archivoAdjunto As New System.Net.Mail.Attachment("C:\dimerc\propuesta " & txtRazons.Text & " cliente.xlsx")
                Dim archivoAdjunto2 As New System.Net.Mail.Attachment("C:\dimerc\propuesta " & txtRazons.Text & " vendedor.xlsx")
                e_mail.Attachments.Add(archivoAdjunto)
                e_mail.Attachments.Add(archivoAdjunto2)
                Smtp_Server.Send(e_mail)
            End If


            Using conn = New OracleConnection(Conexion.retornaConexion)
                conn.Open()
                Dim comando As OracleCommand
                Using transaccion = conn.BeginTransaction
                    Try
                        sql = " update en_reajuste_convenio set estado=2 "
                        sql = sql & " where codemp=3 and num_reajuste= " + reajuste
                        comando = New OracleCommand(sql, conn)
                        comando.Transaction = transaccion
                        comando.ExecuteNonQuery()

                        transaccion.Commit()
                    Catch ex As Exception
                        If Not transaccion.Equals(Nothing) Then
                            transaccion.Rollback()
                        End If
                        PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                        MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Finally
                        conn.Close()
                    End Try
                End Using
            End Using


            'Outlook = New Outlook.Application()
            'Mail.To = cadenaCorreoPara
            'Mail = Outlook.CreateItem(Microsoft.Office.Interop.Outlook.OlItemType.olMailItem)
            'Mail.CC = cadenaCorreoCC
            'Mail.Subject = "Propuesta de Reajuste de Convenio " + txtRazons.Text
            'Dim mensaje As String = "Estimado ejecutivo </br>"
            'mensaje += "Le informo que a " + txtRazons.Text + " le corresponde un reajuste de su convenio con fecha " + Now.Date.ToShortDateString + ". </br></br>"
            'mensaje += "Estamos enviando este correo con 30 días de anticipación para que pueda informar a su cliente que con fecha " + Now.Date.ToShortDateString + " este reajuste será aplicado en forma automática en nuestros sistema por lo cual aparecerán los nuevos precios fijados.</br></br>"
            'mensaje += "Dentro del periodo de los próximos 15 días usted puede solicitar corregir algún código producto para evitar errores en la carga.</br></br>"
            'mensaje += "Quedamos atentos a sus comentarios,</br></br></br> "
            'mensaje += "Saluda atentamente, </br></br>"
            'mensaje += "Equipo de Convenios y Licitaciones Dimerc."
            'Mail.Attachments.Add("C:\dimerc\propuesta " & txtRazons.Text & " cliente.xlsx")
            'Mail.Attachments.Add("C:\dimerc\propuesta " & txtRazons.Text & " vendedor.xlsx")
            'mensaje = "<body style=""font-family:Calibri;"">" + mensaje + "</body>"
            'Mail.HTMLBody = mensaje + Mail.HTMLBody
            ''System.Threading.Thread.Sleep(10000)
            'Mail.Send()


            MessageBox.Show("Correo enviado", "Finalizado", MessageBoxButtons.OK, MessageBoxIcon.Information)
            'ExcelProcessKill()

            PuntoControl.RegistroUso("9")
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub


    Private Sub preparaexcelcorreoVendedor()
        Try
            bd.open_dimerc()

            Dim SQL = " select b.fecini_convenio,a.codpro, a.despro, a.marca, a.linea,a.precot,a.precio,a.mg_comercial,"
            SQL += " a.cantidad,"
            SQL += " a.alternativo,a.original,a.estado  "
            SQL += " from de_reajuste_convenio a,en_reajuste_convenio b  where a.codemp=3 "
            SQL += " And a.num_reajuste= " + reajuste
            SQL += " And a.numcot = " + numcot
            SQL += " and a.codemp=b.codemp"
            SQL += " and a.num_reajuste=b.num_reajuste"
            SQL += " and a.numcot=b.numcot"
            SQL += " order by a.original,a.alternativo "
            Dim dtvendedor = bd.sqlSelect(SQL)

            Dim car As New frmCargando
            car.ProgressBar1.Minimum = 0
            car.ProgressBar1.Value = 0
            car.ProgressBar1.Maximum = dtvendedor.Rows.Count
            car.StartPosition = FormStartPosition.CenterScreen
            car.Show()
            Cursor.Current = Cursors.WaitCursor

            Dim xlApp As Object
            Dim xlWorkBook As Object = Nothing
            Dim xlWorkSheet As Object

            Dim datestart As Date = Date.Now
            'xlApp = New Excel.ApplicationClass
            xlApp = CreateObject("Excel.Application")
            'xlApp.Visible = True

            If IO.File.Exists("C:\Program Files\Convenios_licitaciones\base para vendedor.xlsx") Then xlWorkBook = xlApp.Workbooks.Open("C:\Program Files\Convenios_licitaciones\base para vendedor.xlsx")
            'xlWorkBook = xlApp.Workbooks.Open("C:\test1.xlsx")
            xlWorkSheet = xlWorkBook.Worksheets("Propuesta Original Ejecutivo")

            xlApp.ScreenUpdating = False
            'edit the cell with new value
            xlWorkSheet.Cells(12, 4) = txtRazons.Text
            xlWorkSheet.Cells(13, 4) = txtRutcli.Text
            xlWorkSheet.Cells(15, 2) = "Precios tienen " + txtInfoExcel.Text
            xlWorkSheet.Cells(23, 2) = "Estos precios comienzan a regir desde : " + dtvendedor.Rows(0).Item("fecini_convenio")
            xlWorkSheet.Range("B15").Font.Color = Excel.XlRgbColor.rgbDarkRed

            Dim valorizadoSinMP As Double = 0
            Dim valorizadoConMP As Double = 0

            For i = 0 To dtvendedor.Rows.Count - 1

                If Not dtvendedor.Rows(i).Item("Estado") = "ACTIVO DIMERC" And
                    Not dtvendedor.Rows(i).Item("Estado") = "ARTICULO NUEVO" And
                    Not dtvendedor.Rows(i).Item("Estado") = "EXCLUSIVO CONVENIO" And
                    Not dtvendedor.Rows(i).Item("Estado") = "AGOTADO EN MERCADO" Then
                    xlWorkSheet.Range("B" + (i + 29).ToString + "", "J" + (i + 29).ToString + "").Interior.Color = Excel.XlRgbColor.rgbNavajoWhite
                    xlWorkSheet.Cells(i + 29, 2) = (i + 1).ToString
                    xlWorkSheet.Cells(i + 29, 3) = dtvendedor.Rows(i).Item("Codpro").ToString
                    xlWorkSheet.Cells(i + 29, 4) = dtvendedor.Rows(i).Item("Despro").ToString
                    xlWorkSheet.Cells(i + 29, 5) = dtvendedor.Rows(i).Item("Marca").ToString
                    xlWorkSheet.Cells(i + 29, 6) = 0
                    xlWorkSheet.Cells(i + 29, 7) = 0
                    xlWorkSheet.Cells(i + 29, 8) = 0
                    xlWorkSheet.Cells(i + 29, 9) = 0
                    Continue For
                End If

                If dtvendedor.Rows(i).Item("Alternativo") = "SI" Then
                    xlWorkSheet.Range("B" + (i + 29).ToString + "", "J" + (i + 29).ToString + "").Interior.Color = Excel.XlRgbColor.rgbLightGrey
                    xlWorkSheet.Cells(i + 29, 2) = (i + 1).ToString
                    xlWorkSheet.Cells(i + 29, 3) = dtvendedor.Rows(i).Item("Codpro").ToString
                    xlWorkSheet.Cells(i + 29, 4) = dtvendedor.Rows(i).Item("Despro").ToString
                    xlWorkSheet.Cells(i + 29, 5) = dtvendedor.Rows(i).Item("Marca").ToString
                    xlWorkSheet.Cells(i + 29, 6) = dtvendedor.Rows(i - 1).Item("Precio").ToString
                    xlWorkSheet.Cells(i + 29, 7) = dtvendedor.Rows(i).Item("Precio").ToString
                    xlWorkSheet.Cells(i + 29, 8) = dtvendedor.Rows(i).Item("Mg_Comercial") / 100
                    xlWorkSheet.Cells(i + 29, 9) = (Math.Round((dtvendedor.Rows(i).Item("Precio") - dtvendedor.Rows(i - 1).Item("Precio").ToString) / dtvendedor.Rows(i - 1).Item("Precio").ToString * 100, 2)) / 100
                    xlWorkSheet.Cells(i + 29, 10) = dtvendedor.Rows(i).Item("Original").ToString
                    valorizadoConMP = valorizadoConMP - dtvendedor.Rows(i - 1).Item("Precio").ToString * dtvendedor.Rows(i - 1).Item("Cantidad").ToString
                    valorizadoConMP += dtvendedor.Rows(i - 1).Item("Cantidad").ToString * dtvendedor.Rows(i).Item("Precio").ToString
                Else
                    xlWorkSheet.Cells(i + 29, 2) = (i + 1).ToString
                    xlWorkSheet.Cells(i + 29, 3) = dtvendedor.Rows(i).Item("Codpro").ToString
                    xlWorkSheet.Cells(i + 29, 4) = dtvendedor.Rows(i).Item("Despro").ToString
                    xlWorkSheet.Cells(i + 29, 5) = dtvendedor.Rows(i).Item("Marca").ToString
                    xlWorkSheet.Cells(i + 29, 6) = dtvendedor.Rows(i).Item("Precot").ToString
                    xlWorkSheet.Cells(i + 29, 7) = dtvendedor.Rows(i).Item("Precio").ToString
                    xlWorkSheet.Cells(i + 29, 8) = dtvendedor.Rows(i).Item("Mg_Comercial") / 100
                    xlWorkSheet.Cells(i + 29, 9) = (Math.Round((dtvendedor.Rows(i).Item("Precio") - dtvendedor.Rows(i).Item("Precot")) / dtvendedor.Rows(i).Item("Precot") * 100, 2)) / 100
                    valorizadoSinMP += dtvendedor.Rows(i).Item("Precio").ToString * dtvendedor.Rows(i).Item("Cantidad").ToString
                    valorizadoConMP += dtvendedor.Rows(i).Item("Precio").ToString * dtvendedor.Rows(i).Item("Cantidad").ToString
                End If

                car.ProgressBar1.Value += 1

                Marshal.ReleaseComObject(xlWorkSheet.Cells)
            Next
            SQL = " select toting_old viejo,toting_new nuevo from en_reajuste_convenio where codemp = 3 "
            SQL += " And num_reajuste= " + reajuste.ToString
            SQL += " And numcot = " + numcot
            Dim dtcabeza = bd.sqlSelect(SQL)
            If dtcabeza.Rows.Count > 0 Then
                xlWorkSheet.Cells(22, 7) = dtcabeza.Rows(0).Item("viejo").ToString
                xlWorkSheet.Cells(22, 8) = valorizadoSinMP
                xlWorkSheet.Cells(26, 7) = dtcabeza.Rows(0).Item("viejo").ToString
                xlWorkSheet.Cells(26, 8) = valorizadoConMP
            End If


            xlWorkSheet = xlWorkBook.Worksheets("Resumen Linea")
            xlWorkSheet.Cells(12, 4) = txtRazons.Text
            xlWorkSheet.Cells(13, 4) = txtRutcli.Text
            xlWorkSheet.Cells(15, 2) = "Precios tienen " + txtInfoExcel.Text
            xlWorkSheet.Range("B15").Font.Color = Excel.XlRgbColor.rgbDarkRed
            SQL = " select linea, sum(val_precio) valorizado,  "
            SQL = SQL & " round((sum(Val_Precio)-sum(Val_Analisis))/ sum(Val_Precio) * 100,2) mg_lista from de_reajuste_convenio  "
            SQL = SQL & "where codemp=3 and precio>0 "
            SQL = SQL & "And num_reajuste= " + reajuste.ToString
            SQL = SQL & " And numcot = " + numcot
            SQL = SQL & "group by linea  "
            SQL = SQL & "union   "
            SQL = SQL & "select 'TOTAL' linea,sum(val_precio) valorizado,  "
            SQL = SQL & "round((sum(Val_Precio)-sum(Val_Analisis))/ sum(Val_Precio) * 100,2) mg_lista from de_reajuste_convenio  "
            SQL = SQL & "where codemp=3  "
            SQL = SQL & "and num_reajuste= " + reajuste.ToString
            SQL = SQL & " and numcot = " + numcot

            Dim dtTotales = bd.sqlSelect(SQL)

            'Aca se agregan las cabeceras de nuestro datagrid.



            'Aca se ingresa el detalle recorrera la tabla celda por celda
            For i = 0 To dtTotales.Rows.Count - 1
                If dtTotales.Rows.Count - 1 = i Then
                    xlWorkSheet.Cells(i + 24, 4).Font.FontStyle = "Bold"
                    xlWorkSheet.Range("D" + (i + 24).ToString + "", "F" + (i + 24).ToString + "").Interior.Color = Excel.XlRgbColor.rgbLightGrey
                    xlWorkSheet.Cells(i + 24, 5).Font.FontStyle = "Bold"
                    xlWorkSheet.Cells(i + 24, 6).Font.FontStyle = "Bold"
                End If
                xlWorkSheet.Cells(i + 24, 4) = dtTotales.Rows(i).Item("Linea")
                xlWorkSheet.Cells(i + 24, 5) = dtTotales.Rows(i).Item("Valorizado")
                xlWorkSheet.Cells(i + 24, 6) = dtTotales.Rows(i).Item("Mg_Lista") / 100
            Next

            xlWorkSheet.Activate()

            xlApp.ScreenUpdating = True


            car.Close()
            xlApp.DisplayAlerts = False
            'xlWorkBook.Close(SaveChanges:=True)

            xlWorkBook.SaveAs("C:\dimerc\propuesta " & txtRazons.Text & " vendedor.xlsx")
            xlWorkBook.Close()

            xlApp.DisplayAlerts = True
            xlApp.Quit()

            'xlWorkSheet.ReleaseComObject(pt)
            Marshal.ReleaseComObject(xlApp)
            Marshal.ReleaseComObject(xlWorkBook)
            Marshal.ReleaseComObject(xlWorkSheet)
            Dim dateEnd As Date = Date.Now
            End_Excel_App(datestart, dateEnd)
            Cursor.Current = Cursors.Arrow

        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

        Finally
            bd.close()
        End Try
    End Sub


    Private Sub btn_Devuelve_Click(sender As Object, e As EventArgs) Handles btn_Devuelve.Click
        cambioConBoton = True
        tabReajuste.SelectedIndex = 1
        cambioConBoton = False
    End Sub


    Private Sub btn_Limpiar_Click(sender As Object, e As EventArgs) Handles btn_Limpiar.Click

        dgvReajuste.DataSource = Nothing

        txtRutcli.Text = ""
        txtRazons.Text = ""
        txtRelCom.Text = ""

        txtInfoExcel.Text = ""
        txtCC1.Text = ""
        txtCC2.Text = ""
        txtCC3.Text = ""
        txtMail1.Text = ""
        txtMail2.Text = ""
        txtMail3.Text = ""
    End Sub











    Private Sub preparaexcelcorreoCliente()
        Try
            bd.open_dimerc()

            Dim SQL = " select b.tipoconsumo,b.fecini_convenio,a.codpro, a.despro, a.marca,a.precio,a.cantidad, a.precot,nvl(a.variacion,0) variacion,a.alternativo,a.original,a.estado  "
            SQL += " from de_reajuste_convenio a,en_reajuste_convenio b where a.codemp=3 "
            SQL += " And a.num_reajuste= " + reajuste
            SQL += " and a.numcot = " + numcot
            SQL += " and a.codemp=b.codemp"
            SQL += " and a.num_reajuste=b.num_reajuste"
            SQL += " and a.numcot=b.numcot"
            SQL += " order by a.original,a.alternativo "

            Dim dtvendedor = bd.sqlSelect(SQL)

            Dim car As New frmCargando
            car.ProgressBar1.Minimum = 0
            car.ProgressBar1.Value = 0
            car.ProgressBar1.Maximum = dtvendedor.Rows.Count
            car.StartPosition = FormStartPosition.CenterScreen
            car.Show()
            Cursor.Current = Cursors.WaitCursor

            Dim xlApp As Object
            Dim xlWorkBook As Object = Nothing
            Dim xlWorkSheet As Object

            Dim datestart As Date = Date.Now
            'xlApp = New Excel.ApplicationClass
            xlApp = CreateObject("Excel.Application")
            'xlApp.Visible = True

            If IO.File.Exists("C:\Program Files\Convenios_licitaciones\base para cliente.xlsx") Then
                xlWorkBook = xlApp.Workbooks.Open("C:\Program Files\Convenios_licitaciones\base para cliente.xlsx")
            End If
            'xlWorkBook = xlApp.Workbooks.Open("C:\test1.xlsx")
            xlWorkSheet = xlWorkBook.Worksheets("Propuesta Original Cliente")

            xlApp.ScreenUpdating = False
            'edit the cell with new value
            xlWorkSheet.Cells(12, 4) = txtRazons.Text
            xlWorkSheet.Cells(13, 4) = txtRutcli.Text
            xlWorkSheet.Cells(15, 2) = "Precios tienen " + txtInfoExcel.Text
            xlWorkSheet.cells(28, 6) = "Consumo " + dtvendedor.Rows(0).Item("tipoconsumo").ToString
            xlWorkSheet.Cells(23, 2) = "Estos precios comienzan a regir desde : " + dtvendedor.Rows(0).Item("fecini_convenio")
            xlWorkSheet.Range("B15").Font.Color = Excel.XlRgbColor.rgbDarkRed
            Dim valorizadoSinMP As Double = 0
            Dim valorizadoConMP As Double = 0
            For i = 0 To dtvendedor.Rows.Count - 1

                If Not dtvendedor.Rows(i).Item("Estado") = "ACTIVO DIMERC" And
                    Not dtvendedor.Rows(i).Item("Estado") = "ARTICULO NUEVO" And
                    Not dtvendedor.Rows(i).Item("Estado") = "EXCLUSIVO CONVENIO" And
                    Not dtvendedor.Rows(i).Item("Estado") = "AGOTADO EN MERCADO" Then
                    xlWorkSheet.Range("B" + (i + 29).ToString + "", "J" + (i + 29).ToString + "").Interior.Color = Excel.XlRgbColor.rgbNavajoWhite
                    xlWorkSheet.Cells(i + 29, 2) = (i + 1).ToString
                    xlWorkSheet.Cells(i + 29, 3) = dtvendedor.Rows(i).Item("Codpro").ToString
                    xlWorkSheet.Cells(i + 29, 4) = dtvendedor.Rows(i).Item("Despro").ToString
                    xlWorkSheet.Cells(i + 29, 5) = dtvendedor.Rows(i).Item("Marca").ToString
                    xlWorkSheet.Cells(i + 29, 6) = 0
                    xlWorkSheet.Cells(i + 29, 7) = 0
                    xlWorkSheet.Cells(i + 29, 8) = 0
                    xlWorkSheet.Cells(i + 29, 9) = 0
                    Continue For
                End If

                If dtvendedor.Rows(i).Item("Alternativo") = "SI" Then
                    xlWorkSheet.Range("B" + (i + 29).ToString + "", "J" + (i + 29).ToString + "").Interior.Color = Excel.XlRgbColor.rgbLightGrey
                    xlWorkSheet.Cells(i + 29, 2) = (i + 1).ToString
                    xlWorkSheet.Cells(i + 29, 3) = dtvendedor.Rows(i).Item("Codpro").ToString
                    xlWorkSheet.Cells(i + 29, 4) = dtvendedor.Rows(i).Item("Despro").ToString
                    xlWorkSheet.Cells(i + 29, 5) = dtvendedor.Rows(i).Item("Marca").ToString
                    xlWorkSheet.Cells(i + 29, 6) = dtvendedor.Rows(i - 1).Item("Cantidad").ToString
                    xlWorkSheet.Cells(i + 29, 7) = dtvendedor.Rows(i - 1).Item("Precio").ToString
                    xlWorkSheet.Cells(i + 29, 8) = dtvendedor.Rows(i).Item("Precio").ToString
                    xlWorkSheet.Cells(i + 29, 9) = (Math.Round((dtvendedor.Rows(i).Item("Precio") - dtvendedor.Rows(i - 1).Item("Precio").ToString) / dtvendedor.Rows(i - 1).Item("Precio").ToString * 100, 2)) / 100
                    xlWorkSheet.Cells(i + 29, 10) = dtvendedor.Rows(i).Item("Original").ToString
                    valorizadoConMP = valorizadoConMP - dtvendedor.Rows(i - 1).Item("Precio").ToString * dtvendedor.Rows(i - 1).Item("Cantidad").ToString
                    valorizadoConMP += dtvendedor.Rows(i - 1).Item("Cantidad").ToString * dtvendedor.Rows(i).Item("Precio").ToString
                Else
                    xlWorkSheet.Cells(i + 29, 2) = (i + 1).ToString
                    xlWorkSheet.Cells(i + 29, 3) = dtvendedor.Rows(i).Item("Codpro").ToString
                    xlWorkSheet.Cells(i + 29, 4) = dtvendedor.Rows(i).Item("Despro").ToString
                    xlWorkSheet.Cells(i + 29, 5) = dtvendedor.Rows(i).Item("Marca").ToString
                    xlWorkSheet.Cells(i + 29, 6) = dtvendedor.Rows(i).Item("Cantidad").ToString
                    xlWorkSheet.Cells(i + 29, 7) = dtvendedor.Rows(i).Item("Precot").ToString
                    xlWorkSheet.Cells(i + 29, 8) = dtvendedor.Rows(i).Item("Precio").ToString
                    xlWorkSheet.Cells(i + 29, 9) = (Math.Round((dtvendedor.Rows(i).Item("Precio") - dtvendedor.Rows(i).Item("Precot")) / dtvendedor.Rows(i).Item("Precot") * 100, 2)) / 100
                    valorizadoSinMP += dtvendedor.Rows(i).Item("Precio").ToString * dtvendedor.Rows(i).Item("Cantidad").ToString
                    valorizadoConMP += dtvendedor.Rows(i).Item("Precio").ToString * dtvendedor.Rows(i).Item("Cantidad").ToString
                End If

                car.ProgressBar1.Value += 1
                Marshal.ReleaseComObject(xlWorkSheet.Cells)
            Next
            SQL = " select toting_old viejo,toting_new nuevo from en_reajuste_convenio where codemp = 3 "
            SQL += " And num_reajuste= " + reajuste.ToString
            SQL += " And numcot = " + numcot
            Dim dtcabeza = bd.sqlSelect(SQL)
            If dtcabeza.Rows.Count > 0 Then
                xlWorkSheet.Cells(22, 7) = dtcabeza.Rows(0).Item("viejo").ToString
                xlWorkSheet.Cells(22, 8) = valorizadoSinMP
                xlWorkSheet.Cells(26, 7) = dtcabeza.Rows(0).Item("viejo").ToString
                xlWorkSheet.Cells(26, 8) = valorizadoConMP
            End If

            xlApp.ScreenUpdating = True


            car.Close()
            xlApp.DisplayAlerts = False
            'xlWorkBook.Close(SaveChanges:=True)

            xlWorkBook.SaveAs("C:\dimerc\propuesta " & txtRazons.Text & " cliente.xlsx")
            xlWorkBook.Close()

            xlApp.DisplayAlerts = True
            xlApp.Quit()

            'xlWorkSheet.ReleaseComObject(pt)
            Marshal.ReleaseComObject(xlApp)
            Marshal.ReleaseComObject(xlWorkBook)
            Marshal.ReleaseComObject(xlWorkSheet)
            Dim dateEnd As Date = Date.Now
            End_Excel_App(datestart, dateEnd)


            Cursor.Current = Cursors.Arrow

        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

        Finally
            bd.close()
        End Try
    End Sub

    Private Sub btn_Base_Click(sender As Object, e As EventArgs) Handles btn_Base.Click
        exportar()
    End Sub

    Private Sub End_Excel_App(datestart As Date, dateEnd As Date)
        Dim xlp() As Process = Process.GetProcessesByName("EXCEL")
        For Each Process As Process In xlp
            If Process.StartTime >= datestart And Process.StartTime <= dateEnd Then
                Process.Kill()
                Exit For
            End If
        Next
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles btn_ApruebaConvenio.Click
        Using conn = New OracleConnection(Conexion.retornaConexion)
            conn.Open()
            Using transaccion = conn.BeginTransaction
                Try
                    Dim comando As OracleCommand
                    Dim dt As New DataTable
                    Dim Sql = "Set Role Rol_Aplicacion"
                    comando = New OracleCommand(Sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    For i = 0 To dgvReajuste.Rows.Count - 1
                        If dgvReajuste.Item("¿Aprueba?", i).Value = 1 Then
                            Sql = " update en_reajuste_convenio set estado=2 "
                            Sql = Sql & " where codemp=3 and num_reajuste= " + dgvReajuste.Item("Num. Reajuste", i).Value.ToString
                            comando = New OracleCommand(Sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()
                        End If
                    Next

                    transaccion.Commit()

                    MessageBox.Show("Convenios Aprobados, durante la noche seran cargadas y a partir de mañana estarán disponibles", "Finalizado", MessageBoxButtons.OK, MessageBoxIcon.Information)

                    PuntoControl.RegistroUso("4")

                    buscaCoti()
                Catch ex As Exception
                    If Not transaccion.Equals(Nothing) Then
                        transaccion.Rollback()
                    End If
                    PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                    MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Finally
                    conn.Close()
                End Try

            End Using
        End Using


    End Sub

    Private Sub txtRutcli_Click(sender As Object, e As EventArgs) Handles txtRutcli.Click, txtRazons.Click
        txtRelCom.Text = ""
        txtRutcli.Text = ""
        txtRazons.Text = ""
    End Sub

    Private Sub cmbUsuario_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbUsuario.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            buscaCoti()
        End If
    End Sub

    Private Sub btn_Buscar_Click(sender As Object, e As EventArgs) Handles btn_Buscar.Click
        buscaCoti()
    End Sub

    Private Sub chkLineas_CheckedChanged(sender As Object, e As EventArgs) Handles chkLineas.CheckedChanged
        If chkLineas.Checked = True Then
            For i = 0 To dgvLineasReajuste.RowCount - 1
                dgvLineasReajuste.Item("SEL", i).Value = 1
            Next
        Else
            For i = 0 To dgvLineasReajuste.RowCount - 1
                dgvLineasReajuste.Item("SEL", i).Value = 0
            Next
        End If
    End Sub

    Private Sub chkMarcaTodoConvenio_CheckedChanged(sender As Object, e As EventArgs) Handles chkMarcaTodoConvenio.CheckedChanged
        If chkMarcaTodoConvenio.Checked = True Then
            For i = 0 To dgvRelComercial.RowCount - 1
                dgvRelComercial.Item("SEL", i).Value = 1
            Next
        Else
            For i = 0 To dgvRelComercial.RowCount - 1
                dgvRelComercial.Item("SEL", i).Value = 0
            Next
        End If
    End Sub

    Private Sub btn_AgregaFecha_Click(sender As Object, e As EventArgs) Handles btn_AgregaFecha.Click
        Using conn = New OracleConnection(Conexion.retornaConexion)
            conn.Open()
            Dim comando As OracleCommand
            Dim dataAdapter As OracleDataAdapter
            Using transaccion = conn.BeginTransaction
                Dim sql As String = ""
                Try

                    sql = "Set Role Rol_Aplicacion"
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    sql = "update en_reajuste_convenio set "
                    sql += " fecini_convenio=to_date('" + dtpFeciniConvenio.Value.Date + "','dd/mm/yyyy'),"
                    sql += " fecfin_convenio=to_date('" + dtpFecVenConvenio.Value.Date + "','dd/mm/yyyy') "
                    sql += " where codemp=3 and num_reajuste=" + reajuste
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()
                    For i = 0 To dgvLineasReajuste.RowCount - 1
                        If dgvLineasReajuste.Item("SEL", i).Value = 1 Then
                            Dim dt As New DataTable
                            sql = "select 1 from de_fechas_reajuste_convenio where codemp=3 "
                            sql += " and num_reajuste=" + reajuste
                            sql += " and codlin=" + dgvLineasReajuste.Item("Codigo", i).Value.ToString
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            dataAdapter = New OracleDataAdapter(comando)
                            dataAdapter.Fill(dt)
                            If dt.Rows.Count = 0 Then
                                sql = " insert into de_fechas_reajuste_convenio ( "
                                sql += " codemp,num_reajuste,rutcli,codlin,fecha_reajuste,linea) "
                                sql += " values(3,"
                                sql += reajuste + ","
                                sql += txtRutcli.Text + ","
                                sql += dgvLineasReajuste.Item("Codigo", i).Value.ToString + ","
                                sql += " to_date('" + dtpFechaLineaReajuste.Value.Date + "','dd/mm/yyyy'),"
                                sql += " getnombrelinea(" + dgvLineasReajuste.Item("Codigo", i).Value.ToString + ")"
                                sql += ") "
                                comando = New OracleCommand(sql, conn)
                                comando.Transaction = transaccion
                                comando.ExecuteNonQuery()
                            Else
                                sql = "update de_fechas_reajuste_convenio set "
                                sql += " fecha_reajuste=to_date('" + dtpFechaLineaReajuste.Value.Date + "','dd/mm/yyyy') "
                                sql += "where codemp=3 and num_reajuste=" + reajuste
                                sql += " and codlin=" + dgvLineasReajuste.Item("Codigo", i).Value.ToString
                                comando = New OracleCommand(sql, conn)
                                comando.Transaction = transaccion
                                comando.ExecuteNonQuery()
                            End If
                        End If
                    Next

                    sql = " select b.codpro,nvl(c.FECHA_REAJUSTE,'" + dtpFecVenConvenio.Value.Date + "')  fecven "
                    sql = sql & " from en_reajuste_convenio a,de_reajuste_convenio b,de_fechas_reajuste_convenio c  "
                    sql = sql & " where a.codemp=3   "
                    sql = sql & " and a.num_reajuste=b.num_reajuste  "
                    sql = sql & " and a.codemp=b.codemp  "
                    sql = sql & " and b.estado in ('ACTIVO DIMERC','EXCLUSIVO CONVENIO','AGOTADO EN MERCADO','ARTICULO NUEVO') "
                    sql = sql & " and a.num_reajuste = " + reajuste
                    sql = sql & " and a.numcot = " + numcot
                    sql = sql & " and B.num_reajuste=c.num_reajuste(+)  "
                    sql = sql & " and b.codemp=c.codemp(+)  "
                    sql = sql & " and b.codlin=c.codlin(+) "
                    Dim dtFechas As New DataTable
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    dataAdapter = New OracleDataAdapter(comando)
                    dataAdapter.Fill(dtFechas)
                    For i = 0 To dtFechas.Rows.Count - 1
                        sql = " update de_reajuste_convenio "
                        sql += " set fecfin = to_date('" + dtFechas.Rows(i).Item("fecven") + "','dd/mm/yyyy'), "
                        sql += " fecini= to_date('" + dtpFeciniConvenio.Value.Date + "','dd/mm/yyyy')"
                        sql += " where codemp=3"
                        sql += " and num_reajuste = " + reajuste
                        sql += " and numcot = " + numcot
                        sql += " and codpro='" + dtFechas.Rows(i).Item("codpro").ToString + "'"
                        comando = New OracleCommand(sql, conn)
                        comando.Transaction = transaccion
                        comando.ExecuteNonQuery()
                    Next

                    If dtpFechaLineaReajuste.Value.Date > dtpFecVenConvenio.Value.Date Then
                        dtpFecVenConvenio.Value = dtpFechaLineaReajuste.Value.Date
                    End If

                    sql = "update en_reajuste_convenio set "
                    sql += " fecini_convenio=to_date('" + dtpFeciniConvenio.Value.Date + "','dd/mm/yyyy'),"
                    sql += " fecfin_convenio=to_date('" + dtpFecVenConvenio.Value.Date + "','dd/mm/yyyy') "
                    sql += " where codemp=3 and num_reajuste=" + reajuste
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    transaccion.Commit()

                    MessageBox.Show("Fechas agregadas con éxito", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information)

                    cargaFechasLinea()

                    For i = 0 To dgvLineasReajuste.RowCount - 1
                        If dgvLineasReajuste.Item("Fecha", i).Value < Now.Date Then
                            MessageBox.Show("Existen Lineas con fecha de vencimiento menor al dia de hoy, por favor corregir y luego continuar", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            Exit Sub
                        End If
                    Next
                    For i = 0 To dgvLineasReajuste.Rows.Count - 1
                        If dgvLineasReajuste.Item("Fecha", i).Value > dtpFecVenConvenio.Value.Date Then
                            MessageBox.Show("Existe una linea con fecha de termino mayor a la fecha de termino del convenio, favor revisar antes de continuar", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            Exit Sub
                        End If
                    Next
                    For i = 0 To dgvLineasReajuste.Rows.Count - 1
                        If dgvLineasReajuste.Item("Fecha", i).Value < dtpFeciniConvenio.Value.Date Then
                            MessageBox.Show("Existe una linea con fecha de termino anterior a la fecha de inicio del convenio, favor revisar antes de continuar", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            Exit Sub
                        End If
                    Next
                    If dtpFecVenConvenio.Value.Date < dtpFeciniConvenio.Value.Date Then
                        MessageBox.Show("Fecha de vencimiento de convenio no puede ser anterior a la fecha de inicio", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Exit Sub
                    End If

                    PuntoControl.RegistroUso("7")
                Catch ex As Exception
                    If Not transaccion.Equals(Nothing) Then
                        transaccion.Rollback()
                    End If
                    PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                    MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Finally
                    conn.Close()
                End Try
            End Using
        End Using
    End Sub

    Private Sub dtpFecVenConvenio_ValueChanged(sender As Object, e As EventArgs) Handles dtpFecVenConvenio.ValueChanged
        cargaFechasLinea()
    End Sub

    Private Sub dgvRelComercial_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvRelComercial.CellContentClick
        Try
            If e.RowIndex <> -1 Then
                If e.ColumnIndex = 0 Then
                    If dgvRelComercial.Item("SEL", e.RowIndex).Value = 0 Then
                        dgvRelComercial.Item("SEL", e.RowIndex).Value = 1
                    Else
                        dgvRelComercial.Item("SEL", e.RowIndex).Value = 0
                    End If
                End If
            End If
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub dgvLineasReajuste_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvLineasReajuste.CellContentClick
        Try
            If e.RowIndex <> -1 Then
                If e.ColumnIndex = 0 Then
                    If dgvLineasReajuste.Item("SEL", e.RowIndex).Value = 0 Then
                        dgvLineasReajuste.Item("SEL", e.RowIndex).Value = 1
                    Else
                        dgvLineasReajuste.Item("SEL", e.RowIndex).Value = 0
                    End If
                End If
            End If
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btn_Devuelve2.Click
        cambioConBoton = True
        tabReajuste.SelectedIndex = 0
        grbBotones.Enabled = False
        cambioConBoton = False
    End Sub

    Private Sub btn_Grabar_Click(sender As Object, e As EventArgs) Handles btn_Grabar.Click

        Using conn = New OracleConnection(Conexion.retornaConexion)
            conn.Open()
            Using transaccion = conn.BeginTransaction
                Dim sql As String
                Try

                    Cursor.Current = Cursors.WaitCursor
                    Dim comando As OracleCommand
                    'Dim dataAdapter As OracleDataAdapter
                    Dim dt As New DataTable
                    sql = ""

                    sql = "Set Role Rol_Aplicacion"
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()


                    'AGREGA RUT PARA PROCEDIMIENTO QUE CARGA EL CONVENIO EN ORACLE
                    sql = " delete from re_rut_convenio where codemp=3 and "
                    sql += " num_reajuste =" + reajuste
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    sql = "insert into re_rut_convenio (codemp,num_reajuste,rutcli) values("
                    sql += "3,"
                    sql += reajuste + ","
                    sql += txtRutcli.Text
                    sql += ")"
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()
                    'SI HAY RELACIONADOS MARCADOS, TAMBIEN SE AGREGAN
                    For i = 0 To dgvRelComercial.RowCount - 1
                        If dgvRelComercial.Item("Sel", i).Value = 1 Then
                            sql = "insert into re_rut_convenio (codemp,num_reajuste,rutcli) values("
                            sql += "3,"
                            sql += reajuste + ","
                            sql += dgvRelComercial.Item("Rutcli", i).Value.ToString
                            sql += ")"
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()
                        End If
                    Next

                    transaccion.Commit()
                    Cursor.Current = Cursors.Arrow
                    MessageBox.Show("Carga de Datos Lista, ahora puede grabar el convenio en el botón ubicado mas abajo ", "Listo", MessageBoxButtons.OK, MessageBoxIcon.Information)

                    PuntoControl.RegistroUso("6")

                    'grbfechasLinea.Enabled = True
                Catch ex As Exception
                    If Not transaccion.Equals(Nothing) Then
                        transaccion.Rollback()
                    End If
                    PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                    MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Finally
                    conn.Close()
                End Try
            End Using
        End Using
    End Sub

    Private Sub btn_Avanza_Click(sender As Object, e As EventArgs) Handles btn_Avanza.Click
        cambioConBoton = True
        tabReajuste.SelectedIndex = 2

        cambioConBoton = False
    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles btn_CargaConvenio.Click
        If Trim(reajuste) = "" Then
            MessageBox.Show("No hay un numero de reajuste seleccionado para grabar", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        For i = 0 To dgvLineasReajuste.RowCount - 1
            If dgvLineasReajuste.Item("Fecha", i).Value < Now.Date Then
                MessageBox.Show("Existen Lineas con fecha de vencimiento menor al dia de hoy, por favor corregir y luego continuar", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            End If
        Next
        For i = 0 To dgvLineasReajuste.Rows.Count - 1
            If dgvLineasReajuste.Item("Fecha", i).Value > dtpFecVenConvenio.Value.Date Then
                MessageBox.Show("Existe una linea con fecha de termino mayor a la fecha de termino del convenio, favor revisar antes de continuar", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            End If
        Next
        For i = 0 To dgvLineasReajuste.Rows.Count - 1
            If dgvLineasReajuste.Item("Fecha", i).Value < dtpFeciniConvenio.Value.Date Then
                MessageBox.Show("Existe una linea con fecha de termino anterior a la fecha de inicio del convenio, favor revisar antes de continuar", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            End If
        Next
        If dtpFecVenConvenio.Value.Date < dtpFeciniConvenio.Value.Date Then
            MessageBox.Show("Fecha de vencimiento de convenio no puede ser anterior a la fecha de inicio", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        btn_CargaConvenio.Enabled = False
        Using conn = New OracleConnection(Conexion.retornaConexion)
            conn.Open()
            Using transaccion = conn.BeginTransaction
                Dim sql As String
                Try
                    Dim comando As OracleCommand
                    Dim convenio As String

                    sql = "update en_reajuste_convenio set estado=2 "
                    sql += " where codemp=3 "
                    sql += " and num_reajuste= " + reajuste
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    If cmbTipoConvenio.SelectedItem = "Convenio Tissue" Then
                        convenio = "T"
                    Else
                        convenio = "M"
                    End If

                    sql = " begin "
                    sql = sql & "    put_insertareajuste(" + reajuste + ",3,'" & convenio & "'); "
                    sql = Sql & "end;  "

                    comando = New OracleCommand(Sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    transaccion.Commit()

                    MessageBox.Show("Convenio guardado con éxito", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information)

                    cambioConBoton = True
                    tabReajuste.SelectedIndex = 0
                    buscaCoti()

                    cambioConBoton = False

                    PuntoControl.RegistroUso("8")
                Catch ex As Exception
                    If Not transaccion.Equals(Nothing) Then
                        transaccion.Rollback()
                    End If
                    PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                    MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Finally
                    conn.Close()
                    btn_CargaConvenio.Enabled = True
                End Try
            End Using
        End Using
    End Sub

    Private Sub dtpFeciniConvenio_ValueChanged(sender As Object, e As EventArgs) Handles dtpFeciniConvenio.ValueChanged
        For i = 0 To dgvLineasReajuste.RowCount - 1
            If dgvLineasReajuste.Item("Fecha", i).Value < dtpFeciniConvenio.Value.Date Then
                dgvLineasReajuste.Item("Fecha", i).Value = dtpFeciniConvenio.Value.Date
            End If
        Next
    End Sub

    Private Sub dgvReajuste_CurrentCellChanged(sender As Object, e As EventArgs) Handles dgvReajuste.CurrentCellChanged
        If flaggrilla = True And Not dgvReajuste.CurrentRow Is Nothing Then
            txtRutcli.Text = dgvReajuste.Item("Rutcli", dgvReajuste.CurrentRow.Index).Value.ToString
            txtRazons.Text = dgvReajuste.Item("Razon Social", dgvReajuste.CurrentRow.Index).Value.ToString
            txtRelCom.Text = dgvReajuste.Item("numrel", dgvReajuste.CurrentRow.Index).Value.ToString
        End If
    End Sub

    Private Sub exportar()

        Try
            If IO.File.Exists("C:\dimerc\propuesta " & txtRazons.Text & ".xlsx") Then
                IO.File.Delete("C:\dimerc\propuesta " & txtRazons.Text & ".xlsx")
            End If

            Dim save As New SaveFileDialog
            save.Filter = "Archivo Excel | *.xlsx"
            save.InitialDirectory = "C:\dimerc"
            save.FileName = "propuesta " & txtRazons.Text & ".xlsx"
            'xlWorkBook.SaveAs("C:\dimerc\propuesta " & txtRazons.Text & ".xlsx", Excel.XlFileFormat.xlWorkbookNormal)
            If save.ShowDialog = System.Windows.Forms.DialogResult.OK Then

                bd.open_dimerc()
                Dim sql = " select b.fecini_convenio,a.codpro, a.despro, a.marca, a.linea,a.precio, a.precot, a.mg_comercial, "
                sql += " a.cantidad, a.alternativo,decode(a.original,'',a.codpro,a.original) original,a.estado  "
                sql += " from de_reajuste_convenio a,en_reajuste_convenio b where a.codemp=3  "
                sql += " and a.num_reajuste= " + reajuste
                sql += " and a.numcot=" + numcot.ToString
                sql += " and a.codemp=b.codemp"
                sql += " and a.num_reajuste=b.num_reajuste"
                sql += " and a.numcot=b.numcot"
                sql += " order by a.original,a.alternativo "
                Dim dtvendedor = bd.sqlSelect(sql)

                Dim rutrelacionado = ""
                If dgvRelComercial.RowCount = 0 Then
                    rutrelacionado = txtRutcli.Text
                Else
                    rutrelacionado = txtRutcli.Text
                    For i = 0 To dgvRelComercial.RowCount - 1
                        rutrelacionado += "," + dgvRelComercial.Item("rutcli", i).Value.ToString
                    Next
                End If

                sql = " select a.codpro,despro,mpropia , "
                sql = sql & " linea,marca,cantidad,a.precio, "
                sql = sql & " costoanalisis ""Costo Analisis"", "
                sql = sql & " coscom ""Costo Comercial"", "
                sql = sql & " cosprom ""Costo Promedio"", "
                sql = sql & " cosesp ""Costo Especial"", "
                sql = sql & " cosesppm ""Costo Especial PM"", "
                sql = sql & " aporte,peso, "
                sql = sql & " precot ""Precio Cotizacion"", "
                sql = sql & " estado, "
                sql = sql & " pedsto, "
                sql = sql & " ultcom, "
                sql = sql & " cosrep, "
                sql = sql & " variacion, "
                sql = sql & " fecini_cosesp ""FecIni Costo Especial"", "
                sql = sql & " fecfin_cosesp ""FecFin Costo Especial"", "
                sql = sql & " val_old ""Valorizado Anterior"", "
                sql = sql & " alternativo, "
                sql = sql & " nvl(h.precio,0) ""Precio Min. Venta"", "
                sql = sql & " nvl(i.precio,0) ""Precio Min. Coti"",GET_TIENEPRODUCTOALTERNATIVO(a.codpro) ""Estado Alternativo"" "
                sql = sql & " from de_reajuste_convenio a, "
                sql = sql & " (select codpro,min(precio) precio from   "
                sql = sql & "   (select b.codpro,min(b.precio) precio from en_factura a,de_factura b where a.codemp=3     "
                sql = sql & "   and a.rutcli in(" + rutrelacionado + ")    "
                sql = sql & "   and a.codemp=b.codemp    "
                sql = sql & "   and a.numfac=b.numfac    "
                sql = sql & "   and a.fecemi>= add_months(sysdate, -6)  group by b.codpro  "
                sql = sql & "   union  "
                sql = sql & "   select b.codpro,min(b.precio) precio from  en_factura a,de_guiades b,re_guiafac c where a.codemp=3     "
                sql = sql & "   and a.rutcli in(" + rutrelacionado + ")    "
                sql = sql & "   and a.codemp=b.codemp    "
                sql = sql & "   and a.numfac=c.numfac  "
                sql = sql & "   and b.numgui=c.numgui    "
                sql = sql & "   and a.fecemi>= add_months(sysdate, -6)  group by b.codpro)  "
                sql = sql & "   group by codpro ) h, "
                sql = sql & " (select b.codpro,min(b.precio) precio from en_cotizac a,de_cotizac b  "
                sql = sql & " where a.codemp=3   "
                sql = sql & " and a.rutcli in(" + rutrelacionado + ")  "
                sql = sql & " and a.codemp=b.codemp  "
                sql = sql & " and a.numcot=b.numcot  "
                sql = sql & " and a.fecemi>= add_months(sysdate, -6)  "
                sql = sql & " group by b.codpro) i  "
                sql = sql & " where codemp=3  "
                sql = sql & " and num_reajuste= " + reajuste
                sql = sql & " and numcot=" + numcot.ToString
                sql = sql & " and a.codpro=i.codpro(+) "
                sql = sql & " and a.codpro=h.codpro(+) "
                sql = sql & " order by alternativo,a.codpro"
                Dim dtdetalle = bd.sqlSelect(sql)



                Dim car As New frmCargando
                car.ProgressBar1.Minimum = 0
                car.ProgressBar1.Value = 0
                car.ProgressBar1.Maximum = dtvendedor.Rows.Count * 2
                car.StartPosition = FormStartPosition.CenterScreen
                car.Show()
                Cursor.Current = Cursors.WaitCursor

                Dim xlApp As Object
                Dim xlWorkBook As Object = Nothing
                Dim xlWorkSheet As Object


                'xlApp = New Excel.ApplicationClass
                xlApp = CreateObject("Excel.Application")
                'xlApp.Visible = True


                If IO.File.Exists("C:\Program Files\Convenios_licitaciones\base.xlsx") Then xlWorkBook = xlApp.Workbooks.Open("C:\Program Files\Convenios_licitaciones\base.xlsx")
                'xlWorkBook = xlApp.Workbooks.Open("C:\test1.xlsx")
                xlWorkSheet = xlWorkBook.Worksheets("Propuesta Original")

                xlApp.ScreenUpdating = False
                'edit the cell with new value
                xlWorkSheet.Cells(12, 4) = txtRazons.Text
                xlWorkSheet.Cells(13, 4) = txtRutcli.Text
                xlWorkSheet.Cells(14, 4) = vendedor
                xlWorkSheet.Cells(23, 2) = "Estos precios comienzan a regir desde : " + dtvendedor.Rows(0).Item("fecini_convenio")
                Dim valorizadoSinMP As Double = 0
                Dim valorizadoConMP As Double = 0

                For i = 0 To dtvendedor.Rows.Count - 1
                    If Not dtvendedor.Rows(i).Item("Estado") = "ACTIVO DIMERC" And
                    Not dtvendedor.Rows(i).Item("Estado") = "ARTICULO NUEVO" And
                    Not dtvendedor.Rows(i).Item("Estado") = "EXCLUSIVO CONVENIO" And
                    Not dtvendedor.Rows(i).Item("Estado") = "AGOTADO EN MERCADO" Then
                        xlWorkSheet.Range("B" + (i + 29).ToString + "", "J" + (i + 29).ToString + "").Interior.Color = Excel.XlRgbColor.rgbNavajoWhite
                        xlWorkSheet.Cells(i + 29, 2) = (i + 1).ToString
                        xlWorkSheet.Cells(i + 29, 3) = dtvendedor.Rows(i).Item("Codpro").ToString
                        xlWorkSheet.Cells(i + 29, 4) = dtvendedor.Rows(i).Item("Despro").ToString
                        xlWorkSheet.Cells(i + 29, 5) = dtvendedor.Rows(i).Item("Marca").ToString
                        xlWorkSheet.Cells(i + 29, 6) = 0
                        xlWorkSheet.Cells(i + 29, 7) = 0
                        xlWorkSheet.Cells(i + 29, 8) = 0
                        xlWorkSheet.Cells(i + 29, 9) = 0
                        Continue For
                    End If
                    If dtvendedor.Rows(i).Item("Alternativo") = "SI" Then
                        xlWorkSheet.Range("B" + (i + 29).ToString + "", "J" + (i + 29).ToString + "").Interior.Color = Excel.XlRgbColor.rgbLightGrey
                        xlWorkSheet.Cells(i + 29, 2) = (i + 1).ToString
                        xlWorkSheet.Cells(i + 29, 3) = dtvendedor.Rows(i).Item("Codpro").ToString
                        xlWorkSheet.Cells(i + 29, 4) = dtvendedor.Rows(i).Item("Despro").ToString
                        xlWorkSheet.Cells(i + 29, 5) = dtvendedor.Rows(i).Item("Marca").ToString
                        xlWorkSheet.Cells(i + 29, 6) = dtvendedor.Rows(i - 1).Item("Precio").ToString
                        xlWorkSheet.Cells(i + 29, 7) = dtvendedor.Rows(i).Item("Precio").ToString
                        xlWorkSheet.Cells(i + 29, 8) = dtvendedor.Rows(i).Item("Mg_Comercial") / 100
                        xlWorkSheet.Cells(i + 29, 9) = (Math.Round((dtvendedor.Rows(i).Item("Precio") - dtvendedor.Rows(i - 1).Item("Precio").ToString) / dtvendedor.Rows(i - 1).Item("Precio").ToString * 100, 2)) / 100
                        xlWorkSheet.Cells(i + 29, 10) = dtvendedor.Rows(i - 1).Item("Original").ToString
                        valorizadoConMP = valorizadoConMP - dtvendedor.Rows(i - 1).Item("Precio").ToString * dtvendedor.Rows(i - 1).Item("Cantidad").ToString
                        valorizadoConMP += dtvendedor.Rows(i - 1).Item("Cantidad").ToString * dtvendedor.Rows(i).Item("Precio").ToString
                    Else
                        xlWorkSheet.Cells(i + 29, 2) = (i + 1).ToString
                        xlWorkSheet.Cells(i + 29, 3) = dtvendedor.Rows(i).Item("Codpro").ToString
                        xlWorkSheet.Cells(i + 29, 4) = dtvendedor.Rows(i).Item("Despro").ToString
                        xlWorkSheet.Cells(i + 29, 5) = dtvendedor.Rows(i).Item("Marca").ToString
                        xlWorkSheet.Cells(i + 29, 6) = dtvendedor.Rows(i).Item("Precot").ToString
                        xlWorkSheet.Cells(i + 29, 7) = dtvendedor.Rows(i).Item("Precio").ToString
                        xlWorkSheet.Cells(i + 29, 8) = dtvendedor.Rows(i).Item("Mg_Comercial") / 100
                        xlWorkSheet.Cells(i + 29, 9) = (Math.Round((dtvendedor.Rows(i).Item("Precio") - dtvendedor.Rows(i).Item("Precot")) / dtvendedor.Rows(i).Item("Precot") * 100, 2)) / 100
                        valorizadoSinMP += dtvendedor.Rows(i).Item("Precio").ToString * dtvendedor.Rows(i).Item("Cantidad").ToString
                        valorizadoConMP += dtvendedor.Rows(i).Item("Precio").ToString * dtvendedor.Rows(i).Item("Cantidad").ToString
                    End If
                    car.ProgressBar1.Value += 1
                Next

                sql = " select sum(val_old) viejo,sum(val_precio) nuevo from de_reajuste_convenio where codemp=3 "
                sql = sql & " and num_reajuste= " + reajuste
                sql = sql & " and numcot=" + numcot.ToString
                sql += " and alternativo='NO' "

                Dim dtcabeza = bd.sqlSelect(sql)
                If dtcabeza.Rows.Count > 0 Then
                    xlWorkSheet.Cells(22, 7) = dtcabeza.Rows(0).Item("viejo").ToString
                    xlWorkSheet.Cells(22, 8) = valorizadoSinMP
                    xlWorkSheet.Cells(26, 7) = dtcabeza.Rows(0).Item("viejo").ToString
                    xlWorkSheet.Cells(26, 8) = valorizadoConMP
                End If

                xlWorkSheet = xlWorkBook.Worksheets("Data Original")

                For i = 0 To dtdetalle.Rows.Count - 1
                    xlWorkSheet.Cells(i + 2, 1) = dtdetalle.Rows(i).Item("Codpro").ToString
                    xlWorkSheet.Cells(i + 2, 2) = dtdetalle.Rows(i).Item("Despro").ToString
                    xlWorkSheet.Cells(i + 2, 3) = dtdetalle.Rows(i).Item("Mpropia").ToString
                    xlWorkSheet.Cells(i + 2, 4) = dtdetalle.Rows(i).Item("Linea").ToString
                    xlWorkSheet.Cells(i + 2, 5) = dtdetalle.Rows(i).Item("Marca").ToString
                    xlWorkSheet.Cells(i + 2, 6) = IIf(dtdetalle.Rows(i).Item("Cantidad") > 0, dtdetalle.Rows(i).Item("Cantidad").ToString, 0)
                    xlWorkSheet.Cells(i + 2, 7) = dtdetalle.Rows(i).Item("Precio").ToString
                    xlWorkSheet.Cells(i + 2, 8) = dtdetalle.Rows(i).Item("Costo Analisis").ToString
                    xlWorkSheet.Cells(i + 2, 9) = dtdetalle.Rows(i).Item("Costo Promedio").ToString
                    xlWorkSheet.Cells(i + 2, 10) = dtdetalle.Rows(i).Item("Costo Especial").ToString
                    xlWorkSheet.Cells(i + 2, 11) = dtdetalle.Rows(i).Item("Costo Especial PM").ToString
                    xlWorkSheet.Cells(i + 2, 12) = dtdetalle.Rows(i).Item("Aporte") / 100
                    xlWorkSheet.Cells(i + 2, 13) = dtdetalle.Rows(i).Item("Peso") / 100
                    'xlWorkSheet.Cells(i + 2, 14) = dgvDetalleLineaProductos.Item("Mg. Comercial", i).Value / 100
                    Dim oXLRange As Excel.Range
                    oXLRange = CType(xlWorkSheet.Cells(i + 2, 14), Excel.Range)
                    oXLRange.Formula = "=1-H" & i + 2 & "/G" & i + 2
                    'xlWorkSheet.Cells(i + 2, 15) = dgvDetalleLineaProductos.Item("Mg. Inventario", i).Value / 100

                    oXLRange = CType(xlWorkSheet.Cells(i + 2, 15), Excel.Range)
                    oXLRange.FormulaLocal = "=SI(H" & i + 2 & "<I" & i + 2 & ";1-H" & i + 2 & "/G" & i + 2 & ";1-I" & i + 2 & "/G" & i + 2 & ")"

                    'xlWorkSheet.Cells(i + 2, 16) = dgvDetalleLineaProductos.Item("Mg. Rebate", i).Value / 100
                    oXLRange = CType(xlWorkSheet.Cells(i + 2, 16), Excel.Range)
                    oXLRange.FormulaLocal = "=SI(H" & i + 2 & "<W" & i + 2 & ";1-H" & i + 2 & "/G" & i + 2 & ";1-W" & i + 2 & "/G" & i + 2 & ")"


                    'xlWorkSheet.Cells(i + 2, 17) = dgvDetalleLineaProductos.Item("Valorizado Precio", i).Value.ToString
                    oXLRange = CType(xlWorkSheet.Cells(i + 2, 17), Excel.Range)
                    oXLRange.Formula = "=G" & i + 2 & "*F" & i + 2

                    'xlWorkSheet.Cells(i + 2, 18) = dgvDetalleLineaProductos.Item("Valorizado Analisis", i).Value.ToString
                    ''CALCULAR VALORIZADO ANALISIS
                    oXLRange = CType(xlWorkSheet.Cells(i + 2, 18), Excel.Range)
                    oXLRange.Formula = "=H" & i + 2 & "*F" & i + 2

                    'CALCULAR VALORIZADO INVENTARIO
                    'xlWorkSheet.Cells(i + 2, 19) = dgvDetalleLineaProductos.Item("Valorizado Inventario", i).Value.ToString
                    oXLRange = CType(xlWorkSheet.Cells(i + 2, 19), Excel.Range)
                    oXLRange.Formula = "=I" & i + 2 & "*F" & i + 2

                    'deja con formula valorizado rebate
                    'CALCULAR VALORIZADO REBATE
                    'xlWorkSheet.Cells(i + 2, 20) = dgvDetalleLineaProductos.Item("Valorizado Rebate", i).Value.ToString
                    oXLRange = CType(xlWorkSheet.Cells(i + 2, 20), Excel.Range)
                    oXLRange.Formula = "=W" & i + 2 & "*F" & i + 2
                    xlWorkSheet.Cells(i + 2, 22) = dtdetalle.Rows(i).Item("Costo Comercial").ToString
                    'xlWorkSheet.Cells(i + 2, 23) = dgvDetalleLineaProductos.Item("Costo Rebate", i).Value.ToString
                    oXLRange = CType(xlWorkSheet.Cells(i + 2, 23), Excel.Range)
                    oXLRange.Formula = "=I" & i + 2 & "*(1-L" & i + 2 & ")"

                    'xlWorkSheet.Cells(i + 2, 28) = dgvDetalleLineaProductos.Item("Contribucion Comercial", i).Value.ToString
                    oXLRange = CType(xlWorkSheet.Cells(i + 2, 28), Excel.Range)
                    oXLRange.Formula = "=(Q" & i + 2 & "-R" & i + 2 & ")"
                    'xlWorkSheet.Cells(i + 2, 29) = dgvDetalleLineaProductos.Item("Contrib. Inventario", i).Value.ToString

                    oXLRange = CType(xlWorkSheet.Cells(i + 2, 29), Excel.Range)
                    oXLRange.FormulaLocal = "=SI(C" & i + 2 & "<>""IMPORTADO"";SI(R" & i + 2 & ">S" & i + 2 & ";(R" & i + 2 & "-S" & i + 2 & ");0);0)"
                    'xlWorkSheet.Cells(i + 2, 30) = dgvDetalleLineaProductos.Item("Contrib. Rebate", i).Value.ToString

                    oXLRange = CType(xlWorkSheet.Cells(i + 2, 30), Excel.Range)
                    oXLRange.FormulaLocal = "=SI(C" & i + 2 & "<>""IMPORTADO"";SI(S" & i + 2 & ">T" & i + 2 & ";SI(R" & i + 2 & ">S" & i + 2 & ";(S" & i + 2 & "-T" & i + 2 & ");(R" & i + 2 & "-T" & i + 2 & "));0);0)"
                    'xlWorkSheet.Cells(i + 2, 31) = dgvDetalleLineaProductos.Item("Contrib Rebate MP", i).Value.ToString

                    oXLRange = CType(xlWorkSheet.Cells(i + 2, 31), Excel.Range)
                    oXLRange.FormulaLocal = "=SI(C" & i + 2 & "=""IMPORTADO"";SI(R" & i + 2 & ">S" & i + 2 & ";(R" & i + 2 & "-S" & i + 2 & ");0);0)"
                    xlWorkSheet.Cells(i + 2, 32) = dtdetalle.Rows(i).Item("Precio Cotizacion").ToString
                    xlWorkSheet.Cells(i + 2, 39) = dtdetalle.Rows(i).Item("Estado").ToString
                    xlWorkSheet.Cells(i + 2, 40) = dtdetalle.Rows(i).Item("Pedsto").ToString
                    xlWorkSheet.Cells(i + 2, 41) = dtdetalle.Rows(i).Item("UltCom").ToString
                    xlWorkSheet.Cells(i + 2, 42) = dtdetalle.Rows(i).Item("Cosrep").ToString
                    xlWorkSheet.Cells(i + 2, 43) = dtdetalle.Rows(i).Item("Variacion")
                    xlWorkSheet.Cells(i + 2, 44) = dtdetalle.Rows(i).Item("FecIni Costo Especial")
                    xlWorkSheet.Cells(i + 2, 45) = dtdetalle.Rows(i).Item("FecFin Costo Especial")
                    xlWorkSheet.Cells(i + 2, 46) = dtdetalle.Rows(i).Item("Valorizado Anterior").ToString
                    xlWorkSheet.Cells(i + 2, 47) = dtdetalle.Rows(i).Item("Estado Alternativo").ToString
                    xlWorkSheet.Cells(i + 2, 48) = dtdetalle.Rows(i).Item("Precio Min. Venta").ToString
                    xlWorkSheet.Cells(i + 2, 49) = dtdetalle.Rows(i).Item("Precio Min. Coti").ToString
                    car.ProgressBar1.Value += 1

                    Marshal.ReleaseComObject(xlWorkSheet.Cells)
                Next

                xlWorkSheet = xlWorkBook.Worksheets("Resumen Original")

                'edit the cell with new value
                'xlWorkSheet.Cells(6, 2) = txtRazons.Text
                'xlWorkSheet.Cells(7, 2) = txtRutcli.Text
                'xlWorkSheet.Cells(10, 3) = txtTotIngresiFinal.Text
                'xlWorkSheet.Cells(11, 2) = txtPorContriFinal.Text
                'xlWorkSheet.Cells(11, 3) = txtTotContriFinal.Text

                sql = " select nvl(porc_trans_new,0) transporte, "
                sql = sql & "nvl(porc_venta_new,0) venta, "
                sql = sql & "nvl(porc_gastos_new,0) gastos, "
                sql = sql & "nvl(porc_rebate_new,0) rebate from en_reajuste_convenio  "
                sql = sql & "where num_reajuste= " + reajuste.ToString
                sql = sql & "and codemp=3 and numcot= " + numcot
                Dim dtPorcentajes = bd.sqlSelect(sql)
                If dtPorcentajes.Rows.Count > 0 Then
                    xlWorkSheet.Cells(12, 2) = dtPorcentajes.Rows(0).Item("transporte") / 100
                    xlWorkSheet.Cells(13, 2) = dtPorcentajes.Rows(0).Item("venta") / 100
                    xlWorkSheet.Cells(14, 2) = dtPorcentajes.Rows(0).Item("gastos") / 100
                    xlWorkSheet.Cells(15, 2) = dtPorcentajes.Rows(0).Item("rebate") / 100
                Else
                    xlWorkSheet.Cells(12, 2) = "0,0"
                    xlWorkSheet.Cells(13, 2) = "0,0"
                    xlWorkSheet.Cells(14, 2) = "0,0"
                    xlWorkSheet.Cells(15, 2) = "0,0"
                End If


                'xlWorkSheet.Cells(14, 3) = txtTotGastOpFinal.Text

                'xlWorkSheet.Cells(15, 3) = txtTotRebateFinal.Text
                'xlWorkSheet.Cells(16, 3) = txtTotGastoFinal.Text
                'xlWorkSheet.Cells(17, 3) = txtTotMargenFinal.Text


                xlWorkSheet.Activate()
                Dim pt As Excel.PivotTable
                pt = xlWorkSheet.PivotTables("Tabla dinámica3")
                pt.PivotCache.Refresh()

                pt = xlWorkSheet.PivotTables("Tabla dinámica1")
                pt.PivotCache.Refresh()

                xlApp.ScreenUpdating = True

                car.Close()
                xlApp.DisplayAlerts = False
                'xlWorkBook.Close(SaveChanges:=True)

                xlWorkBook.SaveAs(save.FileName)
                xlWorkBook.Close()

                xlApp.DisplayAlerts = True
                xlApp.Quit()

                'xlWorkSheet.ReleaseComObject(pt)
                Marshal.ReleaseComObject(pt)
                Marshal.ReleaseComObject(xlApp)
                Marshal.ReleaseComObject(xlWorkBook)
                Marshal.ReleaseComObject(xlWorkSheet)

                MessageBox.Show("Listo")

            End If
            Cursor.Current = Cursors.Arrow

            PuntoControl.RegistroUso("10")
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        Finally
            bd.close()
        End Try
    End Sub
End Class

'Private Sub cargacostosespeciales()
'    Using conn = New OracleConnection(Conexion.retornaConexion)
'        conn.Open()
'        Using transaccion = conn.BeginTransaction
'            Try

'                Dim comando As OracleCommand
'                Dim dt As New DataTable
'                Dim dataAdapter As OracleDataAdapter
'                Dim sequen As String
'                Dim Sql = "Set Role Rol_Aplicacion"
'                comando = New OracleCommand(Sql, conn)
'                comando.Transaction = transaccion
'                comando.ExecuteNonQuery()

'                Sql = " Select EN_CLIENTE_COSTO_SEQ.nextval seq from dual "

'                comando = New OracleCommand(Sql, conn)
'                comando.Transaction = transaccion
'                dataAdapter = New OracleDataAdapter(comando)
'                dataAdapter.Fill(dt)
'                If dt.Rows.Count = 0 Then
'                    If Not transaccion.Equals(Nothing) Then
'                        transaccion.Rollback()
'                    End If
'                    MessageBox.Show("NO se encontró una secuencia. Consulte a Sistemas...", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
'                    Exit Sub
'                Else
'                    sequen = dt.Rows(0).Item("seq").ToString
'                End If

'                Dim dtListado As New DataTable
'                Sql = " select a.rutcli,b.codpro,b.precio,b.cosesp,b.mg_comercial,trunc(sysdate) fecini,a.fecfin,1 cantid,'" + Globales.user + "' usuario, "
'                Sql = Sql & "3 codemp,decode(greatest(b.cosesp,getcostopromedio(3,b.codpro)),b.cosesp,2,0 ) tipo,0 codmon  from en_reajuste_convenio a,de_reajuste_convenio b "
'                Sql = Sql & "where a.codemp=b.codemp "
'                Sql = Sql & "and a.num_reajuste=b.num_reajuste "
'                Sql = Sql & "and a.numcot=b.numcot "
'                Sql = Sql & " and a.numcot = " + numcot
'                Sql = Sql & " and a.num_reajuste = " + reajuste
'                Sql = Sql & "and b.cosesp>0 "
'                comando = New OracleCommand(Sql, conn)
'                comando.Transaction = transaccion
'                dataAdapter = New OracleDataAdapter(comando)
'                dataAdapter.Fill(dtListado)

'                Sql = " update en_cliente_costo set fecfin=TO_DATE('" & dtpCosEspIni.Value.AddDays(-1).Date & "','dd/mm/yyyy') "
'                Sql = Sql & " where codemp=3 and (rutcli,codpro) in( "
'                Sql = Sql & " select a.rutcli,b.codpro from en_reajuste_convenio a,de_reajuste_convenio b "
'                Sql = Sql & " where a.codemp=b.codemp "
'                Sql = Sql & " and a.num_reajuste=b.num_reajuste "
'                Sql = Sql & " and a.numcot=b.numcot "
'                Sql = Sql & " and a.numcot = " + numcot
'                Sql = Sql & " and a.num_reajuste = " + reajuste
'                Sql = Sql & " and b.cosesp>0) and trunc(sysdate) between (fecini-1) and (fecfin+1) "
'                comando = New OracleCommand(Sql, conn)
'                comando.Transaction = transaccion
'                comando.ExecuteNonQuery()

'                For i = 0 To dtListado.Rows.Count - 1
'                    Sql = " INSERT INTO en_cliente_costo (ID_NUMERO, RUTCLI, CODPRO, PRECIO, COSTO, MARGEN, FECINI, FECFIN, CANTID, USR_CREAC, CODEMP, TIPO,CODMON ) VALUES( "
'                    Sql = Sql & sequen & ", "
'                    Sql = Sql & dtListado.Rows(i).Item("rutcli").ToString & " , "
'                    Sql = Sql & "'" & dtListado.Rows(i).Item("codpro").ToString & "',"
'                    Sql = Sql & dtListado.Rows(i).Item("precio").ToString & ","
'                    Sql = Sql & dtListado.Rows(i).Item("cosesp").ToString & ", "
'                    Sql = Sql & dtListado.Rows(i).Item("mg_comercial").ToString.Replace(",", ".") & ", "
'                    Sql = Sql & "TO_DATE('" & dtpCosEspIni.Value.Date & "','dd/mm/yyyy'), "
'                    Sql = Sql & "TO_DATE('" & dtpCosEspFin.Value.Date & "','dd/mm/yyyy'), "
'                    Sql = Sql & "1,'"
'                    Sql = Sql & Globales.user & "',"
'                    Sql = Sql & "3,"
'                    Sql = Sql & dtListado.Rows(i).Item("tipo").ToString & " , "
'                    Sql = Sql & "0)"
'                    comando = New OracleCommand(Sql, conn)
'                    comando.Transaction = transaccion
'                    comando.ExecuteNonQuery()
'                Next

'                For x = 0 To dgvEmpRelacion.RowCount - 1
'                    If dgvEmpRelacion.Item("Sel", x).Value = 1 Then
'                        Sql = " Select EN_CLIENTE_COSTO_SEQ.nextval seq from dual "
'                        dt.Clear()
'                        comando = New OracleCommand(Sql, conn)
'                        comando.Transaction = transaccion
'                        dataAdapter = New OracleDataAdapter(comando)
'                        dataAdapter.Fill(dt)
'                        If dt.Rows.Count = 0 Then
'                            If Not transaccion.Equals(Nothing) Then
'                                transaccion.Rollback()
'                            End If
'                            MessageBox.Show("NO se encontró una secuencia. Consulte a Sistemas...", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
'                            Exit Sub
'                        Else
'                            sequen = dt.Rows(0).Item("seq").ToString
'                        End If

'                        dtListado.Clear()
'                        Sql = " select " + dgvEmpRelacion.Item("rutcli", x).Value.ToString + " rutcli,b.codpro,b.precio,b.cosesp,b.mg_comercial,trunc(sysdate) fecini,a.fecfin,1 cantid,'" + Globales.user + "' usuario, "
'                        Sql = Sql & "3 codemp,decode(greatest(b.cosesp,getcostopromedio(3,b.codpro)),b.cosesp,2,0 ) tipo,0 codmon  from en_reajuste_convenio a,de_reajuste_convenio b "
'                        Sql = Sql & " where a.codemp=b.codemp "
'                        Sql = Sql & " and a.num_reajuste=b.num_reajuste "
'                        Sql = Sql & " and a.numcot=b.numcot "
'                        Sql = Sql & " and a.numcot = " + numcot
'                        Sql = Sql & " and a.num_reajuste = " + reajuste
'                        Sql = Sql & " and b.cosesp>0 "
'                        comando = New OracleCommand(Sql, conn)
'                        comando.Transaction = transaccion
'                        dataAdapter = New OracleDataAdapter(comando)
'                        dataAdapter.Fill(dtListado)

'                        Sql = " update en_cliente_costo set fecfin=TO_DATE('" & dtpCosEspIni.Value.AddDays(-1).Date & "','dd/mm/yyyy') "
'                        Sql = Sql & " where codemp=3 and (rutcli,codpro) in( "
'                        Sql = Sql & "select " + dgvEmpRelacion.Item("rutcli", x).Value.ToString + " rutcli,b.codpro "
'                        Sql = Sql & " from en_reajuste_convenio a,de_reajuste_convenio b "
'                        Sql = Sql & " where a.codemp=b.codemp "
'                        Sql = Sql & " And a.num_reajuste=b.num_reajuste "
'                        Sql = Sql & " And a.numcot=b.numcot "
'                        Sql = Sql & " and a.numcot = " + numcot
'                        Sql = Sql & " and a.num_reajuste = " + reajuste
'                        Sql = Sql & "and b.cosesp>0) and trunc(sysdate) between (fecini-1) and (fecfin+1) "
'                        comando = New OracleCommand(Sql, conn)
'                        comando.Transaction = transaccion
'                        comando.ExecuteNonQuery()

'                        For i = 0 To dtListado.Rows.Count - 1
'                            Sql = " INSERT INTO en_cliente_costo (ID_NUMERO, RUTCLI, CODPRO, PRECIO, COSTO, MARGEN, FECINI, FECFIN, CANTID, USR_CREAC, CODEMP, TIPO,CODMON ) VALUES( "
'                            Sql = Sql & sequen & ", "
'                            Sql = Sql & dgvEmpRelacion.Item("rutcli", x).Value.ToString & " , "
'                            Sql = Sql & "'" & dtListado.Rows(i).Item("codpro").ToString & "',"
'                            Sql = Sql & dtListado.Rows(i).Item("precio").ToString & ","
'                            Sql = Sql & dtListado.Rows(i).Item("cosesp").ToString & ", "
'                            Sql = Sql & dtListado.Rows(i).Item("mg_comercial").ToString.Replace(",", ".") & ", "
'                            Sql = Sql & "TO_DATE('" & dtpCosEspIni.Value.Date & "','dd/mm/yyyy'), "
'                            Sql = Sql & "TO_DATE('" & dtpCosEspFin.Value.Date & "','dd/mm/yyyy'), "
'                            Sql = Sql & "1,'"
'                            Sql = Sql & Globales.user & "',"
'                            Sql = Sql & "3,"
'                            Sql = Sql & dtListado.Rows(i).Item("tipo").ToString & " , "
'                            Sql = Sql & "0)"
'                            comando = New OracleCommand(Sql, conn)
'                            comando.Transaction = transaccion
'                            comando.ExecuteNonQuery()
'                        Next
'                    End If
'                Next

'                Sql = " update en_reajuste_convenio set estado=3 "
'                Sql = Sql & " where codemp=3 and num_reajuste= " + reajuste
'                comando = New OracleCommand(Sql, conn)
'                comando.Transaction = transaccion
'                comando.ExecuteNonQuery()

'                transaccion.Commit()

'                MessageBox.Show("Costos Especiales Cargados con éxito", "Finalizado", MessageBoxButtons.OK, MessageBoxIcon.Information)

'            Catch ex As Exception
'                If Not transaccion.Equals(Nothing) Then
'                    transaccion.Rollback()
'                End If
'                MessageBox.Show(ex.ToString, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
'            Finally
'                conn.Close()
'            End Try



'        End Using
'    End Using
'End Sub