﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReporteMasivoConvenio
    Inherits MetroFramework.Forms.MetroForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dgvDatos = New System.Windows.Forms.DataGridView()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnBuscar = New MetroFramework.Controls.MetroButton()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.chbFecha = New MetroFramework.Controls.MetroCheckBox()
        Me.chbCodigo = New MetroFramework.Controls.MetroCheckBox()
        Me.chbLinea = New MetroFramework.Controls.MetroCheckBox()
        Me.chbRut = New MetroFramework.Controls.MetroCheckBox()
        Me.dtpFecha = New MetroFramework.Controls.MetroDateTime()
        Me.txtCodigo = New MetroFramework.Controls.MetroTextBox()
        Me.cmbLinea = New MetroFramework.Controls.MetroComboBox()
        Me.txtRut = New MetroFramework.Controls.MetroTextBox()
        Me.btnExcel = New System.Windows.Forms.Button()
        Me.lbltotal = New System.Windows.Forms.Label()
        Me.MetroProgressBar1 = New MetroFramework.Controls.MetroProgressBar()
        CType(Me.dgvDatos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvDatos
        '
        Me.dgvDatos.AllowUserToAddRows = False
        Me.dgvDatos.AllowUserToDeleteRows = False
        Me.dgvDatos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDatos.Location = New System.Drawing.Point(13, 217)
        Me.dgvDatos.Name = "dgvDatos"
        Me.dgvDatos.ReadOnly = True
        Me.dgvDatos.RowHeadersVisible = False
        Me.dgvDatos.Size = New System.Drawing.Size(1026, 303)
        Me.dgvDatos.TabIndex = 40
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnBuscar)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.chbFecha)
        Me.GroupBox1.Controls.Add(Me.chbCodigo)
        Me.GroupBox1.Controls.Add(Me.chbLinea)
        Me.GroupBox1.Controls.Add(Me.chbRut)
        Me.GroupBox1.Controls.Add(Me.dtpFecha)
        Me.GroupBox1.Controls.Add(Me.txtCodigo)
        Me.GroupBox1.Controls.Add(Me.cmbLinea)
        Me.GroupBox1.Controls.Add(Me.txtRut)
        Me.GroupBox1.Location = New System.Drawing.Point(13, 63)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(577, 148)
        Me.GroupBox1.TabIndex = 41
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Filtros"
        '
        'btnBuscar
        '
        Me.btnBuscar.Location = New System.Drawing.Point(496, 119)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(75, 23)
        Me.btnBuscar.TabIndex = 42
        Me.btnBuscar.Text = "Buscar"
        Me.btnBuscar.UseSelectable = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(46, 119)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(50, 13)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Fecha V."
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(46, 86)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(40, 13)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "Codigo"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(46, 56)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(33, 13)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Linea"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(46, 26)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(24, 13)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Rut"
        '
        'chbFecha
        '
        Me.chbFecha.Location = New System.Drawing.Point(12, 114)
        Me.chbFecha.Name = "chbFecha"
        Me.chbFecha.Size = New System.Drawing.Size(19, 18)
        Me.chbFecha.TabIndex = 7
        Me.chbFecha.UseSelectable = True
        '
        'chbCodigo
        '
        Me.chbCodigo.Location = New System.Drawing.Point(11, 81)
        Me.chbCodigo.Name = "chbCodigo"
        Me.chbCodigo.Size = New System.Drawing.Size(19, 18)
        Me.chbCodigo.TabIndex = 6
        Me.chbCodigo.UseSelectable = True
        '
        'chbLinea
        '
        Me.chbLinea.Location = New System.Drawing.Point(11, 51)
        Me.chbLinea.Name = "chbLinea"
        Me.chbLinea.Size = New System.Drawing.Size(19, 18)
        Me.chbLinea.TabIndex = 5
        Me.chbLinea.UseSelectable = True
        '
        'chbRut
        '
        Me.chbRut.Location = New System.Drawing.Point(11, 21)
        Me.chbRut.Name = "chbRut"
        Me.chbRut.Size = New System.Drawing.Size(19, 18)
        Me.chbRut.TabIndex = 4
        Me.chbRut.UseSelectable = True
        '
        'dtpFecha
        '
        Me.dtpFecha.Location = New System.Drawing.Point(107, 108)
        Me.dtpFecha.MinimumSize = New System.Drawing.Size(0, 29)
        Me.dtpFecha.Name = "dtpFecha"
        Me.dtpFecha.Size = New System.Drawing.Size(215, 29)
        Me.dtpFecha.TabIndex = 3
        '
        'txtCodigo
        '
        '
        '
        '
        Me.txtCodigo.CustomButton.Image = Nothing
        Me.txtCodigo.CustomButton.Location = New System.Drawing.Point(74, 1)
        Me.txtCodigo.CustomButton.Name = ""
        Me.txtCodigo.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.txtCodigo.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txtCodigo.CustomButton.TabIndex = 1
        Me.txtCodigo.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txtCodigo.CustomButton.UseSelectable = True
        Me.txtCodigo.CustomButton.Visible = False
        Me.txtCodigo.DisplayIcon = True
        Me.txtCodigo.Lines = New String() {"Z451245"}
        Me.txtCodigo.Location = New System.Drawing.Point(107, 79)
        Me.txtCodigo.MaxLength = 32767
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtCodigo.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtCodigo.SelectedText = ""
        Me.txtCodigo.SelectionLength = 0
        Me.txtCodigo.SelectionStart = 0
        Me.txtCodigo.ShortcutsEnabled = True
        Me.txtCodigo.Size = New System.Drawing.Size(96, 23)
        Me.txtCodigo.TabIndex = 2
        Me.txtCodigo.Text = "Z451245"
        Me.txtCodigo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCodigo.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txtCodigo.UseSelectable = True
        Me.txtCodigo.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txtCodigo.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'cmbLinea
        '
        Me.cmbLinea.FontSize = MetroFramework.MetroComboBoxSize.Small
        Me.cmbLinea.FormattingEnabled = True
        Me.cmbLinea.ItemHeight = 19
        Me.cmbLinea.Location = New System.Drawing.Point(107, 48)
        Me.cmbLinea.Name = "cmbLinea"
        Me.cmbLinea.Size = New System.Drawing.Size(402, 25)
        Me.cmbLinea.TabIndex = 1
        Me.cmbLinea.UseSelectable = True
        '
        'txtRut
        '
        '
        '
        '
        Me.txtRut.CustomButton.Image = Nothing
        Me.txtRut.CustomButton.Location = New System.Drawing.Point(74, 1)
        Me.txtRut.CustomButton.Name = ""
        Me.txtRut.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.txtRut.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txtRut.CustomButton.TabIndex = 1
        Me.txtRut.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txtRut.CustomButton.UseSelectable = True
        Me.txtRut.CustomButton.Visible = False
        Me.txtRut.DisplayIcon = True
        Me.txtRut.Lines = New String() {" 15.741.524"}
        Me.txtRut.Location = New System.Drawing.Point(107, 19)
        Me.txtRut.MaxLength = 32767
        Me.txtRut.Name = "txtRut"
        Me.txtRut.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtRut.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtRut.SelectedText = ""
        Me.txtRut.SelectionLength = 0
        Me.txtRut.SelectionStart = 0
        Me.txtRut.ShortcutsEnabled = True
        Me.txtRut.Size = New System.Drawing.Size(96, 23)
        Me.txtRut.TabIndex = 0
        Me.txtRut.Text = " 15.741.524"
        Me.txtRut.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtRut.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txtRut.UseSelectable = True
        Me.txtRut.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txtRut.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'btnExcel
        '
        Me.btnExcel.Image = Global.Convenios_New.My.Resources.Resources.Excel
        Me.btnExcel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnExcel.Location = New System.Drawing.Point(991, 159)
        Me.btnExcel.Name = "btnExcel"
        Me.btnExcel.Size = New System.Drawing.Size(48, 52)
        Me.btnExcel.TabIndex = 85
        Me.btnExcel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnExcel.UseVisualStyleBackColor = True
        '
        'lbltotal
        '
        Me.lbltotal.AutoSize = True
        Me.lbltotal.Location = New System.Drawing.Point(967, 523)
        Me.lbltotal.Name = "lbltotal"
        Me.lbltotal.Size = New System.Drawing.Size(37, 13)
        Me.lbltotal.TabIndex = 86
        Me.lbltotal.Text = "Total: "
        '
        'MetroProgressBar1
        '
        Me.MetroProgressBar1.Location = New System.Drawing.Point(597, 72)
        Me.MetroProgressBar1.Name = "MetroProgressBar1"
        Me.MetroProgressBar1.Size = New System.Drawing.Size(429, 23)
        Me.MetroProgressBar1.TabIndex = 87
        Me.MetroProgressBar1.Visible = False
        '
        'frmReporteMasivoConvenio
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1048, 553)
        Me.Controls.Add(Me.MetroProgressBar1)
        Me.Controls.Add(Me.lbltotal)
        Me.Controls.Add(Me.btnExcel)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.dgvDatos)
        Me.Name = "frmReporteMasivoConvenio"
        Me.Text = "Reporte Convenios"
        CType(Me.dgvDatos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents dgvDatos As DataGridView
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents chbFecha As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents chbCodigo As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents chbLinea As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents chbRut As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents dtpFecha As MetroFramework.Controls.MetroDateTime
    Friend WithEvents txtCodigo As MetroFramework.Controls.MetroTextBox
    Friend WithEvents cmbLinea As MetroFramework.Controls.MetroComboBox
    Friend WithEvents txtRut As MetroFramework.Controls.MetroTextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents btnBuscar As MetroFramework.Controls.MetroButton
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents btnExcel As Button
    Friend WithEvents lbltotal As Label
    Friend WithEvents MetroProgressBar1 As MetroFramework.Controls.MetroProgressBar
End Class
