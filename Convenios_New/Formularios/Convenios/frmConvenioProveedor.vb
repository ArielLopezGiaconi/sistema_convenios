﻿Imports System.IO
Imports System.Net.Mail

Public Class frmConvenioProveedor
    Public numcot As Long
    Public PuntoControl As New PuntoControl

    Public Sub New(numcot As Long)
        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        Me.numcot = numcot
        Dim datos As New Conexion
        datos.open_dimerc()
        Dim datosConvenio = datos.sqlSelect("SELECT A.RUTCLI, GET_EMPRELA(A.RUTCLI, 1) NUMREL,
        GET_CLIENTE(3,A.RUTCLI) RAZONS, NUMCOT,
        GET_COMCIUREG(GET_DIRDES(A.RUTCLI,B.DIRDES),'R','D') REGION_DESPACHO
        FROM EN_COTIZAC A, DE_CLIENTE B
        WHERE A.CODEMP = B.CODEMP
        AND A.RUTCLI = B.RUTCLI
        AND A.CENCOS = B.CENCOS
        AND A.NUMCOT = " & Me.numcot)

        lblRutCli.Text = datosConvenio(0)("RUTCLI")
        lblNumRel.Text = datosConvenio(0)("NUMREL")
        lblRazons.Text = datosConvenio(0)("RAZONS")
        lblNumCot.Text = Me.numcot
        lblRegionDespacho.Text = datosConvenio(0)("REGION_DESPACHO")
        lblVigencia.Text = "12 Meses"

        cmbAlternativas.SelectedIndex = 1
        cmbDispensadores.SelectedIndex = 1

        datos.open_dimerc()
        Dim prodsConvenio = datos.sqlSelect("select b.codpro, c.coprpv codproveedor, sap_get_descripcion(b.codpro) descripcion_dimerc,
                                            sap_get_marca(b.codpro) marca, b.cantid promedio_convenio, GET_COSTO_ESPECIAL(B.CODPRO, A.RUTCLI, 'P') costocte,
                                            GET_COSTO_ESPECIAL(B.CODPRO, A.RUTCLI, 'FF') fecfin, round(nvl(e.cantid,0)/6) promedio_real
                                            from EN_COTIZAC a, de_cotizac b, re_provpro c,
                                            (select e.codemp, e.rutcli, e.codpro, sum(e.cantid) cantid
                                            from
                                            (
                                             select y.codemp,y.rutcli, y.cencos, z.codpro, z.cantid
                                             from en_factura y, de_factura z
                                             where y.codemp = z.codemp
                                             and y.numfac = z.numfac
                                             and y.fecemi between trunc(add_months(sysdate,-6),'MM')
                                             and sysdate
                                             and y.rutcli = " & lblRutCli.Text & "
                                             union all
                                             select y.codemp,y.rutcli, y.cencos, z.codpro, z.cantid
                                             from en_guiades y, de_guiades z
                                             where y.codemp = z.codemp
                                             and y.numgui = z.numgui
                                             and y.fecemi between trunc(add_months(sysdate,-6),'MM')
                                             and sysdate
                                             and y.rutcli = " & lblRutCli.Text & ") e
                                             group by e.rutcli, e.codpro, e.codemp) e--, en_factura e
                                            where a.codemp = b.codemp
                                            and a.numcot = b.numcot
                                            and b.codpro = c.codpro
                                            and b.codemp = c.codemp
                                            and b.codpro = e.codpro(+)
                                            and c.priori = 1
                                            and a.numcot = " & lblNumCot.Text & "
                                            and getcodlinea(b.codpro) in (32,43)")

        For Each fila As DataRow In prodsConvenio.Rows
            dgvProductos.Rows.Add(fila("codpro"), fila("codproveedor"), fila("descripcion_dimerc"),
                                  fila("marca"), fila("promedio_convenio"), fila("promedio_real"), fila("costocte"), fila("fecfin"))
        Next

    End Sub

    Private Function generarExcel(Optional visible As Boolean = True) As String
        Try
            Dim save As New SaveFileDialog
            Dim aux As Long
            save.Filter = "Archivo Excel | *.xlsx"
            save.InitialDirectory = "C:\dimerc"
            save.FileName = "Propuesta proveedor " & lblRutCli.Text & ".xlsx"
            'xlWorkBook.SaveAs("C:\dimerc\propuesta " & txtRazons.Text & ".xlsx", Excel.XlFileFormat.xlWorkbookNormal)



            If save.ShowDialog = System.Windows.Forms.DialogResult.OK Then

                Dim car As New frmCargando
                car.ProgressBar1.Minimum = 0
                car.ProgressBar1.Value = 0
                car.ProgressBar1.Maximum = dgvProductos.RowCount
                car.StartPosition = FormStartPosition.CenterScreen
                car.Show()
                Cursor.Current = Cursors.WaitCursor
                Dim app As Microsoft.Office.Interop.Excel._Application = New Microsoft.Office.Interop.Excel.Application()
                Dim workbook As Microsoft.Office.Interop.Excel._Workbook = app.Workbooks.Add(Type.Missing)
                Dim worksheet As Microsoft.Office.Interop.Excel._Worksheet = Nothing
                worksheet = workbook.Sheets("Hoja1")
                worksheet = workbook.ActiveSheet

                Dim datos As New Conexion
                datos.open_dimerc()

                worksheet.Cells(1, 1) = "RUT"
                worksheet.Cells(1, 2) = lblRutCli.Text
                'ruts relacionados
                Dim tblRutRelacionados = datos.sqlSelect("SELECT RUTCLI FROM RE_EMPRELA
                                      WHERE NUMREL = " & lblNumRel.Text & "
                                      AND RUTCLI <> " & lblRutCli.Text)

                If tblRutRelacionados.Rows.Count > 0 Then
                    For i = 0 To tblRutRelacionados.Rows.Count - 1
                        worksheet.Cells(1, i + 3) = tblRutRelacionados.Rows(i)(0).ToString()
                    Next
                End If

                worksheet.Cells(2, 1) = "NUMERO DE RELACION"
                worksheet.Cells(2, 2) = lblNumRel.Text
                worksheet.Cells(3, 1) = "RAZON SOCIAL"
                worksheet.Cells(3, 2) = lblRazons.Text
                worksheet.Cells(4, 1) = "NUMERO DE LA COTIZACIÓN DEL CONVENIO"
                worksheet.Cells(4, 2) = lblNumCot.Text
                worksheet.Cells(5, 1) = "REGION DE DESPACHO"
                worksheet.Cells(5, 2) = lblRegionDespacho.Text
                worksheet.Cells(6, 1) = "VIGENCIA DE PRECIOS"
                worksheet.Cells(6, 2) = lblVigencia.Text
                worksheet.Cells(7, 1) = "FECHA INICIO / FECHA DE TERMINO"
                worksheet.Cells(8, 1) = "TIPO DE VENTA: CONVENIO"
                worksheet.Cells(9, 1) = "MONTO CONSUMO MENSUAL ($)"
                worksheet.Cells(10, 1) = "PROVEEDOR ACTUAL"
                worksheet.Cells(11, 1) = "MARCA ACTUAL"
                worksheet.Cells(12, 1) = "ACEPTA ALTERNATIVAS"
                worksheet.Cells(12, 2) = cmbAlternativas.Text
                worksheet.Cells(13, 1) = "REQUIERE DISPENSADORES [SI/NO]
COLOCAR CANTIDAD EN OBSERVACIONES"
                worksheet.Cells(13, 2) = cmbDispensadores.Text
                worksheet.Cells(14, 1) = cmbDispensadores.Text
                worksheet.Cells(14, 2) = txtObserv.Text

                'Aca se agregan las cabeceras de nuestro datagrid.
                For i As Integer = 1 To dgvProductos.Columns.Count
                    worksheet.Cells(16, i) = dgvProductos.Columns(i - 1).HeaderText
                Next

                'Aca se ingresa el detalle recorrera la tabla celda por celda
                For I = 0 To dgvProductos.Rows.Count - 2
                    For j = 0 To dgvProductos.Columns.Count - 1
                        aux = j
                        If dgvProductos.Rows(I).Cells(j).Value.ToString Is Nothing Then
                            worksheet.Cells(I + 17, j + 1) = "sin dato"
                        Else
                            worksheet.Cells(I + 17, j + 1) = dgvProductos.Rows(I).Cells(j).Value.ToString
                        End If
                    Next
                    car.ProgressBar1.Value += 1
                Next

                'Aca le damos el formato a nuestro excel
                worksheet.SaveAs(Path.Combine(save.InitialDirectory, save.FileName))
                worksheet.Rows.Item(1).Font.Bold = 1
                worksheet.Rows.Item(1).HorizontalAlignment = 3
                worksheet.Columns.AutoFit()
                worksheet.Columns.HorizontalAlignment = 2

                app.Visible = visible
                workbook.Close(0)
                app.Quit()
                app = Nothing
                workbook = Nothing
                worksheet = Nothing
                FileClose(1)
                GC.Collect()
                car.Close()
                Cursor.Current = Cursors.Arrow
                Return Path.Combine(save.InitialDirectory, save.FileName)
            End If

        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return ""
        End Try
    End Function

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        generarExcel()
        PuntoControl.RegistroUso("29")
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles btnEnviar.Click
        If txtMail.Text.Length > 0 Then
            Dim ruta = generarExcel(False)
            Try
                PuntoControl.RegistroUso("30")

                Dim cadenaCorreoPara = ""
                Dim cadenaCorreoCC = ""
                If txtMail.Text = "" Then
                    MessageBox.Show("No se enviara correo, no hay destinatarios ingresados", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Exit Sub
                End If

                Dim correo = "convenios@dimerc-info.cl"
                Dim contraseña = "D1m3rc.2019"
                Dim Smtp_Server As New SmtpClient
                Dim e_mail As New MailMessage()
                Smtp_Server.UseDefaultCredentials = False
                Smtp_Server.Credentials = New Net.NetworkCredential(correo, contraseña)
                Smtp_Server.Host = "mail.gtdinternet.com" 'mail.dimerc.cl"

                e_mail = New MailMessage()
                e_mail.From = New MailAddress(correo)
                If txtMail.Text <> "" Then
                    e_mail.To.Add(txtMail.Text)
                End If
                If txtCC.Text <> "" Then
                    e_mail.CC.Add(txtCC.Text)
                End If

                e_mail.Subject = "Propuesta de Costos Especiales " + lblRazons.Text
                e_mail.IsBodyHtml = False
                Dim mensaje As String = "Estimado proveedor: adjuntamos solicitud de costos especiales para productos tissue." & vbCrLf
                mensaje += "Quedamos atentos a sus comentarios," + vbCrLf + vbCrLf + vbCrLf '"</br></br></br> "
                mensaje += "Saluda atentamente," + vbCrLf + vbCrLf
                mensaje += "Equipo de Convenios y Licitaciones Dimerc."
                e_mail.Body = mensaje
                Dim archivoAdjunto As New System.Net.Mail.Attachment(ruta)
                e_mail.Attachments.Add(archivoAdjunto)
                Smtp_Server.Send(e_mail)

                MessageBox.Show("Correo enviado", "Finalizado", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Catch ex As Exception
                PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Try
        End If
    End Sub

    Private Sub frmConvenioProveedor_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        PuntoControl.RegistroUso("28")
    End Sub
End Class