﻿Imports System.Data.OracleClient
Public Class frmCopiaConvenios
    Dim PuntoControl As New PuntoControl
    Private Sub txtNumCot_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNumCot.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            PuntoControl.RegistroUso("31")
            Dim datos As New Conexion
            Dim Sql As String
            Dim tablazo As DataTable
            If txtNumCot.Text <> "" Then
                Sql = "Select x.razons, x.rutcli,y.cencos, y.descco, a.totnet, count(*) itms from en_cliente x, "
                Sql = Sql & " de_cliente y, en_conveni a, de_conveni b  where "
                Sql = Sql & "     a.numcot = " & txtNumCot.Text
                Sql = Sql & " and a.codemp = " & 3
                Sql = Sql & " and trunc(a.fecven) >= trunc(sysdate) "
                Sql = Sql & " and x.codemp = a.codemp "
                Sql = Sql & " and x.rutcli = a.rutcli "
                Sql = Sql & " and y.rutcli = a.rutcli "
                Sql = Sql & " and y.cencos = a.cencos "
                Sql = Sql & " and y.codemp = a.codemp "
                Sql = Sql & " and b.numcot = a.numcot "
                Sql = Sql & " and b.codemp = a.codemp "
                Sql = Sql & " group by x.razons, x.rutcli,y.cencos, y.descco, a.totnet "
                datos.open_dimerc()
                tablazo = datos.sqlSelect(Sql)
                If tablazo.Rows.Count > 0 Then
                    lblTotNet.Text = Format(CDbl(tablazo.Rows(0)("totnet")), "###,###,##0")
                    lblItems.Text = CStr(tablazo.Rows(0)("itms"))
                Else
                    MsgBox("Convenio no encontrado o esta vencido")
                End If
            End If
        End If
    End Sub

    Private Sub txtRutCli_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtRutCli.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If txtRutCli.Text <> "" Then
                Dim datos As New Conexion
                Dim sql = "Select razons from en_cliente
                           where codemp = 3
                           and rutcli = " & txtRutCli.Text
                datos.open_dimerc()
                Dim tablazo = datos.sqlSelect(sql)
                If tablazo.Rows.Count = 0 Then
                    MessageBox.Show("Cliente no existe.", "Atención.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Exit Sub
                End If
                lblRazons.Text = tablazo.Rows(0)(0).ToString()
                txtCencos.Text = ""
                txtCencos.Select()
            End If
        End If
    End Sub

    Private Sub txtCencos_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCencos.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If txtCencos.Text <> "" Then
                Dim datos As New Conexion
                Dim sql = "Select descco from de_cliente
                           where codemp = 3
                           and rutcli = " & txtRutCli.Text &
                           " and cencos = " & txtCencos.Text
                datos.open_dimerc()
                Dim tablazo = datos.sqlSelect(sql)
                lblDesCco.Text = ""
                lblDesCco.Text = tablazo.Rows(0)(0).ToString()
                txtCencos.Select()
            End If
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Button1.Enabled = False
        ProgressBar1.Visible = True
        ProgressBar1.Maximum = 8
        ProgressBar1.Value = 0
        Application.DoEvents()
        PuntoControl.RegistroUso("33")
        If txtNumCot.Text <> "" And lblTotNet.Text <> "" And txtRutCli.Text <> "" And txtCencos.Text <> "" Then
            Dim sql = "select cotiza_seq.nextval numcot from dual"
            Dim datos As New Conexion
            datos.open_dimerc()
            Dim dt As New DataTable
            Dim secuencia As String
            Dim numcot = datos.sqlSelectUnico(sql, "numcot")
            datos.open_dimerc()
            sql = "SELECT nvl(numcot,0) numcot from en_conveni where rutcli = " & txtRutCli.Text
            Dim tablaConvActual = datos.sqlSelect(sql)
            ProgressBar1.Value = 1
            Application.DoEvents()
            Using conn = New OracleConnection()
                Dim conexString = Conexion.conn_dimerc.Replace("dm_ventas", Conexion.username)
                conexString = conexString.Replace("dimerc", Conexion.password)
                conn.ConnectionString = conexString
                conn.Open()
                Using transaccion = conn.BeginTransaction
                    Try
                        Dim comando As New OracleCommand("SET ROLE ROL_APLICACION", conn)
                        comando.Transaction = transaccion
                        comando.ExecuteNonQuery()
                        'inserto cotizac
                        sql = "Insert into en_cotizac (NUMCOT, RUTCLI, CENCOS, FECEMI, FECVEN, CODVEN, CODCNL, SUBTOT, TOTNET, TOTIVA, TOTGEN, MARGEN, DESCLI, DESFIN, FORPAG, PLAPAG, CODEST, FECEDO, CODUSU, PLANTI, OBSERV, CODMON, CAMBIO, CONVEN, CODDIR, MARDES, VALMAR, TICONV, TIPCOT, USR_CREAC, USRCOMP, MESES, TIPCAR, CODBOD, PARIDAD, NEGOCIO, SUBNEGOCIO, CLASIF_CLIE, SEGMENTO, TIPCON, CODEMP)
                                 SELECT " & numcot & ",
                                 " & txtRutCli.Text & "," & txtCencos.Text & ",
                                 FECEMI, FECVEN, sap_get_rutusu('" & Conexion.username & "'),4,
                                 SUBTOT,TOTNET, TOTIVA, TOTGEN, MARGEN, DESCLI, DESFIN, 
                                 forpag,plapag, CODEST, FECEDO, CODUSU, PLANTI, null, CODMON, CAMBIO, CONVEN, CODDIR, MARDES, VALMAR, TICONV, TIPCOT, USR_CREAC, USRCOMP, MESES, TIPCAR, CODBOD, PARIDAD, NEGOCIO, SUBNEGOCIO, CLASIF_CLIE, get_segmento_cliente(3," & txtRutCli.Text & ") , TIPCON, 3
                                 from  en_cotizac where numcot = " & txtNumCot.Text
                        comando.CommandText = sql
                        comando.ExecuteNonQuery()
                        ProgressBar1.Value = 2
                        Application.DoEvents()

                        sql = "insert into de_cotizac(numcot,sequen,codpro,cantid,precio,costos,totnet,pordes,mondes,margen,mardes,codlis,codlin,oferta,prelis,estpre,costocte)
                                 select " & numcot & ", SEQUEN, CODPRO, CANTID, PRECIO, COSTOS, TOTNET, PORDES, MONDES, MARGEN, MARDES, CODLIS, CODLIN, OFERTA, PRELIS, ESTPRE, COSTOCTE from
                                 de_cotizac where numcot = " & txtNumCot.Text
                        comando.CommandText = sql
                        comando.ExecuteNonQuery()

                        ProgressBar1.Value = 3
                        Application.DoEvents()
                        If tablaConvActual.Rows.Count > 0 Then
                            sql = "delete from de_conveni where numcot = " & tablaConvActual.Rows(0)(0).ToString()
                            comando.CommandText = sql
                            comando.ExecuteNonQuery()

                            sql = "delete from en_conveni where rutcli = " & txtRutCli.Text
                            comando.CommandText = sql
                            comando.ExecuteNonQuery()
                        End If

                        ProgressBar1.Value = 4
                        Application.DoEvents()

                        'sql = "insert into en_conveni_hst select * from en_cotizac where numcot = " & numcot
                        'comando.CommandText = sql
                        'comando.ExecuteNonQuery()

                        sql = "Update en_conveni_hst set fecedo = sysdate  where numcot = " & numcot
                        comando.CommandText = sql
                        comando.ExecuteNonQuery()

                        ProgressBar1.Value = 5
                        Application.DoEvents()

                        sql = "insert into en_conveni select * from en_cotizac where numcot = " & numcot
                        comando.CommandText = sql
                        comando.ExecuteNonQuery()

                        ProgressBar1.Value = 6
                        Application.DoEvents()

                        sql = "insert into de_conveni(NUMCOT, SEQUEN, CODPRO, CANTID, PRECIO, COSTOS, TOTNET, PORDES, MONDES, MARGEN, MARDES, ESTPRE, CODEMP, FECEMI, FECVEN) select 
                                 " & numcot & ",
                                 SEQUEN,
                                 CODPRO,
                                 1,
                                 PRECIO,
                                 COSTOS,
                                 ROUND(PRECIO-(PRECIO*PORDES)/100,0),
                                 PORDES,
                                 MONDES,
                                 Margen,
                                 MARDES,
                                 ESTPRE,
                                 3 CODEMP,
                                 FECEMI,
                                 FECVEN
                                 from DE_cotizac where numcot = " & txtNumCot.Text
                        comando.CommandText = sql
                        comando.ExecuteNonQuery()

                        ProgressBar1.Value = 7
                        Application.DoEvents()

                        sql = "SELECT EN_CLIENTE_COSTO_SEQ.nextval seq from dual"
                        dt = datos.sqlSelect(sql)

                        secuencia = dt.Rows(0).Item("seq")

                        sql = "SELECT RUTCLI from EN_cotizac 
                               WHERE NUMCOT = " & txtNumCot.Text
                        dt = datos.sqlSelect(sql)

                        sql = "insert into en_cliente_costo
                               select " & secuencia & " ID_NUMERO,
                                   " & txtRutCli.Text & " RUTCLI,
                                   B.CODPRO,
                                   B.PRECIO,
                                   B.COSTO,
                                   B.MARGEN,
                                   TO_DATE('" & Date.Now.ToShortDateString & " 00:00:00', 'DD/MM/YYYY HH24:MI:SS') FECINI,
                                   B.FECFIN,
                                   B.CANTID,
                                   B.USR_CREAC,
                                   B.CODEMP,
                                   B.TIPO,
                                   B.CODMON,
                                   B.FECHA_CREAC,
                                   B.FECHA_MODIF,
                                   B.ESTADO from de_conveni A, EN_CLIENTE_COSTO B 
                            WHERE A.NUMCOT = " & txtNumCot.Text & "
                              AND A.CODPRO = B.CODPRO
                              AND B.ESTADO = 'V'
                              AND B.RUTCLI = " & dt.Rows(0).Item("RUTCLI") & "
                            GROUP BY B.CODPRO, B.PRECIO,
                                   B.COSTO, B.MARGEN, B.FECFIN,
                                   B.CANTID, B.USR_CREAC,
                                   B.CODEMP, B.TIPO,
                                   B.CODMON, B.FECHA_CREAC,
                                   B.FECHA_MODIF, B.ESTADO"
                        comando.CommandText = sql
                        comando.ExecuteNonQuery()

                        transaccion.Commit()
                        MsgBox("Convenio copiado, EL NUEVO CONVENIO ES: " & numcot)

                        ProgressBar1.Value = 8
                        Application.DoEvents()

                    Catch ex As Exception
                        PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                        MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        transaccion.Rollback()
                    Finally
                        Button1.Enabled = True
                        ProgressBar1.Visible = False
                        conn.Close()
                    End Try
                End Using
            End Using
        End If
    End Sub

    Private Sub txtCoti_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCoti.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            Dim datos As New Conexion
            Dim Sql As String
            Dim tablazo As DataTable
            PuntoControl.RegistroUso("34")
            If txtCoti.Text <> "" Then

                Sql = "select numcot, rutcli, fecemi, fecven, totgen from en_cotizac
                        where numcot = " & txtCoti.Text
                datos.open_dimerc()
                tablazo = datos.sqlSelect(Sql)
                If tablazo.Rows.Count > 0 Then
                    txtRut.Text = tablazo.Rows(0).Item("rutcli")
                    dtpFechaInicio.Value = Date.Parse(tablazo.Rows(0).Item("fecemi"))
                    dtpFechaVencimiento.Value = Date.Parse(tablazo.Rows(0).Item("fecven"))
                    dtpFechaModificar.Value = Date.Parse(tablazo.Rows(0).Item("fecven"))
                Else
                    MsgBox("Cotización no encontrada")
                End If
            End If
        End If
    End Sub

    Private Sub MetroButton1_Click(sender As Object, e As EventArgs) Handles MetroButton1.Click
        MetroButton1.Enabled = False
        ProgressBar1.Visible = True
        ProgressBar1.Maximum = 4
        ProgressBar1.Value = 0
        Application.DoEvents()
        PuntoControl.RegistroUso("35")
        Using conn = New OracleConnection()
            Dim conexString = Conexion.conn_dimerc.Replace("dm_ventas", Conexion.username)
            Dim sql As String
            conexString = conexString.Replace("dimerc", Conexion.password)
            conn.ConnectionString = conexString
            conn.Open()
            Using transaccion = conn.BeginTransaction
                Try
                    Dim comando As New OracleCommand("SET ROLE ROL_APLICACION", conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    ProgressBar1.Value = 1
                    Application.DoEvents()

                    If txtRut.Text.Length > 0 Then
                        sql = "update en_cotizac set fecven = 
                           to_date('" & dtpFechaModificar.Value.ToShortDateString & " 00:00:00','dd/mm/yyyy HH24:mi:ss') 
                           where numcot = " & txtCoti.Text
                        comando.CommandText = sql
                        comando.ExecuteNonQuery()

                        ProgressBar1.Value = 2
                        Application.DoEvents()

                        sql = "update en_cliente_costo set fecini = 
                           to_date('" & dtpFechaModificar.Value.ToShortDateString & " 00:00:00','dd/mm/yyyy HH24:mi:ss') 
                           where rutcli = " & txtRut.Text &
                           " AND fecini <= to_Date('" & dtpFechaInicio.Value.ToShortDateString & " 00:00:00', 'dd/mm/yyyy HH24:mi:ss')
                             AND fecfin >= to_Date('" & dtpFechaVencimiento.Value.ToShortDateString & " 00:00:00', 'dd/mm/yyyy HH24:mi:ss')"
                        comando.CommandText = sql
                        comando.ExecuteNonQuery()

                        ProgressBar1.Value = 3
                        Application.DoEvents()

                        transaccion.Commit()

                        txtRut.Text = ""
                        dtpFechaModificar.Value = Date.Now
                        dtpFechaInicio.Value = Date.Now
                        dtpFechaVencimiento.Value = Date.Now

                        MessageBox.Show("Cotización extendida con exito.", "Infomación.", MessageBoxButtons.OK, MessageBoxIcon.Information)

                        ProgressBar1.Value = 4
                        Application.DoEvents()

                    End If
                Catch ex As Exception
                    transaccion.Rollback()
                    PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                    MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Finally
                    conn.Close()
                    MetroButton1.Enabled = True
                    ProgressBar1.Visible = False
                End Try
            End Using
        End Using
    End Sub

    Private Sub frmCopiaConvenios_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If Globales.tema = 1 Then
            Me.Theme = 2
            Globales.tema = 2
        Else
            Me.Theme = 1
            Globales.tema = 1
        End If
        PuntoControl.RegistroUso("32")
    End Sub
End Class