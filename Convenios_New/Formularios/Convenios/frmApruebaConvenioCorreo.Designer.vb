﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmApruebaConvenioCorreo
    Inherits MetroFramework.Forms.MetroForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btn_Limpiar = New System.Windows.Forms.Button()
        Me.btn_Salir = New System.Windows.Forms.Button()
        Me.tabReajuste = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.btn_Buscar = New System.Windows.Forms.Button()
        Me.cmbUsuario = New System.Windows.Forms.ComboBox()
        Me.btn_ApruebaConvenio = New System.Windows.Forms.Button()
        Me.btn_CambiaPagina = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtRazons = New System.Windows.Forms.TextBox()
        Me.txtRelCom = New System.Windows.Forms.TextBox()
        Me.txtRutcli = New System.Windows.Forms.TextBox()
        Me.dgvReajuste = New System.Windows.Forms.DataGridView()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.btn_CargaConvenio = New System.Windows.Forms.Button()
        Me.btn_Avanza = New System.Windows.Forms.Button()
        Me.btn_Devuelve2 = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.chkMarcaTodoConvenio = New System.Windows.Forms.CheckBox()
        Me.btn_Grabar = New System.Windows.Forms.Button()
        Me.dgvRelComercial = New System.Windows.Forms.DataGridView()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cmbTipoConvenio = New System.Windows.Forms.ComboBox()
        Me.dgvLineasReajuste = New System.Windows.Forms.DataGridView()
        Me.Label84 = New System.Windows.Forms.Label()
        Me.dtpFecVenConvenio = New System.Windows.Forms.DateTimePicker()
        Me.dtpFechaLineaReajuste = New System.Windows.Forms.DateTimePicker()
        Me.btn_AgregaFecha = New System.Windows.Forms.Button()
        Me.Label73 = New System.Windows.Forms.Label()
        Me.Label83 = New System.Windows.Forms.Label()
        Me.Label86 = New System.Windows.Forms.Label()
        Me.dtpFeciniConvenio = New System.Windows.Forms.DateTimePicker()
        Me.chkLineas = New System.Windows.Forms.CheckBox()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.grbBotones = New System.Windows.Forms.GroupBox()
        Me.btn_Devuelve = New System.Windows.Forms.Button()
        Me.txtInfoExcel = New System.Windows.Forms.TextBox()
        Me.Label75 = New System.Windows.Forms.Label()
        Me.btn_Base = New System.Windows.Forms.Button()
        Me.btn_Correo = New System.Windows.Forms.Button()
        Me.Label70 = New System.Windows.Forms.Label()
        Me.Label69 = New System.Windows.Forms.Label()
        Me.txtCC3 = New System.Windows.Forms.TextBox()
        Me.txtCC2 = New System.Windows.Forms.TextBox()
        Me.txtCC1 = New System.Windows.Forms.TextBox()
        Me.txtMail3 = New System.Windows.Forms.TextBox()
        Me.txtMail2 = New System.Windows.Forms.TextBox()
        Me.txtMail1 = New System.Windows.Forms.TextBox()
        Me.Panel1.SuspendLayout()
        Me.tabReajuste.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.dgvReajuste, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvRelComercial, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvLineasReajuste, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        Me.grbBotones.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.btn_Limpiar)
        Me.Panel1.Controls.Add(Me.btn_Salir)
        Me.Panel1.Location = New System.Drawing.Point(10, 11)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1041, 52)
        Me.Panel1.TabIndex = 0
        '
        'btn_Limpiar
        '
        Me.btn_Limpiar.Image = Global.Convenios_New.My.Resources.Resources.limpiar
        Me.btn_Limpiar.Location = New System.Drawing.Point(6, 3)
        Me.btn_Limpiar.Name = "btn_Limpiar"
        Me.btn_Limpiar.Size = New System.Drawing.Size(47, 43)
        Me.btn_Limpiar.TabIndex = 0
        Me.btn_Limpiar.UseVisualStyleBackColor = True
        '
        'btn_Salir
        '
        Me.btn_Salir.Image = Global.Convenios_New.My.Resources.Resources.salir
        Me.btn_Salir.Location = New System.Drawing.Point(989, 3)
        Me.btn_Salir.Name = "btn_Salir"
        Me.btn_Salir.Size = New System.Drawing.Size(47, 43)
        Me.btn_Salir.TabIndex = 0
        Me.btn_Salir.UseVisualStyleBackColor = True
        '
        'tabReajuste
        '
        Me.tabReajuste.Controls.Add(Me.TabPage1)
        Me.tabReajuste.Controls.Add(Me.TabPage3)
        Me.tabReajuste.Controls.Add(Me.TabPage2)
        Me.tabReajuste.Location = New System.Drawing.Point(13, 69)
        Me.tabReajuste.Name = "tabReajuste"
        Me.tabReajuste.SelectedIndex = 0
        Me.tabReajuste.Size = New System.Drawing.Size(1087, 417)
        Me.tabReajuste.TabIndex = 26
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.SystemColors.Control
        Me.TabPage1.Controls.Add(Me.btn_Buscar)
        Me.TabPage1.Controls.Add(Me.cmbUsuario)
        Me.TabPage1.Controls.Add(Me.btn_ApruebaConvenio)
        Me.TabPage1.Controls.Add(Me.btn_CambiaPagina)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.Label4)
        Me.TabPage1.Controls.Add(Me.Label3)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Controls.Add(Me.txtRazons)
        Me.TabPage1.Controls.Add(Me.txtRelCom)
        Me.TabPage1.Controls.Add(Me.txtRutcli)
        Me.TabPage1.Controls.Add(Me.dgvReajuste)
        Me.TabPage1.Location = New System.Drawing.Point(4, 23)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1079, 390)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Consulta"
        '
        'btn_Buscar
        '
        Me.btn_Buscar.Image = Global.Convenios_New.My.Resources.Resources.search
        Me.btn_Buscar.Location = New System.Drawing.Point(704, 14)
        Me.btn_Buscar.Name = "btn_Buscar"
        Me.btn_Buscar.Size = New System.Drawing.Size(46, 44)
        Me.btn_Buscar.TabIndex = 35
        Me.btn_Buscar.UseVisualStyleBackColor = True
        '
        'cmbUsuario
        '
        Me.cmbUsuario.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbUsuario.FormattingEnabled = True
        Me.cmbUsuario.Location = New System.Drawing.Point(491, 35)
        Me.cmbUsuario.Name = "cmbUsuario"
        Me.cmbUsuario.Size = New System.Drawing.Size(207, 22)
        Me.cmbUsuario.TabIndex = 34
        '
        'btn_ApruebaConvenio
        '
        Me.btn_ApruebaConvenio.Image = Global.Convenios_New.My.Resources.Resources.Tips
        Me.btn_ApruebaConvenio.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_ApruebaConvenio.Location = New System.Drawing.Point(756, 14)
        Me.btn_ApruebaConvenio.Name = "btn_ApruebaConvenio"
        Me.btn_ApruebaConvenio.Size = New System.Drawing.Size(153, 43)
        Me.btn_ApruebaConvenio.TabIndex = 33
        Me.btn_ApruebaConvenio.Text = "Aprobar Convenios"
        Me.btn_ApruebaConvenio.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_ApruebaConvenio.UseVisualStyleBackColor = True
        '
        'btn_CambiaPagina
        '
        Me.btn_CambiaPagina.Image = Global.Convenios_New.My.Resources.Resources.flecha_derecha
        Me.btn_CambiaPagina.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_CambiaPagina.Location = New System.Drawing.Point(915, 13)
        Me.btn_CambiaPagina.Name = "btn_CambiaPagina"
        Me.btn_CambiaPagina.Size = New System.Drawing.Size(156, 44)
        Me.btn_CambiaPagina.TabIndex = 32
        Me.btn_CambiaPagina.Text = "Cargar Convenio Inmediatamante"
        Me.btn_CambiaPagina.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_CambiaPagina.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(3, 39)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(73, 14)
        Me.Label2.TabIndex = 31
        Me.Label2.Text = "Razón Social"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(488, 9)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(46, 14)
        Me.Label4.TabIndex = 29
        Me.Label4.Text = "Usuario"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(237, 9)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(84, 14)
        Me.Label3.TabIndex = 29
        Me.Label3.Text = "Num. Relación"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(67, 14)
        Me.Label1.TabIndex = 30
        Me.Label1.Text = "Rut Cliente"
        '
        'txtRazons
        '
        Me.txtRazons.BackColor = System.Drawing.SystemColors.Window
        Me.txtRazons.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRazons.Location = New System.Drawing.Point(86, 36)
        Me.txtRazons.Name = "txtRazons"
        Me.txtRazons.Size = New System.Drawing.Size(386, 22)
        Me.txtRazons.TabIndex = 28
        '
        'txtRelCom
        '
        Me.txtRelCom.BackColor = System.Drawing.SystemColors.Info
        Me.txtRelCom.Location = New System.Drawing.Point(337, 6)
        Me.txtRelCom.Name = "txtRelCom"
        Me.txtRelCom.Size = New System.Drawing.Size(135, 22)
        Me.txtRelCom.TabIndex = 26
        '
        'txtRutcli
        '
        Me.txtRutcli.BackColor = System.Drawing.SystemColors.Window
        Me.txtRutcli.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRutcli.Location = New System.Drawing.Point(86, 6)
        Me.txtRutcli.Name = "txtRutcli"
        Me.txtRutcli.Size = New System.Drawing.Size(135, 22)
        Me.txtRutcli.TabIndex = 27
        '
        'dgvReajuste
        '
        Me.dgvReajuste.AllowUserToAddRows = False
        Me.dgvReajuste.AllowUserToDeleteRows = False
        Me.dgvReajuste.AllowUserToResizeRows = False
        Me.dgvReajuste.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvReajuste.Location = New System.Drawing.Point(7, 66)
        Me.dgvReajuste.Name = "dgvReajuste"
        Me.dgvReajuste.ReadOnly = True
        Me.dgvReajuste.RowHeadersVisible = False
        Me.dgvReajuste.Size = New System.Drawing.Size(1064, 299)
        Me.dgvReajuste.TabIndex = 0
        '
        'TabPage3
        '
        Me.TabPage3.BackColor = System.Drawing.SystemColors.Control
        Me.TabPage3.Controls.Add(Me.btn_CargaConvenio)
        Me.TabPage3.Controls.Add(Me.btn_Avanza)
        Me.TabPage3.Controls.Add(Me.btn_Devuelve2)
        Me.TabPage3.Controls.Add(Me.GroupBox2)
        Me.TabPage3.Controls.Add(Me.GroupBox1)
        Me.TabPage3.Location = New System.Drawing.Point(4, 23)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(1079, 390)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Editar Fechas de Convenio y Carga Inmediata"
        '
        'btn_CargaConvenio
        '
        Me.btn_CargaConvenio.Image = Global.Convenios_New.My.Resources.Resources.Floppy
        Me.btn_CargaConvenio.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_CargaConvenio.Location = New System.Drawing.Point(920, 337)
        Me.btn_CargaConvenio.Name = "btn_CargaConvenio"
        Me.btn_CargaConvenio.Size = New System.Drawing.Size(147, 47)
        Me.btn_CargaConvenio.TabIndex = 41
        Me.btn_CargaConvenio.Text = "Cargar Convenio Inmediatamente"
        Me.btn_CargaConvenio.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_CargaConvenio.UseVisualStyleBackColor = True
        '
        'btn_Avanza
        '
        Me.btn_Avanza.Image = Global.Convenios_New.My.Resources.Resources.flecha_derecha
        Me.btn_Avanza.Location = New System.Drawing.Point(961, 6)
        Me.btn_Avanza.Name = "btn_Avanza"
        Me.btn_Avanza.Size = New System.Drawing.Size(61, 44)
        Me.btn_Avanza.TabIndex = 40
        Me.btn_Avanza.UseVisualStyleBackColor = True
        '
        'btn_Devuelve2
        '
        Me.btn_Devuelve2.Image = Global.Convenios_New.My.Resources.Resources.flecha_izquierda
        Me.btn_Devuelve2.Location = New System.Drawing.Point(894, 6)
        Me.btn_Devuelve2.Name = "btn_Devuelve2"
        Me.btn_Devuelve2.Size = New System.Drawing.Size(61, 44)
        Me.btn_Devuelve2.TabIndex = 39
        Me.btn_Devuelve2.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chkMarcaTodoConvenio)
        Me.GroupBox2.Controls.Add(Me.btn_Grabar)
        Me.GroupBox2.Controls.Add(Me.dgvRelComercial)
        Me.GroupBox2.Location = New System.Drawing.Point(584, 53)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(489, 250)
        Me.GroupBox2.TabIndex = 38
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Agregar Clientes Relacionados y Fechas de Convenio"
        '
        'chkMarcaTodoConvenio
        '
        Me.chkMarcaTodoConvenio.AutoSize = True
        Me.chkMarcaTodoConvenio.Location = New System.Drawing.Point(6, 21)
        Me.chkMarcaTodoConvenio.Name = "chkMarcaTodoConvenio"
        Me.chkMarcaTodoConvenio.Size = New System.Drawing.Size(90, 18)
        Me.chkMarcaTodoConvenio.TabIndex = 34
        Me.chkMarcaTodoConvenio.Text = "Marca Todo"
        Me.chkMarcaTodoConvenio.UseVisualStyleBackColor = True
        '
        'btn_Grabar
        '
        Me.btn_Grabar.Image = Global.Convenios_New.My.Resources.Resources.Floppy
        Me.btn_Grabar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_Grabar.Location = New System.Drawing.Point(330, 192)
        Me.btn_Grabar.Name = "btn_Grabar"
        Me.btn_Grabar.Size = New System.Drawing.Size(137, 46)
        Me.btn_Grabar.TabIndex = 5
        Me.btn_Grabar.Text = "Modifica Rut de Convenio"
        Me.btn_Grabar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_Grabar.UseVisualStyleBackColor = True
        '
        'dgvRelComercial
        '
        Me.dgvRelComercial.AllowUserToAddRows = False
        Me.dgvRelComercial.AllowUserToDeleteRows = False
        Me.dgvRelComercial.AllowUserToResizeRows = False
        Me.dgvRelComercial.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvRelComercial.Location = New System.Drawing.Point(6, 45)
        Me.dgvRelComercial.Name = "dgvRelComercial"
        Me.dgvRelComercial.ReadOnly = True
        Me.dgvRelComercial.RowHeadersVisible = False
        Me.dgvRelComercial.Size = New System.Drawing.Size(461, 144)
        Me.dgvRelComercial.TabIndex = 33
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.cmbTipoConvenio)
        Me.GroupBox1.Controls.Add(Me.dgvLineasReajuste)
        Me.GroupBox1.Controls.Add(Me.Label84)
        Me.GroupBox1.Controls.Add(Me.dtpFecVenConvenio)
        Me.GroupBox1.Controls.Add(Me.dtpFechaLineaReajuste)
        Me.GroupBox1.Controls.Add(Me.btn_AgregaFecha)
        Me.GroupBox1.Controls.Add(Me.Label73)
        Me.GroupBox1.Controls.Add(Me.Label83)
        Me.GroupBox1.Controls.Add(Me.Label86)
        Me.GroupBox1.Controls.Add(Me.dtpFeciniConvenio)
        Me.GroupBox1.Controls.Add(Me.chkLineas)
        Me.GroupBox1.Location = New System.Drawing.Point(15, 53)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(563, 278)
        Me.GroupBox1.TabIndex = 37
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Agregar fechas vencimiento de las líneas"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(7, 253)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(89, 14)
        Me.Label5.TabIndex = 34
        Me.Label5.Text = "Tipo Convenio:"
        '
        'cmbTipoConvenio
        '
        Me.cmbTipoConvenio.FormattingEnabled = True
        Me.cmbTipoConvenio.Items.AddRange(New Object() {"Convenio Tissue", "Convenio Mixto"})
        Me.cmbTipoConvenio.Location = New System.Drawing.Point(102, 249)
        Me.cmbTipoConvenio.Name = "cmbTipoConvenio"
        Me.cmbTipoConvenio.Size = New System.Drawing.Size(191, 22)
        Me.cmbTipoConvenio.TabIndex = 33
        '
        'dgvLineasReajuste
        '
        Me.dgvLineasReajuste.AllowUserToAddRows = False
        Me.dgvLineasReajuste.AllowUserToDeleteRows = False
        Me.dgvLineasReajuste.AllowUserToResizeRows = False
        Me.dgvLineasReajuste.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLineasReajuste.Location = New System.Drawing.Point(10, 47)
        Me.dgvLineasReajuste.Name = "dgvLineasReajuste"
        Me.dgvLineasReajuste.ReadOnly = True
        Me.dgvLineasReajuste.RowHeadersVisible = False
        Me.dgvLineasReajuste.Size = New System.Drawing.Size(437, 144)
        Me.dgvLineasReajuste.TabIndex = 24
        '
        'Label84
        '
        Me.Label84.AutoSize = True
        Me.Label84.Location = New System.Drawing.Point(7, 204)
        Me.Label84.Name = "Label84"
        Me.Label84.Size = New System.Drawing.Size(143, 14)
        Me.Label84.TabIndex = 27
        Me.Label84.Text = "Fecha Vencimiento Línea"
        '
        'dtpFecVenConvenio
        '
        Me.dtpFecVenConvenio.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecVenConvenio.Location = New System.Drawing.Point(311, 222)
        Me.dtpFecVenConvenio.Name = "dtpFecVenConvenio"
        Me.dtpFecVenConvenio.Size = New System.Drawing.Size(130, 22)
        Me.dtpFecVenConvenio.TabIndex = 31
        '
        'dtpFechaLineaReajuste
        '
        Me.dtpFechaLineaReajuste.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaLineaReajuste.Location = New System.Drawing.Point(10, 221)
        Me.dtpFechaLineaReajuste.Name = "dtpFechaLineaReajuste"
        Me.dtpFechaLineaReajuste.Size = New System.Drawing.Size(110, 22)
        Me.dtpFechaLineaReajuste.TabIndex = 26
        '
        'btn_AgregaFecha
        '
        Me.btn_AgregaFecha.Location = New System.Drawing.Point(447, 204)
        Me.btn_AgregaFecha.Name = "btn_AgregaFecha"
        Me.btn_AgregaFecha.Size = New System.Drawing.Size(110, 40)
        Me.btn_AgregaFecha.TabIndex = 28
        Me.btn_AgregaFecha.Text = "Modificar Fechas"
        Me.btn_AgregaFecha.UseVisualStyleBackColor = True
        '
        'Label73
        '
        Me.Label73.AutoSize = True
        Me.Label73.Location = New System.Drawing.Point(308, 204)
        Me.Label73.Name = "Label73"
        Me.Label73.Size = New System.Drawing.Size(99, 14)
        Me.Label73.TabIndex = 32
        Me.Label73.Text = "Convenio Hasta :"
        '
        'Label83
        '
        Me.Label83.AutoSize = True
        Me.Label83.Location = New System.Drawing.Point(143, 30)
        Me.Label83.Name = "Label83"
        Me.Label83.Size = New System.Drawing.Size(127, 14)
        Me.Label83.TabIndex = 25
        Me.Label83.Text = "Líneas de la cotización"
        '
        'Label86
        '
        Me.Label86.AutoSize = True
        Me.Label86.Location = New System.Drawing.Point(156, 204)
        Me.Label86.Name = "Label86"
        Me.Label86.Size = New System.Drawing.Size(103, 14)
        Me.Label86.TabIndex = 32
        Me.Label86.Text = "Convenio Desde :"
        '
        'dtpFeciniConvenio
        '
        Me.dtpFeciniConvenio.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFeciniConvenio.Location = New System.Drawing.Point(159, 222)
        Me.dtpFeciniConvenio.Name = "dtpFeciniConvenio"
        Me.dtpFeciniConvenio.Size = New System.Drawing.Size(130, 22)
        Me.dtpFeciniConvenio.TabIndex = 31
        '
        'chkLineas
        '
        Me.chkLineas.AutoSize = True
        Me.chkLineas.Location = New System.Drawing.Point(7, 30)
        Me.chkLineas.Name = "chkLineas"
        Me.chkLineas.Size = New System.Drawing.Size(90, 18)
        Me.chkLineas.TabIndex = 29
        Me.chkLineas.Text = "Marca Todo"
        Me.chkLineas.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.SystemColors.Control
        Me.TabPage2.Controls.Add(Me.grbBotones)
        Me.TabPage2.Location = New System.Drawing.Point(4, 23)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1079, 390)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Envio de Correo"
        '
        'grbBotones
        '
        Me.grbBotones.Controls.Add(Me.btn_Devuelve)
        Me.grbBotones.Controls.Add(Me.txtInfoExcel)
        Me.grbBotones.Controls.Add(Me.Label75)
        Me.grbBotones.Controls.Add(Me.btn_Base)
        Me.grbBotones.Controls.Add(Me.btn_Correo)
        Me.grbBotones.Controls.Add(Me.Label70)
        Me.grbBotones.Controls.Add(Me.Label69)
        Me.grbBotones.Controls.Add(Me.txtCC3)
        Me.grbBotones.Controls.Add(Me.txtCC2)
        Me.grbBotones.Controls.Add(Me.txtCC1)
        Me.grbBotones.Controls.Add(Me.txtMail3)
        Me.grbBotones.Controls.Add(Me.txtMail2)
        Me.grbBotones.Controls.Add(Me.txtMail1)
        Me.grbBotones.Enabled = False
        Me.grbBotones.Location = New System.Drawing.Point(6, 6)
        Me.grbBotones.Name = "grbBotones"
        Me.grbBotones.Size = New System.Drawing.Size(1012, 413)
        Me.grbBotones.TabIndex = 8
        Me.grbBotones.TabStop = False
        '
        'btn_Devuelve
        '
        Me.btn_Devuelve.Image = Global.Convenios_New.My.Resources.Resources.flecha_izquierda
        Me.btn_Devuelve.Location = New System.Drawing.Point(850, 16)
        Me.btn_Devuelve.Name = "btn_Devuelve"
        Me.btn_Devuelve.Size = New System.Drawing.Size(61, 44)
        Me.btn_Devuelve.TabIndex = 33
        Me.btn_Devuelve.UseVisualStyleBackColor = True
        '
        'txtInfoExcel
        '
        Me.txtInfoExcel.Location = New System.Drawing.Point(148, 41)
        Me.txtInfoExcel.Multiline = True
        Me.txtInfoExcel.Name = "txtInfoExcel"
        Me.txtInfoExcel.Size = New System.Drawing.Size(354, 60)
        Me.txtInfoExcel.TabIndex = 15
        '
        'Label75
        '
        Me.Label75.AutoSize = True
        Me.Label75.Location = New System.Drawing.Point(144, 21)
        Me.Label75.Name = "Label75"
        Me.Label75.Size = New System.Drawing.Size(248, 14)
        Me.Label75.TabIndex = 14
        Me.Label75.Text = "Texto Informativo Excel : ""Precios tienen...."
        '
        'btn_Base
        '
        Me.btn_Base.Image = Global.Convenios_New.My.Resources.Resources.Excel
        Me.btn_Base.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_Base.Location = New System.Drawing.Point(531, 41)
        Me.btn_Base.Name = "btn_Base"
        Me.btn_Base.Size = New System.Drawing.Size(178, 46)
        Me.btn_Base.TabIndex = 5
        Me.btn_Base.Text = "Recupera Archivo Base Convenios"
        Me.btn_Base.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_Base.UseVisualStyleBackColor = True
        '
        'btn_Correo
        '
        Me.btn_Correo.Image = Global.Convenios_New.My.Resources.Resources.correo
        Me.btn_Correo.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_Correo.Location = New System.Drawing.Point(12, 41)
        Me.btn_Correo.Name = "btn_Correo"
        Me.btn_Correo.Size = New System.Drawing.Size(129, 46)
        Me.btn_Correo.TabIndex = 5
        Me.btn_Correo.Text = "Envia Correo Ejecutivo"
        Me.btn_Correo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_Correo.UseVisualStyleBackColor = True
        '
        'Label70
        '
        Me.Label70.AutoSize = True
        Me.Label70.Location = New System.Drawing.Point(20, 140)
        Me.Label70.Name = "Label70"
        Me.Label70.Size = New System.Drawing.Size(29, 14)
        Me.Label70.TabIndex = 9
        Me.Label70.Text = "CC :"
        '
        'Label69
        '
        Me.Label69.AutoSize = True
        Me.Label69.Location = New System.Drawing.Point(20, 112)
        Me.Label69.Name = "Label69"
        Me.Label69.Size = New System.Drawing.Size(42, 14)
        Me.Label69.TabIndex = 9
        Me.Label69.Text = "Para : "
        '
        'txtCC3
        '
        Me.txtCC3.Location = New System.Drawing.Point(356, 137)
        Me.txtCC3.Name = "txtCC3"
        Me.txtCC3.Size = New System.Drawing.Size(146, 22)
        Me.txtCC3.TabIndex = 8
        '
        'txtCC2
        '
        Me.txtCC2.Location = New System.Drawing.Point(212, 137)
        Me.txtCC2.Name = "txtCC2"
        Me.txtCC2.Size = New System.Drawing.Size(137, 22)
        Me.txtCC2.TabIndex = 8
        '
        'txtCC1
        '
        Me.txtCC1.Location = New System.Drawing.Point(68, 137)
        Me.txtCC1.Name = "txtCC1"
        Me.txtCC1.Size = New System.Drawing.Size(137, 22)
        Me.txtCC1.TabIndex = 8
        '
        'txtMail3
        '
        Me.txtMail3.Location = New System.Drawing.Point(356, 109)
        Me.txtMail3.Name = "txtMail3"
        Me.txtMail3.Size = New System.Drawing.Size(146, 22)
        Me.txtMail3.TabIndex = 8
        '
        'txtMail2
        '
        Me.txtMail2.Location = New System.Drawing.Point(211, 109)
        Me.txtMail2.Name = "txtMail2"
        Me.txtMail2.Size = New System.Drawing.Size(138, 22)
        Me.txtMail2.TabIndex = 8
        '
        'txtMail1
        '
        Me.txtMail1.Location = New System.Drawing.Point(68, 109)
        Me.txtMail1.Name = "txtMail1"
        Me.txtMail1.Size = New System.Drawing.Size(137, 22)
        Me.txtMail1.TabIndex = 8
        '
        'frmApruebaConvenioCorreo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1110, 500)
        Me.ControlBox = False
        Me.Controls.Add(Me.tabReajuste)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmApruebaConvenioCorreo"
        Me.ShowIcon = False
        Me.Text = "Aprobar Propuestas Enviadas por Correo"
        Me.Panel1.ResumeLayout(False)
        Me.tabReajuste.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.dgvReajuste, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.dgvRelComercial, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgvLineasReajuste, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.grbBotones.ResumeLayout(False)
        Me.grbBotones.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents btn_Salir As Button
    Friend WithEvents tabReajuste As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents dgvReajuste As DataGridView
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents txtRazons As TextBox
    Friend WithEvents txtRelCom As TextBox
    Friend WithEvents txtRutcli As TextBox
    Friend WithEvents grbBotones As GroupBox
    Friend WithEvents txtInfoExcel As TextBox
    Friend WithEvents Label75 As Label
    Friend WithEvents btn_Correo As Button
    Friend WithEvents Label70 As Label
    Friend WithEvents Label69 As Label
    Friend WithEvents txtCC3 As TextBox
    Friend WithEvents txtCC2 As TextBox
    Friend WithEvents txtCC1 As TextBox
    Friend WithEvents txtMail3 As TextBox
    Friend WithEvents txtMail2 As TextBox
    Friend WithEvents txtMail1 As TextBox
    Friend WithEvents btn_CambiaPagina As Button
    Friend WithEvents btn_Devuelve As Button
    Friend WithEvents btn_Limpiar As Button
    Friend WithEvents btn_Base As Button
    Friend WithEvents btn_ApruebaConvenio As Button
    Friend WithEvents cmbUsuario As ComboBox
    Friend WithEvents Label4 As Label
    Friend WithEvents btn_Buscar As Button
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents chkMarcaTodoConvenio As CheckBox
    Friend WithEvents btn_Grabar As Button
    Friend WithEvents dtpFecVenConvenio As DateTimePicker
    Friend WithEvents dgvRelComercial As DataGridView
    Friend WithEvents Label73 As Label
    Friend WithEvents Label86 As Label
    Friend WithEvents dtpFeciniConvenio As DateTimePicker
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents dgvLineasReajuste As DataGridView
    Friend WithEvents Label84 As Label
    Friend WithEvents dtpFechaLineaReajuste As DateTimePicker
    Friend WithEvents btn_AgregaFecha As Button
    Friend WithEvents Label83 As Label
    Friend WithEvents chkLineas As CheckBox
    Friend WithEvents btn_Devuelve2 As Button
    Friend WithEvents btn_Avanza As Button
    Friend WithEvents btn_CargaConvenio As Button
    Friend WithEvents Label5 As Label
    Friend WithEvents cmbTipoConvenio As ComboBox
End Class
