﻿Public Class frmReporteMasivoConvenio
    Dim bd As New Conexion
    Dim sql As String
    Dim dt As New DataTable
    Dim PuntoControl As New PuntoControl

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        Try
            bd.open_dimerc()
            PuntoControl.RegistroUso("65")
            MetroProgressBar1.Visible = True
            MetroProgressBar1.Maximum = 4
            Application.DoEvents()
            btnBuscar.Enabled = False

            If chbCodigo.Checked = False And chbLinea.Checked = False And chbRut.Checked = False Then
                If MessageBox.Show("¿Desea realizar la busqueda sin ningun filtro de rut, codigo o linea?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = 7 Then
                    Exit Sub
                End If
            End If

            MetroProgressBar1.Value = 0
            Application.DoEvents()
            sql = "SELECT a.numcot, 
                           a.rutcli, 
                           nvl(get_numrel (a.codemp, a.rutcli),0) numrel, 
                           a.fecemi,
                           a.fecven, 
                           b.codpro, 
                           getlinea (b.codpro) linea, 
                           b.precio,
                           GET_COSTO_ESPECIAL(B.CODPRO, A.RUTCLI,'C') costoespecial,
                           GET_COSTO_ESPECIAL(B.CODPRO, A.RUTCLI,'FF') fecfin_costo,
                                   getcostocomercial (b.codemp, b.codpro) costocomercial,
                                   getcostopromedio (b.codemp, b.codpro) cosprom
                    FROM en_conveni a, de_conveni b
                    WHERE a.numcot = b.numcot"
            If chbFecha.Checked = True Or chbFecha.Checked = 1 Then
                sql = sql & " AND TRUNC (SYSDATE) BETWEEN a.fecemi AND TO_DATE('15/07/2019 23:59:59','DD/MM/YYYY HH24:MI:SS') "
            Else
                sql = sql & " AND TRUNC (SYSDATE) BETWEEN a.fecemi AND a.fecven "
            End If
            If chbRut.Checked = True Or chbRut.Checked = 1 Then
                sql = sql & " AND A.RUTCLI = " & txtRut.Text
            End If
            If chbCodigo.Checked = True Or chbCodigo.Checked = 1 Then
                sql = sql & " AND B.CODPRO = '" & txtCodigo.Text & "'"
            End If
            If chbLinea.Checked = True Or chbLinea.Checked = 1 Then
                sql = sql & " AND GETLINEA(b.CODPRO) = '" & cmbLinea.SelectedValue & "'"
            End If
            MetroProgressBar1.Value = 1
            Application.DoEvents()

            dt = bd.sqlSelect(sql)

            MetroProgressBar1.Value = 2
            Application.DoEvents()

            dgvDatos.DataSource = dt

            MetroProgressBar1.Value = 3
            Application.DoEvents()

            dgvDatos.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells

            lbltotal.Text = "total: " & dgvDatos.RowCount

            MetroProgressBar1.Value = 4
            Application.DoEvents()

        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
            btnBuscar.Enabled = True
            MetroProgressBar1.Visible = False
        End Try
    End Sub

    Private Sub frmReporteMasivoConvenio_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            bd.open_dimerc()

            sql = "SELECT CODLIN, DESLIN FROM MA_LINEAPR ORDER BY DESLIN"
            dt = bd.sqlSelect(sql)

            cmbLinea.DataSource = dt
            cmbLinea.SelectedIndex = -1
            cmbLinea.ValueMember = "DESLIN"
            cmbLinea.DisplayMember = "DESLIN"
            cmbLinea.Refresh()

            txtCodigo.Enabled = False
            txtRut.Enabled = False
            dtpFecha.Enabled = False
            cmbLinea.Enabled = False

            PuntoControl.RegistroUso("66")
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub chbRut_CheckedChanged(sender As Object, e As EventArgs) Handles chbRut.CheckedChanged
        If chbRut.Checked = True Or chbRut.Checked = 1 Then
            txtRut.Enabled = True
            txtRut.Text = ""
        Else
            txtRut.Enabled = False
            txtRut.Text = " 15.741.524"
        End If
    End Sub

    Private Sub chbLinea_CheckedChanged(sender As Object, e As EventArgs) Handles chbLinea.CheckedChanged
        If chbLinea.Checked = True Or chbLinea.Checked = 1 Then
            cmbLinea.Enabled = True
        Else
            cmbLinea.Enabled = False
        End If
    End Sub

    Private Sub chbCodigo_CheckedChanged(sender As Object, e As EventArgs) Handles chbCodigo.CheckedChanged
        If chbCodigo.Checked = True Or chbCodigo.Checked = 1 Then
            txtCodigo.Enabled = True
            txtCodigo.Text = ""
        Else
            txtCodigo.Enabled = False
            txtCodigo.Text = "Z451245"
        End If
    End Sub

    Private Sub chbFecha_CheckedChanged(sender As Object, e As EventArgs) Handles chbFecha.CheckedChanged
        If chbFecha.Checked = True Or chbFecha.Checked = 1 Then
            dtpFecha.Enabled = True
        Else
            dtpFecha.Enabled = False
        End If
    End Sub

    Private Sub btnExcel_Click(sender As Object, e As EventArgs) Handles btnExcel.Click
        exportar(dgvDatos)
        PuntoControl.RegistroUso("67")
    End Sub

    Private Sub exportar(tabla As DataGridView)
        Dim save As New SaveFileDialog
        If tabla.RowCount = 0 Then
            MessageBox.Show("No hay datos consultados en la grilla.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        Else
            save.Filter = "Archivo Excel | *.xlsx"
            If save.ShowDialog = System.Windows.Forms.DialogResult.OK Then
                Globales.exporta(save.FileName, tabla)
            End If
        End If
    End Sub
End Class