﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmReajusteLinea
    Inherits MetroFramework.Forms.MetroForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btn_Elimina = New System.Windows.Forms.Button()
        Me.btn_Limpiar = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.cmbPeriodoConsumo = New System.Windows.Forms.ComboBox()
        Me.Label89 = New System.Windows.Forms.Label()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.Label80 = New System.Windows.Forms.Label()
        Me.Label81 = New System.Windows.Forms.Label()
        Me.txtContratoHasta = New System.Windows.Forms.TextBox()
        Me.txtContratoDesde = New System.Windows.Forms.TextBox()
        Me.txtUltimoContrato = New System.Windows.Forms.TextBox()
        Me.btnCambiaLinea = New System.Windows.Forms.Button()
        Me.btn_CambiaLinea = New System.Windows.Forms.Button()
        Me.txtVariacionMargen = New System.Windows.Forms.TextBox()
        Me.txtMargenActual = New System.Windows.Forms.TextBox()
        Me.txtLinea = New System.Windows.Forms.TextBox()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.txtPorRebate = New System.Windows.Forms.TextBox()
        Me.txtPorGastoOp = New System.Windows.Forms.TextBox()
        Me.txtPorVenta = New System.Windows.Forms.TextBox()
        Me.txtPorTransporte = New System.Windows.Forms.TextBox()
        Me.dgvResumenLínea = New System.Windows.Forms.DataGridView()
        Me.cmbMarcaPropia = New System.Windows.Forms.ComboBox()
        Me.dgvDatosMP = New System.Windows.Forms.DataGridView()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label85 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtTotMargenFinal = New System.Windows.Forms.TextBox()
        Me.txtTotGasto = New System.Windows.Forms.TextBox()
        Me.txtTotRebate = New System.Windows.Forms.TextBox()
        Me.txtGastoOp = New System.Windows.Forms.TextBox()
        Me.txtTotVenta = New System.Windows.Forms.TextBox()
        Me.txtTotTrans = New System.Windows.Forms.TextBox()
        Me.txtTotContr = New System.Windows.Forms.TextBox()
        Me.txtPorTotCon = New System.Windows.Forms.TextBox()
        Me.txtFechaFin = New System.Windows.Forms.TextBox()
        Me.txtFechaIni = New System.Windows.Forms.TextBox()
        Me.txtTotIngreso = New System.Windows.Forms.TextBox()
        Me.txtRazons = New System.Windows.Forms.TextBox()
        Me.txtNumcot = New System.Windows.Forms.TextBox()
        Me.txtVendedor = New System.Windows.Forms.TextBox()
        Me.txtRutcli = New System.Windows.Forms.TextBox()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Label87 = New System.Windows.Forms.Label()
        Me.Label88 = New System.Windows.Forms.Label()
        Me.dgvInput = New System.Windows.Forms.DataGridView()
        Me.btn_Input = New System.Windows.Forms.Button()
        Me.btn_Confirmar = New System.Windows.Forms.Button()
        Me.txtCostoEspecial = New System.Windows.Forms.TextBox()
        Me.txtVariacion = New System.Windows.Forms.TextBox()
        Me.txtPrecioNuevo = New System.Windows.Forms.TextBox()
        Me.txtPrecio = New System.Windows.Forms.TextBox()
        Me.txtDescripcion = New System.Windows.Forms.TextBox()
        Me.txtCodpro = New System.Windows.Forms.TextBox()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.dgvDetalleLineaProductos = New System.Windows.Forms.DataGridView()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.cmbConsumoFinal = New System.Windows.Forms.ComboBox()
        Me.Label86 = New System.Windows.Forms.Label()
        Me.txtPorRebateFinal = New System.Windows.Forms.TextBox()
        Me.txtPorGastoOpFinal = New System.Windows.Forms.TextBox()
        Me.txtPorVentaFinal = New System.Windows.Forms.TextBox()
        Me.txtPorTransFinal = New System.Windows.Forms.TextBox()
        Me.dgvLineaFinal = New System.Windows.Forms.DataGridView()
        Me.cmbMarcaPropiaFinal = New System.Windows.Forms.ComboBox()
        Me.dgvMpFinal = New System.Windows.Forms.DataGridView()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.txtMgFinal = New System.Windows.Forms.TextBox()
        Me.txtTotGastoFinal = New System.Windows.Forms.TextBox()
        Me.txtTotRebateFinal = New System.Windows.Forms.TextBox()
        Me.txtTotGastOpFinal = New System.Windows.Forms.TextBox()
        Me.txtTotVentaFinal = New System.Windows.Forms.TextBox()
        Me.txtTotTransFinal = New System.Windows.Forms.TextBox()
        Me.txtTotContriFinal = New System.Windows.Forms.TextBox()
        Me.txtPorContriFinal = New System.Windows.Forms.TextBox()
        Me.txtPorVariacion = New System.Windows.Forms.TextBox()
        Me.txtValAct = New System.Windows.Forms.TextBox()
        Me.txtValAnt = New System.Windows.Forms.TextBox()
        Me.txtTotIngresiFinal = New System.Windows.Forms.TextBox()
        Me.txtRazonsFinal = New System.Windows.Forms.TextBox()
        Me.txtNumcotFinal = New System.Windows.Forms.TextBox()
        Me.txtRutCliFinal = New System.Windows.Forms.TextBox()
        Me.btnExcel = New System.Windows.Forms.Button()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.Label74 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.chkMarcaTodoConvenio = New System.Windows.Forms.CheckBox()
        Me.btn_Grabar = New System.Windows.Forms.Button()
        Me.dgvRelComercial = New System.Windows.Forms.DataGridView()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.dgvLineasReajuste = New System.Windows.Forms.DataGridView()
        Me.Label84 = New System.Windows.Forms.Label()
        Me.dtpFecVenConvenio = New System.Windows.Forms.DateTimePicker()
        Me.dtpFechaLineaReajuste = New System.Windows.Forms.DateTimePicker()
        Me.btn_AgregaFecha = New System.Windows.Forms.Button()
        Me.Label73 = New System.Windows.Forms.Label()
        Me.Label83 = New System.Windows.Forms.Label()
        Me.Label82 = New System.Windows.Forms.Label()
        Me.dtpFeciniConvenio = New System.Windows.Forms.DateTimePicker()
        Me.chkLineas = New System.Windows.Forms.CheckBox()
        Me.txtmgant = New System.Windows.Forms.TextBox()
        Me.txtmgdif = New System.Windows.Forms.TextBox()
        Me.txtsumaant = New System.Windows.Forms.TextBox()
        Me.txtsumDif = New System.Windows.Forms.TextBox()
        Me.txtrebant = New System.Windows.Forms.TextBox()
        Me.txtrebdif = New System.Windows.Forms.TextBox()
        Me.txtgasant = New System.Windows.Forms.TextBox()
        Me.txtgasdif = New System.Windows.Forms.TextBox()
        Me.txtvenant = New System.Windows.Forms.TextBox()
        Me.txtventdif = New System.Windows.Forms.TextBox()
        Me.txtmgnew = New System.Windows.Forms.TextBox()
        Me.txtsumanew = New System.Windows.Forms.TextBox()
        Me.txttranant = New System.Windows.Forms.TextBox()
        Me.txtrebnew = New System.Windows.Forms.TextBox()
        Me.txttrandif = New System.Windows.Forms.TextBox()
        Me.txtgasnew = New System.Windows.Forms.TextBox()
        Me.txtcontant = New System.Windows.Forms.TextBox()
        Me.txtvennew = New System.Windows.Forms.TextBox()
        Me.txtcontdif = New System.Windows.Forms.TextBox()
        Me.txttrannew = New System.Windows.Forms.TextBox()
        Me.txtingAnt = New System.Windows.Forms.TextBox()
        Me.txtcontnew = New System.Windows.Forms.TextBox()
        Me.txtingdif = New System.Windows.Forms.TextBox()
        Me.txtingnew = New System.Windows.Forms.TextBox()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.Label61 = New System.Windows.Forms.Label()
        Me.Label66 = New System.Windows.Forms.Label()
        Me.Label67 = New System.Windows.Forms.Label()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.Label68 = New System.Windows.Forms.Label()
        Me.Label69 = New System.Windows.Forms.Label()
        Me.Label70 = New System.Windows.Forms.Label()
        Me.txtobserv = New System.Windows.Forms.TextBox()
        Me.Label71 = New System.Windows.Forms.Label()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.grbBotones = New System.Windows.Forms.GroupBox()
        Me.chkMarcaTodoCosEsp = New System.Windows.Forms.CheckBox()
        Me.txtInfoExcel = New System.Windows.Forms.TextBox()
        Me.Label75 = New System.Windows.Forms.Label()
        Me.btn_Correo = New System.Windows.Forms.Button()
        Me.Label72 = New System.Windows.Forms.Label()
        Me.Label76 = New System.Windows.Forms.Label()
        Me.dtpCosEspFin = New System.Windows.Forms.DateTimePicker()
        Me.dtpCosEspIni = New System.Windows.Forms.DateTimePicker()
        Me.Label77 = New System.Windows.Forms.Label()
        Me.Label78 = New System.Windows.Forms.Label()
        Me.txtCC3 = New System.Windows.Forms.TextBox()
        Me.txtCC2 = New System.Windows.Forms.TextBox()
        Me.txtCC1 = New System.Windows.Forms.TextBox()
        Me.txtMail3 = New System.Windows.Forms.TextBox()
        Me.txtMail2 = New System.Windows.Forms.TextBox()
        Me.txtMail1 = New System.Windows.Forms.TextBox()
        Me.Label79 = New System.Windows.Forms.Label()
        Me.dgvEmpRelacion = New System.Windows.Forms.DataGridView()
        Me.btn_CostoEspecial = New System.Windows.Forms.Button()
        Me.Panel1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.dgvResumenLínea, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvDatosMP, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.dgvInput, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvDetalleLineaProductos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        CType(Me.dgvLineaFinal, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvMpFinal, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage4.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvRelComercial, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvLineasReajuste, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage5.SuspendLayout()
        Me.grbBotones.SuspendLayout()
        CType(Me.dgvEmpRelacion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.btn_Elimina)
        Me.Panel1.Controls.Add(Me.btn_Limpiar)
        Me.Panel1.Controls.Add(Me.Button1)
        Me.Panel1.Location = New System.Drawing.Point(12, 56)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1240, 50)
        Me.Panel1.TabIndex = 2
        '
        'btn_Elimina
        '
        Me.btn_Elimina.Image = Global.Convenios_New.My.Resources.Resources.Delete1
        Me.btn_Elimina.Location = New System.Drawing.Point(59, 4)
        Me.btn_Elimina.Name = "btn_Elimina"
        Me.btn_Elimina.Size = New System.Drawing.Size(53, 40)
        Me.btn_Elimina.TabIndex = 2
        Me.btn_Elimina.UseVisualStyleBackColor = True
        '
        'btn_Limpiar
        '
        Me.btn_Limpiar.Image = Global.Convenios_New.My.Resources.Resources.limpiar
        Me.btn_Limpiar.Location = New System.Drawing.Point(9, 3)
        Me.btn_Limpiar.Name = "btn_Limpiar"
        Me.btn_Limpiar.Size = New System.Drawing.Size(45, 43)
        Me.btn_Limpiar.TabIndex = 1
        Me.btn_Limpiar.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Image = Global.Convenios_New.My.Resources.Resources.salir
        Me.Button1.Location = New System.Drawing.Point(1194, 2)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(41, 43)
        Me.Button1.TabIndex = 0
        Me.Button1.UseVisualStyleBackColor = True
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Controls.Add(Me.TabPage5)
        Me.TabControl1.Location = New System.Drawing.Point(8, 115)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1292, 529)
        Me.TabControl1.TabIndex = 5
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.cmbPeriodoConsumo)
        Me.TabPage1.Controls.Add(Me.Label89)
        Me.TabPage1.Controls.Add(Me.Label56)
        Me.TabPage1.Controls.Add(Me.Label80)
        Me.TabPage1.Controls.Add(Me.Label81)
        Me.TabPage1.Controls.Add(Me.txtContratoHasta)
        Me.TabPage1.Controls.Add(Me.txtContratoDesde)
        Me.TabPage1.Controls.Add(Me.txtUltimoContrato)
        Me.TabPage1.Controls.Add(Me.btnCambiaLinea)
        Me.TabPage1.Controls.Add(Me.btn_CambiaLinea)
        Me.TabPage1.Controls.Add(Me.txtVariacionMargen)
        Me.TabPage1.Controls.Add(Me.txtMargenActual)
        Me.TabPage1.Controls.Add(Me.txtLinea)
        Me.TabPage1.Controls.Add(Me.Label57)
        Me.TabPage1.Controls.Add(Me.Label58)
        Me.TabPage1.Controls.Add(Me.Label59)
        Me.TabPage1.Controls.Add(Me.Label52)
        Me.TabPage1.Controls.Add(Me.Label51)
        Me.TabPage1.Controls.Add(Me.txtPorRebate)
        Me.TabPage1.Controls.Add(Me.txtPorGastoOp)
        Me.TabPage1.Controls.Add(Me.txtPorVenta)
        Me.TabPage1.Controls.Add(Me.txtPorTransporte)
        Me.TabPage1.Controls.Add(Me.dgvResumenLínea)
        Me.TabPage1.Controls.Add(Me.cmbMarcaPropia)
        Me.TabPage1.Controls.Add(Me.dgvDatosMP)
        Me.TabPage1.Controls.Add(Me.Label15)
        Me.TabPage1.Controls.Add(Me.Label14)
        Me.TabPage1.Controls.Add(Me.Label13)
        Me.TabPage1.Controls.Add(Me.Label12)
        Me.TabPage1.Controls.Add(Me.Label20)
        Me.TabPage1.Controls.Add(Me.Label19)
        Me.TabPage1.Controls.Add(Me.Label18)
        Me.TabPage1.Controls.Add(Me.Label17)
        Me.TabPage1.Controls.Add(Me.Label16)
        Me.TabPage1.Controls.Add(Me.Label11)
        Me.TabPage1.Controls.Add(Me.Label10)
        Me.TabPage1.Controls.Add(Me.Label9)
        Me.TabPage1.Controls.Add(Me.Label8)
        Me.TabPage1.Controls.Add(Me.Label7)
        Me.TabPage1.Controls.Add(Me.Label6)
        Me.TabPage1.Controls.Add(Me.Label5)
        Me.TabPage1.Controls.Add(Me.Label4)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.Label3)
        Me.TabPage1.Controls.Add(Me.Label22)
        Me.TabPage1.Controls.Add(Me.Label21)
        Me.TabPage1.Controls.Add(Me.Label85)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Controls.Add(Me.txtTotMargenFinal)
        Me.TabPage1.Controls.Add(Me.txtTotGasto)
        Me.TabPage1.Controls.Add(Me.txtTotRebate)
        Me.TabPage1.Controls.Add(Me.txtGastoOp)
        Me.TabPage1.Controls.Add(Me.txtTotVenta)
        Me.TabPage1.Controls.Add(Me.txtTotTrans)
        Me.TabPage1.Controls.Add(Me.txtTotContr)
        Me.TabPage1.Controls.Add(Me.txtPorTotCon)
        Me.TabPage1.Controls.Add(Me.txtFechaFin)
        Me.TabPage1.Controls.Add(Me.txtFechaIni)
        Me.TabPage1.Controls.Add(Me.txtTotIngreso)
        Me.TabPage1.Controls.Add(Me.txtRazons)
        Me.TabPage1.Controls.Add(Me.txtNumcot)
        Me.TabPage1.Controls.Add(Me.txtVendedor)
        Me.TabPage1.Controls.Add(Me.txtRutcli)
        Me.TabPage1.Location = New System.Drawing.Point(4, 23)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1284, 502)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Datos Reajuste"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'cmbPeriodoConsumo
        '
        Me.cmbPeriodoConsumo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbPeriodoConsumo.FormattingEnabled = True
        Me.cmbPeriodoConsumo.Location = New System.Drawing.Point(169, 9)
        Me.cmbPeriodoConsumo.Name = "cmbPeriodoConsumo"
        Me.cmbPeriodoConsumo.Size = New System.Drawing.Size(197, 22)
        Me.cmbPeriodoConsumo.TabIndex = 63
        '
        'Label89
        '
        Me.Label89.AutoSize = True
        Me.Label89.Location = New System.Drawing.Point(12, 12)
        Me.Label89.Name = "Label89"
        Me.Label89.Size = New System.Drawing.Size(120, 14)
        Me.Label89.TabIndex = 62
        Me.Label89.Text = "Periodo de Consumo"
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Location = New System.Drawing.Point(304, 463)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(45, 14)
        Me.Label56.TabIndex = 59
        Me.Label56.Text = "Hasta :"
        '
        'Label80
        '
        Me.Label80.AutoSize = True
        Me.Label80.Location = New System.Drawing.Point(304, 435)
        Me.Label80.Name = "Label80"
        Me.Label80.Size = New System.Drawing.Size(49, 14)
        Me.Label80.TabIndex = 60
        Me.Label80.Text = "Desde :"
        '
        'Label81
        '
        Me.Label81.Location = New System.Drawing.Point(298, 378)
        Me.Label81.Name = "Label81"
        Me.Label81.Size = New System.Drawing.Size(58, 51)
        Me.Label81.TabIndex = 61
        Me.Label81.Text = "Ultimo Contrato Vigente"
        '
        'txtContratoHasta
        '
        Me.txtContratoHasta.BackColor = System.Drawing.SystemColors.Info
        Me.txtContratoHasta.Location = New System.Drawing.Point(359, 460)
        Me.txtContratoHasta.Name = "txtContratoHasta"
        Me.txtContratoHasta.Size = New System.Drawing.Size(116, 22)
        Me.txtContratoHasta.TabIndex = 56
        Me.txtContratoHasta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtContratoDesde
        '
        Me.txtContratoDesde.BackColor = System.Drawing.SystemColors.Info
        Me.txtContratoDesde.Location = New System.Drawing.Point(359, 432)
        Me.txtContratoDesde.Name = "txtContratoDesde"
        Me.txtContratoDesde.Size = New System.Drawing.Size(116, 22)
        Me.txtContratoDesde.TabIndex = 57
        Me.txtContratoDesde.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtUltimoContrato
        '
        Me.txtUltimoContrato.BackColor = System.Drawing.SystemColors.Info
        Me.txtUltimoContrato.Location = New System.Drawing.Point(359, 392)
        Me.txtUltimoContrato.Name = "txtUltimoContrato"
        Me.txtUltimoContrato.Size = New System.Drawing.Size(116, 22)
        Me.txtUltimoContrato.TabIndex = 58
        Me.txtUltimoContrato.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'btnCambiaLinea
        '
        Me.btnCambiaLinea.Location = New System.Drawing.Point(1122, 210)
        Me.btnCambiaLinea.Name = "btnCambiaLinea"
        Me.btnCambiaLinea.Size = New System.Drawing.Size(99, 42)
        Me.btnCambiaLinea.TabIndex = 55
        Me.btnCambiaLinea.Text = "Aplica Cambio"
        Me.btnCambiaLinea.UseVisualStyleBackColor = True
        '
        'btn_CambiaLinea
        '
        Me.btn_CambiaLinea.Image = Global.Convenios_New.My.Resources.Resources.Tips
        Me.btn_CambiaLinea.ImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btn_CambiaLinea.Location = New System.Drawing.Point(1359, 192)
        Me.btn_CambiaLinea.Name = "btn_CambiaLinea"
        Me.btn_CambiaLinea.Size = New System.Drawing.Size(71, 60)
        Me.btn_CambiaLinea.TabIndex = 54
        Me.btn_CambiaLinea.Text = "Confirmar"
        Me.btn_CambiaLinea.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btn_CambiaLinea.UseVisualStyleBackColor = True
        '
        'txtVariacionMargen
        '
        Me.txtVariacionMargen.Location = New System.Drawing.Point(1043, 231)
        Me.txtVariacionMargen.Name = "txtVariacionMargen"
        Me.txtVariacionMargen.Size = New System.Drawing.Size(63, 22)
        Me.txtVariacionMargen.TabIndex = 50
        Me.txtVariacionMargen.Text = "0"
        Me.txtVariacionMargen.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtMargenActual
        '
        Me.txtMargenActual.BackColor = System.Drawing.SystemColors.Info
        Me.txtMargenActual.Location = New System.Drawing.Point(952, 231)
        Me.txtMargenActual.Name = "txtMargenActual"
        Me.txtMargenActual.Size = New System.Drawing.Size(74, 22)
        Me.txtMargenActual.TabIndex = 52
        Me.txtMargenActual.Text = "0"
        Me.txtMargenActual.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtLinea
        '
        Me.txtLinea.BackColor = System.Drawing.SystemColors.Info
        Me.txtLinea.Location = New System.Drawing.Point(545, 231)
        Me.txtLinea.Name = "txtLinea"
        Me.txtLinea.Size = New System.Drawing.Size(395, 22)
        Me.txtLinea.TabIndex = 53
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Location = New System.Drawing.Point(1035, 211)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(81, 14)
        Me.Label57.TabIndex = 47
        Me.Label57.Text = "Variacion (%)"
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Location = New System.Drawing.Point(943, 211)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(85, 14)
        Me.Label58.TabIndex = 48
        Me.Label58.Text = "Margen Actual"
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Location = New System.Drawing.Point(723, 211)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(35, 14)
        Me.Label59.TabIndex = 49
        Me.Label59.Text = "Linea"
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.Location = New System.Drawing.Point(59, 429)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(58, 14)
        Me.Label52.TabIndex = 45
        Me.Label52.Text = "Hasta el :"
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.Location = New System.Drawing.Point(11, 399)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(99, 14)
        Me.Label51.TabIndex = 45
        Me.Label51.Text = "Venta Desde el :"
        '
        'txtPorRebate
        '
        Me.txtPorRebate.Location = New System.Drawing.Point(168, 280)
        Me.txtPorRebate.Name = "txtPorRebate"
        Me.txtPorRebate.Size = New System.Drawing.Size(60, 22)
        Me.txtPorRebate.TabIndex = 44
        Me.txtPorRebate.Text = "0"
        Me.txtPorRebate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPorGastoOp
        '
        Me.txtPorGastoOp.Location = New System.Drawing.Point(168, 250)
        Me.txtPorGastoOp.Name = "txtPorGastoOp"
        Me.txtPorGastoOp.Size = New System.Drawing.Size(60, 22)
        Me.txtPorGastoOp.TabIndex = 44
        Me.txtPorGastoOp.Text = "0"
        Me.txtPorGastoOp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPorVenta
        '
        Me.txtPorVenta.Location = New System.Drawing.Point(168, 220)
        Me.txtPorVenta.Name = "txtPorVenta"
        Me.txtPorVenta.Size = New System.Drawing.Size(60, 22)
        Me.txtPorVenta.TabIndex = 44
        Me.txtPorVenta.Text = "0"
        Me.txtPorVenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPorTransporte
        '
        Me.txtPorTransporte.Location = New System.Drawing.Point(168, 191)
        Me.txtPorTransporte.Name = "txtPorTransporte"
        Me.txtPorTransporte.Size = New System.Drawing.Size(60, 22)
        Me.txtPorTransporte.TabIndex = 44
        Me.txtPorTransporte.Text = "0"
        Me.txtPorTransporte.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'dgvResumenLínea
        '
        Me.dgvResumenLínea.AllowUserToAddRows = False
        Me.dgvResumenLínea.AllowUserToDeleteRows = False
        Me.dgvResumenLínea.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvResumenLínea.Location = New System.Drawing.Point(545, 258)
        Me.dgvResumenLínea.Name = "dgvResumenLínea"
        Me.dgvResumenLínea.ReadOnly = True
        Me.dgvResumenLínea.RowHeadersVisible = False
        Me.dgvResumenLínea.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvResumenLínea.Size = New System.Drawing.Size(645, 201)
        Me.dgvResumenLínea.TabIndex = 43
        '
        'cmbMarcaPropia
        '
        Me.cmbMarcaPropia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMarcaPropia.FormattingEnabled = True
        Me.cmbMarcaPropia.Location = New System.Drawing.Point(699, 42)
        Me.cmbMarcaPropia.Name = "cmbMarcaPropia"
        Me.cmbMarcaPropia.Size = New System.Drawing.Size(229, 22)
        Me.cmbMarcaPropia.TabIndex = 41
        '
        'dgvDatosMP
        '
        Me.dgvDatosMP.AllowUserToAddRows = False
        Me.dgvDatosMP.AllowUserToDeleteRows = False
        Me.dgvDatosMP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDatosMP.Location = New System.Drawing.Point(545, 70)
        Me.dgvDatosMP.Name = "dgvDatosMP"
        Me.dgvDatosMP.ReadOnly = True
        Me.dgvDatosMP.RowHeadersVisible = False
        Me.dgvDatosMP.Size = New System.Drawing.Size(645, 128)
        Me.dgvDatosMP.TabIndex = 39
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(237, 284)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(19, 14)
        Me.Label15.TabIndex = 38
        Me.Label15.Text = "%"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(237, 254)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(19, 14)
        Me.Label14.TabIndex = 29
        Me.Label14.Text = "%"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(237, 223)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(19, 14)
        Me.Label13.TabIndex = 30
        Me.Label13.Text = "%"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(237, 193)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(19, 14)
        Me.Label12.TabIndex = 31
        Me.Label12.Text = "%"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(267, 284)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(16, 14)
        Me.Label20.TabIndex = 32
        Me.Label20.Text = "="
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(267, 254)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(16, 14)
        Me.Label19.TabIndex = 37
        Me.Label19.Text = "="
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(267, 223)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(16, 14)
        Me.Label18.TabIndex = 34
        Me.Label18.Text = "="
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(267, 193)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(16, 14)
        Me.Label17.TabIndex = 33
        Me.Label17.Text = "="
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(267, 166)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(16, 14)
        Me.Label16.TabIndex = 35
        Me.Label16.Text = "="
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(238, 166)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(19, 14)
        Me.Label11.TabIndex = 36
        Me.Label11.Text = "%"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(8, 344)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(126, 14)
        Me.Label10.TabIndex = 27
        Me.Label10.Text = "Mg. Final Contribución"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(8, 314)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(75, 14)
        Me.Label9.TabIndex = 26
        Me.Label9.Text = "Total Gastos"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(8, 254)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(102, 14)
        Me.Label8.TabIndex = 25
        Me.Label8.Text = "Gastos Operación"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(8, 223)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(40, 14)
        Me.Label7.TabIndex = 24
        Me.Label7.Text = "Venta"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(8, 193)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(67, 14)
        Me.Label6.TabIndex = 23
        Me.Label6.Text = "Transporte"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(8, 163)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(107, 14)
        Me.Label5.TabIndex = 22
        Me.Label5.Text = "Total Contribución"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(8, 133)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(85, 14)
        Me.Label4.TabIndex = 28
        Me.Label4.Text = "Total Ingresos"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(8, 103)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(73, 14)
        Me.Label2.TabIndex = 21
        Me.Label2.Text = "Razón Social"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(8, 284)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(46, 14)
        Me.Label3.TabIndex = 19
        Me.Label3.Text = "Rebate"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(609, 44)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(75, 14)
        Me.Label22.TabIndex = 20
        Me.Label22.Text = "Marca Propia"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(8, 45)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(108, 14)
        Me.Label21.TabIndex = 20
        Me.Label21.Text = "Número Cotización"
        '
        'Label85
        '
        Me.Label85.AutoSize = True
        Me.Label85.Location = New System.Drawing.Point(309, 44)
        Me.Label85.Name = "Label85"
        Me.Label85.Size = New System.Drawing.Size(57, 14)
        Me.Label85.TabIndex = 20
        Me.Label85.Text = "Ejecutivo"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 73)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(67, 14)
        Me.Label1.TabIndex = 20
        Me.Label1.Text = "Rut Cliente"
        '
        'txtTotMargenFinal
        '
        Me.txtTotMargenFinal.BackColor = System.Drawing.SystemColors.Info
        Me.txtTotMargenFinal.Location = New System.Drawing.Point(301, 341)
        Me.txtTotMargenFinal.Name = "txtTotMargenFinal"
        Me.txtTotMargenFinal.Size = New System.Drawing.Size(160, 22)
        Me.txtTotMargenFinal.TabIndex = 5
        Me.txtTotMargenFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotGasto
        '
        Me.txtTotGasto.BackColor = System.Drawing.SystemColors.Info
        Me.txtTotGasto.Location = New System.Drawing.Point(301, 311)
        Me.txtTotGasto.Name = "txtTotGasto"
        Me.txtTotGasto.Size = New System.Drawing.Size(160, 22)
        Me.txtTotGasto.TabIndex = 6
        Me.txtTotGasto.Text = "0"
        Me.txtTotGasto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotRebate
        '
        Me.txtTotRebate.BackColor = System.Drawing.SystemColors.Info
        Me.txtTotRebate.Location = New System.Drawing.Point(301, 280)
        Me.txtTotRebate.Name = "txtTotRebate"
        Me.txtTotRebate.Size = New System.Drawing.Size(160, 22)
        Me.txtTotRebate.TabIndex = 7
        Me.txtTotRebate.Text = "0"
        Me.txtTotRebate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtGastoOp
        '
        Me.txtGastoOp.BackColor = System.Drawing.SystemColors.Info
        Me.txtGastoOp.Location = New System.Drawing.Point(301, 250)
        Me.txtGastoOp.Name = "txtGastoOp"
        Me.txtGastoOp.Size = New System.Drawing.Size(160, 22)
        Me.txtGastoOp.TabIndex = 9
        Me.txtGastoOp.Text = "0"
        Me.txtGastoOp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotVenta
        '
        Me.txtTotVenta.BackColor = System.Drawing.SystemColors.Info
        Me.txtTotVenta.Location = New System.Drawing.Point(301, 220)
        Me.txtTotVenta.Name = "txtTotVenta"
        Me.txtTotVenta.Size = New System.Drawing.Size(160, 22)
        Me.txtTotVenta.TabIndex = 10
        Me.txtTotVenta.Text = "0"
        Me.txtTotVenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotTrans
        '
        Me.txtTotTrans.BackColor = System.Drawing.SystemColors.Info
        Me.txtTotTrans.Location = New System.Drawing.Point(301, 190)
        Me.txtTotTrans.Name = "txtTotTrans"
        Me.txtTotTrans.Size = New System.Drawing.Size(160, 22)
        Me.txtTotTrans.TabIndex = 12
        Me.txtTotTrans.Text = "0"
        Me.txtTotTrans.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotContr
        '
        Me.txtTotContr.BackColor = System.Drawing.SystemColors.Info
        Me.txtTotContr.Location = New System.Drawing.Point(301, 160)
        Me.txtTotContr.Name = "txtTotContr"
        Me.txtTotContr.Size = New System.Drawing.Size(160, 22)
        Me.txtTotContr.TabIndex = 14
        Me.txtTotContr.Text = "0"
        Me.txtTotContr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPorTotCon
        '
        Me.txtPorTotCon.BackColor = System.Drawing.SystemColors.Info
        Me.txtPorTotCon.Location = New System.Drawing.Point(168, 160)
        Me.txtPorTotCon.Name = "txtPorTotCon"
        Me.txtPorTotCon.Size = New System.Drawing.Size(60, 22)
        Me.txtPorTotCon.TabIndex = 15
        Me.txtPorTotCon.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtFechaFin
        '
        Me.txtFechaFin.BackColor = System.Drawing.SystemColors.Info
        Me.txtFechaFin.Location = New System.Drawing.Point(139, 425)
        Me.txtFechaFin.Name = "txtFechaFin"
        Me.txtFechaFin.Size = New System.Drawing.Size(135, 22)
        Me.txtFechaFin.TabIndex = 16
        Me.txtFechaFin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtFechaIni
        '
        Me.txtFechaIni.BackColor = System.Drawing.SystemColors.Info
        Me.txtFechaIni.Location = New System.Drawing.Point(139, 395)
        Me.txtFechaIni.Name = "txtFechaIni"
        Me.txtFechaIni.Size = New System.Drawing.Size(135, 22)
        Me.txtFechaIni.TabIndex = 16
        Me.txtFechaIni.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotIngreso
        '
        Me.txtTotIngreso.BackColor = System.Drawing.SystemColors.Info
        Me.txtTotIngreso.Location = New System.Drawing.Point(168, 130)
        Me.txtTotIngreso.Name = "txtTotIngreso"
        Me.txtTotIngreso.Size = New System.Drawing.Size(135, 22)
        Me.txtTotIngreso.TabIndex = 16
        Me.txtTotIngreso.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtRazons
        '
        Me.txtRazons.BackColor = System.Drawing.SystemColors.Info
        Me.txtRazons.Location = New System.Drawing.Point(168, 100)
        Me.txtRazons.Name = "txtRazons"
        Me.txtRazons.Size = New System.Drawing.Size(361, 22)
        Me.txtRazons.TabIndex = 18
        '
        'txtNumcot
        '
        Me.txtNumcot.Location = New System.Drawing.Point(168, 41)
        Me.txtNumcot.Name = "txtNumcot"
        Me.txtNumcot.Size = New System.Drawing.Size(135, 22)
        Me.txtNumcot.TabIndex = 4
        '
        'txtVendedor
        '
        Me.txtVendedor.BackColor = System.Drawing.SystemColors.Info
        Me.txtVendedor.Location = New System.Drawing.Point(384, 41)
        Me.txtVendedor.Name = "txtVendedor"
        Me.txtVendedor.Size = New System.Drawing.Size(210, 22)
        Me.txtVendedor.TabIndex = 4
        '
        'txtRutcli
        '
        Me.txtRutcli.BackColor = System.Drawing.SystemColors.Info
        Me.txtRutcli.Location = New System.Drawing.Point(168, 69)
        Me.txtRutcli.Name = "txtRutcli"
        Me.txtRutcli.Size = New System.Drawing.Size(135, 22)
        Me.txtRutcli.TabIndex = 4
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.Label87)
        Me.TabPage2.Controls.Add(Me.Label88)
        Me.TabPage2.Controls.Add(Me.dgvInput)
        Me.TabPage2.Controls.Add(Me.btn_Input)
        Me.TabPage2.Controls.Add(Me.btn_Confirmar)
        Me.TabPage2.Controls.Add(Me.txtCostoEspecial)
        Me.TabPage2.Controls.Add(Me.txtVariacion)
        Me.TabPage2.Controls.Add(Me.txtPrecioNuevo)
        Me.TabPage2.Controls.Add(Me.txtPrecio)
        Me.TabPage2.Controls.Add(Me.txtDescripcion)
        Me.TabPage2.Controls.Add(Me.txtCodpro)
        Me.TabPage2.Controls.Add(Me.Label50)
        Me.TabPage2.Controls.Add(Me.Label27)
        Me.TabPage2.Controls.Add(Me.Label26)
        Me.TabPage2.Controls.Add(Me.Label25)
        Me.TabPage2.Controls.Add(Me.Label24)
        Me.TabPage2.Controls.Add(Me.Label23)
        Me.TabPage2.Controls.Add(Me.dgvDetalleLineaProductos)
        Me.TabPage2.Location = New System.Drawing.Point(4, 23)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1284, 502)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Cuadros de Resumen"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'Label87
        '
        Me.Label87.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label87.Location = New System.Drawing.Point(184, 469)
        Me.Label87.Name = "Label87"
        Me.Label87.Size = New System.Drawing.Size(554, 23)
        Me.Label87.TabIndex = 60
        Me.Label87.Text = "Columnas deben llamarse  CODPRO - CANTIDAD - PRECIO - COSTO ESPECIAL - APORTE"
        '
        'Label88
        '
        Me.Label88.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label88.Location = New System.Drawing.Point(184, 447)
        Me.Label88.Name = "Label88"
        Me.Label88.Size = New System.Drawing.Size(458, 12)
        Me.Label88.TabIndex = 61
        Me.Label88.Text = "Archivo debe ser excel con formato xls. o xlsx."
        '
        'dgvInput
        '
        Me.dgvInput.AllowUserToAddRows = False
        Me.dgvInput.AllowUserToDeleteRows = False
        Me.dgvInput.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvInput.Location = New System.Drawing.Point(144, 447)
        Me.dgvInput.Name = "dgvInput"
        Me.dgvInput.ReadOnly = True
        Me.dgvInput.Size = New System.Drawing.Size(33, 17)
        Me.dgvInput.TabIndex = 59
        Me.dgvInput.Visible = False
        '
        'btn_Input
        '
        Me.btn_Input.Image = Global.Convenios_New.My.Resources.Resources.Excel
        Me.btn_Input.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_Input.Location = New System.Drawing.Point(6, 447)
        Me.btn_Input.Name = "btn_Input"
        Me.btn_Input.Size = New System.Drawing.Size(132, 50)
        Me.btn_Input.TabIndex = 58
        Me.btn_Input.Text = "Carga Input"
        Me.btn_Input.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_Input.UseVisualStyleBackColor = True
        '
        'btn_Confirmar
        '
        Me.btn_Confirmar.Image = Global.Convenios_New.My.Resources.Resources.Tips
        Me.btn_Confirmar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_Confirmar.Location = New System.Drawing.Point(1081, 29)
        Me.btn_Confirmar.Name = "btn_Confirmar"
        Me.btn_Confirmar.Size = New System.Drawing.Size(145, 42)
        Me.btn_Confirmar.TabIndex = 57
        Me.btn_Confirmar.Text = "Confirmar Cambios"
        Me.btn_Confirmar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_Confirmar.UseVisualStyleBackColor = True
        '
        'txtCostoEspecial
        '
        Me.txtCostoEspecial.Location = New System.Drawing.Point(931, 49)
        Me.txtCostoEspecial.Name = "txtCostoEspecial"
        Me.txtCostoEspecial.Size = New System.Drawing.Size(121, 22)
        Me.txtCostoEspecial.TabIndex = 51
        Me.txtCostoEspecial.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtVariacion
        '
        Me.txtVariacion.Location = New System.Drawing.Point(648, 49)
        Me.txtVariacion.Name = "txtVariacion"
        Me.txtVariacion.Size = New System.Drawing.Size(121, 22)
        Me.txtVariacion.TabIndex = 52
        Me.txtVariacion.Text = "0"
        Me.txtVariacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPrecioNuevo
        '
        Me.txtPrecioNuevo.Location = New System.Drawing.Point(780, 49)
        Me.txtPrecioNuevo.Name = "txtPrecioNuevo"
        Me.txtPrecioNuevo.Size = New System.Drawing.Size(121, 22)
        Me.txtPrecioNuevo.TabIndex = 53
        Me.txtPrecioNuevo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPrecio
        '
        Me.txtPrecio.BackColor = System.Drawing.SystemColors.Info
        Me.txtPrecio.Location = New System.Drawing.Point(521, 49)
        Me.txtPrecio.Name = "txtPrecio"
        Me.txtPrecio.Size = New System.Drawing.Size(121, 22)
        Me.txtPrecio.TabIndex = 54
        Me.txtPrecio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtDescripcion
        '
        Me.txtDescripcion.BackColor = System.Drawing.SystemColors.Info
        Me.txtDescripcion.Location = New System.Drawing.Point(164, 49)
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(351, 22)
        Me.txtDescripcion.TabIndex = 55
        '
        'txtCodpro
        '
        Me.txtCodpro.BackColor = System.Drawing.SystemColors.Info
        Me.txtCodpro.Location = New System.Drawing.Point(48, 49)
        Me.txtCodpro.Name = "txtCodpro"
        Me.txtCodpro.Size = New System.Drawing.Size(100, 22)
        Me.txtCodpro.TabIndex = 56
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Location = New System.Drawing.Point(931, 29)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(123, 14)
        Me.Label50.TabIndex = 45
        Me.Label50.Text = "Costo Especial Nuevo"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(803, 29)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(79, 14)
        Me.Label27.TabIndex = 46
        Me.Label27.Text = "Precio Nuevo"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(653, 30)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(118, 14)
        Me.Label26.TabIndex = 47
        Me.Label26.Text = "Variación Precio (%)"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(544, 29)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(78, 14)
        Me.Label25.TabIndex = 48
        Me.Label25.Text = "Precio Actual"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(307, 29)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(68, 14)
        Me.Label24.TabIndex = 49
        Me.Label24.Text = "Descripción"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(65, 29)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(57, 14)
        Me.Label23.TabIndex = 50
        Me.Label23.Text = "Producto"
        '
        'dgvDetalleLineaProductos
        '
        Me.dgvDetalleLineaProductos.AllowUserToAddRows = False
        Me.dgvDetalleLineaProductos.AllowUserToDeleteRows = False
        Me.dgvDetalleLineaProductos.AllowUserToResizeRows = False
        Me.dgvDetalleLineaProductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetalleLineaProductos.Location = New System.Drawing.Point(6, 83)
        Me.dgvDetalleLineaProductos.Name = "dgvDetalleLineaProductos"
        Me.dgvDetalleLineaProductos.ReadOnly = True
        Me.dgvDetalleLineaProductos.RowHeadersVisible = False
        Me.dgvDetalleLineaProductos.Size = New System.Drawing.Size(1220, 358)
        Me.dgvDetalleLineaProductos.TabIndex = 44
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.cmbConsumoFinal)
        Me.TabPage3.Controls.Add(Me.Label86)
        Me.TabPage3.Controls.Add(Me.txtPorRebateFinal)
        Me.TabPage3.Controls.Add(Me.txtPorGastoOpFinal)
        Me.TabPage3.Controls.Add(Me.txtPorVentaFinal)
        Me.TabPage3.Controls.Add(Me.txtPorTransFinal)
        Me.TabPage3.Controls.Add(Me.dgvLineaFinal)
        Me.TabPage3.Controls.Add(Me.cmbMarcaPropiaFinal)
        Me.TabPage3.Controls.Add(Me.dgvMpFinal)
        Me.TabPage3.Controls.Add(Me.Label49)
        Me.TabPage3.Controls.Add(Me.Label28)
        Me.TabPage3.Controls.Add(Me.Label29)
        Me.TabPage3.Controls.Add(Me.Label30)
        Me.TabPage3.Controls.Add(Me.Label31)
        Me.TabPage3.Controls.Add(Me.Label32)
        Me.TabPage3.Controls.Add(Me.Label33)
        Me.TabPage3.Controls.Add(Me.Label34)
        Me.TabPage3.Controls.Add(Me.Label35)
        Me.TabPage3.Controls.Add(Me.Label36)
        Me.TabPage3.Controls.Add(Me.Label37)
        Me.TabPage3.Controls.Add(Me.Label38)
        Me.TabPage3.Controls.Add(Me.Label39)
        Me.TabPage3.Controls.Add(Me.Label40)
        Me.TabPage3.Controls.Add(Me.Label41)
        Me.TabPage3.Controls.Add(Me.Label42)
        Me.TabPage3.Controls.Add(Me.Label43)
        Me.TabPage3.Controls.Add(Me.Label55)
        Me.TabPage3.Controls.Add(Me.Label54)
        Me.TabPage3.Controls.Add(Me.Label53)
        Me.TabPage3.Controls.Add(Me.Label44)
        Me.TabPage3.Controls.Add(Me.Label45)
        Me.TabPage3.Controls.Add(Me.Label46)
        Me.TabPage3.Controls.Add(Me.Label47)
        Me.TabPage3.Controls.Add(Me.Label48)
        Me.TabPage3.Controls.Add(Me.txtMgFinal)
        Me.TabPage3.Controls.Add(Me.txtTotGastoFinal)
        Me.TabPage3.Controls.Add(Me.txtTotRebateFinal)
        Me.TabPage3.Controls.Add(Me.txtTotGastOpFinal)
        Me.TabPage3.Controls.Add(Me.txtTotVentaFinal)
        Me.TabPage3.Controls.Add(Me.txtTotTransFinal)
        Me.TabPage3.Controls.Add(Me.txtTotContriFinal)
        Me.TabPage3.Controls.Add(Me.txtPorContriFinal)
        Me.TabPage3.Controls.Add(Me.txtPorVariacion)
        Me.TabPage3.Controls.Add(Me.txtValAct)
        Me.TabPage3.Controls.Add(Me.txtValAnt)
        Me.TabPage3.Controls.Add(Me.txtTotIngresiFinal)
        Me.TabPage3.Controls.Add(Me.txtRazonsFinal)
        Me.TabPage3.Controls.Add(Me.txtNumcotFinal)
        Me.TabPage3.Controls.Add(Me.txtRutCliFinal)
        Me.TabPage3.Controls.Add(Me.btnExcel)
        Me.TabPage3.Location = New System.Drawing.Point(4, 23)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(1284, 502)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Estado Resultado Final"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'cmbConsumoFinal
        '
        Me.cmbConsumoFinal.BackColor = System.Drawing.SystemColors.Info
        Me.cmbConsumoFinal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cmbConsumoFinal.FormattingEnabled = True
        Me.cmbConsumoFinal.Location = New System.Drawing.Point(182, 20)
        Me.cmbConsumoFinal.Name = "cmbConsumoFinal"
        Me.cmbConsumoFinal.Size = New System.Drawing.Size(197, 22)
        Me.cmbConsumoFinal.TabIndex = 92
        '
        'Label86
        '
        Me.Label86.AutoSize = True
        Me.Label86.Location = New System.Drawing.Point(22, 23)
        Me.Label86.Name = "Label86"
        Me.Label86.Size = New System.Drawing.Size(120, 14)
        Me.Label86.TabIndex = 91
        Me.Label86.Text = "Periodo de Consumo"
        '
        'txtPorRebateFinal
        '
        Me.txtPorRebateFinal.Location = New System.Drawing.Point(183, 290)
        Me.txtPorRebateFinal.Name = "txtPorRebateFinal"
        Me.txtPorRebateFinal.Size = New System.Drawing.Size(60, 22)
        Me.txtPorRebateFinal.TabIndex = 85
        Me.txtPorRebateFinal.Text = "0"
        Me.txtPorRebateFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPorGastoOpFinal
        '
        Me.txtPorGastoOpFinal.Location = New System.Drawing.Point(183, 260)
        Me.txtPorGastoOpFinal.Name = "txtPorGastoOpFinal"
        Me.txtPorGastoOpFinal.Size = New System.Drawing.Size(60, 22)
        Me.txtPorGastoOpFinal.TabIndex = 86
        Me.txtPorGastoOpFinal.Text = "0"
        Me.txtPorGastoOpFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPorVentaFinal
        '
        Me.txtPorVentaFinal.Location = New System.Drawing.Point(183, 229)
        Me.txtPorVentaFinal.Name = "txtPorVentaFinal"
        Me.txtPorVentaFinal.Size = New System.Drawing.Size(60, 22)
        Me.txtPorVentaFinal.TabIndex = 87
        Me.txtPorVentaFinal.Text = "0"
        Me.txtPorVentaFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPorTransFinal
        '
        Me.txtPorTransFinal.Location = New System.Drawing.Point(183, 200)
        Me.txtPorTransFinal.Name = "txtPorTransFinal"
        Me.txtPorTransFinal.Size = New System.Drawing.Size(60, 22)
        Me.txtPorTransFinal.TabIndex = 88
        Me.txtPorTransFinal.Text = "0"
        Me.txtPorTransFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'dgvLineaFinal
        '
        Me.dgvLineaFinal.AllowUserToAddRows = False
        Me.dgvLineaFinal.AllowUserToDeleteRows = False
        Me.dgvLineaFinal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLineaFinal.Location = New System.Drawing.Point(641, 265)
        Me.dgvLineaFinal.Name = "dgvLineaFinal"
        Me.dgvLineaFinal.ReadOnly = True
        Me.dgvLineaFinal.RowHeadersVisible = False
        Me.dgvLineaFinal.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLineaFinal.Size = New System.Drawing.Size(636, 202)
        Me.dgvLineaFinal.TabIndex = 83
        '
        'cmbMarcaPropiaFinal
        '
        Me.cmbMarcaPropiaFinal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMarcaPropiaFinal.FormattingEnabled = True
        Me.cmbMarcaPropiaFinal.Location = New System.Drawing.Point(912, 20)
        Me.cmbMarcaPropiaFinal.Name = "cmbMarcaPropiaFinal"
        Me.cmbMarcaPropiaFinal.Size = New System.Drawing.Size(229, 22)
        Me.cmbMarcaPropiaFinal.TabIndex = 82
        '
        'dgvMpFinal
        '
        Me.dgvMpFinal.AllowUserToAddRows = False
        Me.dgvMpFinal.AllowUserToDeleteRows = False
        Me.dgvMpFinal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvMpFinal.Location = New System.Drawing.Point(642, 56)
        Me.dgvMpFinal.Name = "dgvMpFinal"
        Me.dgvMpFinal.ReadOnly = True
        Me.dgvMpFinal.RowHeadersVisible = False
        Me.dgvMpFinal.Size = New System.Drawing.Size(636, 200)
        Me.dgvMpFinal.TabIndex = 81
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Location = New System.Drawing.Point(831, 23)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(75, 14)
        Me.Label49.TabIndex = 80
        Me.Label49.Text = "Marca Propia"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(251, 294)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(19, 14)
        Me.Label28.TabIndex = 75
        Me.Label28.Text = "%"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(251, 264)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(19, 14)
        Me.Label29.TabIndex = 66
        Me.Label29.Text = "%"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(251, 234)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(19, 14)
        Me.Label30.TabIndex = 67
        Me.Label30.Text = "%"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(251, 204)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(19, 14)
        Me.Label31.TabIndex = 68
        Me.Label31.Text = "%"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(281, 294)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(16, 14)
        Me.Label32.TabIndex = 69
        Me.Label32.Text = "="
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(281, 264)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(16, 14)
        Me.Label33.TabIndex = 74
        Me.Label33.Text = "="
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(281, 234)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(16, 14)
        Me.Label34.TabIndex = 71
        Me.Label34.Text = "="
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(281, 204)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(16, 14)
        Me.Label35.TabIndex = 70
        Me.Label35.Text = "="
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(281, 177)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(16, 14)
        Me.Label36.TabIndex = 72
        Me.Label36.Text = "="
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(252, 177)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(19, 14)
        Me.Label37.TabIndex = 73
        Me.Label37.Text = "%"
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(22, 354)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(126, 14)
        Me.Label38.TabIndex = 64
        Me.Label38.Text = "Mg. Final Contribución"
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Location = New System.Drawing.Point(22, 324)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(75, 14)
        Me.Label39.TabIndex = 63
        Me.Label39.Text = "Total Gastos"
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(22, 264)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(102, 14)
        Me.Label40.TabIndex = 62
        Me.Label40.Text = "Gastos Operación"
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Location = New System.Drawing.Point(22, 234)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(40, 14)
        Me.Label41.TabIndex = 61
        Me.Label41.Text = "Venta"
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Location = New System.Drawing.Point(22, 204)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(67, 14)
        Me.Label42.TabIndex = 60
        Me.Label42.Text = "Transporte"
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Location = New System.Drawing.Point(22, 173)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(107, 14)
        Me.Label43.TabIndex = 59
        Me.Label43.Text = "Total Contribución"
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Location = New System.Drawing.Point(57, 454)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(55, 14)
        Me.Label55.TabIndex = 65
        Me.Label55.Text = "Variación"
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Location = New System.Drawing.Point(57, 423)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(99, 14)
        Me.Label54.TabIndex = 65
        Me.Label54.Text = "Valorizado Actual"
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.Location = New System.Drawing.Point(57, 393)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(109, 14)
        Me.Label53.TabIndex = 65
        Me.Label53.Text = "Valorizado Anterior"
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Location = New System.Drawing.Point(22, 143)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(85, 14)
        Me.Label44.TabIndex = 65
        Me.Label44.Text = "Total Ingresos"
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Location = New System.Drawing.Point(22, 113)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(73, 14)
        Me.Label45.TabIndex = 58
        Me.Label45.Text = "Razón Social"
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Location = New System.Drawing.Point(22, 294)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(46, 14)
        Me.Label46.TabIndex = 55
        Me.Label46.Text = "Rebate"
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Location = New System.Drawing.Point(22, 53)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(108, 14)
        Me.Label47.TabIndex = 57
        Me.Label47.Text = "Número Cotización"
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Location = New System.Drawing.Point(22, 83)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(67, 14)
        Me.Label48.TabIndex = 56
        Me.Label48.Text = "Rut Cliente"
        '
        'txtMgFinal
        '
        Me.txtMgFinal.BackColor = System.Drawing.SystemColors.Info
        Me.txtMgFinal.Location = New System.Drawing.Point(315, 351)
        Me.txtMgFinal.Name = "txtMgFinal"
        Me.txtMgFinal.Size = New System.Drawing.Size(160, 22)
        Me.txtMgFinal.TabIndex = 45
        Me.txtMgFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotGastoFinal
        '
        Me.txtTotGastoFinal.BackColor = System.Drawing.SystemColors.Info
        Me.txtTotGastoFinal.Location = New System.Drawing.Point(315, 321)
        Me.txtTotGastoFinal.Name = "txtTotGastoFinal"
        Me.txtTotGastoFinal.Size = New System.Drawing.Size(160, 22)
        Me.txtTotGastoFinal.TabIndex = 46
        Me.txtTotGastoFinal.Text = "0"
        Me.txtTotGastoFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotRebateFinal
        '
        Me.txtTotRebateFinal.BackColor = System.Drawing.SystemColors.Info
        Me.txtTotRebateFinal.Location = New System.Drawing.Point(315, 291)
        Me.txtTotRebateFinal.Name = "txtTotRebateFinal"
        Me.txtTotRebateFinal.Size = New System.Drawing.Size(160, 22)
        Me.txtTotRebateFinal.TabIndex = 47
        Me.txtTotRebateFinal.Text = "0"
        Me.txtTotRebateFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotGastOpFinal
        '
        Me.txtTotGastOpFinal.BackColor = System.Drawing.SystemColors.Info
        Me.txtTotGastOpFinal.Location = New System.Drawing.Point(315, 261)
        Me.txtTotGastOpFinal.Name = "txtTotGastOpFinal"
        Me.txtTotGastOpFinal.Size = New System.Drawing.Size(160, 22)
        Me.txtTotGastOpFinal.TabIndex = 48
        Me.txtTotGastOpFinal.Text = "0"
        Me.txtTotGastOpFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotVentaFinal
        '
        Me.txtTotVentaFinal.BackColor = System.Drawing.SystemColors.Info
        Me.txtTotVentaFinal.Location = New System.Drawing.Point(315, 231)
        Me.txtTotVentaFinal.Name = "txtTotVentaFinal"
        Me.txtTotVentaFinal.Size = New System.Drawing.Size(160, 22)
        Me.txtTotVentaFinal.TabIndex = 49
        Me.txtTotVentaFinal.Text = "0"
        Me.txtTotVentaFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotTransFinal
        '
        Me.txtTotTransFinal.BackColor = System.Drawing.SystemColors.Info
        Me.txtTotTransFinal.Location = New System.Drawing.Point(315, 200)
        Me.txtTotTransFinal.Name = "txtTotTransFinal"
        Me.txtTotTransFinal.Size = New System.Drawing.Size(160, 22)
        Me.txtTotTransFinal.TabIndex = 50
        Me.txtTotTransFinal.Text = "0"
        Me.txtTotTransFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotContriFinal
        '
        Me.txtTotContriFinal.BackColor = System.Drawing.SystemColors.Info
        Me.txtTotContriFinal.Location = New System.Drawing.Point(315, 170)
        Me.txtTotContriFinal.Name = "txtTotContriFinal"
        Me.txtTotContriFinal.Size = New System.Drawing.Size(160, 22)
        Me.txtTotContriFinal.TabIndex = 51
        Me.txtTotContriFinal.Text = "0"
        Me.txtTotContriFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPorContriFinal
        '
        Me.txtPorContriFinal.BackColor = System.Drawing.SystemColors.Info
        Me.txtPorContriFinal.Location = New System.Drawing.Point(182, 170)
        Me.txtPorContriFinal.Name = "txtPorContriFinal"
        Me.txtPorContriFinal.Size = New System.Drawing.Size(60, 22)
        Me.txtPorContriFinal.TabIndex = 52
        Me.txtPorContriFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPorVariacion
        '
        Me.txtPorVariacion.BackColor = System.Drawing.SystemColors.Info
        Me.txtPorVariacion.Location = New System.Drawing.Point(191, 450)
        Me.txtPorVariacion.Name = "txtPorVariacion"
        Me.txtPorVariacion.Size = New System.Drawing.Size(135, 22)
        Me.txtPorVariacion.TabIndex = 53
        Me.txtPorVariacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtValAct
        '
        Me.txtValAct.BackColor = System.Drawing.SystemColors.Info
        Me.txtValAct.Location = New System.Drawing.Point(191, 420)
        Me.txtValAct.Name = "txtValAct"
        Me.txtValAct.Size = New System.Drawing.Size(135, 22)
        Me.txtValAct.TabIndex = 53
        Me.txtValAct.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtValAnt
        '
        Me.txtValAnt.BackColor = System.Drawing.SystemColors.Info
        Me.txtValAnt.Location = New System.Drawing.Point(191, 390)
        Me.txtValAnt.Name = "txtValAnt"
        Me.txtValAnt.Size = New System.Drawing.Size(135, 22)
        Me.txtValAnt.TabIndex = 53
        Me.txtValAnt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotIngresiFinal
        '
        Me.txtTotIngresiFinal.BackColor = System.Drawing.SystemColors.Info
        Me.txtTotIngresiFinal.Location = New System.Drawing.Point(182, 140)
        Me.txtTotIngresiFinal.Name = "txtTotIngresiFinal"
        Me.txtTotIngresiFinal.Size = New System.Drawing.Size(135, 22)
        Me.txtTotIngresiFinal.TabIndex = 53
        Me.txtTotIngresiFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtRazonsFinal
        '
        Me.txtRazonsFinal.BackColor = System.Drawing.SystemColors.Info
        Me.txtRazonsFinal.Location = New System.Drawing.Point(182, 110)
        Me.txtRazonsFinal.Name = "txtRazonsFinal"
        Me.txtRazonsFinal.Size = New System.Drawing.Size(452, 22)
        Me.txtRazonsFinal.TabIndex = 54
        '
        'txtNumcotFinal
        '
        Me.txtNumcotFinal.BackColor = System.Drawing.SystemColors.Info
        Me.txtNumcotFinal.Location = New System.Drawing.Point(182, 50)
        Me.txtNumcotFinal.Name = "txtNumcotFinal"
        Me.txtNumcotFinal.Size = New System.Drawing.Size(135, 22)
        Me.txtNumcotFinal.TabIndex = 44
        '
        'txtRutCliFinal
        '
        Me.txtRutCliFinal.BackColor = System.Drawing.SystemColors.Info
        Me.txtRutCliFinal.Location = New System.Drawing.Point(182, 80)
        Me.txtRutCliFinal.Name = "txtRutCliFinal"
        Me.txtRutCliFinal.Size = New System.Drawing.Size(135, 22)
        Me.txtRutCliFinal.TabIndex = 43
        '
        'btnExcel
        '
        Me.btnExcel.Image = Global.Convenios_New.My.Resources.Resources.Excel
        Me.btnExcel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnExcel.Location = New System.Drawing.Point(474, 406)
        Me.btnExcel.Name = "btnExcel"
        Me.btnExcel.Size = New System.Drawing.Size(161, 56)
        Me.btnExcel.TabIndex = 84
        Me.btnExcel.Text = "Exportar a Excel"
        Me.btnExcel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnExcel.UseVisualStyleBackColor = True
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.Label74)
        Me.TabPage4.Controls.Add(Me.GroupBox2)
        Me.TabPage4.Controls.Add(Me.GroupBox1)
        Me.TabPage4.Controls.Add(Me.txtmgant)
        Me.TabPage4.Controls.Add(Me.txtmgdif)
        Me.TabPage4.Controls.Add(Me.txtsumaant)
        Me.TabPage4.Controls.Add(Me.txtsumDif)
        Me.TabPage4.Controls.Add(Me.txtrebant)
        Me.TabPage4.Controls.Add(Me.txtrebdif)
        Me.TabPage4.Controls.Add(Me.txtgasant)
        Me.TabPage4.Controls.Add(Me.txtgasdif)
        Me.TabPage4.Controls.Add(Me.txtvenant)
        Me.TabPage4.Controls.Add(Me.txtventdif)
        Me.TabPage4.Controls.Add(Me.txtmgnew)
        Me.TabPage4.Controls.Add(Me.txtsumanew)
        Me.TabPage4.Controls.Add(Me.txttranant)
        Me.TabPage4.Controls.Add(Me.txtrebnew)
        Me.TabPage4.Controls.Add(Me.txttrandif)
        Me.TabPage4.Controls.Add(Me.txtgasnew)
        Me.TabPage4.Controls.Add(Me.txtcontant)
        Me.TabPage4.Controls.Add(Me.txtvennew)
        Me.TabPage4.Controls.Add(Me.txtcontdif)
        Me.TabPage4.Controls.Add(Me.txttrannew)
        Me.TabPage4.Controls.Add(Me.txtingAnt)
        Me.TabPage4.Controls.Add(Me.txtcontnew)
        Me.TabPage4.Controls.Add(Me.txtingdif)
        Me.TabPage4.Controls.Add(Me.txtingnew)
        Me.TabPage4.Controls.Add(Me.Label60)
        Me.TabPage4.Controls.Add(Me.Label61)
        Me.TabPage4.Controls.Add(Me.Label66)
        Me.TabPage4.Controls.Add(Me.Label67)
        Me.TabPage4.Controls.Add(Me.Label65)
        Me.TabPage4.Controls.Add(Me.Label64)
        Me.TabPage4.Controls.Add(Me.Label63)
        Me.TabPage4.Controls.Add(Me.Label62)
        Me.TabPage4.Controls.Add(Me.Label68)
        Me.TabPage4.Controls.Add(Me.Label69)
        Me.TabPage4.Controls.Add(Me.Label70)
        Me.TabPage4.Controls.Add(Me.txtobserv)
        Me.TabPage4.Controls.Add(Me.Label71)
        Me.TabPage4.Location = New System.Drawing.Point(4, 23)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(1284, 502)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Comparativo EERR"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'Label74
        '
        Me.Label74.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.Label74.Location = New System.Drawing.Point(551, 176)
        Me.Label74.Name = "Label74"
        Me.Label74.Size = New System.Drawing.Size(689, 51)
        Me.Label74.TabIndex = 46
        Me.Label74.Text = "Nota : Para que el programa considere las fechas de vencimiento de cada línea, es" &
    "tas se deben agregar ANTES de grabar los datos del reajuste."
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chkMarcaTodoConvenio)
        Me.GroupBox2.Controls.Add(Me.btn_Grabar)
        Me.GroupBox2.Controls.Add(Me.dgvRelComercial)
        Me.GroupBox2.Location = New System.Drawing.Point(693, 241)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(585, 250)
        Me.GroupBox2.TabIndex = 45
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Agregar Clientes Relacionados y Fechas de Convenio"
        '
        'chkMarcaTodoConvenio
        '
        Me.chkMarcaTodoConvenio.AutoSize = True
        Me.chkMarcaTodoConvenio.Location = New System.Drawing.Point(10, 24)
        Me.chkMarcaTodoConvenio.Name = "chkMarcaTodoConvenio"
        Me.chkMarcaTodoConvenio.Size = New System.Drawing.Size(90, 18)
        Me.chkMarcaTodoConvenio.TabIndex = 34
        Me.chkMarcaTodoConvenio.Text = "Marca Todo"
        Me.chkMarcaTodoConvenio.UseVisualStyleBackColor = True
        '
        'btn_Grabar
        '
        Me.btn_Grabar.Image = Global.Convenios_New.My.Resources.Resources.Floppy
        Me.btn_Grabar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_Grabar.Location = New System.Drawing.Point(429, 197)
        Me.btn_Grabar.Name = "btn_Grabar"
        Me.btn_Grabar.Size = New System.Drawing.Size(120, 46)
        Me.btn_Grabar.TabIndex = 5
        Me.btn_Grabar.Text = "Grabar Datos Reajuste"
        Me.btn_Grabar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_Grabar.UseVisualStyleBackColor = True
        '
        'dgvRelComercial
        '
        Me.dgvRelComercial.AllowUserToAddRows = False
        Me.dgvRelComercial.AllowUserToDeleteRows = False
        Me.dgvRelComercial.AllowUserToResizeRows = False
        Me.dgvRelComercial.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvRelComercial.Location = New System.Drawing.Point(10, 47)
        Me.dgvRelComercial.Name = "dgvRelComercial"
        Me.dgvRelComercial.ReadOnly = True
        Me.dgvRelComercial.RowHeadersVisible = False
        Me.dgvRelComercial.Size = New System.Drawing.Size(539, 144)
        Me.dgvRelComercial.TabIndex = 33
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dgvLineasReajuste)
        Me.GroupBox1.Controls.Add(Me.Label84)
        Me.GroupBox1.Controls.Add(Me.dtpFecVenConvenio)
        Me.GroupBox1.Controls.Add(Me.dtpFechaLineaReajuste)
        Me.GroupBox1.Controls.Add(Me.btn_AgregaFecha)
        Me.GroupBox1.Controls.Add(Me.Label73)
        Me.GroupBox1.Controls.Add(Me.Label83)
        Me.GroupBox1.Controls.Add(Me.Label82)
        Me.GroupBox1.Controls.Add(Me.dtpFeciniConvenio)
        Me.GroupBox1.Controls.Add(Me.chkLineas)
        Me.GroupBox1.Location = New System.Drawing.Point(16, 241)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(657, 250)
        Me.GroupBox1.TabIndex = 44
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Agregar fechas vencimiento de las líneas"
        '
        'dgvLineasReajuste
        '
        Me.dgvLineasReajuste.AllowUserToAddRows = False
        Me.dgvLineasReajuste.AllowUserToDeleteRows = False
        Me.dgvLineasReajuste.AllowUserToResizeRows = False
        Me.dgvLineasReajuste.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLineasReajuste.Location = New System.Drawing.Point(10, 47)
        Me.dgvLineasReajuste.Name = "dgvLineasReajuste"
        Me.dgvLineasReajuste.ReadOnly = True
        Me.dgvLineasReajuste.RowHeadersVisible = False
        Me.dgvLineasReajuste.Size = New System.Drawing.Size(525, 144)
        Me.dgvLineasReajuste.TabIndex = 24
        '
        'Label84
        '
        Me.Label84.AutoSize = True
        Me.Label84.Location = New System.Drawing.Point(18, 203)
        Me.Label84.Name = "Label84"
        Me.Label84.Size = New System.Drawing.Size(143, 14)
        Me.Label84.TabIndex = 27
        Me.Label84.Text = "Fecha Vencimiento Línea"
        '
        'dtpFecVenConvenio
        '
        Me.dtpFecVenConvenio.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecVenConvenio.Location = New System.Drawing.Point(320, 221)
        Me.dtpFecVenConvenio.Name = "dtpFecVenConvenio"
        Me.dtpFecVenConvenio.Size = New System.Drawing.Size(130, 22)
        Me.dtpFecVenConvenio.TabIndex = 31
        '
        'dtpFechaLineaReajuste
        '
        Me.dtpFechaLineaReajuste.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaLineaReajuste.Location = New System.Drawing.Point(21, 220)
        Me.dtpFechaLineaReajuste.Name = "dtpFechaLineaReajuste"
        Me.dtpFechaLineaReajuste.Size = New System.Drawing.Size(110, 22)
        Me.dtpFechaLineaReajuste.TabIndex = 26
        '
        'btn_AgregaFecha
        '
        Me.btn_AgregaFecha.Location = New System.Drawing.Point(539, 203)
        Me.btn_AgregaFecha.Name = "btn_AgregaFecha"
        Me.btn_AgregaFecha.Size = New System.Drawing.Size(110, 40)
        Me.btn_AgregaFecha.TabIndex = 28
        Me.btn_AgregaFecha.Text = "Modificar Fechas"
        Me.btn_AgregaFecha.UseVisualStyleBackColor = True
        '
        'Label73
        '
        Me.Label73.AutoSize = True
        Me.Label73.Location = New System.Drawing.Point(317, 203)
        Me.Label73.Name = "Label73"
        Me.Label73.Size = New System.Drawing.Size(99, 14)
        Me.Label73.TabIndex = 32
        Me.Label73.Text = "Convenio Hasta :"
        '
        'Label83
        '
        Me.Label83.AutoSize = True
        Me.Label83.Location = New System.Drawing.Point(143, 30)
        Me.Label83.Name = "Label83"
        Me.Label83.Size = New System.Drawing.Size(127, 14)
        Me.Label83.TabIndex = 25
        Me.Label83.Text = "Líneas de la cotización"
        '
        'Label82
        '
        Me.Label82.AutoSize = True
        Me.Label82.Location = New System.Drawing.Point(167, 203)
        Me.Label82.Name = "Label82"
        Me.Label82.Size = New System.Drawing.Size(103, 14)
        Me.Label82.TabIndex = 32
        Me.Label82.Text = "Convenio Desde :"
        '
        'dtpFeciniConvenio
        '
        Me.dtpFeciniConvenio.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFeciniConvenio.Location = New System.Drawing.Point(170, 221)
        Me.dtpFeciniConvenio.Name = "dtpFeciniConvenio"
        Me.dtpFeciniConvenio.Size = New System.Drawing.Size(130, 22)
        Me.dtpFeciniConvenio.TabIndex = 31
        '
        'chkLineas
        '
        Me.chkLineas.AutoSize = True
        Me.chkLineas.Location = New System.Drawing.Point(7, 30)
        Me.chkLineas.Name = "chkLineas"
        Me.chkLineas.Size = New System.Drawing.Size(90, 18)
        Me.chkLineas.TabIndex = 29
        Me.chkLineas.Text = "Marca Todo"
        Me.chkLineas.UseVisualStyleBackColor = True
        '
        'txtmgant
        '
        Me.txtmgant.BackColor = System.Drawing.SystemColors.Info
        Me.txtmgant.Location = New System.Drawing.Point(925, 43)
        Me.txtmgant.Name = "txtmgant"
        Me.txtmgant.Size = New System.Drawing.Size(100, 22)
        Me.txtmgant.TabIndex = 27
        Me.txtmgant.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtmgdif
        '
        Me.txtmgdif.BackColor = System.Drawing.SystemColors.Info
        Me.txtmgdif.Location = New System.Drawing.Point(924, 122)
        Me.txtmgdif.Name = "txtmgdif"
        Me.txtmgdif.Size = New System.Drawing.Size(100, 22)
        Me.txtmgdif.TabIndex = 28
        Me.txtmgdif.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtsumaant
        '
        Me.txtsumaant.BackColor = System.Drawing.SystemColors.Info
        Me.txtsumaant.Location = New System.Drawing.Point(812, 43)
        Me.txtsumaant.Name = "txtsumaant"
        Me.txtsumaant.Size = New System.Drawing.Size(100, 22)
        Me.txtsumaant.TabIndex = 29
        Me.txtsumaant.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtsumDif
        '
        Me.txtsumDif.BackColor = System.Drawing.SystemColors.Info
        Me.txtsumDif.Location = New System.Drawing.Point(811, 122)
        Me.txtsumDif.Name = "txtsumDif"
        Me.txtsumDif.Size = New System.Drawing.Size(100, 22)
        Me.txtsumDif.TabIndex = 30
        Me.txtsumDif.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtrebant
        '
        Me.txtrebant.BackColor = System.Drawing.SystemColors.Info
        Me.txtrebant.Location = New System.Drawing.Point(699, 43)
        Me.txtrebant.Name = "txtrebant"
        Me.txtrebant.Size = New System.Drawing.Size(100, 22)
        Me.txtrebant.TabIndex = 31
        Me.txtrebant.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtrebdif
        '
        Me.txtrebdif.BackColor = System.Drawing.SystemColors.Info
        Me.txtrebdif.Location = New System.Drawing.Point(698, 122)
        Me.txtrebdif.Name = "txtrebdif"
        Me.txtrebdif.Size = New System.Drawing.Size(100, 22)
        Me.txtrebdif.TabIndex = 32
        Me.txtrebdif.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtgasant
        '
        Me.txtgasant.BackColor = System.Drawing.SystemColors.Info
        Me.txtgasant.Location = New System.Drawing.Point(583, 43)
        Me.txtgasant.Name = "txtgasant"
        Me.txtgasant.Size = New System.Drawing.Size(100, 22)
        Me.txtgasant.TabIndex = 33
        Me.txtgasant.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtgasdif
        '
        Me.txtgasdif.BackColor = System.Drawing.SystemColors.Info
        Me.txtgasdif.Location = New System.Drawing.Point(582, 122)
        Me.txtgasdif.Name = "txtgasdif"
        Me.txtgasdif.Size = New System.Drawing.Size(100, 22)
        Me.txtgasdif.TabIndex = 34
        Me.txtgasdif.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtvenant
        '
        Me.txtvenant.BackColor = System.Drawing.SystemColors.Info
        Me.txtvenant.Location = New System.Drawing.Point(468, 43)
        Me.txtvenant.Name = "txtvenant"
        Me.txtvenant.Size = New System.Drawing.Size(100, 22)
        Me.txtvenant.TabIndex = 35
        Me.txtvenant.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtventdif
        '
        Me.txtventdif.BackColor = System.Drawing.SystemColors.Info
        Me.txtventdif.Location = New System.Drawing.Point(467, 122)
        Me.txtventdif.Name = "txtventdif"
        Me.txtventdif.Size = New System.Drawing.Size(100, 22)
        Me.txtventdif.TabIndex = 36
        Me.txtventdif.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtmgnew
        '
        Me.txtmgnew.BackColor = System.Drawing.SystemColors.Info
        Me.txtmgnew.Location = New System.Drawing.Point(925, 73)
        Me.txtmgnew.Name = "txtmgnew"
        Me.txtmgnew.Size = New System.Drawing.Size(100, 22)
        Me.txtmgnew.TabIndex = 37
        Me.txtmgnew.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtsumanew
        '
        Me.txtsumanew.BackColor = System.Drawing.SystemColors.Info
        Me.txtsumanew.Location = New System.Drawing.Point(812, 73)
        Me.txtsumanew.Name = "txtsumanew"
        Me.txtsumanew.Size = New System.Drawing.Size(100, 22)
        Me.txtsumanew.TabIndex = 38
        Me.txtsumanew.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txttranant
        '
        Me.txttranant.BackColor = System.Drawing.SystemColors.Info
        Me.txttranant.Location = New System.Drawing.Point(351, 43)
        Me.txttranant.Name = "txttranant"
        Me.txttranant.Size = New System.Drawing.Size(100, 22)
        Me.txttranant.TabIndex = 39
        Me.txttranant.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtrebnew
        '
        Me.txtrebnew.BackColor = System.Drawing.SystemColors.Info
        Me.txtrebnew.Location = New System.Drawing.Point(699, 73)
        Me.txtrebnew.Name = "txtrebnew"
        Me.txtrebnew.Size = New System.Drawing.Size(100, 22)
        Me.txtrebnew.TabIndex = 40
        Me.txtrebnew.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txttrandif
        '
        Me.txttrandif.BackColor = System.Drawing.SystemColors.Info
        Me.txttrandif.Location = New System.Drawing.Point(350, 122)
        Me.txttrandif.Name = "txttrandif"
        Me.txttrandif.Size = New System.Drawing.Size(100, 22)
        Me.txttrandif.TabIndex = 41
        Me.txttrandif.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtgasnew
        '
        Me.txtgasnew.BackColor = System.Drawing.SystemColors.Info
        Me.txtgasnew.Location = New System.Drawing.Point(583, 73)
        Me.txtgasnew.Name = "txtgasnew"
        Me.txtgasnew.Size = New System.Drawing.Size(100, 22)
        Me.txtgasnew.TabIndex = 26
        Me.txtgasnew.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtcontant
        '
        Me.txtcontant.BackColor = System.Drawing.SystemColors.Info
        Me.txtcontant.Location = New System.Drawing.Point(236, 43)
        Me.txtcontant.Name = "txtcontant"
        Me.txtcontant.Size = New System.Drawing.Size(100, 22)
        Me.txtcontant.TabIndex = 42
        Me.txtcontant.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtvennew
        '
        Me.txtvennew.BackColor = System.Drawing.SystemColors.Info
        Me.txtvennew.Location = New System.Drawing.Point(468, 73)
        Me.txtvennew.Name = "txtvennew"
        Me.txtvennew.Size = New System.Drawing.Size(100, 22)
        Me.txtvennew.TabIndex = 25
        Me.txtvennew.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtcontdif
        '
        Me.txtcontdif.BackColor = System.Drawing.SystemColors.Info
        Me.txtcontdif.Location = New System.Drawing.Point(235, 122)
        Me.txtcontdif.Name = "txtcontdif"
        Me.txtcontdif.Size = New System.Drawing.Size(100, 22)
        Me.txtcontdif.TabIndex = 19
        Me.txtcontdif.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txttrannew
        '
        Me.txttrannew.BackColor = System.Drawing.SystemColors.Info
        Me.txttrannew.Location = New System.Drawing.Point(351, 73)
        Me.txttrannew.Name = "txttrannew"
        Me.txttrannew.Size = New System.Drawing.Size(100, 22)
        Me.txttrannew.TabIndex = 24
        Me.txttrannew.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtingAnt
        '
        Me.txtingAnt.BackColor = System.Drawing.SystemColors.Info
        Me.txtingAnt.Location = New System.Drawing.Point(119, 43)
        Me.txtingAnt.Name = "txtingAnt"
        Me.txtingAnt.Size = New System.Drawing.Size(100, 22)
        Me.txtingAnt.TabIndex = 23
        Me.txtingAnt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtcontnew
        '
        Me.txtcontnew.BackColor = System.Drawing.SystemColors.Info
        Me.txtcontnew.Location = New System.Drawing.Point(236, 73)
        Me.txtcontnew.Name = "txtcontnew"
        Me.txtcontnew.Size = New System.Drawing.Size(100, 22)
        Me.txtcontnew.TabIndex = 22
        Me.txtcontnew.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtingdif
        '
        Me.txtingdif.BackColor = System.Drawing.SystemColors.Info
        Me.txtingdif.Location = New System.Drawing.Point(118, 122)
        Me.txtingdif.Name = "txtingdif"
        Me.txtingdif.Size = New System.Drawing.Size(100, 22)
        Me.txtingdif.TabIndex = 21
        Me.txtingdif.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtingnew
        '
        Me.txtingnew.BackColor = System.Drawing.SystemColors.Info
        Me.txtingnew.Location = New System.Drawing.Point(119, 73)
        Me.txtingnew.Name = "txtingnew"
        Me.txtingnew.Size = New System.Drawing.Size(100, 22)
        Me.txtingnew.TabIndex = 20
        Me.txtingnew.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.Location = New System.Drawing.Point(10, 125)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(55, 14)
        Me.Label60.TabIndex = 9
        Me.Label60.Text = "Variación"
        '
        'Label61
        '
        Me.Label61.AutoSize = True
        Me.Label61.Location = New System.Drawing.Point(11, 76)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(74, 14)
        Me.Label61.TabIndex = 13
        Me.Label61.Text = "EERR Nuevo"
        '
        'Label66
        '
        Me.Label66.AutoSize = True
        Me.Label66.Location = New System.Drawing.Point(946, 26)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(54, 14)
        Me.Label66.TabIndex = 18
        Me.Label66.Text = "Mg. Final"
        '
        'Label67
        '
        Me.Label67.AutoSize = True
        Me.Label67.Location = New System.Drawing.Point(825, 26)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(77, 14)
        Me.Label67.TabIndex = 11
        Me.Label67.Text = "Suma Gastos"
        '
        'Label65
        '
        Me.Label65.AutoSize = True
        Me.Label65.Location = New System.Drawing.Point(249, 26)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(75, 14)
        Me.Label65.TabIndex = 12
        Me.Label65.Text = "Contribución"
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.Location = New System.Drawing.Point(722, 26)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(46, 14)
        Me.Label64.TabIndex = 17
        Me.Label64.Text = "Rebate"
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.Location = New System.Drawing.Point(581, 26)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(102, 14)
        Me.Label63.TabIndex = 14
        Me.Label63.Text = "Gastos Operación"
        '
        'Label62
        '
        Me.Label62.AutoSize = True
        Me.Label62.Location = New System.Drawing.Point(495, 26)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(40, 14)
        Me.Label62.TabIndex = 15
        Me.Label62.Text = "Venta"
        '
        'Label68
        '
        Me.Label68.AutoSize = True
        Me.Label68.Location = New System.Drawing.Point(367, 26)
        Me.Label68.Name = "Label68"
        Me.Label68.Size = New System.Drawing.Size(67, 14)
        Me.Label68.TabIndex = 16
        Me.Label68.Text = "Transporte"
        '
        'Label69
        '
        Me.Label69.AutoSize = True
        Me.Label69.Location = New System.Drawing.Point(141, 26)
        Me.Label69.Name = "Label69"
        Me.Label69.Size = New System.Drawing.Size(53, 14)
        Me.Label69.TabIndex = 8
        Me.Label69.Text = "Ingresos"
        '
        'Label70
        '
        Me.Label70.AutoSize = True
        Me.Label70.Location = New System.Drawing.Point(11, 46)
        Me.Label70.Name = "Label70"
        Me.Label70.Size = New System.Drawing.Size(83, 14)
        Me.Label70.TabIndex = 10
        Me.Label70.Text = "EERR Anterior"
        '
        'txtobserv
        '
        Me.txtobserv.Location = New System.Drawing.Point(14, 173)
        Me.txtobserv.MaxLength = 500
        Me.txtobserv.Multiline = True
        Me.txtobserv.Name = "txtobserv"
        Me.txtobserv.Size = New System.Drawing.Size(521, 62)
        Me.txtobserv.TabIndex = 7
        '
        'Label71
        '
        Me.Label71.AutoSize = True
        Me.Label71.Location = New System.Drawing.Point(11, 155)
        Me.Label71.Name = "Label71"
        Me.Label71.Size = New System.Drawing.Size(73, 14)
        Me.Label71.TabIndex = 6
        Me.Label71.Text = "Observación"
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.grbBotones)
        Me.TabPage5.Location = New System.Drawing.Point(4, 23)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage5.Size = New System.Drawing.Size(1284, 502)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "Carga de Costos y Correo"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'grbBotones
        '
        Me.grbBotones.Controls.Add(Me.chkMarcaTodoCosEsp)
        Me.grbBotones.Controls.Add(Me.txtInfoExcel)
        Me.grbBotones.Controls.Add(Me.Label75)
        Me.grbBotones.Controls.Add(Me.btn_Correo)
        Me.grbBotones.Controls.Add(Me.Label72)
        Me.grbBotones.Controls.Add(Me.Label76)
        Me.grbBotones.Controls.Add(Me.dtpCosEspFin)
        Me.grbBotones.Controls.Add(Me.dtpCosEspIni)
        Me.grbBotones.Controls.Add(Me.Label77)
        Me.grbBotones.Controls.Add(Me.Label78)
        Me.grbBotones.Controls.Add(Me.txtCC3)
        Me.grbBotones.Controls.Add(Me.txtCC2)
        Me.grbBotones.Controls.Add(Me.txtCC1)
        Me.grbBotones.Controls.Add(Me.txtMail3)
        Me.grbBotones.Controls.Add(Me.txtMail2)
        Me.grbBotones.Controls.Add(Me.txtMail1)
        Me.grbBotones.Controls.Add(Me.Label79)
        Me.grbBotones.Controls.Add(Me.dgvEmpRelacion)
        Me.grbBotones.Controls.Add(Me.btn_CostoEspecial)
        Me.grbBotones.Enabled = False
        Me.grbBotones.Location = New System.Drawing.Point(6, 6)
        Me.grbBotones.Name = "grbBotones"
        Me.grbBotones.Size = New System.Drawing.Size(1219, 433)
        Me.grbBotones.TabIndex = 8
        Me.grbBotones.TabStop = False
        Me.grbBotones.Text = "Envio de Correo"
        '
        'chkMarcaTodoCosEsp
        '
        Me.chkMarcaTodoCosEsp.AutoSize = True
        Me.chkMarcaTodoCosEsp.Location = New System.Drawing.Point(630, 113)
        Me.chkMarcaTodoCosEsp.Name = "chkMarcaTodoCosEsp"
        Me.chkMarcaTodoCosEsp.Size = New System.Drawing.Size(90, 18)
        Me.chkMarcaTodoCosEsp.TabIndex = 16
        Me.chkMarcaTodoCosEsp.Text = "Marca Todo"
        Me.chkMarcaTodoCosEsp.UseVisualStyleBackColor = True
        Me.chkMarcaTodoCosEsp.Visible = False
        '
        'txtInfoExcel
        '
        Me.txtInfoExcel.Location = New System.Drawing.Point(142, 38)
        Me.txtInfoExcel.Multiline = True
        Me.txtInfoExcel.Name = "txtInfoExcel"
        Me.txtInfoExcel.Size = New System.Drawing.Size(472, 60)
        Me.txtInfoExcel.TabIndex = 15
        '
        'Label75
        '
        Me.Label75.AutoSize = True
        Me.Label75.Location = New System.Drawing.Point(138, 18)
        Me.Label75.Name = "Label75"
        Me.Label75.Size = New System.Drawing.Size(248, 14)
        Me.Label75.TabIndex = 14
        Me.Label75.Text = "Texto Informativo Excel : ""Precios tienen...."
        '
        'btn_Correo
        '
        Me.btn_Correo.Image = Global.Convenios_New.My.Resources.Resources.Excel
        Me.btn_Correo.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_Correo.Location = New System.Drawing.Point(6, 38)
        Me.btn_Correo.Name = "btn_Correo"
        Me.btn_Correo.Size = New System.Drawing.Size(129, 46)
        Me.btn_Correo.TabIndex = 5
        Me.btn_Correo.Text = "Envia Correo Ejecutivo"
        Me.btn_Correo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_Correo.UseVisualStyleBackColor = True
        '
        'Label72
        '
        Me.Label72.AutoSize = True
        Me.Label72.Location = New System.Drawing.Point(900, 34)
        Me.Label72.Name = "Label72"
        Me.Label72.Size = New System.Drawing.Size(45, 14)
        Me.Label72.TabIndex = 11
        Me.Label72.Text = "Hasta :"
        Me.Label72.Visible = False
        '
        'Label76
        '
        Me.Label76.AutoSize = True
        Me.Label76.Location = New System.Drawing.Point(764, 34)
        Me.Label76.Name = "Label76"
        Me.Label76.Size = New System.Drawing.Size(49, 14)
        Me.Label76.TabIndex = 11
        Me.Label76.Text = "Desde :"
        Me.Label76.Visible = False
        '
        'dtpCosEspFin
        '
        Me.dtpCosEspFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpCosEspFin.Location = New System.Drawing.Point(900, 55)
        Me.dtpCosEspFin.Name = "dtpCosEspFin"
        Me.dtpCosEspFin.Size = New System.Drawing.Size(130, 22)
        Me.dtpCosEspFin.TabIndex = 10
        Me.dtpCosEspFin.Visible = False
        '
        'dtpCosEspIni
        '
        Me.dtpCosEspIni.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpCosEspIni.Location = New System.Drawing.Point(764, 55)
        Me.dtpCosEspIni.Name = "dtpCosEspIni"
        Me.dtpCosEspIni.Size = New System.Drawing.Size(130, 22)
        Me.dtpCosEspIni.TabIndex = 10
        Me.dtpCosEspIni.Visible = False
        '
        'Label77
        '
        Me.Label77.AutoSize = True
        Me.Label77.Location = New System.Drawing.Point(14, 149)
        Me.Label77.Name = "Label77"
        Me.Label77.Size = New System.Drawing.Size(29, 14)
        Me.Label77.TabIndex = 9
        Me.Label77.Text = "CC :"
        '
        'Label78
        '
        Me.Label78.AutoSize = True
        Me.Label78.Location = New System.Drawing.Point(14, 109)
        Me.Label78.Name = "Label78"
        Me.Label78.Size = New System.Drawing.Size(42, 14)
        Me.Label78.TabIndex = 9
        Me.Label78.Text = "Para : "
        '
        'txtCC3
        '
        Me.txtCC3.Location = New System.Drawing.Point(434, 146)
        Me.txtCC3.Name = "txtCC3"
        Me.txtCC3.Size = New System.Drawing.Size(180, 22)
        Me.txtCC3.TabIndex = 8
        '
        'txtCC2
        '
        Me.txtCC2.Location = New System.Drawing.Point(248, 146)
        Me.txtCC2.Name = "txtCC2"
        Me.txtCC2.Size = New System.Drawing.Size(180, 22)
        Me.txtCC2.TabIndex = 8
        '
        'txtCC1
        '
        Me.txtCC1.Location = New System.Drawing.Point(62, 146)
        Me.txtCC1.Name = "txtCC1"
        Me.txtCC1.Size = New System.Drawing.Size(180, 22)
        Me.txtCC1.TabIndex = 8
        '
        'txtMail3
        '
        Me.txtMail3.Location = New System.Drawing.Point(434, 106)
        Me.txtMail3.Name = "txtMail3"
        Me.txtMail3.Size = New System.Drawing.Size(180, 22)
        Me.txtMail3.TabIndex = 8
        '
        'txtMail2
        '
        Me.txtMail2.Location = New System.Drawing.Point(248, 106)
        Me.txtMail2.Name = "txtMail2"
        Me.txtMail2.Size = New System.Drawing.Size(180, 22)
        Me.txtMail2.TabIndex = 8
        '
        'txtMail1
        '
        Me.txtMail1.Location = New System.Drawing.Point(62, 106)
        Me.txtMail1.Name = "txtMail1"
        Me.txtMail1.Size = New System.Drawing.Size(180, 22)
        Me.txtMail1.TabIndex = 8
        '
        'Label79
        '
        Me.Label79.AutoSize = True
        Me.Label79.Location = New System.Drawing.Point(841, 112)
        Me.Label79.Name = "Label79"
        Me.Label79.Size = New System.Drawing.Size(130, 14)
        Me.Label79.TabIndex = 7
        Me.Label79.Text = "Empresas Relacionadas"
        Me.Label79.Visible = False
        '
        'dgvEmpRelacion
        '
        Me.dgvEmpRelacion.AllowUserToAddRows = False
        Me.dgvEmpRelacion.AllowUserToDeleteRows = False
        Me.dgvEmpRelacion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvEmpRelacion.Location = New System.Drawing.Point(627, 136)
        Me.dgvEmpRelacion.Name = "dgvEmpRelacion"
        Me.dgvEmpRelacion.ReadOnly = True
        Me.dgvEmpRelacion.RowHeadersVisible = False
        Me.dgvEmpRelacion.Size = New System.Drawing.Size(560, 144)
        Me.dgvEmpRelacion.TabIndex = 6
        Me.dgvEmpRelacion.Visible = False
        '
        'btn_CostoEspecial
        '
        Me.btn_CostoEspecial.Image = Global.Convenios_New.My.Resources.Resources.Floppy
        Me.btn_CostoEspecial.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_CostoEspecial.Location = New System.Drawing.Point(629, 34)
        Me.btn_CostoEspecial.Name = "btn_CostoEspecial"
        Me.btn_CostoEspecial.Size = New System.Drawing.Size(129, 46)
        Me.btn_CostoEspecial.TabIndex = 5
        Me.btn_CostoEspecial.Text = "Grabar Costos Especiales"
        Me.btn_CostoEspecial.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_CostoEspecial.UseVisualStyleBackColor = True
        Me.btn_CostoEspecial.Visible = False
        '
        'frmReajusteLinea
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1316, 653)
        Me.ControlBox = False
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmReajusteLinea"
        Me.ShowIcon = False
        Me.Text = "Reajuste Automatico por Linea"
        Me.Panel1.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.dgvResumenLínea, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvDatosMP, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        CType(Me.dgvInput, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvDetalleLineaProductos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        CType(Me.dgvLineaFinal, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvMpFinal, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage4.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.dgvRelComercial, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgvLineasReajuste, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage5.ResumeLayout(False)
        Me.grbBotones.ResumeLayout(False)
        Me.grbBotones.PerformLayout()
        CType(Me.dgvEmpRelacion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents Button1 As Button
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents Label52 As Label
    Friend WithEvents Label51 As Label
    Friend WithEvents txtPorRebate As TextBox
    Friend WithEvents txtPorGastoOp As TextBox
    Friend WithEvents txtPorVenta As TextBox
    Friend WithEvents txtPorTransporte As TextBox
    Friend WithEvents dgvResumenLínea As DataGridView
    Friend WithEvents cmbMarcaPropia As ComboBox
    Friend WithEvents dgvDatosMP As DataGridView
    Friend WithEvents Label15 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents txtTotMargenFinal As TextBox
    Friend WithEvents txtTotGasto As TextBox
    Friend WithEvents txtTotRebate As TextBox
    Friend WithEvents txtGastoOp As TextBox
    Friend WithEvents txtTotVenta As TextBox
    Friend WithEvents txtTotTrans As TextBox
    Friend WithEvents txtTotContr As TextBox
    Friend WithEvents txtPorTotCon As TextBox
    Friend WithEvents txtFechaFin As TextBox
    Friend WithEvents txtFechaIni As TextBox
    Friend WithEvents txtTotIngreso As TextBox
    Friend WithEvents txtRazons As TextBox
    Friend WithEvents txtNumcot As TextBox
    Friend WithEvents txtRutcli As TextBox
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents txtPorRebateFinal As TextBox
    Friend WithEvents txtPorGastoOpFinal As TextBox
    Friend WithEvents txtPorVentaFinal As TextBox
    Friend WithEvents txtPorTransFinal As TextBox
    Friend WithEvents btnExcel As Button
    Friend WithEvents dgvLineaFinal As DataGridView
    Friend WithEvents cmbMarcaPropiaFinal As ComboBox
    Friend WithEvents dgvMpFinal As DataGridView
    Friend WithEvents Label49 As Label
    Friend WithEvents Label28 As Label
    Friend WithEvents Label29 As Label
    Friend WithEvents Label30 As Label
    Friend WithEvents Label31 As Label
    Friend WithEvents Label32 As Label
    Friend WithEvents Label33 As Label
    Friend WithEvents Label34 As Label
    Friend WithEvents Label35 As Label
    Friend WithEvents Label36 As Label
    Friend WithEvents Label37 As Label
    Friend WithEvents Label38 As Label
    Friend WithEvents Label39 As Label
    Friend WithEvents Label40 As Label
    Friend WithEvents Label41 As Label
    Friend WithEvents Label42 As Label
    Friend WithEvents Label43 As Label
    Friend WithEvents Label55 As Label
    Friend WithEvents Label54 As Label
    Friend WithEvents Label53 As Label
    Friend WithEvents Label44 As Label
    Friend WithEvents Label45 As Label
    Friend WithEvents Label46 As Label
    Friend WithEvents Label47 As Label
    Friend WithEvents Label48 As Label
    Friend WithEvents txtMgFinal As TextBox
    Friend WithEvents txtTotGastoFinal As TextBox
    Friend WithEvents txtTotRebateFinal As TextBox
    Friend WithEvents txtTotGastOpFinal As TextBox
    Friend WithEvents txtTotVentaFinal As TextBox
    Friend WithEvents txtTotTransFinal As TextBox
    Friend WithEvents txtTotContriFinal As TextBox
    Friend WithEvents txtPorContriFinal As TextBox
    Friend WithEvents txtPorVariacion As TextBox
    Friend WithEvents txtValAct As TextBox
    Friend WithEvents txtValAnt As TextBox
    Friend WithEvents txtTotIngresiFinal As TextBox
    Friend WithEvents txtRazonsFinal As TextBox
    Friend WithEvents txtNumcotFinal As TextBox
    Friend WithEvents txtRutCliFinal As TextBox
    Friend WithEvents btn_Limpiar As Button
    Friend WithEvents txtVariacionMargen As TextBox
    Friend WithEvents txtMargenActual As TextBox
    Friend WithEvents txtLinea As TextBox
    Friend WithEvents Label57 As Label
    Friend WithEvents Label58 As Label
    Friend WithEvents Label59 As Label
    Friend WithEvents btn_CambiaLinea As Button
    Friend WithEvents btn_Confirmar As Button
    Friend WithEvents txtCostoEspecial As TextBox
    Friend WithEvents txtVariacion As TextBox
    Friend WithEvents txtPrecioNuevo As TextBox
    Friend WithEvents txtPrecio As TextBox
    Friend WithEvents txtDescripcion As TextBox
    Friend WithEvents txtCodpro As TextBox
    Friend WithEvents Label50 As Label
    Friend WithEvents Label27 As Label
    Friend WithEvents Label26 As Label
    Friend WithEvents Label25 As Label
    Friend WithEvents Label24 As Label
    Friend WithEvents Label23 As Label
    Friend WithEvents dgvDetalleLineaProductos As DataGridView
    Friend WithEvents btnCambiaLinea As Button
    Friend WithEvents TabPage4 As TabPage
    Friend WithEvents txtmgant As TextBox
    Friend WithEvents txtmgdif As TextBox
    Friend WithEvents txtsumaant As TextBox
    Friend WithEvents txtsumDif As TextBox
    Friend WithEvents txtrebant As TextBox
    Friend WithEvents txtrebdif As TextBox
    Friend WithEvents txtgasant As TextBox
    Friend WithEvents txtgasdif As TextBox
    Friend WithEvents txtvenant As TextBox
    Friend WithEvents txtventdif As TextBox
    Friend WithEvents txtmgnew As TextBox
    Friend WithEvents txtsumanew As TextBox
    Friend WithEvents txttranant As TextBox
    Friend WithEvents txtrebnew As TextBox
    Friend WithEvents txttrandif As TextBox
    Friend WithEvents txtgasnew As TextBox
    Friend WithEvents txtcontant As TextBox
    Friend WithEvents txtvennew As TextBox
    Friend WithEvents txtcontdif As TextBox
    Friend WithEvents txttrannew As TextBox
    Friend WithEvents txtingAnt As TextBox
    Friend WithEvents txtcontnew As TextBox
    Friend WithEvents txtingdif As TextBox
    Friend WithEvents txtingnew As TextBox
    Friend WithEvents Label60 As Label
    Friend WithEvents Label61 As Label
    Friend WithEvents Label66 As Label
    Friend WithEvents Label67 As Label
    Friend WithEvents Label65 As Label
    Friend WithEvents Label64 As Label
    Friend WithEvents Label63 As Label
    Friend WithEvents Label62 As Label
    Friend WithEvents Label68 As Label
    Friend WithEvents Label69 As Label
    Friend WithEvents Label70 As Label
    Friend WithEvents txtobserv As TextBox
    Friend WithEvents Label71 As Label
    Friend WithEvents TabPage5 As TabPage
    Friend WithEvents grbBotones As GroupBox
    Friend WithEvents txtInfoExcel As TextBox
    Friend WithEvents Label75 As Label
    Friend WithEvents btn_Correo As Button
    Friend WithEvents Label72 As Label
    Friend WithEvents Label76 As Label
    Friend WithEvents dtpCosEspFin As DateTimePicker
    Friend WithEvents dtpCosEspIni As DateTimePicker
    Friend WithEvents Label77 As Label
    Friend WithEvents Label78 As Label
    Friend WithEvents txtCC3 As TextBox
    Friend WithEvents txtCC2 As TextBox
    Friend WithEvents txtCC1 As TextBox
    Friend WithEvents txtMail3 As TextBox
    Friend WithEvents txtMail2 As TextBox
    Friend WithEvents txtMail1 As TextBox
    Friend WithEvents Label79 As Label
    Friend WithEvents dgvEmpRelacion As DataGridView
    Friend WithEvents btn_CostoEspecial As Button
    Friend WithEvents Label56 As Label
    Friend WithEvents Label80 As Label
    Friend WithEvents Label81 As Label
    Friend WithEvents txtContratoHasta As TextBox
    Friend WithEvents txtContratoDesde As TextBox
    Friend WithEvents txtUltimoContrato As TextBox
    Friend WithEvents chkMarcaTodoCosEsp As CheckBox
    Friend WithEvents btn_Elimina As Button
    Friend WithEvents Label85 As Label
    Friend WithEvents txtVendedor As TextBox
    Friend WithEvents dgvInput As DataGridView
    Friend WithEvents btn_Input As Button
    Friend WithEvents Label87 As Label
    Friend WithEvents Label88 As Label
    Friend WithEvents Label74 As Label
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents chkMarcaTodoConvenio As CheckBox
    Friend WithEvents btn_Grabar As Button
    Friend WithEvents dtpFecVenConvenio As DateTimePicker
    Friend WithEvents dgvRelComercial As DataGridView
    Friend WithEvents Label73 As Label
    Friend WithEvents Label82 As Label
    Friend WithEvents dtpFeciniConvenio As DateTimePicker
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents dgvLineasReajuste As DataGridView
    Friend WithEvents Label84 As Label
    Friend WithEvents dtpFechaLineaReajuste As DateTimePicker
    Friend WithEvents btn_AgregaFecha As Button
    Friend WithEvents Label83 As Label
    Friend WithEvents chkLineas As CheckBox
    Friend WithEvents cmbPeriodoConsumo As ComboBox
    Friend WithEvents Label89 As Label
    Friend WithEvents cmbConsumoFinal As ComboBox
    Friend WithEvents Label86 As Label
End Class
