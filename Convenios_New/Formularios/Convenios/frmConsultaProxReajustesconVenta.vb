﻿Public Class frmConsultaProxReajustesconVenta
    Dim bd As New Conexion
    Dim PuntoControl As New PuntoControl
    Private Sub rdbFecha_CheckedChanged(sender As Object, e As EventArgs) Handles rdbFecha.CheckedChanged
        If rdbFecha.Checked = True Then
            dtpFechaIni.Enabled = True
            dtpFechaFin.Enabled = True
            txtRazons.Enabled = False
            txtRelCom.Enabled = False
            txtRutcli.Enabled = False
        End If
    End Sub

    Private Sub rdbCliente_CheckedChanged(sender As Object, e As EventArgs) Handles rdbCliente.CheckedChanged
        If rdbCliente.Checked = True Then
            dtpFechaIni.Enabled = False
            dtpFechaFin.Enabled = False
            txtRazons.Enabled = True
            txtRelCom.Enabled = False
            txtRutcli.Enabled = True
        End If
    End Sub

    Private Sub rdbRelación_CheckedChanged(sender As Object, e As EventArgs) Handles rdbRelación.CheckedChanged
        If rdbRelación.Checked = True Then
            dtpFechaIni.Enabled = False
            dtpFechaFin.Enabled = False
            txtRazons.Enabled = False
            txtRelCom.Enabled = True
            txtRutcli.Enabled = False
        End If
    End Sub

    Private Sub frmConsultaProxReajustesconVenta_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If Globales.tema = 1 Then
            Me.Theme = 2
            Globales.tema = 2
        Else
            Me.Theme = 1
            Globales.tema = 1
        End If
        StartPosition = FormStartPosition.Manual
        Location = New Point(0, 0)
        Show()
        Application.DoEvents()
        dtpFechaIni.Focus()
        PuntoControl.RegistroUso("23")
    End Sub

    Private Sub btn_Excel_Click(sender As Object, e As EventArgs) Handles btn_Excel.Click
        Dim save As New SaveFileDialog
        If dgvConsulta.RowCount = 0 Then
            MessageBox.Show("No hay datos consultados en la grilla, procese datos antes de exportar", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        Else
            save.Filter = "Archivo Excel | *.xlsx"
            If save.ShowDialog = System.Windows.Forms.DialogResult.OK Then
                Globales.exporta(save.FileName, dgvConsulta)
            End If
        End If
        PuntoControl.RegistroUso("27")
    End Sub

    Private Sub txtRutcli_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtRutcli.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Trim(txtRutcli.Text) <> "" Then
                buscaCliente()
                PuntoControl.RegistroUso("24")
            Else
                txtRazons.Focus()
            End If
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtRazons_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtRazons.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If txtRazons.Text.ToString.Trim <> "" Then

                Dim buscador As New frmBuscaCliente(txtRazons.Text)
                buscador.ShowDialog()
                If buscador.dgvDatos.CurrentRow Is Nothing Then
                    Exit Sub
                End If
                txtRutcli.Text = buscador.dgvDatos.Item("Rut", buscador.dgvDatos.CurrentRow.Index).Value.ToString
                txtRazons.Text = buscador.dgvDatos.Item("Razon Social", buscador.dgvDatos.CurrentRow.Index).Value.ToString
                buscaDatos()
                PuntoControl.RegistroUso("17")
            Else
                If txtRutcli.Text = "" Then
                    txtRutcli.Focus()
                End If
            End If
        End If
    End Sub
    Private Sub buscaCliente()
        Try
            bd.open_dimerc()
            Dim StrQuery = "select razons nombre from en_cliente "
            StrQuery = StrQuery & " where rutcli = '" & txtRutcli.Text & "'"
            StrQuery = StrQuery & "   and codemp = 3"
            Dim OdnCliente = bd.sqlSelect(StrQuery)
            If Not OdnCliente.Rows.Count = 0 Then
                txtRazons.Text = CStr(OdnCliente.Rows(0).Item("nombre"))
                buscaDatos()
            Else
                MessageBox.Show("Cliente No Existe. Debe ser Ingresado Previamente.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtRutcli.Text = ""
                txtRutcli.Focus()
                Exit Sub
            End If
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub btn_Buscar_Click(sender As Object, e As EventArgs) Handles btn_Buscar.Click
        buscaDatos()
        PuntoControl.RegistroUso("22")
    End Sub
    Private Sub buscaDatos()
        Try
            bd.open_dimerc()
            Dim sql As String
            Dim fechaini As String = ""
            Dim fechafin As String = ""
            Dim fechainiBusqueda As String = ""
            Dim fechafinBusqueda As String = ""
            Dim rutcli = ""
            If rdbFecha.Checked = True Then
                fechainiBusqueda = dtpFechaIni.Value.ToShortDateString
                fechafinBusqueda = dtpFechaFin.Value.ToShortDateString
            ElseIf rdbCliente.Checked = True Then
                rutcli = txtRutcli.Text
            Else
                sql = "select 1 from re_emprela where numrel=" + txtRelCom.Text

                Dim dtvalida = bd.sqlSelect(sql)
                If dtvalida.Rows.Count = 0 Then
                    MessageBox.Show("Número de relación no encontrado, intente con otro número", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Exit Sub
                Else
                    sql = " select distinct rutcli  "
                    sql = sql & " FROM re_emprela   "
                    sql = sql & " where numrel in(" + txtRelCom.Text + ")   "
                    sql = sql & " order by rutcli "
                    Dim dtClientes = bd.sqlSelect(sql)
                    For i = 0 To dtClientes.Rows.Count - 1
                        If i = dtClientes.Rows.Count - 1 Then
                            rutcli += dtClientes.Rows(i).Item("rutcli").ToString
                        Else
                            rutcli += dtClientes.Rows(i).Item("rutcli").ToString + ","
                        End If
                    Next
                End If

            End If
            sql = " select a.rutcli,getrazonsocial(a.codemp,a.rutcli) razons, a.numcot, a.fecemi, a.fecven, b.numrel, "
            If rdbRelación.Checked = True Then
                sql = sql & " decode(b.estado,1,'FINANCIERO',2,'COMERCIAL') ""Tipo Relación"",   "
            End If
            sql = sql & " count(e.codpro) ""Cantidad Productos"",   "
            sql = sql & "  nvl(c.ventaneta,0) ""Venta Con Convenio"",nvl(d.ventaneta,0) ""Venta Sin Convenio""   "
            sql = sql & "  from en_conveni a, de_conveni e, re_emprela b,   "
            sql = sql & " (select a.rutcli,sum(decode(greatest(nvl(decode(b.tipo,'N',b.neto*-1,b.neto),0),0),0,1,  nvl(decode(b.tipo,'N',b.neto*-1,b.neto),0))) ventaNeta "
            sql = sql & "  from en_conveni a,qv_control_margen b "
            sql = sql & " where sysdate between a.fecemi and a.fecven "
            sql = sql & " and a.codemp=b.codemp "
            sql = sql & " and a.rutcli=b.rutcli "
            sql = sql & " and a.cencos=b.cencos "
            sql = sql & " and b.fecha between a.fecemi and a.fecven "
            sql = sql & " and b.prodconvenio='S' "
            sql = sql & " group by a.rutcli) c, "
            sql = sql & " (select a.rutcli,sum(decode(greatest(nvl(decode(b.tipo,'N',b.neto*-1,b.neto),0),0),0,1,  nvl(decode(b.tipo,'N',b.neto*-1,b.neto),0))) ventaNeta "
            sql = sql & "  from en_conveni a,qv_control_margen b "
            sql = sql & " where sysdate between a.fecemi and a.fecven "
            sql = sql & " and a.codemp=b.codemp "
            sql = sql & " and a.rutcli=b.rutcli "
            sql = sql & " and a.cencos=b.cencos "
            sql = sql & " and b.fecha between a.fecemi and a.fecven "
            sql = sql & " and b.prodconvenio='N' "
            sql = sql & " group by a.rutcli) d "
            sql = sql & " where a.codemp=3   "
            If rdbFecha.Checked = True Then
                sql = sql & " and a.fecven between to_date('" + fechainiBusqueda + "','dd/mm/yyyy')  "
                sql = sql & " and to_date('" + fechafinBusqueda + "','dd/mm/yyyy') "
            End If
            If Not rdbFecha.Checked = True Then
                sql = sql & " and a.rutcli in(" + rutcli + ")  "
            End If
            If rdbRelación.Checked = True Then
                sql = sql & " and b.numrel = " + txtRelCom.Text + ""
            End If
            sql = sql & " and a.rutcli=b.rutcli  "
            sql = sql & " and a.codemp=b.codemp  "
            sql = sql & " and a.rutcli=c.rutcli(+)  "
            sql = sql & " and a.rutcli=d.rutcli(+) "
            sql = sql & " and a.numcot=e.numcot "
            sql = sql & " and a.codemp=e.codemp  "
            sql = sql & " group by a.rutcli,getrazonsocial(a.codemp,a.rutcli), a.numcot, a.fecemi, a.fecven, b.numrel, "
            If rdbRelación.Checked = True Then
                sql = sql & "decode(b.estado,1,'FINANCIERO',2,'COMERCIAL'),"
            End If
            sql = sql & " c.ventaneta,d.ventaneta "
            sql = sql & " order by b.numrel,a.rutcli"

            Dim dt = bd.sqlSelect(sql)
            dgvConsulta.DataSource = dt
            dgvConsulta.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            dgvConsulta.Columns("Venta Con Convenio").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvConsulta.Columns("Venta Con Convenio").DefaultCellStyle.Format = "n0"
            dgvConsulta.Columns("Venta Sin Convenio").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvConsulta.Columns("Venta Sin Convenio").DefaultCellStyle.Format = "n0"
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub btn_Salir_Click(sender As Object, e As EventArgs) Handles btn_Salir.Click
        Close()
    End Sub

    Private Sub dtpFechaFin_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFechaFin.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            buscaDatos()
            PuntoControl.RegistroUso("26")
        End If
    End Sub

    Private Sub txtRelCom_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtRelCom.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            buscaDatos()
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub dtpFechaIni_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFechaIni.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Or e.KeyChar = Convert.ToChar(Keys.Tab) Then
            dtpFechaFin.Focus()
        End If
    End Sub
End Class