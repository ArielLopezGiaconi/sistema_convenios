﻿Imports System.Data.OracleClient
Imports System.Net.Mail
Imports System.Runtime.InteropServices
Imports Microsoft.Office.Interop

Public Class frmReajusteLinea

    Dim bd As New Conexion
    Dim flag As Boolean = False
    Dim flaggrilla As Boolean = False
    Dim indice As Integer = 0
    Dim sumacosto, sumacostoFinal As Integer
    Dim num_reajuste As Double
    Dim flagEmpRel As Boolean = False
    Private mExcelProcesses() As Process
    Dim rutrelacionado As String
    Dim cencos As String
    Dim Outlook As Outlook.Application
    Dim Mail As Outlook.MailItem
    Dim PuntoControl As New PuntoControl

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub frmReajusteAutomatico_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.StartPosition = FormStartPosition.Manual
        Me.Location = New Point(0, 0)

        cargacombo()
        PuntoControl.RegistroUso("57")
    End Sub
    Private Sub cargaNumeroSecuencia()
        Try
            bd.open_dimerc()

            Dim Sql = "select reajuste_convenio_seq.nextval numero from dual"
            Dim dt = bd.sqlSelect(Sql)

            num_reajuste = dt.Rows(0).Item("numero").ToString
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub
    Private Sub carganegociosrelacionados()
        Try
            bd.open_dimerc()
            Dim SQL = " select rutcli,getrazonsocial(codemp,rutcli) razons,decode(estado,1,'FINANCIERO',2,'COMERCIAL') RELACION "
            SQL = SQL & "FROM re_emprela  "
            SQL = SQL & "where numrel in(select numrel from re_emprela where rutcli=" + txtRutcli.Text + " and codemp=3)  "
            SQL = SQL & "and rutcli not in(select rutcli from re_emprela where rutcli=" + txtRutcli.Text + " and codemp=3)order by rutcli "

            dgvEmpRelacion.DataSource = bd.sqlSelect(SQL)
            SQL = " select rutcli,getrazonsocial(codemp,rutcli) razons "
            SQL = SQL & " FROM re_emprela  "
            SQL = SQL & " where numrel in(select numrel from re_emprela where rutcli=" + txtRutcli.Text + " and codemp=3)  "
            SQL = SQL & " and rutcli not in(select rutcli from re_emprela where rutcli=" + txtRutcli.Text + " and codemp=3)and estado=2  "
            SQL = SQL & " order by rutcli"
            dgvRelComercial.DataSource = bd.sqlSelect(SQL)

            SQL = " select distinct codlin ""Codigo"",getnombrelinea(codlin) ""Linea""  "
            SQL = SQL & "from de_cotizac  "
            SQL = SQL & "where numcot=" + txtNumcot.Text + " and codemp=3  "
            SQL = SQL & "order by getnombrelinea(codlin) "
            dgvLineasReajuste.DataSource = bd.sqlSelect(SQL)
            If flagEmpRel = False Then
                Dim check, check2, check3 As New DataGridViewCheckBoxColumn
                check.HeaderText = "SEL"
                check.Name = "SEL"
                check2.HeaderText = "SEL"
                check2.Name = "SEL"
                check3.HeaderText = "SEL"
                check3.Name = "SEL"
                dgvEmpRelacion.Columns.Insert(0, check)
                dgvEmpRelacion.Columns(0).Visible = True
                dgvRelComercial.Columns.Insert(0, check2)
                dgvRelComercial.Columns(0).Visible = True
                dgvLineasReajuste.Columns.Insert(0, check3)
                dgvLineasReajuste.Columns(0).Visible = True
                flagEmpRel = True
            End If
            dgvEmpRelacion.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            dgvRelComercial.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            dgvLineasReajuste.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            If dgvEmpRelacion.RowCount = 0 Then
                rutrelacionado = txtRutcli.Text
            Else
                rutrelacionado = txtRutcli.Text
                For i = 0 To dgvEmpRelacion.RowCount - 1
                    rutrelacionado += "," + dgvEmpRelacion.Item("rutcli", i).Value.ToString
                Next
            End If

        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub
    Private Sub cargacombo()
        Try
            Dim dtMP As DataTable = New DataTable

            dtMP.Columns.Add("Codigo")
            dtMP.Columns.Add("Descripcion")

            dtMP.Rows.Add("TODO", "TODO")
            dtMP.Rows.Add("NACIONAL", "NACIONAL")
            dtMP.Rows.Add("IMPORTADO", "IMPORTADO")
            dtMP.Rows.Add("NO MP", "NO MP")

            cmbMarcaPropia.DataSource = dtMP
            cmbMarcaPropia.SelectedIndex = -1
            cmbMarcaPropia.ValueMember = "Codigo"
            cmbMarcaPropia.DisplayMember = "Descripcion"
            cmbMarcaPropia.Refresh()

            cmbMarcaPropiaFinal.DataSource = dtMP
            cmbMarcaPropiaFinal.SelectedIndex = -1
            cmbMarcaPropiaFinal.ValueMember = "Codigo"
            cmbMarcaPropiaFinal.DisplayMember = "Descripcion"
            cmbMarcaPropiaFinal.Refresh()

            Dim dtPeriodo As DataTable = New DataTable

            dtPeriodo.Columns.Add("Codigo")
            dtPeriodo.Columns.Add("Descripcion")

            dtPeriodo.Rows.Add("COTIZACION", "COTIZACION")
            dtPeriodo.Rows.Add("MENSUAL", "MENSUAL")
            dtPeriodo.Rows.Add("TRIMESTRAL", "TRIMESTRAL")
            dtPeriodo.Rows.Add("SEMESTRAL", "SEMESTRAL")
            dtPeriodo.Rows.Add("ANUAL", "ANUAL")

            cmbPeriodoConsumo.DataSource = dtPeriodo
            cmbPeriodoConsumo.SelectedIndex = -1
            cmbPeriodoConsumo.ValueMember = "Codigo"
            cmbPeriodoConsumo.DisplayMember = "Descripcion"
            cmbPeriodoConsumo.Refresh()

            cmbConsumoFinal.DataSource = dtPeriodo
            cmbConsumoFinal.SelectedIndex = -1
            cmbConsumoFinal.ValueMember = "Codigo"
            cmbConsumoFinal.DisplayMember = "Descripcion"
            cmbConsumoFinal.Refresh()

            flag = True
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub txtRutcli_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtVendedor.KeyPress, txtTotVenta.KeyPress, txtTotTrans.KeyPress, txtTotRebate.KeyPress, txtTotMargenFinal.KeyPress, txtTotIngreso.KeyPress, txtTotGasto.KeyPress, txtTotContr.KeyPress, txtRutcli.KeyPress, txtRazons.KeyPress, txtPorTotCon.KeyPress, txtGastoOp.KeyPress, txtFechaIni.KeyPress, txtFechaFin.KeyPress
        e.Handled = True
    End Sub

    Private Sub coloreaGrilla()
        For Each fila As DataGridViewRow In dgvDetalleLineaProductos.Rows
            If fila.Cells("Alternativo").Value = "SI" Then
                fila.DefaultCellStyle.BackColor = Color.LightGreen
            End If
        Next
    End Sub
    Private Sub txtNumcot_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNumcot.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If cmbPeriodoConsumo.SelectedIndex < 0 Then
                MessageBox.Show("Debe seleccionar un periodo de consumo para poder continuar", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                cmbPeriodoConsumo.Focus()
                Exit Sub
            End If
            If txtNumcot.Text <> "" Then
                cargaNumeroSecuencia()
                buscacotizacion()
            End If
            PuntoControl.RegistroUso("56")
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub
    Private Sub buscar()
        Try
            btnExcel.Enabled = True
            btn_CambiaLinea.Enabled = True
            btn_Grabar.Enabled = True
            btn_Input.Enabled = True
            btn_Elimina.Enabled = True
            btn_Confirmar.Enabled = True
            btn_AgregaFecha.Enabled = True
            Dim Sql = "select rutcli,get_cliente(codemp,rutcli) razons,fecemi,fecven,cencos,GET_NOMBRE(codven,1) vendedor from en_cotizac where codemp=3 and numcot=" + txtNumcot.Text

            Dim dt = bd.sqlSelect(Sql)
            If dt.Rows.Count = 0 Then
                MessageBox.Show("Número de de cotización no encontrado", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            Else
                txtRutcli.Text = dt.Rows(0).Item("rutcli").ToString
                txtVendedor.Text = dt.Rows(0).Item("vendedor").ToString
                txtRutCliFinal.Text = dt.Rows(0).Item("rutcli").ToString
                txtRazons.Text = dt.Rows(0).Item("razons").ToString
                txtRazonsFinal.Text = dt.Rows(0).Item("razons").ToString
                txtNumcotFinal.Text = txtNumcot.Text
                cencos = dt.Rows(0).Item("cencos").ToString
                carganegociosrelacionados()

                Dim fechaini As String = dt.Rows(0).Item("fecemi")
                Dim fechafin As String = dt.Rows(0).Item("fecven")
                txtFechaIni.Text = dt.Rows(0).Item("fecemi")
                txtFechaFin.Text = dt.Rows(0).Item("fecven")

                If cmbPeriodoConsumo.SelectedIndex = 1 Then
                    fechaini = DateAdd(DateInterval.Month, -1, Now).ToShortDateString()
                    fechafin = Now.Date
                ElseIf cmbPeriodoConsumo.SelectedIndex = 2 Then
                    fechaini = DateAdd(DateInterval.Month, -3, Now).ToShortDateString()
                    fechafin = Now.Date
                ElseIf cmbPeriodoConsumo.SelectedIndex = 3 Then
                    fechaini = DateAdd(DateInterval.Month, -6, Now).ToShortDateString()
                    fechafin = Now.Date
                ElseIf cmbPeriodoConsumo.SelectedIndex = 4 Then
                    fechaini = DateAdd(DateInterval.Month, -12, Now).ToShortDateString()
                    fechafin = Now.Date
                End If

                CargaTablaTemporal(fechaini, fechafin)

                Sql = " select rutcli,numcot,fecemi,fecven  "
                Sql = Sql & "from en_conveni  "
                Sql = Sql & "where codemp=3 and numcot =(select max(numcot)  "
                Sql = Sql & "from en_conveni where codemp=3  "
                Sql = Sql & "and rutcli=" + txtRutcli.Text + ") "
                Dim dtConvenio = bd.sqlSelect(Sql)
                If dtConvenio.Rows.Count > 0 Then
                    txtUltimoContrato.Text = dtConvenio.Rows(0).Item("numcot").ToString
                    txtContratoDesde.Text = dtConvenio.Rows(0).Item("fecemi")
                    txtContratoHasta.Text = dtConvenio.Rows(0).Item("fecven")
                Else
                    txtUltimoContrato.Text = "No Encontrado"
                End If

                Sql = "select * from tmp_proceso_convenio order by alternativo"
                dgvDetalleLineaProductos.DataSource = bd.sqlSelect(Sql)
                formatogrilla()
                If flaggrilla = False Then
                    Dim check As New DataGridViewCheckBoxColumn
                    check.HeaderText = "¿Elimina?"
                    check.Name = "¿Elimina?"
                    dgvDetalleLineaProductos.Columns.Insert(0, check)
                    dgvDetalleLineaProductos.Columns(0).Visible = True
                End If
                flaggrilla = True
                coloreaGrilla()
                recalculaEstadoResultado()
                dgvDetalleLineaProductos.Columns("ahorro").Visible = False
                dgvDetalleLineaProductos.Columns("codlin").Visible = False

                cargaComparativaEERR()

                If IO.File.Exists("C:\dimerc\propuesta " & txtRazons.Text & " cliente.xlsx") Then
                    IO.File.Delete("C:\dimerc\propuesta " & txtRazons.Text & " cliente.xlsx")
                End If
                If IO.File.Exists("C:\dimerc\propuesta " & txtRazons.Text & " vendedor.xlsx") Then
                    IO.File.Delete("C:\dimerc\propuesta " & txtRazons.Text & " vendedor.xlsx")
                End If
            End If
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub consultaReajuste(numero As String)
        Try
            bd.open_dimerc()
            btnExcel.Enabled = False
            btn_Grabar.Enabled = False
            btn_Input.Enabled = False
            btn_Elimina.Enabled = False
            btn_Confirmar.Enabled = False
            btn_AgregaFecha.Enabled = False
            btn_CambiaLinea.Enabled = False
            Dim SQL = " select codpro,despro,mpropia,linea,marca,cantidad,precio, "
            SQL = SQL & "costoanalisis ""Costo Analisis"",coscom ""Costo Comercial"", "
            SQL = SQL & "cosprom ""Costo Promedio"", Cosesp ""Costo Especial"",  "
            SQL = SQL & "cosesppm ""Costo Especial PM"",fecini_cosesp ""Fecini Costo Especial"",fecfin_cosesp ""FecFin Costo Especial"", "
            SQL = SQL & "cosrebate ""Costo Rebate"",aporte,peso,mg_comercial ""Mg. Comercial"",mg_inventario ""Mg. Inventario"", "
            SQL = SQL & "mg_rebate ""Mg. Rebate"",val_precio ""Valorizado Precio"",val_Analisis ""Valorizado Analisis"", "
            SQL = SQL & "val_inventario ""Valorizado Inventario"", val_rebate ""Valorizado Rebate"", "
            SQL = SQL & "cont_comercial ""Contribucion Comercial"", cont_inventario ""Contrib. Inventario"", "
            SQL = SQL & "cont_rebate ""Contrib. Rebate"", cont_rebate_mp ""Contrib Rebate MP"",precot ""Precio Cotizacion"", "
            SQL = SQL & "val_old ""Valorizado Anterior"",estado,pedsto,variacion,cosrep,ultcom, "
            SQL = SQL & "original,alternativo from de_reajuste_convenio "
            SQL = SQL & "where codemp=3 and num_reajuste= " + numero
            dgvDetalleLineaProductos.DataSource = bd.sqlSelect(SQL)

            formatogrilla()
            SQL = " select Linea,sum(val_precio) Valorizado, round((sum(val_precio)-sum(val_analisis))/ sum(val_precio)*100,2) ""MG. Lista"", "
            SQL = SQL & "round((sum(cont_comercial)+ sum(cont_inventario))/ sum(val_precio)*100,2) ""Mg. Comercial"", "
            SQL = SQL & " round((sum(val_precio)-sum(val_inventario))/ sum(val_precio)*100,2) ""Mg. Comer + Aporte MP"", "
            SQL = SQL & " round((sum(val_precio)-sum(val_rebate))/ sum(val_precio)*100,2) ""Mg. Global"" "
            SQL = SQL & " from de_reajuste_convenio "
            SQL = SQL & " where codemp = 3 and num_reajuste =" + numero + " and val_precio >0 and alternativo='NO' "
            SQL = SQL & "group by linea order by sum(val_precio) desc"
            Dim dtResumenLinea As New DataTable
            dtResumenLinea = bd.sqlSelect(SQL)

            Dim dtjuntar As New DataTable
            SQL = " select sum(val_precio) Valorizado, round((sum(val_precio)-sum(val_analisis))/ sum(val_precio)*100,2) ""MG. Lista"", "
            SQL = SQL & "round((sum(cont_comercial)+ sum(cont_inventario))/ sum(val_precio)*100,2) ""Mg. Comercial"", "
            SQL = SQL & " round((sum(val_precio)-sum(val_inventario))/ sum(val_precio)*100,2) ""Mg. Comer + Aporte MP"", "
            SQL = SQL & " round((sum(val_precio)-sum(val_rebate))/ sum(val_precio)*100,2) ""Mg. Global"" "
            SQL = SQL & " from de_reajuste_convenio "
            SQL = SQL & " where codemp = 3 and num_reajuste =" + numero + " and val_precio >0 and alternativo='NO' "
            dtjuntar = bd.sqlSelect(SQL)

            dtResumenLinea.Merge(dtjuntar)
            dgvLineaFinal.DataSource = dtResumenLinea

            dgvLineaFinal.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            dgvLineaFinal.Columns("Valorizado").DefaultCellStyle.Format = "n0"
            dgvLineaFinal.Columns("Valorizado").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLineaFinal.Columns("Mg. Lista").DefaultCellStyle.Format = "n2"
            dgvLineaFinal.Columns("Mg. Lista").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLineaFinal.Columns("Mg. Comercial").DefaultCellStyle.Format = "n2"
            dgvLineaFinal.Columns("Mg. Comercial").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLineaFinal.Columns("Mg. Comer + Aporte MP").DefaultCellStyle.Format = "n2"
            dgvLineaFinal.Columns("Mg. Comer + Aporte MP").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLineaFinal.Columns("Mg. Global").DefaultCellStyle.Format = "n2"
            dgvLineaFinal.Columns("Mg. Global").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

            SQL = " select Linea, sum(val_precio) Valorizado,round((sum(val_precio)-sum(val_analisis))/ sum(val_precio)*100,2) ""MG. Lista"", "
            SQL = SQL & "round((sum(cont_comercial)+ sum(cont_inventario))/ sum(val_precio)*100,2) ""Mg. Comercial"", "
            SQL = SQL & " round((sum(val_precio)-sum(val_inventario))/ sum(val_precio)*100,2) ""Mg. Comer + Aporte MP"", "
            SQL = SQL & " round((sum(val_precio)-sum(val_rebate))/ sum(val_precio)*100,2) ""Mg. Global"" "
            SQL = SQL & " from de_reajuste_convenio "
            SQL = SQL & " where codemp=3 and num_reajuste=" + numero + " and val_precio>0 and alternativo='NO'"
            SQL = SQL & "group by linea order by sum(val_precio) desc"
            Dim dtDatosMp As New DataTable
            dtDatosMp = bd.sqlSelect(SQL)

            Dim dtjuntar2 As New DataTable
            SQL = " select  sum(val_precio) Valorizado,round((sum(val_precio)-sum(val_analisis))/ sum(val_precio)*100,2) ""MG. Lista"", "
            SQL = SQL & "round((sum(cont_comercial)+ sum(cont_inventario))/ sum(val_precio)*100,2) ""Mg. Comercial"", "
            SQL = SQL & " round((sum(val_precio)-sum(val_inventario))/ sum(val_precio)*100,2) ""Mg. Comer + Aporte MP"", "
            SQL = SQL & " round((sum(val_precio)-sum(val_rebate))/ sum(val_precio)*100,2) ""Mg. Global"" "
            SQL = SQL & " from de_reajuste_convenio "
            SQL = SQL & " where codemp=3 and num_reajuste=" + numero + " and val_precio>0 and alternativo='NO'"
            dtjuntar2 = bd.sqlSelect(SQL)
            dtDatosMp.Merge(dtjuntar2)
            dgvMpFinal.DataSource = dtDatosMp

            dgvMpFinal.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            dgvMpFinal.Columns("Valorizado").DefaultCellStyle.Format = "n0"
            dgvMpFinal.Columns("Valorizado").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvMpFinal.Columns("Mg. Lista").DefaultCellStyle.Format = "n2"
            dgvMpFinal.Columns("Mg. Lista").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvMpFinal.Columns("Mg. Comercial").DefaultCellStyle.Format = "n2"
            dgvMpFinal.Columns("Mg. Comercial").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvMpFinal.Columns("Mg. Comer + Aporte MP").DefaultCellStyle.Format = "n2"
            dgvMpFinal.Columns("Mg. Comer + Aporte MP").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvMpFinal.Columns("Mg. Global").DefaultCellStyle.Format = "n2"
            dgvMpFinal.Columns("Mg. Global").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

            SQL = "select nvl(sum(val_precio),0) Ingreso, nvl(sum(val_old),0) Anterior, sum(cantidad * cosprom) costo "
            SQL += ", round((sum(val_precio)-sum(val_rebate))/ sum(val_precio)*100,2) margen from de_reajuste_convenio where "
            SQL += "codemp=3 and num_reajuste=" + numero + " and cantidad>0 And alternativo='NO'"
            Dim dt = bd.sqlSelect(SQL)
            If dt.Rows.Count > 0 Then
                If dt.Rows(0).Item("Ingreso") > 0 Then
                    txtTotIngresiFinal.Text = dt.Rows(0).Item("Ingreso")
                    txtValAnt.Text = dt.Rows(0).Item("Anterior")
                    txtValAct.Text = dt.Rows(0).Item("Ingreso")
                    txtTotContriFinal.Text = Math.Round(dt.Rows(0).Item("Ingreso") * (dt.Rows(0).Item("margen") / 100))
                    txtPorContriFinal.Text = dt.Rows(0).Item("margen")
                    txtPorContriFinal.Text = FormatPercent(txtPorContriFinal.Text / 100, 2)

                    txtMgFinal.Text = (dt.Rows(0).Item("margen"))
                    txtMgFinal.Text = FormatPercent(txtMgFinal.Text / 100, 2)
                    txtTotIngresiFinal.Text = FormatNumber(txtTotIngresiFinal.Text, 0)
                    txtTotContriFinal.Text = FormatNumber(txtTotContriFinal.Text, 0)

                    txtValAnt.Text = FormatNumber(txtValAnt.Text, 0)
                    txtValAct.Text = FormatNumber(txtValAct.Text, 0)
                    txtPorVariacion.Text = txtValAct.Text / txtValAnt.Text - 1
                    'txtPorContriFinal.Text = FormatPercent(txtPorContriFinal.Text, 2)
                    txtPorVariacion.Text = FormatPercent(txtPorVariacion.Text, 2)
                Else
                    txtTotIngresiFinal.Text = "0"
                    txtValAct.Text = "0"
                    txtTotContriFinal.Text = "0"
                    txtPorContriFinal.Text = "0"
                    txtMgFinal.Text = "0"
                    txtMgFinal.Text = FormatPercent(txtMgFinal.Text, 2)
                    txtTotIngresiFinal.Text = FormatNumber(txtTotIngresiFinal.Text, 0)
                    txtTotContriFinal.Text = FormatNumber(txtTotContriFinal.Text, 0)
                    txtValAct.Text = FormatNumber(txtValAct.Text, 0)
                    txtPorVariacion.Text = "0"
                    'txtPorContriFinal.Text = FormatPercent(txtPorContriFinal.Text, 2)
                    txtPorVariacion.Text = FormatPercent(txtPorVariacion.Text, 2)
                End If
            End If

            'ENCABEZADO
            SQL = " select Linea,sum(val_old) Valorizado, round((sum(val_old)-sum(val_analisis))/ sum(val_old)*100,2) ""MG. Lista"", "
            SQL = SQL & "round((sum(cont_comercial)+ sum(cont_inventario))/ sum(val_old)*100,2) ""Mg. Comercial"", "
            SQL = SQL & " round((sum(val_old)-sum(val_inventario))/ sum(val_old)*100,2) ""Mg. Comer + Aporte MP"", "
            SQL = SQL & " round((sum(val_old)-sum(val_rebate))/ sum(val_old)*100,2) ""Mg. Global"" "
            SQL = SQL & " from de_reajuste_convenio "
            SQL = SQL & " where codemp = 3 and num_reajuste =" + numero + " and val_precio >0 and alternativo='NO' "
            SQL = SQL & "group by linea order by sum(val_precio) desc"
            Dim dtCabezaLinea As New DataTable
            dtCabezaLinea = bd.sqlSelect(SQL)

            Dim dtjuntar3 As New DataTable
            SQL = " select sum(val_old) Valorizado, round((sum(val_old)-sum(val_analisis))/ sum(val_old)*100,2) ""MG. Lista"", "
            SQL = SQL & "round((sum(cont_comercial)+ sum(cont_inventario))/ sum(val_old)*100,2) ""Mg. Comercial"", "
            SQL = SQL & " round((sum(val_old)-sum(val_inventario))/ sum(val_old)*100,2) ""Mg. Comer + Aporte MP"", "
            SQL = SQL & " round((sum(val_old)-sum(val_rebate))/ sum(val_old)*100,2) ""Mg. Global"" "
            SQL = SQL & " from de_reajuste_convenio "
            SQL = SQL & " where codemp = 3 and num_reajuste =" + numero + " and val_precio >0 and alternativo='NO' "
            dtjuntar3 = bd.sqlSelect(SQL)

            dtCabezaLinea.Merge(dtjuntar3)
            dgvResumenLínea.DataSource = dtCabezaLinea

            dgvResumenLínea.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            dgvResumenLínea.Columns("Valorizado").DefaultCellStyle.Format = "n0"
            dgvResumenLínea.Columns("Valorizado").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvResumenLínea.Columns("Mg. Lista").DefaultCellStyle.Format = "n2"
            dgvResumenLínea.Columns("Mg. Lista").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvResumenLínea.Columns("Mg. Comercial").DefaultCellStyle.Format = "n2"
            dgvResumenLínea.Columns("Mg. Comercial").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvResumenLínea.Columns("Mg. Comer + Aporte MP").DefaultCellStyle.Format = "n2"
            dgvResumenLínea.Columns("Mg. Comer + Aporte MP").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvResumenLínea.Columns("Mg. Global").DefaultCellStyle.Format = "n2"
            dgvResumenLínea.Columns("Mg. Global").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

            SQL = " select Linea, sum(val_old) Valorizado,round((sum(val_old)-sum(val_analisis))/ sum(val_old)*100,2) ""MG. Lista"", "
            SQL = SQL & "round((sum(cont_comercial)+ sum(cont_inventario))/ sum(val_old)*100,2) ""Mg. Comercial"", "
            SQL = SQL & " round((sum(val_old)-sum(val_inventario))/ sum(val_old)*100,2) ""Mg. Comer + Aporte MP"", "
            SQL = SQL & " round((sum(val_old)-sum(val_rebate))/ sum(val_old)*100,2) ""Mg. Global"" "
            SQL = SQL & " from de_reajuste_convenio "
            SQL = SQL & " where codemp=3 and num_reajuste=" + numero + " and val_precio>0 and alternativo='NO'"
            SQL = SQL & "group by linea order by sum(val_precio) desc"
            Dim dtCabezaMp As New DataTable
            dtCabezaMp = bd.sqlSelect(SQL)

            Dim dtjuntar4 As New DataTable
            SQL = " select  sum(val_old) Valorizado,round((sum(val_old)-sum(val_analisis))/ sum(val_old)*100,2) ""MG. Lista"", "
            SQL = SQL & "round((sum(cont_comercial)+ sum(cont_inventario))/ sum(val_old)*100,2) ""Mg. Comercial"", "
            SQL = SQL & " round((sum(val_old)-sum(val_inventario))/ sum(val_old)*100,2) ""Mg. Comer + Aporte MP"", "
            SQL = SQL & " round((sum(val_old)-sum(val_rebate))/ sum(val_old)*100,2) ""Mg. Global"" "
            SQL = SQL & " from de_reajuste_convenio "
            SQL = SQL & " where codemp=3 and num_reajuste=" + numero + " and val_precio>0 and alternativo='NO'"
            dtjuntar4 = bd.sqlSelect(SQL)
            dtCabezaMp.Merge(dtjuntar4)
            dgvDatosMP.DataSource = dtCabezaMp

            dgvDatosMP.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            dgvDatosMP.Columns("Valorizado").DefaultCellStyle.Format = "n0"
            dgvDatosMP.Columns("Valorizado").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvDatosMP.Columns("Mg. Lista").DefaultCellStyle.Format = "n2"
            dgvDatosMP.Columns("Mg. Lista").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvDatosMP.Columns("Mg. Comercial").DefaultCellStyle.Format = "n2"
            dgvDatosMP.Columns("Mg. Comercial").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvDatosMP.Columns("Mg. Comer + Aporte MP").DefaultCellStyle.Format = "n2"
            dgvDatosMP.Columns("Mg. Comer + Aporte MP").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvDatosMP.Columns("Mg. Global").DefaultCellStyle.Format = "n2"
            dgvDatosMP.Columns("Mg. Global").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

            SQL = " select a.numcot,a.rutcli,getrazonsocial(3,a.rutcli) razons, "
            SQL = SQL & "  a.fecini,a.fecfin, "
            SQL = SQL & "  a.toting_old, a.porc_cont_old,a.tot_cont_old, "
            SQL = SQL & "  a.porc_trans_old,a.tot_trans_old, "
            SQL = SQL & "  a.porc_venta_old, a.tot_venta_old, "
            SQL = SQL & "  a.porc_gastos_old,a.tot_gastos_old, "
            SQL = SQL & "  a.porc_rebate_old,a.tot_rebate_old, "
            SQL = SQL & "  a.suma_gastos_old,a.mg_final_old,  "
            SQL = SQL & "  a.toting_new ,a.porc_cont_new,a.tot_cont_new,  "
            SQL = SQL & "  a.porc_trans_new, a.tot_trans_new, "
            SQL = SQL & "  a.porc_venta_new,a.tot_venta_new, "
            SQL = SQL & "  a.porc_gastos_new,a.tot_gastos_new, "
            SQL = SQL & "  a.porc_rebate_new,a.tot_rebate_new, "
            SQL = SQL & "  a.suma_gastos_new,a.mg_final_new, "
            SQL = SQL & "  a.observacion,get_nombre(b.codven,1) vendedor  "
            SQL = SQL & "  from en_reajuste_convenio a, en_cotizac b  "
            SQL = SQL & "  where a.codemp=3  "
            SQL = SQL & "  and a.num_reajuste= " + numero
            SQL = SQL & "  and a.codemp=b.codemp "
            SQL = SQL & "  and a.numcot=b.numcot "
            Dim dtCabeza = bd.sqlSelect(SQL)
            If dtCabeza.Rows.Count > 0 Then
                txtNumcot.Text = dtCabeza.Rows(0).Item("numcot").ToString
                txtNumcotFinal.Text = dtCabeza.Rows(0).Item("numcot").ToString
                txtRutcli.Text = dtCabeza.Rows(0).Item("rutcli").ToString
                txtRutCliFinal.Text = dtCabeza.Rows(0).Item("rutcli").ToString
                txtRazons.Text = dtCabeza.Rows(0).Item("razons").ToString
                txtRazonsFinal.Text = dtCabeza.Rows(0).Item("razons").ToString
                txtTotIngreso.Text = dtCabeza.Rows(0).Item("toting_old").ToString
                txtPorTotCon.Text = dtCabeza.Rows(0).Item("porc_cont_old").ToString
                txtTotContr.Text = dtCabeza.Rows(0).Item("tot_cont_old").ToString
                txtcontant.Text = txtTotContr.Text
                txtcontant.Text = FormatNumber(txtcontant.Text, 0)
                txtPorTransporte.Text = dtCabeza.Rows(0).Item("porc_trans_old").ToString
                txtTotTrans.Text = dtCabeza.Rows(0).Item("tot_trans_old").ToString
                txtPorVenta.Text = dtCabeza.Rows(0).Item("porc_venta_old").ToString
                txtTotVenta.Text = dtCabeza.Rows(0).Item("tot_venta_old").ToString
                txtPorGastoOp.Text = dtCabeza.Rows(0).Item("porc_gastos_old").ToString
                txtGastoOp.Text = dtCabeza.Rows(0).Item("tot_gastos_old").ToString
                txtPorRebate.Text = dtCabeza.Rows(0).Item("porc_rebate_old").ToString
                txtTotRebate.Text = dtCabeza.Rows(0).Item("tot_rebate_old").ToString
                txtTotGasto.Text = dtCabeza.Rows(0).Item("suma_gastos_old").ToString
                txtTotMargenFinal.Text = dtCabeza.Rows(0).Item("mg_final_old").ToString
                txtobserv.Text = dtCabeza.Rows(0).Item("observacion").ToString
                txtFechaIni.Text = dtCabeza.Rows(0).Item("fecini")
                txtFechaFin.Text = dtCabeza.Rows(0).Item("fecfin")
                txtVendedor.Text = dtCabeza.Rows(0).Item("vendedor")

                formatear()

                calculaEstado()
                calculaEstadoFinal()
                cargaComparativaEERR()
                SQL = " select rutcli,numcot,fecemi,fecven  "
                SQL = SQL & "from en_conveni  "
                SQL = SQL & "where codemp=3 and numcot =(select max(numcot)  "
                SQL = SQL & "from en_conveni where codemp=3  "
                SQL = SQL & "and rutcli=" + txtRutcli.Text + ") "
                Dim dtConvenio = bd.sqlSelect(SQL)
                If dtConvenio.Rows.Count > 0 Then
                    txtUltimoContrato.Text = dtConvenio.Rows(0).Item("numcot").ToString
                    txtContratoDesde.Text = dtConvenio.Rows(0).Item("fecemi")
                    txtContratoHasta.Text = dtConvenio.Rows(0).Item("fecven")
                Else
                    txtUltimoContrato.Text = "No Encontrado"
                End If
            End If

        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub buscacotizacion()
        Try
            bd.open_dimerc()
            Dim sql = " select num_reajuste,rutcli,numcot,usr_creac usuario,estado "
            sql = sql & "from en_reajuste_convenio  "
            sql = sql & "where codemp=3  "
            sql = sql & "and numcot= " + txtNumcot.Text
            sql = sql & "order by estado desc"
            Dim dt = bd.sqlSelect(sql)
            If dt.Rows.Count = 0 Then
                buscar()
            Else
                If dt.Rows(0).Item("estado") >= 2 Then
                    If MessageBox.Show("Numero de Cotización ya tiene una propuesta cerrada por " + dt.Rows(0).Item("usuario").ToString + ", no podra crear una nueva a partir de esta." + vbCrLf +
                                    "¿Desea de todas formas consultar los datos? (NO PODRA MODIFICARLOS) ", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                        consultaReajuste(dt.Rows(0).Item("num_reajuste").ToString)
                        Exit Sub
                    Else
                        Exit Sub
                    End If
                Else
                    If MessageBox.Show("Número de cotización ya tomado por usuario " + dt.Rows(0).Item("usuario").ToString + "¿Desea procesar la propuesta nuevamente?", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                        buscar()
                    Else
                        Exit Sub
                    End If
                End If
            End If

        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub
    Private Sub cargaComparativaEERR()
        Try
            txtingAnt.Text = txtValAnt.Text
            txtingnew.Text = txtTotIngresiFinal.Text
            txtingdif.Text = txtingnew.Text - txtingAnt.Text

            'txtcontant.Text = txtTotContr.Text
            txtcontnew.Text = txtTotContriFinal.Text
            txtcontdif.Text = txtcontnew.Text - txtcontant.Text

            txttranant.Text = Math.Round((txtPorTransFinal.Text / 100) * txtValAnt.Text, 0)
            txttranant.Text = FormatNumber(txttranant.Text, 0)
            txttrannew.Text = txtTotTransFinal.Text
            txttrandif.Text = txttrannew.Text - txttranant.Text

            txtvenant.Text = Math.Round(txtTotIngresiFinal.Text * getMargenComercial() * (txtPorVentaFinal.Text / 100), 0)
            txtvenant.Text = FormatNumber(txtvenant.Text, 0)
            txtvennew.Text = txtTotVentaFinal.Text
            txtventdif.Text = txtvennew.Text - txtvenant.Text

            txtgasant.Text = Math.Round((txtPorGastoOpFinal.Text / 100) * txtValAnt.Text, 0)
            txtgasant.Text = FormatNumber(txtgasant.Text, 0)
            txtgasnew.Text = txtTotGastOpFinal.Text
            txtgasdif.Text = txtgasnew.Text - txtgasant.Text

            txtrebant.Text = Math.Round((txtPorRebateFinal.Text / 100) * txtValAnt.Text, 0)
            txtrebant.Text = FormatNumber(txtrebant.Text, 0)
            txtrebnew.Text = txtTotRebateFinal.Text
            txtrebdif.Text = txtrebnew.Text - txtrebant.Text

            Dim valor As Double = Convert.ToDouble(txtrebant.Text) + Convert.ToDouble(txtvenant.Text) + Convert.ToDouble(txttranant.Text) + Convert.ToDouble(txtgasant.Text)

            txtsumaant.Text = valor.ToString
            txtsumaant.Text = FormatNumber(txtsumaant.Text, 0)
            txtsumanew.Text = txtTotGastoFinal.Text
            txtsumDif.Text = txtsumanew.Text - txtsumaant.Text

            txtmgant.Text = (Convert.ToDouble(txtcontant.Text) - Convert.ToDouble(txtsumaant.Text)) / Convert.ToDouble(txtValAnt.Text)
            txtmgant.Text = FormatPercent(txtmgant.Text, 2)
            'txtmgant.Text = txtTotMargenFinal.Text
            txtmgnew.Text = txtMgFinal.Text

            Dim nuevo As Double = (Convert.ToDouble(txtTotContriFinal.Text) - Convert.ToDouble(txtTotGastoFinal.Text)) / Convert.ToDouble(txtTotIngresiFinal.Text)
            Dim viejo As Double = (Convert.ToDouble(txtcontant.Text) - Convert.ToDouble(txtsumaant.Text)) / Convert.ToDouble(txtValAnt.Text)
            txtmgdif.Text = nuevo - viejo
            txtmgdif.Text = FormatPercent(txtmgdif.Text, 2)
            txtingdif.Text = FormatNumber(txtingdif.Text, 0)
            txtcontdif.Text = FormatNumber(txtcontdif.Text, 0)
            txttrandif.Text = FormatNumber(txttrandif.Text, 0)
            txtventdif.Text = FormatNumber(txtventdif.Text, 0)
            txtgasdif.Text = FormatNumber(txtgasdif.Text, 0)
            txtrebdif.Text = FormatNumber(txtrebdif.Text, 0)
            txtsumDif.Text = FormatNumber(txtsumDif.Text, 0)
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub CargaTablaTemporal(fechaini, fechafin)
        Using conn = New OracleConnection(Conexion.retornaConexion)
            conn.Open()
            Dim comando As OracleCommand
            Dim dataadapter As OracleDataAdapter
            Dim dt As New DataTable
            Using transaccion = conn.BeginTransaction
                Try

                    Dim sql = "select tname from tab where tname = 'TMP_PROCESO_CONVENIO'"
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    dataadapter = New OracleDataAdapter(comando)
                    dataadapter.Fill(dt)
                    If dt.Rows.Count > 0 Then
                        sql = "drop table tmp_proceso_convenio"
                        comando = New OracleCommand(sql, conn)
                        comando.Transaction = transaccion
                        comando.ExecuteNonQuery()
                    End If

                    sql = " create table tmp_proceso_convenio as ( "
                    sql = sql & "  select c.codpro,c.despro,nvl(e.tipo,'NO MP') mpropia,getlinea(c.codpro) linea,getmarca(c.codpro) marca,  "
                    If cmbPeriodoConsumo.SelectedIndex = 0 Then
                        sql = sql & " b.cantid cantidad ,"
                    Else
                        sql = sql & " nvl(d.cantidad,1) cantidad ,"
                    End If
                    sql = sql & " b.precio,  "
                    sql = sql & " nvl(decode(f.costo,'',C.COSTO,f.costo),0) ""Costo Analisis"",  "
                    'sql = sql & " nvl(decode(f.costo,'',C.COSTO,f.costo),0) ""Costo Comercial"",   "
                    sql = sql & " nvl(C.COSTO,0) ""Costo Comercial"",  "
                    sql = sql & " decode(nvl(decode(f.costo,'',C.cosprom,f.costo),0),0,c.costo,c.cosprom) ""Costo Promedio"",  "
                    sql = sql & " 0 ""Costo Especial"",  "
                    sql = sql & " nvl(f.costo,0) ""Costo Especial PM"",  "
                    sql = sql & " nvl(f.fecini,'') ""FecIni Costo Especial"",  "
                    sql = sql & " nvl(f.fecfin,'') ""FecFIn Costo Especial"",  "
                    sql = sql & " round(decode(nvl(decode(f.costo,'',C.cosprom,f.costo),0),0,c.costo,c.cosprom) * (1-(nvl(g.aporte,0)/100)),2) ""Costo Rebate"",  "
                    sql = sql & " nvl(g.aporte,0) aporte, 0 PESO, round(((b.precio-nvl(decode(f.costo,'',C.COSTO,f.costo),0))/b.precio)*100,2) ""Mg. Comercial"",  "
                    sql = sql & " round((1-decode(nvl(decode(f.costo,'',C.cosprom,f.costo),0),0,c.costo,c.cosprom)/b.precio)*100,2) ""Mg. Inventario"",  "
                    sql = sql & " round((1-(decode(nvl(decode(f.costo,'',C.cosprom,f.costo),0),0,c.costo,c.cosprom) * (1-(nvl(g.aporte,0)/100)))/b.precio)*100,2) ""Mg. Rebate"",  "
                    sql = sql & " round(nvl(" + IIf(cmbPeriodoConsumo.SelectedIndex = 0, "b.cantid", "d.cantidad") + " ,1) * b.precio,0) ""Valorizado Precio"", round(nvl(" + IIf(cmbPeriodoConsumo.SelectedIndex = 0, "b.cantid", "d.cantidad") + ",1) * nvl(decode(f.costo,'',C.COSTO,f.costo),0),0) ""Valorizado Analisis"",  "
                    sql = sql & " round(nvl(" + IIf(cmbPeriodoConsumo.SelectedIndex = 0, "b.cantid", "d.cantidad") + ",1) * nvl(decode(f.costo,'',C.cosprom,f.costo),0),0) ""Valorizado Inventario"",  "
                    sql = sql & " round(nvl(nvl(" + IIf(cmbPeriodoConsumo.SelectedIndex = 0, "b.cantid", "d.cantidad") + ",1) * round(decode(nvl(decode(f.costo,'',C.cosprom,f.costo),0),0,c.costo,c.cosprom) * (1-(nvl(g.aporte,0)/100)),2),0),0) ""Valorizado Rebate"",  "
                    sql = sql & " round((nvl(" + IIf(cmbPeriodoConsumo.SelectedIndex = 0, "b.cantid", "d.cantidad") + ",1) * b.precio)  - (nvl(" + IIf(cmbPeriodoConsumo.SelectedIndex = 0, "b.cantid", "d.cantidad") + ",1) * nvl(decode(f.costo,'',C.COSTO,f.costo),0)),0) ""Contribucion Comercial"",  "
                    sql = sql & " round(decode(e.tipo,'IMPORTADO',0,DECODE(greatest(c.costo,c.cosprom),c.costo,(nvl(" + IIf(cmbPeriodoConsumo.SelectedIndex = 0, "b.cantid", "d.cantidad") + ",1) * nvl(decode(f.costo,'',C.COSTO,f.costo),0))-nvl(" + IIf(cmbPeriodoConsumo.SelectedIndex = 0, "b.cantid", "d.cantidad") + ",1) * decode(nvl(decode(f.costo,'',C.cosprom,f.costo),0),0,c.costo,c.cosprom),0)),0) ""Contrib. Inventario"",  "
                    sql = sql & " round(decode(e.tipo,'IMPORTADO',0,decode(nvl(g.aporte,0),0,0,decode(greatest(c.costo,c.cosprom),c.costo,(nvl(" + IIf(cmbPeriodoConsumo.SelectedIndex = 0, "b.cantid", "d.cantidad") + ",1) * decode(nvl(decode(f.costo,'',C.cosprom,f.costo),0),0,c.costo,c.cosprom)) -(nvl(" + IIf(cmbPeriodoConsumo.SelectedIndex = 0, "b.cantid", "d.cantidad") + ",1) * round(decode(nvl(decode(f.costo,'',C.cosprom,f.costo),0),0,c.costo,c.cosprom) * (1-(aporte/100)),2)),(nvl(" + IIf(cmbPeriodoConsumo.SelectedIndex = 0, "b.cantid", "d.cantidad") + ",1) * nvl(decode(f.costo,'',C.COSTO,f.costo),0))- (nvl(" + IIf(cmbPeriodoConsumo.SelectedIndex = 0, "b.cantid", "d.cantidad") + ",1) * round(decode(nvl(decode(f.costo,'',C.cosprom,f.costo),0),0,c.costo,c.cosprom) * (1-(aporte/100)),2))))),0) ""Contrib. Rebate"",  "
                    sql = sql & " round(decode(e.tipo,'IMPORTADO',DECODE(greatest(c.costo,c.cosprom),c.costo,(nvl(" + IIf(cmbPeriodoConsumo.SelectedIndex = 0, "b.cantid", "d.cantidad") + ",1) * nvl(decode(f.costo,'',C.COSTO,f.costo),0))-nvl(" + IIf(cmbPeriodoConsumo.SelectedIndex = 0, "b.cantid", "d.cantidad") + ",1) * decode(nvl(decode(f.costo,'',C.cosprom,f.costo),0),0,c.costo,c.cosprom),0),0),0) ""Contrib Rebate Mp"",  "
                    sql = sql & " b.precio ""Precio Cotizacion"",  "
                    sql = sql & " round(nvl(" + IIf(cmbPeriodoConsumo.SelectedIndex = 0, "b.cantid", "d.cantidad") + ",1) * b.precio,0) ""Valorizado Anterior"",  "
                    sql = sql & " getestadoprod(c.codpro) estado,  "
                    sql = sql & " decode(getpedsto(c.codpro),'S','STOCK','P','PEDIDO','VACIO') pedsto,  "
                    sql = sql & " 0 variacion, "
                    sql = sql & " GETCOSTOREPOSICION3(C.CODPRO) cosrep, "
                    sql = sql & " nvl(GETULTIMACOMPRA('3',c.CODPRO,'V'),0) ultcom, "
                    sql = sql & " 'NO' alternativo, '' original, 0 ahorro,getcodigolinea(b.codpro) codlin,  "
                    sql = sql & " nvl(h.precio,0) ""Precio Min. Venta"", "
                    sql = sql & " nvl(i.precio,0) ""Precio Min. Coti"",nvl(c.cosprom,0) cosprom,GET_TIENEPRODUCTOALTERNATIVO(b.codpro) ""Estado Alternativo"" , x.cosprom ""Costo Santi."", x1.cosprom ""Costo Anto."", x2.cosfut "
                    sql = sql & "  from en_cotizac a,  de_cotizac b,  ma_product c, (SELECT centro, codpro, cosprom FROM MA_COSTOSXCENTROS WHERE CENTRO IN(2001) and canal = 10 and codemp = 3) x, "
                    sql = sql & " (SELECT centro, codpro, cosprom FROM MA_COSTOSXCENTROS WHERE CENTRO IN(2003) and canal = 10 and codemp = 3) x1, (select * from ma_costo_futuro where to_Date(sysdate) between fecini and fecfin and codemp = 3) x2,"
                    sql = sql & " (select codpro,mpropia_hst,decode(greatest(nvl(sum(decode(tipo,'N',cantid*-1,cantid)),0),0),0,1,nvl(sum(decode(tipo,'N',cantid*-1,cantid)),0)) cantidad   "
                    sql = sql & " from qv_control_margen a  "
                    sql = sql & " where a.codemp = 3  "
                    sql = sql & " and a.rutcli in(" + rutrelacionado + ") "
                    sql = sql & " and a.fecha  "
                    sql = sql & " between to_date('" + fechaini + "','dd/mm/yyyy')   "
                    sql = sql & " and to_date('" + fechafin + "','dd/mm/yyyy')  "
                    sql = sql & " and ((tipo = 'F' and internet in (0,1)) or (tipo = 'G' and internet in (0,2)) or (tipo = 'N' and internet in (1,2,7,9)))  "
                    sql = sql & " group by codpro,mpropia_hst ) d,  "
                    sql = sql & " (select codpro,tipo from qv_mpropia_dimerc_hst  "
                    sql = sql & " where (fechavig,codpro) in(  "
                    sql = sql & " select max(fechavig),codpro from qv_mpropia_dimerc_hst   "
                    sql = sql & " group by codpro)) e,  "
                    sql = sql & " (select codpro,costo,FECINI,FECFIN from en_cliente_costo a  "
                    sql = sql & " where codemp=3 "
                    sql = sql & " and rutcli= " + txtRutcli.Text
                    sql = sql & " AND SYSDATE BETWEEN FECINI AND FECFIN      "
                    sql = sql & " and tipo=0 and estado='V') f,  "
                    sql = sql & " ma_prod_aporte g,  "
                    'sql = sql & " ma_product_negocio g,  "
                    sql = sql & " (select codpro,min(precio) precio from  "
                    sql = sql & "   (select b.codpro,min(b.precio) precio from en_factura a,de_factura b where a.codemp=3    "
                    sql = sql & "   and a.rutcli in(" + rutrelacionado + ")   "
                    sql = sql & "   and a.codemp=b.codemp   "
                    sql = sql & "   and a.numfac=b.numfac   "
                    sql = sql & "   and a.fecemi>= add_months(sysdate, -6)  group by b.codpro "
                    sql = sql & "   union "
                    sql = sql & "   select b.codpro,min(b.precio) precio from  en_factura a,de_guiades b,re_guiafac c where a.codemp=3    "
                    sql = sql & "   and a.rutcli in(" + rutrelacionado + ")   "
                    sql = sql & "   and a.codemp=b.codemp   "
                    sql = sql & "   and a.numfac=c.numfac "
                    sql = sql & "   and b.numgui=c.numgui   "
                    sql = sql & "   and a.fecemi>= add_months(sysdate, -6)  group by b.codpro) "
                    sql = sql & "   group by codpro ) h,"
                    sql = sql & " (select b.codpro,min(b.precio) precio from en_cotizac a,de_cotizac b "
                    sql = sql & " where a.codemp=3  "
                    sql = sql & " and a.rutcli in(" + rutrelacionado + ") "
                    sql = sql & " and a.codemp=b.codemp "
                    sql = sql & " and a.numcot=b.numcot "
                    sql = sql & " and a.fecemi>= add_months(sysdate, -6) "
                    sql = sql & " group by b.codpro) i "
                    sql = sql & " where a.codemp=b.codemp  "
                    sql = sql & " and a.numcot=b.numcot  "
                    sql = sql & " and b.codpro=c.codpro  "
                    sql = sql & " and a.numcot=  " + txtNumcot.Text
                    sql = sql & " and a.codemp=3  "
                    sql = sql & " and c.codpro=d.codpro(+)  "
                    sql = sql & " and c.codpro=e.codpro(+)  "
                    sql = sql & " and c.codpro=f.codpro(+)  "
                    sql = sql & " and c.codpro=g.codpro(+) "
                    sql = sql & " and c.codpro=h.codpro(+) "
                    sql = sql & " and c.codpro=i.codpro(+) "
                    sql = sql & " and c.codpro=x.codpro(+) "
                    sql = sql & " and c.codpro=x1.codpro(+) "
                    sql = sql & " and c.codpro=x2.codpro(+) "
                    sql = sql & " and b.precio > 0 "
                    sql = sql & " union "
                    sql = sql & " (select b.codpromp,b.despromp,nvl(e.tipo,'NO MP') mpropia,getlinea(c.codpro) linea, "
                    sql = sql & " getmarca(c.codpro) marca,  "
                    sql = sql & " nvl(d.cantidad,1) cantidad , "
                    sql = sql & " GET_PRELIS(3,b.codpromp," + txtRutcli.Text + ") PRECIO,  "
                    sql = sql & " nvl(decode(f.costo,'',C.COSTO,f.costo),0) ""Costo Analisis"",  "
                    'sql = sql & " nvl(decode(f.costo,'',C.COSTO,f.costo),0) ""Costo Comercial"",   "
                    sql = sql & " nvl(C.COSTO,0) ""Costo Comercial"",  "
                    sql = sql & " nvl(decode(f.costo,'',C.cosprom,f.costo),0) ""Costo Promedio"",  "
                    sql = sql & " 0 ""Costo Especial"",  "
                    sql = sql & " nvl(f.costo,0) ""Costo Especial PM"",  "
                    sql = sql & " nvl(f.fecini,'') ""FecIni Costo Especial"",  "
                    sql = sql & " nvl(f.fecfin,'') ""FecFIn Costo Especial"",  "
                    sql = sql & " round(nvl(decode(f.costo,'',C.cosprom,f.costo),0) * (1-(nvl(g.aporte,0)/100)),2) ""Costo Rebate"",  "
                    sql = sql & " nvl(g.aporte,0) aporte, 0 PESO, round(((GET_PRELIS(3,b.codpromp," + txtRutcli.Text + ")-nvl(decode(f.costo,'',C.COSTO,f.costo),0))/GET_PRELIS(3,b.codpromp," + txtRutcli.Text + "))*100,2) ""Mg. Comercial"",  "
                    sql = sql & " round((1-nvl(decode(f.costo,'',C.cosprom,f.costo),0)/GET_PRELIS(3,b.codpromp," + txtRutcli.Text + "))*100,2) ""Mg. Inventario"",  "
                    sql = sql & " round((1-(nvl(decode(f.costo,'',C.cosprom,f.costo),0) * (1-(nvl(g.aporte,0)/100)))/GET_PRELIS(3,b.codpromp," + txtRutcli.Text + "))*100,2) ""Mg. Rebate"",  "
                    sql = sql & " round(nvl(d.cantidad,1) * GET_PRELIS(3,b.codpromp," + txtRutcli.Text + "),0) ""Valorizado Precio"", round(nvl(d.cantidad,1) * nvl(decode(f.costo,'',C.COSTO,f.costo),0),0) ""Valorizado Analisis"",  "
                    sql = sql & " round(nvl(d.cantidad,1) * nvl(decode(f.costo,'',C.cosprom,f.costo),0),0) ""Valorizado Inventario"",  "
                    sql = sql & " round(nvl(nvl(d.cantidad,1) * round(nvl(decode(f.costo,'',C.cosprom,f.costo),0) * (1-(nvl(g.aporte,0)/100)),2),0),0) ""Valorizado Rebate"",  "
                    sql = sql & " round((nvl(d.cantidad,1) * GET_PRELIS(3,b.codpromp," + txtRutcli.Text + "))  - (nvl(d.cantidad,1) * nvl(decode(f.costo,'',C.COSTO,f.costo),0)),0) ""Contribucion Comercial"",  "
                    sql = sql & " round(decode(e.tipo,'IMPORTADO',0,DECODE(greatest(c.costo,c.cosprom),c.costo,(nvl(d.cantidad,1) * nvl(decode(f.costo,'',C.COSTO,f.costo),0))-nvl(d.cantidad,1) * nvl(decode(f.costo,'',C.cosprom,f.costo),0),0)),0) ""Contrib. Inventario"",  "
                    sql = sql & " round(decode(e.tipo,'IMPORTADO',0,decode(nvl(g.aporte,0),0,0,decode(greatest(c.costo,c.cosprom),c.costo,(nvl(d.cantidad,1) * nvl(decode(f.costo,'',C.cosprom,f.costo),0)) -(nvl(d.cantidad,1) * round(nvl(decode(f.costo,'',C.cosprom,f.costo),0) * (1-(aporte/100)),2)),(nvl(d.cantidad,1) * nvl(decode(f.costo,'',C.COSTO,f.costo),0))- (nvl(d.cantidad,1) * round(nvl(decode(f.costo,'',C.cosprom,f.costo),0) * (1-(aporte/100)),2))))),0) ""Contrib. Rebate"",  "
                    sql = sql & " round(decode(e.tipo,'IMPORTADO',DECODE(greatest(c.costo,c.cosprom),c.costo,(nvl(d.cantidad,1) * nvl(decode(f.costo,'',C.COSTO,f.costo),0))-nvl(d.cantidad,1) * nvl(decode(f.costo,'',C.cosprom,f.costo),0),0),0),0) ""Contrib Rebate Mp"",  "
                    sql = sql & " GET_PRELIS(3,b.codpromp," + txtRutcli.Text + ") ""Precio Cotizacion"",  "
                    sql = sql & " round(nvl(d.cantidad,1) * GET_PRELIS(3,b.codpromp," + txtRutcli.Text + "),0) ""Valorizado Anterior"",  "
                    sql = sql & " getestadoprod(b.codpromp) estado,  "
                    sql = sql & " decode(getpedsto(b.codpromp),'S','STOCK','P','PEDIDO','VACIO') pedsto,  "
                    sql = sql & " 0 variacion, "
                    sql = sql & " GETCOSTOREPOSICION3(b.codpromp) cosrep, "
                    sql = sql & " nvl(GETULTIMACOMPRA('3',b.codpromp,'V'),0) ultcom, "
                    sql = sql & " 'SI' alternativo, b.codpro original ,b.ahorro,getcodigolinea(c.codpro) codlin,  "
                    sql = sql & " nvl(h.precio,0) ""Precio Min. Venta"", "
                    sql = sql & " nvl(i.precio,0) ""Precio Min. Coti"",nvl(c.cosprom,0) cosprom,'Alternativo' ""Estado Alternativo"" , x.cosprom ""Costo Santi."", x1.cosprom ""Costo Anto."", x2.cosfut "
                    sql = sql & "  from re_producto_alternativa b,  "
                    sql = sql & " ma_product c, (SELECT centro, codpro, cosprom FROM MA_COSTOSXCENTROS WHERE CENTRO IN(2001) and canal = 10 and codemp = 3) x, "
                    sql = sql & " (SELECT centro, codpro, cosprom FROM MA_COSTOSXCENTROS WHERE CENTRO IN(2003) and canal = 10 and codemp = 3) x1, (select * from ma_costo_futuro where to_Date(sysdate) between fecini and fecfin and codemp = 3) x2, "
                    sql = sql & " (select codpro,mpropia_hst,decode(greatest(nvl(sum(decode(tipo,'N',cantid*-1,cantid)),0),0),0,1,nvl(sum(decode(tipo,'N',cantid*-1,cantid)),0)) cantidad   "
                    sql = sql & " from qv_control_margen a  "
                    sql = sql & " where a.codemp = 3  "
                    sql = sql & " and a.rutcli in(" + rutrelacionado + ") "
                    sql = sql & " and a.fecha  "
                    sql = sql & " between to_date('" + fechaini + "','dd/mm/yyyy')   "
                    sql = sql & " and to_date('" + fechafin + "','dd/mm/yyyy')  "
                    sql = sql & " and ((tipo = 'F' and internet in (0,1)) or (tipo = 'G' and internet in (0,2)) or (tipo = 'N' and internet in (1,2,7,9)))  "
                    sql = sql & " group by codpro,mpropia_hst ) d,  "
                    sql = sql & " (select codpro,tipo from qv_mpropia_dimerc_hst  "
                    sql = sql & " where (fechavig,codpro) in(  "
                    sql = sql & " select max(fechavig),codpro from qv_mpropia_dimerc_hst   "
                    sql = sql & " group by codpro)) e,  "
                    sql = sql & " (select codpro,costo,FECINI,FECFIN from en_cliente_costo a  "
                    sql = sql & " where codemp=3      "
                    sql = sql & " and rutcli= " + txtRutcli.Text
                    sql = sql & " AND SYSDATE BETWEEN FECINI AND FECFIN      "
                    sql = sql & " and tipo=0 and estado='V') f,  "
                    sql = sql & " ma_prod_aporte g,  "
                    'sql = sql & " ma_product_negocio g,  "
                    sql = sql & " (select codpro,min(precio) precio from  "
                    sql = sql & "   (select b.codpro,min(b.precio) precio from en_factura a,de_factura b where a.codemp=3    "
                    sql = sql & "   and a.rutcli in(" + rutrelacionado + ")   "
                    sql = sql & "   and a.codemp=b.codemp   "
                    sql = sql & "   and a.numfac=b.numfac   "
                    sql = sql & "   and a.fecemi>= add_months(sysdate, -6)  group by b.codpro "
                    sql = sql & "   union "
                    sql = sql & "   select b.codpro,min(b.precio) precio from  en_factura a,de_guiades b,re_guiafac c where a.codemp=3    "
                    sql = sql & "   and a.rutcli in(" + rutrelacionado + ")   "
                    sql = sql & "   and a.codemp=b.codemp   "
                    sql = sql & "   and a.numfac=c.numfac "
                    sql = sql & "   and b.numgui=c.numgui   "
                    sql = sql & "   and a.fecemi>= add_months(sysdate, -6)  group by b.codpro) "
                    sql = sql & "   group by codpro ) h,"
                    sql = sql & " (select b.codpro,min(b.precio) precio from en_cotizac a,de_cotizac b "
                    sql = sql & " where a.codemp=3  "
                    sql = sql & " and a.rutcli in(" + rutrelacionado + ") "
                    sql = sql & " and a.codemp=b.codemp "
                    sql = sql & " and a.numcot=b.numcot "
                    sql = sql & " and a.fecemi>= add_months(sysdate, -6) "
                    sql = sql & " group by b.codpro) i "
                    sql = sql & " where b.codpro in(select codpro from de_cotizac  "
                    sql = sql & " where codemp=3 and numcot=" + txtNumcot.Text + ") "
                    sql = sql & " and b.codpromp=c.codpro "
                    sql = sql & " and c.estpro=1 "
                    sql = sql & " and c.codpro=d.codpro(+)  "
                    sql = sql & " and c.codpro=e.codpro(+)  "
                    sql = sql & " and c.codpro=f.codpro(+)  "
                    sql = sql & " and c.codpro=g.codpro(+) "
                    sql = sql & " and c.codpro=h.codpro(+) "
                    sql = sql & " and c.codpro=i.codpro(+) "
                    sql = sql & " and c.codpro=x.codpro(+) "
                    sql = sql & " and c.codpro=x1.codpro(+) "
                    sql = sql & " and c.codpro=x2.codpro(+) "
                    sql = sql & " and GET_PRELIS(3,b.codpromp," + txtRutcli.Text + ")>0"
                    sql = sql & " and c.codpro not in(select codpro from de_cotizac where codemp=3 and numcot=" + txtNumcot.Text + ") "
                    sql = sql & ") "
                    sql = sql & ") "
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    'DESPUES DE CARGAR LA TABLA CALCULO ESTADO RESULTADO ACTUAL, LUEGO HARE LAS CORRECCIONES AUTOMATICAS Y PRESNETARE
                    'EL ESTADO RESULTADO NUEVO

                    sql = " select Linea,sum(""Valorizado Precio"") Valorizado, round((sum(""Valorizado Precio"")-sum(""Valorizado Analisis""))/ sum(""Valorizado Precio"")*100,2) ""MG. Lista"", "
                    sql = sql & "round((sum(""Contribucion Comercial"")+ sum(""Contrib. Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comercial"", "
                    sql = sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comer + Aporte MP"", "
                    sql = sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Rebate""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Global"" "
                    sql = sql & " from tmp_proceso_convenio "
                    sql = sql & " where ""Valorizado Precio"">0 and alternativo ='NO'"
                    sql = sql & " group by linea order by sum(""Valorizado Precio"") desc"
                    Dim dtResumenLinea As New DataTable
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    dataadapter = New OracleDataAdapter(comando)
                    dataadapter.Fill(dtResumenLinea)

                    Dim dtjuntar As New DataTable
                    sql = " select sum(""Valorizado Precio"") Valorizado, round((sum(""Valorizado Precio"")-sum(""Valorizado Analisis""))/ sum(""Valorizado Precio"")*100,2) ""MG. Lista"", "
                    sql = sql & "round((sum(""Contribucion Comercial"")+ sum(""Contrib. Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comercial"", "
                    sql = sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comer + Aporte MP"", "
                    sql = sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Rebate""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Global"" "
                    sql = sql & " from tmp_proceso_convenio "
                    sql = sql & " where ""Valorizado Precio"">0 and alternativo ='NO'"
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    dataadapter = New OracleDataAdapter(comando)
                    dataadapter.Fill(dtjuntar)

                    dtResumenLinea.Merge(dtjuntar)
                    dgvResumenLínea.DataSource = dtResumenLinea

                    dgvResumenLínea.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
                    dgvResumenLínea.Columns("Valorizado").DefaultCellStyle.Format = "n0"
                    dgvResumenLínea.Columns("Valorizado").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvResumenLínea.Columns("Mg. Lista").DefaultCellStyle.Format = "n2"
                    dgvResumenLínea.Columns("Mg. Lista").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvResumenLínea.Columns("Mg. Comercial").DefaultCellStyle.Format = "n2"
                    dgvResumenLínea.Columns("Mg. Comercial").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvResumenLínea.Columns("Mg. Comer + Aporte MP").DefaultCellStyle.Format = "n2"
                    dgvResumenLínea.Columns("Mg. Comer + Aporte MP").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvResumenLínea.Columns("Mg. Global").DefaultCellStyle.Format = "n2"
                    dgvResumenLínea.Columns("Mg. Global").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

                    sql = " select Linea, sum(""Valorizado Precio"") Valorizado,round((sum(""Valorizado Precio"")-sum(""Valorizado Analisis""))/ sum(""Valorizado Precio"")*100,2) ""MG. Lista"", "
                    sql = sql & "round((sum(""Contribucion Comercial"")+ sum(""Contrib. Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comercial"", "
                    sql = sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comer + Aporte MP"", "
                    sql = sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Rebate""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Global"" "
                    sql = sql & "from tmp_proceso_convenio "
                    sql = sql & " where ""Valorizado Precio"">0 and alternativo ='NO' "
                    sql = sql & "group by linea order by sum(""Valorizado Precio"") desc"
                    Dim dtDatosMp As New DataTable
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    dataadapter = New OracleDataAdapter(comando)
                    dataadapter.Fill(dtDatosMp)
                    dgvDatosMP.DataSource = dtDatosMp

                    Dim dtjuntar2 As New DataTable
                    sql = " select sum(""Valorizado Precio"") Valorizado,round((sum(""Valorizado Precio"")-sum(""Valorizado Analisis""))/ sum(""Valorizado Precio"")*100,2) ""MG. Lista"", "
                    sql = sql & "round((sum(""Contribucion Comercial"")+ sum(""Contrib. Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comercial"", "
                    sql = sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comer + Aporte MP"", "
                    sql = sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Rebate""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Global"" "
                    sql = sql & " from tmp_proceso_convenio "
                    sql = sql & " where ""Valorizado Precio"">0 and alternativo ='NO'"
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    dataadapter = New OracleDataAdapter(comando)
                    dataadapter.Fill(dtjuntar2)
                    dtDatosMp.Merge(dtjuntar2)
                    dgvDatosMP.DataSource = dtDatosMp

                    dgvDatosMP.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
                    dgvDatosMP.Columns("Valorizado").DefaultCellStyle.Format = "n0"
                    dgvDatosMP.Columns("Valorizado").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvDatosMP.Columns("Mg. Lista").DefaultCellStyle.Format = "n2"
                    dgvDatosMP.Columns("Mg. Lista").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvDatosMP.Columns("Mg. Comercial").DefaultCellStyle.Format = "n2"
                    dgvDatosMP.Columns("Mg. Comercial").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvDatosMP.Columns("Mg. Comer + Aporte MP").DefaultCellStyle.Format = "n2"
                    dgvDatosMP.Columns("Mg. Comer + Aporte MP").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvDatosMP.Columns("Mg. Global").DefaultCellStyle.Format = "n2"
                    dgvDatosMP.Columns("Mg. Global").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

                    sql = "select nvl(sum(""Valorizado Precio"") ,0)precio,sum(""Valorizado Analisis"") costo, "
                    sql += " round((sum(""Valorizado Precio"")-sum(""Valorizado Rebate""))/ sum(""Valorizado Precio"")*100,2) margen, round(avg(""Mg. Inventario""),2) margen2 from tmp_proceso_convenio where cantidad>0 and alternativo ='NO'"
                    Dim dt2 As New DataTable
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    dataadapter = New OracleDataAdapter(comando)
                    dataadapter.Fill(dt2)
                    If dt2.Rows.Count > 0 Then

                        If Convert.ToDouble(dt2.Rows(0).Item("precio")) > 0 Then
                            txtTotIngreso.Text = dt2.Rows(0).Item("precio")
                            txtTotContr.Text = Math.Round(dt2.Rows(0).Item("Precio") * (dt2.Rows(0).Item("margen") / 100))
                            txtPorTotCon.Text = dt2.Rows(0).Item("margen")
                            txtPorTotCon.Text = FormatPercent(txtPorTotCon.Text / 100, 2)
                            txtTotMargenFinal.Text = (dt2.Rows(0).Item("margen"))
                            txtTotMargenFinal.Text = FormatPercent(txtTotMargenFinal.Text / 100, 2)
                            txtTotIngreso.Text = FormatNumber(txtTotIngreso.Text, 0)
                            txtTotContr.Text = FormatNumber(txtTotContr.Text, 0)
                            txtValAnt.Text = dt2.Rows(0).Item("precio")
                            txtValAct.Text = dt2.Rows(0).Item("precio")
                            txtValAnt.Text = FormatNumber(txtValAnt.Text, 0)
                            txtValAct.Text = FormatNumber(txtValAct.Text, 0)
                            txtPorVariacion.Text = "0"
                        Else
                            txtTotIngreso.Text = "0"
                            txtTotContr.Text = "0"
                            txtTotMargenFinal.Text = "0"
                            txtPorTotCon.Text = "0"
                            txtValAnt.Text = "0"
                            txtValAct.Text = "0"
                            txtValAnt.Text = FormatNumber(txtValAnt.Text, 0)
                            txtValAct.Text = FormatNumber(txtValAct.Text, 0)
                            txtPorVariacion.Text = "0"
                            txtPorTotCon.Text = FormatPercent(txtPorTotCon.Text / 100, 2)
                        End If

                    End If

                    'Query deshabilitada por petición de JTAPIA (Para diferenciar criterios), dia 23-05-2018
                    ''CORRIJO MARGEN DE INVENTARIO, TODOS DEBEN SER MAYOR A 12

                    'sql = " update tmp_proceso_convenio  "
                    'sql = sql & "set precio = round(""Costo Promedio"" /0.88), "
                    'sql = sql & "variacion = round(((round((""Costo Promedio"" /0.88))- ""Precio Cotizacion"") / ""Precio Cotizacion"") * 100,2), "
                    'sql = sql & """Valorizado Precio"" = round(""Costo Promedio"" /0.88) * cantidad, "
                    'sql = sql & """Mg. Inventario"" = round((1-""Costo Promedio""/round(""Costo Promedio"" /0.88))*100,2), "
                    'sql = sql & """Mg. Comercial"" = round((1-""Costo Analisis""/round(""Costo Promedio"" /0.88))*100,2), "
                    'sql = sql & """Mg. Rebate"" = round((1-(""Costo Promedio"" * (1-(APORTE/100)))/round(""Costo Promedio"" /0.88))*100,2), "
                    'sql = sql & """Contribucion Comercial"" = round((CANTIDAD* round(""Costo Promedio"" /0.88))  - (CANTIDAD * ""Costo Analisis""),0) "
                    'sql = sql & "where ""Mg. Inventario"" <12 "

                    'comando = New OracleCommand(sql, conn)
                    'comando.Transaction = transaccion
                    'comando.ExecuteNonQuery()

                    If Convert.ToDouble(txtTotIngreso.Text) > 0 Then
                        'CALCULA EL PESO
                        sql = "  Declare            "
                        sql = sql & " Cursor C2 is (   "
                        sql = sql & " select codpro,SUM(CANTIDAD * PRECIO) MONTO,(SELECT SUM(CANTIDAD * PRECIO) FROM TMP_PROCESO_CONVENIO) TOTAL from tmp_proceso_convenio GROUP BY CODPRO  "
                        sql = sql & " );    "
                        sql = sql & " Begin   "
                        sql = sql & "     for D in C2 loop   "
                        sql = sql & "            update tmp_proceso_convenio  "
                        sql = sql & "            set PESO=ROUND((D.MONTO/D.TOTAL)*100,2)              "
                        sql = sql & "            where CODPRO=d.codpro;      "
                        sql = sql & "         end loop;   "
                        sql = sql & " End;    "
                        comando = New OracleCommand(sql, conn)
                        comando.Transaction = transaccion
                        comando.ExecuteNonQuery()
                    End If

                    'COmienza a generar propuesta de costo especial
                    'SOLO PARA PRODUCTOS IMPORTADO
                    'Si margen Inventario > 60 y mg. Comercial <20, se corrige el mg. comercial y se deja en 20

                    sql = " update tmp_proceso_convenio  "
                    sql = sql & "set ""Costo Especial"" = round(precio * 0.8), "
                    sql = sql & " ""Costo Analisis"" = round(precio * 0.8), "
                    sql = sql & " ""Valorizado Analisis"" = round(round(precio * 0.8) * cantidad,0), "
                    sql = sql & """Mg. Comercial"" = round((1-round(precio * 0.8)/precio)*100,2) "
                    sql = sql & "where ""Mg. Inventario"" > 60 and ""Mg. Comercial"" <20 and mpropia='IMPORTADO' "

                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    sql = " update tmp_proceso_convenio  "
                    sql = sql & "set ""Costo Especial"" = round(precio * 0.85), "
                    sql = sql & " ""Costo Analisis"" = round(precio * 0.85), "
                    sql = sql & " ""Valorizado Analisis"" = round(round(precio * 0.85) * cantidad,0), "
                    sql = sql & """Mg. Comercial"" = round((1-round(precio * 0.85)/precio)*100,2) "
                    sql = sql & "where ""Mg. Inventario"" > 50 and ""Mg. Inventario"" < 60 and ""Mg. Comercial"" < 15 and mpropia='IMPORTADO' "

                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    sql = " update tmp_proceso_convenio  "
                    sql = sql & "set ""Costo Especial"" = round(precio * 0.88), "
                    sql = sql & " ""Costo Analisis"" = round(precio * 0.88), "
                    sql = sql & " ""Valorizado Analisis"" = round(round(precio * 0.88) * cantidad,0), "
                    sql = sql & """Mg. Comercial"" = round((1-round(precio * 0.88)/precio)*100,2) "
                    sql = sql & "where ""Mg. Inventario"" > 40 and ""Mg. Inventario"" < 50 and ""Mg. Comercial"" < 12 and mpropia='IMPORTADO' "

                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    sql = " update tmp_proceso_convenio  "
                    sql = sql & "set ""Costo Especial"" = round(precio * 0.90), "
                    sql = sql & " ""Costo Analisis"" = round(precio * 0.90), "
                    sql = sql & " ""Valorizado Analisis"" = round(round(precio * 0.90) * cantidad,0), "
                    sql = sql & """Mg. Comercial"" = round((1-round(precio * 0.90)/precio)*100,2) "
                    sql = sql & "where ""Mg. Inventario"" > 30 and ""Mg. Inventario"" < 40 and ""Mg. Comercial"" < 10 and mpropia='IMPORTADO' "

                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    'SOLO PARA PRODUCTOS QUE NO SEAN IMPORTADO
                    'Si margen Inventario >8 y <15 se generan costos para que margen comercial quede 1 punto debajo del margen inventario

                    sql = " update tmp_proceso_convenio "
                    sql = sql & "set ""Costo Especial"" = round(precio * ((100-(""Mg. Inventario""-1))/100)),  "
                    sql = sql & " ""Costo Analisis"" = round(precio * ((100-(""Mg. Inventario""-1))/100)), "
                    sql = sql & " ""Valorizado Analisis"" = round(round(precio * ((100-(""Mg. Inventario""-1))/100)) * cantidad,0), "
                    sql = sql & """Mg. Comercial"" = round((1-round(precio * ((100-(""Mg. Inventario""-1))/100))/precio) * 100,2)  "
                    sql = sql & "where ""Mg. Inventario"" > 7 and ""Mg. Inventario""<15 and ""Mg. Comercial"" < (""Mg. Inventario""-1) and not mpropia ='IMPORTADO'"

                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    'Si margen Inventario >15 y <20 se generan costos para que margen comercial quede 3 punto debajo del margen inventario

                    sql = " update tmp_proceso_convenio "
                    sql = sql & "set ""Costo Especial"" = round(precio * ((100-(""Mg. Inventario""-3))/100)),  "
                    sql = sql & " ""Costo Analisis"" = round(precio * ((100-(""Mg. Inventario""-3))/100)),  "
                    sql = sql & " ""Valorizado Analisis"" = round(round(precio * ((100-(""Mg. Inventario""-3))/100)) * cantidad,0), "
                    sql = sql & """Mg. Comercial"" = round((1-round(precio * ((100-(""Mg. Inventario""-3))/100))/precio) * 100,2)  "
                    sql = sql & "where ""Mg. Inventario"" > 15 and ""Mg. Inventario""<20 and ""Mg. Comercial"" < (""Mg. Inventario""-3) and not mpropia ='IMPORTADO'"
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    'Si margen Inventario >20 y <30 se generan costos para que margen comercial quede 5 puntos debajo del margen inventario

                    sql = " update tmp_proceso_convenio "
                    sql = sql & "set ""Costo Especial"" = round(precio * ((100-(""Mg. Inventario""-5))/100)),  "
                    sql = sql & " ""Costo Analisis"" = round(precio * ((100-(""Mg. Inventario""-5))/100)),  "
                    sql = sql & " ""Valorizado Analisis"" = round(round(precio * ((100-(""Mg. Inventario""-5))/100)) * cantidad,0), "
                    sql = sql & """Mg. Comercial"" = round((1-round(precio * ((100-(""Mg. Inventario""-5))/100))/precio) * 100,2)  "
                    sql = sql & "where ""Mg. Inventario"" > 20 and ""Mg. Inventario""<30 and ""Mg. Comercial"" < (""Mg. Inventario""-5) and not mpropia ='IMPORTADO'"

                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    'Si margen Inventario >30 y <40 se generan costos para que margen comercial quede 10 puntos debajo del margen inventario

                    sql = " update tmp_proceso_convenio "
                    sql = sql & "set ""Costo Especial"" = round(precio * ((100-(""Mg. Inventario""-10))/100)),  "
                    sql = sql & " ""Costo Analisis"" = round(precio * ((100-(""Mg. Inventario""-10))/100)),  "
                    sql = sql & " ""Valorizado Analisis"" = round(round(precio * ((100-(""Mg. Inventario""-10))/100)) * cantidad,0), "
                    sql = sql & """Mg. Comercial"" = round((1-round(precio * ((100-(""Mg. Inventario""-10))/100))/precio) * 100,2)  "
                    sql = sql & "where ""Mg. Inventario"" > 30 and ""Mg. Inventario""<40 and ""Mg. Comercial"" < (""Mg. Inventario""-10) and not mpropia ='IMPORTADO'"

                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    'Si margen Inventario >40 y <60 se generan costos para que margen comercial quede 15 puntos debajo del margen inventario

                    sql = " update tmp_proceso_convenio "
                    sql = sql & "set ""Costo Especial"" = round(precio * ((100-(""Mg. Inventario""-15))/100)),  "
                    sql = sql & " ""Costo Analisis"" = round(precio * ((100-(""Mg. Inventario""-15))/100)),  "
                    sql = sql & " ""Valorizado Analisis"" = round(round(precio * ((100-(""Mg. Inventario""-15))/100)) * cantidad,0), "
                    sql = sql & """Mg. Comercial"" = round((1-round(precio * ((100-(""Mg. Inventario""-15))/100))/precio) * 100,2)  "
                    sql = sql & "where ""Mg. Inventario"" > 40 and ""Mg. Inventario""<60 and ""Mg. Comercial"" < (""Mg. Inventario""-15) and not mpropia ='IMPORTADO'"

                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    'Si margen Inventario >60  se generan costos para que margen comercial quede 20 puntos debajo del margen inventario

                    sql = " update tmp_proceso_convenio "
                    sql = sql & "set ""Costo Especial"" = round(precio * ((100-(""Mg. Inventario""-20))/100)),  "
                    sql = sql & " ""Costo Analisis"" = round(precio * ((100-(""Mg. Inventario""-20))/100)),  "
                    sql = sql & " ""Valorizado Analisis"" = round(round(precio * ((100-(""Mg. Inventario""-20))/100)) * cantidad,0), "
                    sql = sql & """Mg. Comercial"" = round((1-round(precio * ((100-(""Mg. Inventario""-20))/100))/precio) * 100,2)  "
                    sql = sql & "where ""Mg. Inventario"" > 40  and ""Mg. Comercial"" < (""Mg. Inventario""-20) and not mpropia ='IMPORTADO'"

                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    sql = " update tmp_proceso_convenio set  "
                    sql = sql & """Costo Rebate""= round(""Costo Promedio"" * (1-(nvl(aporte,0)/100)),2)  "
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    'COLOCA PRECIO DE PRODUCTO ORIGINAL A LOS PRODUCTOS ALTERNATIVOS
                    sql = " Declare           "
                    sql = sql & " Cursor C2 is (  "
                    sql = sql & " select codpro,precio from tmp_proceso_convenio where codpro in(select original from tmp_proceso_convenio where alternativo='SI') "
                    sql = sql & " );   "
                    sql = sql & " Begin  "
                    sql = sql & "     for D in C2 loop  "
                    sql = sql & "            update tmp_proceso_convenio "
                    sql = sql & "            set precio=d.precio, "
                    sql = sql & "            ""Mg. Comercial"" =  round(((d.precio-""Costo Analisis"")/d.precio)*100,2), "
                    sql = sql & "            ""Mg. Inventario"" =  round(((d.precio-""Costo Promedio"")/d.precio)*100,2), "
                    sql = sql & "            ""Mg. Rebate"" = round(((d.precio-""Costo Rebate"")/d.precio)*100,2) "
                    sql = sql & "            where original=d.codpro;     "
                    sql = sql & "         end loop;  "
                    sql = sql & " End;   "

                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    sql = " update tmp_proceso_convenio  "
                    sql = sql & "set precio = round(precio * (1-(nvl(ahorro,0)/100)),0) "
                    sql = sql & "where alternativo='SI' "
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    'Este proceso es para identificar productos originales duplicados y cuyo objetivo es dejar el producto con el menor precio encontrado
                    sql = " select codpro,original,precio from tmp_proceso_convenio "
                    sql = sql & "  where  (codpro) in ( "
                    sql = sql & "   select codpro "
                    sql = sql & " from   tmp_proceso_convenio "
                    sql = sql & " group  by codpro "
                    sql = sql & " having count(*) > 1 "
                    sql = sql & "  ) order by codpro,precio asc "
                    Dim dtEliminar As New DataTable
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    dataadapter = New OracleDataAdapter(comando)
                    dataadapter.Fill(dtEliminar)

                    If dtEliminar.Rows.Count > 0 Then
                        Dim anterior As String = dtEliminar(0).Item("codpro").ToString
                        For i = 1 To dtEliminar.Rows.Count - 1
                            If anterior <> dtEliminar(i).Item("codpro").ToString Then
                                anterior = dtEliminar(i).Item("codpro").ToString
                                Continue For
                            Else
                                sql = " delete from tmp_proceso_convenio where codpro='" + dtEliminar.Rows(i).Item("codpro") + "'"
                                sql += " And original='" + dtEliminar.Rows(i).Item("original") + "' "
                                comando = New OracleCommand(sql, conn)
                                comando.Transaction = transaccion
                                comando.ExecuteNonQuery()
                            End If
                        Next
                    End If

                    sql = " update tmp_proceso_convenio set  "
                    sql = sql & " ""Mg. Comercial"" =  round(((precio-""Costo Analisis"")/precio)*100,2), "
                    sql = sql & " ""Mg. Inventario"" =  round(((precio-""Costo Promedio"")/precio)*100,2), "
                    sql = sql & " ""Mg. Rebate"" = round(((precio-""Costo Rebate"")/precio)*100,2) "
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    Dim dtMargenes As New DataTable
                    For i = 0 To dgvDetalleLineaProductos.RowCount - 1
                        If dgvDetalleLineaProductos.Item("Alternativo", i).Value = "SI" Then
                            sql = "select ""Mg. Comercial"" margen,""Mg. Inventario"" inventario from tmp_proceso_convenio where codpro='" + dgvDetalleLineaProductos.Item("Original", i).Value + "'"
                            dtMargenes.Clear()
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            dataadapter = New OracleDataAdapter(comando)
                            dataadapter.Fill(dtMargenes)
                            If dtMargenes.Rows.Count > 0 Then
                                Dim inventario = Convert.ToDouble(dtMargenes.Rows(0).Item("inventario"))
                                Dim margen = Convert.ToDouble(dtMargenes.Rows(0).Item("margen")) + 5
                                If dgvDetalleLineaProductos.Item("Mg. Comercial", i).Value < margen Then
                                    If margen < inventario Then
                                        sql = " update tmp_proceso_convenio "
                                        sql = sql & "set ""Costo Especial"" = round(precio * ((100-(" + margen.ToString.Replace(",", ".") + "))/100)), "
                                        sql = sql & " ""Costo Analisis"" = round(precio * ((100-(" + margen.ToString.Replace(",", ".") + "))/100)),  "
                                        sql = sql & " ""Valorizado Analisis"" = round(round(precio * ((100-(" + margen.ToString.Replace(",", ".") + "))/100)) * cantidad,0), "
                                        sql = sql & """Mg. Comercial"" = round((1-round(precio * ((100-(" + margen.ToString.Replace(",", ".") + "))/100))/precio) * 100,2)  "
                                        sql = sql & "where codpro = '" + dgvDetalleLineaProductos.Item("Codpro", i).Value + "'"
                                        comando = New OracleCommand(sql, conn)
                                        comando.Transaction = transaccion
                                        comando.ExecuteNonQuery()
                                    End If
                                End If
                            End If
                        End If
                    Next

                    sql = " update tmp_proceso_convenio set  "
                    sql = sql & " ""Mg. Comercial"" =  round(((precio-""Costo Analisis"")/precio)*100,2), "
                    sql = sql & " ""Mg. Inventario"" =  round(((precio-""Costo Promedio"")/precio)*100,2), "
                    sql = sql & " ""Mg. Rebate"" = round(((precio-""Costo Rebate"")/precio)*100,2) "
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    sql = " update tmp_proceso_convenio set  "
                    sql = sql & """Valorizado Analisis""=round(""Costo Analisis"" * cantidad,0), "
                    sql = sql & """Valorizado Precio""=round(PRECIO * cantidad,0), "
                    sql = sql & """Valorizado Rebate"" = round(""Costo Rebate""*cantidad,0), "
                    sql = sql & """Valorizado Inventario"" = round(""Costo Promedio""*cantidad,0) "
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    sql = " update tmp_proceso_convenio set  "
                    sql = sql & """Contribucion Comercial"" = round(""Valorizado Precio""-""Valorizado Analisis"",0), "
                    sql = sql & """Contrib. Inventario"" = round(decode(mpropia,'IMPORTADO',0,DECODE(greatest(""Costo Analisis"",""Costo Promedio""),""Costo Analisis"",""Valorizado Analisis""-""Valorizado Inventario"",0))), "
                    sql = sql & " ""Contrib. Rebate""= round(decode(mpropia,'IMPORTADO',0,decode(aporte,0,0,""Valorizado Inventario""-""Valorizado Rebate"")),0), "
                    sql = sql & """Contrib Rebate Mp"" = round(decode(mpropia,'IMPORTADO',""Valorizado Analisis""-""Valorizado Inventario"" ,0),0) "
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    sql = " update tmp_proceso_convenio "
                    sql = sql & "  set precio=0, "
                    sql = sql & "   ""Mg. Comercial"" = 0, "
                    sql = sql & "   ""Mg. Inventario""=0, "
                    sql = sql & "   ""Mg. Rebate""=0, "
                    sql = sql & "   ""Valorizado Analisis""=0, "
                    sql = sql & "   ""Valorizado Precio""=0, "
                    sql = sql & "   ""Valorizado Rebate""=0, "
                    sql = sql & "   ""Valorizado Inventario""=0, "
                    sql = sql & "   ""Contribucion Comercial""=0, "
                    sql = sql & "   ""Contrib. Inventario""=0, "
                    sql = sql & "   ""Contrib. Rebate""=0, "
                    sql = sql & "   ""Contrib Rebate Mp""=0, "
                    sql = sql & "   ""Valorizado Anterior"" =0 "
                    sql = sql & "  where estado not in('ACTIVO DIMERC','AGOTADO EN MERCADO','ARTICULO NUEVO','EXCLUSIVO CONVENIO') "
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    transaccion.Commit()

                Catch ex As Exception
                    PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                    MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    If Not transaccion.Equals(Nothing) Then
                        transaccion.Rollback()
                    End If
                Finally
                    conn.Close()
                End Try
            End Using
        End Using
    End Sub

    Private Sub cmbMarcaPropia_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbMarcaPropia.SelectedIndexChanged
        If flag = True Then
            Try

                bd.open_dimerc()
                Dim Sql = " select Linea, sum(""Valorizado Precio"") Valorizado,round((sum(""Valorizado Precio"")-sum(""Valorizado Analisis""))/ sum(""Valorizado Precio"")*100,2) ""MG. Lista"", "
                Sql = Sql & "round((sum(""Contribucion Comercial"")+ sum(""Contrib. Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comercial"", "
                Sql = Sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comer + Aporte MP"", "
                Sql = Sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Rebate""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Global"" "
                Sql = Sql & " from tmp_proceso_convenio "
                Sql = Sql & " where ""Valorizado Precio"">0 and alternativo='NO' "
                If cmbMarcaPropia.SelectedIndex > 0 Then
                    Sql = Sql & " and mpropia='" + cmbMarcaPropia.SelectedValue.ToString + "' "
                End If
                Sql = Sql & "group by linea order by sum(""Valorizado Precio"") desc"
                Dim dtDatosMp As New DataTable
                dtDatosMp = bd.sqlSelect(Sql)

                Dim dtjuntar2 As New DataTable
                Sql = " select sum(""Valorizado Precio"") Valorizado,round((sum(""Valorizado Precio"")-sum(""Valorizado Analisis""))/ sum(""Valorizado Precio"")*100,2) ""MG. Lista"", "
                Sql = Sql & "round((sum(""Contribucion Comercial"")+ sum(""Contrib. Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comercial"", "
                Sql = Sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comer + Aporte MP"", "
                Sql = Sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Rebate""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Global"" "
                Sql = Sql & "from tmp_proceso_convenio "
                Sql = Sql & " where ""Valorizado Precio"">0 and alternativo='NO'"
                If cmbMarcaPropia.SelectedIndex > 0 Then
                    Sql = Sql & "and mpropia='" + cmbMarcaPropia.SelectedValue.ToString + "' "
                End If
                dtjuntar2 = bd.sqlSelect(Sql)
                dtDatosMp.Merge(dtjuntar2)
                dgvDatosMP.DataSource = dtDatosMp

                dgvDatosMP.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
                dgvDatosMP.Columns("Valorizado").DefaultCellStyle.Format = "n0"
                dgvDatosMP.Columns("Valorizado").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgvDatosMP.Columns("Mg. Lista").DefaultCellStyle.Format = "n2"
                dgvDatosMP.Columns("Mg. Lista").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgvDatosMP.Columns("Mg. Comercial").DefaultCellStyle.Format = "n2"
                dgvDatosMP.Columns("Mg. Comercial").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgvDatosMP.Columns("Mg. Comer + Aporte MP").DefaultCellStyle.Format = "n2"
                dgvDatosMP.Columns("Mg. Comer + Aporte MP").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgvDatosMP.Columns("Mg. Global").DefaultCellStyle.Format = "n2"
                dgvDatosMP.Columns("Mg. Global").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

            Catch ex As Exception
                PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Finally
                bd.close()
            End Try
        End If
    End Sub
    Private Sub formatogrilla()
        dgvDetalleLineaProductos.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
        dgvDetalleLineaProductos.Columns("Cantidad").DefaultCellStyle.Format = "n0"
        dgvDetalleLineaProductos.Columns("Cantidad").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Precio").DefaultCellStyle.Format = "n0"
        dgvDetalleLineaProductos.Columns("Precio").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Costo Analisis").DefaultCellStyle.Format = "n0"
        dgvDetalleLineaProductos.Columns("Costo Analisis").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Costo Comercial").DefaultCellStyle.Format = "n0"
        dgvDetalleLineaProductos.Columns("Costo Comercial").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Costo Promedio").DefaultCellStyle.Format = "n0"
        dgvDetalleLineaProductos.Columns("Costo Promedio").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Costo Especial").DefaultCellStyle.Format = "n0"
        dgvDetalleLineaProductos.Columns("Costo Especial").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Costo Especial PM").DefaultCellStyle.Format = "n0"
        dgvDetalleLineaProductos.Columns("Costo Especial PM").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Costo Rebate").DefaultCellStyle.Format = "n0"
        dgvDetalleLineaProductos.Columns("Costo Rebate").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Aporte").DefaultCellStyle.Format = "n2"
        dgvDetalleLineaProductos.Columns("Aporte").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Peso").DefaultCellStyle.Format = "n2"
        dgvDetalleLineaProductos.Columns("Peso").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Mg. Comercial").DefaultCellStyle.Format = "n2"
        dgvDetalleLineaProductos.Columns("Mg. Comercial").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Mg. Inventario").DefaultCellStyle.Format = "n2"
        dgvDetalleLineaProductos.Columns("Mg. Inventario").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Mg. Rebate").DefaultCellStyle.Format = "n2"
        dgvDetalleLineaProductos.Columns("Mg. Rebate").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Valorizado Precio").DefaultCellStyle.Format = "n0"
        dgvDetalleLineaProductos.Columns("Valorizado Precio").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Valorizado Analisis").DefaultCellStyle.Format = "n0"
        dgvDetalleLineaProductos.Columns("Valorizado Analisis").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Valorizado Inventario").DefaultCellStyle.Format = "n0"
        dgvDetalleLineaProductos.Columns("Valorizado Inventario").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Valorizado Rebate").DefaultCellStyle.Format = "n0"
        dgvDetalleLineaProductos.Columns("Valorizado Rebate").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Contribucion Comercial").DefaultCellStyle.Format = "n0"
        dgvDetalleLineaProductos.Columns("Contribucion Comercial").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Contrib. Inventario").DefaultCellStyle.Format = "n0"
        dgvDetalleLineaProductos.Columns("Contrib. Inventario").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Contrib. Rebate").DefaultCellStyle.Format = "n0"
        dgvDetalleLineaProductos.Columns("Contrib. Rebate").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Contrib Rebate MP").DefaultCellStyle.Format = "n0"
        dgvDetalleLineaProductos.Columns("Contrib Rebate MP").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Precio Cotizacion").DefaultCellStyle.Format = "n0"
        dgvDetalleLineaProductos.Columns("Precio Cotizacion").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Valorizado Anterior").DefaultCellStyle.Format = "n0"
        dgvDetalleLineaProductos.Columns("Valorizado Anterior").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Variacion").DefaultCellStyle.Format = "n2"
        dgvDetalleLineaProductos.Columns("Variacion").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Cosrep").DefaultCellStyle.Format = "n2"
        dgvDetalleLineaProductos.Columns("Cosrep").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalleLineaProductos.Columns("Ultcom").DefaultCellStyle.Format = "n2"
        dgvDetalleLineaProductos.Columns("Ultcom").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
    End Sub

    Private Sub txtCodpro_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCodpro.KeyPress
        e.Handled = True
    End Sub

    Private Sub dgvDetalleLineaProductos_CurrentCellChanged(sender As Object, e As EventArgs) Handles dgvDetalleLineaProductos.CurrentCellChanged
        If flaggrilla = True And Not dgvDetalleLineaProductos.CurrentRow Is Nothing Then
            txtCodpro.Text = dgvDetalleLineaProductos.Item("Codpro", dgvDetalleLineaProductos.CurrentRow.Index).Value.ToString
            txtDescripcion.Text = dgvDetalleLineaProductos.Item("Despro", dgvDetalleLineaProductos.CurrentRow.Index).Value.ToString
            txtPrecio.Text = dgvDetalleLineaProductos.Item("Precio", dgvDetalleLineaProductos.CurrentRow.Index).Value.ToString
            txtPrecioNuevo.Text = dgvDetalleLineaProductos.Item("Precio", dgvDetalleLineaProductos.CurrentRow.Index).Value.ToString
            txtCostoEspecial.Text = dgvDetalleLineaProductos.Item("Costo Especial", dgvDetalleLineaProductos.CurrentRow.Index).Value.ToString
            indice = dgvDetalleLineaProductos.CurrentRow.Index
        End If
    End Sub

    Private Sub dgvDetalleLineaProductos_KeyDown(sender As Object, e As KeyEventArgs) Handles dgvDetalleLineaProductos.KeyDown
        If e.KeyValue = Keys.Enter Then
            txtVariacion.Focus()
            e.Handled = True
            indice = dgvDetalleLineaProductos.CurrentRow.Index
        End If
    End Sub

    Private Sub txtVariacion_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtVariacion.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Not Trim(txtVariacion.Text) = "" Then
                txtPrecioNuevo.Text = Math.Round(txtPrecio.Text * (1 + (txtVariacion.Text / 100)))
            End If
            txtPrecioNuevo.Focus()
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub
    Private Sub txtCostoEspecial_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCostoEspecial.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            btn_Confirmar.Focus()
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub
    Private Sub cambiaprecio()
        If Not dgvDetalleLineaProductos.Item("Estado", indice).Value = "ACTIVO DIMERC" And
            Not dgvDetalleLineaProductos.Item("Estado", indice).Value = "ARTICULO NUEVO" And
            Not dgvDetalleLineaProductos.Item("Estado", indice).Value = "EXCLUSIVO CONVENIO" And
            Not dgvDetalleLineaProductos.Item("Estado", indice).Value = "AGOTADO EN MERCADO" Then
            MessageBox.Show("Producto descontinuado, no puede modificarlo", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        Using conn = New OracleConnection(Conexion.retornaConexion)
            conn.Open()
            Dim comando As OracleCommand
            Using transaccion = conn.BeginTransaction
                Try
                    If Trim(txtCostoEspecial.Text) = "" Then
                        txtCostoEspecial.Text = "0"
                    End If

                    Dim Sql = " update tmp_proceso_convenio  "
                    Sql = Sql & "set precio = " + txtPrecioNuevo.Text + ", "
                    Sql = Sql & "variacion = round(((" + txtPrecioNuevo.Text + "- ""Precio Cotizacion"") / ""Precio Cotizacion"") * 100,2), "
                    Sql = Sql & """Valorizado Precio"" = round(" + txtPrecioNuevo.Text + " * cantidad,0), "
                    If Convert.ToDouble(txtCostoEspecial.Text) > 0 Then
                        Sql = Sql & """Costo Especial""= " + txtCostoEspecial.Text + ","
                        Sql = Sql & """Costo Analisis""= " + txtCostoEspecial.Text + ","
                    End If
                    Sql = Sql & """Mg. Rebate"" = round((1-(""Costo Promedio"" * (1-(APORTE/100)))/" + txtPrecioNuevo.Text + ")*100,2) "
                    Sql = Sql & "where codpro='" + txtCodpro.Text + "' "

                    comando = New OracleCommand(Sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    If Convert.ToDouble(txtCostoEspecial.Text) > 0 Then
                        If txtCostoEspecial.Text < dgvDetalleLineaProductos.Item("Costo Promedio", indice).Value Then
                            Sql = " update tmp_proceso_convenio  "
                            Sql = Sql & "set ""Costo Promedio"" = " + txtCostoEspecial.Text + ", "
                            Sql = Sql & " ""Costo Comercial""= " + txtCostoEspecial.Text + ","
                            Sql = Sql & " ""Costo Rebate"" = round(" + txtCostoEspecial.Text + " * (1-(nvl(aporte,0)/100)),2)   "
                            Sql = Sql & " where codpro='" + txtCodpro.Text + "' "
                            comando = New OracleCommand(Sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()

                        End If
                    End If

                    Sql = " update tmp_proceso_convenio set  "
                    Sql = Sql & """Valorizado Analisis""=round(""Costo Analisis"" * cantidad,0), "
                    Sql = Sql & """Valorizado Inventario""=round(""Costo Promedio"" * cantidad,0), "
                    Sql = Sql & """Valorizado Rebate""=round(""Costo Rebate"" * cantidad,0), "
                    Sql = Sql & """Mg. Comercial"" = round((1-""Costo Analisis""/precio)*100,2), "
                    Sql = Sql & """Mg. Inventario"" = round((1-""Costo Promedio""/precio)*100,2), "
                    Sql = Sql & """Mg. Rebate"" = round((1-""Costo Rebate""/precio)*100,2) "
                    Sql = Sql & "  where estado in('ACTIVO DIMERC','AGOTADO EN MERCADO','ARTICULO NUEVO','EXCLUSIVO CONVENIO') "
                    comando = New OracleCommand(Sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    Sql = " update tmp_proceso_convenio set ""Contribucion Comercial"" = round(""Valorizado Precio""-""Valorizado Analisis"",0), "
                    Sql = Sql & """Contrib. Inventario"" =round(decode(mpropia,'IMPORTADO',0,DECODE(greatest(""Costo Analisis"",""Costo Promedio""),""Costo Analisis"",""Valorizado Analisis""-""Valorizado Inventario"",0))), "
                    Sql = Sql & " ""Contrib. Rebate""= round(decode(mpropia,'IMPORTADO',0,decode(aporte,0,0,""Valorizado Inventario""-""Valorizado Rebate"")),0), "
                    Sql = Sql & """Contrib Rebate Mp"" = round(decode(mpropia,'IMPORTADO',""Valorizado Analisis""-""Valorizado Inventario"" ,0),0) "
                    Sql = Sql & "  where estado in('ACTIVO DIMERC','AGOTADO EN MERCADO','ARTICULO NUEVO','EXCLUSIVO CONVENIO') "
                    comando = New OracleCommand(Sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    transaccion.Commit()

                    flaggrilla = False
                    Sql = "select * from tmp_proceso_convenio"
                    dgvDetalleLineaProductos.DataSource = bd.sqlSelect(Sql)
                    formatogrilla()
                    flaggrilla = True

                    MessageBox.Show("Cambio Listo")

                Catch ex As Exception
                    If Not transaccion.Equals(Nothing) Then
                        transaccion.Rollback()
                    End If
                    PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                    MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Finally
                    conn.Close()
                End Try
            End Using
        End Using
    End Sub

    Private Sub recalculaEstadoResultado()
        Try
            bd.open_dimerc()
            Dim sql = " select distinct codlin ""Codigo"",getnombrelinea(codlin) ""Linea"",'' ""Fecha""  "
            sql = sql & " from tmp_proceso_convenio  "
            sql = sql & " order by getnombrelinea(codlin) "
            dgvLineasReajuste.DataSource = Nothing
            dgvLineasReajuste.DataSource = bd.sqlSelect(sql)

            sql = " select Linea,sum(""Valorizado Precio"") Valorizado, sum(""Valorizado Precio""-""Valorizado Anterior"") Diferencia,round((sum(""Valorizado Precio"")-sum(""Valorizado Analisis""))/ sum(""Valorizado Precio"")*100,2) ""MG. Lista"", "
            sql = Sql & "round((sum(""Contribucion Comercial"")+ sum(""Contrib. Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comercial"", "
            Sql = Sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comer + Aporte MP"", "
            Sql = Sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Rebate""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Global"" "
            Sql = Sql & " from tmp_proceso_convenio "
            Sql = Sql & " where ""Valorizado Precio"">0 and alternativo='NO' "
            Sql = Sql & "group by linea order by sum(""Valorizado Precio"") desc"
            Dim dtResumenLinea As New DataTable
            dtResumenLinea = bd.sqlSelect(Sql)

            Dim dtjuntar As New DataTable
            sql = " select sum(""Valorizado Precio"") Valorizado,sum(""Valorizado Precio""-""Valorizado Anterior"") Diferencia, round((sum(""Valorizado Precio"")-sum(""Valorizado Analisis""))/ sum(""Valorizado Precio"")*100,2) ""MG. Lista"", "
            sql = Sql & "round((sum(""Contribucion Comercial"")+ sum(""Contrib. Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comercial"", "
            Sql = Sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comer + Aporte MP"", "
            Sql = Sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Rebate""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Global"" "
            Sql = Sql & " from tmp_proceso_convenio "
            Sql = Sql & " where ""Valorizado Precio"">0 and alternativo='NO'"
            dtjuntar = bd.sqlSelect(Sql)

            dtResumenLinea.Merge(dtjuntar)
            dgvLineaFinal.DataSource = dtResumenLinea

            dgvLineaFinal.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            dgvLineaFinal.Columns("Valorizado").DefaultCellStyle.Format = "n0"
            dgvLineaFinal.Columns("Valorizado").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLineaFinal.Columns("Diferencia").DefaultCellStyle.Format = "n0"
            dgvLineaFinal.Columns("Diferencia").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLineaFinal.Columns("Mg. Lista").DefaultCellStyle.Format = "n2"
            dgvLineaFinal.Columns("Mg. Lista").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLineaFinal.Columns("Mg. Comercial").DefaultCellStyle.Format = "n2"
            dgvLineaFinal.Columns("Mg. Comercial").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLineaFinal.Columns("Mg. Comer + Aporte MP").DefaultCellStyle.Format = "n2"
            dgvLineaFinal.Columns("Mg. Comer + Aporte MP").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLineaFinal.Columns("Mg. Global").DefaultCellStyle.Format = "n2"
            dgvLineaFinal.Columns("Mg. Global").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

            sql = " select Linea, sum(""Valorizado Precio"") Valorizado,sum(""Valorizado Precio""-""Valorizado Anterior"") Diferencia,round((sum(""Valorizado Precio"")-sum(""Valorizado Analisis""))/ sum(""Valorizado Precio"")*100,2) ""MG. Lista"", "
            sql = Sql & "round((sum(""Contribucion Comercial"")+ sum(""Contrib. Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comercial"", "
            Sql = Sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comer + Aporte MP"", "
            Sql = Sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Rebate""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Global"" "
            Sql = Sql & " from tmp_proceso_convenio "
            Sql = Sql & " where ""Valorizado Precio"">0 and alternativo='NO'"
            Sql = Sql & "group by linea order by sum(""Valorizado Precio"") desc"
            Dim dtDatosMp As New DataTable
            dtDatosMp = bd.sqlSelect(Sql)

            Dim dtjuntar2 As New DataTable
            sql = " select sum(""Valorizado Precio"") Valorizado,sum(""Valorizado Precio""-""Valorizado Anterior"") Diferencia,round((sum(""Valorizado Precio"")-sum(""Valorizado Analisis""))/ sum(""Valorizado Precio"")*100,2) ""MG. Lista"", "
            sql = Sql & "round((sum(""Contribucion Comercial"")+ sum(""Contrib. Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comercial"", "
            Sql = Sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comer + Aporte MP"", "
            Sql = Sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Rebate""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Global"" "
            Sql = Sql & " from tmp_proceso_convenio "
            Sql = Sql & " where ""Valorizado Precio"">0 and alternativo='NO'"
            dtjuntar2 = bd.sqlSelect(Sql)
            dtDatosMp.Merge(dtjuntar2)
            dgvMpFinal.DataSource = dtDatosMp

            dgvMpFinal.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            dgvMpFinal.Columns("Valorizado").DefaultCellStyle.Format = "n0"
            dgvMpFinal.Columns("Valorizado").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvMpFinal.Columns("Diferencia").DefaultCellStyle.Format = "n0"
            dgvMpFinal.Columns("Diferencia").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvMpFinal.Columns("Mg. Lista").DefaultCellStyle.Format = "n2"
            dgvMpFinal.Columns("Mg. Lista").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvMpFinal.Columns("Mg. Comercial").DefaultCellStyle.Format = "n2"
            dgvMpFinal.Columns("Mg. Comercial").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvMpFinal.Columns("Mg. Comer + Aporte MP").DefaultCellStyle.Format = "n2"
            dgvMpFinal.Columns("Mg. Comer + Aporte MP").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvMpFinal.Columns("Mg. Global").DefaultCellStyle.Format = "n2"
            dgvMpFinal.Columns("Mg. Global").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

            sql = "select nvl(sum(""Valorizado Precio""),0) Ingreso, nvl(sum(""Valorizado Anterior""),0) Anterior, sum(cantidad * ""Costo Promedio"") costo "
            sql += ", round((sum(""Valorizado Precio"")-sum(""Valorizado Rebate""))/ sum(""Valorizado Precio"")*100,2) margen, "
            sql += "round((sum(""Valorizado Anterior"")-sum(""Valorizado Rebate""))/ sum(""Valorizado Anterior"")*100,2) margen_old from tmp_proceso_convenio where cantidad>0 And alternativo='NO'"
            Dim dt = bd.sqlSelect(Sql)
            If dt.Rows.Count > 0 Then
                If dt.Rows(0).Item("Ingreso") > 0 Then
                    txtTotIngresiFinal.Text = dt.Rows(0).Item("Ingreso")
                    txtValAct.Text = dt.Rows(0).Item("Ingreso")
                    txtValAnt.Text = dt.Rows(0).Item("Anterior")
                    txtTotContriFinal.Text = Math.Round(dt.Rows(0).Item("Ingreso") * (dt.Rows(0).Item("margen") / 100))
                    txtPorContriFinal.Text = dt.Rows(0).Item("margen")
                    txtPorContriFinal.Text = FormatPercent(txtPorContriFinal.Text / 100, 2)
                    txtcontant.Text = Math.Round(txtValAnt.Text * (dt.Rows(0).Item("margen_old") / 100), 2)
                    txtMgFinal.Text = (dt.Rows(0).Item("margen"))
                    txtMgFinal.Text = FormatPercent(txtMgFinal.Text / 100, 2)
                    txtTotIngresiFinal.Text = FormatNumber(txtTotIngresiFinal.Text, 0)
                    txtTotContriFinal.Text = FormatNumber(txtTotContriFinal.Text, 0)
                    txtcontant.Text = FormatNumber(txtcontant.Text, 0)
                    txtValAct.Text = FormatNumber(txtValAct.Text, 0)
                    txtValAnt.Text = FormatNumber(txtValAnt.Text, 0)
                    txtPorVariacion.Text = txtValAct.Text / txtValAnt.Text - 1
                    'txtPorContriFinal.Text = FormatPercent(txtPorContriFinal.Text, 2)
                    txtPorVariacion.Text = FormatPercent(txtPorVariacion.Text, 2)
                    calculaEstadoFinal()
                    cargaComparativaEERR()
                Else
                    txtTotIngresiFinal.Text = "0"
                    txtValAct.Text = "0"
                    txtTotContriFinal.Text = "0"
                    txtPorContriFinal.Text = "0"
                    txtMgFinal.Text = "0"
                    txtMgFinal.Text = FormatPercent(txtMgFinal.Text, 2)
                    txtTotIngresiFinal.Text = FormatNumber(txtTotIngresiFinal.Text, 0)
                    txtTotContriFinal.Text = FormatNumber(txtTotContriFinal.Text, 0)
                    txtValAct.Text = FormatNumber(txtValAct.Text, 0)
                    txtPorVariacion.Text = "0"
                    'txtPorContriFinal.Text = FormatPercent(txtPorContriFinal.Text, 2)
                    txtPorVariacion.Text = FormatPercent(txtPorVariacion.Text, 2)
                    calculaEstadoFinal()
                    cargaComparativaEERR()
                End If
            End If

        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub btn_Confirmar_Click(sender As Object, e As EventArgs) Handles btn_Confirmar.Click
        cambiaprecio()
        recalculaEstadoResultado()
        PuntoControl.RegistroUso("59")
    End Sub

    Private Sub txtPorTransporte_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPorTransporte.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Trim(txtPorTransporte.Text) <> "," Then
                txtTotTrans.Text = Math.Round((txtPorTransporte.Text / 100) * txtTotIngreso.Text)
                txtPorTransFinal.Text = txtPorTransporte.Text
                txtTotTransFinal.Text = Math.Round((txtPorTransFinal.Text / 100) * txtTotIngresiFinal.Text)
                txtPorVenta.Focus()
                formatear()
                calculaEstado()
                calculaEstadoFinal()
            End If
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtPorTransFinal_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPorTransFinal.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Trim(txtPorTransFinal.Text) <> "," Then
                txtTotTransFinal.Text = Math.Round((txtPorTransFinal.Text / 100) * txtTotIngresiFinal.Text)
                formatear()
                txtPorVentaFinal.Focus()
                calculaEstadoFinal()
            End If
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtPorVenta_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPorVenta.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Trim(txtPorVenta.Text) <> "," Then
                Dim costo = (Convert.ToDouble(txtTotIngreso.Text) - sumacosto) / Convert.ToDouble(txtTotIngreso.Text)
                txtTotVenta.Text = txtTotIngreso.Text * getMargenComercial() * (txtPorVenta.Text / 100)
                txtPorVentaFinal.Text = txtPorVenta.Text
                txtTotVentaFinal.Text = txtTotIngresiFinal.Text * getMargenComercial() * (txtPorVentaFinal.Text / 100)
                txtPorGastoOp.Focus()
                formatear()
                calculaEstado()
                calculaEstadoFinal()
            End If
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtPorVentaFinal_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPorVentaFinal.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Trim(txtPorVentaFinal.Text) <> "," Then
                Dim costo = (Convert.ToDouble(txtTotIngresiFinal.Text) - sumacostoFinal) / Convert.ToDouble(txtTotIngresiFinal.Text)
                txtTotVentaFinal.Text = txtTotIngresiFinal.Text * getMargenComercial() * (txtPorVentaFinal.Text / 100)
                txtPorGastoOpFinal.Focus()
                formatear()
                calculaEstadoFinal()
            End If

        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub
    Private Function getMargenComercial()
        Try
            bd.open_dimerc()
            Dim sql = "select round(1-sum(""Valorizado Analisis"")/sum(""Valorizado Precio"") ,4) margen from tmp_proceso_convenio where alternativo='NO'"
            Dim dt As DataTable = bd.sqlSelect(sql)
            If dt.Rows.Count > 0 Then
                Return Convert.ToDouble(dt.Rows(0).Item("margen"))
            Else
                Return 0
            End If
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return 0
        Finally
            bd.close()
        End Try
    End Function
    Private Sub txtPorGastoOp_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPorGastoOp.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then

            If Trim(txtPorGastoOp.Text) <> "," Then
                txtGastoOp.Text = Math.Round((txtPorGastoOp.Text / 100) * txtTotIngreso.Text)
                txtPorGastoOpFinal.Text = txtPorGastoOp.Text
                txtTotGastOpFinal.Text = Math.Round((txtPorGastoOpFinal.Text / 100) * txtTotIngresiFinal.Text)
                txtPorRebate.Focus()
                formatear()
                calculaEstado()
                calculaEstadoFinal()
            End If
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtPorGastoOpFinal_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPorGastoOpFinal.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Trim(txtPorGastoOpFinal.Text) <> "," Then
                txtTotGastOpFinal.Text = Math.Round((txtPorGastoOpFinal.Text / 100) * txtTotIngresiFinal.Text)

                txtPorRebateFinal.Focus()
                formatear()
                calculaEstadoFinal()
            End If
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtPorRebate_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPorRebate.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Trim(txtPorRebate.Text) <> "," Then
                txtPorRebateFinal.Text = txtPorRebate.Text
                txtTotRebate.Text = Math.Round((Trim(txtPorRebate.Text) / 100) * txtTotIngreso.Text)
                txtTotRebateFinal.Text = Math.Round((Trim(txtPorRebateFinal.Text) / 100) * txtTotIngresiFinal.Text)
                formatear()
                calculaEstado()
                calculaEstadoFinal()
            End If
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtPorRebateFinal_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPorRebateFinal.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Trim(txtPorRebateFinal.Text <> ",") Then
                txtTotRebateFinal.Text = Math.Round((Trim(txtPorRebateFinal.Text) / 100) * txtTotIngresiFinal.Text)
                formatear()
                calculaEstadoFinal()
            End If
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub calculaEstado()
        txtTotGasto.Text = ""
        Dim valor As Double = Convert.ToDouble(txtTotRebate.Text) + Convert.ToDouble(txtTotVenta.Text) + Convert.ToDouble(txtTotTrans.Text) + Convert.ToDouble(txtGastoOp.Text)
        txtTotGasto.Text = valor
        txtTotGasto.Text = FormatNumber(txtTotGasto.Text, 0)
        txtTotMargenFinal.Text = ""
        txtTotMargenFinal.Text = (Convert.ToDouble(txtTotContr.Text) - Convert.ToDouble(txtTotGasto.Text)) / Convert.ToDouble(txtTotIngreso.Text)
        txtTotMargenFinal.Text = FormatPercent(txtTotMargenFinal.Text, 2)
        txtTotRebate.Text = FormatNumber(txtTotRebate.Text, 0)
        txtGastoOp.Text = FormatNumber(txtGastoOp.Text, 0)
        txtTotVenta.Text = FormatNumber(txtTotVenta.Text, 0)
        txtTotTrans.Text = FormatNumber(txtTotTrans.Text, 0)
    End Sub

    Private Sub calculaEstadoFinal()
        txtTotTransFinal.Text = Math.Round((txtPorTransFinal.Text / 100) * txtTotIngresiFinal.Text)
        txtTotVentaFinal.Text = txtTotIngresiFinal.Text * getMargenComercial() * (txtPorVentaFinal.Text / 100)
        txtTotGastOpFinal.Text = Math.Round((txtPorGastoOpFinal.Text / 100) * txtTotIngresiFinal.Text)
        txtTotRebateFinal.Text = Math.Round((Trim(txtPorRebateFinal.Text) / 100) * txtTotIngresiFinal.Text)
        txtTotGastoFinal.Text = ""
        Dim valor As Double = Convert.ToDouble(txtTotRebateFinal.Text) + Convert.ToDouble(txtTotVentaFinal.Text) + Convert.ToDouble(txtTotTransFinal.Text) + Convert.ToDouble(txtTotGastOpFinal.Text)
        txtTotGastoFinal.Text = valor
        txtTotGastoFinal.Text = FormatNumber(txtTotGastoFinal.Text, 0)
        txtMgFinal.Text = ""
        txtMgFinal.Text = (Convert.ToDouble(txtTotContriFinal.Text) - Convert.ToDouble(txtTotGastoFinal.Text)) / Convert.ToDouble(txtTotIngresiFinal.Text)
        txtMgFinal.Text = FormatPercent(txtMgFinal.Text, 2)
        txtTotRebateFinal.Text = FormatNumber(txtTotRebateFinal.Text, 0)
        txtTotGastOpFinal.Text = FormatNumber(txtTotGastOpFinal.Text, 0)
        txtTotVentaFinal.Text = FormatNumber(txtTotVentaFinal.Text, 0)
        txtTotTransFinal.Text = FormatNumber(txtTotTransFinal.Text, 0)

        cargaComparativaEERR()
    End Sub
    Private Sub exportar()
        Try
            If IO.File.Exists("C:\dimerc\propuesta " & txtRazons.Text & ".xlsx") Then
                IO.File.Delete("C:\dimerc\propuesta " & txtRazons.Text & ".xlsx")
            End If

            Dim save As New SaveFileDialog
            save.Filter = "Archivo Excel | *.xlsx"
            save.InitialDirectory = "C:\dimerc"

            save.FileName = "propuesta " & txtRazons.Text & ".xlsx"
            'xlWorkBook.SaveAs("C:\dimerc\propuesta " & txtRazons.Text & ".xlsx", Excel.XlFileFormat.xlWorkbookNormal)
            If save.ShowDialog = System.Windows.Forms.DialogResult.OK Then

                Dim car As New frmCargando
                car.ProgressBar1.Minimum = 0
                car.ProgressBar1.Value = 0
                car.ProgressBar1.Maximum = dgvDetalleLineaProductos.RowCount * 2
                car.StartPosition = FormStartPosition.CenterScreen
                car.Show()
                Cursor.Current = Cursors.WaitCursor

                Dim xlApp As Object
                Dim xlWorkBook As Object = Nothing
                Dim xlWorkSheet As Object

                'xlApp = New Excel.ApplicationClass
                xlApp = CreateObject("Excel.Application")
                'xlApp.Visible = True

                bd.open_dimerc()
                Dim Sql = " select codpro, despro, marca, linea,precio,""Precio Cotizacion"" precot,""Mg. Comercial"" mg_comercial, "
                Sql += " cantidad, alternativo,decode(original,'',codpro,original) original,estado  "
                Sql += " from tmp_proceso_convenio  "
                Sql += " order by original,alternativo "
                Dim dtvendedor = bd.sqlSelect(Sql)

                If IO.File.Exists("C:\Program Files\Convenios_licitaciones\base.xlsx") Then xlWorkBook = xlApp.Workbooks.Open("C:\Program Files\Convenios_licitaciones\base.xlsx")
                'xlWorkBook = xlApp.Workbooks.Open("C:\test1.xlsx")
                xlWorkSheet = xlWorkBook.Worksheets("Propuesta Original")

                xlApp.ScreenUpdating = False
                'edit the cell with new value
                xlWorkSheet.Cells(9, 4) = txtRazons.Text
                xlWorkSheet.Cells(10, 4) = txtRutcli.Text
                xlWorkSheet.Cells(11, 4) = txtVendedor.Text
                xlWorkSheet.Cells(23, 2) = "Estos precios comienzan a regir desde : " + dtpFeciniConvenio.Value.Date.ToShortDateString
                Dim valorizadoSinMP As Double = 0
                Dim valorizadoConMP As Double = 0

                For i = 0 To dtvendedor.Rows.Count - 1
                    If Not dtvendedor.Rows(i).Item("Estado") = "ACTIVO DIMERC" And
                    Not dtvendedor.Rows(i).Item("Estado") = "ARTICULO NUEVO" And
                    Not dtvendedor.Rows(i).Item("Estado") = "EXCLUSIVO CONVENIO" And
                    Not dtvendedor.Rows(i).Item("Estado") = "AGOTADO EN MERCADO" Then
                        xlWorkSheet.Range("B" + (i + 29).ToString + "", "J" + (i + 29).ToString + "").Interior.Color = Excel.XlRgbColor.rgbNavajoWhite
                        xlWorkSheet.Cells(i + 29, 2) = (i + 1).ToString
                        xlWorkSheet.Cells(i + 29, 3) = dtvendedor.Rows(i).Item("Codpro").ToString
                        xlWorkSheet.Cells(i + 29, 4) = dtvendedor.Rows(i).Item("Despro").ToString
                        xlWorkSheet.Cells(i + 29, 5) = dtvendedor.Rows(i).Item("Marca").ToString
                        xlWorkSheet.Cells(i + 29, 6) = 0
                        xlWorkSheet.Cells(i + 29, 7) = 0
                        xlWorkSheet.Cells(i + 29, 8) = 0
                        xlWorkSheet.Cells(i + 29, 9) = 0
                        Continue For
                    End If
                    If dtvendedor.Rows(i).Item("Alternativo") = "SI" Then
                        xlWorkSheet.Range("B" + (i + 29).ToString + "", "J" + (i + 29).ToString + "").Interior.Color = Excel.XlRgbColor.rgbLightGrey
                        xlWorkSheet.Cells(i + 29, 2) = (i + 1).ToString
                        xlWorkSheet.Cells(i + 29, 3) = dtvendedor.Rows(i).Item("Codpro").ToString
                        xlWorkSheet.Cells(i + 29, 4) = dtvendedor.Rows(i).Item("Despro").ToString
                        xlWorkSheet.Cells(i + 29, 5) = dtvendedor.Rows(i).Item("Marca").ToString
                        xlWorkSheet.Cells(i + 29, 6) = dtvendedor.Rows(i - 1).Item("Precio").ToString
                        xlWorkSheet.Cells(i + 29, 7) = dtvendedor.Rows(i).Item("Precio").ToString
                        xlWorkSheet.Cells(i + 29, 8) = dtvendedor.Rows(i).Item("Mg_Comercial") / 100
                        xlWorkSheet.Cells(i + 29, 9) = (Math.Round((dtvendedor.Rows(i).Item("Precio") - dtvendedor.Rows(i - 1).Item("Precio").ToString) / dtvendedor.Rows(i - 1).Item("Precio").ToString * 100, 2)) / 100
                        xlWorkSheet.Cells(i + 29, 10) = dtvendedor.Rows(i - 1).Item("Original").ToString
                        valorizadoConMP = valorizadoConMP - dtvendedor.Rows(i - 1).Item("Precio").ToString * dtvendedor.Rows(i - 1).Item("Cantidad").ToString
                        valorizadoConMP += dtvendedor.Rows(i - 1).Item("Cantidad").ToString * dtvendedor.Rows(i).Item("Precio").ToString
                    Else
                        xlWorkSheet.Cells(i + 29, 2) = (i + 1).ToString
                        xlWorkSheet.Cells(i + 29, 3) = dtvendedor.Rows(i).Item("Codpro").ToString
                        xlWorkSheet.Cells(i + 29, 4) = dtvendedor.Rows(i).Item("Despro").ToString
                        xlWorkSheet.Cells(i + 29, 5) = dtvendedor.Rows(i).Item("Marca").ToString
                        xlWorkSheet.Cells(i + 29, 6) = dtvendedor.Rows(i).Item("Precot").ToString
                        xlWorkSheet.Cells(i + 29, 7) = dtvendedor.Rows(i).Item("Precio").ToString
                        xlWorkSheet.Cells(i + 29, 8) = dtvendedor.Rows(i).Item("Mg_Comercial") / 100
                        xlWorkSheet.Cells(i + 29, 9) = (Math.Round((dtvendedor.Rows(i).Item("Precio") - dtvendedor.Rows(i).Item("Precot")) / dtvendedor.Rows(i).Item("Precot") * 100, 2)) / 100
                        valorizadoSinMP += dtvendedor.Rows(i).Item("Precio").ToString * dtvendedor.Rows(i).Item("Cantidad").ToString
                        valorizadoConMP += dtvendedor.Rows(i).Item("Precio").ToString * dtvendedor.Rows(i).Item("Cantidad").ToString
                    End If

                    car.ProgressBar1.Value += 1
                Next

                Sql = " select sum(""Valorizado Anterior"") viejo,sum(""Valorizado Precio"") nuevo from tmp_proceso_convenio where alternativo='NO' "

                Dim dtcabeza = bd.sqlSelect(Sql)
                If dtcabeza.Rows.Count > 0 Then
                    xlWorkSheet.Cells(22, 7) = dtcabeza.Rows(0).Item("viejo").ToString
                    xlWorkSheet.Cells(22, 8) = valorizadoSinMP
                    xlWorkSheet.Cells(26, 7) = dtcabeza.Rows(0).Item("viejo").ToString
                    xlWorkSheet.Cells(26, 8) = valorizadoConMP
                End If

                xlWorkSheet = xlWorkBook.Worksheets("Data Original")

                For i = 0 To dgvDetalleLineaProductos.RowCount - 1

                    xlWorkSheet.Cells(i + 2, 1) = dgvDetalleLineaProductos.Item("Codpro", i).Value.ToString
                    xlWorkSheet.Cells(i + 2, 2) = dgvDetalleLineaProductos.Item("Despro", i).Value.ToString
                    xlWorkSheet.Cells(i + 2, 3) = dgvDetalleLineaProductos.Item("Mpropia", i).Value.ToString
                    xlWorkSheet.Cells(i + 2, 4) = dgvDetalleLineaProductos.Item("Linea", i).Value.ToString
                    xlWorkSheet.Cells(i + 2, 5) = dgvDetalleLineaProductos.Item("Marca", i).Value.ToString
                    xlWorkSheet.Cells(i + 2, 6) = IIf(dgvDetalleLineaProductos.Item("Cantidad", i).Value > 0, dgvDetalleLineaProductos.Item("Cantidad", i).Value, 0)
                    xlWorkSheet.Cells(i + 2, 7) = dgvDetalleLineaProductos.Item("Precio", i).Value.ToString
                    xlWorkSheet.Cells(i + 2, 8) = dgvDetalleLineaProductos.Item("Costo Analisis", i).Value.ToString
                    xlWorkSheet.Cells(i + 2, 9) = dgvDetalleLineaProductos.Item("Costo Promedio", i).Value.ToString
                    xlWorkSheet.Cells(i + 2, 10) = dgvDetalleLineaProductos.Item("Costo Especial", i).Value.ToString
                    xlWorkSheet.Cells(i + 2, 11) = dgvDetalleLineaProductos.Item("Costo Especial PM", i).Value.ToString
                    xlWorkSheet.Cells(i + 2, 12) = dgvDetalleLineaProductos.Item("Aporte", i).Value / 100
                    xlWorkSheet.Cells(i + 2, 13) = dgvDetalleLineaProductos.Item("Peso", i).Value / 100
                    'xlWorkSheet.Cells(i + 2, 14) = dgvDetalleLineaProductos.Item("Mg. Comercial", i).Value / 100
                    Dim oXLRange As Excel.Range
                    oXLRange = CType(xlWorkSheet.Cells(i + 2, 14), Excel.Range)
                    oXLRange.Formula = "=1-H" & i + 2 & "/G" & i + 2
                    'xlWorkSheet.Cells(i + 2, 15) = dgvDetalleLineaProductos.Item("Mg. Inventario", i).Value / 100

                    oXLRange = CType(xlWorkSheet.Cells(i + 2, 15), Excel.Range)
                    oXLRange.FormulaLocal = "=SI(H" & i + 2 & "<I" & i + 2 & ";1-H" & i + 2 & "/G" & i + 2 & ";1-I" & i + 2 & "/G" & i + 2 & ")"

                    'xlWorkSheet.Cells(i + 2, 16) = dgvDetalleLineaProductos.Item("Mg. Rebate", i).Value / 100
                    oXLRange = CType(xlWorkSheet.Cells(i + 2, 16), Excel.Range)
                    oXLRange.FormulaLocal = "=SI(H" & i + 2 & "<W" & i + 2 & ";1-H" & i + 2 & "/G" & i + 2 & ";1-W" & i + 2 & "/G" & i + 2 & ")"


                    'xlWorkSheet.Cells(i + 2, 17) = dgvDetalleLineaProductos.Item("Valorizado Precio", i).Value.ToString
                    oXLRange = CType(xlWorkSheet.Cells(i + 2, 17), Excel.Range)
                    oXLRange.Formula = "=G" & i + 2 & "*F" & i + 2

                    'xlWorkSheet.Cells(i + 2, 18) = dgvDetalleLineaProductos.Item("Valorizado Analisis", i).Value.ToString
                    ''CALCULAR VALORIZADO ANALISIS
                    oXLRange = CType(xlWorkSheet.Cells(i + 2, 18), Excel.Range)
                    oXLRange.Formula = "=H" & i + 2 & "*F" & i + 2

                    'CALCULAR VALORIZADO INVENTARIO
                    'xlWorkSheet.Cells(i + 2, 19) = dgvDetalleLineaProductos.Item("Valorizado Inventario", i).Value.ToString
                    oXLRange = CType(xlWorkSheet.Cells(i + 2, 19), Excel.Range)
                    oXLRange.Formula = "=I" & i + 2 & "*F" & i + 2

                    'deja con formula valorizado rebate
                    'CALCULAR VALORIZADO REBATE
                    'xlWorkSheet.Cells(i + 2, 20) = dgvDetalleLineaProductos.Item("Valorizado Rebate", i).Value.ToString
                    oXLRange = CType(xlWorkSheet.Cells(i + 2, 20), Excel.Range)
                    oXLRange.Formula = "=W" & i + 2 & "*F" & i + 2
                    xlWorkSheet.Cells(i + 2, 22) = dgvDetalleLineaProductos.Item("Costo Comercial", i).Value.ToString
                    'xlWorkSheet.Cells(i + 2, 23) = dgvDetalleLineaProductos.Item("Costo Rebate", i).Value.ToString
                    oXLRange = CType(xlWorkSheet.Cells(i + 2, 23), Excel.Range)
                    oXLRange.Formula = "=I" & i + 2 & "*(1-L" & i + 2 & ")"

                    xlWorkSheet.Cells(i + 2, 24) = dgvDetalleLineaProductos.Item("Cosprom", i).Value.ToString

                    'xlWorkSheet.Cells(i + 2, 28) = dgvDetalleLineaProductos.Item("Contribucion Comercial", i).Value.ToString
                    oXLRange = CType(xlWorkSheet.Cells(i + 2, 28), Excel.Range)
                    oXLRange.Formula = "=(Q" & i + 2 & "-R" & i + 2 & ")"
                    'xlWorkSheet.Cells(i + 2, 29) = dgvDetalleLineaProductos.Item("Contrib. Inventario", i).Value.ToString

                    oXLRange = CType(xlWorkSheet.Cells(i + 2, 29), Excel.Range)
                    oXLRange.FormulaLocal = "=SI(C" & i + 2 & "<>""IMPORTADO"";SI(R" & i + 2 & ">S" & i + 2 & ";(R" & i + 2 & "-S" & i + 2 & ");0);0)"
                    'xlWorkSheet.Cells(i + 2, 30) = dgvDetalleLineaProductos.Item("Contrib. Rebate", i).Value.ToString

                    oXLRange = CType(xlWorkSheet.Cells(i + 2, 30), Excel.Range)
                    oXLRange.FormulaLocal = "=SI(C" & i + 2 & "<>""IMPORTADO"";SI(S" & i + 2 & ">T" & i + 2 & ";SI(R" & i + 2 & ">S" & i + 2 & ";(S" & i + 2 & "-T" & i + 2 & ");(R" & i + 2 & "-T" & i + 2 & "));0);0)"
                    'xlWorkSheet.Cells(i + 2, 31) = dgvDetalleLineaProductos.Item("Contrib Rebate MP", i).Value.ToString

                    oXLRange = CType(xlWorkSheet.Cells(i + 2, 31), Excel.Range)
                    oXLRange.FormulaLocal = "=SI(C" & i + 2 & "=""IMPORTADO"";SI(R" & i + 2 & ">S" & i + 2 & ";(R" & i + 2 & "-S" & i + 2 & ");0);0)"
                    xlWorkSheet.Cells(i + 2, 32) = dgvDetalleLineaProductos.Item("Precio Cotizacion", i).Value.ToString
                    xlWorkSheet.Cells(i + 2, 39) = dgvDetalleLineaProductos.Item("Estado", i).Value.ToString
                    xlWorkSheet.Cells(i + 2, 40) = dgvDetalleLineaProductos.Item("Pedsto", i).Value.ToString
                    xlWorkSheet.Cells(i + 2, 41) = dgvDetalleLineaProductos.Item("UltCom", i).Value.ToString
                    xlWorkSheet.Cells(i + 2, 42) = dgvDetalleLineaProductos.Item("Cosrep", i).Value.ToString
                    xlWorkSheet.Cells(i + 2, 43) = dgvDetalleLineaProductos.Item("Variacion", i).Value / 100
                    xlWorkSheet.Cells(i + 2, 44) = dgvDetalleLineaProductos.Item("FecIni Costo Especial", i).Value.ToString
                    xlWorkSheet.Cells(i + 2, 45) = dgvDetalleLineaProductos.Item("FecFin Costo Especial", i).Value.ToString
                    xlWorkSheet.Cells(i + 2, 46) = dgvDetalleLineaProductos.Item("Valorizado Anterior", i).Value.ToString
                    xlWorkSheet.Cells(i + 2, 47) = dgvDetalleLineaProductos.Item("Estado Alternativo", i).Value.ToString
                    xlWorkSheet.Cells(i + 2, 48) = dgvDetalleLineaProductos.Item("Precio Min. Venta", i).Value.ToString
                    xlWorkSheet.Cells(i + 2, 49) = dgvDetalleLineaProductos.Item("Precio Min. Coti", i).Value.ToString
                    xlWorkSheet.Cells(i + 2, 50) = dgvDetalleLineaProductos.Item("Costo Santi.", i).Value.ToString
                    xlWorkSheet.Cells(i + 2, 51) = dgvDetalleLineaProductos.Item("Costo Anto.", i).Value.ToString
                    xlWorkSheet.Cells(i + 2, 52) = dgvDetalleLineaProductos.Item("cosfut", i).Value.ToString
                    oXLRange = CType(xlWorkSheet.Cells(i + 2, 53), Excel.Range)
                    oXLRange.FormulaLocal = "=(V" & i + 2 & "-X" & i + 2 & ")/X" & i + 2

                    car.ProgressBar1.Value += 1

                    Marshal.ReleaseComObject(xlWorkSheet.Cells)
                Next

                xlWorkSheet = xlWorkBook.Worksheets("Resumen Original")

                'edit the cell with new value
                'xlWorkSheet.Cells(6, 2) = txtRazons.Text
                'xlWorkSheet.Cells(7, 2) = txtRutcli.Text
                'xlWorkSheet.Cells(10, 3) = txtTotIngresiFinal.Text
                'xlWorkSheet.Cells(11, 2) = txtPorContriFinal.Text
                'xlWorkSheet.Cells(11, 3) = txtTotContriFinal.Text
                If Trim(txtPorTransFinal.Text) = "," Then
                    xlWorkSheet.Cells(12, 2) = txtPorTransFinal.Text = "0,0"

                Else
                    xlWorkSheet.Cells(12, 2) = txtPorTransFinal.Text / 100
                End If
                If Trim(txtPorVentaFinal.Text) = "," Then
                    xlWorkSheet.Cells(13, 2) = "0,0"
                Else
                    xlWorkSheet.Cells(13, 2) = txtPorVentaFinal.Text / 100
                End If
                If Trim(txtPorGastoOp.Text) = "," Then
                    xlWorkSheet.Cells(14, 2) = "0,0"
                Else
                    xlWorkSheet.Cells(14, 2) = txtPorGastoOp.Text / 100
                End If
                'xlWorkSheet.Cells(12, 3) = txtTotTransFinal.Text

                'xlWorkSheet.Cells(13, 3) = txtTotVentaFinal.Text
                If Trim(txtPorRebateFinal.Text) = "," Then
                    xlWorkSheet.Cells(15, 2) = "0,0"
                Else
                    xlWorkSheet.Cells(15, 2) = txtPorRebateFinal.Text / 100
                End If

                'xlWorkSheet.Cells(14, 3) = txtTotGastOpFinal.Text

                'xlWorkSheet.Cells(15, 3) = txtTotRebateFinal.Text
                'xlWorkSheet.Cells(16, 3) = txtTotGastoFinal.Text
                'xlWorkSheet.Cells(17, 3) = txtTotMargenFinal.Text

                xlWorkSheet.Activate()
                Dim pt As Excel.PivotTable
                pt = xlWorkSheet.PivotTables("Tabla dinámica3")
                pt.PivotCache.Refresh()

                pt = xlWorkSheet.PivotTables("Tabla dinámica1")
                pt.PivotCache.Refresh()

                xlApp.ScreenUpdating = True

                car.Close()
                xlApp.DisplayAlerts = False
                'xlWorkBook.Close(SaveChanges:=True)

                xlWorkBook.SaveAs(save.FileName)
                xlWorkBook.Close()

                xlApp.DisplayAlerts = True
                xlApp.Quit()

                'xlWorkSheet.ReleaseComObject(pt)
                Marshal.ReleaseComObject(pt)
                Marshal.ReleaseComObject(xlApp)
                Marshal.ReleaseComObject(xlWorkBook)
                Marshal.ReleaseComObject(xlWorkSheet)

                MessageBox.Show("Listo")

            End If
            Cursor.Current = Cursors.Arrow
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End Try
    End Sub

    Private Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub

    Private Sub cmbMarcaPropiaFinal_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbMarcaPropiaFinal.SelectedIndexChanged
        If flag = True Then
            Try
                bd.open_dimerc()
                Dim Sql = " select Linea, sum(""Valorizado Precio"") Valorizado,sum(""Valorizado Precio""-""Valorizado Anterior"") Diferencia,round((sum(""Valorizado Precio"")-sum(""Valorizado Analisis""))/ sum(""Valorizado Precio"")*100,2) ""MG. Lista"", "
                Sql = Sql & "round((sum(""Contribucion Comercial"")+ sum(""Contrib. Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comercial"", "
                Sql = Sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comer + Aporte MP"", "
                Sql = Sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Rebate""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Global"" "
                Sql = Sql & " from tmp_proceso_convenio "
                Sql = Sql & " where ""Valorizado Precio"">0 and alternativo='NO'"
                If cmbMarcaPropiaFinal.SelectedIndex > 0 Then
                    Sql = Sql & " and mpropia='" + cmbMarcaPropiaFinal.SelectedValue.ToString + "' "
                End If
                Sql = Sql & "group by linea order by sum(""Valorizado Precio"") desc"
                Dim dtDatosMp As New DataTable
                dtDatosMp = bd.sqlSelect(Sql)

                Dim dtjuntar2 As New DataTable
                Sql = " select sum(""Valorizado Precio"") Valorizado,sum(""Valorizado Precio""-""Valorizado Anterior"") Diferencia,round((sum(""Valorizado Precio"")-sum(""Valorizado Analisis""))/ sum(""Valorizado Precio"")*100,2) ""MG. Lista"", "
                Sql = Sql & "round((sum(""Contribucion Comercial"")+ sum(""Contrib. Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comercial"", "
                Sql = Sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Inventario""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Comer + Aporte MP"", "
                Sql = Sql & " round((sum(""Valorizado Precio"")-sum(""Valorizado Rebate""))/ sum(""Valorizado Precio"")*100,2) ""Mg. Global"" "
                Sql = Sql & "from tmp_proceso_convenio "
                Sql = Sql & " where ""Valorizado Precio"">0 and alternativo='NO' "
                If cmbMarcaPropiaFinal.SelectedIndex > 0 Then
                    Sql = Sql & " and  mpropia='" + cmbMarcaPropiaFinal.SelectedValue.ToString + "' "
                End If
                dtjuntar2 = bd.sqlSelect(Sql)
                dtDatosMp.Merge(dtjuntar2)
                dgvMpFinal.DataSource = dtDatosMp

                dgvMpFinal.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
                dgvMpFinal.Columns("Valorizado").DefaultCellStyle.Format = "n0"
                dgvMpFinal.Columns("Valorizado").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgvMpFinal.Columns("Diferencia").DefaultCellStyle.Format = "n0"
                dgvMpFinal.Columns("Diferencia").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgvMpFinal.Columns("Mg. Lista").DefaultCellStyle.Format = "n2"
                dgvMpFinal.Columns("Mg. Lista").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgvMpFinal.Columns("Mg. Comercial").DefaultCellStyle.Format = "n2"
                dgvMpFinal.Columns("Mg. Comercial").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgvMpFinal.Columns("Mg. Comer + Aporte MP").DefaultCellStyle.Format = "n2"
                dgvMpFinal.Columns("Mg. Comer + Aporte MP").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgvMpFinal.Columns("Mg. Global").DefaultCellStyle.Format = "n2"
                dgvMpFinal.Columns("Mg. Global").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

            Catch ex As Exception
                PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Finally
                bd.close()
            End Try
        End If
    End Sub

    Private Sub btnExcel_Click_1(sender As Object, e As EventArgs) Handles btnExcel.Click
        exportar()
        PuntoControl.RegistroUso("61")
    End Sub

    Private Sub txtNumcotFinal_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtValAnt.KeyPress, txtValAct.KeyPress, txtTotVentaFinal.KeyPress, txtTotTransFinal.KeyPress, txtTotRebateFinal.KeyPress, txtTotIngresiFinal.KeyPress, txtTotGastOpFinal.KeyPress, txtTotGastoFinal.KeyPress, txtTotContriFinal.KeyPress, txtRutCliFinal.KeyPress, txtRazonsFinal.KeyPress, txtPorVariacion.KeyPress, txtPorContriFinal.KeyPress, txtNumcotFinal.KeyPress, txtMgFinal.KeyPress
        e.Handled = True
    End Sub

    Private Sub btn_Limpiar_Click(sender As Object, e As EventArgs) Handles btn_Limpiar.Click
        btnExcel.Enabled = True
        btn_CambiaLinea.Enabled = True
        btn_Grabar.Enabled = True
        btn_Input.Enabled = True
        btn_Elimina.Enabled = True
        btn_Confirmar.Enabled = True
        btn_AgregaFecha.Enabled = True
        rutrelacionado = ""
        txtNumcot.Text = ""
        txtVariacionMargen.Text = "0"
        txtMargenActual.Text = "0"
        dgvDatosMP.DataSource = Nothing
        dgvMpFinal.DataSource = Nothing
        dgvResumenLínea.DataSource = Nothing
        dgvLineaFinal.DataSource = Nothing
        dgvDetalleLineaProductos.DataSource = Nothing
        txtRutcli.Text = ""
        txtRazons.Text = ""
        txtTotIngreso.Text = ""
        txtTotContr.Text = ""
        txtPorTotCon.Text = ""
        txtPorTransporte.Text = "0"
        txtTotTrans.Text = "0"
        txtPorVenta.Text = "0"
        txtPorGastoOp.Text = "0"
        txtTotGasto.Text = "0"
        txtPorRebate.Text = "0"
        txtTotRebate.Text = "0"
        txtTotMargenFinal.Text = "0"
        txtFechaIni.Text = ""
        txtFechaFin.Text = ""
        txtCodpro.Text = ""
        txtDescripcion.Text = ""
        txtPorVariacion.Text = ""
        txtPrecioNuevo.Text = ""
        txtPrecio.Text = ""
        txtCostoEspecial.Text = ""
        txtNumcotFinal.Text = ""
        txtRutCliFinal.Text = ""
        txtRazonsFinal.Text = ""
        txtTotIngresiFinal.Text = ""
        txtPorContriFinal.Text = ""
        txtTotContriFinal.Text = "0"
        txtPorTransFinal.Text = "0"
        txtTotTransFinal.Text = "0"
        txtPorVentaFinal.Text = "0"
        txtTotVentaFinal.Text = "0"
        txtPorGastoOpFinal.Text = "0"
        txtTotGastOpFinal.Text = "0"
        txtPorRebateFinal.Text = "0"
        txtTotRebateFinal.Text = "0"
        txtTotGastoFinal.Text = "0"
        txtMgFinal.Text = "0"
        txtValAct.Text = ""
        txtValAnt.Text = ""
        txtPorVariacion.Text = ""
        txtingAnt.Text = ""
        txtingnew.Text = ""
        txtingdif.Text = ""
        txttranant.Text = ""
        txttrandif.Text = ""
        txtvenant.Text = ""
        txtvennew.Text = ""
        txtventdif.Text = ""
        txtcontant.Text = ""
        txtcontnew.Text = ""
        txtcontdif.Text = ""
        txtgasant.Text = ""
        txtgasnew.Text = ""
        txtgasdif.Text = ""
        txtrebant.Text = ""
        txtrebnew.Text = ""
        txtrebdif.Text = ""
        txtsumaant.Text = ""
        txtsumanew.Text = ""
        txtsumDif.Text = ""
        txtmgant.Text = ""
        txtmgnew.Text = ""
        txtmgdif.Text = ""
        txtobserv.Text = ""
        grbBotones.Enabled = False

        txtCC1.Text = ""
        txtCC2.Text = ""
        txtCC3.Text = ""
        txtMail1.Text = ""
        txtMail2.Text = ""
        txtMail3.Text = ""
        txtInfoExcel.Text = ""

        dgvEmpRelacion.DataSource = Nothing

        txtUltimoContrato.Text = ""
        txtContratoDesde.Text = ""
        txtContratoHasta.Text = ""
    End Sub

    Private Sub dgvResumenLínea_CurrentCellChanged(sender As Object, e As EventArgs) Handles dgvResumenLínea.CurrentCellChanged
        If flaggrilla = True And Not dgvResumenLínea.CurrentRow Is Nothing Then
            txtLinea.Text = dgvResumenLínea.Item("Linea", dgvResumenLínea.CurrentRow.Index).Value.ToString
            txtMargenActual.Text = dgvResumenLínea.Item("Mg. Lista", dgvResumenLínea.CurrentRow.Index).Value.ToString
        End If
    End Sub

    Private Sub txtLinea_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMargenActual.KeyPress, txtLinea.KeyPress
        e.Handled = True
    End Sub

    Private Sub txtVariacionMargen_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtVariacionMargen.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then

            btnCambiaLinea.Focus()
            formatear()

        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub btn_CambiaLinea_Click(sender As Object, e As EventArgs) Handles btnCambiaLinea.Click
        If Trim(txtLinea.Text) = "" Then
            MessageBox.Show("No debe seleccionar la fila de los totales, para reajustar, solo puede ser una línea", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        Using conn = New OracleConnection(Conexion.retornaConexion)
            conn.Open()
            Dim comando As OracleCommand
            Using transaccion = conn.BeginTransaction
                Try
                    PuntoControl.RegistroUso("58")
                    ''hacer el calculo del precio y afinar esos detalles, con eso estaria listo....
                    Dim Sql = " update tmp_proceso_convenio "
                    Sql = Sql & "set precio = round(precio * " + (1 + (txtVariacionMargen.Text) / 100).ToString.Replace(",", ".") + ") "
                    Sql = Sql & "where Linea='" + txtLinea.Text + "' and alternativo='NO'"

                    comando = New OracleCommand(Sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    'COLOCA PRECIO DE PRODUCTO ORIGINAL A LOS PRODUCTOS ALTERNATIVOS
                    Sql = " Declare           "
                    Sql = Sql & " Cursor C2 is (  "
                    Sql = Sql & " select codpro,precio from tmp_proceso_convenio where codpro in(select original from tmp_proceso_convenio where alternativo='SI') and precio>0 "
                    Sql = Sql & " );   "
                    Sql = Sql & " Begin  "
                    Sql = Sql & "     for D in C2 loop  "
                    Sql = Sql & "            update tmp_proceso_convenio "
                    Sql = Sql & "            set precio=d.precio, "
                    Sql = Sql & "            ""Mg. Comercial"" =  round(((d.precio-""Costo Analisis"")/d.precio)*100,2), "
                    Sql = Sql & "            ""Mg. Inventario"" =  round(((d.precio-""Costo Promedio"")/d.precio)*100,2), "
                    Sql = Sql & "            ""Mg. Rebate"" = round(((d.precio-""Costo Rebate"")/d.precio)*100,2) "
                    Sql = Sql & "            where original=d.codpro;     "
                    Sql = Sql & "         end loop;  "
                    Sql = Sql & " End;   "

                    comando = New OracleCommand(Sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    Sql = " update tmp_proceso_convenio  "
                    Sql = Sql & "set precio = round(precio * (1-(nvl(ahorro,0)/100)),0) "
                    Sql = Sql & "where alternativo='SI' "

                    comando = New OracleCommand(Sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    Sql = " update tmp_proceso_convenio set  "
                    Sql = Sql & " ""Mg. Comercial"" =  round(((precio-""Costo Analisis"")/precio)*100,2), "
                    Sql = Sql & " ""Mg. Inventario"" =  round(((precio-""Costo Promedio"")/precio)*100,2), "
                    Sql = Sql & " ""Mg. Rebate"" = round(((precio-""Costo Rebate"")/precio)*100,2) "
                    Sql = Sql & "  where estado in('ACTIVO DIMERC','AGOTADO EN MERCADO','ARTICULO NUEVO','EXCLUSIVO CONVENIO') "
                    comando = New OracleCommand(Sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    Sql = " update tmp_proceso_convenio set  "
                    Sql = Sql & """Valorizado Precio""=round(Precio * cantidad,0), "
                    Sql = Sql & " variacion = round(((Precio- ""Precio Cotizacion"") / ""Precio Cotizacion"") * 100,2), "
                    Sql = Sql & """Valorizado Analisis""=round(""Costo Analisis"" * cantidad,0), "
                    Sql = Sql & """Valorizado Inventario""=round(""Costo Promedio"" * cantidad,0), "
                    Sql = Sql & """Valorizado Rebate""=round(""Costo Rebate"" * cantidad,0) "
                    Sql = Sql & "  where estado in('ACTIVO DIMERC','AGOTADO EN MERCADO','ARTICULO NUEVO','EXCLUSIVO CONVENIO') "
                    comando = New OracleCommand(Sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    Sql = " update tmp_proceso_convenio set ""Contribucion Comercial"" = round(""Valorizado Precio""-""Valorizado Analisis"",0), "
                    Sql = Sql & """Contrib. Inventario"" =round(decode(mpropia,'IMPORTADO',0,DECODE(greatest(""Costo Analisis"",""Costo Promedio""),""Costo Analisis"",""Valorizado Analisis""-""Valorizado Inventario"",0))), "
                    Sql = Sql & " ""Contrib. Rebate""= round(decode(mpropia,'IMPORTADO',0,decode(aporte,0,0,""Valorizado Inventario""-""Valorizado Rebate"")),0), "
                    Sql = Sql & """Contrib Rebate Mp"" = round(decode(mpropia,'IMPORTADO',""Valorizado Analisis""-""Valorizado Inventario"" ,0),0) "
                    Sql = Sql & "  where estado in('ACTIVO DIMERC','AGOTADO EN MERCADO','ARTICULO NUEVO','EXCLUSIVO CONVENIO') "
                    comando = New OracleCommand(Sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    transaccion.Commit()

                    flaggrilla = False
                    Sql = "select * from tmp_proceso_convenio"
                    dgvDetalleLineaProductos.DataSource = bd.sqlSelect(Sql)
                    formatogrilla()
                    flaggrilla = True

                    txtVariacionMargen.Text = "0"

                    recalculaEstadoResultado()

                    MessageBox.Show("Cambio de Línea hecho con Éxito", "Finalizado", MessageBoxButtons.OK, MessageBoxIcon.Information)

                Catch ex As Exception
                    If Not transaccion.Equals(Nothing) Then
                        transaccion.Rollback()
                    End If
                    PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                    MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Finally
                    conn.Close()
                End Try
            End Using
        End Using
    End Sub

    Private Sub dgvDetalleLineaProductos_DataBindingComplete(sender As Object, e As DataGridViewBindingCompleteEventArgs) Handles dgvDetalleLineaProductos.DataBindingComplete
        coloreaGrilla()
    End Sub

    Public Sub formatear()
        'transforma los enteror en numeros con 2 decimales
        txtTotContr.Text = Format(CDec(txtTotContr.Text), "N0")
        txtTotContriFinal.Text = Format(CDec(txtTotContriFinal.Text), "N0")
        txtPorTransporte.Text = Format(CDec(txtPorTransporte.Text), "N2")
        txtPorVenta.Text = Format(CDec(txtPorVenta.Text), "N2")
        txtPorGastoOp.Text = Format(CDec(txtPorGastoOp.Text), "N2")
        txtPorRebate.Text = Format(CDec(txtPorRebate.Text), "N2")
        txtVariacion.Text = Format(CDec(txtVariacion.Text), "N2")
        txtPorTransFinal.Text = Format(CDec(txtPorTransFinal.Text), "N2")
        txtPorVentaFinal.Text = Format(CDec(txtPorVentaFinal.Text), "N2")
        txtPorGastoOpFinal.Text = Format(CDec(txtPorGastoOpFinal.Text), "N2")
        txtPorRebateFinal.Text = Format(CDec(txtPorRebateFinal.Text), "N2")
        'txtMargenActual.Text = Format(CDec(txtMargenActual.Text), "N2")
        'txtVariacionMargen.Text = Format(CDec(txtVariacionMargen.Text), "N2")
    End Sub
    Private Sub txtmgant_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtventdif.KeyPress, txtvennew.KeyPress, txtvenant.KeyPress, txttrannew.KeyPress, txttrandif.KeyPress, txttranant.KeyPress, txtsumDif.KeyPress, txtsumanew.KeyPress, txtsumaant.KeyPress, txtrebnew.KeyPress, txtrebdif.KeyPress, txtrebant.KeyPress, txtmgnew.KeyPress, txtmgdif.KeyPress, txtmgant.KeyPress, txtingnew.KeyPress, txtingdif.KeyPress, txtingAnt.KeyPress, txtgasnew.KeyPress, txtgasdif.KeyPress, txtgasant.KeyPress, txtcontnew.KeyPress, txtcontdif.KeyPress, txtcontant.KeyPress
        e.Handled = True
    End Sub

    Private Function validaProductosDuplicados() As Boolean
        Try
            bd.open_dimerc()
            Dim Sql = " select codpro,count(*)  "
            Sql = Sql & "   from tmp_proceso_convenio "
            Sql = Sql & "   group by codpro "
            Sql = Sql & "   having count(*)>1 "
            Dim dt = bd.sqlSelect(Sql)
            If dt.Rows.Count > 0 Then
                MessageBox.Show("El producto " + dt.Rows(0).Item("codpro").ToString + " esta duplicado en el detalle, por favor elimine uno de esos registros antes de poder continuar", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        Finally
            bd.close()
        End Try
    End Function

    Private Sub btn_Grabar_Click(sender As Object, e As EventArgs) Handles btn_Grabar.Click
        cargaFechasLinea()
        PuntoControl.RegistroUso("63")
        If validaProductosDuplicados() = False Then
            Exit Sub
        End If
        For i = 0 To dgvLineasReajuste.RowCount - 1
            If dgvLineasReajuste.Item("Fecha", i).Value < Now.Date Then
                MessageBox.Show("Existen Lineas con fecha de vencimiento menor al dia de hoy, por favor corregir y luego continuar", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            End If
        Next
        For i = 0 To dgvLineasReajuste.Rows.Count - 1
            If dgvLineasReajuste.Item("Fecha", i).Value > dtpFecVenConvenio.Value.Date Then
                MessageBox.Show("Existe una linea con fecha de termino mayor a la fecha de termino del convenio, favor revisar antes de continuar", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            End If
        Next
        For i = 0 To dgvLineasReajuste.Rows.Count - 1
            If dgvLineasReajuste.Item("Fecha", i).Value < dtpFeciniConvenio.Value.Date Then
                MessageBox.Show("Existe una linea con fecha de termino anterior a la fecha de inicio del convenio, favor revisar antes de continuar", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            End If
        Next
        If dtpFecVenConvenio.Value.Date < dtpFeciniConvenio.Value.Date Then
            MessageBox.Show("Fecha de vencimiento de convenio no puede ser anterior a la fecha de inicio", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        If Trim(txtobserv.Text) = "" Then
            MessageBox.Show("Debe colocar una observación para poder grabar la propuesta", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        Else
            Using conn = New OracleConnection(Conexion.retornaConexion)
                conn.Open()
                Using transaccion = conn.BeginTransaction
                    Dim copia = ""
                    Try
                        Cursor.Current = Cursors.WaitCursor
                        Dim comando As OracleCommand
                        Dim dataAdapter As OracleDataAdapter
                        Dim dt As New DataTable
                        Dim Sql = ""

                        Sql = "Set Role Rol_Aplicacion"
                        comando = New OracleCommand(Sql, conn)
                        comando.Transaction = transaccion
                        comando.ExecuteNonQuery()

                        Sql = " select num_reajuste from en_reajuste_convenio a where num_reajuste= " + num_reajuste.ToString
                        Dim dtelimina As New DataTable
                        comando = New OracleCommand(Sql, conn)
                        comando.Transaction = transaccion
                        dataAdapter = New OracleDataAdapter(comando)
                        dataAdapter.Fill(dtelimina)
                        If dtelimina.Rows.Count > 0 Then
                            Sql = "delete from de_reajuste_convenio where num_reajuste=" + num_reajuste.ToString + " and codemp=3"
                            comando = New OracleCommand(Sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()
                            Sql = "delete from en_reajuste_convenio where num_reajuste=" + num_reajuste.ToString + " and codemp=3"
                            comando = New OracleCommand(Sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()
                        End If

                        Sql = " INSERT INTO EN_REAJUSTE_CONVENIO(CODEMP,NUM_REAJUSTE, NUMCOT,RUTCLI,FECINI,FECFIN,TOTING_OLD,PORC_CONT_OLD,TOT_CONT_OLD,PORC_TRANS_OLD,TOT_TRANS_OLD, "
                        Sql += "  PORC_VENTA_OLD,TOT_VENTA_OLD,PORC_GASTOS_OLD,TOT_GASTOS_OLD, PORC_REBATE_OLD,TOT_REBATE_OLD,SUMA_GASTOS_OLD,MG_FINAL_OLD,TOTING_NEW, "
                        Sql += "  PORC_CONT_NEW,TOT_CONT_NEW,PORC_TRANS_NEW,TOT_TRANS_NEW,PORC_VENTA_NEW,TOT_VENTA_NEW,PORC_GASTOS_NEW,TOT_GASTOS_NEW, PORC_REBATE_NEW, "
                        Sql += "  TOT_REBATE_NEW,SUMA_GASTOS_NEW,MG_FINAL_NEW,OBSERVACION,USR_CREAC,USR_MODIF,ESTADO,TIPOPROPUESTA,FECINI_CONVENIO,FECFIN_CONVENIO,TIPOCONSUMO) VALUES(3,"
                        Sql += num_reajuste.ToString + ","
                        Sql += txtNumcot.Text + ","
                        Sql += txtRutcli.Text + ","
                        Sql += "to_date('" + txtFechaIni.Text + "','dd/mm/yyyy'),"
                        Sql += "to_date('" + txtFechaFin.Text + "','dd/mm/yyyy'),"
                        Sql += Convert.ToDouble(txtingAnt.Text).ToString + ","
                        Sql += PercentToString(txtPorTotCon.Text) + ","
                        Sql += Convert.ToDouble(txtcontant.Text).ToString + ","
                        Sql += PercentToString(txtPorTransporte.Text) + ","
                        Sql += Convert.ToDouble(txttranant.Text).ToString + ","
                        Sql += PercentToString(txtPorVenta.Text) + ","
                        Sql += Convert.ToDouble(txtvenant.Text).ToString + ","
                        Sql += PercentToString(txtPorGastoOp.Text) + ","
                        Sql += Convert.ToDouble(txtgasant.Text).ToString + ","
                        Sql += PercentToString(txtPorRebate.Text) + ","
                        Sql += Convert.ToDouble(txtrebant.Text).ToString + ","
                        Sql += Convert.ToDouble(txtsumaant.Text).ToString + ","
                        Sql += PercentToString(txtTotMargenFinal.Text) + ","

                        Sql += Convert.ToDouble(txtTotIngresiFinal.Text).ToString + ","
                        Sql += PercentToString(txtPorContriFinal.Text) + ","
                        Sql += Convert.ToDouble(txtTotContriFinal.Text).ToString + ","
                        Sql += PercentToString(txtPorTransFinal.Text) + ","
                        Sql += Convert.ToDouble(txtTotTransFinal.Text).ToString + ","
                        Sql += PercentToString(txtPorVentaFinal.Text) + ","
                        Sql += Convert.ToDouble(txtTotVentaFinal.Text).ToString + ","
                        Sql += PercentToString(txtPorGastoOpFinal.Text) + ","
                        Sql += Convert.ToDouble(txtTotGastOpFinal.Text).ToString + ","
                        Sql += PercentToString(txtPorRebateFinal.Text) + ","
                        Sql += Convert.ToDouble(txtTotRebateFinal.Text).ToString + ","
                        Sql += Convert.ToDouble(txtTotGastoFinal.Text).ToString + ","
                        Sql += PercentToString(txtMgFinal.Text) + ","
                        Sql += "'" + txtobserv.Text + "',"
                        Sql += "'" + Globales.user + "',"
                        Sql += "'" + Globales.user + "',"
                        Sql += "1,1,"
                        Sql += "to_date('" + dtpFeciniConvenio.Value.Date + "','dd/mm/yyyy'),"
                        Sql += "to_date('" + dtpFecVenConvenio.Value.Date + "','dd/mm/yyyy'), "
                        Sql += "'" + cmbPeriodoConsumo.SelectedValue.ToString + "'"
                        Sql += ") "
                        comando = New OracleCommand(Sql, conn)
                        comando.Transaction = transaccion
                        comando.ExecuteNonQuery()

                        For i = 0 To dgvDetalleLineaProductos.RowCount - 1

                            Sql = "   INSERT INTO DE_REAJUSTE_CONVENIO(CODEMP,NUM_REAJUSTE, NUMCOT,CODPRO,DESPRO,MPROPIA,LINEA,MARCA,CANTIDAD,PRECIO,COSTOANALISIS, "
                            Sql = Sql & "  COSCOM,COSPROM,COSESP,COSESPPM,FECINI_COSESP,FECFIN_COSESP,COSREBATE,APORTE,PESO,MG_COMERCIAL, "
                            Sql = Sql & "  MG_INVENTARIO,MG_REBATE,VAL_PRECIO,VAL_ANALISIS,VAL_INVENTARIO,VAL_REBATE,CONT_COMERCIAL,CONT_INVENTARIO, "
                            Sql = Sql & "  CONT_REBATE,CONT_REBATE_MP,PRECOT,VAL_OLD,ESTADO,PEDSTO,VARIACION,USR_CREAC,USR_MODIF,COSREP,ULTCOM,ALTERNATIVO,ORIGINAL,CODLIN,FECINI) VALUES(3,"
                            Sql += num_reajuste.ToString + ","
                            Sql += txtNumcot.Text + ","
                            Sql += "'" + dgvDetalleLineaProductos.Item("Codpro", i).Value.ToString + "',"
                            Sql += "'" + dgvDetalleLineaProductos.Item("Despro", i).Value.ToString + "',"
                            Sql += "'" + dgvDetalleLineaProductos.Item("Mpropia", i).Value.ToString + "',"
                            Sql += "'" + dgvDetalleLineaProductos.Item("Linea", i).Value.ToString + "',"
                            Sql += "'" + dgvDetalleLineaProductos.Item("Marca", i).Value.ToString + "',"
                            Sql += dgvDetalleLineaProductos.Item("Cantidad", i).Value.ToString + ","
                            Sql += dgvDetalleLineaProductos.Item("Precio", i).Value.ToString + ","
                            Sql += dgvDetalleLineaProductos.Item("Costo Analisis", i).Value.ToString + ","
                            Sql += dgvDetalleLineaProductos.Item("Costo Comercial", i).Value.ToString + ","
                            Sql += dgvDetalleLineaProductos.Item("Costo Promedio", i).Value.ToString.Replace(",", ".") + ","
                            Sql += dgvDetalleLineaProductos.Item("Costo Especial", i).Value.ToString + ","
                            Sql += dgvDetalleLineaProductos.Item("Costo Especial PM", i).Value.ToString + ","
                            Sql += "to_date('" + dgvDetalleLineaProductos.Item("FecIni Costo Especial", i).Value + "','dd/mm/yyyy'),"
                            Sql += "to_date('" + dgvDetalleLineaProductos.Item("FecFin Costo Especial", i).Value + "','dd/mm/yyyy'),"
                            Sql += dgvDetalleLineaProductos.Item("Costo Rebate", i).Value.ToString.Replace(",", ".") + ","
                            Sql += dgvDetalleLineaProductos.Item("Aporte", i).Value.ToString.Replace(",", ".") + ","
                            Sql += dgvDetalleLineaProductos.Item("Peso", i).Value.ToString.Replace(",", ".") + ","
                            Sql += dgvDetalleLineaProductos.Item("Mg. Comercial", i).Value.ToString.Replace(",", ".") + ","
                            Sql += dgvDetalleLineaProductos.Item("Mg. Inventario", i).Value.ToString.Replace(",", ".") + ","
                            Sql += dgvDetalleLineaProductos.Item("Mg. Rebate", i).Value.ToString.Replace(",", ".") + ","
                            Sql += dgvDetalleLineaProductos.Item("Valorizado Precio", i).Value.ToString + ","
                            Sql += dgvDetalleLineaProductos.Item("Valorizado Analisis", i).Value.ToString + ","
                            Sql += dgvDetalleLineaProductos.Item("Valorizado Inventario", i).Value.ToString + ","
                            Sql += dgvDetalleLineaProductos.Item("Valorizado Rebate", i).Value.ToString + ","
                            Sql += dgvDetalleLineaProductos.Item("Contribucion Comercial", i).Value.ToString + ","
                            Sql += dgvDetalleLineaProductos.Item("Contrib. Inventario", i).Value.ToString + ","
                            Sql += dgvDetalleLineaProductos.Item("Contrib. Rebate", i).Value.ToString + ","
                            Sql += dgvDetalleLineaProductos.Item("Contrib Rebate Mp", i).Value.ToString + ","
                            Sql += dgvDetalleLineaProductos.Item("Precio Cotizacion", i).Value.ToString + ","
                            Sql += dgvDetalleLineaProductos.Item("Valorizado Anterior", i).Value.ToString + ","
                            Sql += "'" + dgvDetalleLineaProductos.Item("Estado", i).Value.ToString + "',"
                            Sql += "'" + dgvDetalleLineaProductos.Item("Pedsto", i).Value.ToString + "',"
                            Sql += dgvDetalleLineaProductos.Item("Variacion", i).Value.ToString.Replace(",", ".") + ","
                            Sql += "'" + Globales.user + "',"
                            Sql += "'" + Globales.user + "',"
                            Sql += dgvDetalleLineaProductos.Item("Cosrep", i).Value.ToString.Replace(",", ".") + ","
                            Sql += dgvDetalleLineaProductos.Item("Ultcom", i).Value.ToString.Replace(",", ".") + ","
                            Sql += "'" + dgvDetalleLineaProductos.Item("Alternativo", i).Value.ToString + "',"
                            If dgvDetalleLineaProductos.Item("Alternativo", i).Value = "SI" Then
                                Sql += "'" + dgvDetalleLineaProductos.Item("Original", i).Value.ToString + "',"
                            Else
                                Sql += "'" + dgvDetalleLineaProductos.Item("Codpro", i).Value.ToString + "',"
                            End If
                            Sql += dgvDetalleLineaProductos.Item("codlin", i).Value.ToString + ","
                            Sql += "to_date('" + dtpFeciniConvenio.Value.Date + "','dd/mm/yyyy') "
                            Sql += ") "
                            copia = Sql
                            comando = New OracleCommand(Sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()

                        Next

                        Sql = " select b.codpro,nvl(c.FECHA_REAJUSTE,'" + dtpFecVenConvenio.Value.Date + "')  fecven "
                        Sql = Sql & " from en_reajuste_convenio a,de_reajuste_convenio b,de_fechas_reajuste_convenio c  "
                        Sql = Sql & " where a.codemp=3   "
                        Sql = Sql & " and a.num_reajuste=b.num_reajuste  "
                        Sql = Sql & " and a.codemp=b.codemp  "
                        Sql = Sql & " and a.num_reajuste = " + num_reajuste.ToString
                        Sql = Sql & " and a.numcot = " + txtNumcot.Text
                        Sql = Sql & " and B.num_reajuste=c.num_reajuste(+)  "
                        Sql = Sql & " and b.codemp=c.codemp(+)  "
                        Sql = Sql & " and b.codlin=c.codlin(+) "
                        Dim dtFechas As New DataTable
                        comando = New OracleCommand(Sql, conn)
                        comando.Transaction = transaccion
                        dataAdapter = New OracleDataAdapter(comando)
                        dataAdapter.Fill(dtFechas)
                        For i = 0 To dtFechas.Rows.Count - 1
                            Sql = " update de_reajuste_convenio "
                            Sql += " set fecfin = to_date('" + dtFechas.Rows(i).Item("fecven") + "','dd/mm/yyyy') "
                            Sql += " where codemp=3"
                            Sql += " and num_reajuste = " + num_reajuste.ToString
                            Sql += " and numcot = " + txtNumcot.Text
                            Sql += " and codpro='" + dtFechas.Rows(i).Item("codpro").ToString + "'"
                            comando = New OracleCommand(Sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()
                        Next

                        'AGREGA RUT PARA PROCEDIMIENTO QUE CARGA EL CONVENIO EN ORACLE
                        Sql = "insert into re_rut_convenio (codemp,num_reajuste,rutcli) values("
                        Sql += "3,"
                        Sql += num_reajuste.ToString + ","
                        Sql += txtRutcli.Text
                        Sql += ")"
                        comando = New OracleCommand(Sql, conn)
                        comando.Transaction = transaccion
                        comando.ExecuteNonQuery()
                        'SI HAY RELACIONADOS MARCADOS, TAMBIEN SE AGREGAN
                        For i = 0 To dgvRelComercial.RowCount - 1
                            If dgvRelComercial.Item("Sel", i).Value = 1 Then
                                Sql = "insert into re_rut_convenio (codemp,num_reajuste,rutcli) values("
                                Sql += "3,"
                                Sql += num_reajuste.ToString + ","
                                Sql += dgvRelComercial.Item("Rutcli", i).Value.ToString
                                Sql += ")"
                                comando = New OracleCommand(Sql, conn)
                                comando.Transaction = transaccion
                                comando.ExecuteNonQuery()
                            End If
                        Next

                        transaccion.Commit()
                        Cursor.Current = Cursors.Arrow
                        MessageBox.Show("Carga de Datos Lista, ahora tiene mas opciones habilitadas en la pestaña 'Acciones' ")

                        grbBotones.Enabled = True

                    Catch ex As Exception
                        If Not transaccion.Equals(Nothing) Then
                            transaccion.Rollback()
                        End If
                        copia += ""
                        PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                        MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Finally
                        conn.Close()
                    End Try
                End Using
            End Using
        End If
    End Sub
    Private Function PercentToString(pc As String) As String
        Dim dbl As Double = 0.0
        If pc.Contains("%") Then pc = pc.Substring(0, pc.Length - 1)
        If Double.TryParse(pc, dbl) Then Return dbl.ToString.Replace(",", ".")
        Return 0.0
    End Function
    'Private Sub btn_CostoEspecial_Click(sender As Object, e As EventArgs) Handles btn_CostoEspecial.Click
    '    If MessageBox.Show("¿Esta seguro de cargar los costos especiales?", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
    '        If dtpCosEspIni.Value.Date > dtpCosEspFin.Value.Date Then
    '            MessageBox.Show("Fecha de Termino no puede ser menor a fecha de inicio", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
    '            Exit Sub
    '        End If
    '        cargacostosespeciales()
    '    End If
    'End Sub
    Private Sub btn_Correo_Click(sender As Object, e As EventArgs) Handles btn_Correo.Click
        PuntoControl.RegistroUso("64")
        Try
            If Not IO.File.Exists("C:\dimerc\propuesta " & txtRazons.Text & " cliente.xlsx") Then
                'IO.File.Delete("C:\dimerc\propuesta nueva " & txtRazons.Text & " cliente.xlsx")
                preparaexcelcorreoCliente()
            End If
            If Not IO.File.Exists("C:\dimerc\propuesta " & txtRazons.Text & " vendedor.xlsx") Then
                'IO.File.Delete("C:\dimerc\propuesta nueva " & txtRazons.Text & " vendedor.xlsx")
                preparaexcelcorreoVendedor()
            End If

            Dim cadenaCorreoPara = ""
            Dim cadenaCorreoCC = ""
            If txtMail1.Text = "" And txtMail2.Text = "" And txtMail3.Text = "" Then
                MessageBox.Show("No se enviara correo, no hay destinatarios ingresados", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            End If

            Dim sql = "select mail01, psw_mail from ma_usuario"
            sql += " where userid = '" + Globales.user + "'"
            Dim dt = bd.sqlSelect(sql)
            If dt.Rows.Count = 0 Then
                MessageBox.Show("No se encuentra correo de usuario, favor comunicarse con Sistemas.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            Else
                Dim correo = dt.Rows(0).Item("mail01").ToString
                Dim contraseña = dt.Rows(0).Item("psw_mail").ToString
                Dim Smtp_Server As New SmtpClient
                Dim e_mail As New MailMessage()
                Smtp_Server.UseDefaultCredentials = False
                Smtp_Server.Credentials = New Net.NetworkCredential(correo, contraseña)
                Smtp_Server.Host = "mail.dimerc.cl"

                e_mail = New MailMessage()
                e_mail.From = New MailAddress(correo)
                If txtMail1.Text <> "" Then
                    e_mail.To.Add(txtMail1.Text)
                End If
                If txtMail2.Text <> "" Then
                    e_mail.To.Add(txtMail2.Text)
                End If
                If txtMail3.Text <> "" Then
                    e_mail.To.Add(txtMail3.Text)
                End If

                If txtCC1.Text <> "" Then
                    e_mail.CC.Add(txtCC1.Text)
                End If
                If txtCC2.Text <> "" Then
                    e_mail.CC.Add(txtCC2.Text)
                End If
                If txtCC3.Text <> "" Then
                    e_mail.CC.Add(txtCC3.Text)
                End If

                e_mail.Subject = "Propuesta de Reajuste de Convenio " + txtRazons.Text
                e_mail.IsBodyHtml = False
                Dim mensaje As String = "Estimado ejecutivo : " + vbCrLf '"</br>"
                mensaje += "Le informo que a " + txtRazons.Text + " le corresponde un reajuste de su convenio con fecha " + Now.Date.ToShortDateString + "." + vbCrLf + vbCrLf '" </br></br>"
                mensaje += "Estamos enviando este correo con anticipación para que pueda informar a su cliente que con fecha " + dtpFeciniConvenio.Value.Date.ToShortDateString + " este reajuste será aplicado en forma automática en nuestros sistema por lo cual aparecerán los nuevos precios fijados." + vbCrLf + vbCrLf '"</br></br>"
                mensaje += "Dentro del periodo de una semana usted puede solicitar corregir algún código producto para evitar errores en la carga." + vbCrLf + vbCrLf '"</br></br>"
                mensaje += "Quedamos atentos a sus comentarios," + vbCrLf + vbCrLf + vbCrLf '"</br></br></br> "
                mensaje += "Saluda atentamente," + vbCrLf + vbCrLf
                mensaje += "Equipo de Convenios y Licitaciones Dimerc."
                e_mail.Body = mensaje
                Dim archivoAdjunto As New System.Net.Mail.Attachment("C:\dimerc\propuesta " & txtRazons.Text & " cliente.xlsx")
                Dim archivoAdjunto2 As New System.Net.Mail.Attachment("C:\dimerc\propuesta " & txtRazons.Text & " vendedor.xlsx")
                e_mail.Attachments.Add(archivoAdjunto)
                e_mail.Attachments.Add(archivoAdjunto2)
                Smtp_Server.Send(e_mail)
            End If

            'Outlook = New Outlook.Application()
            'Mail.To = cadenaCorreoPara
            'Mail = Outlook.CreateItem(Microsoft.Office.Interop.Outlook.OlItemType.olMailItem)
            'Mail.CC = cadenaCorreoCC
            'Mail.Subject = "Propuesta de Reajuste de Convenio " + txtRazons.Text
            'Dim mensaje As String = "Estimado ejecutivo </br>"
            'mensaje += "Le informo que a " + txtRazons.Text + " le corresponde un reajuste de su convenio con fecha " + Now.Date.ToShortDateString + ". </br></br>"
            'mensaje += "Estamos enviando este correo con 30 días de anticipación para que pueda informar a su cliente que con fecha " + Now.Date.ToShortDateString + " este reajuste será aplicado en forma automática en nuestros sistema por lo cual aparecerán los nuevos precios fijados.</br></br>"
            'mensaje += "Dentro del periodo de los próximos 15 días usted puede solicitar corregir algún código producto para evitar errores en la carga.</br></br>"
            'mensaje += "Quedamos atentos a sus comentarios,</br></br></br> "
            'mensaje += "Saluda atentamente, </br></br>"
            'mensaje += "Equipo de Convenios y Licitaciones Dimerc."
            'Mail.Attachments.Add("C:\dimerc\propuesta " & txtRazons.Text & " cliente.xlsx")
            'Mail.Attachments.Add("C:\dimerc\propuesta " & txtRazons.Text & " vendedor.xlsx")
            'mensaje = "<body style=""font-family:Calibri;"">" + mensaje + "</body>"
            'Mail.HTMLBody = mensaje + Mail.HTMLBody
            ''System.Threading.Thread.Sleep(10000)
            'Mail.Send()

            MessageBox.Show("Correo enviado", "Finalizado", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub preparaexcelcorreoCliente()
        Try
            bd.open_dimerc()

            Dim SQL = " select b.tipoconsumo,b.fecini_convenio,a.codpro, a.despro, a.marca,a.precio,a.cantidad, a.precot,nvl(a.variacion,0) variacion,a.alternativo,a.original,a.estado  "
            SQL += " from de_reajuste_convenio a,en_reajuste_convenio b where a.codemp=3 "
            SQL += " And a.num_reajuste= " + num_reajuste.ToString
            SQL += " and a.numcot = " + txtNumcot.Text
            SQL += " and a.codemp=b.codemp"
            SQL += " and a.num_reajuste=b.num_reajuste"
            SQL += " and a.numcot=b.numcot"
            SQL += " order by a.original,a.alternativo "
            Dim dtvendedor = bd.sqlSelect(SQL)

            Dim car As New frmCargando
            car.ProgressBar1.Minimum = 0
            car.ProgressBar1.Value = 0
            car.ProgressBar1.Maximum = dtvendedor.Rows.Count
            car.StartPosition = FormStartPosition.CenterScreen
            car.Show()
            Cursor.Current = Cursors.WaitCursor

            Dim xlApp As Object
            Dim xlWorkBook As Object = Nothing
            Dim xlWorkSheet As Object

            Dim datestart As Date = Date.Now
            'xlApp = New Excel.ApplicationClass
            xlApp = CreateObject("Excel.Application")
            'xlApp.Visible = True

            If IO.File.Exists("C:\Program Files\Convenios_licitaciones\base para cliente.xlsx") Then
                xlWorkBook = xlApp.Workbooks.Open("C:\Program Files\Convenios_licitaciones\base para cliente.xlsx")
            End If
            'xlWorkBook = xlApp.Workbooks.Open("C:\test1.xlsx")
            xlWorkSheet = xlWorkBook.Worksheets("Propuesta Original Cliente")

            xlApp.ScreenUpdating = False
            'edit the cell with new value
            xlWorkSheet.Cells(12, 4) = txtRazons.Text
            xlWorkSheet.Cells(13, 4) = txtRutcli.Text
            xlWorkSheet.Cells(15, 2) = "Precios tienen " + txtInfoExcel.Text
            xlWorkSheet.Cells(23, 2) = "Estos precios comienzan a regir desde : " + dtvendedor.Rows(0).Item("fecini_convenio")
            xlWorkSheet.cells(28, 6) = "Consumo " + dtvendedor.Rows(0).Item("tipoconsumo").ToString
            xlWorkSheet.Range("B15").Font.Color = Excel.XlRgbColor.rgbDarkRed
            Dim valorizadoSinMP As Double = 0
            Dim valorizadoConMP As Double = 0
            For i = 0 To dtvendedor.Rows.Count - 1
                If Not dtvendedor.Rows(i).Item("Estado") = "ACTIVO DIMERC" And
                    Not dtvendedor.Rows(i).Item("Estado") = "ARTICULO NUEVO" And
                    Not dtvendedor.Rows(i).Item("Estado") = "EXCLUSIVO CONVENIO" And
                    Not dtvendedor.Rows(i).Item("Estado") = "AGOTADO EN MERCADO" Then
                    xlWorkSheet.Range("B" + (i + 29).ToString + "", "J" + (i + 29).ToString + "").Interior.Color = Excel.XlRgbColor.rgbNavajoWhite
                    xlWorkSheet.Cells(i + 29, 2) = (i + 1).ToString
                    xlWorkSheet.Cells(i + 29, 3) = dtvendedor.Rows(i).Item("Codpro").ToString
                    xlWorkSheet.Cells(i + 29, 4) = dtvendedor.Rows(i).Item("Despro").ToString
                    xlWorkSheet.Cells(i + 29, 5) = dtvendedor.Rows(i).Item("Marca").ToString
                    xlWorkSheet.Cells(i + 29, 6) = 0
                    xlWorkSheet.Cells(i + 29, 7) = 0
                    xlWorkSheet.Cells(i + 29, 8) = 0
                    xlWorkSheet.Cells(i + 29, 9) = 0
                    Continue For
                End If

                If dtvendedor.Rows(i).Item("Alternativo") = "SI" Then
                    xlWorkSheet.Range("B" + (i + 29).ToString + "", "J" + (i + 29).ToString + "").Interior.Color = Excel.XlRgbColor.rgbLightGrey
                    xlWorkSheet.Cells(i + 29, 2) = (i + 1).ToString
                    xlWorkSheet.Cells(i + 29, 3) = dtvendedor.Rows(i).Item("Codpro").ToString
                    xlWorkSheet.Cells(i + 29, 4) = dtvendedor.Rows(i).Item("Despro").ToString
                    xlWorkSheet.Cells(i + 29, 5) = dtvendedor.Rows(i).Item("Marca").ToString
                    xlWorkSheet.Cells(i + 29, 6) = dtvendedor.Rows(i - 1).Item("Cantidad").ToString
                    xlWorkSheet.Cells(i + 29, 7) = dtvendedor.Rows(i - 1).Item("Precio").ToString
                    xlWorkSheet.Cells(i + 29, 8) = dtvendedor.Rows(i).Item("Precio").ToString
                    xlWorkSheet.Cells(i + 29, 9) = (Math.Round((dtvendedor.Rows(i).Item("Precio") - dtvendedor.Rows(i - 1).Item("Precio").ToString) / dtvendedor.Rows(i - 1).Item("Precio").ToString * 100, 2)) / 100
                    xlWorkSheet.Cells(i + 29, 10) = dtvendedor.Rows(i).Item("Original").ToString
                    valorizadoConMP = valorizadoConMP - dtvendedor.Rows(i - 1).Item("Precio").ToString * dtvendedor.Rows(i - 1).Item("Cantidad").ToString
                    valorizadoConMP += dtvendedor.Rows(i - 1).Item("Cantidad").ToString * dtvendedor.Rows(i).Item("Precio").ToString
                Else
                    xlWorkSheet.Cells(i + 29, 2) = (i + 1).ToString
                    xlWorkSheet.Cells(i + 29, 3) = dtvendedor.Rows(i).Item("Codpro").ToString
                    xlWorkSheet.Cells(i + 29, 4) = dtvendedor.Rows(i).Item("Despro").ToString
                    xlWorkSheet.Cells(i + 29, 5) = dtvendedor.Rows(i).Item("Marca").ToString
                    xlWorkSheet.Cells(i + 29, 6) = dtvendedor.Rows(i).Item("Cantidad").ToString
                    xlWorkSheet.Cells(i + 29, 7) = dtvendedor.Rows(i).Item("Precot").ToString
                    xlWorkSheet.Cells(i + 29, 8) = dtvendedor.Rows(i).Item("Precio").ToString
                    xlWorkSheet.Cells(i + 29, 9) = (Math.Round((dtvendedor.Rows(i).Item("Precio") - dtvendedor.Rows(i).Item("Precot")) / dtvendedor.Rows(i).Item("Precot") * 100, 2)) / 100
                    valorizadoSinMP += dtvendedor.Rows(i).Item("Precio").ToString * dtvendedor.Rows(i).Item("Cantidad").ToString
                    valorizadoConMP += dtvendedor.Rows(i).Item("Precio").ToString * dtvendedor.Rows(i).Item("Cantidad").ToString
                End If

                car.ProgressBar1.Value += 1
                Marshal.ReleaseComObject(xlWorkSheet.Cells)
            Next

            SQL = " select toting_old viejo,toting_new nuevo from en_reajuste_convenio where codemp = 3 "
            SQL += " And num_reajuste= " + num_reajuste.ToString
            SQL += " And numcot = " + txtNumcot.Text
            Dim dtcabeza = bd.sqlSelect(SQL)
            If dtcabeza.Rows.Count > 0 Then
                xlWorkSheet.Cells(22, 7) = dtcabeza.Rows(0).Item("viejo").ToString
                xlWorkSheet.Cells(22, 8) = valorizadoSinMP
                xlWorkSheet.Cells(26, 7) = dtcabeza.Rows(0).Item("viejo").ToString
                xlWorkSheet.Cells(26, 8) = valorizadoConMP
            End If

            xlApp.ScreenUpdating = True

            car.Close()
            xlApp.DisplayAlerts = False
            'xlWorkBook.Close(SaveChanges:=True)

            xlWorkBook.SaveAs("C:\dimerc\propuesta " & txtRazons.Text & " cliente.xlsx")
            xlWorkBook.Close()

            xlApp.DisplayAlerts = True
            xlApp.Quit()

            'xlWorkSheet.ReleaseComObject(pt)
            Marshal.ReleaseComObject(xlApp)
            Marshal.ReleaseComObject(xlWorkBook)
            Marshal.ReleaseComObject(xlWorkSheet)
            Dim dateEnd As Date = Date.Now
            End_Excel_App(datestart, dateEnd)

            Cursor.Current = Cursors.Arrow

        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub
    Private Sub End_Excel_App(datestart As Date, dateEnd As Date)
        Dim xlp() As Process = Process.GetProcessesByName("EXCEL")
        For Each Process As Process In xlp
            If Process.StartTime >= datestart And Process.StartTime <= dateEnd Then
                Process.Kill()
                Exit For
            End If
        Next
    End Sub
    Private Sub ExcelProcessKill()
        Dim oProcesses() As Process
        Dim bFound As Boolean

        Try
            'Get all currently running process Ids for Excel applications
            oProcesses = Process.GetProcessesByName("Excel")
            mExcelProcesses = Process.GetProcessesByName("Excel")
            If oProcesses.Length > 0 Then
                For i As Integer = 0 To oProcesses.Length - 1
                    bFound = False

                    For j As Integer = 0 To mExcelProcesses.Length - 1
                        If oProcesses(i).Id = mExcelProcesses(j).Id Then
                            bFound = True
                            Exit For
                        End If
                    Next

                    If bFound Then
                        oProcesses(i).Kill()
                    End If
                Next
            End If
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub preparaexcelcorreoVendedor()
        Try
            bd.open_dimerc()

            Dim SQL = " select b.fecini_convenio,a.codpro, a.despro, a.marca, a.linea,a.precot,a.precio,a.mg_comercial,"
            SQL += " a.cantidad,"
            SQL += " a.alternativo,a.original,a.estado  "
            SQL += " from de_reajuste_convenio a,en_reajuste_convenio b  where a.codemp=3 "
            SQL += " And a.num_reajuste= " + num_reajuste.ToString
            SQL += " And a.numcot = " + txtNumcot.Text
            SQL += " and a.codemp=b.codemp"
            SQL += " and a.num_reajuste=b.num_reajuste"
            SQL += " and a.numcot=b.numcot"
            SQL += " order by a.original,a.alternativo "
            Dim dtvendedor = bd.sqlSelect(SQL)

            Dim car As New frmCargando
            car.ProgressBar1.Minimum = 0
            car.ProgressBar1.Value = 0
            car.ProgressBar1.Maximum = dtvendedor.Rows.Count
            car.StartPosition = FormStartPosition.CenterScreen
            car.Show()
            Cursor.Current = Cursors.WaitCursor

            Dim xlApp As Object
            Dim xlWorkBook As Object = Nothing
            Dim xlWorkSheet As Object

            Dim datestart As Date = Date.Now
            'xlApp = New Excel.ApplicationClass
            xlApp = CreateObject("Excel.Application")
            'xlApp.Visible = True

            If IO.File.Exists("C:\Program Files\Convenios_licitaciones\base para vendedor.xlsx") Then xlWorkBook = xlApp.Workbooks.Open("C:\Program Files\Convenios_licitaciones\base para vendedor.xlsx")
            'xlWorkBook = xlApp.Workbooks.Open("C:\test1.xlsx")
            xlWorkSheet = xlWorkBook.Worksheets("Propuesta Original Ejecutivo")

            xlApp.ScreenUpdating = False
            'edit the cell with new value
            xlWorkSheet.Cells(12, 4) = txtRazons.Text
            xlWorkSheet.Cells(13, 4) = txtRutcli.Text
            xlWorkSheet.Cells(15, 2) = "Precios tienen " + txtInfoExcel.Text
            xlWorkSheet.Cells(23, 2) = "Estos precios comienzan a regir desde : " + dtvendedor.Rows(0).Item("fecini_convenio")
            xlWorkSheet.Range("B15").Font.Color = Excel.XlRgbColor.rgbDarkRed

            Dim valorizadoSinMP As Double = 0
            Dim valorizadoConMP As Double = 0

            For i = 0 To dtvendedor.Rows.Count - 1
                If Not dtvendedor.Rows(i).Item("Estado") = "ACTIVO DIMERC" And
                    Not dtvendedor.Rows(i).Item("Estado") = "ARTICULO NUEVO" And
                    Not dtvendedor.Rows(i).Item("Estado") = "EXCLUSIVO CONVENIO" And
                    Not dtvendedor.Rows(i).Item("Estado") = "AGOTADO EN MERCADO" Then
                    xlWorkSheet.Range("B" + (i + 29).ToString + "", "J" + (i + 29).ToString + "").Interior.Color = Excel.XlRgbColor.rgbNavajoWhite
                    xlWorkSheet.Cells(i + 29, 2) = (i + 1).ToString
                    xlWorkSheet.Cells(i + 29, 3) = dtvendedor.Rows(i).Item("Codpro").ToString
                    xlWorkSheet.Cells(i + 29, 4) = dtvendedor.Rows(i).Item("Despro").ToString
                    xlWorkSheet.Cells(i + 29, 5) = dtvendedor.Rows(i).Item("Marca").ToString
                    xlWorkSheet.Cells(i + 29, 6) = 0
                    xlWorkSheet.Cells(i + 29, 7) = 0
                    xlWorkSheet.Cells(i + 29, 8) = 0
                    xlWorkSheet.Cells(i + 29, 9) = 0
                    Continue For
                End If

                If dtvendedor.Rows(i).Item("Alternativo") = "SI" Then
                    xlWorkSheet.Range("B" + (i + 29).ToString + "", "J" + (i + 29).ToString + "").Interior.Color = Excel.XlRgbColor.rgbLightGrey
                    xlWorkSheet.Cells(i + 29, 2) = (i + 1).ToString
                    xlWorkSheet.Cells(i + 29, 3) = dtvendedor.Rows(i).Item("Codpro").ToString
                    xlWorkSheet.Cells(i + 29, 4) = dtvendedor.Rows(i).Item("Despro").ToString
                    xlWorkSheet.Cells(i + 29, 5) = dtvendedor.Rows(i).Item("Marca").ToString
                    xlWorkSheet.Cells(i + 29, 6) = dtvendedor.Rows(i - 1).Item("Precio").ToString
                    xlWorkSheet.Cells(i + 29, 7) = dtvendedor.Rows(i).Item("Precio").ToString
                    xlWorkSheet.Cells(i + 29, 8) = dtvendedor.Rows(i).Item("Mg_Comercial") / 100
                    xlWorkSheet.Cells(i + 29, 9) = (Math.Round((dtvendedor.Rows(i).Item("Precio") - dtvendedor.Rows(i - 1).Item("Precio").ToString) / dtvendedor.Rows(i - 1).Item("Precio").ToString * 100, 2)) / 100
                    xlWorkSheet.Cells(i + 29, 10) = dtvendedor.Rows(i).Item("Original").ToString
                    valorizadoConMP = valorizadoConMP - dtvendedor.Rows(i - 1).Item("Precio").ToString * dtvendedor.Rows(i - 1).Item("Cantidad").ToString
                    valorizadoConMP += dtvendedor.Rows(i - 1).Item("Cantidad").ToString * dtvendedor.Rows(i).Item("Precio").ToString
                Else
                    xlWorkSheet.Cells(i + 29, 2) = (i + 1).ToString
                    xlWorkSheet.Cells(i + 29, 3) = dtvendedor.Rows(i).Item("Codpro").ToString
                    xlWorkSheet.Cells(i + 29, 4) = dtvendedor.Rows(i).Item("Despro").ToString
                    xlWorkSheet.Cells(i + 29, 5) = dtvendedor.Rows(i).Item("Marca").ToString
                    xlWorkSheet.Cells(i + 29, 6) = dtvendedor.Rows(i).Item("Precot").ToString
                    xlWorkSheet.Cells(i + 29, 7) = dtvendedor.Rows(i).Item("Precio").ToString
                    xlWorkSheet.Cells(i + 29, 8) = dtvendedor.Rows(i).Item("Mg_Comercial") / 100
                    xlWorkSheet.Cells(i + 29, 9) = (Math.Round((dtvendedor.Rows(i).Item("Precio") - dtvendedor.Rows(i).Item("Precot")) / dtvendedor.Rows(i).Item("Precot") * 100, 2)) / 100
                    valorizadoSinMP += dtvendedor.Rows(i).Item("Precio").ToString * dtvendedor.Rows(i).Item("Cantidad").ToString
                    valorizadoConMP += dtvendedor.Rows(i).Item("Precio").ToString * dtvendedor.Rows(i).Item("Cantidad").ToString
                End If

                car.ProgressBar1.Value += 1

                Marshal.ReleaseComObject(xlWorkSheet.Cells)
            Next

            SQL = " select toting_old viejo,toting_new nuevo from en_reajuste_convenio where codemp = 3 "
            SQL += " And num_reajuste= " + num_reajuste.ToString
            SQL += " And numcot = " + txtNumcot.Text
            Dim dtcabeza = bd.sqlSelect(SQL)
            If dtcabeza.Rows.Count > 0 Then
                xlWorkSheet.Cells(22, 7) = dtcabeza.Rows(0).Item("viejo").ToString
                xlWorkSheet.Cells(22, 8) = valorizadoSinMP
                xlWorkSheet.Cells(26, 7) = dtcabeza.Rows(0).Item("viejo").ToString
                xlWorkSheet.Cells(26, 8) = valorizadoConMP
            End If

            xlWorkSheet = xlWorkBook.Worksheets("Resumen Linea")
            xlWorkSheet.Cells(12, 4) = txtRazons.Text
            xlWorkSheet.Cells(13, 4) = txtRutcli.Text
            xlWorkSheet.Cells(15, 2) = "Precios tienen " + txtInfoExcel.Text
            xlWorkSheet.Range("B15").Font.Color = Excel.XlRgbColor.rgbDarkRed
            SQL = " select linea, sum(val_precio) valorizado,  "
            SQL = SQL & " round((sum(Val_Precio)-sum(Val_Analisis))/ sum(Val_Precio) * 100,2) mg_lista from de_reajuste_convenio  "
            SQL = SQL & "where codemp=3 and precio>0  "
            SQL = SQL & "And num_reajuste= " + num_reajuste.ToString
            SQL = SQL & " And numcot = " + txtNumcot.Text
            SQL = SQL & "group by linea  "
            SQL = SQL & "union   "
            SQL = SQL & "select 'TOTAL' linea,sum(val_precio) valorizado,  "
            SQL = SQL & "round((sum(Val_Precio)-sum(Val_Analisis))/ sum(Val_Precio) * 100,2) mg_lista from de_reajuste_convenio  "
            SQL = SQL & "where codemp=3  "
            SQL = SQL & "and num_reajuste= " + num_reajuste.ToString
            SQL = SQL & " and numcot = " + txtNumcot.Text

            Dim dtTotales = bd.sqlSelect(SQL)

            'Aca se agregan las cabeceras de nuestro datagrid.

            'Aca se ingresa el detalle recorrera la tabla celda por celda
            For i = 0 To dtTotales.Rows.Count - 1
                If dtTotales.Rows.Count - 1 = i Then
                    xlWorkSheet.Cells(i + 24, 4).Font.FontStyle = "Bold"
                    xlWorkSheet.Range("D" + (i + 24).ToString + "", "F" + (i + 24).ToString + "").Interior.Color = Excel.XlRgbColor.rgbLightGrey
                    xlWorkSheet.Cells(i + 24, 5).Font.FontStyle = "Bold"
                    xlWorkSheet.Cells(i + 24, 6).Font.FontStyle = "Bold"
                End If
                xlWorkSheet.Cells(i + 24, 4) = dtTotales.Rows(i).Item("Linea")
                xlWorkSheet.Cells(i + 24, 5) = dtTotales.Rows(i).Item("Valorizado")
                xlWorkSheet.Cells(i + 24, 6) = dtTotales.Rows(i).Item("Mg_Lista") / 100
                indice += 1
                Marshal.ReleaseComObject(xlWorkSheet.Cells)
            Next

            xlWorkSheet.Activate()

            xlApp.ScreenUpdating = True

            car.Close()
            xlApp.DisplayAlerts = False
            'xlWorkBook.Close(SaveChanges:=True)

            xlWorkBook.SaveAs("C:\dimerc\propuesta " & txtRazons.Text & " vendedor.xlsx")
            xlWorkBook.Close()

            xlApp.DisplayAlerts = True
            xlApp.Quit()

            'xlWorkSheet.ReleaseComObject(pt)
            Marshal.ReleaseComObject(xlApp)
            Marshal.ReleaseComObject(xlWorkBook)
            Marshal.ReleaseComObject(xlWorkSheet)
            Dim dateEnd As Date = Date.Now
            End_Excel_App(datestart, dateEnd)
            Cursor.Current = Cursors.Arrow


        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub dgvEmpRelacion_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvEmpRelacion.CellContentClick
        Try
            If e.RowIndex <> -1 Then
                If e.ColumnIndex = 0 Then
                    If dgvEmpRelacion.Item("SEL", e.RowIndex).Value = 0 Then
                        dgvEmpRelacion.Item("SEL", e.RowIndex).Value = 1
                    Else
                        dgvEmpRelacion.Item("SEL", e.RowIndex).Value = 0
                    End If

                End If
            End If
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub


    Private Sub dgvRelComercial_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvRelComercial.CellContentClick
        Try
            If e.RowIndex <> -1 Then
                If e.ColumnIndex = 0 Then
                    If dgvRelComercial.Item("SEL", e.RowIndex).Value = 0 Then
                        dgvRelComercial.Item("SEL", e.RowIndex).Value = 1
                    Else
                        dgvRelComercial.Item("SEL", e.RowIndex).Value = 0
                    End If
                End If
            End If
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub txtPrecioNuevo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPrecioNuevo.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            txtCostoEspecial.Focus()
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub dgvResumenLínea_KeyDown(sender As Object, e As KeyEventArgs) Handles dgvResumenLínea.KeyDown
        If Not dgvResumenLínea.CurrentRow Is Nothing Then
            If e.KeyValue = Keys.Enter Then
                txtVariacionMargen.Focus()
                indice = dgvResumenLínea.CurrentRow.Index
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub TextBox3_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtUltimoContrato.KeyPress, txtContratoHasta.KeyPress, txtContratoDesde.KeyPress
        e.Handled = True
    End Sub

    Private Sub chkMarcaTodoCosEsp_CheckedChanged(sender As Object, e As EventArgs) Handles chkMarcaTodoCosEsp.CheckedChanged
        If chkMarcaTodoCosEsp.Checked = True Then
            For i = 0 To dgvEmpRelacion.RowCount - 1
                dgvEmpRelacion.Item("SEL", i).Value = 1
            Next
        Else
            For i = 0 To dgvEmpRelacion.RowCount - 1
                dgvEmpRelacion.Item("SEL", i).Value = 0
            Next
        End If
    End Sub

    Private Sub chkMarcaTodoConvenio_CheckedChanged(sender As Object, e As EventArgs) Handles chkMarcaTodoConvenio.CheckedChanged
        If chkMarcaTodoConvenio.Checked = True Then
            For i = 0 To dgvRelComercial.RowCount - 1
                dgvRelComercial.Item("SEL", i).Value = 1
            Next
        Else
            For i = 0 To dgvRelComercial.RowCount - 1
                dgvRelComercial.Item("SEL", i).Value = 0
            Next
        End If
    End Sub

    Private Sub chkLineas_CheckedChanged(sender As Object, e As EventArgs) Handles chkLineas.CheckedChanged
        If chkLineas.Checked = True Then
            For i = 0 To dgvLineasReajuste.RowCount - 1
                dgvLineasReajuste.Item("SEL", i).Value = 1
            Next
        Else
            For i = 0 To dgvLineasReajuste.RowCount - 1
                dgvLineasReajuste.Item("SEL", i).Value = 0
            Next
        End If
    End Sub

    Private Sub btn_AgregaFecha_Click(sender As Object, e As EventArgs) Handles btn_AgregaFecha.Click
        Using conn = New OracleConnection(Conexion.retornaConexion)
            conn.Open()
            Dim comando As OracleCommand
            Dim dataAdapter As OracleDataAdapter
            Using transaccion = conn.BeginTransaction
                Try
                    PuntoControl.RegistroUso("62")
                    For i = 0 To dgvLineasReajuste.RowCount - 1
                        If dgvLineasReajuste.Item("SEL", i).Value = 1 Then
                            Dim dt As New DataTable
                            Dim sql = "select 1 from de_fechas_reajuste_convenio where codemp=3 "
                            sql += " and num_reajuste=" + num_reajuste.ToString
                            sql += " and codlin=" + dgvLineasReajuste.Item("Codigo", i).Value.ToString
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            dataAdapter = New OracleDataAdapter(comando)
                            dataAdapter.Fill(dt)
                            If dt.Rows.Count = 0 Then
                                sql = " insert into de_fechas_reajuste_convenio ( "
                                sql += " codemp,num_reajuste,rutcli,codlin,fecha_reajuste,linea) "
                                sql += " values(3,"
                                sql += num_reajuste.ToString + ","
                                sql += txtRutcli.Text + ","
                                sql += dgvLineasReajuste.Item("Codigo", i).Value.ToString + ","
                                sql += " to_date('" + dtpFechaLineaReajuste.Value.Date + "','dd/mm/yyyy'),"
                                sql += " getnombrelinea(" + dgvLineasReajuste.Item("Codigo", i).Value.ToString + ")"
                                sql += ") "
                                comando = New OracleCommand(sql, conn)
                                comando.Transaction = transaccion
                                comando.ExecuteNonQuery()
                            Else
                                sql = "update de_fechas_reajuste_convenio set "
                                sql += " fecha_reajuste=to_date('" + dtpFechaLineaReajuste.Value.Date + "','dd/mm/yyyy') "
                                sql += "where codemp=3 and num_reajuste=" + num_reajuste.ToString
                                sql += " and codlin=" + dgvLineasReajuste.Item("Codigo", i).Value.ToString
                                comando = New OracleCommand(sql, conn)
                                comando.Transaction = transaccion
                                comando.ExecuteNonQuery()
                            End If
                        End If
                    Next

                    transaccion.Commit()

                    MessageBox.Show("Fechas agregadas con éxito", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    If dtpFechaLineaReajuste.Value.Date > dtpFecVenConvenio.Value.Date Then
                        dtpFecVenConvenio.Value = dtpFechaLineaReajuste.Value.Date
                    End If
                    cargaFechasLinea()
                Catch ex As Exception
                    If Not transaccion.Equals(Nothing) Then
                        transaccion.Rollback()
                    End If
                    PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                    MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Finally
                    conn.Close()
                End Try
            End Using
        End Using
    End Sub

    Private Sub dgvLineasReajuste_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvLineasReajuste.CellContentClick
        Try
            If e.RowIndex <> -1 Then
                If e.ColumnIndex = 0 Then
                    If dgvLineasReajuste.Item("SEL", e.RowIndex).Value = 0 Then
                        dgvLineasReajuste.Item("SEL", e.RowIndex).Value = 1
                    Else
                        dgvLineasReajuste.Item("SEL", e.RowIndex).Value = 0
                    End If
                End If
            End If
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub dgvDetalleLineaProductos_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvDetalleLineaProductos.CellContentClick
        Try
            If e.RowIndex <> -1 Then
                If e.ColumnIndex = 0 Then
                    If dgvDetalleLineaProductos.Item("¿Elimina?", e.RowIndex).Value = 0 Then
                        dgvDetalleLineaProductos.Item("¿Elimina?", e.RowIndex).Value = 1
                    Else
                        dgvDetalleLineaProductos.Item("¿Elimina?", e.RowIndex).Value = 0
                    End If
                End If
            End If
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub btn_Elimina_Click(sender As Object, e As EventArgs) Handles btn_Elimina.Click
        If MessageBox.Show("¿Esta seguro de eliminar los productos marcados del convenio?", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            Using conn = New OracleConnection(Conexion.retornaConexion)
                conn.Open()
                Dim comando As OracleCommand
                Dim sql As String
                Using transaccion = conn.BeginTransaction
                    Try
                        For i = 0 To dgvDetalleLineaProductos.RowCount - 1
                            If dgvDetalleLineaProductos.Item("¿Elimina?", i).Value = 1 Then
                                sql = "delete from tmp_proceso_convenio "
                                sql += "where codpro='" + dgvDetalleLineaProductos.Item("Codpro", i).Value + "'"
                                comando = New OracleCommand(sql, conn)
                                comando.Transaction = transaccion
                                comando.ExecuteNonQuery()
                            End If
                        Next
                        transaccion.Commit()
                        flaggrilla = False
                        sql = "select * from tmp_proceso_convenio"
                        dgvDetalleLineaProductos.DataSource = bd.sqlSelect(sql)
                        formatogrilla()
                        flaggrilla = True

                        recalculaEstadoResultado()

                        MessageBox.Show("Productos eliminados con éxito", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Catch ex As Exception
                        If Not transaccion.Equals(Nothing) Then
                            transaccion.Rollback()
                        End If
                        PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                        MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Finally
                        bd.close()
                    End Try
                End Using
            End Using
        End If

    End Sub

    Private Sub txtPorcentajeMinimo_KeyPress(sender As Object, e As KeyPressEventArgs)
        Globales.soloNumeros(sender, e)
    End Sub

    Private Sub btn_Input_Click(sender As Object, e As EventArgs) Handles btn_Input.Click
        Dim openFile As New OpenFileDialog
        If openFile.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            Globales.Importar(dgvInput, openFile.FileName)
            dgvInput.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            cargaInput()
            PuntoControl.RegistroUso("60")
        End If
    End Sub
    Private Sub cargaInput()
        Using conn = New OracleConnection(Conexion.retornaConexion)
            conn.Open()
            Using transaccion = conn.BeginTransaction
                Try
                    Dim comando As OracleCommand
                    Dim dt As New DataTable
                    Dim dataAdapter As OracleDataAdapter

                    Dim Sql = "Set Role Rol_Aplicacion"
                    comando = New OracleCommand(Sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    Sql = "delete from tmp_proceso_convenio"
                    comando = New OracleCommand(Sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    Dim car As New frmCargando

                    car.ProgressBar1.Minimum = 0
                    car.ProgressBar1.Value = 0
                    car.ProgressBar1.Maximum = dgvInput.RowCount
                    car.StartPosition = FormStartPosition.CenterScreen
                    car.Show()
                    Cursor.Current = Cursors.WaitCursor

                    For i = 0 To dgvInput.RowCount - 1

                        'If dgvInput.Item("Codpro", i).Value Is Nothing Or dgvInput.Item("Codpro", i).Value.ToString = "" Then
                        '    car.ProgressBar1.Value += 1
                        '    Continue For
                        'End If

                        Dim codpro = dgvInput.Item("Codpro", i).Value.ToString
                        Dim cantidad = dgvInput.Item("Cantidad", i).Value.ToString
                        Dim Precio = dgvInput.Item("Precio", i).Value.ToString
                        Dim CostoEspecial = dgvInput.Item("Costo Especial", i).Value.ToString
                        Dim aporte = Math.Round(dgvInput.Item("Aporte", i).Value.ToString * 100, 2).ToString.Replace(",", ".")
                        If CostoEspecial = "" Then
                            CostoEspecial = 0
                        End If
                        If Precio = "" Then
                            Precio = 0
                        End If
                        If aporte = "" Then
                            aporte = ""
                        End If
                        Sql = "insert into tmp_proceso_convenio select c.codpro,c.despro,nvl(e.tipo,'NO MP') mpropia,getlinea(c.codpro) linea,getmarca(c.codpro) marca,   "
                        Sql = Sql & " nvl(" + cantidad + ",1) cantidad ," + Precio + " precio,   "
                        If CostoEspecial > 0 Then
                            Sql = Sql & " " + CostoEspecial + " ""Costo Analisis"",   "
                        Else
                            Sql = Sql & " nvl(decode(f.costo,'',C.COSTO,f.costo),0) ""Costo Analisis"",   "
                        End If
                        'sql = sql & " nvl(decode(f.costo,'',C.COSTO,f.costo),0) ""Costo Comercial"",   "
                        Sql = Sql & " nvl(C.COSTO,0) ""Costo Comercial"",  "
                        Sql = Sql & " decode(nvl(decode(f.costo,'',C.cosprom,f.costo),0),0,c.costo,c.cosprom) ""Costo Promedio"",   "
                        Sql = Sql & " " + CostoEspecial + " ""Costo Especial"",   "
                        Sql = Sql & " nvl(f.costo,0) ""Costo Especial PM"",   "
                        Sql = Sql & " nvl(f.fecini,'') ""FecIni Costo Especial"",   "
                        Sql = Sql & " nvl(f.fecfin,'') ""FecFIn Costo Especial"",   "
                        Sql = Sql & " round(decode(nvl(decode(f.costo,'',C.cosprom,f.costo),0),0,c.costo,c.cosprom) * (1-(nvl(" + aporte + ",0)/100)),2) ""Costo Rebate"",   "
                        Sql = Sql & " nvl(" + aporte + ",0) aporte, 0 PESO,  "
                        Sql = Sql & " round(((" + Precio + "-decode(f.costo,'',C.COSTO,f.costo))/" + Precio + ")*100,2) ""Mg. Comercial"",   "
                        Sql = Sql & " round(((" + Precio + "-decode(nvl(decode(f.costo,'',C.cosprom,f.costo),0),0,c.costo,c.cosprom))/" + Precio + ")*100,2) ""Mg. Inventario"",   "
                        Sql = Sql & " round((1-(decode(nvl(decode(f.costo,'',C.cosprom,f.costo),0),0,c.costo,c.cosprom) * (1-(nvl(" + aporte + ",0)/100)))/" + Precio + ")*100,2) ""Mg. Rebate"",   "
                        Sql = Sql & " round(nvl(" + cantidad + ",1) * " + Precio + ",0) ""Valorizado Precio"",  "
                        Sql = Sql & " round(nvl(" + cantidad + ",1) * nvl(decode(f.costo,'',C.COSTO,f.costo),0),0) ""Valorizado Analisis"",   "
                        Sql = Sql & " round(nvl(" + cantidad + ",1) * decode(nvl(decode(f.costo,'',C.cosprom,f.costo),0),0,c.costo,c.cosprom),0) ""Valorizado Inventario"",   "
                        Sql = Sql & " round(nvl(nvl(" + cantidad + ",1) * round(decode(nvl(decode(f.costo,'',C.cosprom,f.costo),0),0,c.costo,c.cosprom) * (1-(nvl(" + aporte + ",0)/100)),2),0),0) ""Valorizado Rebate"",   "
                        Sql = Sql & " round((nvl(" + cantidad + ",1) * " + Precio + ")  - (nvl(" + cantidad + ",1) * nvl(decode(f.costo,'',C.COSTO,f.costo),0)),0) ""Contribucion Comercial"",   "
                        Sql = Sql & " round(decode(e.tipo,'IMPORTADO',0,DECODE(greatest(c.costo,c.cosprom),c.costo,(nvl(" + cantidad + ",1) * nvl(decode(f.costo,'',C.COSTO,f.costo),0))-nvl(" + cantidad + ",1) * decode(nvl(decode(f.costo,'',C.cosprom,f.costo),0),0,c.costo,c.cosprom),0)),0) ""Contrib. Inventario"",   "
                        Sql = Sql & " round(decode(e.tipo,'IMPORTADO',0,decode(nvl(" + aporte + ",0),0,0,decode(greatest(c.costo,c.cosprom),c.costo,(nvl(" + cantidad + ",1) * decode(nvl(decode(f.costo,'',C.cosprom,f.costo),0),0,c.costo,c.cosprom)) -(nvl(" + cantidad + ",1) * round(decode(nvl(decode(f.costo,'',C.cosprom,f.costo),0),0,c.costo,c.cosprom) * (1-(" + aporte + "/100)),2)),(nvl(" + cantidad + ",1) * nvl(decode(f.costo,'',C.COSTO,f.costo),0))- (nvl(" + cantidad + ",1) * round(decode(nvl(decode(f.costo,'',C.cosprom,f.costo),0),0,c.costo,c.cosprom) * (1-(" + aporte + "/100)),2))))),0) ""Contrib. Rebate"",   "
                        Sql = Sql & " round(decode(e.tipo,'IMPORTADO',DECODE(greatest(c.costo,c.cosprom),c.costo,(nvl(" + cantidad + ",1) * nvl(decode(f.costo,'',C.COSTO,f.costo),0))-nvl(" + cantidad + ",1) * decode(nvl(decode(f.costo,'',C.cosprom,f.costo),0),0,c.costo,c.cosprom),0),0),0) ""Contrib Rebate Mp"",   "
                        Sql = Sql & " decode(nvl(b.precio,0),0,GET_PRELIS(3,c.codpro," + txtRutcli.Text + "),b.precio) ""Precio Cotizacion"",   "
                        Sql = Sql & " round(nvl(" + cantidad + ",1) * decode(nvl(b.precio,0),0,GET_PRELIS(3,c.codpro," + txtRutcli.Text + "),b.precio),0) ""Valorizado Anterior"",   "
                        Sql = Sql & " getestadoprod(c.codpro) estado,   "
                        Sql = Sql & " decode(getpedsto(c.codpro),'S','STOCK','P','PEDIDO','VACIO') pedsto,   "
                        Sql = Sql & " 0 variacion,   "
                        Sql = Sql & " decode(GETCOSTOREPOSICION3(C.CODPRO),0,c.cosprom,GETCOSTOREPOSICION3(C.CODPRO)) ""Costo Reposición"", "
                        Sql = Sql & " nvl(GETULTIMACOMPRA('3',c.CODPRO,'V'),0) ultcom,  "
                        Sql = Sql & " 'NO' alternativo, '' original, 0 ahorro,getcodigolinea(c.codpro) codlin, "
                        Sql = Sql & " nvl(h.precio,0) ""Precio Min. Venta"", "
                        Sql = Sql & " nvl(i.precio,0) ""Precio Min. Coti"",nvl(c.cosprom,0) cosprom,GET_TIENEPRODUCTOALTERNATIVO(c.codpro) ""Estado Alternativo"" "
                        Sql = Sql & " from ma_product c,   "
                        Sql = Sql & " (select codpro,min(precio) precio from de_cotizac where numcot=" + txtNumcot.Text + " and codemp=3 group by codpro) b,"
                        Sql = Sql & " (select codpro,mpropia_hst,decode(greatest(nvl(sum(decode(tipo,'N',cantid*-1,cantid)),0),0),0,1,nvl(sum(decode(tipo,'N',cantid*-1,cantid)),0)) cantidad    "
                        Sql = Sql & " from qv_control_margen a   "
                        Sql = Sql & " where a.codemp = 3   "
                        Sql = Sql & " and a.rutcli in(" + rutrelacionado + ")  "
                        Sql = Sql & " and a.fecha   "
                        Sql = Sql & " between to_date('" + txtFechaIni.Text + "','dd/mm/yyyy')    "
                        Sql = Sql & " and to_date('" + txtFechaFin.Text + "','dd/mm/yyyy')   "
                        Sql = Sql & " and ((tipo = 'F' and internet in (0,1)) or (tipo = 'G' and internet in (0,2)) or (tipo = 'N' and internet in (1,2,7,9)))   "
                        Sql = Sql & " group by codpro,mpropia_hst ) d,   "
                        Sql = Sql & " (select codpro,tipo from qv_mpropia_dimerc_hst   "
                        Sql = Sql & " where (fechavig,codpro) in(   "
                        Sql = Sql & " select max(fechavig),codpro from qv_mpropia_dimerc_hst    "
                        Sql = Sql & " group by codpro)) e,   "
                        Sql = Sql & " (select codpro,costo,FECINI,FECFIN from en_cliente_costo a   "
                        Sql = Sql & " where codemp=3  "
                        Sql = Sql & " and rutcli= " + txtRutcli.Text + " "
                        Sql = Sql & " and codpro='" + codpro + "' "
                        Sql = Sql & " AND SYSDATE BETWEEN FECINI AND FECFIN       "
                        Sql = Sql & " and tipo=0 and estado='V') f,   "
                        Sql = Sql & " ma_prod_aporte g,   "
                        Sql = Sql & " (select codpro,min(precio) precio from  "
                        Sql = Sql & "   (select b.codpro,min(b.precio) precio from en_factura a,de_factura b where a.codemp=3    "
                        Sql = Sql & "   and a.rutcli in(" + rutrelacionado + ")   "
                        Sql = Sql & "   and b.codpro='" + codpro + "'"
                        Sql = Sql & "   and a.codemp=b.codemp   "
                        Sql = Sql & "   and a.numfac=b.numfac   "
                        Sql = Sql & "   and a.fecemi>= add_months(sysdate, -6)  group by b.codpro "
                        Sql = Sql & "   union "
                        Sql = Sql & "   select b.codpro,min(b.precio) precio from  en_factura a,de_guiades b,re_guiafac c where a.codemp=3    "
                        Sql = Sql & "   and a.rutcli in(" + rutrelacionado + ")   "
                        Sql = Sql & "   and b.codpro='" + codpro + "'"
                        Sql = Sql & "   and a.codemp=b.codemp   "
                        Sql = Sql & "   and a.numfac=c.numfac "
                        Sql = Sql & "   and b.numgui=c.numgui   "
                        Sql = Sql & "   and a.fecemi>= add_months(sysdate, -6)  group by b.codpro) "
                        Sql = Sql & "   group by codpro ) h,"
                        Sql = Sql & " (select b.codpro,min(b.precio) precio from en_cotizac a,de_cotizac b "
                        Sql = Sql & " where a.codemp=3  "
                        Sql = Sql & " and a.rutcli in(" + rutrelacionado + ") "
                        Sql = Sql & " and b.codpro='" + codpro + "' "
                        Sql = Sql & " and a.codemp=b.codemp "
                        Sql = Sql & " and a.numcot=b.numcot "
                        Sql = Sql & " and a.fecemi>= add_months(sysdate, -6) "
                        Sql = Sql & " group by b.codpro) i "
                        Sql = Sql & " where  "
                        Sql = Sql & " c.codpro='" + codpro + "' "
                        Sql = Sql & " and c.codpro=b.codpro(+)   "
                        Sql = Sql & " and c.codpro=d.codpro(+)   "
                        Sql = Sql & " and c.codpro=e.codpro(+)   "
                        Sql = Sql & " and c.codpro=f.codpro(+)   "
                        Sql = Sql & " and c.codpro=g.codpro(+) "
                        Sql = Sql & " and c.codpro=h.codpro(+) "
                        Sql = Sql & " and c.codpro=i.codpro(+) "
                        comando = New OracleCommand(Sql, conn)
                        comando.Transaction = transaccion
                        comando.ExecuteNonQuery()

                        car.ProgressBar1.Value += 1
                    Next

                    'DESPUES DE CARGAR LA TABLA CALCULO ESTADO RESULTADO ACTUAL, LUEGO HARE LAS CORRECCIONES AUTOMATICAS Y PRESNETARE
                    'EL ESTADO RESULTADO NUEVO

                    If Convert.ToDouble(txtTotIngreso.Text) > 0 Then
                        'CALCULA EL PESO
                        Sql = "  Declare            "
                        Sql = Sql & " Cursor C2 is (   "
                        Sql = Sql & " select codpro,SUM(CANTIDAD * PRECIO) MONTO,(SELECT SUM(CANTIDAD * PRECIO) FROM TMP_PROCESO_CONVENIO) TOTAL from tmp_proceso_convenio GROUP BY CODPRO  "
                        Sql = Sql & " );    "
                        Sql = Sql & " Begin   "
                        Sql = Sql & "     for D in C2 loop   "
                        Sql = Sql & "            update tmp_proceso_convenio  "
                        Sql = Sql & "            set PESO=ROUND((D.MONTO/D.TOTAL)*100,2)              "
                        Sql = Sql & "            where CODPRO=d.codpro;      "
                        Sql = Sql & "         end loop;   "
                        Sql = Sql & " End;    "

                        comando = New OracleCommand(Sql, conn)
                        comando.Transaction = transaccion
                        comando.ExecuteNonQuery()
                    End If

                    Sql = " update tmp_proceso_convenio set  "
                    Sql = Sql & " ""Mg. Comercial"" =  round(((precio-""Costo Analisis"")/precio)*100,2), "
                    Sql = Sql & " ""Mg. Inventario"" =  round(((precio-""Costo Promedio"")/precio)*100,2), "
                    Sql = Sql & " ""Mg. Rebate"" = round(((precio-""Costo Rebate"")/precio)*100,2) "
                    comando = New OracleCommand(Sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    Sql = " update tmp_proceso_convenio set  "
                    Sql = Sql & """Valorizado Analisis""=round(""Costo Analisis"" * cantidad,0), "
                    Sql = Sql & """Valorizado Precio""=round(PRECIO * cantidad,0), "
                    Sql = Sql & """Valorizado Rebate"" = round(""Costo Rebate""*cantidad,0), "
                    Sql = Sql & """Valorizado Inventario"" = round(""Costo Promedio""*cantidad,0) "
                    comando = New OracleCommand(Sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    Sql = " update tmp_proceso_convenio set  "
                    Sql = Sql & """Contribucion Comercial"" = round(""Valorizado Precio""-""Valorizado Analisis"",0), "
                    Sql = Sql & """Contrib. Inventario"" = round(decode(mpropia,'IMPORTADO',0,DECODE(greatest(""Costo Analisis"",""Costo Promedio""),""Costo Analisis"",""Valorizado Analisis""-""Valorizado Inventario"",0))), "
                    Sql = Sql & " ""Contrib. Rebate""= round(decode(mpropia,'IMPORTADO',0,decode(aporte,0,0,""Valorizado Inventario""-""Valorizado Rebate"")),0), "
                    Sql = Sql & """Contrib Rebate Mp"" = round(decode(mpropia,'IMPORTADO',""Valorizado Analisis""-""Valorizado Inventario"" ,0),0) "
                    comando = New OracleCommand(Sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    car.Close()
                    Cursor.Current = Cursors.Arrow
                    transaccion.Commit()

                    Sql = "select * from tmp_proceso_convenio order by alternativo"
                    Dim dtGrillaResumen As New DataTable
                    comando = New OracleCommand(Sql, conn)
                    comando.Transaction = transaccion
                    dataAdapter = New OracleDataAdapter(comando)
                    dataAdapter.Fill(dtGrillaResumen)
                    dgvDetalleLineaProductos.DataSource = dtGrillaResumen
                    dgvDetalleLineaProductos.Columns("ahorro").Visible = False
                    dgvDetalleLineaProductos.Columns("codlin").Visible = False
                    formatogrilla()
                    calculaEstadoFinal()
                    recalculaEstadoResultado()

                    MessageBox.Show("Input Cargado con éxito", "Finalizado", MessageBoxButtons.OK, MessageBoxIcon.Information)

                Catch ex As Exception
                    If Not transaccion.Equals(Nothing) Then
                        transaccion.Rollback()
                    End If
                    PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                    MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Finally
                    conn.Close()
                End Try
            End Using
        End Using
    End Sub

    Private Sub cmbPeriodoConsumo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbPeriodoConsumo.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            txtNumcot.Focus()
            txtNumcot.SelectAll()
        End If
    End Sub

    Private Sub dtpFecVenConvenio_ValueChanged(sender As Object, e As EventArgs) Handles dtpFecVenConvenio.ValueChanged
        cargaFechasLinea()
    End Sub

    Private Sub dtpFeciniConvenio_ValueChanged(sender As Object, e As EventArgs) Handles dtpFeciniConvenio.ValueChanged
        For i = 0 To dgvLineasReajuste.RowCount - 1
            If dgvLineasReajuste.Item("Fecha", i).Value < dtpFeciniConvenio.Value.Date Then
                dgvLineasReajuste.Item("Fecha", i).Value = dtpFeciniConvenio.Value.Date
            End If
        Next
    End Sub

    Private Sub cmbPeriodoConsumo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbPeriodoConsumo.SelectedIndexChanged
        If flag = True Then
            cmbConsumoFinal.SelectedIndex = cmbPeriodoConsumo.SelectedIndex
        End If

    End Sub

    Private Sub cmbConsumoFinal_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbConsumoFinal.KeyPress
        e.Handled = True
    End Sub

    Private Sub cargaFechasLinea()
        Try
            bd.open_dimerc()
            Dim sql = " select distinct a.codlin ""Codigo"",getnombrelinea(a.codlin) ""Linea"",nvl(b.fecha_reajuste,'" + Now.Date + "') ""Fecha""  "
            sql += " from tmp_proceso_convenio a left outer join de_fechas_reajuste_convenio b  "
            sql += " on b.codemp=3 and num_reajuste=" + num_reajuste.ToString
            sql += " and a.codlin=b.codlin "
            sql += " order by getnombrelinea(a.codlin) "
            dgvLineasReajuste.DataSource = Nothing
            dgvLineasReajuste.DataSource = bd.sqlSelect(sql)

            sql = " select max(nvl(b.fecha_reajuste,'" + dtpFecVenConvenio.Value.Date + "')) ""Fecha""     "
            sql += " from tmp_proceso_convenio a left outer join de_fechas_reajuste_convenio b  "
            sql += " on b.codemp=3 and num_reajuste=" + num_reajuste.ToString
            sql += " and a.codlin=b.codlin "
            Dim dtFinal = bd.sqlSelect(sql)
            If dtFinal.Rows.Count > 0 Then
                dtpFecVenConvenio.Value = dtFinal.Rows(0).Item("fecha")
            End If

        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub
End Class
'Private Sub Button2_Click(sender As Object, e As EventArgs)
'    Using conn = New OracleConnection(Conexion.retornaConexion)
'        conn.Open()
'        Using transaccion = conn.BeginTransaction
'            Try
'                Dim comando As OracleCommand
'                Dim dataAdapter As OracleDataAdapter
'                Dim dt As New DataTable
'                Dim Sql = "Set Role Rol_Aplicacion"
'                comando = New OracleCommand(Sql, conn)
'                comando.Transaction = transaccion
'                comando.ExecuteNonQuery()

'                Sql = "delete from cro_coti"
'                comando = New OracleCommand(Sql, conn)
'                comando.Transaction = transaccion
'                comando.ExecuteNonQuery()

'                Sql = "select numcot from en_conveni where codemp=3"
'                Sql += " and rutcli=" + txtRutcli.Text
'                Sql += " and cencos='" + cencos + "'"
'                Dim dtborrar As New DataTable
'                comando = New OracleCommand(Sql, conn)
'                comando.Transaction = transaccion
'                dataAdapter = New OracleDataAdapter(comando)
'                dataAdapter.Fill(dtborrar)
'                For i = 0 To dtborrar.Rows.Count - 1
'                    Sql = "update en_cotizac "
'                    Sql = Sql & " set fecven=trunc(sysdate-1)  "
'                    Sql = Sql & " where codemp= 3"
'                    Sql = Sql & " and numcot= " + dtborrar.Rows(i).Item("numcot").ToString
'                    comando = New OracleCommand(Sql, conn)
'                    comando.Transaction = transaccion
'                    comando.ExecuteNonQuery()

'                    Sql = "delete from de_conveni "
'                    Sql = Sql & " where codemp= 3 "
'                    Sql = Sql & " and numcot= " + dtborrar.Rows(i).Item("numcot").ToString

'                    comando = New OracleCommand(Sql, conn)
'                    comando.Transaction = transaccion
'                    comando.ExecuteNonQuery()

'                    Sql = "delete from en_conveni "
'                    Sql = Sql & " where codemp= 3 "
'                    Sql = Sql & " and numcot= " + dtborrar.Rows(i).Item("numcot").ToString

'                    comando = New OracleCommand(Sql, conn)
'                    comando.Transaction = transaccion
'                    comando.ExecuteNonQuery()
'                Next


'                For i = 0 To dgvRelComercial.RowCount - 1
'                    Sql = "select numcot from en_conveni where codemp=3"
'                    Sql += " and rutcli=" + dgvRelComercial.Item("rutcli", i).Value.ToString
'                    Sql += " and cencos=" + cencos
'                    dtborrar.Clear()
'                    comando = New OracleCommand(Sql, conn)
'                    comando.Transaction = transaccion
'                    dataAdapter = New OracleDataAdapter(comando)
'                    dataAdapter.Fill(dtborrar)
'                    For x = 0 To dtborrar.Rows.Count - 1
'                        Sql = "update en_cotizac "
'                        Sql = Sql & " set fecven=trunc(sysdate-1)  "
'                        Sql = Sql & " where codemp= 3"
'                        Sql = Sql & " and numcot= " + dtborrar.Rows(x).Item("numcot").ToString
'                        comando = New OracleCommand(Sql, conn)
'                        comando.Transaction = transaccion
'                        comando.ExecuteNonQuery()

'                        Sql = "delete from de_conveni "
'                        Sql = Sql & " where codemp= 3 "
'                        Sql = Sql & " and numcot= " + dtborrar.Rows(x).Item("numcot").ToString

'                        comando = New OracleCommand(Sql, conn)
'                        comando.Transaction = transaccion
'                        comando.ExecuteNonQuery()

'                        Sql = "delete from en_conveni "
'                        Sql = Sql & " where codemp= 3 "
'                        Sql = Sql & " and numcot= " + dtborrar.Rows(x).Item("numcot").ToString

'                        comando = New OracleCommand(Sql, conn)
'                        comando.Transaction = transaccion
'                        comando.ExecuteNonQuery()
'                    Next

'                Next
'                Sql = "select codlin,fecha_reajuste from de_fechas_reajuste_convenio where codemp=3"
'                Sql += " and num_reajuste = " + num_reajuste.ToString
'                Sql += " and codlin=99"
'                dt.Clear()
'                comando = New OracleCommand(Sql, conn)
'                comando.Transaction = transaccion
'                dataAdapter = New OracleDataAdapter(comando)
'                dataAdapter.Fill(dt)
'                Dim todos As Boolean = False
'                Dim fecha_reajusteTodos = ""
'                If dt.Rows.Count > 0 Then
'                    todos = True
'                    fecha_reajusteTodos = dt.Rows(0).Item("fecha_reajuste")
'                End If

'                Sql = " select a.rutcli,b.codpro,b.cantidad,b.precio,b.costoanalisis,nvl(c.FECHA_REAJUSTE,'" + dtpFecVenConvenio.Value.Date + "')  fecven "
'                Sql = Sql & " from en_reajuste_convenio a,de_reajuste_convenio b,de_fechas_reajuste_convenio c  "
'                Sql = Sql & " where a.codemp=3   "
'                Sql = Sql & " and a.num_reajuste=b.num_reajuste  "
'                Sql = Sql & " and a.codemp=b.codemp  "
'                Sql = Sql & " and b.estado in ('ACTIVO DIMERC','EXCLUSIVO CONVENIO','AGOTADO EN MERCADO','ARTICULO NUEVO') "
'                Sql = Sql & " and a.num_reajuste = " + num_reajuste.ToString
'                Sql = Sql & " and a.numcot = " + txtNumcot.Text
'                Sql = Sql & " and B.num_reajuste=c.num_reajuste(+)  "
'                Sql = Sql & " and b.codemp=c.codemp(+)  "
'                Sql = Sql & " and b.codlin=c.codlin(+) "
'                dt.Clear()
'                comando = New OracleCommand(Sql, conn)
'                comando.Transaction = transaccion
'                dataAdapter = New OracleDataAdapter(comando)
'                dataAdapter.Fill(dt)

'                For i = 0 To dt.Rows.Count - 1
'                    Sql = "Insert into cro_coti (rutcli,codpro,cantidad,precio,costo,fecha,fecven) values ("
'                    Sql += txtRutcli.Text + ","
'                    Sql += "'" + dt.Rows(i).Item("Codpro").ToString + "',"
'                    Sql += dt.Rows(i).Item("Cantidad").ToString + ","
'                    Sql += dt.Rows(i).Item("Precio").ToString + ","
'                    Sql += dt.Rows(i).Item("costoanalisis").ToString + ","
'                    Sql += "to_date('" + Now.Date + "','dd/mm/yyyy'),"
'                    If todos = True Then
'                        Sql += "to_date('" + fecha_reajusteTodos + "','dd/mm/yyyy')"
'                    Else
'                        Sql += "to_date('" + dt.Rows(i).Item("fecven") + "','dd/mm/yyyy')"
'                    End If
'                    Sql += ")"
'                    comando = New OracleCommand(Sql, conn)
'                    comando.Transaction = transaccion
'                    comando.ExecuteNonQuery()

'                Next



'                For i = 0 To dgvRelComercial.RowCount - 1
'                    If dgvRelComercial.Item("Sel", i).Value = 1 Then
'                        For x = 0 To dt.Rows.Count - 1
'                            Sql = "Insert into cro_coti (rutcli,codpro,cantidad,precio,costo,fecha,fecven) values ("
'                            Sql += dgvRelComercial.Item("Rutcli", i).Value.ToString + ","
'                            Sql += "'" + dt.Rows(x).Item("Codpro").ToString + "',"
'                            Sql += dt.Rows(x).Item("Cantidad").ToString + ","
'                            Sql += dt.Rows(x).Item("Precio").ToString + ","
'                            Sql += dt.Rows(x).Item("costoanalisis").ToString + ","
'                            Sql += "to_date('" + Now.Date + "','dd/mm/yyyy'),"
'                            If todos = True Then
'                                Sql += "to_date('" + fecha_reajusteTodos + "','dd/mm/yyyy')"
'                            Else
'                                Sql += "to_date('" + dt.Rows(x).Item("fecven") + "','dd/mm/yyyy')"
'                            End If
'                            Sql += ")"
'                            comando = New OracleCommand(Sql, conn)
'                            comando.Transaction = transaccion
'                            comando.ExecuteNonQuery()

'                        Next

'                    End If
'                Next


'                comando.CommandType = CommandType.StoredProcedure
'                'comando.Parameters.AddWithValue("FECHAENCABEZADO_", "to_date(20/10/2018,'dd/mm/yyyy')")
'                'comando.Parameters.AddWithValue("OBSERV", txtObservConvenio.Text)
'                'comando.Parameters.Add("FECHAENCABEZADO_", OracleType.DateTime).Value = dtpFecVenConvenio.Value.Date
'                comando.Parameters.Add("OBSERV_", OracleType.VarChar).Value = txtObservConvenio.Text
'                comando.CommandText = "COTI_MASIVA_CONVENIO"
'                comando.Transaction = transaccion
'                comando.ExecuteNonQuery()

'                Sql = "delete from cro_coti"
'                comando = New OracleCommand(Sql, conn)
'                comando.Transaction = transaccion
'                comando.ExecuteNonQuery()

'                Sql = " update en_reajuste_convenio set estado=4 "
'                Sql = Sql & " where codemp=3 and num_reajuste= " + num_reajuste.ToString
'                comando = New OracleCommand(Sql, conn)
'                comando.Transaction = transaccion
'                comando.ExecuteNonQuery()


'                transaccion.Commit()

'                MessageBox.Show("Carga de Convenio Lista", "Finalizado", MessageBoxButtons.OK, MessageBoxIcon.Information)
'            Catch ex As Exception
'                If Not transaccion.Equals(Nothing) Then
'                    transaccion.Rollback()
'                End If
'                MessageBox.Show(ex.ToString, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
'            End Try
'        End Using
'    End Using


'End Sub

'Private Sub cargacostosespeciales()
'    Using conn = New OracleConnection(Conexion.retornaConexion)
'        conn.Open()
'        Using transaccion = conn.BeginTransaction
'            Try

'                Dim comando As OracleCommand
'                Dim dt As New DataTable
'                Dim dataAdapter As OracleDataAdapter
'                Dim sequen As String
'                Dim Sql = "Set Role Rol_Aplicacion"
'                comando = New OracleCommand(Sql, conn)
'                comando.Transaction = transaccion
'                comando.ExecuteNonQuery()

'                Sql = " Select EN_CLIENTE_COSTO_SEQ.nextval seq from dual "

'                comando = New OracleCommand(Sql, conn)
'                comando.Transaction = transaccion
'                dataAdapter = New OracleDataAdapter(comando)
'                dataAdapter.Fill(dt)
'                If dt.Rows.Count = 0 Then
'                    If Not transaccion.Equals(Nothing) Then
'                        transaccion.Rollback()
'                    End If
'                    MessageBox.Show("NO se encontró una secuencia. Consulte a Sistemas...", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
'                    Exit Sub
'                Else
'                    sequen = dt.Rows(0).Item("seq").ToString
'                End If

'                Dim dtListado As New DataTable
'                Sql = " select a.rutcli,b.codpro,b.precio,b.cosesp,b.mg_comercial,trunc(sysdate) fecini,a.fecfin,1 cantid,'" + Globales.user + "' usuario, "
'                Sql = Sql & "3 codemp,decode(greatest(b.cosesp,getcostopromedio(3,b.codpro)),b.cosesp,2,0 ) tipo,0 codmon  from en_reajuste_convenio a,de_reajuste_convenio b "
'                Sql = Sql & "where a.codemp=b.codemp "
'                Sql = Sql & "and a.num_reajuste=b.num_reajuste "
'                Sql = Sql & "and a.numcot=b.numcot "
'                Sql = Sql & " and a.numcot = " + txtNumcot.Text
'                Sql = Sql & " and a.num_reajuste = " + num_reajuste.ToString
'                Sql = Sql & "and b.cosesp>0 "
'                comando = New OracleCommand(Sql, conn)
'                comando.Transaction = transaccion
'                dataAdapter = New OracleDataAdapter(comando)
'                dataAdapter.Fill(dtListado)

'                Sql = " update en_cliente_costo set fecfin=TO_DATE('" & dtpCosEspIni.Value.AddDays(-1).Date & "','dd/mm/yyyy') "
'                Sql = Sql & " where codemp=3 and (rutcli,codpro) in( "
'                Sql = Sql & " select a.rutcli,b.codpro from en_reajuste_convenio a,de_reajuste_convenio b "
'                Sql = Sql & " where a.codemp=b.codemp "
'                Sql = Sql & " and a.num_reajuste=b.num_reajuste "
'                Sql = Sql & " and a.numcot=b.numcot "
'                Sql = Sql & " and a.numcot = " + txtNumcot.Text
'                Sql = Sql & " and a.num_reajuste = " + num_reajuste.ToString
'                Sql = Sql & "and b.cosesp>0) and trunc(sysdate) between (fecini-1) and (fecfin+1) "
'                comando = New OracleCommand(Sql, conn)
'                comando.Transaction = transaccion
'                comando.ExecuteNonQuery()

'                For i = 0 To dtListado.Rows.Count - 1
'                    Sql = " INSERT INTO en_cliente_costo (ID_NUMERO, RUTCLI, CODPRO, PRECIO, COSTO, MARGEN, FECINI, FECFIN, CANTID, USR_CREAC, CODEMP, TIPO,CODMON,ESTADO ) VALUES( "
'                    Sql = Sql & sequen & ", "
'                    Sql = Sql & dtListado.Rows(i).Item("rutcli").ToString & " , "
'                    Sql = Sql & "'" & dtListado.Rows(i).Item("codpro").ToString & "',"
'                    Sql = Sql & dtListado.Rows(i).Item("precio").ToString & ","
'                    Sql = Sql & dtListado.Rows(i).Item("cosesp").ToString & ", "
'                    Sql = Sql & dtListado.Rows(i).Item("mg_comercial").ToString.Replace(",", ".") & ", "
'                    Sql = Sql & "TO_DATE('" & dtpCosEspIni.Value.Date & "','dd/mm/yyyy'), "
'                    Sql = Sql & "TO_DATE('" & dtpCosEspFin.Value.Date & "','dd/mm/yyyy'), "
'                    Sql = Sql & "1,'"
'                    Sql = Sql & Globales.user & "',"
'                    Sql = Sql & "3,"
'                    Sql = Sql & dtListado.Rows(i).Item("tipo").ToString & " , "
'                    Sql = Sql & "0,'V')"
'                    comando = New OracleCommand(Sql, conn)
'                    comando.Transaction = transaccion
'                    comando.ExecuteNonQuery()
'                Next

'                For x = 0 To dgvEmpRelacion.RowCount - 1
'                    If dgvEmpRelacion.Item("Sel", x).Value = 1 Then
'                        Sql = " Select EN_CLIENTE_COSTO_SEQ.nextval seq from dual "
'                        dt.Clear()
'                        comando = New OracleCommand(Sql, conn)
'                        comando.Transaction = transaccion
'                        dataAdapter = New OracleDataAdapter(comando)
'                        dataAdapter.Fill(dt)
'                        If dt.Rows.Count = 0 Then
'                            If Not transaccion.Equals(Nothing) Then
'                                transaccion.Rollback()
'                            End If
'                            MessageBox.Show("NO se encontró una secuencia. Consulte a Sistemas...", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
'                            Exit Sub
'                        Else
'                            sequen = dt.Rows(0).Item("seq").ToString
'                        End If

'                        dtListado.Clear()
'                        Sql = " select " + dgvEmpRelacion.Item("rutcli", x).Value.ToString + " rutcli,b.codpro,b.precio,b.cosesp,b.mg_comercial,trunc(sysdate) fecini,a.fecfin,1 cantid,'" + Globales.user + "' usuario, "
'                        Sql = Sql & "3 codemp,decode(greatest(b.cosesp,getcostopromedio(3,b.codpro)),b.cosesp,2,0 ) tipo,0 codmon  from en_reajuste_convenio a,de_reajuste_convenio b "
'                        Sql = Sql & " where a.codemp=b.codemp "
'                        Sql = Sql & " and a.num_reajuste=b.num_reajuste "
'                        Sql = Sql & " and a.numcot=b.numcot "
'                        Sql = Sql & " and a.numcot = " + txtNumcot.Text
'                        Sql = Sql & " and a.num_reajuste = " + num_reajuste.ToString
'                        Sql = Sql & " and b.cosesp>0 "
'                        comando = New OracleCommand(Sql, conn)
'                        comando.Transaction = transaccion
'                        dataAdapter = New OracleDataAdapter(comando)
'                        dataAdapter.Fill(dtListado)

'                        Sql = " update en_cliente_costo set fecfin=TO_DATE('" & dtpCosEspIni.Value.AddDays(-1).Date & "','dd/mm/yyyy') "
'                        Sql = Sql & " where codemp=3 and (rutcli,codpro) in( "
'                        Sql = Sql & " select " + dgvEmpRelacion.Item("rutcli", x).Value.ToString + " rutcli,b.codpro "
'                        Sql = Sql & " from en_reajuste_convenio a,de_reajuste_convenio b "
'                        Sql = Sql & " where a.codemp=b.codemp "
'                        Sql = Sql & " And a.num_reajuste=b.num_reajuste "
'                        Sql = Sql & " And a.numcot=b.numcot "
'                        Sql = Sql & " and a.numcot = " + txtNumcot.Text
'                        Sql = Sql & " and a.num_reajuste = " + num_reajuste.ToString
'                        Sql = Sql & "and b.cosesp>0) and trunc(sysdate) between (fecini-1) and (fecfin+1) "
'                        comando = New OracleCommand(Sql, conn)
'                        comando.Transaction = transaccion
'                        comando.ExecuteNonQuery()

'                        For i = 0 To dtListado.Rows.Count - 1
'                            Sql = " INSERT INTO en_cliente_costo (ID_NUMERO, RUTCLI, CODPRO, PRECIO, COSTO, MARGEN, FECINI, FECFIN, CANTID, USR_CREAC, CODEMP, TIPO,CODMON,ESTADO ) VALUES( "
'                            Sql = Sql & sequen & ", "
'                            Sql = Sql & dgvEmpRelacion.Item("rutcli", x).Value.ToString & " , "
'                            Sql = Sql & "'" & dtListado.Rows(i).Item("codpro").ToString & "',"
'                            Sql = Sql & dtListado.Rows(i).Item("precio").ToString & ","
'                            Sql = Sql & dtListado.Rows(i).Item("cosesp").ToString & ", "
'                            Sql = Sql & dtListado.Rows(i).Item("mg_comercial").ToString.Replace(",", ".") & ", "
'                            Sql = Sql & "TO_DATE('" & dtpCosEspIni.Value.Date & "','dd/mm/yyyy'), "
'                            Sql = Sql & "TO_DATE('" & dtpCosEspFin.Value.Date & "','dd/mm/yyyy'), "
'                            Sql = Sql & "1,'"
'                            Sql = Sql & Globales.user & "',"
'                            Sql = Sql & "3,"
'                            Sql = Sql & dtListado.Rows(i).Item("tipo").ToString & " , "
'                            Sql = Sql & "0,'V')"
'                            comando = New OracleCommand(Sql, conn)
'                            comando.Transaction = transaccion
'                            comando.ExecuteNonQuery()
'                        Next
'                    End If
'                Next
'                Sql = " update en_reajuste_convenio set estado=3 "
'                Sql = Sql & " where codemp=3 and num_reajuste= " + num_reajuste.ToString
'                comando = New OracleCommand(Sql, conn)
'                comando.Transaction = transaccion
'                comando.ExecuteNonQuery()

'                transaccion.Commit()

'                MessageBox.Show("Costos Especiales Cargados con éxito", "Finalizado", MessageBoxButtons.OK, MessageBoxIcon.Information)

'            Catch ex As Exception
'                If Not transaccion.Equals(Nothing) Then
'                    transaccion.Rollback()
'                End If
'                MessageBox.Show(ex.ToString, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
'            Finally
'                conn.Close()
'            End Try



'        End Using
'    End Using
'End Sub