﻿Imports System.ComponentModel
Imports ClosedXML.Excel

Public Class frmCargaOferton
    Dim dtxml As New DataTable
    Dim botOfertas As New Bot2
    Dim PuntoControl As New PuntoControl
    Private Sub frmCargaMercadoPublico_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If Globales.tema = 1 Then
            Me.Theme = 2
            Globales.tema = 2
        Else
            Me.Theme = 1
            Globales.tema = 1
        End If
        PuntoControl.RegistroUso("20")
    End Sub

    Private Sub MetroButton1_Click(sender As Object, e As EventArgs) Handles MetroButton1.Click
        If OpenFileDialog1.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            Globales.Importar(dgvDatos, OpenFileDialog1.FileName)
            dgvDatos.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            dgvDatos.Sort(dgvDatos.Columns(0), ListSortDirection.Ascending)
            lblTotales.Text = "Total: " & dgvDatos.RowCount.ToString
        End If
        PuntoControl.RegistroUso("14")
    End Sub

    Private Sub btn_Grabar_Click(sender As Object, e As EventArgs) Handles btn_Grabar.Click
        If dgvDatos.RowCount = 0 Then
            MessageBox.Show("No hay datos consultados en la grilla de cargos", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        Else
            ExportarDgvExcel("\\10.10.20.222\sistema\ArielLopez\MercadoPublico\Ofertas.xlsx")
            MessageBox.Show("Exportado satisfactoriamente.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
        PuntoControl.RegistroUso("15")
    End Sub

    Public Sub ExportarDgvExcel(ruta As String)
        Try
            Dim WorkBook As New XLWorkbook
            dtxml = dgvDatos.DataSource
            dtxml.TableName = "DATOS"
            WorkBook.Worksheets.Add(dtxml)
            WorkBook.SaveAs(ruta)
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub MetroButton2_Click(sender As Object, e As EventArgs) Handles btnCarga.Click
        btnCarga.Enabled = False
        Timer1.Enabled = True
        PuntoControl.RegistroUso("16")
        BackgroundWorker1.RunWorkerAsync()
    End Sub

    Private Sub BackgroundWorker1_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        'botOfertas.inicioBot()
    End Sub

    Private Sub BackgroundWorker1_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted
        MetroProgressSpinner1.Visible = True
        btnCarga.Enabled = True
        Timer1.Enabled = False
        ProgressBar1.Maximum = 0
        ProgressBar2.Maximum = 0
        ProgressBar1.Value = 0
        ProgressBar2.Value = 0
        MetroProgressSpinner1.Visible = False
        MessageBox.Show("Proceso finalizado", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        'ProgressBar1.Maximum = botOfertas.maximoLinea
        'ProgressBar2.Maximum = botOfertas.maximoProducto
        'ProgressBar1.Value = botOfertas.valorLinea
        'ProgressBar2.Value = botOfertas.valorProducto
    End Sub
End Class