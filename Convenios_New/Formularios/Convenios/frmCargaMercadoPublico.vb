﻿Imports System.ComponentModel
Imports ClosedXML.Excel

Public Class frmCargaMercadoPublico
    Dim dtxml As New DataTable
    Dim botProductos As New Bot1
    Dim PuntoControl As New PuntoControl
    'Dim botProductos As New RobotMercadoPublico
    Private Sub frmCargaMercadoPublico_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If Globales.tema = 1 Then
            Me.Theme = 2
            Globales.tema = 2
        Else
            Me.Theme = 1
            Globales.tema = 1
        End If
        PuntoControl.RegistroUso("19")
    End Sub

    Private Sub btn_Grabar_Click(sender As Object, e As EventArgs)
        If dgvDatos.RowCount = 0 Then
            MessageBox.Show("No hay datos consultados en la grilla de cargos", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        Else
            ExportarDgvExcel("\\10.10.20.222\sistema\ArielLopez\MercadoPublico\Productos.xlsx")
            MessageBox.Show("Exportado satisfactoriamente.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Public Sub ExportarDgvExcel(ruta As String)
        Try
            Dim WorkBook As New XLWorkbook
            dtxml = dgvDatos.DataSource
            dtxml.TableName = "DATOS"
            WorkBook.Worksheets.Add(dtxml)
            WorkBook.SaveAs(ruta)
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Public Sub ExportarDgvExcel(ruta As String, data As DataTable)
        Try
            Dim WorkBook As New XLWorkbook
            data.TableName = "DATOS"
            WorkBook.Worksheets.Add(data)
            WorkBook.SaveAs(ruta)
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub BackgroundWorker1_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        botProductos.tablaTrabajo = DirectCast(dgvDatos.DataSource, DataTable)
        botProductos.inicioBot()
    End Sub

    Private Sub BackgroundWorker1_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted
        btnCarga.Enabled = True
        Timer1.Enabled = False
        ProgressBar1.Maximum = 0
        ProgressBar2.Maximum = 0
        ProgressBar1.Value = 0
        ProgressBar2.Value = 0
        MetroProgressSpinner1.Visible = False
        MessageBox.Show("Proceso finalizado", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        MetroProgressSpinner1.Visible = True
        ProgressBar1.Maximum = botProductos.maximoLinea
        ProgressBar2.Maximum = botProductos.maximoProducto
        ProgressBar1.Value = botProductos.valorLinea
        ProgressBar2.Value = botProductos.valorProducto
        lblCuenta.Text = botProductos.valorProducto & " de " & botProductos.maximoProducto
        lblEstado.Text = botProductos.textoProceso
        If cbxVisible.Checked = True Or cbxVisible.Checked = 1 Then
            botProductos.visible = True
        Else
            botProductos.visible = False
        End If
    End Sub

    Private Sub btnCarga_Click(sender As Object, e As EventArgs) Handles btnCarga.Click
        btnCarga.Enabled = False
        Timer1.Enabled = True
        PuntoControl.RegistroUso("13")
        BackgroundWorker1.RunWorkerAsync()
    End Sub

    Private Sub btn_Grabar_Click_1(sender As Object, e As EventArgs)
        If dgvDatos.RowCount = 0 Then
            MessageBox.Show("No hay datos consultados en la grilla de cargos", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        Else
            ExportarDgvExcel("\\10.10.20.222\sistema\ArielLopez\MercadoPublico\Productos.xlsx")
            MessageBox.Show("Exportado satisfactoriamente.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub MetroButton1_Click_1(sender As Object, e As EventArgs) Handles MetroButton1.Click
        If OpenFileDialog1.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            Globales.Importar(dgvDatos, OpenFileDialog1.FileName)
            dgvDatos.Sort(dgvDatos.Columns(0), ListSortDirection.Ascending)
            dgvDatos.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            lblTotales.Text = "Total: " & dgvDatos.RowCount.ToString
        End If
        MessageBox.Show("Recordar que las licitaciones deben de estar ordenadas de nemor a mayor antes de cargar y que los precios en Dolar deben tener 2 decimales y sin signo de moneda.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        PuntoControl.RegistroUso("12")
    End Sub

    Private Sub MetroButton2_Click(sender As Object, e As EventArgs) Handles MetroButton2.Click
        If SaveFileDialog1.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            Dim BD As New Conexion
            Dim sql As String
            Dim dt As New DataTable
            Dim dgv As New DataGridView
            BD.open_dimerc()

            sql = "select codpro codigo, codpro_alt cod_chileCompra, descripcion, rutcompetidor licitacion, nvl(stock,'No encontrado') stock, nvl(precio,0) precio from re_producto_negocio where tipo = 'CV'"
            dt = BD.sqlSelect(sql)
            ExportarDgvExcel(SaveFileDialog1.FileName & ".xlsx", dt)

            PuntoControl.RegistroUso("11")
        End If
    End Sub
End Class