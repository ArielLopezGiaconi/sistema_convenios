﻿Public Class frmConsultaProximoReajuste
    Dim bd As New Conexion
    Dim PuntoControl As New PuntoControl
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btn_Salir.Click
        Close()
    End Sub

    Private Sub dtpFechaLineaReajuste_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFechaLineaReajuste.KeyPress
        buscaProximosReajustes()
    End Sub

    Private Sub buscaProximosReajustes()
        Try
            bd.open_dimerc()
            Dim SQL = " select a.rutcli ""Rutcli"",getrazonsocial(a.codemp,a.rutcli) ""Razón Social"",a.num_reajuste ""Num. Reajuste"", "
            SQL = SQL & " a.numcot, c.fecemi,c.fecven, b.linea ""Línea"",fecha_reajuste ""Fecha Reajuste""  "
            SQL = SQL & " from en_reajuste_convenio a, de_fechas_reajuste_convenio b, en_cotizac c "
            SQL = SQL & " where a.codemp=b.codemp "
            SQL = SQL & " and a.num_reajuste=b.num_reajuste "
            SQL = SQL & " and a.codemp=c.codemp "
            SQL = SQL & " and a.numcot=c.numcot "
            SQL = SQL & " and fecha_reajuste=to_date('" + dtpFechaLineaReajuste.Value.Date + "','dd/mm/yyyy')"
            SQL = SQL & " order by a.rutcli,b.linea "
            Dim dt = bd.sqlSelect(SQL)
            If dt.Rows.Count = 0 Then
                MessageBox.Show("No hay reajustes programados para esta fecha", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            Else
                dgvConsulta.DataSource = dt
                dgvConsulta.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            End If


        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub frmConsultaProximoReajuste_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If Globales.tema = 1 Then
            Me.Theme = 2
            Globales.tema = 2
        Else
            Me.Theme = 1
            Globales.tema = 1
        End If
        Me.StartPosition = FormStartPosition.Manual
        Me.Location = New Point(0, 0)
        PuntoControl.RegistroUso("21")
    End Sub

    Private Sub btn_Buscar_Click(sender As Object, e As EventArgs) Handles btn_Buscar.Click
        buscaProximosReajustes()
        PuntoControl.RegistroUso("17")
    End Sub

    Private Sub btn_Excel_Click(sender As Object, e As EventArgs) Handles btn_Excel.Click
        Dim save As New SaveFileDialog
        If dgvConsulta.RowCount = 0 Then
            MessageBox.Show("No hay datos consultados en la grilla, procese datos antes de exportar", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        Else
            save.Filter = "Archivo Excel | *.xlsx"
            If save.ShowDialog = System.Windows.Forms.DialogResult.OK Then
                Globales.exporta(save.FileName, dgvConsulta)
            End If
        End If
        PuntoControl.RegistroUso("18")
    End Sub
End Class