﻿Public Class frmBuscarProducto
    Dim bd As New Conexion
    Dim datos As String
    Dim comienza As Boolean
    Dim flagGrilla As Boolean = False
    Dim PuntoControl As New PuntoControl
    Public ok As Boolean = False
    Private Sub frmBuscarProducto_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.StartPosition = FormStartPosition.Manual
        Me.Location = New Point(100, 85)
        buscar()
        PuntoControl.RegistroUso("237")
    End Sub

    Public Sub New(dato As String, comienzaOContiene As Boolean)
        ' Llamada necesaria para el diseñador.
        InitializeComponent()
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        datos = dato
        comienza = comienzaOContiene
        buscar()
    End Sub

    Private Sub buscar()
        Try
            bd.open_dimerc()
            Dim sql = "select  "
            sql = sql & " a.codpro ""Codigo"",a.despro ""Descripción"", b.desmar ""Marca"",a.costo ""Costo"",(a.stkfsc - a.stkcom) ""Stock"", c.desval ""Estado"",a.codigo_uni, a.pagcat     fdlStock  "
            sql = sql & " from MA_PRODUCT a, ma_marcapr b , de_dominio c "
            If comienza = True Then
                sql = sql & " where a.despro like '" & datos & "%'"
            Else
                sql = sql & " where a.despro like '%" & datos & "%' "
            End If
            sql = sql & " and c.coddom = '20' and c.codval = to_char(a.estpro) "
            sql = sql & " and b.codmar = a.codmar and a.estpro in(1,3,10,11) "
            sql = sql & " ORDER BY A.DESPRO "

            dgvDatos.DataSource = bd.sqlSelect(sql)

            If dgvDatos.RowCount = 0 Then
                MessageBox.Show("No se han encontrado coincidencias.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Else
                dgvDatos.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
                dgvDatos.Columns("codigo_uni").Visible = False
                dgvDatos.Columns("fdlStock").Visible = False
                dgvDatos.Columns("Costo").DefaultCellStyle.Format = "n0"
                dgvDatos.Columns("Stock").DefaultCellStyle.Format = "n0"
                dgvDatos.Columns("Costo").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgvDatos.Columns("Stock").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

                flagGrilla = True
            End If

        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try

    End Sub

    Private Sub frmBuscarProducto_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.Handled = True
            ok = True
            Me.Close()
        ElseIf e.KeyCode = Keys.Escape Then
            e.Handled = True
            ok = False
            Me.Close()
        End If
    End Sub

    Private Sub txtCodUni_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtUniReem.KeyPress, txtCodUni.KeyPress
        e.Handled = True
    End Sub

    Private Sub dgvDatos_CurrentCellChanged(sender As System.Object, e As System.EventArgs) Handles dgvDatos.CurrentCellChanged
        If flagGrilla = True And Not dgvDatos.CurrentRow Is Nothing Then
            txtCodUni.Text = dgvDatos.Item("codigo_uni", dgvDatos.CurrentRow.Index).Value.ToString
            txtUniReem.Text = dgvDatos.Item("fdlStock", dgvDatos.CurrentRow.Index).Value.ToString
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        ok = False
        Close()
    End Sub
End Class