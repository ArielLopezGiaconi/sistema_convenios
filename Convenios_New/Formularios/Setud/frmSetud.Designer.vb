﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSetud
    Inherits MetroFramework.Forms.MetroForm    'Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSetud))
        Me.cmbTheme = New MetroFramework.Controls.MetroComboBox()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.cmbEstilos = New MetroFramework.Controls.MetroComboBox()
        Me.tracVolumen = New MetroFramework.Controls.MetroTrackBar()
        Me.MetroLabel3 = New MetroFramework.Controls.MetroLabel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'cmbTheme
        '
        Me.cmbTheme.FormattingEnabled = True
        Me.cmbTheme.ItemHeight = 23
        Me.cmbTheme.Items.AddRange(New Object() {"Dark", "Light"})
        Me.cmbTheme.Location = New System.Drawing.Point(97, 68)
        Me.cmbTheme.Name = "cmbTheme"
        Me.cmbTheme.Size = New System.Drawing.Size(121, 29)
        Me.cmbTheme.TabIndex = 0
        Me.cmbTheme.UseSelectable = True
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel1.Location = New System.Drawing.Point(12, 68)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(51, 25)
        Me.MetroLabel1.TabIndex = 1
        Me.MetroLabel1.Text = "Tema"
        '
        'MetroLabel2
        '
        Me.MetroLabel2.AutoSize = True
        Me.MetroLabel2.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel2.Location = New System.Drawing.Point(12, 103)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(51, 25)
        Me.MetroLabel2.TabIndex = 3
        Me.MetroLabel2.Text = "Estilo"
        '
        'cmbEstilos
        '
        Me.cmbEstilos.FormattingEnabled = True
        Me.cmbEstilos.ItemHeight = 23
        Me.cmbEstilos.Items.AddRange(New Object() {"Lime", "Green", "Blue", "Bronw", "Orange", "Black", "Pink", "Red", "Yellow"})
        Me.cmbEstilos.Location = New System.Drawing.Point(97, 103)
        Me.cmbEstilos.Name = "cmbEstilos"
        Me.cmbEstilos.Size = New System.Drawing.Size(121, 29)
        Me.cmbEstilos.TabIndex = 2
        Me.cmbEstilos.UseSelectable = True
        '
        'tracVolumen
        '
        Me.tracVolumen.BackColor = System.Drawing.Color.Transparent
        Me.tracVolumen.Location = New System.Drawing.Point(97, 147)
        Me.tracVolumen.Name = "tracVolumen"
        Me.tracVolumen.Size = New System.Drawing.Size(75, 23)
        Me.tracVolumen.TabIndex = 4
        Me.tracVolumen.Text = "MetroTrackBar1"
        '
        'MetroLabel3
        '
        Me.MetroLabel3.AutoSize = True
        Me.MetroLabel3.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel3.Location = New System.Drawing.Point(12, 145)
        Me.MetroLabel3.Name = "MetroLabel3"
        Me.MetroLabel3.Size = New System.Drawing.Size(79, 25)
        Me.MetroLabel3.TabIndex = 5
        Me.MetroLabel3.Text = "Volumen"
        '
        'Button1
        '
        Me.Button1.Image = CType(resources.GetObject("Button1.Image"), System.Drawing.Image)
        Me.Button1.Location = New System.Drawing.Point(97, 176)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(43, 41)
        Me.Button1.TabIndex = 6
        Me.Button1.UseVisualStyleBackColor = True
        '
        'frmSetud
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(684, 305)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.MetroLabel3)
        Me.Controls.Add(Me.tracVolumen)
        Me.Controls.Add(Me.MetroLabel2)
        Me.Controls.Add(Me.cmbEstilos)
        Me.Controls.Add(Me.MetroLabel1)
        Me.Controls.Add(Me.cmbTheme)
        Me.ForeColor = System.Drawing.Color.Black
        Me.Name = "frmSetud"
        Me.Text = "Configuraciones"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents cmbTheme As MetroFramework.Controls.MetroComboBox
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents cmbEstilos As MetroFramework.Controls.MetroComboBox
    Friend WithEvents tracVolumen As MetroFramework.Controls.MetroTrackBar
    Friend WithEvents MetroLabel3 As MetroFramework.Controls.MetroLabel
    Friend WithEvents Button1 As Button
End Class
