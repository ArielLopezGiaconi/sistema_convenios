﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmPantallas
    Inherits MetroFramework.Forms.MetroForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPantallas))
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox8 = New System.Windows.Forms.GroupBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.lblCostoCoti = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.lblPRecioCoti = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.lblCotizacion = New System.Windows.Forms.Label()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.lblCostoMarco = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.lblPrecioMarco = New System.Windows.Forms.Label()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.lblAPConvenio = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.lblCostoConvenio = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.lblPrecioConvenio = New System.Windows.Forms.Label()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.lblmargen = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.lblAPCosto = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.lblCostoEspecial = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.lblPrecioEspecial = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.lblPrecioMin = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.lblPrecioMax = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.lblListasPrecio = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.lblRutcli = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.lblEjecutivo = New System.Windows.Forms.Label()
        Me.rbtCosto = New System.Windows.Forms.RadioButton()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.rbtCosprom = New System.Windows.Forms.RadioButton()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.rbtCostocte = New System.Windows.Forms.RadioButton()
        Me.lblCosto = New System.Windows.Forms.Label()
        Me.lblCosprom = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.lblCostocte = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblPrecioNota = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.lblLinea = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lblCostoPromedio = New System.Windows.Forms.Label()
        Me.lblCostoComercial = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtCostoModif = New System.Windows.Forms.TextBox()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.txtProducto = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtNota = New System.Windows.Forms.TextBox()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.btnImportar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtCodigo = New System.Windows.Forms.TextBox()
        Me.dtpFecha = New System.Windows.Forms.DateTimePicker()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.dgvDatosSku = New System.Windows.Forms.DataGridView()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        CType(Me.dgvDatosSku, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Location = New System.Drawing.Point(10, 26)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(686, 641)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.GroupBox1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(678, 615)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Ficha Modificacion Nota"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.GroupBox8)
        Me.GroupBox1.Controls.Add(Me.GroupBox7)
        Me.GroupBox1.Controls.Add(Me.GroupBox6)
        Me.GroupBox1.Controls.Add(Me.GroupBox5)
        Me.GroupBox1.Controls.Add(Me.GroupBox4)
        Me.GroupBox1.Controls.Add(Me.GroupBox3)
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtCostoModif)
        Me.GroupBox1.Controls.Add(Me.btnBuscar)
        Me.GroupBox1.Controls.Add(Me.Label19)
        Me.GroupBox1.Controls.Add(Me.txtProducto)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtNota)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 6)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(669, 599)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Seguimiento Precio x nota"
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.Label27)
        Me.GroupBox8.Controls.Add(Me.lblCostoCoti)
        Me.GroupBox8.Controls.Add(Me.Label7)
        Me.GroupBox8.Controls.Add(Me.lblPRecioCoti)
        Me.GroupBox8.Controls.Add(Me.Label20)
        Me.GroupBox8.Controls.Add(Me.lblCotizacion)
        Me.GroupBox8.Location = New System.Drawing.Point(6, 524)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(657, 69)
        Me.GroupBox8.TabIndex = 53
        Me.GroupBox8.TabStop = False
        Me.GroupBox8.Text = "Datros Cotizacion"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(167, 16)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(34, 13)
        Me.Label27.TabIndex = 26
        Me.Label27.Text = "Costo"
        '
        'lblCostoCoti
        '
        Me.lblCostoCoti.AutoSize = True
        Me.lblCostoCoti.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostoCoti.Location = New System.Drawing.Point(172, 33)
        Me.lblCostoCoti.Name = "lblCostoCoti"
        Me.lblCostoCoti.Size = New System.Drawing.Size(34, 20)
        Me.lblCostoCoti.TabIndex = 27
        Me.lblCostoCoti.Text = "$ 0"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(7, 19)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(37, 13)
        Me.Label7.TabIndex = 24
        Me.Label7.Text = "Precio"
        '
        'lblPRecioCoti
        '
        Me.lblPRecioCoti.AutoSize = True
        Me.lblPRecioCoti.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPRecioCoti.Location = New System.Drawing.Point(12, 36)
        Me.lblPRecioCoti.Name = "lblPRecioCoti"
        Me.lblPRecioCoti.Size = New System.Drawing.Size(34, 20)
        Me.lblPRecioCoti.TabIndex = 25
        Me.lblPRecioCoti.Text = "$ 0"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(325, 19)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(96, 13)
        Me.Label20.TabIndex = 32
        Me.Label20.Text = "Numero Cotizacion"
        '
        'lblCotizacion
        '
        Me.lblCotizacion.AutoSize = True
        Me.lblCotizacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCotizacion.Location = New System.Drawing.Point(330, 36)
        Me.lblCotizacion.Name = "lblCotizacion"
        Me.lblCotizacion.Size = New System.Drawing.Size(24, 20)
        Me.lblCotizacion.TabIndex = 33
        Me.lblCotizacion.Text = "..."
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.Label25)
        Me.GroupBox7.Controls.Add(Me.lblCostoMarco)
        Me.GroupBox7.Controls.Add(Me.Label18)
        Me.GroupBox7.Controls.Add(Me.lblPrecioMarco)
        Me.GroupBox7.Location = New System.Drawing.Point(6, 457)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(656, 67)
        Me.GroupBox7.TabIndex = 52
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Datos Convenio Marco"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(167, 19)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(34, 13)
        Me.Label25.TabIndex = 21
        Me.Label25.Text = "Costo"
        '
        'lblCostoMarco
        '
        Me.lblCostoMarco.AutoSize = True
        Me.lblCostoMarco.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostoMarco.Location = New System.Drawing.Point(172, 36)
        Me.lblCostoMarco.Name = "lblCostoMarco"
        Me.lblCostoMarco.Size = New System.Drawing.Size(34, 20)
        Me.lblCostoMarco.TabIndex = 22
        Me.lblCostoMarco.Text = "$ 0"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(8, 19)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(37, 13)
        Me.Label18.TabIndex = 19
        Me.Label18.Text = "Precio"
        '
        'lblPrecioMarco
        '
        Me.lblPrecioMarco.AutoSize = True
        Me.lblPrecioMarco.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPrecioMarco.Location = New System.Drawing.Point(13, 36)
        Me.lblPrecioMarco.Name = "lblPrecioMarco"
        Me.lblPrecioMarco.Size = New System.Drawing.Size(34, 20)
        Me.lblPrecioMarco.TabIndex = 20
        Me.lblPrecioMarco.Text = "$ 0"
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.Label24)
        Me.GroupBox6.Controls.Add(Me.lblAPConvenio)
        Me.GroupBox6.Controls.Add(Me.Label21)
        Me.GroupBox6.Controls.Add(Me.lblCostoConvenio)
        Me.GroupBox6.Controls.Add(Me.Label16)
        Me.GroupBox6.Controls.Add(Me.lblPrecioConvenio)
        Me.GroupBox6.Location = New System.Drawing.Point(5, 400)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(658, 58)
        Me.GroupBox6.TabIndex = 51
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Datos Convenio"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(329, 16)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(79, 13)
        Me.Label24.TabIndex = 21
        Me.Label24.Text = "Analista Pricing"
        '
        'lblAPConvenio
        '
        Me.lblAPConvenio.AutoSize = True
        Me.lblAPConvenio.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAPConvenio.Location = New System.Drawing.Point(334, 33)
        Me.lblAPConvenio.Name = "lblAPConvenio"
        Me.lblAPConvenio.Size = New System.Drawing.Size(24, 20)
        Me.lblAPConvenio.TabIndex = 22
        Me.lblAPConvenio.Text = "..."
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(168, 16)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(34, 13)
        Me.Label21.TabIndex = 19
        Me.Label21.Text = "Costo"
        '
        'lblCostoConvenio
        '
        Me.lblCostoConvenio.AutoSize = True
        Me.lblCostoConvenio.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostoConvenio.Location = New System.Drawing.Point(173, 33)
        Me.lblCostoConvenio.Name = "lblCostoConvenio"
        Me.lblCostoConvenio.Size = New System.Drawing.Size(34, 20)
        Me.lblCostoConvenio.TabIndex = 20
        Me.lblCostoConvenio.Text = "$ 0"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(6, 16)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(37, 13)
        Me.Label16.TabIndex = 17
        Me.Label16.Text = "Precio"
        '
        'lblPrecioConvenio
        '
        Me.lblPrecioConvenio.AutoSize = True
        Me.lblPrecioConvenio.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPrecioConvenio.Location = New System.Drawing.Point(11, 33)
        Me.lblPrecioConvenio.Name = "lblPrecioConvenio"
        Me.lblPrecioConvenio.Size = New System.Drawing.Size(34, 20)
        Me.lblPrecioConvenio.TabIndex = 18
        Me.lblPrecioConvenio.Text = "$ 0"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.Label34)
        Me.GroupBox5.Controls.Add(Me.lblmargen)
        Me.GroupBox5.Controls.Add(Me.Label23)
        Me.GroupBox5.Controls.Add(Me.lblAPCosto)
        Me.GroupBox5.Controls.Add(Me.Label13)
        Me.GroupBox5.Controls.Add(Me.lblCostoEspecial)
        Me.GroupBox5.Controls.Add(Me.Label14)
        Me.GroupBox5.Controls.Add(Me.lblPrecioEspecial)
        Me.GroupBox5.Location = New System.Drawing.Point(6, 337)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(656, 61)
        Me.GroupBox5.TabIndex = 50
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Datos costo especial"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(314, 16)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(43, 13)
        Me.Label34.TabIndex = 38
        Me.Label34.Text = "Margen"
        '
        'lblmargen
        '
        Me.lblmargen.AutoSize = True
        Me.lblmargen.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblmargen.Location = New System.Drawing.Point(319, 33)
        Me.lblmargen.Name = "lblmargen"
        Me.lblmargen.Size = New System.Drawing.Size(34, 20)
        Me.lblmargen.TabIndex = 39
        Me.lblmargen.Text = "0%"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(468, 16)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(79, 13)
        Me.Label23.TabIndex = 19
        Me.Label23.Text = "Analista Pricing"
        '
        'lblAPCosto
        '
        Me.lblAPCosto.AutoSize = True
        Me.lblAPCosto.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAPCosto.Location = New System.Drawing.Point(473, 33)
        Me.lblAPCosto.Name = "lblAPCosto"
        Me.lblAPCosto.Size = New System.Drawing.Size(24, 20)
        Me.lblAPCosto.TabIndex = 20
        Me.lblAPCosto.Text = "..."
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(177, 16)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(34, 13)
        Me.Label13.TabIndex = 17
        Me.Label13.Text = "Costo"
        '
        'lblCostoEspecial
        '
        Me.lblCostoEspecial.AutoSize = True
        Me.lblCostoEspecial.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostoEspecial.Location = New System.Drawing.Point(182, 33)
        Me.lblCostoEspecial.Name = "lblCostoEspecial"
        Me.lblCostoEspecial.Size = New System.Drawing.Size(34, 20)
        Me.lblCostoEspecial.TabIndex = 18
        Me.lblCostoEspecial.Text = "$ 0"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(6, 16)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(37, 13)
        Me.Label14.TabIndex = 15
        Me.Label14.Text = "Precio"
        '
        'lblPrecioEspecial
        '
        Me.lblPrecioEspecial.AutoSize = True
        Me.lblPrecioEspecial.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPrecioEspecial.Location = New System.Drawing.Point(11, 33)
        Me.lblPrecioEspecial.Name = "lblPrecioEspecial"
        Me.lblPrecioEspecial.Size = New System.Drawing.Size(34, 20)
        Me.lblPrecioEspecial.TabIndex = 16
        Me.lblPrecioEspecial.Text = "$ 0"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.lblPrecioMin)
        Me.GroupBox4.Controls.Add(Me.Label12)
        Me.GroupBox4.Controls.Add(Me.lblPrecioMax)
        Me.GroupBox4.Controls.Add(Me.Label10)
        Me.GroupBox4.Controls.Add(Me.lblListasPrecio)
        Me.GroupBox4.Controls.Add(Me.Label8)
        Me.GroupBox4.Location = New System.Drawing.Point(6, 225)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(656, 109)
        Me.GroupBox4.TabIndex = 49
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Cliente x Linea x Lista"
        '
        'lblPrecioMin
        '
        Me.lblPrecioMin.AutoSize = True
        Me.lblPrecioMin.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPrecioMin.Location = New System.Drawing.Point(247, 75)
        Me.lblPrecioMin.Name = "lblPrecioMin"
        Me.lblPrecioMin.Size = New System.Drawing.Size(34, 20)
        Me.lblPrecioMin.TabIndex = 20
        Me.lblPrecioMin.Text = "$ 0"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(242, 58)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(180, 13)
        Me.Label12.TabIndex = 19
        Me.Label12.Text = "Precio minimo de las listas asociadas"
        '
        'lblPrecioMax
        '
        Me.lblPrecioMax.AutoSize = True
        Me.lblPrecioMax.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPrecioMax.Location = New System.Drawing.Point(21, 75)
        Me.lblPrecioMax.Name = "lblPrecioMax"
        Me.lblPrecioMax.Size = New System.Drawing.Size(34, 20)
        Me.lblPrecioMax.TabIndex = 18
        Me.lblPrecioMax.Text = "$ 0"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(16, 58)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(183, 13)
        Me.Label10.TabIndex = 17
        Me.Label10.Text = "Precio maximo de las listas asociadas"
        '
        'lblListasPrecio
        '
        Me.lblListasPrecio.AutoSize = True
        Me.lblListasPrecio.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblListasPrecio.Location = New System.Drawing.Point(21, 38)
        Me.lblListasPrecio.Name = "lblListasPrecio"
        Me.lblListasPrecio.Size = New System.Drawing.Size(24, 20)
        Me.lblListasPrecio.TabIndex = 16
        Me.lblListasPrecio.Text = "..."
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(16, 21)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(165, 13)
        Me.Label8.TabIndex = 15
        Me.Label8.Text = "Listas de precio del cliente x linea"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Label26)
        Me.GroupBox3.Controls.Add(Me.lblRutcli)
        Me.GroupBox3.Controls.Add(Me.Label11)
        Me.GroupBox3.Controls.Add(Me.lblEjecutivo)
        Me.GroupBox3.Controls.Add(Me.rbtCosto)
        Me.GroupBox3.Controls.Add(Me.Label9)
        Me.GroupBox3.Controls.Add(Me.rbtCosprom)
        Me.GroupBox3.Controls.Add(Me.Label17)
        Me.GroupBox3.Controls.Add(Me.rbtCostocte)
        Me.GroupBox3.Controls.Add(Me.lblCosto)
        Me.GroupBox3.Controls.Add(Me.lblCosprom)
        Me.GroupBox3.Controls.Add(Me.Label22)
        Me.GroupBox3.Controls.Add(Me.lblCostocte)
        Me.GroupBox3.Controls.Add(Me.Label3)
        Me.GroupBox3.Controls.Add(Me.lblPrecioNota)
        Me.GroupBox3.Location = New System.Drawing.Point(6, 123)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(657, 101)
        Me.GroupBox3.TabIndex = 48
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Datos de la Nota de Venta"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(177, 20)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(39, 13)
        Me.Label26.TabIndex = 47
        Me.Label26.Text = "Cliente"
        '
        'lblRutcli
        '
        Me.lblRutcli.AutoSize = True
        Me.lblRutcli.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRutcli.Location = New System.Drawing.Point(182, 37)
        Me.lblRutcli.Name = "lblRutcli"
        Me.lblRutcli.Size = New System.Drawing.Size(24, 20)
        Me.lblRutcli.TabIndex = 48
        Me.lblRutcli.Text = "..."
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(10, 20)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(51, 13)
        Me.Label11.TabIndex = 28
        Me.Label11.Text = "Ejecutivo"
        '
        'lblEjecutivo
        '
        Me.lblEjecutivo.AutoSize = True
        Me.lblEjecutivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEjecutivo.Location = New System.Drawing.Point(15, 37)
        Me.lblEjecutivo.Name = "lblEjecutivo"
        Me.lblEjecutivo.Size = New System.Drawing.Size(24, 20)
        Me.lblEjecutivo.TabIndex = 29
        Me.lblEjecutivo.Text = "..."
        '
        'rbtCosto
        '
        Me.rbtCosto.AutoSize = True
        Me.rbtCosto.Location = New System.Drawing.Point(76, 57)
        Me.rbtCosto.Name = "rbtCosto"
        Me.rbtCosto.Size = New System.Drawing.Size(14, 13)
        Me.rbtCosto.TabIndex = 46
        Me.rbtCosto.TabStop = True
        Me.rbtCosto.UseVisualStyleBackColor = True
        Me.rbtCosto.Visible = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(167, 57)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(104, 13)
        Me.Label9.TabIndex = 36
        Me.Label9.Text = "Costo promedio nota"
        '
        'rbtCosprom
        '
        Me.rbtCosprom.AutoSize = True
        Me.rbtCosprom.Location = New System.Drawing.Point(277, 57)
        Me.rbtCosprom.Name = "rbtCosprom"
        Me.rbtCosprom.Size = New System.Drawing.Size(14, 13)
        Me.rbtCosprom.TabIndex = 45
        Me.rbtCosprom.TabStop = True
        Me.rbtCosprom.UseVisualStyleBackColor = True
        Me.rbtCosprom.Visible = False
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(10, 57)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(60, 13)
        Me.Label17.TabIndex = 34
        Me.Label17.Text = "Costo Nota"
        '
        'rbtCostocte
        '
        Me.rbtCostocte.AutoSize = True
        Me.rbtCostocte.Location = New System.Drawing.Point(404, 57)
        Me.rbtCostocte.Name = "rbtCostocte"
        Me.rbtCostocte.Size = New System.Drawing.Size(14, 13)
        Me.rbtCostocte.TabIndex = 44
        Me.rbtCostocte.TabStop = True
        Me.rbtCostocte.UseVisualStyleBackColor = True
        Me.rbtCostocte.Visible = False
        '
        'lblCosto
        '
        Me.lblCosto.AutoSize = True
        Me.lblCosto.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCosto.Location = New System.Drawing.Point(15, 74)
        Me.lblCosto.Name = "lblCosto"
        Me.lblCosto.Size = New System.Drawing.Size(34, 20)
        Me.lblCosto.TabIndex = 35
        Me.lblCosto.Text = "$ 0"
        '
        'lblCosprom
        '
        Me.lblCosprom.AutoSize = True
        Me.lblCosprom.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCosprom.Location = New System.Drawing.Point(165, 74)
        Me.lblCosprom.Name = "lblCosprom"
        Me.lblCosprom.Size = New System.Drawing.Size(34, 20)
        Me.lblCosprom.TabIndex = 37
        Me.lblCosprom.Text = "$ 0"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(316, 57)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(82, 13)
        Me.Label22.TabIndex = 38
        Me.Label22.Text = "Costo CTE nota"
        '
        'lblCostocte
        '
        Me.lblCostocte.AutoSize = True
        Me.lblCostocte.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostocte.Location = New System.Drawing.Point(321, 74)
        Me.lblCostocte.Name = "lblCostocte"
        Me.lblCostocte.Size = New System.Drawing.Size(34, 20)
        Me.lblCostocte.TabIndex = 39
        Me.lblCostocte.Text = "$ 0"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(468, 57)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(76, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Precio en nota"
        '
        'lblPrecioNota
        '
        Me.lblPrecioNota.AutoSize = True
        Me.lblPrecioNota.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPrecioNota.Location = New System.Drawing.Point(467, 74)
        Me.lblPrecioNota.Name = "lblPrecioNota"
        Me.lblPrecioNota.Size = New System.Drawing.Size(34, 20)
        Me.lblPrecioNota.TabIndex = 6
        Me.lblPrecioNota.Text = "$ 0"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.lblLinea)
        Me.GroupBox2.Controls.Add(Me.Label15)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.lblCostoPromedio)
        Me.GroupBox2.Controls.Add(Me.lblCostoComercial)
        Me.GroupBox2.Location = New System.Drawing.Point(6, 59)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(657, 67)
        Me.GroupBox2.TabIndex = 47
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Datos del Producto"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(16, 21)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(78, 13)
        Me.Label6.TabIndex = 7
        Me.Label6.Text = "Linea producto"
        '
        'lblLinea
        '
        Me.lblLinea.AutoSize = True
        Me.lblLinea.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLinea.Location = New System.Drawing.Point(21, 38)
        Me.lblLinea.Name = "lblLinea"
        Me.lblLinea.Size = New System.Drawing.Size(24, 20)
        Me.lblLinea.TabIndex = 8
        Me.lblLinea.Text = "..."
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(553, 16)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(83, 13)
        Me.Label15.TabIndex = 30
        Me.Label15.Text = "Costo Comercial"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(369, 16)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(81, 13)
        Me.Label5.TabIndex = 26
        Me.Label5.Text = "Costo Promedio"
        '
        'lblCostoPromedio
        '
        Me.lblCostoPromedio.AutoSize = True
        Me.lblCostoPromedio.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostoPromedio.Location = New System.Drawing.Point(374, 33)
        Me.lblCostoPromedio.Name = "lblCostoPromedio"
        Me.lblCostoPromedio.Size = New System.Drawing.Size(34, 20)
        Me.lblCostoPromedio.TabIndex = 27
        Me.lblCostoPromedio.Text = "$ 0"
        '
        'lblCostoComercial
        '
        Me.lblCostoComercial.AutoSize = True
        Me.lblCostoComercial.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostoComercial.Location = New System.Drawing.Point(558, 33)
        Me.lblCostoComercial.Name = "lblCostoComercial"
        Me.lblCostoComercial.Size = New System.Drawing.Size(34, 20)
        Me.lblCostoComercial.TabIndex = 31
        Me.lblCostoComercial.Text = "$ 0"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(433, 20)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(88, 13)
        Me.Label4.TabIndex = 43
        Me.Label4.Text = "Costo a modificar"
        '
        'txtCostoModif
        '
        Me.txtCostoModif.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCostoModif.Location = New System.Drawing.Point(433, 36)
        Me.txtCostoModif.Name = "txtCostoModif"
        Me.txtCostoModif.Size = New System.Drawing.Size(88, 20)
        Me.txtCostoModif.TabIndex = 42
        '
        'btnBuscar
        '
        Me.btnBuscar.Location = New System.Drawing.Point(308, 33)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(119, 23)
        Me.btnBuscar.TabIndex = 23
        Me.btnBuscar.Text = "Buscar"
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(158, 20)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(50, 13)
        Me.Label19.TabIndex = 22
        Me.Label19.Text = "Producto"
        '
        'txtProducto
        '
        Me.txtProducto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtProducto.Location = New System.Drawing.Point(158, 36)
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Size = New System.Drawing.Size(144, 20)
        Me.txtProducto.TabIndex = 21
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 20)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(75, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Nota de venta"
        '
        'txtNota
        '
        Me.txtNota.Location = New System.Drawing.Point(6, 36)
        Me.txtNota.Name = "txtNota"
        Me.txtNota.Size = New System.Drawing.Size(144, 20)
        Me.txtNota.TabIndex = 0
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.btnImportar)
        Me.TabPage2.Controls.Add(Me.Label1)
        Me.TabPage2.Controls.Add(Me.txtCodigo)
        Me.TabPage2.Controls.Add(Me.dtpFecha)
        Me.TabPage2.Controls.Add(Me.Button1)
        Me.TabPage2.Controls.Add(Me.dgvDatosSku)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(678, 615)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Productos Bajo Costo Inventario"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'btnImportar
        '
        Me.btnImportar.Image = CType(resources.GetObject("btnImportar.Image"), System.Drawing.Image)
        Me.btnImportar.Location = New System.Drawing.Point(619, 296)
        Me.btnImportar.Name = "btnImportar"
        Me.btnImportar.Size = New System.Drawing.Size(53, 53)
        Me.btnImportar.TabIndex = 39
        Me.btnImportar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnImportar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(187, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(43, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Codigo:"
        '
        'txtCodigo
        '
        Me.txtCodigo.Location = New System.Drawing.Point(236, 18)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.Size = New System.Drawing.Size(100, 20)
        Me.txtCodigo.TabIndex = 3
        '
        'dtpFecha
        '
        Me.dtpFecha.Location = New System.Drawing.Point(472, 14)
        Me.dtpFecha.Name = "dtpFecha"
        Me.dtpFecha.Size = New System.Drawing.Size(200, 20)
        Me.dtpFecha.TabIndex = 2
        Me.dtpFecha.Value = New Date(2019, 12, 1, 0, 0, 0, 0)
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Aqua
        Me.Button1.Location = New System.Drawing.Point(9, 15)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "Actualizar"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'dgvDatosSku
        '
        Me.dgvDatosSku.AllowUserToAddRows = False
        Me.dgvDatosSku.AllowUserToDeleteRows = False
        Me.dgvDatosSku.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDatosSku.Location = New System.Drawing.Point(6, 44)
        Me.dgvDatosSku.Name = "dgvDatosSku"
        Me.dgvDatosSku.ReadOnly = True
        Me.dgvDatosSku.RowHeadersVisible = False
        Me.dgvDatosSku.Size = New System.Drawing.Size(666, 246)
        Me.dgvDatosSku.TabIndex = 0
        '
        'frmPantallas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(700, 661)
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "frmPantallas"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox8.ResumeLayout(False)
        Me.GroupBox8.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        CType(Me.dgvDatosSku, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents lblPrecioMarco As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents lblPrecioConvenio As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents lblPrecioEspecial As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents lblLinea As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents lblPrecioNota As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents txtNota As TextBox
    Friend WithEvents btnBuscar As Button
    Friend WithEvents Label19 As Label
    Friend WithEvents txtProducto As TextBox
    Friend WithEvents lblPRecioCoti As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents lblEjecutivo As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents lblCostoPromedio As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents lblCotizacion As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents lblCostoComercial As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents lblCostocte As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents lblCosprom As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents lblCosto As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents rbtCosto As RadioButton
    Friend WithEvents rbtCosprom As RadioButton
    Friend WithEvents rbtCostocte As RadioButton
    Friend WithEvents Label4 As Label
    Friend WithEvents txtCostoModif As TextBox
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents lblPrecioMin As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents lblPrecioMax As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents lblListasPrecio As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents Label23 As Label
    Friend WithEvents lblAPCosto As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents lblCostoEspecial As Label
    Friend WithEvents GroupBox6 As GroupBox
    Friend WithEvents Label24 As Label
    Friend WithEvents lblAPConvenio As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents lblCostoConvenio As Label
    Friend WithEvents GroupBox7 As GroupBox
    Friend WithEvents GroupBox8 As GroupBox
    Friend WithEvents Label27 As Label
    Friend WithEvents lblCostoCoti As Label
    Friend WithEvents Label25 As Label
    Friend WithEvents lblCostoMarco As Label
    Friend WithEvents Label26 As Label
    Friend WithEvents lblRutcli As Label
    Friend WithEvents Label34 As Label
    Friend WithEvents lblmargen As Label
    Friend WithEvents dtpFecha As DateTimePicker
    Friend WithEvents Button1 As Button
    Friend WithEvents dgvDatosSku As DataGridView
    Friend WithEvents Label1 As Label
    Friend WithEvents txtCodigo As TextBox
    Friend WithEvents btnImportar As Button
End Class
