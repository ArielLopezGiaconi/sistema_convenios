﻿Imports System.Data.OracleClient

Public Class frmClientesxLista
    Dim bd As New Conexion
    Dim tablaListas, tablaNewListas As New DataTable

    Private Sub frmClientesxLista_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cargaCombo()
    End Sub

    Private Sub cargaCombo()
        Try
            Dim dt, dt1, dt2 As New DataTable
            Dim sql As String
            bd.open_dimerc()

            sql = "select codcnl, nomcnl lista from ma_listaprecio
                    where codemp = 3
                      and activa = 'S'
                      and actualiza = 'S'"
            dt = bd.sqlSelect(sql)
            cmbListas.DataSource = dt
            cmbListas.ValueMember = "codcnl"
            cmbListas.DisplayMember = "nomcnl"
            cmbListas.SelectedIndex = -1
            cmbListas.Refresh()

            dt1 = dt.Copy

            cmbListaNueva.DataSource = dt1
            cmbListaNueva.ValueMember = "codcnl"
            cmbListaNueva.DisplayMember = "nomcnl"
            cmbListaNueva.SelectedIndex = -1
            cmbListaNueva.Refresh()

            sql = "select codlin, deslin from ma_lineapr"
            dt2 = bd.sqlSelect(sql)
            dt2.Rows.Add(0, "Todas las Lineas")
            cmbLinea.DataSource = dt2
            cmbLinea.ValueMember = "codlin"
            cmbLinea.DisplayMember = "deslin"
            cmbLinea.SelectedIndex = -1
            cmbLinea.Refresh()

        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Error", MessageBoxButtons.OK)
        Finally
            bd.close()
        End Try
    End Sub

    Public Sub BuscaClientes()
        Try
            Dim sql As String
            bd.open_dimerc()
            lblcargando.Visible = True

            sql = "select a.rutcli, a.codcnl, c.deslin, a.codcat, oldlin lista_old from re_cliente_lista_linea a, ma_listaprecio b, ma_lineapr c 
                    where a.codemp = 3
                      and a.codemp = b.codemp
                      and a.codcnl = b.codcnl
                      and a.codcnl = " & cmbListas.SelectedValue & "
                      and b.activa = 'S'
                      and b.actualiza = 'S'
                      and a.codlin = c.codlin "
            If cmbLinea.SelectedValue <> 0 Then
                sql = sql & " and a.codlin = " & cmbLinea.SelectedValue
            End If

            tablaListas = bd.sqlSelect(sql)

            dgvClientesxListas.DataSource = tablaListas
            dgvClientesxListas.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells

            lblcargando.Visible = False

        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            lblcargando.Visible = False
        End Try
    End Sub

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        BuscaClientes()
    End Sub

    Private Sub btnMover_Click(sender As Object, e As EventArgs) Handles btnMover.Click
        Using con = New OracleConnection(Conexion.retornaConexion)
            con.Open()
            Dim comando As OracleCommand
            'Dim dataAdapter As OracleDataAdapter
            Dim sql As String
            Dim dt As New DataTable
            Using transs = con.BeginTransaction
                Try
                    lblProcesando.Visible = True
                    Application.DoEvents()

                    If cmbListaNueva.SelectedItem Is Nothing Then
                        MessageBox.Show("Debe expecificar la lista de destino", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Exit Sub
                    End If

                    For i = 0 To dgvClientesxListas.Rows.Count - 1

                        sql = "UPDATE re_cliente_lista_linea set codcnl = " & cmbListaNueva.SelectedItem & ", newlin = " & cmbListaNueva.SelectedValue & ", oldlin = " & cmbListas.SelectedValue & "
                        WHERE codcnl = " & cmbListas.SelectedValue & "
                          and codemp = 3 
                          and rutcli = " & dgvClientesxListas.Item("rutcli", 0).Value
                        If cmbLinea.SelectedValue <> 0 Then
                            sql = sql & " and codlin = " & cmbLinea.SelectedValue
                        End If
                        comando = New OracleCommand(sql, con, transs)
                        comando.ExecuteNonQuery()

                        tablaNewListas = tablaListas.Copy
                        tablaNewListas.Rows.Clear()

                        tablaNewListas.Rows.Add(dgvClientesxListas.Item("rutcli", i).Value,
                                                cmbListaNueva.SelectedValue,
                                                dgvClientesxListas.Item("deslin", i).Value,
                                                dgvClientesxListas.Item("codcat", i).Value,
                                                cmbListas.SelectedValue)

                        'transs.Commit()

                    Next

                    MessageBox.Show("Clientes movidos correctamente", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)

                    btnBuscar.PerformClick()

                Catch ex As Exception
                    MessageBox.Show(ex.ToString, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Finally
                    con.Close()
                    lblProcesando.Visible = False
                End Try
            End Using
        End Using
    End Sub
End Class