﻿Public Class Menu
    Dim bd As New Conexion

    Private Sub ConveniosLicitacionesNuevasToolStripMenuItem_Click(sender As Object, e As EventArgs)
        Dim frmprodalt As New frmPropuestaNueva
        frmprodalt.Show()
    End Sub

    Private Sub SalirToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SalirToolStripMenuItem.Click
        Application.Exit()
    End Sub

    Private Sub Menu_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.WindowState = FormWindowState.Maximized

        Me.Panel1.Width = Me.Size.Width - 50
        Me.Panel1.Height = Me.Height - 150
        If Globales.user = "ALOPEZG" Then
            SETUDToolStripMenuItem.Visible = True
            btnPantallas.Visible = True
            btnColor.Visible = True
        Else
            SETUDToolStripMenuItem.Visible = False
        End If

        If Globales.detDerUsu(Globales.user, 140) = False Then
            CargaMercadoPublicoToolStripMenuItem.Enabled = False
            CargaOfertasMercadoPublicoToolStripMenuItem.Enabled = False
        End If

        If Globales.user = "KSERRANO" Then
            btnPantallas.Visible = True
        End If
        PuntoControl.RegistroUso("241")
    End Sub

    Private Sub CargaPotoPad()
        bd.open_dimerc()
        Dim dt1 As New DataTable
        Dim tipoven As Int32 = 0

        Try
            Dim Sql = "select count(b.codpro) valor from en_notavta a, de_notavta b, ma_product c
                        where a.codemp = b.codemp
                          and a.numnvt = b.numnvt
                          and b.codpro = c.codpro
                          and b.cosprom is null
                          and trunc(a.fecha_creac) = trunc(sysdate)"
            dt1.Clear()
            'dt1 = bd.sqlSelect(Sql)

            If dt1.Rows.Count > 0 Then
                'PotoPad("Existen " & dt1.Rows(0).Item("valor") & " Notas de ventas Sin Costo Promedio")
            End If

        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            bd.close()
        End Try
    End Sub
    Private Sub PotoPad(mensaje As String)
        Dim slice As New ToastForm(5000, mensaje)
        slice.Height = 150
        slice.Show()
    End Sub

    Private Sub Menu_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        Application.Exit()
    End Sub

    Private Sub ProductosAlternativosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ProductosAlternativosToolStripMenuItem.Click
        If Globales.detDerUsu(Globales.user, 144) = True Then
            Dim frmprodalt As New frmProductoAlternativo
            frmprodalt.Show()
        End If
    End Sub

    Private Sub ConsultarRelacionesPorRutToolStripMenuItem_Click(sender As Object, e As EventArgs)
        Dim frmconsulta As New frmConsultaRutRelacionado
        frmconsulta.Show()
    End Sub

    Private Sub ReajusteAutomáticoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ReajusteAutomáticoToolStripMenuItem.Click
        If Globales.detDerUsu(Globales.user, 161) = True Then
            Dim frmconsulta As New frmReajusteAutomatico
            frmconsulta.Show()
        End If
    End Sub

    Private Sub ReajustePorLíneaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ReajustePorLíneaToolStripMenuItem.Click
        If Globales.detDerUsu(Globales.user, 162) = True Then
            Dim frmconsulta As New frmReajusteLinea
            frmconsulta.Show()
        End If
    End Sub

    Private Sub ConsultaReajustePorClienteToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ConsultaReajustePorClienteToolStripMenuItem.Click
        If Globales.detDerUsu(Globales.user, 163) = True Then
            Dim frmconsulta As New frmApruebaConvenioCorreo
            frmconsulta.Show()
        End If
    End Sub

    Private Sub ConsultaProximosReajustesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ConsultaProximosReajustesToolStripMenuItem.Click
        If Globales.detDerUsu(Globales.user, 154) = True Then
            Dim frmconsulta As New frmConsultaProxReajustesconVenta
            frmconsulta.Show()
        End If
    End Sub

    Private Sub CostoFuturoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CostoFuturoToolStripMenuItem.Click
        Dim frmconsulta As New frmCostoFuturo
        frmconsulta.Show()
    End Sub

    Private Sub FichaClienteToolStripMenuItem_Click_1(sender As Object, e As EventArgs) Handles FichaClienteToolStripMenuItem.Click
        Dim frmconsulta As New frmFichaCliente
        frmconsulta.Show()
    End Sub

    Private Sub ConveniosLicitacionesNuevasToolStripMenuItem_Click_1(sender As Object, e As EventArgs) Handles ConveniosLicitacionesNuevasToolStripMenuItem.Click
        If Globales.detDerUsu(Globales.user, 160) = True Then
            Dim frmconsulta As New frmPropuestaNueva
            frmconsulta.Show()
        End If
    End Sub

    Private Sub ConsultarRelacionesPorRutToolStripMenuItem_Click_1(sender As Object, e As EventArgs) Handles ConsultarRelacionesPorRutToolStripMenuItem.Click
        Dim frmconsulta As New frmConsultaRutRelacionado
        frmconsulta.Show()
    End Sub

    Private Sub ConsultaReajustesPorDiaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ConsultaReajustesPorDiaToolStripMenuItem.Click
        Dim frmconsulta As New frmConsultaReajustesporDia
        frmconsulta.Show()
    End Sub

    Private Sub EditarConveniosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles EditarConveniosToolStripMenuItem.Click
        If Globales.detDerUsu(Globales.user, 145) = True Then
            Dim frmconsulta As New frmEditarConvenios
            frmconsulta.Show()
        End If
    End Sub

    Private Sub ReversarPropuestasToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ReversarPropuestasToolStripMenuItem.Click
        If Globales.detDerUsu(Globales.user, 146) = True Then
            Dim frmconsulta As New frmReversaPropuestas
            frmconsulta.Show()
        End If
    End Sub

    Private Sub CostoEspecialToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CostoEspecialToolStripMenuItem.Click
        If Globales.detDerUsu(Globales.user, 147) = True Then
            Dim frmCosto As New frmCostoEspecial
            frmCosto.Show()
        End If
    End Sub

    Private Sub FichaProductoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles FichaProductoToolStripMenuItem.Click
        If Globales.detDerUsu(Globales.user, 143) = True Then
            Dim frmFP As New frmFichaProducto
            frmFP.Show()
        End If
    End Sub

    Private Sub btnFichaProducto_Click(sender As Object, e As EventArgs) Handles btnFichaProducto.Click
        If Globales.detDerUsu(Globales.user, 143) = True Then
            Dim frmFP As New frmFichaProducto
            frmFP.Show()
        End If
    End Sub

    Private Sub ConsultaMaestraProductosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ConsultaMaestraProductosToolStripMenuItem.Click
        Dim frmMP As New frmMaestraProductos
        frmMP.Show()
    End Sub

    Private Sub btnFichaProducto_MouseHover(sender As Object, e As EventArgs) Handles btnFichaProducto.MouseHover
        'primer evento
        'btnFichaProducto.Padding = (5.0)
    End Sub

    Private Sub SETUDToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SETUDToolStripMenuItem.Click
        Dim frmSetud As New frmSetud
        frmSetud.Show()
    End Sub

    Private Sub CopiaConveniosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CopiaConveniosToolStripMenuItem.Click
        If Globales.detDerUsu(Globales.user, 164) = True Then
            Dim frmCopiaConvenios As New frmCopiaConvenios
            frmCopiaConvenios.Show()
        End If
    End Sub

    Private Sub AuditoriaProductosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AuditoriaProductosToolStripMenuItem.Click
        Dim frmProductosValidacion As New frmAuditoriaProductos
        frmProductosValidacion.Show()
    End Sub

    Private Sub MotorDeBusquedaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MotorDeBusquedaToolStripMenuItem.Click
        System.Diagnostics.Process.Start("https://cse.google.com/cse?cx=000339134946577956807:tpdvaijohg8")
    End Sub

    Private Sub ReportesVariosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ReportesVariosToolStripMenuItem.Click
        Dim frmReportesVarios As New frmReportesVarios
        frmReportesVarios.Show()
    End Sub

    Private Sub ReporteConveniosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ReporteConveniosToolStripMenuItem.Click
        Dim frmReporteMasivoConvenio As New frmReporteMasivoConvenio
        frmReporteMasivoConvenio.Show()
    End Sub

    Private Sub CargaMercadoPublicoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CargaMercadoPublicoToolStripMenuItem.Click
        Dim frmCargaMercadoPublico As New frmCargaMercadoPublico
        frmCargaMercadoPublico.Show()
    End Sub

    Private Sub CargaOfertasMercadoPublicoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CargaOfertasMercadoPublicoToolStripMenuItem.Click
        Dim frmCargaOferton As New frmCargaOferton
        frmCargaOferton.Show()
    End Sub

    Private Sub ProductosMercadoPublicoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ProductosMercadoPublicoToolStripMenuItem.Click
        Dim frmDescargaMercadoPublico As New frmDescargaMercadoPublico
        frmDescargaMercadoPublico.Show()
    End Sub

    Private Sub ElasticidadToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ElasticidadToolStripMenuItem.Click
        Dim Elasticidad As New Elasticidad
        Elasticidad.Show()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnPantallas.Click
        Dim frmPantallas As New frmPantallas
        frmPantallas.Show()
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        CargaPotoPad()
    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles btnColor.Click
        If Globales.tema = 1 Then
            Me.Theme = 2
            Globales.tema = 2
        Else
            Me.Theme = 1
            Globales.tema = 1
        End If
        Me.WindowState = 2
        Application.DoEvents()
    End Sub

    Private Sub CargaMasivaCostoEspecialToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CargaMasivaCostoEspecialToolStripMenuItem.Click
        If Globales.detDerUsu(Globales.user, 148) = True Then
            Dim frmCargaMasivaCostoEspecial As New frmCargaMasivaCostoEspecial
            frmCargaMasivaCostoEspecial.Show()
        End If
    End Sub

    Private Sub MantenedorLIstasDePreciosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MantenedorLIstasDePreciosToolStripMenuItem.Click
        If Globales.detDerUsu(Globales.user, 149) = True Then
            Dim MantenedorListasPrecios As New MantenedorListasPrecios
            MantenedorListasPrecios.Show()
        End If
    End Sub

    Private Sub Button1_Click_2(sender As Object, e As EventArgs) Handles Button1.Click
        Dim frmClientesxLista As New frmClientesxLista
        frmClientesxLista.Show()
    End Sub

    Private Sub ReporteVentaTissueToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ReporteVentaTissueToolStripMenuItem.Click
        Dim frmReporteVentaTissue As New frmVentaTissue
        frmReporteVentaTissue.Show()
    End Sub

    Private Sub AtributosTissueToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AtributosTissueToolStripMenuItem.Click
        Dim frmTissue As New AtribTissue
        frmTissue.Show()
    End Sub

    Private Sub CargaMaestraToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles CargaMaestraToolStripMenuItem1.Click
        Dim frmCargaMaestra As New frmCargaMaestra
        frmCargaMaestra.Show()
    End Sub

    Private Sub CargaCompetenciaToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles CargaCompetenciaToolStripMenuItem1.Click
        Dim frmCargaCompetencia As New frmCargaCompetencia
        frmCargaCompetencia.Show()
    End Sub

    Private Sub DescargarActualesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DescargarActualesToolStripMenuItem.Click
        Dim datos As New Conexion
        Dim sql = "SELECT CODPRO, CODPRO_ALT CODIGO_COMPETENCIA,
                   RUTCOMPETIDOR, SAP_GET_CLIENTE(3,RUTCOMPETIDOR) COMPETIDOR, MULTIPLO 
                   FROM RE_PRODUCTO_NEGOCIO WHERE RUTCOMPETIDOR IN (96556940, 78885550, 96439000, 84358900) AND TIPO = 'CT'"
        datos.open_dimerc()
        Dim tabla = datos.sqlSelect(sql)
        Dim dgvDatos = New DataGridView
        dgvDatos.DataSource = tabla
        Globales.DataTableToExcel(tabla, dgvDatos)
    End Sub

    Private Sub CargaMasivaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CargaMasivaToolStripMenuItem.Click
        MsgBox("Recuerda que los datos de competidores para el scraping
                serán borrados y reemplazados con lo que cargues desde este Excel.
                Para continuar, en el siguiente cuadro selecciona el archivo.
                Debe tener CODPRO, CODIGO_COMPETENCIA, RUTCOMPETIDOR, MULTIPLO
                Da igual el orden o si hay más columnas pero deben estar al menos estas columnas con esos nombres.
                CODPRO es el código Dimerc, CODPRO_ALT es el código de la competencia")
        Dim OpenFileDialog1 As New OpenFileDialog
        If OpenFileDialog1.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            Dim dgvDatos = New DataGridView
            Dim datos = New Conexion()
            Try
                datos.open_dimercFull()
                Globales.Importar(dgvDatos, OpenFileDialog1.FileName)
                datos.ejecutar("DELETE FROM DM_VENTAS.RE_PRODUCTO_NEGOCIO
                               WHERE RUTCOMPETIDOR IN (96556940, 78885550, 96439000, 84358900)
                               AND TIPO = 'CT'")
                Controls.Add(dgvDatos)
                dgvDatos.Visible = False
                Dim car As New frmCargando

                car.ProgressBar1.Minimum = 0
                car.ProgressBar1.Value = 0
                car.ProgressBar1.Maximum = dgvDatos.RowCount
                car.StartPosition = FormStartPosition.CenterScreen
                car.Text = ""
                car.Label1.Visible = True
                car.BringToFront()
                car.Show()

                For Each fila As DataGridViewRow In dgvDatos.Rows
                    If fila.Cells.Item(dgvDatos.Columns("CODPRO").Index).Value <> "" Then
                        car.ProgressBar1.Value += 1
                        'car.Label1.Text = "Cargando " & car.ProgressBar1.Value & " de " & dgvDatos.RowCount - 1
                        datos.ejecutar("INSERT INTO DM_VENTAS.RE_PRODUCTO_NEGOCIO(CODPRO,CODPRO_ALT, RUTCOMPETIDOR, MULTIPLO, TIPO)
                                    VALUES('" & fila.Cells.Item(dgvDatos.Columns("CODPRO").Index).Value & "', '" &
                                                fila.Cells.Item(dgvDatos.Columns("CODIGO_COMPETENCIA").Index).Value & "',
                                            " & fila.Cells.Item(dgvDatos.Columns("RUTCOMPETIDOR").Index).Value &
                                            ", " & fila.Cells.Item(dgvDatos.Columns("MULTIPLO").Index).Value & ",'CT')")
                    End If
                Next
                car.Close()
                MsgBox("Datos cargados exitosamente")

            Catch ex As Exception
                MsgBox(ex.ToString())
            Finally
                datos.close()
            End Try

        End If
    End Sub

    Private Sub RicardoRodriguezToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RicardoRodriguezToolStripMenuItem.Click
        Dim correrScraping As New frmCorrerScraping(2)
        correrScraping.Show()
    End Sub

    Private Sub PrisaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PrisaToolStripMenuItem.Click
        Dim correrScraping As New frmCorrerScraping(1)
        correrScraping.Show()
    End Sub

    Private Sub PCFactoryToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PCFactoryToolStripMenuItem.Click
        Dim correrScraping As New frmCorrerScraping(3)
        correrScraping.Show()
    End Sub

    Private Sub LiderToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles LiderToolStripMenuItem.Click
        Dim correrScraping As New frmCorrerScraping(4)
        correrScraping.Show()
    End Sub

    Private Sub JumboToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles JumboToolStripMenuItem.Click
        Dim correrScraping As New frmCorrerScraping(6)
        correrScraping.Show()
    End Sub

    Private Sub RedOfficeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RedOfficeToolStripMenuItem.Click
        Dim correrScraping As New frmCorrerScraping(7)
        correrScraping.Show()
    End Sub

    Private Sub LapizLopezToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles LapizLopezToolStripMenuItem.Click
        Dim correrScraping As New frmCorrerScraping(8)
        correrScraping.Show()
    End Sub

    Private Sub DescargarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DescargarToolStripMenuItem.Click

    End Sub

    Private Sub CorrerFlexibleToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CorrerFlexibleToolStripMenuItem.Click
        Dim formCorrerFlexible As New frmCorrerFlexible
        formCorrerFlexible.Show()
    End Sub

    Private Sub DiarioToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DiarioToolStripMenuItem.Click
        Dim datos As New ConexionMySQL()
        datos.open(ConexionMySQL.conn_competencias)
        Dim tabla = datos.sqlSelect("SELECT c.competencia,a.rutcli,a.razonsocial,a.codigo_competencia, b.codigo_dimerc,
                                    a.precio_competencia, a.precio_dimerc
                                    FROM informacion_competencia a, productos_competencia b, competencias c
                                    WHERE a.id_competencia = b.id_competencia
                                    AND a.codigo_competencia = b.codigo_competencia
                                    AND a.id_competencia = c.id
                                    AND a.fecha_extraccion>='" & Format(Now, "yyyy-MM-dd") & "'")
        Dim dgvDatos = New DataGridView
        dgvDatos.DataSource = tabla
        Globales.DataTableToExcel(tabla, dgvDatos)
    End Sub

    Private Sub APedidoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles APedidoToolStripMenuItem.Click
        Dim datos As New ConexionMySQL()
        datos.open(ConexionMySQL.conn_competencias)
        Dim tabla = datos.sqlSelect("SELECT c.competencia,a.rutcli,a.razonsocial,a.codigo_competencia, b.codigo_dimerc,
                                    a.precio_competencia, a.precio_dimerc
                                    FROM informacion_competencia_ondemand a, productos_competencia b, competencias c
                                    WHERE a.id_competencia = b.id_competencia
                                    AND a.codigo_competencia = b.codigo_competencia
                                    AND a.id_competencia = c.id
                                    AND a.fecha_extraccion>='" & Format(Now, "yyyy-MM-dd") & "'")
        Dim dgvDatos = New DataGridView
        dgvDatos.DataSource = tabla
        Globales.DataTableToExcel(tabla, dgvDatos)
    End Sub
End Class
