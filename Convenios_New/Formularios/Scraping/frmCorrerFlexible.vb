﻿Public Class frmCorrerFlexible
    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbCompetencia.SelectedIndexChanged
        If cmbCompetencia.SelectedIndex <> 4 Then
            PuntoControl.RegistroUso("231")
            Dim datos As New ConexionMySQL
            datos.open(ConexionMySQL.conn_competencias)
            Dim tabla = datos.sqlSelect("SELECT rutcli from usuarios_competencia
                                         where id_competencia = " & cmbCompetencia.SelectedIndex + 1)
            dgvClientes.Rows.Clear()
            For Each fila As DataRow In tabla.Rows
                dgvClientes.Rows.Add(False, fila(0).ToString)
            Next
        End If
    End Sub

    Private Sub btnCorrer_Click(sender As Object, e As EventArgs) Handles btnCorrer.Click
        PuntoControl.RegistroUso("232")
        Dim clientes As New List(Of String)
        Dim productos As New List(Of String)
        For Each fila As DataGridViewRow In dgvClientes.Rows
            If fila.Cells()(0).Value = True Then
                clientes.Add(fila.Cells()(1).Value)
            End If
        Next
        For Each fila As DataGridViewRow In dgvProductos.Rows
            If fila.Cells()(0).Value <> "" Then
                productos.Add(fila.Cells()(0).Value & Chr(9) & "" & Chr(9)) 'fila.Cells()(1).Value & Chr(9) & fila.Cells()(2).Value)
            End If
        Next
        Dim formCorrerScraping As New frmCorrerScraping(cmbCompetencia.SelectedIndex + 1, clientes, productos)
        formCorrerScraping.Show()
    End Sub

    Private Sub frmCorrerFlexible_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        PuntoControl.RegistroUso("230")

    Private Sub dgvProductos_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvProductos.CellContentClick

    End Sub
End Class