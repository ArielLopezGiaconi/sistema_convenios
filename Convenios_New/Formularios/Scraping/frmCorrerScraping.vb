﻿Imports System.Net
Imports System.Reflection

Public Class frmCorrerScraping
    Dim idCompetencia As Integer = 0
    Dim ruts As List(Of String)
    Dim productos As List(Of String)
    Public Sub New(idCompetencia As Integer)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        Me.idCompetencia = idCompetencia
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub
    Public Sub New(idCompetencia As Integer, ruts As List(Of String), productos As List(Of String))

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        Me.idCompetencia = idCompetencia
        Me.ruts = ruts
        Me.productos = productos
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
    End Sub
    Private Sub frmCorrerScraping_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        If idCompetencia <> 0 Then
            PuntoControl.RegistroUso("233")
            Dim datos As New ConexionMySQL()
            Dim params As String = ""
            Dim json As String
            datos.open(ConexionMySQL.conn_competencias)
            datos.ejecutar("delete FROM informacion_competencia_ondemand
                            WHERE id_competencia = " & idCompetencia & "
                            And fecha_extraccion>='" & Format(Now, "yyyy-MM-dd") & "'")
            'datos.ejecutar("UPDATE usuarios_competencia
            '               set fecha_ultima_extraccion = '" & Format(Now.AddDays(-1), "yyyy-MM-dd") & "'
            '               where id_competencia = " & idCompetencia & "
            '               and activo = 1")
            If Not IsNothing(Me.ruts) Then
                json = "?params={""ruts"":["
                For Each rut In ruts
                    json = json & "{""rut"":""" & rut & """},"
                Next
                json = json.Trim(",")
                json = json & "],"
                json = json & """productos"":["
                For Each producto In productos
                    Dim prod = producto.Split(Chr(9))
                    json = json & "{""id"":""0"",
                                    ""codigo_competencia"":""" & prod(1) & """,
                                    ""codigo_dimerc"":""" & prod(0) & """,
                                    ""multiplo_venta_dimerc"": """ & prod(2) & """,
                                    ""url"":""-1""},"
                Next
                json = json.Trim(",")
                json = json & "]}"
                params = params & json
            End If
            Dim urls = New String() {"http://192.168.10.58:8080",
                                  "http://192.168.10.51/competencia/scrapingRodriguez",
                                  "http://192.168.10.51/competencia/scrapingPcFactory",
                                  "http://192.168.10.51/competencia/scrapingLider",
                                  "",
                                  "http://192.168.10.51/competencia/scrapingJumbo",
                                  "http://192.168.10.51/competencia/scrapingRedOffice",
                                  "http://192.168.10.51/competencia/scrapingLapizLopez"
                                  }
            Dim url As String = urls(idCompetencia - 1) & params
            Dim fr As System.Net.HttpWebRequest
            Dim targetURI As New Uri(url)
            Try
                fr = DirectCast(HttpWebRequest.Create(targetURI), System.Net.HttpWebRequest)

                SetAllowUnsafeHeaderParsing20()
                Dim response As System.Net.HttpWebResponse = fr.GetResponse()
                If (response.StatusDescription.Equals("OK")) Then
                    Dim str As New System.IO.StreamReader(response.GetResponseStream())
                    txtConsola.Text = str.ReadToEnd()
                    str.Close()
                End If
            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try
        End If
    End Sub

    Private Sub SetAllowUnsafeHeaderParsing20()
        Dim a As New System.Net.Configuration.SettingsSection
        Dim aNetAssembly As System.Reflection.Assembly = Assembly.GetAssembly(a.GetType)
        Dim aSettingsType As Type = aNetAssembly.GetType("System.Net.Configuration.SettingsSectionInternal")
        Dim args As Object()
        Dim anInstance As Object = aSettingsType.InvokeMember("Section", BindingFlags.Static Or BindingFlags.GetProperty Or BindingFlags.NonPublic, Nothing, Nothing, args)
        Dim aUseUnsafeHeaderParsing As FieldInfo = aSettingsType.GetField("useUnsafeHeaderParsing", BindingFlags.NonPublic Or BindingFlags.Instance)
        aUseUnsafeHeaderParsing.SetValue(anInstance, True)
    End Sub
	
End Class