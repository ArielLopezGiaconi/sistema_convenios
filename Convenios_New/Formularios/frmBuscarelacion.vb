﻿Public Class frmBuscarelacion
    Dim bd As New Conexion
    Dim datos As String
    Dim PuntoControl As New PuntoControl
    Private Sub frmBuscaCliente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Location = New Point(100, 85)
        buscar()
        PuntoControl.RegistroUso("236")
    End Sub
    Public Sub New(dato As String)
        ' Llamada necesaria para el diseñador.
        InitializeComponent()
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        datos = dato
    End Sub
    Private Sub buscar()
        Try
            bd.open_dimerc()
            Dim Sql = " select a.rutcli ""Rut"",getrazonsocial(3,a.rutcli) ""Razon Social"",nvl(b.observacion,'SIN REAJUSTES') ""Observacion"",trunc(b.fecha_creac) ""Fecha"" from  "
            Sql = Sql & " ( "
            Sql = Sql & " SELECT   c.rutcli ,getrazonsocial(3,c.rutcli) razons,nvl(max(b.num_reajuste),'') num_reajuste "
            Sql = Sql & " FROM en_reajuste_convenio b,re_emprela c     "
            Sql = Sql & " WHERE b.codemp(+)=c.codemp    "
            Sql = Sql & " and b.rutcli(+)=c.rutcli  "
            Sql = Sql & " and c.numrel= " + datos
            Sql = Sql & " group by c.rutcli,getrazonsocial(3,c.rutcli)  "
            Sql = Sql & " ) a, "
            Sql = Sql & " en_reajuste_convenio b "
            Sql = Sql & " where b.codemp(+)=3 "
            Sql = Sql & " and a.num_reajuste=b.num_reajuste(+) "
            Sql = Sql & " order by ""Fecha"" asc "

            dgvDatos.DataSource = bd.sqlSelect(SQL)
            dgvDatos.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            dgvDatos.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dgvDatos.Columns("Rut").Width = 150
            dgvDatos.Columns("Rut").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvDatos.Columns("Razon Social").Width = 480
            dgvDatos.Columns("Razon Social").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub frmBuscaCliente_KeyPress(sender As Object, e As KeyPressEventArgs) Handles MyBase.KeyPress
        Close()
    End Sub

    Private Sub frmBuscaCliente_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.Handled = True
            Me.Close()
        End If
    End Sub

End Class