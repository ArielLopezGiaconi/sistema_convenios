﻿Public Class creaCostoEsp
    Public rowid As String
    Public Sub New()

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    Public Sub New(rowid As Boolean)
        Me.rowid = rowid
        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        If txtRutcli.Text.Length > 0 And txtCodpro.Text.Length > 0 And txtCosto.Text.Length > 0 And txtPrecio.Text.Length > 0 Then
            Dim datos As New Conexion
            Dim sql As String
            Dim caducar As Boolean
            Dim respuesta As New MsgBoxResult
            datos.open_dimerc()
            PuntoControl.RegistroUso("135")
            sql = "SELECT 1 FROM EN_CLIENTE_COSTO
                                           WHERE to_date('" & dtDFecini.Value.ToShortDateString & "','dd/mm/yyyy')
                                           BETWEEN FECINI AND FECFIN
                                           AND CODPRO = '" & txtCodpro.Text & "'
                                           AND RUTCLI = " & txtRutcli.Text
            Dim tablazo = datos.sqlSelect(sql)
            If tablazo.Rows.Count > 0 Then
                respuesta = MsgBox("Hay ofertas vigentes para período seleccionado. ¿Caducar?", vbYesNo, "Confirmar")
                If respuesta = vbYes Then
                    caducar = True
                Else
                    MsgBox("Operación cancelada")
                    Exit Sub
                End If
            End If

            grabaCosto(txtRutcli.Text, caducar)
            MsgBox("Datos ingresados exitosamente")
        End If
    End Sub

    Private Sub creaCostoEsp_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        Dim datos As New Conexion
        Dim sql As String
        sql = "SELECT CODVAL, DESVAL FROM DE_DOMINIO
               WHERE CODDOM = 401"
        datos.open_dimerc()
        Dim tabla = datos.sqlSelect(sql)
        For Each fila As DataRow In tabla.Rows
            Dim tipo As New Tipo
            tipo.desval = fila(1).ToString
            tipo.codval = fila(0).ToString
            cmbTipo.Items.Add(tipo)
        Next
        cmbTipo.SelectedIndex = 0
        txtCodpro.Select()
    End Sub

    Private Sub txtCodpro_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCodpro.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) And txtCodpro.Text.Length > 0 Then
            Dim datos As New Conexion
            datos.open_dimerc()
            lblDespro.Text = datos.sqlSelectUnico("SELECT SUBSTR(SAP_GET_DESCRIPCION('" & txtCodpro.Text & "'),1,30) DESPRO FROM DUAL", "DESPRO")
            lblCoscom.Text = datos.sqlSelectUnico("SELECT SAP_GET_COSTOCOMERCIAL(3,1,'" & txtCodpro.Text & "') COSCOM FROM DUAL", "COSCOM")
            lblCosprom.Text = datos.sqlSelectUnico("SELECT SAP_GET_COSPROM_UNICO('" & txtCodpro.Text & "') COSPROM FROM DUAL", "COSPROM")
            lblStock.Text = datos.sqlSelectUnico("SELECT stocks - get_stkcom(codemp,codpro,codbod) STOCK from re_bodprod_dimerc
                                                             WHERE CODEMP = 3
                                                             AND CODBOD = 1
                                                             AND CODPRO = '" & txtCodpro.Text & "'", "STOCK")
            lblEstado.Text = datos.sqlSelectUnico("SELECT sap_get_dominio(20,ESTPRO) ESTPRO
                                                   FROM MA_PRODUCT
                                                   WHERE CODPRO = '" & txtCodpro.Text & "'", "ESTPRO")
            txtPrecio.Select()
            datos.close()
        End If
    End Sub

    Private Sub txtRutcli_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtRutcli.KeyPress
        Globales.solonumerosyEnter(e)
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            txtCodpro.Select()
        End If
    End Sub

    Private Sub txtPrecio_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPrecio.KeyPress
        Globales.solonumerosyEnter(e)
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            txtCosto.Select()
        End If
    End Sub

    Private Sub txtCosto_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCosto.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            Dim margen = Math.Round(((CInt(txtPrecio.Text) - CInt(txtCosto.Text)) / CInt(txtPrecio.Text)) * 100, 2).ToString()
            margen = margen.Replace(",", ".")
            lblMargen.Text = margen & "%"
        End If
    End Sub

    Private Sub grabaCosto(rutcli As String, sobreescribir As Boolean)
        Dim datos As New Conexion
        Dim sql As String
        datos.open_dimerc()

        If sobreescribir Then
            sql = "UPDATE EN_CLIENTE_COSTO
                       SET ESTADO = 'C',
                       FECFIN = to_date('" & dtDFecini.Value.ToShortDateString & "','dd/mm/yyyy') - 1
                       WHERE to_date('" & dtDFecini.Value.ToShortDateString & "','dd/mm/yyyy')
                       BETWEEN FECINI AND FECFIN
                       AND CODPRO = '" & txtCodpro.Text & "'
                       AND RUTCLI = " & rutcli
            datos.ejecutar(sql)
        End If

        Dim margen = Math.Round(((CInt(txtPrecio.Text) - CInt(txtCosto.Text)) / CInt(txtPrecio.Text)) * 100, 2).ToString()
        margen = margen.Replace(",", ".")
        Dim tipo = cmbTipo.SelectedItem.codval
        datos.open_dimerc()
        sql = String.Format("INSERT INTO EN_CLIENTE_COSTO(USR_CREAC,ID_NUMERO, RUTCLI,CODPRO,PRECIO,COSTO,MARGEN,FECINI,FECFIN,CODEMP,TIPO,CODMON,ESTADO)
                       VALUES(SUBSTR('" & Globales.user & "',1,30),EN_CLIENTE_COSTO_SEQ.nextval, {0},'{1}',{2},{3},{4},to_date('{5}','dd/mm/yyyy'),to_date('{6}','dd/mm/yyyy'),3,{7},0,'V')", rutcli, txtCodpro.Text, txtPrecio.Text, txtCosto.Text, margen,
                                                                           dtDFecini.Value.ToShortDateString, dtFecfin.Value.ToShortDateString, tipo)

        datos.ejecutar(sql)
        datos.close()
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        If txtRutcli.Text.Length > 0 And txtCodpro.Text.Length > 0 And txtCosto.Text.Length > 0 And txtPrecio.Text.Length > 0 Then
            Dim datos As New Conexion
            Dim sql As String
            Dim caducaCostos As Boolean
            Dim respuesta As New MsgBoxResult
            datos.open_dimerc()
            PuntoControl.RegistroUso("136")
            sql = "SELECT 1 FROM EN_CLIENTE_COSTO
                                           WHERE to_date('" & dtDFecini.Value.ToShortDateString & "','dd/mm/yyyy')
                                           BETWEEN FECINI AND FECFIN
                                           AND CODPRO = '" & txtCodpro.Text & "'
                                           AND RUTCLI = " & txtRutcli.Text
            Dim tablazo = datos.sqlSelect(sql)
            If tablazo.Rows.Count > 0 Then
                respuesta = MsgBox("Hay ofertas vigentes para período seleccionado. ¿Caducar?", vbYesNo, "Confirmar")
                If respuesta = vbYes Then
                    caducaCostos = True
                Else
                    MsgBox("Operación cancelada")
                    Exit Sub
                End If
            End If

            grabaCosto(txtRutcli.Text, caducaCostos)

            Dim sobreescribir = MsgBox("Grabado costo para cliente solicitado, ¿caducar costos vigentes en relacionados?", vbYesNo, "Confirmar")

            If sobreescribir = vbYes Then
                caducaCostos = True
            End If

            datos.open_dimerc()

            sql = "SELECT RUTCLI FROM RE_EMPRELA
                   WHERE NUMREL = SAP_GET_NUMREL(3," & txtRutcli.Text & ") 
                   AND RUTCLI <> " & txtRutcli.Text
            tablazo = datos.sqlSelect(sql)

            If tablazo.Rows.Count > 0 Then
                For Each fila As DataRow In tablazo.Rows
                    grabaCosto(fila(0).ToString(), caducaCostos)
                Next

                MsgBox("Grabado costo para relacionados")
            Else
                MsgBox("Cliente no tiene relacionados")
            End If
        Else
            MsgBox("Ingrese todos los datos")
        End If

    End Sub

    Private Sub creaCostoEsp_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Location = New Point(0, 0)
        PuntoControl.RegistroUso("134")
    End Sub
End Class