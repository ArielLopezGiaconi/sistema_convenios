﻿Imports System.Data.OracleClient
Public Class frmAnotaciones
    Dim bd As New Conexion
    Dim cargado As Boolean = False
    Dim PuntoControl As New PuntoControl
    Private Sub btn_Salir_Click(sender As System.Object, e As System.EventArgs) Handles btn_Salir.Click
        Me.Close()
    End Sub

    Public Sub New()

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub
    Public Sub New(codpro As String)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()
        txtCodigo.Text = codpro
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    Private Sub frmAnotaciones_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.StartPosition = FormStartPosition.Manual
        Me.Location = New Point(100, 50)

        PuntoControl.RegistroUso("68")

        Dim dt2 As New DataTable
        dt2.Columns.Add("Texto", GetType(String))
        dt2.Columns.Add("Valor", GetType(String))
        dt2.Rows.Add(Globales.user, Globales.user)
        cmbUserid.DataSource = dt2
        cmbUserid.DisplayMember = "Texto"
        cmbUserid.ValueMember = "Valor"
        cmbUserid.SelectedIndex = 0
        cmbUserid.Refresh()
    End Sub

    Public Sub getDatos()
        Try
            bd.open_dimerc()
            txtDespro.Text = bd.sqlSelectUnico("Select despro FROM ma_product where codpro = '" + txtCodigo.Text + "'", "despro")

            'Dim sql = "select * from tm_product_hst where codpro='" + txtCodigo.Text + "'"
            Dim Sql = "Select fechas ""Fecha"", horass ""Horas"", userid ""Usuario"","
            Sql = Sql & " observ ""Observación"",fecven ""Fecha Vencimiento"",codest  from tm_product_hst "
            Sql = Sql & " where codpro = '" & txtCodigo.Text & "'"
            Sql = Sql & " and fecven >= trunc(sysdate) "
            Sql = Sql & " and codemp = " & Globales.empresa.ToString
            Sql = Sql & " and codest = 0 "
            'Sql = Sql & " order by to_char(fechas,'yyyymmdd'), TO_CHAR(SUBSTR(horass,1,2) || SUBSTR(horass,4,2) || SUBSTR(horass,7,2))  desc"
            Sql = Sql & " order by fecven desc"

            Dim dt As DataTable = bd.sqlSelect(Sql)
            dgvVta.DataSource = dt
            dgvVta.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            If cargado = False Then
                Dim check As New DataGridViewCheckBoxColumn
                check.HeaderText = "Anula"
                check.Name = "Anula"
                dgvVta.Columns.Insert(0, check)
                dgvVta.Columns(0).Visible = True
                dgvVta.Columns("codest").Visible = False

            End If
            For i = 0 To dgvVta.RowCount - 1
                If dgvVta.Item("codest", i).Value = 0 Then
                    dgvVta.Item("Anula", i).Value = 0
                Else
                    dgvVta.Item("Anula", i).Value = 1
                End If
            Next
            dgvVta.Columns("Fecha").Width = 90
            dgvVta.Columns("Horas").Width = 90

            cargado = True
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub txtCodigo_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtCodigo.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Trim(txtCodigo.Text) <> "" Then
                getDatos()
                PuntoControl.RegistroUso("69")
            End If
        End If
    End Sub

    Private Sub actualiza(estado As Boolean, indiceGrilla As Integer)
        Using conn = New OracleConnection(Conexion.retornaConexion)
            conn.Open()
            Dim comando As OracleCommand
            Using transac = conn.BeginTransaction

                Try
                    Dim sql = "Update tm_product_hst set codest = " + IIf(estado = True, "5", "0")
                    sql += " where codpro= '" + txtCodigo.Text + "'"
                    sql += " And fechas='" + dgvVta.Item("Fecha", indiceGrilla).Value + "' "
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transac
                    comando.ExecuteNonQuery()

                    transac.Commit()
                Catch ex As Exception
                    If Not transac.Equals(Nothing) Then
                        transac.Rollback()
                    End If
                    PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                    MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Finally
                    conn.Close()
                End Try
            End Using
        End Using
    End Sub

    Private Sub txtObservacion_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtObservacion.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            dtpFecLla.Focus()
        End If
    End Sub

    Private Sub btn_Grabar_Click(sender As System.Object, e As System.EventArgs) Handles btn_Grabar.Click
        PuntoControl.RegistroUso("70")
        If Trim(txtObservacion.Text) = "" Then
            MessageBox.Show("Debe ingresar la observación para grabar la anotación", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        Else
            grabar()
        End If
    End Sub
    Private Sub grabar()
        Using conn = New OracleConnection(Conexion.retornaConexion)
            conn.Open()
            Dim comando As OracleCommand
            Using transac = conn.BeginTransaction
                Try
                    Dim sql = "insert into tm_product_hst (fechas, horass,"
                    sql = sql & " userid, codpro, codemp, observ ,codest"
                    sql = sql & " ,fecven) values("
                    sql = sql & "to_date('" + Now.Date + "','dd/mm/yyyy'),"
                    sql = sql & "'" & Now.Hour.ToString & ":" + Now.Minute.ToString + ":" + Now.Second.ToString + "',"
                    sql = sql & "'" & Globales.user & "' , "
                    sql = sql & "'" & txtCodigo.Text & "',"
                    sql = sql & "'" & Globales.empresa.ToString & "',"
                    sql = sql & "'" & Mid(txtObservacion.Text, 1, 500).Replace(",", ".") & "','0'"
                    sql = sql & ",to_date('" + dtpFecLla.Value.Date + "','dd/mm/yyyy'))"
                    comando = New OracleCommand(sql, conn, transac)
                    comando.ExecuteNonQuery()

                    transac.Commit()

                    MessageBox.Show("Los datos se han grabado exitosamente", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information)

                    getDatos()
                Catch ex As Exception
                    If Not transac.Equals(Nothing) Then
                        transac.Rollback()
                    End If
                    PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                    MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Finally
                    conn.Close()
                End Try
            End Using
        End Using
    End Sub

    Private Sub frmAnotaciones_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        If txtCodigo.Text <> "" Then
            getDatos()
        End If

    End Sub

    Private Sub dgvVta_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvVta.CellContentClick
        If e.RowIndex <> -1 Then
            If e.ColumnIndex = 0 Then
                If dgvVta.Item("Anula", e.RowIndex).Value = 0 Then
                    dgvVta.Item("Anula", e.RowIndex).Value = 1
                    actualiza(True, dgvVta.CurrentRow.Index)
                Else
                    dgvVta.Item("Anula", e.RowIndex).Value = 0
                    actualiza(False, dgvVta.CurrentRow.Index)
                End If
            End If
        End If
    End Sub

    Private Sub dgvVta_ColumnHeaderMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvVta.ColumnHeaderMouseClick
        For i = 0 To dgvVta.RowCount - 1
            If dgvVta.Item("codest", i).Value = 0 Then
                dgvVta.Item("Anula", i).Value = 0
            Else
                dgvVta.Item("Anula", i).Value = 1
            End If
        Next
    End Sub
End Class