﻿Public Class frmProductosRelacionados
    Dim bd As New Conexion
    Dim sql As String
    Dim PuntoControl As New PuntoControl
    Public Sub New(codpro As String, descrip As String)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        txtCodpro.Text = codpro
        txtDespro.Text = descrip
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim datos As New Conexion
        Dim sql, dato As String
        PuntoControl.RegistroUso("95")
        datos.open_dimerc()
        sql = "DELETE FROM RE_PRODUCTO_NEGOCIO
               WHERE CODPRO = '" & txtCodpro.Text & "'"
        datos.ejecutar(sql)

        '  TIPO PRODUCTO
        '   AL = PRODUCTO ALTERNATIVO
        '   CV = PRODUCTO CONVENIO MARCO
        '   CT = PRODUCTO DE COMPETENCIA
        '   AR = PRODUCTO DE ARRASTRE

        For Each fila As DataGridViewRow In dgvAlternativas.Rows
            sql = "INSERT INTO RE_PRODUCTO_NEGOCIO (CODPRO, CODPRO_ALT,TIPO) VALUES
                   ('" & txtCodpro.Text & "','" & fila.Cells(0).Value.ToString() & "','AL')"
            datos.ejecutar(sql)
        Next
        For Each fila As DataGridViewRow In dgvCompetencia.Rows
            sql = "INSERT INTO RE_PRODUCTO_NEGOCIO(CODPRO, CODPRO_ALT,DESCRIPCION,MARCA, RUTCOMPETIDOR, TIPO) VALUES
                  ('" & txtCodpro.Text & "','" & fila.Cells(0).Value.ToString() & "','" & fila.Cells(1).Value.ToString() & "','" &
                     fila.Cells(2).Value.ToString() & "'," & fila.Cells(3).Value.ToString() & ",'CT')"
            datos.ejecutar(sql)
        Next
        For Each fila As DataGridViewRow In dgvConvenioMarco.Rows
            dato = cmbConveni.SelectedIndex

            sql = "INSERT INTO RE_PRODUCTO_NEGOCIO(CODPRO, CODPRO_ALT, RUTCOMPETIDOR, MULTIPLO, TIPO, DESCRIPCION, MARCA) VALUES
                  ('" & txtCodpro.Text & "','" & fila.Cells(0).Value.ToString() & "'," & dato & "," & fila.Cells(2).Value.ToString() & ",'CV','" & fila.Cells(1).Value.ToString() & "',SAP_GET_MARCA('" & txtCodpro.Text & "'))"
            datos.ejecutar(sql)
        Next
        For Each fila As DataGridViewRow In dgvArrastre.Rows
            sql = "INSERT INTO RE_PRODUCTO_NEGOCIO(CODPRO, CODPRO_ALT, TIPO) VALUES
                  ('" & txtCodpro.Text & "','" & fila.Cells(0).Value.ToString() & "','AR')"
            datos.ejecutar(sql)
        Next
        MsgBox("Datos ingresados exitosamente")
    End Sub

    Private Sub dgvAlternativas_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvAlternativas.CellDoubleClick
        dgvAlternativas.Rows.Remove(dgvAlternativas.CurrentRow)
    End Sub

    Private Sub dgvCompetencia_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvCompetencia.CellDoubleClick
        dgvCompetencia.Rows.Remove(dgvCompetencia.CurrentRow)
    End Sub

    Private Sub dgvConvenioMarco_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvConvenioMarco.CellDoubleClick
        dgvConvenioMarco.Rows.Remove(dgvConvenioMarco.CurrentRow)
    End Sub

    Private Sub dgvArrastre_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvArrastre.CellDoubleClick
        dgvArrastre.Rows.Remove(dgvArrastre.CurrentRow)
    End Sub

    Private Sub btnAgregaAlt_Click(sender As Object, e As EventArgs) Handles btnAgregaAlt.Click
        If txtCodproAlt.Text.Length > 0 Then
            PuntoControl.RegistroUso("91")
            Dim datos As New Conexion
            datos.open_dimerc()
            Dim tablazo = datos.sqlSelect("SELECT CODPRO, SAP_GET_DESCRIPCION(CODPRO) DESCRIP, SAP_GET_MARCA(CODPRO) MARCA,
                                           SAP_GET_DOMINIO(20,SAP_GET_ESTPRO(CODPRO)) ESTADO FROM MA_PRODUCT WHERE CODPRO = '" & txtCodproAlt.Text & "'")
            If tablazo.Rows.Count > 0 Then
                dgvAlternativas.Rows.Add(tablazo.Rows(0)(0).ToString(),
                                         tablazo.Rows(0)(1).ToString(),
                                         tablazo.Rows(0)(2).ToString(),
                                         tablazo.Rows(0)(3).ToString())
                txtCodproAlt.Text = ""
            End If
        End If
    End Sub

    Private Sub btnAgregaCompetidor_Click(sender As Object, e As EventArgs) Handles btnAgregaCompetidor.Click
        If txtCodproComp.Text.Length > 0 And txtRutComp.Text.Length > 0 Then
            PuntoControl.RegistroUso("92")
            dgvCompetencia.Rows.Add(txtCodproComp.Text, txtDescripComp.Text, txtMarcaComp.Text, txtRutComp.Text)
            txtCodproComp.Text = ""
            txtDescripComp.Text = ""
            txtMarcaComp.Text = ""
            txtRutComp.Text = ""
        End If
    End Sub

    Private Sub btnAgregaCMarco_Click(sender As Object, e As EventArgs) Handles btnAgregaCMarco.Click
        If txtCodproCMarco.Text.Length > 0 Then
            PuntoControl.RegistroUso("93")
            Dim datos As New Conexion
            datos.open_dimerc()
            Dim tablazo = datos.sqlSelect("SELECT CODPRO, DESCRIPCION, MARCA, MULTIPLO
                                           FROM RE_PRODUCTO_NEGOCIO WHERE CODPRO_ALT = '" & txtCodproCMarco.Text & "'")

            If tablazo.Rows.Count = 0 Then
                dgvConvenioMarco.Rows.Add(txtCodproCMarco.Text,
                                         txtdescrip.Text,
                                         txtMultiploCMarco.Text,
                                         cmbConveni.SelectedItem)
                txtCodproCMarco.Text = ""
                txtMultiploCMarco.Text = ""
                txtdescrip.Text = ""
            Else
                MessageBox.Show("El codigo ya existe.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        End If
    End Sub

    Private Sub btnAgregaArrastre_Click(sender As Object, e As EventArgs) Handles btnAgregaArrastre.Click
        If txtCodproArrastre.Text.Length > 0 Then
            PuntoControl.RegistroUso("94")
            Dim datos As New Conexion
            datos.open_dimerc()
            Dim tablazo = datos.sqlSelect("SELECT CODPRO, SAP_GET_DESCRIPCION(CODPRO) DESCRIP, SAP_GET_MARCA(CODPRO) MARCA
                                           FROM MA_PRODUCT WHERE CODPRO = '" & txtCodproArrastre.Text & "'")
            If tablazo.Rows.Count > 0 Then
                dgvArrastre.Rows.Add(tablazo.Rows(0)(0).ToString(),
                                         tablazo.Rows(0)(1).ToString(),
                                         tablazo.Rows(0)(2).ToString())
                txtCodproArrastre.Text = ""
            End If
        End If
    End Sub

    Private Sub frmProductosRelacionados_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        Dim datos As New Conexion

        PuntoControl.RegistroUso("90")

        datos.open_dimerc()
        Dim tablazo = datos.sqlSelect("SELECT CODPRO_ALT, DESCRIPCION, 
                                       SAP_GET_DOMINIO(20,SAP_GET_ESTPRO(CODPRO)) ESTADO, MULTIPLO, GETDOMINIO(62,RUTCOMPETIDOR) CONVENIO, TIPO, SAP_GET_MARCA(CODPRO) MARCA
                                       FROM RE_PRODUCTO_NEGOCIO
                                       WHERE CODPRO = '" & txtCodpro.Text & "'
                                       AND TIPO <> 'CT'
                                       ORDER BY TIPO ASC")

        For Each fila As DataRow In tablazo.Rows
            If fila("TIPO").ToString.Equals("AL") Then
                dgvAlternativas.Rows.Add(fila(0), fila(1), fila(2), fila(3))
            End If
            If fila("TIPO").ToString.Equals("CV") Then
                'cmbConveni.SelectedIndex() = Integer.Parse(fila(4)) - 1
                dgvConvenioMarco.Rows.Add(fila(0), fila(1), fila(3), fila(4))
            End If
            If fila("TIPO").ToString.Equals("AR") Then
                dgvArrastre.Rows.Add(fila(0), fila(1), fila(2))
            End If
        Next

        tablazo = datos.sqlSelect("SELECT CODPRO_ALT, DESCRIPCION, MARCA, RUTCOMPETIDOR
                                       FROM RE_PRODUCTO_NEGOCIO
                                       WHERE CODPRO = '" & txtCodpro.Text & "'
                                       AND TIPO = 'CT'
                                       ORDER BY TIPO ASC")
        For Each fila As DataRow In tablazo.Rows
            dgvCompetencia.Rows.Add(fila(0), fila(1), fila(2), fila(3))
        Next

        dgvAlternativas.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
        dgvAlternativas.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvConvenioMarco.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
        dgvConvenioMarco.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvArrastre.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
        dgvArrastre.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvCompetencia.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
        dgvCompetencia.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

    End Sub

    Private Sub frmProductosRelacionados_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        PuntoControl.RegistroUso("89")
        Me.Location = New Point(100, 50)
        cargaCombo()
    End Sub

    Private Sub cargaCombo()
        Try
            bd.open_dimerc()

            sql = "select desval, codval from de_dominio where coddom = '62'"
            Dim dt As DataTable = bd.sqlSelect(Sql)
            cmbConveni.DataSource = dt
            cmbConveni.ValueMember = "desval"
            cmbConveni.DisplayMember = "desval"
            cmbConveni.SelectedIndex = -1
            cmbConveni.Refresh()

        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try

    End Sub

End Class