﻿Public Class frmSubnegocioPri
    Dim campo As Integer
    Public Sub New(ByVal campo As Integer)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        Me.campo = campo
        If campo = 1 Then
            Text = "Proyectos"
        Else
            Text = "Subnegocios"
        End If
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        PuntoControl.RegistroUso("97")
        Dim frmAgregaSubnegocio As New frmAgregaSubnegocio(True, 0, campo)
        frmAgregaSubnegocio.ShowDialog()
    End Sub

    Private Sub frmSubnegocioPri_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        Dim datos As New Conexion()
        datos.open_dimerc()
        Dim tablazo As DataTable
        If campo = 1 Then
            tablazo = datos.sqlSelect("SELECT ID, DESCRIPCION
                                       FROM MA_NEGOCIO_PRICING")
        End If
        If campo = 2 Then
            tablazo = datos.sqlSelect("SELECT ID, DESCRIPCION
                                       FROM MA_NEGOCIO_SUBGRUPO")
        End If
        For Each fila In tablazo.Rows
            grdData.Rows.Add(fila(0).ToString(), fila(1).ToString, "Modificar")
        Next
    End Sub

    Private Sub grdData_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles grdData.CellClick
        If e.ColumnIndex = 2 Then
            Dim id = CLng(grdData.Rows()(e.RowIndex).Cells(0).Value)
            Dim frmAgregaSubnegocio As New frmAgregaSubnegocio(False, id, campo)
            frmAgregaSubnegocio.ShowDialog()
        End If
    End Sub

    Private Sub frmSubnegocioPri_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Location = New Point(150, 50)
        PuntoControl.RegistroUso("96")
    End Sub
End Class