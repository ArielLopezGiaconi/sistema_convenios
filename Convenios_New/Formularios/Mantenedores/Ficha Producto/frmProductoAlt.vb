﻿Public Class frmProductoAlt
    Dim bd As New Conexion
    Dim codpro As String
    Dim PuntoControl As New PuntoControl
    Private Sub frmProductoAlt_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        PuntoControl.RegistroUso("88")
        Me.StartPosition = FormStartPosition.Manual
        Me.Location = New Point(100, 50)
        cargaGrilla()
    End Sub
    Public Sub New(codigo As String)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()
        codpro = codigo
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub
    Private Sub cargaGrilla()
        Try
            bd.open_dimerc()
            Dim Sql = " select "
            Sql = Sql & "  a.codalt  ""Código"", "
            Sql = Sql & "  b.despro  ""Descripción"", "
            Sql = Sql & "  c.desval  ""Unidad"", "
            Sql = Sql & "  x.desmar  ""Marca"", "
            Sql = Sql & "  b.stkfsc  ""Fisico"", "
            Sql = Sql & "  b.stkcom  ""Comprometido"", "
            Sql = Sql & "  b.stkpen  ""Pendiente"", "
            Sql = Sql & "  b.stkase  ""Asegurado"", "
            Sql = Sql & "  b.minimo  ""Minimo"", "
            Sql = Sql & "  b.maximo  ""Máximo"",  'x' FldRow"
            Sql = Sql & " from "
            Sql = Sql & "     re_prodalt a"
            Sql = Sql & "    ,ma_product b"
            Sql = Sql & "    ,ma_marcapr x"
            Sql = Sql & "    ,de_dominio c "
            Sql = Sql & " where "
            Sql = Sql & "     a.codpro = '" & codpro & "'"
            Sql = Sql & " and a.codemp = " & Globales.empresa.ToString
            Sql = Sql & " and b.codpro = a.codalt "
            Sql = Sql & " and x.codmar = b.codmar "
            Sql = Sql & " and c.coddom = '18' and c.codval(+) = to_char(b.coduni)"
            dgvDatos.DataSource = bd.sqlSelect(Sql)
            dgvDatos.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            dgvDatos.Columns("Fisico").DefaultCellStyle.Format = "n0"
            dgvDatos.Columns("Comprometido").DefaultCellStyle.Format = "n0"
            dgvDatos.Columns("Pendiente").DefaultCellStyle.Format = "n0"
            dgvDatos.Columns("Asegurado").DefaultCellStyle.Format = "n0"
            dgvDatos.Columns("Minimo").DefaultCellStyle.Format = "n0"
            dgvDatos.Columns("Máximo").DefaultCellStyle.Format = "n0"

        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub btn_Salir_Click(sender As System.Object, e As System.EventArgs) Handles btn_Salir.Click
        Me.Close()
    End Sub
End Class