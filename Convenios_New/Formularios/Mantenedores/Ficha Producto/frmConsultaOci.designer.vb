﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConsultaOci
    Inherits MetroFramework.Forms.MetroForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btn_Salir = New System.Windows.Forms.Button()
        Me.dgvDeOrd = New System.Windows.Forms.DataGridView()
        Me.dgvReOrd = New System.Windows.Forms.DataGridView()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        CType(Me.dgvDeOrd, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvReOrd, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btn_Salir
        '
        Me.btn_Salir.Image = Global.Convenios_New.My.Resources.Resources.salir
        Me.btn_Salir.Location = New System.Drawing.Point(1016, 1)
        Me.btn_Salir.Name = "btn_Salir"
        Me.btn_Salir.Size = New System.Drawing.Size(40, 40)
        Me.btn_Salir.TabIndex = 0
        Me.btn_Salir.UseVisualStyleBackColor = True
        '
        'dgvDeOrd
        '
        Me.dgvDeOrd.AllowUserToAddRows = False
        Me.dgvDeOrd.AllowUserToDeleteRows = False
        Me.dgvDeOrd.AllowUserToResizeColumns = False
        Me.dgvDeOrd.AllowUserToResizeRows = False
        Me.dgvDeOrd.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDeOrd.Location = New System.Drawing.Point(12, 74)
        Me.dgvDeOrd.Name = "dgvDeOrd"
        Me.dgvDeOrd.ReadOnly = True
        Me.dgvDeOrd.RowHeadersVisible = False
        Me.dgvDeOrd.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDeOrd.Size = New System.Drawing.Size(1033, 176)
        Me.dgvDeOrd.TabIndex = 1
        '
        'dgvReOrd
        '
        Me.dgvReOrd.AllowUserToAddRows = False
        Me.dgvReOrd.AllowUserToDeleteRows = False
        Me.dgvReOrd.AllowUserToResizeColumns = False
        Me.dgvReOrd.AllowUserToResizeRows = False
        Me.dgvReOrd.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvReOrd.Location = New System.Drawing.Point(12, 278)
        Me.dgvReOrd.Name = "dgvReOrd"
        Me.dgvReOrd.ReadOnly = True
        Me.dgvReOrd.RowHeadersVisible = False
        Me.dgvReOrd.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvReOrd.Size = New System.Drawing.Size(1033, 189)
        Me.dgvReOrd.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 54)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(108, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Ordenes de Compras"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(13, 262)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(123, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Ordenes Recepcionadas"
        '
        'frmConsultaOci
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1057, 481)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dgvReOrd)
        Me.Controls.Add(Me.dgvDeOrd)
        Me.Controls.Add(Me.btn_Salir)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmConsultaOci"
        Me.ShowIcon = False
        Me.Text = "Consulta de Compras Internacionales"
        CType(Me.dgvDeOrd, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvReOrd, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btn_Salir As System.Windows.Forms.Button
    Friend WithEvents dgvDeOrd As System.Windows.Forms.DataGridView
    Friend WithEvents dgvReOrd As System.Windows.Forms.DataGridView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
End Class
