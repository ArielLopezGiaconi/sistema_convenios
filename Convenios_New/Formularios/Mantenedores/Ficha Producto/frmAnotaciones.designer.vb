﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAnotaciones
    Inherits MetroFramework.Forms.MetroForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.dtpFecLla = New System.Windows.Forms.DateTimePicker()
        Me.cmbUserid = New System.Windows.Forms.ComboBox()
        Me.txtDespro = New System.Windows.Forms.TextBox()
        Me.txtObservacion = New System.Windows.Forms.TextBox()
        Me.txtCodigo = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btn_Salir = New System.Windows.Forms.Button()
        Me.btn_Grabar = New System.Windows.Forms.Button()
        Me.dgvVta = New System.Windows.Forms.DataGridView()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvVta, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dtpFecLla)
        Me.GroupBox1.Controls.Add(Me.cmbUserid)
        Me.GroupBox1.Controls.Add(Me.txtDespro)
        Me.GroupBox1.Controls.Add(Me.txtObservacion)
        Me.GroupBox1.Controls.Add(Me.txtCodigo)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(13, 54)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(806, 158)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'dtpFecLla
        '
        Me.dtpFecLla.AccessibleDescription = ""
        Me.dtpFecLla.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecLla.Location = New System.Drawing.Point(690, 44)
        Me.dtpFecLla.Name = "dtpFecLla"
        Me.dtpFecLla.Size = New System.Drawing.Size(97, 21)
        Me.dtpFecLla.TabIndex = 3
        '
        'cmbUserid
        '
        Me.cmbUserid.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbUserid.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbUserid.FormattingEnabled = True
        Me.cmbUserid.Location = New System.Drawing.Point(92, 43)
        Me.cmbUserid.Name = "cmbUserid"
        Me.cmbUserid.Size = New System.Drawing.Size(250, 21)
        Me.cmbUserid.TabIndex = 2
        '
        'txtDespro
        '
        Me.txtDespro.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDespro.Location = New System.Drawing.Point(198, 17)
        Me.txtDespro.Name = "txtDespro"
        Me.txtDespro.Size = New System.Drawing.Size(589, 21)
        Me.txtDespro.TabIndex = 1
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Location = New System.Drawing.Point(92, 71)
        Me.txtObservacion.MaxLength = 500
        Me.txtObservacion.Multiline = True
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(695, 73)
        Me.txtObservacion.TabIndex = 1
        '
        'txtCodigo
        '
        Me.txtCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodigo.Location = New System.Drawing.Point(92, 17)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.Size = New System.Drawing.Size(100, 21)
        Me.txtCodigo.TabIndex = 1
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(620, 47)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(64, 13)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Vencimiento"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(7, 46)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(43, 13)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Usuario"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 74)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(67, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Observación"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(7, 20)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(40, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Código"
        '
        'btn_Salir
        '
        Me.btn_Salir.Image = Global.Convenios_New.My.Resources.Resources.salir
        Me.btn_Salir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btn_Salir.Location = New System.Drawing.Point(825, 64)
        Me.btn_Salir.Name = "btn_Salir"
        Me.btn_Salir.Size = New System.Drawing.Size(57, 54)
        Me.btn_Salir.TabIndex = 1
        Me.btn_Salir.Text = "Salir"
        Me.btn_Salir.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btn_Salir.UseVisualStyleBackColor = True
        '
        'btn_Grabar
        '
        Me.btn_Grabar.Image = Global.Convenios_New.My.Resources.Resources.Floppy
        Me.btn_Grabar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btn_Grabar.Location = New System.Drawing.Point(825, 144)
        Me.btn_Grabar.Name = "btn_Grabar"
        Me.btn_Grabar.Size = New System.Drawing.Size(57, 54)
        Me.btn_Grabar.TabIndex = 1
        Me.btn_Grabar.Text = "Agregar"
        Me.btn_Grabar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btn_Grabar.UseVisualStyleBackColor = True
        '
        'dgvVta
        '
        Me.dgvVta.AllowUserToAddRows = False
        Me.dgvVta.AllowUserToDeleteRows = False
        Me.dgvVta.AllowUserToResizeColumns = False
        Me.dgvVta.AllowUserToResizeRows = False
        Me.dgvVta.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvVta.Location = New System.Drawing.Point(13, 218)
        Me.dgvVta.Name = "dgvVta"
        Me.dgvVta.ReadOnly = True
        Me.dgvVta.RowHeadersVisible = False
        Me.dgvVta.Size = New System.Drawing.Size(869, 171)
        Me.dgvVta.TabIndex = 2
        '
        'frmAnotaciones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(890, 402)
        Me.ControlBox = False
        Me.Controls.Add(Me.dgvVta)
        Me.Controls.Add(Me.btn_Grabar)
        Me.Controls.Add(Me.btn_Salir)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmAnotaciones"
        Me.Text = "Anotaciones a Productos"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgvVta, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmbUserid As System.Windows.Forms.ComboBox
    Friend WithEvents txtDespro As System.Windows.Forms.TextBox
    Friend WithEvents txtObservacion As System.Windows.Forms.TextBox
    Friend WithEvents txtCodigo As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btn_Salir As System.Windows.Forms.Button
    Friend WithEvents btn_Grabar As System.Windows.Forms.Button
    Friend WithEvents dtpFecLla As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents dgvVta As DataGridView
End Class
