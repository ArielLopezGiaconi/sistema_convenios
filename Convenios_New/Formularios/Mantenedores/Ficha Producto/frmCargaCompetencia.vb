﻿Imports System.Data.OracleClient

Public Class frmCargaCompetencia
    Dim PuntoControl As New PuntoControl
    Private Sub btnCargar_Click(sender As Object, e As EventArgs) Handles btnCargar.Click
        Dim dialogo = New OpenFileDialog
        dialogo.Filter = "Excel|*.xls"
        Dim ruta As DialogResult = dialogo.ShowDialog()
        If ruta = DialogResult.OK Then
            If dialogo.FileName <> "" Then
                Globales.Importar(dgvProds, dialogo.FileName)
            Else
                MsgBox("Ingrese nombre de archivo")
            End If
        End If
    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Dim datos As New Conexion
        Dim sql As String = ""
        Dim codRota As String
        datos.open_dimerc()
        Using transac = datos.sqlConn.BeginTransaction
            Try
                PuntoControl.RegistroUso("72")
                Using command = New OracleCommand()
                    command.Transaction = transac
                    command.Connection = datos.sqlConn

                    For Each fila As DataGridViewRow In dgvProds.Rows
                        If fila.Cells(0).Value.ToString.Length = 0 Or fila.Cells(1).Value.ToString.Length = 0 Or fila.Cells(2).Value.ToString.Length = 0 Then
                            MsgBox("Revise valor Codpro, codigo competencia y rut competencia")
                            transac.Rollback()
                        End If
                        sql = "SELECT 1 FROM RE_PRODUCTO_NEGOCIO
                               WHERE CODPRO = '" & fila.Cells(0).Value & "'
                               AND CODPRO_ALT = '" & fila.Cells(1).Value & "'
                               AND RUTCOMPETIDOR = " & fila.Cells(2).Value
                        Dim datafiller As OracleDataAdapter = New OracleDataAdapter(command)
                        command.CommandText = sql
                        Dim tablita As New DataTable
                        datafiller.Fill(tablita)
                        If tablita.Rows.Count > 0 Then
                            sql = "UPDATE RE_PRODUCTO_NEGOCIO
                                   SET MULTIPLO = " & fila.Cells(3).Value & ",
                                   TIPO = 'CT',
                                   DESCRIPCION = '" & fila.Cells(5).Value & "',
                                   MARCA = '" & fila.Cells(6).Value & "'
                                   WHERE CODPRO = '" & fila.Cells(0).Value & "'
                                   AND CODPRO_ALT = '" & fila.Cells(1).Value & "'
                                   AND RUTCOMPETIDOR = " & fila.Cells(2).Value
                        Else
                            sql = "INSERT INTO RE_PRODUCTO_NEGOCIO(CODPRO,CODPRO_ALT,RUTCOMPETIDOR,MULTIPLO,TIPO,DESCRIPCION,MARCA)
                                   VALUES('" & fila.Cells(0).Value & "','" & fila.Cells(1).Value & "', " & fila.Cells(2).Value & ",1,'CT', '" & fila.Cells(5).Value & "','" & fila.Cells(6).Value & "')"
                        End If
                        command.CommandText = sql
                        command.ExecuteNonQuery()

                    Next
                    transac.Commit()
                    MsgBox("Datos ingresados exitosamente")
                End Using
            Catch ex As Exception
                PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                transac.Rollback()
            End Try

        End Using
    End Sub

    Private Sub frmCargaCompetencia_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Location = New Point(100, 50)
        PuntoControl.RegistroUso("71")
    End Sub
End Class