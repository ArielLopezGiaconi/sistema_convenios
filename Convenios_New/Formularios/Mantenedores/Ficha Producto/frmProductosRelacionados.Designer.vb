﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmProductosRelacionados
    Inherits MetroFramework.Forms.MetroForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtCodpro = New System.Windows.Forms.TextBox()
        Me.txtDespro = New System.Windows.Forms.TextBox()
        Me.lblCodpro = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.dgvAlternativas = New System.Windows.Forms.DataGridView()
        Me.CODPRO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DESCRIP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MARCAP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ESTADO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvConvenioMarco = New System.Windows.Forms.DataGridView()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.dgvCompetencia = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MARCA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COMPETIDOR = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvArrastre = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DESCRIPCION = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btnAgregaAlt = New System.Windows.Forms.Button()
        Me.txtCodproAlt = New System.Windows.Forms.TextBox()
        Me.txtCodproComp = New System.Windows.Forms.TextBox()
        Me.btnAgregaCompetidor = New System.Windows.Forms.Button()
        Me.btnAgregaCMarco = New System.Windows.Forms.Button()
        Me.txtCodproCMarco = New System.Windows.Forms.TextBox()
        Me.btnAgregaArrastre = New System.Windows.Forms.Button()
        Me.txtCodproArrastre = New System.Windows.Forms.TextBox()
        Me.txtRutComp = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtMultiploCMarco = New System.Windows.Forms.TextBox()
        Me.txtMarcaComp = New System.Windows.Forms.TextBox()
        Me.txtDescripComp = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtdescrip = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.cmbConveni = New System.Windows.Forms.ComboBox()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MULTIPLO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.dgvAlternativas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvConvenioMarco, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvCompetencia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvArrastre, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtCodpro
        '
        Me.txtCodpro.Enabled = False
        Me.txtCodpro.Location = New System.Drawing.Point(51, 54)
        Me.txtCodpro.Name = "txtCodpro"
        Me.txtCodpro.Size = New System.Drawing.Size(100, 20)
        Me.txtCodpro.TabIndex = 0
        '
        'txtDespro
        '
        Me.txtDespro.Enabled = False
        Me.txtDespro.Location = New System.Drawing.Point(51, 80)
        Me.txtDespro.Name = "txtDespro"
        Me.txtDespro.Size = New System.Drawing.Size(409, 20)
        Me.txtDespro.TabIndex = 1
        '
        'lblCodpro
        '
        Me.lblCodpro.AutoSize = True
        Me.lblCodpro.Location = New System.Drawing.Point(6, 57)
        Me.lblCodpro.Name = "lblCodpro"
        Me.lblCodpro.Size = New System.Drawing.Size(41, 13)
        Me.lblCodpro.TabIndex = 2
        Me.lblCodpro.Text = "Codpro"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 86)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(43, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Descrip"
        '
        'dgvAlternativas
        '
        Me.dgvAlternativas.AllowUserToAddRows = False
        Me.dgvAlternativas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAlternativas.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CODPRO, Me.DESCRIP, Me.MARCAP, Me.ESTADO})
        Me.dgvAlternativas.Location = New System.Drawing.Point(10, 151)
        Me.dgvAlternativas.Name = "dgvAlternativas"
        Me.dgvAlternativas.RowHeadersVisible = False
        Me.dgvAlternativas.Size = New System.Drawing.Size(450, 134)
        Me.dgvAlternativas.TabIndex = 4
        '
        'CODPRO
        '
        Me.CODPRO.HeaderText = "CODPRO"
        Me.CODPRO.Name = "CODPRO"
        Me.CODPRO.ReadOnly = True
        '
        'DESCRIP
        '
        Me.DESCRIP.HeaderText = "DESCRIP"
        Me.DESCRIP.Name = "DESCRIP"
        Me.DESCRIP.ReadOnly = True
        '
        'MARCAP
        '
        Me.MARCAP.HeaderText = "MARCA"
        Me.MARCAP.Name = "MARCAP"
        Me.MARCAP.ReadOnly = True
        '
        'ESTADO
        '
        Me.ESTADO.HeaderText = "ESTADO"
        Me.ESTADO.Name = "ESTADO"
        Me.ESTADO.ReadOnly = True
        '
        'dgvConvenioMarco
        '
        Me.dgvConvenioMarco.AllowUserToAddRows = False
        Me.dgvConvenioMarco.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvConvenioMarco.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.MULTIPLO})
        Me.dgvConvenioMarco.Location = New System.Drawing.Point(9, 407)
        Me.dgvConvenioMarco.Name = "dgvConvenioMarco"
        Me.dgvConvenioMarco.RowHeadersVisible = False
        Me.dgvConvenioMarco.Size = New System.Drawing.Size(451, 131)
        Me.dgvConvenioMarco.TabIndex = 5
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Button1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Button1.Location = New System.Drawing.Point(831, 57)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(94, 52)
        Me.Button1.TabIndex = 6
        Me.Button1.Text = "Guardar"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'dgvCompetencia
        '
        Me.dgvCompetencia.AllowUserToAddRows = False
        Me.dgvCompetencia.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCompetencia.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.MARCA, Me.COMPETIDOR})
        Me.dgvCompetencia.Location = New System.Drawing.Point(477, 226)
        Me.dgvCompetencia.Name = "dgvCompetencia"
        Me.dgvCompetencia.RowHeadersVisible = False
        Me.dgvCompetencia.Size = New System.Drawing.Size(448, 134)
        Me.dgvCompetencia.TabIndex = 7
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "CODPRO"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "DESCRIP"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        '
        'MARCA
        '
        Me.MARCA.HeaderText = "MARCA"
        Me.MARCA.Name = "MARCA"
        Me.MARCA.ReadOnly = True
        '
        'COMPETIDOR
        '
        Me.COMPETIDOR.HeaderText = "COMPETIDOR"
        Me.COMPETIDOR.Name = "COMPETIDOR"
        Me.COMPETIDOR.ReadOnly = True
        '
        'dgvArrastre
        '
        Me.dgvArrastre.AllowUserToAddRows = False
        Me.dgvArrastre.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvArrastre.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn6, Me.DESCRIPCION, Me.DataGridViewTextBoxColumn7})
        Me.dgvArrastre.Location = New System.Drawing.Point(477, 406)
        Me.dgvArrastre.Name = "dgvArrastre"
        Me.dgvArrastre.RowHeadersVisible = False
        Me.dgvArrastre.Size = New System.Drawing.Size(448, 131)
        Me.dgvArrastre.TabIndex = 8
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "CODPRO"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        '
        'DESCRIPCION
        '
        Me.DESCRIPCION.HeaderText = "DESCRIPCION"
        Me.DESCRIPCION.Name = "DESCRIPCION"
        Me.DESCRIPCION.ReadOnly = True
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "MARCA"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 109)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(62, 13)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "Alternativas"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(474, 132)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(109, 13)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "Codigos competencia"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(478, 363)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(93, 13)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "Productos arrastre"
        '
        'btnAgregaAlt
        '
        Me.btnAgregaAlt.Location = New System.Drawing.Point(115, 124)
        Me.btnAgregaAlt.Name = "btnAgregaAlt"
        Me.btnAgregaAlt.Size = New System.Drawing.Size(75, 23)
        Me.btnAgregaAlt.TabIndex = 13
        Me.btnAgregaAlt.Text = "Agregar"
        Me.btnAgregaAlt.UseVisualStyleBackColor = True
        '
        'txtCodproAlt
        '
        Me.txtCodproAlt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodproAlt.Location = New System.Drawing.Point(9, 126)
        Me.txtCodproAlt.Name = "txtCodproAlt"
        Me.txtCodproAlt.Size = New System.Drawing.Size(100, 20)
        Me.txtCodproAlt.TabIndex = 14
        '
        'txtCodproComp
        '
        Me.txtCodproComp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodproComp.Location = New System.Drawing.Point(524, 151)
        Me.txtCodproComp.Name = "txtCodproComp"
        Me.txtCodproComp.Size = New System.Drawing.Size(100, 20)
        Me.txtCodproComp.TabIndex = 15
        '
        'btnAgregaCompetidor
        '
        Me.btnAgregaCompetidor.Location = New System.Drawing.Point(850, 149)
        Me.btnAgregaCompetidor.Name = "btnAgregaCompetidor"
        Me.btnAgregaCompetidor.Size = New System.Drawing.Size(75, 23)
        Me.btnAgregaCompetidor.TabIndex = 16
        Me.btnAgregaCompetidor.Text = "Agregar"
        Me.btnAgregaCompetidor.UseVisualStyleBackColor = True
        '
        'btnAgregaCMarco
        '
        Me.btnAgregaCMarco.Location = New System.Drawing.Point(373, 5)
        Me.btnAgregaCMarco.Name = "btnAgregaCMarco"
        Me.btnAgregaCMarco.Size = New System.Drawing.Size(75, 23)
        Me.btnAgregaCMarco.TabIndex = 18
        Me.btnAgregaCMarco.Text = "Agregar"
        Me.btnAgregaCMarco.UseVisualStyleBackColor = True
        '
        'txtCodproCMarco
        '
        Me.txtCodproCMarco.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodproCMarco.Location = New System.Drawing.Point(58, 6)
        Me.txtCodproCMarco.Name = "txtCodproCMarco"
        Me.txtCodproCMarco.Size = New System.Drawing.Size(100, 20)
        Me.txtCodproCMarco.TabIndex = 17
        '
        'btnAgregaArrastre
        '
        Me.btnAgregaArrastre.Location = New System.Drawing.Point(583, 379)
        Me.btnAgregaArrastre.Name = "btnAgregaArrastre"
        Me.btnAgregaArrastre.Size = New System.Drawing.Size(75, 23)
        Me.btnAgregaArrastre.TabIndex = 20
        Me.btnAgregaArrastre.Text = "Agregar"
        Me.btnAgregaArrastre.UseVisualStyleBackColor = True
        '
        'txtCodproArrastre
        '
        Me.txtCodproArrastre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodproArrastre.Location = New System.Drawing.Point(477, 381)
        Me.txtCodproArrastre.Name = "txtCodproArrastre"
        Me.txtCodproArrastre.Size = New System.Drawing.Size(100, 20)
        Me.txtCodproArrastre.TabIndex = 19
        '
        'txtRutComp
        '
        Me.txtRutComp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRutComp.Location = New System.Drawing.Point(735, 151)
        Me.txtRutComp.Name = "txtRutComp"
        Me.txtRutComp.Size = New System.Drawing.Size(100, 20)
        Me.txtRutComp.TabIndex = 21
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(478, 155)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(41, 13)
        Me.Label6.TabIndex = 22
        Me.Label6.Text = "Codpro"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(649, 152)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(80, 13)
        Me.Label7.TabIndex = 23
        Me.Label7.Text = "Rut Competidor"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(7, 9)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(41, 13)
        Me.Label8.TabIndex = 24
        Me.Label8.Text = "Codpro"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(160, 9)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(43, 13)
        Me.Label9.TabIndex = 25
        Me.Label9.Text = "Multiplo"
        '
        'txtMultiploCMarco
        '
        Me.txtMultiploCMarco.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMultiploCMarco.Location = New System.Drawing.Point(209, 6)
        Me.txtMultiploCMarco.Name = "txtMultiploCMarco"
        Me.txtMultiploCMarco.Size = New System.Drawing.Size(100, 20)
        Me.txtMultiploCMarco.TabIndex = 26
        '
        'txtMarcaComp
        '
        Me.txtMarcaComp.Location = New System.Drawing.Point(524, 188)
        Me.txtMarcaComp.Name = "txtMarcaComp"
        Me.txtMarcaComp.Size = New System.Drawing.Size(100, 20)
        Me.txtMarcaComp.TabIndex = 27
        '
        'txtDescripComp
        '
        Me.txtDescripComp.Location = New System.Drawing.Point(735, 187)
        Me.txtDescripComp.Name = "txtDescripComp"
        Me.txtDescripComp.Size = New System.Drawing.Size(100, 20)
        Me.txtDescripComp.TabIndex = 28
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(478, 191)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(37, 13)
        Me.Label10.TabIndex = 29
        Me.Label10.Text = "Marca"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(652, 188)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(63, 13)
        Me.Label11.TabIndex = 30
        Me.Label11.Text = "Descripción"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(5, 36)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(43, 13)
        Me.Label12.TabIndex = 32
        Me.Label12.Text = "Descrip"
        '
        'txtdescrip
        '
        Me.txtdescrip.Location = New System.Drawing.Point(58, 33)
        Me.txtdescrip.Name = "txtdescrip"
        Me.txtdescrip.Size = New System.Drawing.Size(390, 20)
        Me.txtdescrip.TabIndex = 31
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(6, 63)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(52, 13)
        Me.Label13.TabIndex = 33
        Me.Label13.Text = "Convenio"
        '
        'cmbConveni
        '
        Me.cmbConveni.FormattingEnabled = True
        Me.cmbConveni.Items.AddRange(New Object() {"Menaje, Aseo Y Cuidado Personal", "Artículos De Ferretería, Materiales Para La Construcción Y Electrohogar", "Convenio Marco De Venta, Arriendo Y Suministros De Impresoras.", "Convenio Marco De Hardware, Licencias De Software Y Recursos Educativos Digitales" &
                "", "CM Mobiliario Gral, Oficina, Escolar, Clínico, Urbano", "CM Articulos De Escritorio Y Papeleria", "Neumáticos, Lubricantes, Accesorios Para Vehículos Y Servicios Complementarios", "Órtesis, Prótesis, Endoprótesis E Insumos De Salud", "Productos Y Servicios Para Emergencia - 2016 (Emergencia De Primer Nivel)", "Productos Y Servicios Para Emergencia - 2016 (Emergencias Hidrologicas)", "Productos Y Servicios Para Emergencia - 2016 (Ropa De Cama)", "ALIMENTOS"})
        Me.cmbConveni.Location = New System.Drawing.Point(58, 58)
        Me.cmbConveni.Name = "cmbConveni"
        Me.cmbConveni.Size = New System.Drawing.Size(391, 21)
        Me.cmbConveni.TabIndex = 34
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Location = New System.Drawing.Point(9, 290)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(463, 112)
        Me.TabControl1.TabIndex = 35
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.txtCodproCMarco)
        Me.TabPage1.Controls.Add(Me.cmbConveni)
        Me.TabPage1.Controls.Add(Me.Label13)
        Me.TabPage1.Controls.Add(Me.btnAgregaCMarco)
        Me.TabPage1.Controls.Add(Me.Label12)
        Me.TabPage1.Controls.Add(Me.Label8)
        Me.TabPage1.Controls.Add(Me.txtdescrip)
        Me.TabPage1.Controls.Add(Me.Label9)
        Me.TabPage1.Controls.Add(Me.txtMultiploCMarco)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(455, 86)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Codigo Convenio Marco"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "CODPRO"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "DESCRIP"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "MULTIPLO"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        '
        'MULTIPLO
        '
        Me.MULTIPLO.HeaderText = "CONVENIO"
        Me.MULTIPLO.Name = "MULTIPLO"
        Me.MULTIPLO.ReadOnly = True
        '
        'frmProductosRelacionados
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(938, 557)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.txtDescripComp)
        Me.Controls.Add(Me.txtMarcaComp)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txtRutComp)
        Me.Controls.Add(Me.btnAgregaArrastre)
        Me.Controls.Add(Me.txtCodproArrastre)
        Me.Controls.Add(Me.btnAgregaCompetidor)
        Me.Controls.Add(Me.txtCodproComp)
        Me.Controls.Add(Me.txtCodproAlt)
        Me.Controls.Add(Me.btnAgregaAlt)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dgvArrastre)
        Me.Controls.Add(Me.dgvCompetencia)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.dgvConvenioMarco)
        Me.Controls.Add(Me.dgvAlternativas)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblCodpro)
        Me.Controls.Add(Me.txtDespro)
        Me.Controls.Add(Me.txtCodpro)
        Me.Name = "frmProductosRelacionados"
        Me.Text = "Productos Relacionados"
        CType(Me.dgvAlternativas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvConvenioMarco, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvCompetencia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvArrastre, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtCodpro As TextBox
    Friend WithEvents txtDespro As TextBox
    Friend WithEvents lblCodpro As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents dgvAlternativas As DataGridView
    Friend WithEvents dgvConvenioMarco As DataGridView
    Friend WithEvents Button1 As Button
    Friend WithEvents dgvCompetencia As DataGridView
    Friend WithEvents dgvArrastre As DataGridView
    Friend WithEvents Label1 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents CODPRO As DataGridViewTextBoxColumn
    Friend WithEvents DESCRIP As DataGridViewTextBoxColumn
    Friend WithEvents MARCAP As DataGridViewTextBoxColumn
    Friend WithEvents ESTADO As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents MARCA As DataGridViewTextBoxColumn
    Friend WithEvents COMPETIDOR As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As DataGridViewTextBoxColumn
    Friend WithEvents DESCRIPCION As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As DataGridViewTextBoxColumn
    Friend WithEvents btnAgregaAlt As Button
    Friend WithEvents txtCodproAlt As TextBox
    Friend WithEvents txtCodproComp As TextBox
    Friend WithEvents btnAgregaCompetidor As Button
    Friend WithEvents btnAgregaCMarco As Button
    Friend WithEvents txtCodproCMarco As TextBox
    Friend WithEvents btnAgregaArrastre As Button
    Friend WithEvents txtCodproArrastre As TextBox
    Friend WithEvents txtRutComp As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents txtMultiploCMarco As TextBox
    Friend WithEvents txtMarcaComp As TextBox
    Friend WithEvents txtDescripComp As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents txtdescrip As TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents cmbConveni As ComboBox
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents DataGridViewTextBoxColumn3 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As DataGridViewTextBoxColumn
    Friend WithEvents MULTIPLO As DataGridViewTextBoxColumn
End Class
