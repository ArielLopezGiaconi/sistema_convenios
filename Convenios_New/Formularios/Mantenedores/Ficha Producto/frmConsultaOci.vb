﻿Public Class frmConsultaOci
    Dim bd As New Conexion
    Dim codigo, descripcion As String
    Dim PuntoControl As New PuntoControl
    Private Sub frmConsultaOci_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.StartPosition = FormStartPosition.Manual
        Me.Location = New Point(0, 0)
        cargaGrillas()
        PuntoControl.RegistroUso("74")
    End Sub
    Public Sub New()

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub
    Public Sub New(codpro As String, despro As String)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()
        codigo = codpro
        descripcion = despro
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub
    Private Sub cargaGrillas()
        Try
            bd.open_dimerc()
            Dim Sql = "SELECT d.nombre ""Empresa"", "
            Sql = Sql & " nvl(a.numord,0) ""Nro. Orden"","
            Sql = Sql & " b.fecemi ""Fecha"", "
            Sql = Sql & " a.codpro ""Código"", "
            Sql = Sql & "'" & descripcion & "' ""Descripción"", "
            Sql = Sql & " nvl(a.caucon,0)""Cantidad"", "
            Sql = Sql & " nvl(a.saucon,0) ""Saldo"","
            Sql = Sql & " nvl(a.precom,0) ""Costo"","
            Sql = Sql & " e.desval ""Estado"", "
            Sql = Sql & " g.coprpv ""Prod.Prov"", "
            Sql = Sql & " h.nombre ""Proveedor"", "
            Sql = Sql & " b.fecemba ""Fecha Emba"" "
            Sql = Sql & " FROM ma_empresa d, ma_proveed h, re_provpro g, "
            Sql = Sql & " de_dominio e, en_ordcomp_inter b, de_ordcomp_inter a "
            Sql = Sql & " WHERE "
            Sql = Sql & "       a.codpro =  '" & codigo & "' "
            Sql = Sql & "   and a.codemp IN('" & Globales.empresa.ToString & "') "
            Sql = Sql & "   and b.numord = a.numord "
            Sql = Sql & "   and b.codemp = a.codemp "
            Sql = Sql & "   and e.coddom = '308' "   'Estados de la O/C
            Sql = Sql & "   and e.codval = a.codest "
            Sql = Sql & "   and g.codemp = a.codemp "
            Sql = Sql & "   and g.codpro = a.codpro "
            Sql = Sql & "   and g.rutprv = b.codprv "
            Sql = Sql & "   and h.rutprv = b.codprv "
            Sql = Sql & "   and d.codemp = a.codemp "
            Sql = Sql & "  ORDER BY a.sequen "
            dgvDeOrd.DataSource = bd.sqlSelect(Sql)
            dgvDeOrd.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            dgvDeOrd.Columns("Nro. Orden").DefaultCellStyle.Format = "n0"
            dgvDeOrd.Columns("Nro. Orden").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvDeOrd.Columns("Cantidad").DefaultCellStyle.Format = "n0"
            dgvDeOrd.Columns("Cantidad").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvDeOrd.Columns("Saldo").DefaultCellStyle.Format = "n0"
            dgvDeOrd.Columns("Saldo").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvDeOrd.Columns("Costo").DefaultCellStyle.Format = "n0"
            dgvDeOrd.Columns("Costo").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvDeOrd.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

            Sql = "SELECT "
            Sql = Sql & "d.nombre ""Empresa"", "
            Sql = Sql & " nvl(b.numord,0)""Nro. Orden"", "
            Sql = Sql & "b.codpro ""Código"", "
            Sql = Sql & "'" & descripcion & "' ""Descripción"", "
            Sql = Sql & " nvl(b.caucon,0) ""Recibido"", "
            Sql = Sql & " nvl(b.numrec,0) ""Nro. Rec"","
            Sql = Sql & "a.fecrec ""Fecha.Rec"", "
            Sql = Sql & "a.tipdoc ""Tipo"", "
            Sql = Sql & " nvl(a.numdoc,0) ""Nro.Doc"" "
            Sql = Sql & "FROM ma_empresa d, de_recepci_inter b, en_recepci_inter a "
            Sql = Sql & "WHERE a.codemp  IN(" & Globales.empresa.ToString & ") "
            Sql = Sql & "  and b.codpro = '" & codigo & "' "
            Sql = Sql & "  and b.codemp = a.codemp"
            Sql = Sql & "  and b.numrec = a.numrec"
            Sql = Sql & "  and d.codemp = a.codemp"
            Sql = Sql & " ORDER by b.sequen "
            dgvReOrd.DataSource = bd.sqlSelect(Sql)
            dgvReOrd.Columns("Empresa").Width = 100
            dgvReOrd.Columns("Nro. Orden").Width = 50
            dgvReOrd.Columns("Código").Width = 80
            dgvReOrd.Columns("Descripción").Width = 350
            dgvReOrd.Columns("Recibido").Width = 100
            dgvReOrd.Columns("Nro. Rec").Width = 80
            dgvReOrd.Columns("Fecha.Rec").Width = 100
            dgvReOrd.Columns("Tipo").Width = 50
            dgvReOrd.Columns("Nro.Doc").Width = 100
            dgvReOrd.Columns("Nro. Orden").DefaultCellStyle.Format = "n0"
            dgvReOrd.Columns("Nro. Orden").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvReOrd.Columns("Recibido").DefaultCellStyle.Format = "n0"
            dgvReOrd.Columns("Recibido").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvReOrd.Columns("Nro. Rec").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvReOrd.Columns("Nro.Doc").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvReOrd.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter


        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub btn_Salir_Click(sender As System.Object, e As System.EventArgs) Handles btn_Salir.Click
        Me.Close()
    End Sub
End Class