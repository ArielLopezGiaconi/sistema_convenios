﻿Public Class frmConsulta_Hist
    Dim bd As New Conexion
    Dim StrFec1A, StrFec1B As String
    Dim StrFec2A, StrFec2B As String
    Dim StrFec3A, StrFec3B As String
    Dim StrFec4A, StrFec4B As String
    Dim StrFec5A, StrFec5B As String
    Dim PuntoControl As New PuntoControl

    Private Sub txtMarca_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtMarca.KeyPress, txtDescrip.KeyPress, txtCodigo.KeyPress
        e.Handled = True
    End Sub
    Public Sub New(codpro As String, despro As String, marc As String)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()
        txtCodigo.Text = codpro
        txtDescrip.Text = despro
        txtMarca.Text = marc
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub
    Private Sub frmConsulta_Hist_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.StartPosition = FormStartPosition.Manual
        Me.Location = New Point(0, 100)

        StrFec1A = "01/01/" & Mid(Now, 7, 4)
        StrFec1B = "31/12/" & Mid(Now, 7, 4)

        StrFec2A = DateAdd("M", -12, StrFec1A)
        StrFec2B = DateAdd("M", -12, StrFec1B)

        StrFec3A = DateAdd("M", -12, StrFec2A)
        StrFec3B = DateAdd("M", -12, StrFec2B)

        StrFec4A = DateAdd("M", -12, StrFec3A)
        StrFec4B = DateAdd("M", -12, StrFec3B)

        StrFec5A = DateAdd("M", -12, StrFec4A)
        StrFec5B = DateAdd("M", -12, StrFec4B)
        PuntoControl.RegistroUso("73")
        cargaGrillas()

    End Sub
    Private Sub cargaGrillas()
        Try
            bd.open_dimerc()
            Dim Sql = " Select to_char(to_date('01/'||substr(trunc(a.fecha),4,7)),'MM-YYYY') ""Fecha"",sum(a.cantid) ""Cantidad""  "
            Sql = Sql & " from ges_producto_dia a "
            Sql = Sql & " where a.FECHA between to_date('" & StrFec1A & "','dd/mm/yyyy')  "
            Sql = Sql & "                   and to_date('" & StrFec1B & "','dd/mm/yyyy') "
            Sql = Sql & "  and a.CODPRO = '" & txtCodigo.Text & "'"
            Sql = Sql & "  and a.CODEMP = " & Globales.empresa.ToString
            Sql = Sql & " group by substr(trunc(a.fecha),4,7) "
            Sql = Sql & " order by substr(trunc(a.fecha),4,7) "
            dgv1.DataSource = bd.sqlSelect(Sql)
            Sql = " select to_char(to_date('01/'||substr(trunc(a.fecha),4,7)),'MM-YYYY') ""Fecha"",sum(a.cantid) ""Cantidad""  "
            Sql = Sql & "from ges_producto_dia a "
            Sql = Sql & " where a.FECHA between to_date('" & StrFec2A & "','dd/mm/yyyy')  "
            Sql = Sql & "                   and to_date('" & StrFec2B & "','dd/mm/yyyy') "
            Sql = Sql & "  and a.CODPRO = '" & txtCodigo.Text & "'"
            Sql = Sql & "  and a.CODEMP = " & Globales.empresa.ToString
            Sql = Sql & "group by substr(trunc(a.fecha),4,7) "
            Sql = Sql & "order by substr(trunc(a.fecha),4,7) "
            dgv2.DataSource = bd.sqlSelect(Sql)
            Sql = " select  to_char(to_date('01/'||substr(trunc(a.fecha),4,7)),'MM-YYYY') ""Fecha"",sum(a.cantid) ""Cantidad""   "
            Sql = Sql & "from ges_producto_dia a "
            Sql = Sql & " where a.FECHA between to_date('" & StrFec3A & "','dd/mm/yyyy')  "
            Sql = Sql & "                   and to_date('" & StrFec3B & "','dd/mm/yyyy') "
            Sql = Sql & "  and a.CODPRO = '" & txtCodigo.Text & "'"
            Sql = Sql & "  and a.CODEMP = " & Globales.empresa.ToString
            Sql = Sql & "group by substr(trunc(a.fecha),4,7) "
            Sql = Sql & "order by substr(trunc(a.fecha),4,7) "
            dgv3.DataSource = bd.sqlSelect(Sql)
            Sql = " select  to_char(to_date('01/'||substr(trunc(a.fecha),4,7)),'MM-YYYY') ""Fecha"",sum(a.cantid) ""Cantidad""  "
            Sql = Sql & "from ges_producto_dia a "
            Sql = Sql & " where a.FECHA between to_date('" & StrFec4A & "','dd/mm/yyyy')  "
            Sql = Sql & "                   and to_date('" & StrFec4B & "','dd/mm/yyyy') "
            Sql = Sql & "  and a.CODPRO = '" & txtCodigo.Text & "'"
            Sql = Sql & "  and a.CODEMP = " & Globales.empresa.ToString
            Sql = Sql & "group by substr(trunc(a.fecha),4,7) "
            Sql = Sql & "order by substr(trunc(a.fecha),4,7) "
            dgv4.DataSource = bd.sqlSelect(Sql)
            Sql = " select  to_char(to_date('01/'||substr(trunc(a.fecha),4,7)),'MM-YYYY') ""Fecha"",sum(a.cantid) ""Cantidad""  "
            Sql = Sql & "from ges_producto_dia a "
            Sql = Sql & " where a.FECHA between to_date('" & StrFec5A & "','dd/mm/yyyy')  "
            Sql = Sql & "                   and to_date('" & StrFec5B & "','dd/mm/yyyy') "
            Sql = Sql & "  and a.CODPRO = '" & txtCodigo.Text & "'"
            Sql = Sql & "  and a.CODEMP = " & Globales.empresa.ToString
            Sql = Sql & "group by substr(trunc(a.fecha),4,7) "
            Sql = Sql & "order by substr(trunc(a.fecha),4,7) "
            dgv5.DataSource = bd.sqlSelect(Sql)

            dgv1.Columns("Cantidad").DefaultCellStyle.Format = "n0"
            dgv1.Columns("Cantidad").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgv2.Columns("Cantidad").DefaultCellStyle.Format = "n0"
            dgv2.Columns("Cantidad").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgv3.Columns("Cantidad").DefaultCellStyle.Format = "n0"
            dgv3.Columns("Cantidad").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgv4.Columns("Cantidad").DefaultCellStyle.Format = "n0"
            dgv4.Columns("Cantidad").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgv5.Columns("Cantidad").DefaultCellStyle.Format = "n0"
            dgv5.Columns("Cantidad").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

            dgv1.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dgv1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
            dgv2.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dgv2.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
            dgv3.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dgv3.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
            dgv4.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dgv4.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
            dgv5.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dgv5.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill

        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub btn_Salir_Click(sender As System.Object, e As System.EventArgs) Handles btn_Salir.Click
        Me.Close()
    End Sub
End Class