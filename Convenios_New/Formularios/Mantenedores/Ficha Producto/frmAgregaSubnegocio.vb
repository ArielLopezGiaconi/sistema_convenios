﻿Public Class frmAgregaSubnegocio
    Dim agrega As Boolean
    Dim campo As Integer
    Dim id As Long
    Public Sub New(ByVal agrega As Boolean, id As Long, campo As Integer)
        Dim datos As New Conexion()
        datos.open_dimerc()
        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        Me.agrega = agrega
        Me.campo = campo
        Me.id = id
        If agrega Then
            Me.Text = "Agrega"
        Else
            Me.Text = "Modifica"
        End If

        If campo = 1 Then
            Me.Text = Me.Text & " Proyecto"
        Else
            Text = Text & " Subgrupo"
        End If

        If Not agrega Then
            If campo = 1 Then
                txtDescrip.Text = datos.sqlSelectUnico("SELECT DESCRIPCION
                                                        FROM MA_NEGOCIO_PRICING
                                                        WHERE ID = " & id, "DESCRIPCION")
            End If
            If campo = 2 Then
                txtDescrip.Text = datos.sqlSelectUnico("SELECT DESCRIPCION
                                                        FROM MA_NEGOCIO_SUBGRUPO
                                                        WHERE ID = " & id, "DESCRIPCION")
            End If

        End If
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Dim sql As String = ""
        Dim datos As New Conexion()
        If agrega Then
            If campo = 1 Then
                sql = "INSERT INTO MA_NEGOCIO_PRICING(ID,DESCRIPCION)
                   VALUES(SEQ_NEGOCIO.NEXTVAL, '" & txtDescrip.Text & "')"
            Else
                sql = "INSERT INTO MA_NEGOCIO_SUBGRUPO(ID,DESCRIPCION)
                   VALUES(SEQ_NEGOCIO.NEXTVAL, '" & txtDescrip.Text & "')"
            End If

        Else
            If campo = 1 Then
                sql = "UPDATE MA_NEGOCIO_PRICING
                           SET DESCRIPCION = '" & txtDescrip.Text & "'
                           WHERE ID = " & id
            Else
                sql = "UPDATE MA_NEGOCIO_SUBGRUPO
                           SET DESCRIPCION = '" & txtDescrip.Text & "'
                           WHERE ID = " & id
            End If
        End If
        datos.open_dimerc()
        datos.ejecutar(sql)
        MsgBox("Datos ingresados")
        Me.Close()
    End Sub

    Private Sub frmAgregaSubnegocio_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Location = New Point(150, 100)
    End Sub
End Class