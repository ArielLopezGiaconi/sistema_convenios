﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmFichaProducto
    Inherits MetroFramework.Forms.MetroForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFichaProducto))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btnSubgrupo = New System.Windows.Forms.Button()
        Me.btnNegocio = New System.Windows.Forms.Button()
        Me.btnRelprod = New System.Windows.Forms.Button()
        Me.btn_ProvProd = New System.Windows.Forms.Button()
        Me.btn_Grabar = New System.Windows.Forms.Button()
        Me.btn_Historia = New System.Windows.Forms.Button()
        Me.btn_ListaPrecio = New System.Windows.Forms.Button()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.cmdAlter = New System.Windows.Forms.Button()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.txtProdDescripCorta = New System.Windows.Forms.TextBox()
        Me.txtCodUni = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.CmdAgregar = New System.Windows.Forms.Button()
        Me.dtpFchaVig = New System.Windows.Forms.DateTimePicker()
        Me.cmbComision = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.dgvComision = New System.Windows.Forms.DataGridView()
        Me.cmbUnidad = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label61 = New System.Windows.Forms.Label()
        Me.txtCodsap = New System.Windows.Forms.TextBox()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.cmbNegocio = New System.Windows.Forms.ComboBox()
        Me.rdbContenga = New System.Windows.Forms.RadioButton()
        Me.rdbComience = New System.Windows.Forms.RadioButton()
        Me.dtpFechaIng = New System.Windows.Forms.DateTimePicker()
        Me.cmbCM = New System.Windows.Forms.ComboBox()
        Me.cmbTipoProd = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtDescripcion = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtCodigo = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtCategoria = New System.Windows.Forms.TextBox()
        Me.txtMarca = New System.Windows.Forms.TextBox()
        Me.cmbNegDmc = New System.Windows.Forms.ComboBox()
        Me.Label71 = New System.Windows.Forms.Label()
        Me.cmbSubgrupo = New System.Windows.Forms.ComboBox()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.cmbProvPrinc = New System.Windows.Forms.ComboBox()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.rdbMPInventarioNO = New System.Windows.Forms.RadioButton()
        Me.rdbMPInventarioSI = New System.Windows.Forms.RadioButton()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.txtGrupo = New System.Windows.Forms.TextBox()
        Me.txtPaleta = New System.Windows.Forms.TextBox()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.chkImportado = New System.Windows.Forms.CheckBox()
        Me.cmbRotacion = New System.Windows.Forms.ComboBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.chkMPropia = New System.Windows.Forms.CheckBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.cmbLinea = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.cmbEstado = New System.Windows.Forms.ComboBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.rdbContraPedido = New System.Windows.Forms.RadioButton()
        Me.rdbStock = New System.Windows.Forms.RadioButton()
        Me.cmbCategoria = New System.Windows.Forms.ComboBox()
        Me.cmbMarca = New System.Windows.Forms.ComboBox()
        Me.txtTipoDelta = New System.Windows.Forms.TextBox()
        Me.txtValorDelta = New System.Windows.Forms.TextBox()
        Me.lblDelta = New System.Windows.Forms.Label()
        Me.lblMensaje = New System.Windows.Forms.Label()
        Me.chkGobierno = New System.Windows.Forms.CheckBox()
        Me.chkRDimeiggs = New System.Windows.Forms.CheckBox()
        Me.cmbTipoImp = New System.Windows.Forms.ComboBox()
        Me.chkRDimerc = New System.Windows.Forms.CheckBox()
        Me.chkCalidad = New System.Windows.Forms.CheckBox()
        Me.chkNumSer = New System.Windows.Forms.CheckBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.chkAsegura = New System.Windows.Forms.CheckBox()
        Me.chkReembalaje = New System.Windows.Forms.CheckBox()
        Me.txtCatAct = New System.Windows.Forms.TextBox()
        Me.txtVidaUtil = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txtConVence = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.txtUndPed = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtUndEmb = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtCodBarra = New System.Windows.Forms.TextBox()
        Me.txtUMinima = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.chkMoneda = New System.Windows.Forms.CheckBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.chkColateral = New System.Windows.Forms.CheckBox()
        Me.cmbTipoAlmacenamiento = New System.Windows.Forms.ComboBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.cmbFamilia = New System.Windows.Forms.ComboBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.lblRespuesta = New System.Windows.Forms.Label()
        Me.txtStkComAntof = New System.Windows.Forms.TextBox()
        Me.txtFecInv = New System.Windows.Forms.TextBox()
        Me.txtIntCd = New System.Windows.Forms.TextBox()
        Me.txtStkCri = New System.Windows.Forms.TextBox()
        Me.txtStkDimerc = New System.Windows.Forms.TextBox()
        Me.txtMinCd = New System.Windows.Forms.TextBox()
        Me.txtTtoLoc = New System.Windows.Forms.TextBox()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.txtStkInt = New System.Windows.Forms.TextBox()
        Me.txtUltInv = New System.Windows.Forms.TextBox()
        Me.txtCriCd = New System.Windows.Forms.TextBox()
        Me.txtSMaximo = New System.Windows.Forms.TextBox()
        Me.txtStkPenDmg = New System.Windows.Forms.TextBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.txtStkDimeiggs = New System.Windows.Forms.TextBox()
        Me.txtOcLoc = New System.Windows.Forms.TextBox()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.txtMaxCd = New System.Windows.Forms.TextBox()
        Me.txtSMinimo = New System.Windows.Forms.TextBox()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.txtStkAse = New System.Windows.Forms.TextBox()
        Me.txtFscLoc = New System.Windows.Forms.TextBox()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.txtCFCosto = New System.Windows.Forms.TextBox()
        Me.txtCFFecha = New System.Windows.Forms.TextBox()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.txtCompraAnt = New System.Windows.Forms.TextBox()
        Me.txtReposicion = New System.Windows.Forms.TextBox()
        Me.txtUCDMG = New System.Windows.Forms.TextBox()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.txtUcPNB = New System.Windows.Forms.TextBox()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.txtCosto = New System.Windows.Forms.TextBox()
        Me.txtCosProm = New System.Windows.Forms.TextBox()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.txtUbicacion = New System.Windows.Forms.TextBox()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.chkCorrosivo = New System.Windows.Forms.CheckBox()
        Me.chkImflamable = New System.Windows.Forms.CheckBox()
        Me.chkToxico = New System.Windows.Forms.CheckBox()
        Me.chkNoAplica = New System.Windows.Forms.CheckBox()
        Me.TabControl2 = New System.Windows.Forms.TabControl()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.Label75 = New System.Windows.Forms.Label()
        Me.txtCDAntofagasta = New System.Windows.Forms.TextBox()
        Me.Label76 = New System.Windows.Forms.Label()
        Me.Label77 = New System.Windows.Forms.Label()
        Me.txtCDNoviciado = New System.Windows.Forms.TextBox()
        Me.txtCDSantiago = New System.Windows.Forms.TextBox()
        Me.Label73 = New System.Windows.Forms.Label()
        Me.Label72 = New System.Windows.Forms.Label()
        Me.txtCostoSanti = New System.Windows.Forms.TextBox()
        Me.txtCostoAnto = New System.Windows.Forms.TextBox()
        Me.Label70 = New System.Windows.Forms.Label()
        Me.txtTextoLibre = New System.Windows.Forms.TextBox()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.txtVolumen = New System.Windows.Forms.TextBox()
        Me.txtPeso = New System.Windows.Forms.TextBox()
        Me.Label67 = New System.Windows.Forms.Label()
        Me.Label66 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.rdbSobreVenta = New System.Windows.Forms.RadioButton()
        Me.rdbAgotarStock = New System.Windows.Forms.RadioButton()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.txtUltCos = New System.Windows.Forms.TextBox()
        Me.Label68 = New System.Windows.Forms.Label()
        Me.txtXDelta = New System.Windows.Forms.TextBox()
        Me.txtPorAporte = New System.Windows.Forms.TextBox()
        Me.txtPorCM = New System.Windows.Forms.TextBox()
        Me.Label74 = New System.Windows.Forms.Label()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.txtDolar = New System.Windows.Forms.TextBox()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.txtDescripAlter = New System.Windows.Forms.TextBox()
        Me.Label69 = New System.Windows.Forms.Label()
        Me.TabPage6 = New System.Windows.Forms.TabPage()
        Me.btn_Oci = New System.Windows.Forms.Button()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.btn_Anotación = New System.Windows.Forms.Button()
        Me.Panel1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        CType(Me.dgvComision, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.TabControl2.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.TabPage6.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Button1)
        Me.Panel1.Controls.Add(Me.btnSubgrupo)
        Me.Panel1.Controls.Add(Me.btnNegocio)
        Me.Panel1.Controls.Add(Me.btnRelprod)
        Me.Panel1.Controls.Add(Me.btn_ProvProd)
        Me.Panel1.Controls.Add(Me.btn_Grabar)
        Me.Panel1.Controls.Add(Me.btn_Historia)
        Me.Panel1.Controls.Add(Me.btn_ListaPrecio)
        Me.Panel1.Location = New System.Drawing.Point(1027, 52)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(88, 526)
        Me.Panel1.TabIndex = 0
        '
        'Button1
        '
        Me.Button1.Image = CType(resources.GetObject("Button1.Image"), System.Drawing.Image)
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button1.Location = New System.Drawing.Point(4, 443)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(81, 54)
        Me.Button1.TabIndex = 4
        Me.Button1.Text = "Historia"
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button1.UseVisualStyleBackColor = True
        '
        'btnSubgrupo
        '
        Me.btnSubgrupo.Location = New System.Drawing.Point(4, 380)
        Me.btnSubgrupo.Name = "btnSubgrupo"
        Me.btnSubgrupo.Size = New System.Drawing.Size(81, 58)
        Me.btnSubgrupo.TabIndex = 3
        Me.btnSubgrupo.Text = "Subgrupo Negocio"
        Me.btnSubgrupo.UseVisualStyleBackColor = True
        '
        'btnNegocio
        '
        Me.btnNegocio.Location = New System.Drawing.Point(4, 310)
        Me.btnNegocio.Name = "btnNegocio"
        Me.btnNegocio.Size = New System.Drawing.Size(81, 64)
        Me.btnNegocio.TabIndex = 2
        Me.btnNegocio.Text = "Negocios Producto"
        Me.btnNegocio.UseVisualStyleBackColor = True
        '
        'btnRelprod
        '
        Me.btnRelprod.Location = New System.Drawing.Point(3, 4)
        Me.btnRelprod.Name = "btnRelprod"
        Me.btnRelprod.Size = New System.Drawing.Size(82, 61)
        Me.btnRelprod.TabIndex = 1
        Me.btnRelprod.Text = "Productos Relacionados"
        Me.btnRelprod.UseVisualStyleBackColor = True
        '
        'btn_ProvProd
        '
        Me.btn_ProvProd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_ProvProd.Image = Global.Convenios_New.My.Resources.Resources.Floppy
        Me.btn_ProvProd.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btn_ProvProd.Location = New System.Drawing.Point(3, 250)
        Me.btn_ProvProd.Name = "btn_ProvProd"
        Me.btn_ProvProd.Size = New System.Drawing.Size(82, 53)
        Me.btn_ProvProd.TabIndex = 0
        Me.btn_ProvProd.Text = "Prov.Prod"
        Me.btn_ProvProd.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btn_ProvProd.UseVisualStyleBackColor = True
        Me.btn_ProvProd.Visible = False
        '
        'btn_Grabar
        '
        Me.btn_Grabar.Image = Global.Convenios_New.My.Resources.Resources.Floppy
        Me.btn_Grabar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btn_Grabar.Location = New System.Drawing.Point(4, 71)
        Me.btn_Grabar.Name = "btn_Grabar"
        Me.btn_Grabar.Size = New System.Drawing.Size(81, 53)
        Me.btn_Grabar.TabIndex = 0
        Me.btn_Grabar.Text = "Grabar"
        Me.btn_Grabar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btn_Grabar.UseVisualStyleBackColor = True
        '
        'btn_Historia
        '
        Me.btn_Historia.Image = Global.Convenios_New.My.Resources.Resources.search
        Me.btn_Historia.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btn_Historia.Location = New System.Drawing.Point(3, 132)
        Me.btn_Historia.Name = "btn_Historia"
        Me.btn_Historia.Size = New System.Drawing.Size(82, 53)
        Me.btn_Historia.TabIndex = 0
        Me.btn_Historia.Text = "Historia"
        Me.btn_Historia.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btn_Historia.UseVisualStyleBackColor = True
        Me.btn_Historia.Visible = False
        '
        'btn_ListaPrecio
        '
        Me.btn_ListaPrecio.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_ListaPrecio.Image = Global.Convenios_New.My.Resources.Resources.Floppy
        Me.btn_ListaPrecio.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btn_ListaPrecio.Location = New System.Drawing.Point(3, 191)
        Me.btn_ListaPrecio.Name = "btn_ListaPrecio"
        Me.btn_ListaPrecio.Size = New System.Drawing.Size(82, 53)
        Me.btn_ListaPrecio.TabIndex = 0
        Me.btn_ListaPrecio.Text = "L. Precio"
        Me.btn_ListaPrecio.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btn_ListaPrecio.UseVisualStyleBackColor = True
        Me.btn_ListaPrecio.Visible = False
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Location = New System.Drawing.Point(3, 3)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1007, 103)
        Me.TabControl1.TabIndex = 1
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.SystemColors.Control
        Me.TabPage1.Controls.Add(Me.cmdAlter)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(999, 77)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Código Dimerc"
        '
        'cmdAlter
        '
        Me.cmdAlter.Image = Global.Convenios_New.My.Resources.Resources.editar
        Me.cmdAlter.Location = New System.Drawing.Point(763, 24)
        Me.cmdAlter.Name = "cmdAlter"
        Me.cmdAlter.Size = New System.Drawing.Size(34, 23)
        Me.cmdAlter.TabIndex = 5
        Me.cmdAlter.UseVisualStyleBackColor = True
        Me.cmdAlter.Visible = False
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.SystemColors.Control
        Me.TabPage2.Controls.Add(Me.txtProdDescripCorta)
        Me.TabPage2.Controls.Add(Me.txtCodUni)
        Me.TabPage2.Controls.Add(Me.Label6)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(999, 77)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Código Unificado"
        '
        'txtProdDescripCorta
        '
        Me.txtProdDescripCorta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtProdDescripCorta.Location = New System.Drawing.Point(255, 11)
        Me.txtProdDescripCorta.Multiline = True
        Me.txtProdDescripCorta.Name = "txtProdDescripCorta"
        Me.txtProdDescripCorta.Size = New System.Drawing.Size(597, 58)
        Me.txtProdDescripCorta.TabIndex = 3
        '
        'txtCodUni
        '
        Me.txtCodUni.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodUni.Location = New System.Drawing.Point(115, 11)
        Me.txtCodUni.Name = "txtCodUni"
        Me.txtCodUni.Size = New System.Drawing.Size(134, 21)
        Me.txtCodUni.TabIndex = 4
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(17, 14)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(87, 13)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "Código Unificado"
        '
        'TabPage3
        '
        Me.TabPage3.BackColor = System.Drawing.SystemColors.Control
        Me.TabPage3.Controls.Add(Me.CmdAgregar)
        Me.TabPage3.Controls.Add(Me.dtpFchaVig)
        Me.TabPage3.Controls.Add(Me.cmbComision)
        Me.TabPage3.Controls.Add(Me.Label8)
        Me.TabPage3.Controls.Add(Me.Label7)
        Me.TabPage3.Controls.Add(Me.dgvComision)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(999, 77)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Comisiones"
        '
        'CmdAgregar
        '
        Me.CmdAgregar.Image = CType(resources.GetObject("CmdAgregar.Image"), System.Drawing.Image)
        Me.CmdAgregar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.CmdAgregar.Location = New System.Drawing.Point(374, 7)
        Me.CmdAgregar.Name = "CmdAgregar"
        Me.CmdAgregar.Size = New System.Drawing.Size(67, 59)
        Me.CmdAgregar.TabIndex = 4
        Me.CmdAgregar.Text = "(Der. 72)"
        Me.CmdAgregar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.CmdAgregar.UseVisualStyleBackColor = True
        '
        'dtpFchaVig
        '
        Me.dtpFchaVig.AccessibleDescription = ""
        Me.dtpFchaVig.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFchaVig.Location = New System.Drawing.Point(587, 42)
        Me.dtpFchaVig.Name = "dtpFchaVig"
        Me.dtpFchaVig.Size = New System.Drawing.Size(105, 21)
        Me.dtpFchaVig.TabIndex = 3
        '
        'cmbComision
        '
        Me.cmbComision.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbComision.FormattingEnabled = True
        Me.cmbComision.Location = New System.Drawing.Point(587, 15)
        Me.cmbComision.Name = "cmbComision"
        Me.cmbComision.Size = New System.Drawing.Size(185, 21)
        Me.cmbComision.TabIndex = 2
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(503, 18)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(49, 13)
        Me.Label8.TabIndex = 1
        Me.Label8.Text = "Comisión"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(503, 45)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(78, 13)
        Me.Label7.TabIndex = 1
        Me.Label7.Text = "Fecha Vigencia"
        '
        'dgvComision
        '
        Me.dgvComision.AllowUserToAddRows = False
        Me.dgvComision.AllowUserToDeleteRows = False
        Me.dgvComision.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvComision.Location = New System.Drawing.Point(89, 7)
        Me.dgvComision.Name = "dgvComision"
        Me.dgvComision.ReadOnly = True
        Me.dgvComision.RowHeadersVisible = False
        Me.dgvComision.Size = New System.Drawing.Size(221, 64)
        Me.dgvComision.TabIndex = 0
        '
        'cmbUnidad
        '
        Me.cmbUnidad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbUnidad.FormattingEnabled = True
        Me.cmbUnidad.Location = New System.Drawing.Point(105, 216)
        Me.cmbUnidad.Name = "cmbUnidad"
        Me.cmbUnidad.Size = New System.Drawing.Size(190, 21)
        Me.cmbUnidad.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(59, 219)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(40, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Unidad"
        '
        'Label61
        '
        Me.Label61.AutoSize = True
        Me.Label61.Location = New System.Drawing.Point(159, 12)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(48, 13)
        Me.Label61.TabIndex = 12
        Me.Label61.Text = "CODSAP"
        '
        'txtCodsap
        '
        Me.txtCodsap.Enabled = False
        Me.txtCodsap.Location = New System.Drawing.Point(211, 8)
        Me.txtCodsap.Name = "txtCodsap"
        Me.txtCodsap.Size = New System.Drawing.Size(100, 21)
        Me.txtCodsap.TabIndex = 11
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.Location = New System.Drawing.Point(309, 38)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(50, 13)
        Me.Label60.TabIndex = 2
        Me.Label60.Text = "Proyecto"
        '
        'cmbNegocio
        '
        Me.cmbNegocio.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbNegocio.FormattingEnabled = True
        Me.cmbNegocio.Location = New System.Drawing.Point(398, 36)
        Me.cmbNegocio.Name = "cmbNegocio"
        Me.cmbNegocio.Size = New System.Drawing.Size(207, 21)
        Me.cmbNegocio.TabIndex = 10
        '
        'rdbContenga
        '
        Me.rdbContenga.AutoSize = True
        Me.rdbContenga.Location = New System.Drawing.Point(88, 36)
        Me.rdbContenga.Name = "rdbContenga"
        Me.rdbContenga.Size = New System.Drawing.Size(72, 17)
        Me.rdbContenga.TabIndex = 4
        Me.rdbContenga.Text = "Contenga"
        Me.rdbContenga.UseVisualStyleBackColor = True
        '
        'rdbComience
        '
        Me.rdbComience.AutoSize = True
        Me.rdbComience.Checked = True
        Me.rdbComience.Location = New System.Drawing.Point(10, 36)
        Me.rdbComience.Name = "rdbComience"
        Me.rdbComience.Size = New System.Drawing.Size(71, 17)
        Me.rdbComience.TabIndex = 4
        Me.rdbComience.TabStop = True
        Me.rdbComience.Text = "Comience"
        Me.rdbComience.UseVisualStyleBackColor = True
        '
        'dtpFechaIng
        '
        Me.dtpFechaIng.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaIng.Location = New System.Drawing.Point(401, 7)
        Me.dtpFechaIng.Name = "dtpFechaIng"
        Me.dtpFechaIng.Size = New System.Drawing.Size(106, 21)
        Me.dtpFechaIng.TabIndex = 3
        '
        'cmbCM
        '
        Me.cmbCM.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCM.FormattingEnabled = True
        Me.cmbCM.Location = New System.Drawing.Point(398, 60)
        Me.cmbCM.Name = "cmbCM"
        Me.cmbCM.Size = New System.Drawing.Size(207, 21)
        Me.cmbCM.TabIndex = 2
        '
        'cmbTipoProd
        '
        Me.cmbTipoProd.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTipoProd.FormattingEnabled = True
        Me.cmbTipoProd.Location = New System.Drawing.Point(398, 12)
        Me.cmbTipoProd.Name = "cmbTipoProd"
        Me.cmbTipoProd.Size = New System.Drawing.Size(207, 21)
        Me.cmbTipoProd.TabIndex = 2
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(314, 9)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(81, 13)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Fecha Creación"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(309, 62)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(22, 13)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "CM"
        '
        'txtDescripcion
        '
        Me.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripcion.Location = New System.Drawing.Point(166, 32)
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(270, 21)
        Me.txtDescripcion.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(308, 17)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(73, 13)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Tipo Producto"
        '
        'txtCodigo
        '
        Me.txtCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodigo.Location = New System.Drawing.Point(53, 9)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.Size = New System.Drawing.Size(100, 21)
        Me.txtCodigo.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(7, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(40, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Código"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtCategoria)
        Me.GroupBox1.Controls.Add(Me.txtMarca)
        Me.GroupBox1.Controls.Add(Me.cmbNegDmc)
        Me.GroupBox1.Controls.Add(Me.Label71)
        Me.GroupBox1.Controls.Add(Me.cmbSubgrupo)
        Me.GroupBox1.Controls.Add(Me.Label60)
        Me.GroupBox1.Controls.Add(Me.cmbNegocio)
        Me.GroupBox1.Controls.Add(Me.Label63)
        Me.GroupBox1.Controls.Add(Me.cmbProvPrinc)
        Me.GroupBox1.Controls.Add(Me.cmbTipoProd)
        Me.GroupBox1.Controls.Add(Me.GroupBox6)
        Me.GroupBox1.Controls.Add(Me.cmbCM)
        Me.GroupBox1.Controls.Add(Me.Label62)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtGrupo)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtPaleta)
        Me.GroupBox1.Controls.Add(Me.Label59)
        Me.GroupBox1.Controls.Add(Me.chkImportado)
        Me.GroupBox1.Controls.Add(Me.cmbRotacion)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.Label58)
        Me.GroupBox1.Controls.Add(Me.Label22)
        Me.GroupBox1.Controls.Add(Me.chkMPropia)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.cmbLinea)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.cmbEstado)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Location = New System.Drawing.Point(6, 74)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(642, 212)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        '
        'txtCategoria
        '
        Me.txtCategoria.Enabled = False
        Me.txtCategoria.Location = New System.Drawing.Point(95, 173)
        Me.txtCategoria.Name = "txtCategoria"
        Me.txtCategoria.Size = New System.Drawing.Size(201, 21)
        Me.txtCategoria.TabIndex = 20
        '
        'txtMarca
        '
        Me.txtMarca.Enabled = False
        Me.txtMarca.Location = New System.Drawing.Point(95, 59)
        Me.txtMarca.Name = "txtMarca"
        Me.txtMarca.ReadOnly = True
        Me.txtMarca.Size = New System.Drawing.Size(201, 21)
        Me.txtMarca.TabIndex = 19
        '
        'cmbNegDmc
        '
        Me.cmbNegDmc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbNegDmc.FormattingEnabled = True
        Me.cmbNegDmc.Location = New System.Drawing.Point(398, 115)
        Me.cmbNegDmc.Name = "cmbNegDmc"
        Me.cmbNegDmc.Size = New System.Drawing.Size(207, 21)
        Me.cmbNegDmc.TabIndex = 18
        '
        'Label71
        '
        Me.Label71.AutoSize = True
        Me.Label71.Location = New System.Drawing.Point(312, 116)
        Me.Label71.Name = "Label71"
        Me.Label71.Size = New System.Drawing.Size(80, 13)
        Me.Label71.TabIndex = 17
        Me.Label71.Text = "Negocio Dimerc"
        '
        'cmbSubgrupo
        '
        Me.cmbSubgrupo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbSubgrupo.FormattingEnabled = True
        Me.cmbSubgrupo.Location = New System.Drawing.Point(95, 127)
        Me.cmbSubgrupo.Name = "cmbSubgrupo"
        Me.cmbSubgrupo.Size = New System.Drawing.Size(201, 21)
        Me.cmbSubgrupo.TabIndex = 16
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.Location = New System.Drawing.Point(12, 176)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(62, 13)
        Me.Label63.TabIndex = 14
        Me.Label63.Text = "Cat. Pricing"
        '
        'cmbProvPrinc
        '
        Me.cmbProvPrinc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbProvPrinc.FormattingEnabled = True
        Me.cmbProvPrinc.Location = New System.Drawing.Point(95, 149)
        Me.cmbProvPrinc.Name = "cmbProvPrinc"
        Me.cmbProvPrinc.Size = New System.Drawing.Size(201, 21)
        Me.cmbProvPrinc.TabIndex = 13
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.rdbMPInventarioNO)
        Me.GroupBox6.Controls.Add(Me.rdbMPInventarioSI)
        Me.GroupBox6.Location = New System.Drawing.Point(311, 142)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(146, 53)
        Me.GroupBox6.TabIndex = 6
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Marca Propia Inventario"
        '
        'rdbMPInventarioNO
        '
        Me.rdbMPInventarioNO.AutoSize = True
        Me.rdbMPInventarioNO.Location = New System.Drawing.Point(23, 24)
        Me.rdbMPInventarioNO.Name = "rdbMPInventarioNO"
        Me.rdbMPInventarioNO.Size = New System.Drawing.Size(40, 17)
        Me.rdbMPInventarioNO.TabIndex = 0
        Me.rdbMPInventarioNO.Text = "NO"
        Me.rdbMPInventarioNO.UseVisualStyleBackColor = True
        '
        'rdbMPInventarioSI
        '
        Me.rdbMPInventarioSI.AutoSize = True
        Me.rdbMPInventarioSI.Location = New System.Drawing.Point(87, 24)
        Me.rdbMPInventarioSI.Name = "rdbMPInventarioSI"
        Me.rdbMPInventarioSI.Size = New System.Drawing.Size(35, 17)
        Me.rdbMPInventarioSI.TabIndex = 0
        Me.rdbMPInventarioSI.Text = "SI"
        Me.rdbMPInventarioSI.UseVisualStyleBackColor = True
        '
        'Label62
        '
        Me.Label62.AutoSize = True
        Me.Label62.Location = New System.Drawing.Point(10, 155)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(63, 13)
        Me.Label62.TabIndex = 12
        Me.Label62.Text = "Prov. Princ."
        '
        'txtGrupo
        '
        Me.txtGrupo.Enabled = False
        Me.txtGrupo.Location = New System.Drawing.Point(95, 104)
        Me.txtGrupo.Name = "txtGrupo"
        Me.txtGrupo.Size = New System.Drawing.Size(201, 21)
        Me.txtGrupo.TabIndex = 10
        '
        'txtPaleta
        '
        Me.txtPaleta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPaleta.Location = New System.Drawing.Point(398, 85)
        Me.txtPaleta.Name = "txtPaleta"
        Me.txtPaleta.Size = New System.Drawing.Size(207, 21)
        Me.txtPaleta.TabIndex = 8
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Location = New System.Drawing.Point(309, 90)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(37, 13)
        Me.Label59.TabIndex = 7
        Me.Label59.Text = "Paleta"
        '
        'chkImportado
        '
        Me.chkImportado.AutoSize = True
        Me.chkImportado.ForeColor = System.Drawing.Color.Green
        Me.chkImportado.Location = New System.Drawing.Point(469, 155)
        Me.chkImportado.Name = "chkImportado"
        Me.chkImportado.Size = New System.Drawing.Size(76, 17)
        Me.chkImportado.TabIndex = 3
        Me.chkImportado.Text = "Importado"
        Me.chkImportado.UseVisualStyleBackColor = True
        '
        'cmbRotacion
        '
        Me.cmbRotacion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbRotacion.FormattingEnabled = True
        Me.cmbRotacion.Location = New System.Drawing.Point(95, 82)
        Me.cmbRotacion.Name = "cmbRotacion"
        Me.cmbRotacion.Size = New System.Drawing.Size(201, 21)
        Me.cmbRotacion.TabIndex = 2
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(11, 85)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(49, 13)
        Me.Label14.TabIndex = 0
        Me.Label14.Text = "Rotación"
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Location = New System.Drawing.Point(11, 132)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(58, 13)
        Me.Label58.TabIndex = 0
        Me.Label58.Text = "Sub-Grupo"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(11, 109)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(36, 13)
        Me.Label22.TabIndex = 0
        Me.Label22.Text = "Grupo"
        '
        'chkMPropia
        '
        Me.chkMPropia.AutoSize = True
        Me.chkMPropia.ForeColor = System.Drawing.Color.Black
        Me.chkMPropia.Location = New System.Drawing.Point(469, 173)
        Me.chkMPropia.Name = "chkMPropia"
        Me.chkMPropia.Size = New System.Drawing.Size(123, 17)
        Me.chkMPropia.TabIndex = 3
        Me.chkMPropia.Text = "Marca Propia Dimerc"
        Me.chkMPropia.UseVisualStyleBackColor = True
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(11, 62)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(36, 13)
        Me.Label11.TabIndex = 0
        Me.Label11.Text = "Marca"
        '
        'cmbLinea
        '
        Me.cmbLinea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbLinea.FormattingEnabled = True
        Me.cmbLinea.Location = New System.Drawing.Point(95, 35)
        Me.cmbLinea.Name = "cmbLinea"
        Me.cmbLinea.Size = New System.Drawing.Size(201, 21)
        Me.cmbLinea.TabIndex = 2
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(11, 39)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(32, 13)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "Línea"
        '
        'cmbEstado
        '
        Me.cmbEstado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbEstado.FormattingEnabled = True
        Me.cmbEstado.Location = New System.Drawing.Point(95, 12)
        Me.cmbEstado.Name = "cmbEstado"
        Me.cmbEstado.Size = New System.Drawing.Size(201, 21)
        Me.cmbEstado.TabIndex = 2
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(11, 15)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(40, 13)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "Estado"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.rdbContraPedido)
        Me.GroupBox3.Controls.Add(Me.rdbStock)
        Me.GroupBox3.Location = New System.Drawing.Point(3, 85)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(129, 53)
        Me.GroupBox3.TabIndex = 4
        Me.GroupBox3.TabStop = False
        '
        'rdbContraPedido
        '
        Me.rdbContraPedido.AutoSize = True
        Me.rdbContraPedido.Location = New System.Drawing.Point(6, 28)
        Me.rdbContraPedido.Name = "rdbContraPedido"
        Me.rdbContraPedido.Size = New System.Drawing.Size(93, 17)
        Me.rdbContraPedido.TabIndex = 0
        Me.rdbContraPedido.Text = "Contra Pedido"
        Me.rdbContraPedido.UseVisualStyleBackColor = True
        '
        'rdbStock
        '
        Me.rdbStock.AutoSize = True
        Me.rdbStock.Checked = True
        Me.rdbStock.Location = New System.Drawing.Point(6, 9)
        Me.rdbStock.Name = "rdbStock"
        Me.rdbStock.Size = New System.Drawing.Size(51, 17)
        Me.rdbStock.TabIndex = 0
        Me.rdbStock.TabStop = True
        Me.rdbStock.Text = "Stock"
        Me.rdbStock.UseVisualStyleBackColor = True
        '
        'cmbCategoria
        '
        Me.cmbCategoria.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCategoria.FormattingEnabled = True
        Me.cmbCategoria.Location = New System.Drawing.Point(753, 321)
        Me.cmbCategoria.Name = "cmbCategoria"
        Me.cmbCategoria.Size = New System.Drawing.Size(126, 21)
        Me.cmbCategoria.TabIndex = 15
        Me.cmbCategoria.Visible = False
        '
        'cmbMarca
        '
        Me.cmbMarca.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMarca.FormattingEnabled = True
        Me.cmbMarca.Location = New System.Drawing.Point(753, 299)
        Me.cmbMarca.Name = "cmbMarca"
        Me.cmbMarca.Size = New System.Drawing.Size(126, 21)
        Me.cmbMarca.TabIndex = 2
        Me.cmbMarca.Visible = False
        '
        'txtTipoDelta
        '
        Me.txtTipoDelta.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.txtTipoDelta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTipoDelta.Location = New System.Drawing.Point(485, 9)
        Me.txtTipoDelta.Name = "txtTipoDelta"
        Me.txtTipoDelta.Size = New System.Drawing.Size(51, 21)
        Me.txtTipoDelta.TabIndex = 1
        Me.txtTipoDelta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtValorDelta
        '
        Me.txtValorDelta.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.txtValorDelta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtValorDelta.Location = New System.Drawing.Point(412, 9)
        Me.txtValorDelta.Name = "txtValorDelta"
        Me.txtValorDelta.Size = New System.Drawing.Size(67, 21)
        Me.txtValorDelta.TabIndex = 1
        Me.txtValorDelta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblDelta
        '
        Me.lblDelta.AutoSize = True
        Me.lblDelta.Location = New System.Drawing.Point(374, 13)
        Me.lblDelta.Name = "lblDelta"
        Me.lblDelta.Size = New System.Drawing.Size(32, 13)
        Me.lblDelta.TabIndex = 0
        Me.lblDelta.Text = "Delta"
        '
        'lblMensaje
        '
        Me.lblMensaje.AutoSize = True
        Me.lblMensaje.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMensaje.ForeColor = System.Drawing.Color.Red
        Me.lblMensaje.Location = New System.Drawing.Point(326, 123)
        Me.lblMensaje.Name = "lblMensaje"
        Me.lblMensaje.Size = New System.Drawing.Size(212, 19)
        Me.lblMensaje.TabIndex = 9
        Me.lblMensaje.Text = "Producto no se podra cotizar"
        '
        'chkGobierno
        '
        Me.chkGobierno.AutoSize = True
        Me.chkGobierno.ForeColor = System.Drawing.Color.Blue
        Me.chkGobierno.Location = New System.Drawing.Point(202, 206)
        Me.chkGobierno.Name = "chkGobierno"
        Me.chkGobierno.Size = New System.Drawing.Size(69, 17)
        Me.chkGobierno.TabIndex = 3
        Me.chkGobierno.Text = "Gobierno"
        Me.chkGobierno.UseVisualStyleBackColor = True
        '
        'chkRDimeiggs
        '
        Me.chkRDimeiggs.AutoSize = True
        Me.chkRDimeiggs.ForeColor = System.Drawing.Color.Green
        Me.chkRDimeiggs.Location = New System.Drawing.Point(202, 187)
        Me.chkRDimeiggs.Name = "chkRDimeiggs"
        Me.chkRDimeiggs.Size = New System.Drawing.Size(97, 17)
        Me.chkRDimeiggs.TabIndex = 3
        Me.chkRDimeiggs.Text = "Stock Dimeiggs"
        Me.chkRDimeiggs.UseVisualStyleBackColor = True
        '
        'cmbTipoImp
        '
        Me.cmbTipoImp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTipoImp.FormattingEnabled = True
        Me.cmbTipoImp.Location = New System.Drawing.Point(94, 5)
        Me.cmbTipoImp.Name = "cmbTipoImp"
        Me.cmbTipoImp.Size = New System.Drawing.Size(179, 21)
        Me.cmbTipoImp.TabIndex = 2
        '
        'chkRDimerc
        '
        Me.chkRDimerc.AutoSize = True
        Me.chkRDimerc.ForeColor = System.Drawing.Color.Green
        Me.chkRDimerc.Location = New System.Drawing.Point(202, 172)
        Me.chkRDimerc.Name = "chkRDimerc"
        Me.chkRDimerc.Size = New System.Drawing.Size(109, 17)
        Me.chkRDimerc.TabIndex = 3
        Me.chkRDimerc.Text = "Prod. Temporada"
        Me.chkRDimerc.UseVisualStyleBackColor = True
        '
        'chkCalidad
        '
        Me.chkCalidad.AutoSize = True
        Me.chkCalidad.Location = New System.Drawing.Point(202, 158)
        Me.chkCalidad.Name = "chkCalidad"
        Me.chkCalidad.Size = New System.Drawing.Size(114, 17)
        Me.chkCalidad.TabIndex = 3
        Me.chkCalidad.Text = "Control de Calidad"
        Me.chkCalidad.UseVisualStyleBackColor = True
        '
        'chkNumSer
        '
        Me.chkNumSer.AutoSize = True
        Me.chkNumSer.Location = New System.Drawing.Point(202, 128)
        Me.chkNumSer.Name = "chkNumSer"
        Me.chkNumSer.Size = New System.Drawing.Size(89, 17)
        Me.chkNumSer.TabIndex = 3
        Me.chkNumSer.Text = "Nro. de Serie"
        Me.chkNumSer.UseVisualStyleBackColor = True
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(10, 11)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(75, 13)
        Me.Label21.TabIndex = 0
        Me.Label21.Text = "Tipo Impuesto"
        '
        'chkAsegura
        '
        Me.chkAsegura.AutoSize = True
        Me.chkAsegura.Location = New System.Drawing.Point(202, 113)
        Me.chkAsegura.Name = "chkAsegura"
        Me.chkAsegura.Size = New System.Drawing.Size(78, 17)
        Me.chkAsegura.TabIndex = 3
        Me.chkAsegura.Text = "Asegurado"
        Me.chkAsegura.UseVisualStyleBackColor = True
        '
        'chkReembalaje
        '
        Me.chkReembalaje.AutoSize = True
        Me.chkReembalaje.Location = New System.Drawing.Point(202, 98)
        Me.chkReembalaje.Name = "chkReembalaje"
        Me.chkReembalaje.Size = New System.Drawing.Size(82, 17)
        Me.chkReembalaje.TabIndex = 3
        Me.chkReembalaje.Text = "Reembalaje"
        Me.chkReembalaje.UseVisualStyleBackColor = True
        '
        'txtCatAct
        '
        Me.txtCatAct.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCatAct.Location = New System.Drawing.Point(118, 202)
        Me.txtCatAct.Name = "txtCatAct"
        Me.txtCatAct.Size = New System.Drawing.Size(67, 21)
        Me.txtCatAct.TabIndex = 1
        Me.txtCatAct.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtVidaUtil
        '
        Me.txtVidaUtil.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtVidaUtil.Location = New System.Drawing.Point(118, 177)
        Me.txtVidaUtil.Name = "txtVidaUtil"
        Me.txtVidaUtil.Size = New System.Drawing.Size(67, 21)
        Me.txtVidaUtil.TabIndex = 1
        Me.txtVidaUtil.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(51, 205)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(54, 13)
        Me.Label20.TabIndex = 0
        Me.Label20.Text = "Categoria"
        '
        'txtConVence
        '
        Me.txtConVence.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtConVence.Location = New System.Drawing.Point(118, 152)
        Me.txtConVence.Name = "txtConVence"
        Me.txtConVence.Size = New System.Drawing.Size(67, 21)
        Me.txtConVence.TabIndex = 1
        Me.txtConVence.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.ForeColor = System.Drawing.Color.Chocolate
        Me.Label19.Location = New System.Drawing.Point(51, 180)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(45, 13)
        Me.Label19.TabIndex = 0
        Me.Label19.Text = "Vida Util"
        '
        'txtUndPed
        '
        Me.txtUndPed.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUndPed.Location = New System.Drawing.Point(118, 128)
        Me.txtUndPed.Name = "txtUndPed"
        Me.txtUndPed.Size = New System.Drawing.Size(67, 21)
        Me.txtUndPed.TabIndex = 1
        Me.txtUndPed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.ForeColor = System.Drawing.Color.Chocolate
        Me.Label18.Location = New System.Drawing.Point(51, 155)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(64, 13)
        Me.Label18.TabIndex = 0
        Me.Label18.Text = "Vencimiento"
        '
        'txtUndEmb
        '
        Me.txtUndEmb.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUndEmb.Location = New System.Drawing.Point(118, 103)
        Me.txtUndEmb.Name = "txtUndEmb"
        Me.txtUndEmb.Size = New System.Drawing.Size(67, 21)
        Me.txtUndEmb.TabIndex = 1
        Me.txtUndEmb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(51, 131)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(58, 13)
        Me.Label17.TabIndex = 0
        Me.Label17.Text = "Uni.Pedido"
        '
        'txtCodBarra
        '
        Me.txtCodBarra.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodBarra.Location = New System.Drawing.Point(106, 125)
        Me.txtCodBarra.Name = "txtCodBarra"
        Me.txtCodBarra.Size = New System.Drawing.Size(201, 21)
        Me.txtCodBarra.TabIndex = 1
        '
        'txtUMinima
        '
        Me.txtUMinima.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUMinima.Location = New System.Drawing.Point(118, 78)
        Me.txtUMinima.Name = "txtUMinima"
        Me.txtUMinima.Size = New System.Drawing.Size(67, 21)
        Me.txtUMinima.TabIndex = 1
        Me.txtUMinima.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(51, 106)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(55, 13)
        Me.Label16.TabIndex = 0
        Me.Label16.Text = "Sub.Emba"
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Location = New System.Drawing.Point(21, 129)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(69, 13)
        Me.Label57.TabIndex = 0
        Me.Label57.Text = "Código Barra"
        '
        'chkMoneda
        '
        Me.chkMoneda.AutoSize = True
        Me.chkMoneda.Location = New System.Drawing.Point(202, 83)
        Me.chkMoneda.Name = "chkMoneda"
        Me.chkMoneda.Size = New System.Drawing.Size(89, 17)
        Me.chkMoneda.TabIndex = 3
        Me.chkMoneda.Text = "Otra Moneda"
        Me.chkMoneda.UseVisualStyleBackColor = True
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(51, 81)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(61, 13)
        Me.Label15.TabIndex = 0
        Me.Label15.Text = "Uni. Mínima"
        '
        'chkColateral
        '
        Me.chkColateral.AutoSize = True
        Me.chkColateral.Location = New System.Drawing.Point(202, 69)
        Me.chkColateral.Name = "chkColateral"
        Me.chkColateral.Size = New System.Drawing.Size(94, 17)
        Me.chkColateral.TabIndex = 3
        Me.chkColateral.Text = "Prod Colateral"
        Me.chkColateral.UseVisualStyleBackColor = True
        '
        'cmbTipoAlmacenamiento
        '
        Me.cmbTipoAlmacenamiento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTipoAlmacenamiento.FormattingEnabled = True
        Me.cmbTipoAlmacenamiento.Location = New System.Drawing.Point(105, 187)
        Me.cmbTipoAlmacenamiento.Name = "cmbTipoAlmacenamiento"
        Me.cmbTipoAlmacenamiento.Size = New System.Drawing.Size(201, 21)
        Me.cmbTipoAlmacenamiento.TabIndex = 2
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(14, 190)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(85, 13)
        Me.Label13.TabIndex = 0
        Me.Label13.Text = "Almacenamiento"
        '
        'cmbFamilia
        '
        Me.cmbFamilia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFamilia.FormattingEnabled = True
        Me.cmbFamilia.Location = New System.Drawing.Point(105, 156)
        Me.cmbFamilia.Name = "cmbFamilia"
        Me.cmbFamilia.Size = New System.Drawing.Size(201, 21)
        Me.cmbFamilia.TabIndex = 2
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(30, 164)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(39, 13)
        Me.Label12.TabIndex = 0
        Me.Label12.Text = "Familia"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.lblRespuesta)
        Me.GroupBox4.Controls.Add(Me.txtStkComAntof)
        Me.GroupBox4.Controls.Add(Me.txtFecInv)
        Me.GroupBox4.Controls.Add(Me.txtIntCd)
        Me.GroupBox4.Controls.Add(Me.txtStkCri)
        Me.GroupBox4.Controls.Add(Me.txtStkDimerc)
        Me.GroupBox4.Controls.Add(Me.txtMinCd)
        Me.GroupBox4.Controls.Add(Me.txtTtoLoc)
        Me.GroupBox4.Controls.Add(Me.Label38)
        Me.GroupBox4.Controls.Add(Me.Label30)
        Me.GroupBox4.Controls.Add(Me.Label45)
        Me.GroupBox4.Controls.Add(Me.Label37)
        Me.GroupBox4.Controls.Add(Me.Label28)
        Me.GroupBox4.Controls.Add(Me.Label44)
        Me.GroupBox4.Controls.Add(Me.Label36)
        Me.GroupBox4.Controls.Add(Me.txtStkInt)
        Me.GroupBox4.Controls.Add(Me.txtUltInv)
        Me.GroupBox4.Controls.Add(Me.txtCriCd)
        Me.GroupBox4.Controls.Add(Me.txtSMaximo)
        Me.GroupBox4.Controls.Add(Me.txtStkPenDmg)
        Me.GroupBox4.Controls.Add(Me.Label35)
        Me.GroupBox4.Controls.Add(Me.Label29)
        Me.GroupBox4.Controls.Add(Me.txtStkDimeiggs)
        Me.GroupBox4.Controls.Add(Me.txtOcLoc)
        Me.GroupBox4.Controls.Add(Me.Label42)
        Me.GroupBox4.Controls.Add(Me.Label34)
        Me.GroupBox4.Controls.Add(Me.Label27)
        Me.GroupBox4.Controls.Add(Me.Label41)
        Me.GroupBox4.Controls.Add(Me.Label33)
        Me.GroupBox4.Controls.Add(Me.txtMaxCd)
        Me.GroupBox4.Controls.Add(Me.txtSMinimo)
        Me.GroupBox4.Controls.Add(Me.Label40)
        Me.GroupBox4.Controls.Add(Me.Label32)
        Me.GroupBox4.Controls.Add(Me.txtStkAse)
        Me.GroupBox4.Controls.Add(Me.txtFscLoc)
        Me.GroupBox4.Controls.Add(Me.Label39)
        Me.GroupBox4.Controls.Add(Me.Label31)
        Me.GroupBox4.Location = New System.Drawing.Point(12, 302)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(663, 189)
        Me.GroupBox4.TabIndex = 3
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Stocks"
        '
        'lblRespuesta
        '
        Me.lblRespuesta.AutoSize = True
        Me.lblRespuesta.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRespuesta.ForeColor = System.Drawing.Color.Red
        Me.lblRespuesta.Location = New System.Drawing.Point(440, 151)
        Me.lblRespuesta.Name = "lblRespuesta"
        Me.lblRespuesta.Size = New System.Drawing.Size(0, 19)
        Me.lblRespuesta.TabIndex = 10
        '
        'txtStkComAntof
        '
        Me.txtStkComAntof.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtStkComAntof.Location = New System.Drawing.Point(323, 154)
        Me.txtStkComAntof.Name = "txtStkComAntof"
        Me.txtStkComAntof.Size = New System.Drawing.Size(100, 21)
        Me.txtStkComAntof.TabIndex = 1
        Me.txtStkComAntof.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtFecInv
        '
        Me.txtFecInv.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.txtFecInv.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFecInv.Location = New System.Drawing.Point(95, 154)
        Me.txtFecInv.Name = "txtFecInv"
        Me.txtFecInv.Size = New System.Drawing.Size(100, 21)
        Me.txtFecInv.TabIndex = 1
        Me.txtFecInv.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtIntCd
        '
        Me.txtIntCd.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtIntCd.Location = New System.Drawing.Point(548, 114)
        Me.txtIntCd.Name = "txtIntCd"
        Me.txtIntCd.Size = New System.Drawing.Size(100, 21)
        Me.txtIntCd.TabIndex = 1
        Me.txtIntCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtStkCri
        '
        Me.txtStkCri.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtStkCri.Location = New System.Drawing.Point(323, 114)
        Me.txtStkCri.Name = "txtStkCri"
        Me.txtStkCri.Size = New System.Drawing.Size(100, 21)
        Me.txtStkCri.TabIndex = 1
        Me.txtStkCri.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtStkDimerc
        '
        Me.txtStkDimerc.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.txtStkDimerc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtStkDimerc.Location = New System.Drawing.Point(95, 114)
        Me.txtStkDimerc.Name = "txtStkDimerc"
        Me.txtStkDimerc.Size = New System.Drawing.Size(100, 21)
        Me.txtStkDimerc.TabIndex = 1
        Me.txtStkDimerc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtMinCd
        '
        Me.txtMinCd.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMinCd.Location = New System.Drawing.Point(548, 54)
        Me.txtMinCd.Name = "txtMinCd"
        Me.txtMinCd.Size = New System.Drawing.Size(100, 21)
        Me.txtMinCd.TabIndex = 1
        Me.txtMinCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTtoLoc
        '
        Me.txtTtoLoc.BackColor = System.Drawing.Color.PaleGreen
        Me.txtTtoLoc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTtoLoc.Location = New System.Drawing.Point(323, 54)
        Me.txtTtoLoc.Name = "txtTtoLoc"
        Me.txtTtoLoc.Size = New System.Drawing.Size(100, 21)
        Me.txtTtoLoc.TabIndex = 1
        Me.txtTtoLoc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(225, 157)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(89, 13)
        Me.Label38.TabIndex = 0
        Me.Label38.Text = "StkCom. Antofag"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(11, 157)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(79, 13)
        Me.Label30.TabIndex = 0
        Me.Label30.Text = "Fec. Ult Inven."
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Location = New System.Drawing.Point(464, 117)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(60, 13)
        Me.Label45.TabIndex = 0
        Me.Label45.Text = "Int. Dimerc"
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(225, 117)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(87, 13)
        Me.Label37.TabIndex = 0
        Me.Label37.Text = "Cri. Antofagasta"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(11, 117)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(60, 13)
        Me.Label28.TabIndex = 0
        Me.Label28.Text = "Stk_Dimerc"
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Location = New System.Drawing.Point(464, 57)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(62, 13)
        Me.Label44.TabIndex = 0
        Me.Label44.Text = "Mín. Dimerc"
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(225, 57)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(54, 13)
        Me.Label36.TabIndex = 0
        Me.Label36.Text = "Tto. Local"
        '
        'txtStkInt
        '
        Me.txtStkInt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtStkInt.Location = New System.Drawing.Point(323, 134)
        Me.txtStkInt.Name = "txtStkInt"
        Me.txtStkInt.Size = New System.Drawing.Size(100, 21)
        Me.txtStkInt.TabIndex = 1
        Me.txtStkInt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtUltInv
        '
        Me.txtUltInv.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.txtUltInv.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUltInv.Location = New System.Drawing.Point(95, 134)
        Me.txtUltInv.Name = "txtUltInv"
        Me.txtUltInv.Size = New System.Drawing.Size(100, 21)
        Me.txtUltInv.TabIndex = 1
        Me.txtUltInv.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtCriCd
        '
        Me.txtCriCd.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCriCd.Location = New System.Drawing.Point(548, 94)
        Me.txtCriCd.Name = "txtCriCd"
        Me.txtCriCd.Size = New System.Drawing.Size(100, 21)
        Me.txtCriCd.TabIndex = 1
        Me.txtCriCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtSMaximo
        '
        Me.txtSMaximo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSMaximo.Location = New System.Drawing.Point(323, 94)
        Me.txtSMaximo.Name = "txtSMaximo"
        Me.txtSMaximo.Size = New System.Drawing.Size(100, 21)
        Me.txtSMaximo.TabIndex = 1
        Me.txtSMaximo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtStkPenDmg
        '
        Me.txtStkPenDmg.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.txtStkPenDmg.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtStkPenDmg.Location = New System.Drawing.Point(95, 94)
        Me.txtStkPenDmg.Name = "txtStkPenDmg"
        Me.txtStkPenDmg.Size = New System.Drawing.Size(100, 21)
        Me.txtStkPenDmg.TabIndex = 1
        Me.txtStkPenDmg.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(225, 137)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(88, 13)
        Me.Label35.TabIndex = 0
        Me.Label35.Text = "Int. Antofagasta"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(11, 137)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(77, 13)
        Me.Label29.TabIndex = 0
        Me.Label29.Text = "Ult. Inventario"
        '
        'txtStkDimeiggs
        '
        Me.txtStkDimeiggs.BackColor = System.Drawing.Color.Orange
        Me.txtStkDimeiggs.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtStkDimeiggs.Location = New System.Drawing.Point(548, 34)
        Me.txtStkDimeiggs.Name = "txtStkDimeiggs"
        Me.txtStkDimeiggs.Size = New System.Drawing.Size(100, 21)
        Me.txtStkDimeiggs.TabIndex = 1
        Me.txtStkDimeiggs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtOcLoc
        '
        Me.txtOcLoc.BackColor = System.Drawing.Color.PaleGreen
        Me.txtOcLoc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtOcLoc.Location = New System.Drawing.Point(323, 34)
        Me.txtOcLoc.Name = "txtOcLoc"
        Me.txtOcLoc.Size = New System.Drawing.Size(100, 21)
        Me.txtOcLoc.TabIndex = 1
        Me.txtOcLoc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Location = New System.Drawing.Point(464, 97)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(59, 13)
        Me.Label42.TabIndex = 0
        Me.Label42.Text = "Cri. Dimerc"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(225, 97)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(94, 13)
        Me.Label34.TabIndex = 0
        Me.Label34.Text = "Máx. Antofagasta"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(11, 97)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(80, 13)
        Me.Label27.TabIndex = 0
        Me.Label27.Text = "Por Llegar DMG"
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Location = New System.Drawing.Point(464, 37)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(70, 13)
        Me.Label41.TabIndex = 0
        Me.Label41.Text = "Stk_Dimeiggs"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(225, 37)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(78, 13)
        Me.Label33.TabIndex = 0
        Me.Label33.Text = "Por Llegar Loc."
        '
        'txtMaxCd
        '
        Me.txtMaxCd.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMaxCd.Location = New System.Drawing.Point(548, 74)
        Me.txtMaxCd.Name = "txtMaxCd"
        Me.txtMaxCd.Size = New System.Drawing.Size(100, 21)
        Me.txtMaxCd.TabIndex = 1
        Me.txtMaxCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtSMinimo
        '
        Me.txtSMinimo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSMinimo.Location = New System.Drawing.Point(323, 74)
        Me.txtSMinimo.Name = "txtSMinimo"
        Me.txtSMinimo.Size = New System.Drawing.Size(100, 21)
        Me.txtSMinimo.TabIndex = 1
        Me.txtSMinimo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(464, 77)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(66, 13)
        Me.Label40.TabIndex = 0
        Me.Label40.Text = "Máx. Dimerc"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(225, 77)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(90, 13)
        Me.Label32.TabIndex = 0
        Me.Label32.Text = "Mín. Antofagasta"
        '
        'txtStkAse
        '
        Me.txtStkAse.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtStkAse.Location = New System.Drawing.Point(548, 14)
        Me.txtStkAse.Name = "txtStkAse"
        Me.txtStkAse.Size = New System.Drawing.Size(100, 21)
        Me.txtStkAse.TabIndex = 1
        Me.txtStkAse.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtFscLoc
        '
        Me.txtFscLoc.BackColor = System.Drawing.Color.PaleGreen
        Me.txtFscLoc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFscLoc.Location = New System.Drawing.Point(323, 14)
        Me.txtFscLoc.Name = "txtFscLoc"
        Me.txtFscLoc.Size = New System.Drawing.Size(100, 21)
        Me.txtFscLoc.TabIndex = 1
        Me.txtFscLoc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Location = New System.Drawing.Point(464, 17)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(59, 13)
        Me.Label39.TabIndex = 0
        Me.Label39.Text = "Asegurado"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(225, 17)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(58, 13)
        Me.Label31.TabIndex = 0
        Me.Label31.Text = "FscLocales"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.txtCFCosto)
        Me.GroupBox5.Controls.Add(Me.txtCFFecha)
        Me.GroupBox5.Controls.Add(Me.Label43)
        Me.GroupBox5.Controls.Add(Me.Label46)
        Me.GroupBox5.Controls.Add(Me.txtCompraAnt)
        Me.GroupBox5.Controls.Add(Me.txtReposicion)
        Me.GroupBox5.Controls.Add(Me.txtUCDMG)
        Me.GroupBox5.Controls.Add(Me.Label50)
        Me.GroupBox5.Controls.Add(Me.Label53)
        Me.GroupBox5.Controls.Add(Me.txtUcPNB)
        Me.GroupBox5.Controls.Add(Me.Label49)
        Me.GroupBox5.Controls.Add(Me.Label47)
        Me.GroupBox5.Location = New System.Drawing.Point(679, 355)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(407, 105)
        Me.GroupBox5.TabIndex = 6
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Costos"
        '
        'txtCFCosto
        '
        Me.txtCFCosto.BackColor = System.Drawing.SystemColors.Info
        Me.txtCFCosto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCFCosto.Location = New System.Drawing.Point(295, 84)
        Me.txtCFCosto.Name = "txtCFCosto"
        Me.txtCFCosto.Size = New System.Drawing.Size(100, 21)
        Me.txtCFCosto.TabIndex = 1
        Me.txtCFCosto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtCFFecha
        '
        Me.txtCFFecha.BackColor = System.Drawing.SystemColors.Info
        Me.txtCFFecha.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCFFecha.Location = New System.Drawing.Point(78, 74)
        Me.txtCFFecha.Name = "txtCFFecha"
        Me.txtCFFecha.Size = New System.Drawing.Size(100, 21)
        Me.txtCFFecha.TabIndex = 1
        Me.txtCFFecha.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Location = New System.Drawing.Point(192, 26)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(58, 13)
        Me.Label43.TabIndex = 0
        Me.Label43.Text = "Reposición"
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Location = New System.Drawing.Point(192, 66)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(58, 13)
        Me.Label46.TabIndex = 0
        Me.Label46.Text = "U. Compra"
        '
        'txtCompraAnt
        '
        Me.txtCompraAnt.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.txtCompraAnt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCompraAnt.Location = New System.Drawing.Point(78, 34)
        Me.txtCompraAnt.Name = "txtCompraAnt"
        Me.txtCompraAnt.Size = New System.Drawing.Size(100, 21)
        Me.txtCompraAnt.TabIndex = 1
        Me.txtCompraAnt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtReposicion
        '
        Me.txtReposicion.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.txtReposicion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtReposicion.Location = New System.Drawing.Point(295, 24)
        Me.txtReposicion.Name = "txtReposicion"
        Me.txtReposicion.Size = New System.Drawing.Size(100, 21)
        Me.txtReposicion.TabIndex = 1
        Me.txtReposicion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtUCDMG
        '
        Me.txtUCDMG.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.txtUCDMG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUCDMG.Location = New System.Drawing.Point(295, 44)
        Me.txtUCDMG.Name = "txtUCDMG"
        Me.txtUCDMG.Size = New System.Drawing.Size(100, 21)
        Me.txtUCDMG.TabIndex = 1
        Me.txtUCDMG.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Location = New System.Drawing.Point(13, 40)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(44, 13)
        Me.Label50.TabIndex = 0
        Me.Label50.Text = "Compra"
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.Location = New System.Drawing.Point(193, 84)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(70, 13)
        Me.Label53.TabIndex = 0
        Me.Label53.Text = "Costo Futuro"
        '
        'txtUcPNB
        '
        Me.txtUcPNB.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.txtUcPNB.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUcPNB.Location = New System.Drawing.Point(295, 64)
        Me.txtUcPNB.Name = "txtUcPNB"
        Me.txtUcPNB.Size = New System.Drawing.Size(100, 21)
        Me.txtUcPNB.TabIndex = 1
        Me.txtUcPNB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Location = New System.Drawing.Point(13, 80)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(62, 13)
        Me.Label49.TabIndex = 0
        Me.Label49.Text = "Fecha C . F"
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Location = New System.Drawing.Point(192, 46)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(55, 13)
        Me.Label47.TabIndex = 0
        Me.Label47.Text = "U.Compra"
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Location = New System.Drawing.Point(197, 12)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(51, 13)
        Me.Label56.TabIndex = 0
        Me.Label56.Text = "COSCOM"
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Location = New System.Drawing.Point(14, 13)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(57, 13)
        Me.Label55.TabIndex = 0
        Me.Label55.Text = "COSPROM"
        '
        'txtCosto
        '
        Me.txtCosto.BackColor = System.Drawing.Color.Pink
        Me.txtCosto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCosto.Location = New System.Drawing.Point(254, 9)
        Me.txtCosto.Name = "txtCosto"
        Me.txtCosto.Size = New System.Drawing.Size(100, 21)
        Me.txtCosto.TabIndex = 1
        Me.txtCosto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtCosProm
        '
        Me.txtCosProm.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.txtCosProm.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCosProm.Location = New System.Drawing.Point(82, 9)
        Me.txtCosProm.Name = "txtCosProm"
        Me.txtCosProm.Size = New System.Drawing.Size(100, 21)
        Me.txtCosProm.TabIndex = 1
        Me.txtCosProm.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Location = New System.Drawing.Point(14, 32)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(52, 13)
        Me.Label48.TabIndex = 0
        Me.Label48.Text = "Ubicación"
        '
        'txtUbicacion
        '
        Me.txtUbicacion.BackColor = System.Drawing.SystemColors.Info
        Me.txtUbicacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUbicacion.Location = New System.Drawing.Point(72, 26)
        Me.txtUbicacion.Name = "txtUbicacion"
        Me.txtUbicacion.Size = New System.Drawing.Size(318, 21)
        Me.txtUbicacion.TabIndex = 1
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.chkCorrosivo)
        Me.GroupBox7.Controls.Add(Me.chkImflamable)
        Me.GroupBox7.Controls.Add(Me.chkToxico)
        Me.GroupBox7.Controls.Add(Me.chkNoAplica)
        Me.GroupBox7.Location = New System.Drawing.Point(12, 243)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(376, 53)
        Me.GroupBox7.TabIndex = 7
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Clasificación Nueva"
        '
        'chkCorrosivo
        '
        Me.chkCorrosivo.AutoSize = True
        Me.chkCorrosivo.ForeColor = System.Drawing.Color.DarkOrange
        Me.chkCorrosivo.Location = New System.Drawing.Point(278, 24)
        Me.chkCorrosivo.Name = "chkCorrosivo"
        Me.chkCorrosivo.Size = New System.Drawing.Size(87, 17)
        Me.chkCorrosivo.TabIndex = 7
        Me.chkCorrosivo.Text = "CORROSIVO"
        Me.chkCorrosivo.UseVisualStyleBackColor = True
        '
        'chkImflamable
        '
        Me.chkImflamable.AutoSize = True
        Me.chkImflamable.ForeColor = System.Drawing.Color.Red
        Me.chkImflamable.Location = New System.Drawing.Point(184, 24)
        Me.chkImflamable.Name = "chkImflamable"
        Me.chkImflamable.Size = New System.Drawing.Size(88, 17)
        Me.chkImflamable.TabIndex = 6
        Me.chkImflamable.Text = "IMFLAMABLE"
        Me.chkImflamable.UseVisualStyleBackColor = True
        '
        'chkToxico
        '
        Me.chkToxico.AutoSize = True
        Me.chkToxico.ForeColor = System.Drawing.Color.Green
        Me.chkToxico.Location = New System.Drawing.Point(113, 24)
        Me.chkToxico.Name = "chkToxico"
        Me.chkToxico.Size = New System.Drawing.Size(65, 17)
        Me.chkToxico.TabIndex = 4
        Me.chkToxico.Text = "TOXICO"
        Me.chkToxico.UseVisualStyleBackColor = True
        '
        'chkNoAplica
        '
        Me.chkNoAplica.AutoSize = True
        Me.chkNoAplica.ForeColor = System.Drawing.Color.Blue
        Me.chkNoAplica.Location = New System.Drawing.Point(11, 24)
        Me.chkNoAplica.Name = "chkNoAplica"
        Me.chkNoAplica.Size = New System.Drawing.Size(80, 17)
        Me.chkNoAplica.TabIndex = 5
        Me.chkNoAplica.Text = "NO APLICA"
        Me.chkNoAplica.UseVisualStyleBackColor = True
        '
        'TabControl2
        '
        Me.TabControl2.Controls.Add(Me.TabPage4)
        Me.TabControl2.Controls.Add(Me.TabPage6)
        Me.TabControl2.Location = New System.Drawing.Point(2, 55)
        Me.TabControl2.Name = "TabControl2"
        Me.TabControl2.SelectedIndex = 0
        Me.TabControl2.Size = New System.Drawing.Size(1019, 523)
        Me.TabControl2.TabIndex = 8
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.Label75)
        Me.TabPage4.Controls.Add(Me.txtCDAntofagasta)
        Me.TabPage4.Controls.Add(Me.Label76)
        Me.TabPage4.Controls.Add(Me.Label77)
        Me.TabPage4.Controls.Add(Me.txtCDNoviciado)
        Me.TabPage4.Controls.Add(Me.txtCDSantiago)
        Me.TabPage4.Controls.Add(Me.Label73)
        Me.TabPage4.Controls.Add(Me.Label72)
        Me.TabPage4.Controls.Add(Me.cmbCategoria)
        Me.TabPage4.Controls.Add(Me.txtCostoSanti)
        Me.TabPage4.Controls.Add(Me.txtCostoAnto)
        Me.TabPage4.Controls.Add(Me.Label70)
        Me.TabPage4.Controls.Add(Me.txtTextoLibre)
        Me.TabPage4.Controls.Add(Me.Panel4)
        Me.TabPage4.Controls.Add(Me.Panel3)
        Me.TabPage4.Controls.Add(Me.Panel2)
        Me.TabPage4.Controls.Add(Me.GroupBox1)
        Me.TabPage4.Controls.Add(Me.cmbMarca)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(1011, 497)
        Me.TabPage4.TabIndex = 0
        Me.TabPage4.Text = "Ficha Producto"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'Label75
        '
        Me.Label75.AutoSize = True
        Me.Label75.Location = New System.Drawing.Point(9, 462)
        Me.Label75.Name = "Label75"
        Me.Label75.Size = New System.Drawing.Size(84, 13)
        Me.Label75.TabIndex = 29
        Me.Label75.Text = "CD Antofagasta"
        '
        'txtCDAntofagasta
        '
        Me.txtCDAntofagasta.Location = New System.Drawing.Point(113, 459)
        Me.txtCDAntofagasta.Name = "txtCDAntofagasta"
        Me.txtCDAntofagasta.ReadOnly = True
        Me.txtCDAntofagasta.Size = New System.Drawing.Size(100, 21)
        Me.txtCDAntofagasta.TabIndex = 28
        '
        'Label76
        '
        Me.Label76.AutoSize = True
        Me.Label76.Location = New System.Drawing.Point(9, 435)
        Me.Label76.Name = "Label76"
        Me.Label76.Size = New System.Drawing.Size(70, 13)
        Me.Label76.TabIndex = 27
        Me.Label76.Text = "CD Noviciado"
        '
        'Label77
        '
        Me.Label77.AutoSize = True
        Me.Label77.Location = New System.Drawing.Point(9, 408)
        Me.Label77.Name = "Label77"
        Me.Label77.Size = New System.Drawing.Size(66, 13)
        Me.Label77.TabIndex = 26
        Me.Label77.Text = "CD Santiago"
        '
        'txtCDNoviciado
        '
        Me.txtCDNoviciado.Location = New System.Drawing.Point(113, 432)
        Me.txtCDNoviciado.Name = "txtCDNoviciado"
        Me.txtCDNoviciado.ReadOnly = True
        Me.txtCDNoviciado.Size = New System.Drawing.Size(100, 21)
        Me.txtCDNoviciado.TabIndex = 25
        '
        'txtCDSantiago
        '
        Me.txtCDSantiago.Location = New System.Drawing.Point(113, 405)
        Me.txtCDSantiago.Name = "txtCDSantiago"
        Me.txtCDSantiago.ReadOnly = True
        Me.txtCDSantiago.Size = New System.Drawing.Size(100, 21)
        Me.txtCDSantiago.TabIndex = 24
        '
        'Label73
        '
        Me.Label73.AutoSize = True
        Me.Label73.Location = New System.Drawing.Point(232, 408)
        Me.Label73.Name = "Label73"
        Me.Label73.Size = New System.Drawing.Size(80, 13)
        Me.Label73.TabIndex = 21
        Me.Label73.Text = "Costo Santiago"
        '
        'Label72
        '
        Me.Label72.AutoSize = True
        Me.Label72.Location = New System.Drawing.Point(232, 438)
        Me.Label72.Name = "Label72"
        Me.Label72.Size = New System.Drawing.Size(98, 13)
        Me.Label72.TabIndex = 20
        Me.Label72.Text = "Costo Antofagasta"
        '
        'txtCostoSanti
        '
        Me.txtCostoSanti.Location = New System.Drawing.Point(336, 405)
        Me.txtCostoSanti.Name = "txtCostoSanti"
        Me.txtCostoSanti.ReadOnly = True
        Me.txtCostoSanti.Size = New System.Drawing.Size(100, 21)
        Me.txtCostoSanti.TabIndex = 19
        '
        'txtCostoAnto
        '
        Me.txtCostoAnto.Location = New System.Drawing.Point(336, 435)
        Me.txtCostoAnto.Name = "txtCostoAnto"
        Me.txtCostoAnto.ReadOnly = True
        Me.txtCostoAnto.Size = New System.Drawing.Size(100, 21)
        Me.txtCostoAnto.TabIndex = 18
        '
        'Label70
        '
        Me.Label70.AutoSize = True
        Me.Label70.Location = New System.Drawing.Point(567, 285)
        Me.Label70.Name = "Label70"
        Me.Label70.Size = New System.Drawing.Size(61, 13)
        Me.Label70.TabIndex = 17
        Me.Label70.Text = "Texto Libre"
        '
        'txtTextoLibre
        '
        Me.txtTextoLibre.Location = New System.Drawing.Point(570, 302)
        Me.txtTextoLibre.Multiline = True
        Me.txtTextoLibre.Name = "txtTextoLibre"
        Me.txtTextoLibre.Size = New System.Drawing.Size(175, 97)
        Me.txtTextoLibre.TabIndex = 16
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.txtVolumen)
        Me.Panel4.Controls.Add(Me.txtPeso)
        Me.Panel4.Controls.Add(Me.Label67)
        Me.Panel4.Controls.Add(Me.Label66)
        Me.Panel4.Controls.Add(Me.Label21)
        Me.Panel4.Controls.Add(Me.cmbTipoImp)
        Me.Panel4.Controls.Add(Me.GroupBox3)
        Me.Panel4.Controls.Add(Me.GroupBox2)
        Me.Panel4.Enabled = False
        Me.Panel4.Location = New System.Drawing.Point(654, 81)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(310, 196)
        Me.Panel4.TabIndex = 15
        '
        'txtVolumen
        '
        Me.txtVolumen.Location = New System.Drawing.Point(94, 54)
        Me.txtVolumen.Name = "txtVolumen"
        Me.txtVolumen.Size = New System.Drawing.Size(179, 21)
        Me.txtVolumen.TabIndex = 6
        '
        'txtPeso
        '
        Me.txtPeso.Location = New System.Drawing.Point(94, 30)
        Me.txtPeso.Name = "txtPeso"
        Me.txtPeso.Size = New System.Drawing.Size(179, 21)
        Me.txtPeso.TabIndex = 5
        '
        'Label67
        '
        Me.Label67.AutoSize = True
        Me.Label67.Location = New System.Drawing.Point(10, 57)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(47, 13)
        Me.Label67.TabIndex = 4
        Me.Label67.Text = "Volumen"
        '
        'Label66
        '
        Me.Label66.AutoSize = True
        Me.Label66.Location = New System.Drawing.Point(10, 36)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(30, 13)
        Me.Label66.TabIndex = 3
        Me.Label66.Text = "Peso"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.rdbSobreVenta)
        Me.GroupBox2.Controls.Add(Me.rdbAgotarStock)
        Me.GroupBox2.Location = New System.Drawing.Point(138, 85)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(146, 53)
        Me.GroupBox2.TabIndex = 10
        Me.GroupBox2.TabStop = False
        '
        'rdbSobreVenta
        '
        Me.rdbSobreVenta.AutoSize = True
        Me.rdbSobreVenta.Checked = True
        Me.rdbSobreVenta.Location = New System.Drawing.Point(6, 29)
        Me.rdbSobreVenta.Name = "rdbSobreVenta"
        Me.rdbSobreVenta.Size = New System.Drawing.Size(81, 17)
        Me.rdbSobreVenta.TabIndex = 0
        Me.rdbSobreVenta.TabStop = True
        Me.rdbSobreVenta.Text = "Sobreventa"
        Me.rdbSobreVenta.UseVisualStyleBackColor = True
        '
        'rdbAgotarStock
        '
        Me.rdbAgotarStock.AutoSize = True
        Me.rdbAgotarStock.Location = New System.Drawing.Point(6, 10)
        Me.rdbAgotarStock.Name = "rdbAgotarStock"
        Me.rdbAgotarStock.Size = New System.Drawing.Size(118, 17)
        Me.rdbAgotarStock.TabIndex = 0
        Me.rdbAgotarStock.Text = "Hasta Agotar Stock"
        Me.rdbAgotarStock.UseVisualStyleBackColor = True
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.Label54)
        Me.Panel3.Controls.Add(Me.txtUltCos)
        Me.Panel3.Controls.Add(Me.Label68)
        Me.Panel3.Controls.Add(Me.txtXDelta)
        Me.Panel3.Controls.Add(Me.txtPorAporte)
        Me.Panel3.Controls.Add(Me.txtPorCM)
        Me.Panel3.Controls.Add(Me.Label74)
        Me.Panel3.Controls.Add(Me.Label65)
        Me.Panel3.Controls.Add(Me.txtDolar)
        Me.Panel3.Controls.Add(Me.Label64)
        Me.Panel3.Controls.Add(Me.Label55)
        Me.Panel3.Controls.Add(Me.txtCosProm)
        Me.Panel3.Controls.Add(Me.txtTipoDelta)
        Me.Panel3.Controls.Add(Me.Label56)
        Me.Panel3.Controls.Add(Me.lblDelta)
        Me.Panel3.Controls.Add(Me.txtCosto)
        Me.Panel3.Controls.Add(Me.txtValorDelta)
        Me.Panel3.Location = New System.Drawing.Point(6, 299)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(558, 100)
        Me.Panel3.TabIndex = 14
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Location = New System.Drawing.Point(12, 66)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(46, 13)
        Me.Label54.TabIndex = 9
        Me.Label54.Text = "Anterior"
        '
        'txtUltCos
        '
        Me.txtUltCos.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.txtUltCos.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUltCos.Location = New System.Drawing.Point(82, 63)
        Me.txtUltCos.Name = "txtUltCos"
        Me.txtUltCos.Size = New System.Drawing.Size(100, 21)
        Me.txtUltCos.TabIndex = 8
        Me.txtUltCos.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label68
        '
        Me.Label68.AutoSize = True
        Me.Label68.Location = New System.Drawing.Point(197, 66)
        Me.Label68.Name = "Label68"
        Me.Label68.Size = New System.Drawing.Size(44, 13)
        Me.Label68.TabIndex = 7
        Me.Label68.Text = "XDELTA"
        '
        'txtXDelta
        '
        Me.txtXDelta.Location = New System.Drawing.Point(254, 63)
        Me.txtXDelta.Name = "txtXDelta"
        Me.txtXDelta.Size = New System.Drawing.Size(100, 21)
        Me.txtXDelta.TabIndex = 6
        '
        'txtPorAporte
        '
        Me.txtPorAporte.Location = New System.Drawing.Point(254, 36)
        Me.txtPorAporte.Name = "txtPorAporte"
        Me.txtPorAporte.Size = New System.Drawing.Size(100, 21)
        Me.txtPorAporte.TabIndex = 5
        '
        'txtPorCM
        '
        Me.txtPorCM.Enabled = False
        Me.txtPorCM.Location = New System.Drawing.Point(82, 36)
        Me.txtPorCM.Name = "txtPorCM"
        Me.txtPorCM.Size = New System.Drawing.Size(100, 21)
        Me.txtPorCM.TabIndex = 4
        '
        'Label74
        '
        Me.Label74.AutoSize = True
        Me.Label74.Location = New System.Drawing.Point(374, 41)
        Me.Label74.Name = "Label74"
        Me.Label74.Size = New System.Drawing.Size(67, 13)
        Me.Label74.TabIndex = 23
        Me.Label74.Text = "Dolar del Día"
        '
        'Label65
        '
        Me.Label65.AutoSize = True
        Me.Label65.Location = New System.Drawing.Point(197, 41)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(51, 13)
        Me.Label65.TabIndex = 3
        Me.Label65.Text = "%Aporte"
        '
        'txtDolar
        '
        Me.txtDolar.Location = New System.Drawing.Point(445, 36)
        Me.txtDolar.Name = "txtDolar"
        Me.txtDolar.ReadOnly = True
        Me.txtDolar.Size = New System.Drawing.Size(100, 21)
        Me.txtDolar.TabIndex = 22
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.Location = New System.Drawing.Point(14, 41)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(33, 13)
        Me.Label64.TabIndex = 2
        Me.Label64.Text = "%CM"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.txtDescripAlter)
        Me.Panel2.Controls.Add(Me.Label69)
        Me.Panel2.Controls.Add(Me.txtCodigo)
        Me.Panel2.Controls.Add(Me.Label61)
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Controls.Add(Me.txtDescripcion)
        Me.Panel2.Controls.Add(Me.txtCodsap)
        Me.Panel2.Controls.Add(Me.rdbComience)
        Me.Panel2.Controls.Add(Me.dtpFechaIng)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.rdbContenga)
        Me.Panel2.Location = New System.Drawing.Point(6, 6)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(853, 62)
        Me.Panel2.TabIndex = 13
        '
        'txtDescripAlter
        '
        Me.txtDescripAlter.Location = New System.Drawing.Point(507, 31)
        Me.txtDescripAlter.Name = "txtDescripAlter"
        Me.txtDescripAlter.Size = New System.Drawing.Size(343, 21)
        Me.txtDescripAlter.TabIndex = 14
        '
        'Label69
        '
        Me.Label69.AutoSize = True
        Me.Label69.Location = New System.Drawing.Point(442, 35)
        Me.Label69.Name = "Label69"
        Me.Label69.Size = New System.Drawing.Size(61, 13)
        Me.Label69.TabIndex = 13
        Me.Label69.Text = "Descr.Alter"
        '
        'TabPage6
        '
        Me.TabPage6.Controls.Add(Me.btn_Oci)
        Me.TabPage6.Controls.Add(Me.Label12)
        Me.TabPage6.Controls.Add(Me.Panel5)
        Me.TabPage6.Controls.Add(Me.GroupBox7)
        Me.TabPage6.Controls.Add(Me.btn_Anotación)
        Me.TabPage6.Controls.Add(Me.TabControl1)
        Me.TabPage6.Controls.Add(Me.lblMensaje)
        Me.TabPage6.Controls.Add(Me.GroupBox5)
        Me.TabPage6.Controls.Add(Me.GroupBox4)
        Me.TabPage6.Controls.Add(Me.cmbUnidad)
        Me.TabPage6.Controls.Add(Me.Label57)
        Me.TabPage6.Controls.Add(Me.Label2)
        Me.TabPage6.Controls.Add(Me.txtCodBarra)
        Me.TabPage6.Controls.Add(Me.cmbTipoAlmacenamiento)
        Me.TabPage6.Controls.Add(Me.Label13)
        Me.TabPage6.Controls.Add(Me.cmbFamilia)
        Me.TabPage6.Location = New System.Drawing.Point(4, 22)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Size = New System.Drawing.Size(1011, 497)
        Me.TabPage6.TabIndex = 2
        Me.TabPage6.Text = "Eliminados"
        Me.TabPage6.UseVisualStyleBackColor = True
        '
        'btn_Oci
        '
        Me.btn_Oci.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Oci.Image = Global.Convenios_New.My.Resources.Resources.search
        Me.btn_Oci.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btn_Oci.Location = New System.Drawing.Point(507, 243)
        Me.btn_Oci.Name = "btn_Oci"
        Me.btn_Oci.Size = New System.Drawing.Size(68, 53)
        Me.btn_Oci.TabIndex = 0
        Me.btn_Oci.Text = "O.C.I"
        Me.btn_Oci.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btn_Oci.UseVisualStyleBackColor = True
        Me.btn_Oci.Visible = False
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.txtUbicacion)
        Me.Panel5.Controls.Add(Me.Label20)
        Me.Panel5.Controls.Add(Me.txtVidaUtil)
        Me.Panel5.Controls.Add(Me.txtConVence)
        Me.Panel5.Controls.Add(Me.Label19)
        Me.Panel5.Controls.Add(Me.txtCatAct)
        Me.Panel5.Controls.Add(Me.txtUndPed)
        Me.Panel5.Controls.Add(Me.Label48)
        Me.Panel5.Controls.Add(Me.chkGobierno)
        Me.Panel5.Controls.Add(Me.Label18)
        Me.Panel5.Controls.Add(Me.txtUndEmb)
        Me.Panel5.Controls.Add(Me.chkReembalaje)
        Me.Panel5.Controls.Add(Me.Label17)
        Me.Panel5.Controls.Add(Me.chkAsegura)
        Me.Panel5.Controls.Add(Me.txtUMinima)
        Me.Panel5.Controls.Add(Me.chkNumSer)
        Me.Panel5.Controls.Add(Me.Label16)
        Me.Panel5.Controls.Add(Me.chkMoneda)
        Me.Panel5.Controls.Add(Me.chkRDimeiggs)
        Me.Panel5.Controls.Add(Me.chkCalidad)
        Me.Panel5.Controls.Add(Me.Label15)
        Me.Panel5.Controls.Add(Me.chkColateral)
        Me.Panel5.Controls.Add(Me.chkRDimerc)
        Me.Panel5.Location = New System.Drawing.Point(679, 108)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(329, 242)
        Me.Panel5.TabIndex = 7
        '
        'btn_Anotación
        '
        Me.btn_Anotación.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Anotación.Image = Global.Convenios_New.My.Resources.Resources.Tips
        Me.btn_Anotación.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btn_Anotación.Location = New System.Drawing.Point(581, 243)
        Me.btn_Anotación.Name = "btn_Anotación"
        Me.btn_Anotación.Size = New System.Drawing.Size(68, 53)
        Me.btn_Anotación.TabIndex = 0
        Me.btn_Anotación.Text = "Anotación"
        Me.btn_Anotación.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btn_Anotación.UseVisualStyleBackColor = True
        Me.btn_Anotación.Visible = False
        '
        'frmFichaProducto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1125, 592)
        Me.Controls.Add(Me.TabControl2)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.KeyPreview = True
        Me.Name = "frmFichaProducto"
        Me.ShowIcon = False
        Me.Text = "Ficha de Producto"
        Me.Panel1.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        CType(Me.dgvComision, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.TabControl2.ResumeLayout(False)
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage4.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.TabPage6.ResumeLayout(False)
        Me.TabPage6.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dtpFechaIng As System.Windows.Forms.DateTimePicker
    Friend WithEvents cmbCM As System.Windows.Forms.ComboBox
    Friend WithEvents cmbTipoProd As System.Windows.Forms.ComboBox
    Friend WithEvents cmbUnidad As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtCodigo As System.Windows.Forms.TextBox
    Friend WithEvents rdbComience As System.Windows.Forms.RadioButton
    Friend WithEvents rdbContenga As System.Windows.Forms.RadioButton
    Friend WithEvents txtProdDescripCorta As System.Windows.Forms.TextBox
    Friend WithEvents txtCodUni As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents dtpFchaVig As System.Windows.Forms.DateTimePicker
    Friend WithEvents cmbComision As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents dgvComision As System.Windows.Forms.DataGridView
    Friend WithEvents CmdAgregar As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmbRotacion As System.Windows.Forms.ComboBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents cmbTipoAlmacenamiento As System.Windows.Forms.ComboBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents cmbFamilia As System.Windows.Forms.ComboBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents cmbMarca As System.Windows.Forms.ComboBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents cmbLinea As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents cmbEstado As System.Windows.Forms.ComboBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents chkCalidad As System.Windows.Forms.CheckBox
    Friend WithEvents chkMPropia As System.Windows.Forms.CheckBox
    Friend WithEvents chkNumSer As System.Windows.Forms.CheckBox
    Friend WithEvents chkAsegura As System.Windows.Forms.CheckBox
    Friend WithEvents chkReembalaje As System.Windows.Forms.CheckBox
    Friend WithEvents chkMoneda As System.Windows.Forms.CheckBox
    Friend WithEvents chkColateral As System.Windows.Forms.CheckBox
    Friend WithEvents chkImportado As System.Windows.Forms.CheckBox
    Friend WithEvents chkGobierno As System.Windows.Forms.CheckBox
    Friend WithEvents chkRDimeiggs As System.Windows.Forms.CheckBox
    Friend WithEvents chkRDimerc As System.Windows.Forms.CheckBox
    Friend WithEvents txtCatAct As System.Windows.Forms.TextBox
    Friend WithEvents txtVidaUtil As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txtConVence As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents txtUndPed As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txtUndEmb As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtUMinima As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents rdbContraPedido As System.Windows.Forms.RadioButton
    Friend WithEvents rdbStock As System.Windows.Forms.RadioButton
    Friend WithEvents cmbTipoImp As System.Windows.Forms.ComboBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents txtTipoDelta As System.Windows.Forms.TextBox
    Friend WithEvents txtValorDelta As System.Windows.Forms.TextBox
    Friend WithEvents lblDelta As System.Windows.Forms.Label
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents txtStkComAntof As System.Windows.Forms.TextBox
    Friend WithEvents txtFecInv As System.Windows.Forms.TextBox
    Friend WithEvents txtIntCd As System.Windows.Forms.TextBox
    Friend WithEvents txtStkCri As System.Windows.Forms.TextBox
    Friend WithEvents txtStkDimerc As System.Windows.Forms.TextBox
    Friend WithEvents txtMinCd As System.Windows.Forms.TextBox
    Friend WithEvents txtTtoLoc As System.Windows.Forms.TextBox
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents txtStkInt As System.Windows.Forms.TextBox
    Friend WithEvents txtUltInv As System.Windows.Forms.TextBox
    Friend WithEvents txtCriCd As System.Windows.Forms.TextBox
    Friend WithEvents txtSMaximo As System.Windows.Forms.TextBox
    Friend WithEvents txtStkPenDmg As System.Windows.Forms.TextBox
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents txtStkDimeiggs As System.Windows.Forms.TextBox
    Friend WithEvents txtOcLoc As System.Windows.Forms.TextBox
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents txtMaxCd As System.Windows.Forms.TextBox
    Friend WithEvents txtSMinimo As System.Windows.Forms.TextBox
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents txtStkAse As System.Windows.Forms.TextBox
    Friend WithEvents txtFscLoc As System.Windows.Forms.TextBox
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents txtCFCosto As System.Windows.Forms.TextBox
    Friend WithEvents txtCFFecha As System.Windows.Forms.TextBox
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents txtCosto As System.Windows.Forms.TextBox
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents txtCompraAnt As System.Windows.Forms.TextBox
    Friend WithEvents txtCosProm As System.Windows.Forms.TextBox
    Friend WithEvents txtReposicion As System.Windows.Forms.TextBox
    Friend WithEvents txtUCDMG As System.Windows.Forms.TextBox
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents txtUcPNB As System.Windows.Forms.TextBox
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents txtUbicacion As System.Windows.Forms.TextBox
    Friend WithEvents txtCodBarra As System.Windows.Forms.TextBox
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents cmdAlter As System.Windows.Forms.Button
    Friend WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents GroupBox6 As GroupBox
    Friend WithEvents rdbMPInventarioNO As RadioButton
    Friend WithEvents rdbMPInventarioSI As RadioButton
    Friend WithEvents GroupBox7 As GroupBox
    Friend WithEvents chkToxico As CheckBox
    Friend WithEvents chkNoAplica As CheckBox
    Friend WithEvents chkCorrosivo As CheckBox
    Friend WithEvents chkImflamable As CheckBox
    Friend WithEvents lblMensaje As Label
    Friend WithEvents txtPaleta As TextBox
    Friend WithEvents Label59 As Label
    Friend WithEvents Label60 As Label
    Friend WithEvents cmbNegocio As ComboBox
    Friend WithEvents lblRespuesta As Label
    Friend WithEvents Label61 As Label
    Friend WithEvents txtCodsap As TextBox
    Friend WithEvents txtGrupo As TextBox
    Friend WithEvents btnRelprod As Button
    Friend WithEvents TabControl2 As TabControl
    Friend WithEvents TabPage4 As TabPage
    Friend WithEvents TabPage6 As TabPage
    Friend WithEvents Panel2 As Panel
    Friend WithEvents cmbProvPrinc As ComboBox
    Friend WithEvents Label62 As Label
    Friend WithEvents Label63 As Label
    Friend WithEvents Panel3 As Panel
    Friend WithEvents Panel4 As Panel
    Friend WithEvents Panel5 As Panel
    Friend WithEvents btn_Oci As Button
    Friend WithEvents btn_ProvProd As Button
    Friend WithEvents btn_ListaPrecio As Button
    Friend WithEvents btn_Anotación As Button
    Friend WithEvents btn_Historia As Button
    Friend WithEvents btn_Grabar As Button
    Friend WithEvents txtPorAporte As TextBox
    Friend WithEvents txtPorCM As TextBox
    Friend WithEvents Label65 As Label
    Friend WithEvents Label64 As Label
    Friend WithEvents txtVolumen As TextBox
    Friend WithEvents txtPeso As TextBox
    Friend WithEvents Label67 As Label
    Friend WithEvents Label66 As Label
    Friend WithEvents cmbCategoria As ComboBox
    Friend WithEvents Label68 As Label
    Friend WithEvents txtXDelta As TextBox
    Friend WithEvents txtDescripAlter As TextBox
    Friend WithEvents Label69 As Label
    Friend WithEvents btnNegocio As Button
    Friend WithEvents Label70 As Label
    Friend WithEvents txtTextoLibre As TextBox
    Friend WithEvents cmbSubgrupo As ComboBox
    Friend WithEvents btnSubgrupo As Button
    Friend WithEvents cmbNegDmc As ComboBox
    Friend WithEvents Label71 As Label
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents rdbSobreVenta As RadioButton
    Friend WithEvents rdbAgotarStock As RadioButton
    Friend WithEvents txtUltCos As TextBox
    Friend WithEvents Label54 As Label
    Friend WithEvents Button1 As Button
    Friend WithEvents Label73 As Label
    Friend WithEvents Label72 As Label
    Friend WithEvents txtCostoSanti As TextBox
    Friend WithEvents txtCostoAnto As TextBox
    Friend WithEvents Label74 As Label
    Friend WithEvents txtDolar As TextBox
    Friend WithEvents txtMarca As TextBox
    Friend WithEvents txtCategoria As TextBox
    Friend WithEvents Label75 As Label
    Friend WithEvents txtCDAntofagasta As TextBox
    Friend WithEvents Label76 As Label
    Friend WithEvents Label77 As Label
    Friend WithEvents txtCDNoviciado As TextBox
    Friend WithEvents txtCDSantiago As TextBox
End Class
