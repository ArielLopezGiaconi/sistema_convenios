﻿Imports System.Data.OracleClient
Imports System.Net.Mail


Public Class frmFichaProducto
    Dim dtComisiones, dtCliente As New DataTable
    Dim bd As New Conexion
    Dim PuntoControl As New PuntoControl
    Dim BolEstaProducto As Boolean
    Dim BolProducto As Boolean
    Dim sql, desProOriginal, strSobreventa As String
    Dim costoOriginal As Double
    Dim cargo As Boolean = False
    '---- Variables para Actualizacion de Linea, Familia, marca y Modelo ----
    Dim PIntLinea As Integer
    Dim PIntFamilia As Integer
    Dim PIntMarca As Integer
    Dim PIntModelo As Integer
    Dim codEstado As Integer
    Dim bolCambiaCosto As Boolean
    Dim LngCostoOriginal As Double = 0
    Public StrDesOriginal, StrEstado, PStrDespro As String
    Public LngCodlin As Double
    Public BolSucursal As Boolean


    Private Sub frmFichaProducto_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.Location = New Point(0, 0)
        TabControl2.Appearance = TabAppearance.FlatButtons
        TabControl2.ItemSize = New Size(0, 1)
        TabControl2.SizeMode = TabSizeMode.Fixed
        Cursor.Current = Cursors.WaitCursor
        StartPosition = FormStartPosition.Manual
        btn_ListaPrecio.Visible = False
        btn_ProvProd.Visible = False
        btn_Oci.Visible = False
        lblMensaje.Visible = False
        rdbMPInventarioSI.Checked = True
        cargaCombo()
        PuntoControl.RegistroUso("75")
        txtCosProm.Enabled = False
        txtPorCM.Enabled = False
        txtValorDelta.Enabled = False
        txtTipoDelta.Enabled = False
        'txtFeUlCo.Enabled = False
        'txtUCompra.Enabled = False

        If Globales.detDerUsu(Globales.user, 317) Then
            txtStkAse.Enabled = True
        Else
            txtStkAse.Enabled = False
        End If

        'If Globales.detDerUsu(Globales.user, 232) Then
        '    dgvSucursal.Enabled = True
        'Else
        '    dgvSucursal.Enabled = False
        'End If

        Me.Show()
        Application.DoEvents()
        txtCodigo.Focus()
    End Sub
    Private Sub cargaCombo()
        Try
            bd.open_dimerc()

            dtComisiones.Columns.Add("Fecha", Type.GetType("System.DateTime"))
            dtComisiones.Columns.Add("Tipo", Type.GetType("System.String"))
            dtComisiones.Columns.Add("%Comi", Type.GetType("System.Double"))
            sql = "select desval, codval from de_dominio where coddom = '201' and valnum = 1"
            Dim dt As DataTable = bd.sqlSelect(sql)
            cmbRotacion.DataSource = dt
            cmbRotacion.ValueMember = "codval"
            cmbRotacion.DisplayMember = "desval"
            cmbRotacion.SelectedIndex = -1
            cmbRotacion.Refresh()

            sql = "select desval, codval from de_dominio where coddom = '275' order by desval"
            dt = bd.sqlSelect(sql)
            cmbComision.DataSource = dt
            cmbComision.ValueMember = "codval"
            cmbComision.DisplayMember = "desval"
            cmbComision.SelectedIndex = -1
            cmbComision.Refresh()

            sql = "select desval, codval from de_dominio where coddom = '246' order by desval"
            dt = bd.sqlSelect(sql)
            cmbCategoria.DataSource = dt
            cmbCategoria.ValueMember = "codval"
            cmbCategoria.DisplayMember = "desval"
            cmbCategoria.SelectedIndex = -1
            cmbCategoria.Refresh()

            sql = "select desval, codval from de_dominio where coddom = '861' order by codval"
            dt = bd.sqlSelect(sql)
            cmbCM.DataSource = dt
            cmbCM.ValueMember = "codval"
            cmbCM.DisplayMember = "desval"
            cmbCM.SelectedIndex = -1
            cmbCM.Refresh()

            sql = "select id, descripcion from ma_negocio_pricing"
            dt = bd.sqlSelect(sql)
            cmbNegocio.DataSource = dt
            cmbNegocio.ValueMember = "id"
            cmbNegocio.DisplayMember = "descripcion"
            cmbNegocio.SelectedIndex = -1
            cmbNegocio.Refresh()

            sql = "select id, descripcion from ma_negocio_subgrupo"
            dt = bd.sqlSelect(sql)
            cmbSubgrupo.DataSource = dt
            cmbSubgrupo.ValueMember = "id"
            cmbSubgrupo.DisplayMember = "descripcion"
            cmbSubgrupo.SelectedIndex = -1
            cmbSubgrupo.Refresh()

            sql = "select desval, codval from de_dominio where coddom = '358' order by codval"
            dt = bd.sqlSelect(sql)
            cmbTipoProd.DataSource = dt
            cmbTipoProd.ValueMember = "codval"
            cmbTipoProd.DisplayMember = "desval"
            cmbTipoProd.SelectedIndex = -1
            cmbTipoProd.Refresh()

            Dim dt2 As New DataTable
            'dt2.Columns.Add("nomimp", GetType(String))
            'dt2.Columns.Add("codimp", GetType(Double))
            sql = " select nomimp,codimp from ma_impuestos "
            'dt2.Merge(bd.sqlSelect(sql))
            dt2 = bd.sqlSelect(sql)
            dt2.Rows.Add("SIN IMPUESTO", 0)
            cmbTipoImp.DataSource = dt2
            cmbTipoImp.ValueMember = "codimp"
            cmbTipoImp.DisplayMember = "nomimp"
            cmbTipoImp.SelectedIndex = -1
            cmbTipoImp.Refresh()

            sql = "select codlin, deslin from ma_lineapr order by deslin "
            dt = bd.sqlSelect(sql)
            cmbLinea.DataSource = dt
            cmbLinea.ValueMember = "codlin"
            cmbLinea.DisplayMember = "deslin"
            cmbLinea.SelectedIndex = -1
            cmbLinea.Refresh()

            sql = "select codfam,desfam from ma_familia order by desfam "
            dt = bd.sqlSelect(sql)
            cmbFamilia.DataSource = dt
            cmbFamilia.ValueMember = "codfam"
            cmbFamilia.DisplayMember = "desfam"
            cmbFamilia.SelectedIndex = -1
            cmbFamilia.Refresh()

            'sql = "select codmar,desmar from ma_marcapr order by desmar "
            'dt = bd.sqlSelect(sql)
            'cmbMarca.DataSource = dt
            'cmbMarca.ValueMember = "codmar"
            'cmbMarca.DisplayMember = "desmar"
            'cmbMarca.SelectedIndex = -1
            'cmbMarca.Refresh()

            sql = "select desval, codval from de_dominio where coddom = '18' order by desval"
            dt = bd.sqlSelect(sql)
            cmbUnidad.DataSource = dt
            cmbUnidad.ValueMember = "codval"
            cmbUnidad.DisplayMember = "desval"
            cmbUnidad.SelectedIndex = -1
            cmbUnidad.Refresh()

            sql = "select desval, codval from de_dominio where coddom = '20' order by desval"
            dt = bd.sqlSelect(sql)
            cmbEstado.DataSource = dt
            cmbEstado.ValueMember = "codval"
            cmbEstado.DisplayMember = "desval"
            cmbEstado.SelectedIndex = -1
            cmbEstado.Refresh()

            sql = "select desval, codval from de_dominio where coddom = '30' order by desval"
            dt = bd.sqlSelect(sql)
            cmbTipoAlmacenamiento.DataSource = dt
            cmbTipoAlmacenamiento.ValueMember = "codval"
            cmbTipoAlmacenamiento.DisplayMember = "desval"
            cmbTipoAlmacenamiento.SelectedIndex = -1
            cmbTipoAlmacenamiento.Refresh()

            sql = "select desval, codval from de_dominio where coddom = '1002' order by desval"
            dt = bd.sqlSelect(sql)
            cmbNegDmc.DataSource = dt
            cmbNegDmc.ValueMember = "codval"
            cmbNegDmc.DisplayMember = "desval"
            cmbNegDmc.SelectedIndex = -1
            cmbNegDmc.Refresh()

            txtCodigo.Focus()

        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub txtDescripcion_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtDescripcion.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            PuntoControl.RegistroUso("78")
            If Trim(txtDescripcion.Text) = "" And Trim(txtCodigo.Text.ToString.Trim) = "" Then
                txtCodigo.Focus()
                Exit Sub
            End If
            If Trim$(txtDescripcion.Text) <> "" And Trim$(txtCodigo.Text.ToString.Trim) = "" Then
                Dim frmBuscar As New frmBuscarProducto(txtDescripcion.Text, IIf(rdbComience.Checked = True, True, False))
                If frmBuscar.dgvDatos.RowCount > 0 Then
                    frmBuscar.ShowDialog()
                    txtDescripcion.Text = frmBuscar.dgvDatos.Item("Descripcion", frmBuscar.dgvDatos.CurrentRow.Index).Value.ToString
                    txtCodigo.Text = frmBuscar.dgvDatos.Item("Codigo", frmBuscar.dgvDatos.CurrentRow.Index).Value.ToString
                    traeDatos()
                End If
            End If
            If Trim(txtDescripcion.Text) = "" Or Trim(txtDescripcion.Text) = "  " Then
                MessageBox.Show("Descripción No puede quedar en Blanco", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            End If
            cmbUnidad.Focus()
        End If
    End Sub

    Private Sub buscarProducto()
        Try
            'Globales.basePrueba = False
            bd.open_dimerc()
            Dim dt As New DataTable
            If Not Trim(txtCodigo.Text.ToString.Trim) = "" Then
                txtCodigo.Text = Globales.BuscaCodigoDimerc(txtCodigo.Text.ToString.Trim)
                ''''''''''' Inserto para buscar por cualquier Código '''''''''''''
                If Trim(txtCodigo.Text.ToString.Trim) = "" Then
                    MessageBox.Show("Producto No Existe", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtCodigo.Focus()
                End If
                Cursor.Current = Cursors.WaitCursor
                traeDatos()
                Cursor.Current = Cursors.Arrow
                If BolEstaProducto = False Then
                    sql = "select pror_prod_id from cat_producto_organizacion@ora92 where PROR_CODIGO = '" & txtCodigo.Text.ToString.Trim & "'"
                    sql = sql & " and PROR_ORGA_ID = 8"
                    dt.Clear()
                    dt = bd.sqlSelect(sql)
                    If dt.Rows.Count > 0 Then
                        If MessageBox.Show("Producto No Existe. ¿Lo Crea?", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = System.Windows.Forms.DialogResult.Yes Then
                            txtStkAse.Enabled = True
                            txtMaxCd.Enabled = True
                            txtMinCd.Enabled = True
                            txtCriCd.Enabled = True
                            txtIntCd.Enabled = True
                            txtCodUni.Text = CStr(dt.Rows(0).Item("pror_prod_id"))
                            'cargaSucursal()
                            rdbStock.Enabled = True
                            rdbContraPedido.Enabled = True
                            txtDescripcion.Focus()
                            cmbTipoImp.SelectedValue = 0
                            LngCostoOriginal = 0
                        Else
                            txtCodigo.Focus()
                        End If
                    Else
                        MessageBox.Show("Producto no se puede crear. No existe en Maestra Unificada", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        txtCodigo.Focus()
                    End If
                End If
            Else
                If Trim(txtDescripcion.Text) <> "" Then
                    txtDescripcion.Focus()
                End If
            End If
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
            'Globales.basePrueba = True
        End Try


    End Sub
    Private Sub traeDatos()
        Try
            bd.open_dimerc()
            'limpiar()
            BolEstaProducto = False
            Dim dtaux, dtaux2, dt, dtAuxiliar, dt1, dt2 As New DataTable
            sql = "select /*+ ordered use_nl(a,b,c)*/ "
            sql = sql & "a.codpro, a.desmar, a.despro,a.codlin,a.codneg,a.codfam,nvl(a.ultinv,0) ultinv,a.fecinv, nvl(a.con_vencimiento,'N') convence, nvl(control_calidad,'N') control_calidad, nvl(get_vidautil(" & Globales.empresa.ToString & ",a.codpro,247),0) vida_util,"
            sql = sql & "a.codmar,a.codmod,nvl(a.codbar,' ') codbar,a.coduni, nvl(d.codval,0) claspro,a.tipoprod, nvl(a.costo_corp,0) costo_corp, nvl(a.cosprom_corp,0) cosprom_corp, "
            sql = sql & "nvl(a.stkfsc,0) stkfsc,nvl(a.stkcom,0) stkcom,nvl(a.stkpen,0) stkpen,nvl(a.stkpen_dmg,0) stkpen_dmg,nvl(a.costo,0) costo,nvl(a.ultcos,0) ultcos,nvl(a.fecing,'01/01/2000') fecing,nvl(a.asegur,'N') asegur,"
            sql = sql & "a.indsbr,a.unmivt,a.pedsto,a.moneda,a.colate,a.stkase, nvl(a.paleta_dc,'N') paletadc,nvl(a.paleta_dg,'N') paletadg,nvl(getcostofuturo(a.codpro,'C'),0) costofut, nvl(getcostofuturo(a.codpro,'F'),0) fechafut, decode(nvl(a.importado,'N'),'X','N',nvl(a.importado,'N')) importado, nvl(impuesto,0) impuesto, nvl(gobierno,'N') gobierno, "
            sql = sql & "nvl(getparambodega@ora92(2,'MAX',getcodigouni(a.codpro)),0) maximo,"
            sql = sql & "nvl(getparambodega@ora92(2,'MIN',getcodigouni(a.codpro)),0) minimo,"
            sql = sql & "nvl(getparambodega@ora92(2,'INT',getcodigouni(a.codpro)),0) stkint,"
            sql = sql & "nvl(getparambodega@ora92(2,'CRI',getcodigouni(a.codpro)),0) stkcri,"
            sql = sql & "GET_MPROPIA_INVENTARIO_DIMERC(a.codpro) mp_inventario, a.paleta_dc, "
            sql = sql & "nvl(a.maxcd,0) maxcd, nvl(a.mincd,0) mincd, nvl(a.cricd,0) cricd, nvl(a.intcd,0) intcd,nvl(a.codsubfam,0) codsubfam, nvl(a.codsubfam2,0) codsubfam2,"
            sql = sql & " b.desval, b.codval , a.estpro, nvl(a.reembj,0) reembj, a.pagcat, a.numfto, nvl(a.stock_dc,'N') stock_dc, nvl(a.stock_dg,'N') stock_dg, "
            '30      sql = sql & " a.numser, nvl(a.stkdis,0) stkdis, nvl(a.codigo_uni,'0') codigo_uni, nvl(a.stk_dimerc,0) stk_dimerc, nvl(getcategoria_producto(a.codpro),'D') categoria, nvl(GetCategoria_Prod_Borrador(a.codpro),'D') newcla, nvl(a.stk_dimeiggs,0) stk_dimeiggs, "
            '31      sql = sql & " nvl(e.codclasifica,0) catact, nvl(e.newcla,0) catbor, nvl(a.cosprom,0) cosprom,nvl(GETCATEGORIA_PROD_BORRADOR(a.codpro),'D') catborrador,nvl(GETCATEGORIA_PRODUCTO(a.codpro),'D') catactual "
            sql = sql & " nvl(a.numser,'N') numser, nvl(a.stkdis,0) stkdis, nvl(a.codigo_uni,'0') codigo_uni, nvl(a.stk_dimerc,0) stk_dimerc, nvl(getcategoria_producto(e.codemp,a.codpro),'D') categoria, nvl(GetCategoria_Prod_Borrador(e.codemp,a.codpro),'D') newcla, nvl(a.stk_dimeiggs,0) stk_dimeiggs, "
            sql = sql & " nvl(e.codclasifica,0) catact, nvl(e.newcla,0) catbor, nvl(a.cosprom,0) cosprom,nvl(GETCATEGORIA_PROD_BORRADOR(e.codemp,a.codpro),'D') catborrador,nvl(GETCATEGORIA_PRODUCTO(e.codemp,a.codpro),'D') catactual, a.codsap, a.desgru, a.dessubgru,
                          nvl(f.rutprv,0) rutprv, a.peso, a.volumen, a.xdelta"
            sql = sql & " from MA_PRODUCT a ,DE_DOMINIO b, re_codebar c, re_claspro d, re_claprod e, re_provpro f "
            sql = sql & " where a.codpro = '" & txtCodigo.Text.ToString.Trim & "'"
            sql = sql & "   and a.estpro <> 9"
            sql = sql & "   and b.coddom = 30"
            sql = sql & "   and b.codval(+) = to_char(a.tipalm) "
            sql = sql & "   and c.codpro(+) = a.codpro"
            'OJO CON ESTA LINEA SI EL PROGRAMA NO TRAE EL PRODUCTO A BUSCAR
            sql = sql & "   And c.codbar(+) = a.codbar "
            sql = sql & "   and d.codpro(+) = a.codpro"
            sql = sql & "   and d.codemp(+) = " & Globales.empresa.ToString
            sql = sql & "   and e.codpro(+) = a.codpro"
            sql = sql & "   and e.codgen(+) = 246"
            sql = sql & "   and e.codemp(+) = " & Globales.empresa.ToString
            sql = sql & "   and a.codpro = f.codpro(+)
                            and f.priori(+) = 1"
            dtCliente = bd.sqlSelect(sql)

            sql = "SELECT centro, codpro, cosprom FROM MA_COSTOSXCENTROS WHERE CENTRO IN(2001,2003) AND codpro = '" & txtCodigo.Text.ToString.Trim & "' and canal = 10 and codemp = 3"
            dt1 = bd.sqlSelect(sql)

            If dt1.Rows.Count > 0 Then
                txtCostoSanti.Text = dt1.Rows(0).Item("cosprom")
                If dt1.Rows.Count > 1 Then
                    txtCostoAnto.Text = dt1.Rows(1).Item("cosprom")
                End If
            End If

            sql = "select valpar from ma_paridad
                    WHERE CODMON = 1
                      AND TRUNC(FECPAR) = TRUNC(SYSDATE)"
            dt2 = bd.sqlSelect(sql)

            If dt.Rows.Count > 0 Then
                txtDolar.Text = dt2.Rows(0).Item("valpar")
            End If
            
            sql = "SELECT DESCRIP_ALTER, PORC_APORTE, ID_SUBGRUPO, ID_NEGOCIO, TEXTO_LIBRE
                   FROM MA_PRODUCT_NEGOCIO
                   WHERE CODPRO = '" & txtCodigo.Text & "'"
            bd.open_dimerc()
            Dim dtExtendido = bd.sqlSelect(sql)

            sql = "SELECT aporte
                   FROM ma_prod_aporte
                   WHERE CODPRO = '" & txtCodigo.Text & "'"
            Dim dtaporte = bd.sqlSelect(sql)
            If dtCliente.Rows.Count > 0 Then
                If CStr(Trim(dtCliente.Rows(0).Item("codbar"))) <> "" Then
                    BolProducto = True
                End If
                cmbLinea.SelectedValue = Convert.ToDouble(dtCliente.Rows(0).Item("codlin"))
                BolEstaProducto = True
                sql = "Select * from re_prodalt where codpro = '" & txtCodigo.Text.ToString.Trim & "'"
                sql = sql & " and codemp = " & Globales.empresa.ToString
                dtaux.Clear()
                dtaux = bd.sqlSelect(sql)
                If dtaux.Rows.Count > 0 Then
                    cmdAlter.Visible = True
                End If
                cmbRotacion.SelectedValue = dtCliente.Rows(0).Item("claspro")
                cmbTipoProd.SelectedValue = dtCliente.Rows(0).Item("tipoprod")
                txtCatAct.Text = dtCliente.Rows(0).Item("catactual")

                sql = "select getdominio(246, " & dtCliente.Rows(0).Item("catact") & ") valor from dual"
                dtaux.Clear()
                dtaux = bd.sqlSelect(sql)

                txtCategoria.Text = dtaux.Rows(0).Item("valor")

                'cmbCategoria.SelectedValue = dtCliente.Rows(0).Item("catact")

                txtDescripcion.Text = Globales.EsNulo(dtCliente.Rows(0).Item("despro"))
                txtXDelta.Text = Globales.EsNulo(dtCliente.Rows(0).Item("xdelta"))
                desProOriginal = Globales.EsNulo(dtCliente.Rows(0).Item("despro"))
                txtCodsap.Text = Globales.EsNulo(dtCliente.Rows(0).Item("codsap"))
                txtGrupo.Text = Globales.EsNulo(dtCliente.Rows(0).Item("desgru"))

                If dtExtendido.Rows.Count > 0 Then
                    cmbSubgrupo.SelectedValue = dtExtendido.Rows(0).Item("id_subgrupo")
                    cmbNegocio.SelectedValue = dtExtendido.Rows(0).Item("id_negocio")
                    txtDescripAlter.Text = Globales.EsNulo(dtExtendido.Rows(0).Item("descrip_alter"))
                    txtTextoLibre.Text = Globales.EsNulo(dtExtendido.Rows(0).Item("texto_libre"))
                End If
                If dtaporte.Rows.Count > 0 Then
                    txtPorAporte.Text = Globales.EsNulo(dtaporte.Rows(0).Item("aporte"))
                End If
                txtPeso.Text = Globales.EsNulo(dtCliente.Rows(0).Item("peso"))
                txtVolumen.Text = Globales.EsNulo(dtCliente.Rows(0).Item("volumen"))
                If dtCliente.Rows(0).Item("fecing").ToString = "" Then
                    dtpFechaIng.Value = "01/01/2000"
                Else
                    dtpFechaIng.Value = dtCliente.Rows(0).Item("fecing")
                End If

                txtPaleta.Text = dtCliente.Rows(0).Item("PALETA_DC")
                cmbEstado.SelectedValue = dtCliente.Rows(0).Item("estpro")
                codEstado = dtCliente.Rows(0).Item("estpro")

                If Val(dtCliente.Rows(0).Item("codigo_uni")) > 0 Then
                    sql = "select /* index (cat_prod_pk) */ prod_id, nvl(prod_descrip_corta,'SIN DESCRIPCION') prod_descrip_corta from cat_producto@ora92 where PROd_ID = '" & CStr(dtCliente.Rows(0).Item("codigo_uni")) & "'"
                    dtaux.Clear()
                    dtaux = bd.sqlSelect(sql)
                    If dtaux.Rows.Count > 0 Then
                        txtCodUni.Text = dtaux.Rows(0).Item("prod_id")
                        txtProdDescripCorta.Text = dtaux.Rows(0).Item("prod_descrip_corta")
                    End If
                Else
                    sql = "select pror_prod_id from cat_producto_organizacion@ora92 where PROR_CODIGO = '" & txtCodigo.Text.ToString.Trim & "'"
                    sql = sql & " and PROR_ORGA_ID = 8"
                    dtaux.Clear()
                    dtaux = bd.sqlSelect(sql)
                    If dtaux.Rows.Count > 0 Then
                        sql = "select /* index (cat_prod_pk) */ prod_id, prod_descrip_corta from cat_producto@ora92 where PROd_ID = '" & CStr(dtaux.Rows(0).Item("pror_prod_id")) & "'"
                        dtaux2.Clear()
                        dtaux2 = bd.sqlSelect(sql)
                        If dtaux2.Rows.Count > 0 Then
                            txtCodUni.Text = dtaux2.Rows(0).Item("prod_id")
                            txtProdDescripCorta.Text = Globales.EsNulo(dtaux2.Rows(0).Item("prod_descrip_corta"))
                        End If
                    End If
                End If

                cmbLinea.SelectedValue = dtCliente.Rows(0).Item("codlin")
                cmbFamilia.SelectedValue = dtCliente.Rows(0).Item("codfam")

                sql = "select a.nombre, a.rutprv from ma_proveed a, re_provpro b
                       where a.rutprv = b.rutprv
                       and b.codpro = '" & txtCodigo.Text & "'
                       and priori = 1
                       order by rutprv"
                dt = bd.sqlSelect(sql)
                cmbProvPrinc.DataSource = dt
                cmbProvPrinc.ValueMember = "rutprv"
                cmbProvPrinc.DisplayMember = "nombre"
                cmbProvPrinc.SelectedIndex = -1
                cmbProvPrinc.Refresh()
                If Val(dtCliente.Rows(0).Item("rutprv")) > 0 Then
                    cmbProvPrinc.SelectedValue = dtCliente.Rows(0).Item("rutprv")
                End If

                cmbNegDmc.SelectedValue = dtCliente.Rows(0).Item("codneg")
                'cmbSubGrupo.SelectedValue = dtCliente.Rows(0).Item("codsubfam2")
                If dtCliente.Rows(0).Item("desmar").ToString = "" Then
                    txtMarca.Text = "SIN MARCA"
                Else
                    txtMarca.Text = dtCliente.Rows(0).Item("desmar")
                End If
                If dtCliente.Rows(0).Item("mp_inventario").ToString = "S" Then
                    rdbMPInventarioSI.Checked = True
                ElseIf dtCliente.Rows(0).Item("mp_inventario").ToString = "N" Then
                    rdbMPInventarioNO.Checked = True
                End If
                txtCodBarra.Text = Globales.EsNuloConCero(dtCliente.Rows(0).Item("codbar"))
                cmbUnidad.SelectedValue = dtCliente.Rows(0).Item("coduni")
                cmbTipoAlmacenamiento.SelectedValue = dtCliente.Rows(0).Item("codval")

                'txtSFisico.Text = Globales.EsNuloConCero(dtCliente.Rows(0).Item("stkfsc"))
                txtStkAse.Text = Globales.EsNuloConCero(dtCliente.Rows(0).Item("stkase"))
                'txtSComprometido.Text = Globales.EsNuloConCero(dtCliente.Rows(0).Item("stkcom"))
                'txtsPendiente.Text = Globales.EsNuloConCero(dtCliente.Rows(0).Item("stkpen"))
                txtStkPenDmg.Text = Globales.EsNuloConCero(dtCliente.Rows(0).Item("stkpen_dmg"))
                'txtDisponible.Text = Globales.EsNuloConCero(dtCliente.Rows(0).Item("stkfsc")) - Globales.EsNuloConCero(dtCliente.Rows(0).Item("stkcom"))
                If Globales.empresa = 3 Then
                    txtCosto.Text = Globales.EsNuloConCero(dtCliente.Rows(0).Item("costo"))
                    txtCosProm.Text = Globales.EsNuloConCero(dtCliente.Rows(0).Item("cosprom"))
                    LngCostoOriginal = Convert.ToDouble(txtCosto.Text)
                ElseIf Globales.empresa = 6 Then
                    txtCosto.Text = Globales.EsNuloConCero(dtCliente.Rows(0).Item("costo_corp"))
                    txtCosProm.Text = Globales.EsNuloConCero(dtCliente.Rows(0).Item("cosprom_corp"))
                    LngCostoOriginal = Convert.ToDouble(txtCosto.Text)
                End If
                costoOriginal = Val(txtCosto.Text)
                txtUltCos.Text = Globales.EsNuloConCero(dtCliente.Rows(0).Item("ultcos"))
                cmbTipoImp.SelectedValue = dtCliente.Rows(0).Item("impuesto")

                strSobreventa = dtCliente.Rows(0).Item("indsbr")
                If dtCliente.Rows(0).Item("indsbr").ToString = "S" Then
                    rdbSobreVenta.Checked = True
                    rdbAgotarStock.Checked = False
                Else
                    rdbSobreVenta.Checked = False
                    rdbAgotarStock.Checked = True
                End If
                chkReembalaje.Checked = IIf(dtCliente.Rows(0).Item("reembj") = "0", False, True)
                chkImportado.Checked = IIf(dtCliente.Rows(0).Item("importado") = "N", False, True)
                chkNumSer.Checked = IIf(dtCliente.Rows(0).Item("numser") = "S", True, False)
                chkAsegura.Checked = IIf(dtCliente.Rows(0).Item("asegur") = "N", False, True)
                chkGobierno.Checked = IIf(dtCliente.Rows(0).Item("Gobierno") = "S", True, False)

                sql = " select nvl(stkcom,0) stkcom,nvl(stkmin,0) stkmin,nvl(stkmax,0) stkmax,nvl(stkcri,0) stkcri,nvl(stkint,0) stkint "
                sql = sql & "from re_bodprod_dimerc "
                sql = sql & "where codemp = " & Globales.empresa.ToString
                sql = sql & "  and codbod = 66"
                sql = sql & "  and codpro =  '" & txtCodigo.Text.ToString.Trim & "'"
                dtaux.Clear()
                dtaux = bd.sqlSelect(sql)
                If Not dtaux.Rows.Count = 0 Then
                    txtSMinimo.Text = Globales.EsNuloConCero(dtaux.Rows(0).Item("stkmin"))
                    txtSMaximo.Text = Globales.EsNuloConCero(dtaux.Rows(0).Item("stkmax"))
                    txtStkCri.Text = Globales.EsNuloConCero(dtaux.Rows(0).Item("stkcri"))
                    txtStkInt.Text = Globales.EsNuloConCero(dtaux.Rows(0).Item("stkint"))
                    txtStkComAntof.Text = Globales.EsNuloConCero(dtaux.Rows(0).Item("stkcom"))
                End If

                '|||||||||||||||||||||||||||||||||||||
                'AQUI AGREGO LA NUEVA CATEGORIA
                sql = " SELECT codclasifica CLASE, getdominio(codgen,codclasifica) DESCRIPCION FROM re_claprod "
                sql = sql & " where codpro = '" & txtCodigo.Text.ToString.Trim & "' "
                sql = sql & "  And codgen = 277 "
                dtAuxiliar.Clear()
                dtAuxiliar = bd.sqlSelect(sql)
                If dtAuxiliar.Rows.Count > 0 Then
                    If dtAuxiliar.Rows(0).Item("CLASE") = 0 Then
                        chkNoAplica.Checked = True
                    ElseIf dtAuxiliar.Rows(0).Item("CLASE") = 1 Then
                        chkCorrosivo.Checked = True
                    ElseIf dtAuxiliar.Rows(0).Item("CLASE") = 2 Then
                        chkImflamable.Checked = True
                    ElseIf dtAuxiliar.Rows(0).Item("CLASE") = 3 Then
                        chkToxico.Checked = True
                    End If
                Else
                    chkNoAplica.Checked = False
                    chkCorrosivo.Checked = False
                    chkImflamable.Checked = False
                    chkToxico.Checked = False
                End If
                
                txtMaxCd.Text = Globales.EsNuloConCero(dtCliente.Rows(0).Item("maxcd"))
                txtMinCd.Text = Globales.EsNuloConCero(dtCliente.Rows(0).Item("mincd"))
                txtCriCd.Text = Globales.EsNuloConCero(dtCliente.Rows(0).Item("cricd"))
                txtIntCd.Text = Globales.EsNuloConCero(dtCliente.Rows(0).Item("intcd"))
                txtVidaUtil.Text = IIf(Val(dtCliente.Rows(0).Item("vida_util")) = 0, "", dtCliente.Rows(0).Item("vida_util"))
                txtUMinima.Text = Globales.EsNulo(dtCliente.Rows(0).Item("unmivt"))
                txtStkDimerc.Text = Globales.EsNuloConCero(dtCliente.Rows(0).Item("stk_dimerc"))
                txtStkDimeiggs.Text = Globales.EsNuloConCero(dtCliente.Rows(0).Item("stk_dimeiggs"))
                txtUltInv.Text = Globales.EsNuloConCero(dtCliente.Rows(0).Item("ultinv"))
                txtFecInv.Text = Globales.EsNulo(dtCliente.Rows(0).Item("fecinv"))
                rdbStock.Checked = IIf(dtCliente.Rows(0).Item("pedsto") = "S", True, False)
                rdbContraPedido.Checked = IIf(dtCliente.Rows(0).Item("pedsto") = "S", False, True)
                txtCFCosto.Text = Globales.EsNuloConCero(dtCliente.Rows(0).Item("costofut"))
                txtCFFecha.Text = Globales.EsNulo(dtCliente.Rows(0).Item("fechafut"))


                sql = "select nvl(getstockprodtotal@ora92(2,7,a.codigo_uni),0) ttoloc"
                sql = sql & ",nvl(getstockprodtotal@ora92(2,2,a.codigo_uni),0) ocloc"
                sql = sql & ",nvl(getstkfsctot@ora92(2,a.codigo_uni),0) fscloc"
                sql = sql & ",nvl(ubicacionproductoorden@ora92(a.codigo_uni),0) ubic"
                sql = sql & " from MA_PRODUCT a "
                sql = sql & " where a.codpro = '" & txtCodigo.Text.ToString.Trim & "'"
                dtaux.Clear()
                dtaux = bd.sqlSelect(sql)
                If Not dtaux.Rows.Count = 0 Then
                    txtFscLoc.Text = Globales.EsNuloConCero(dtaux.Rows(0).Item("fscloc"))
                    txtOcLoc.Text = Globales.EsNuloConCero(dtaux.Rows(0).Item("ocloc"))
                    txtTtoLoc.Text = Globales.EsNuloConCero(dtaux.Rows(0).Item("ttoloc"))
                    txtUbicacion.Text = Globales.EsNuloConCero(dtaux.Rows(0).Item("ubic"))
                End If
                chkColateral.Checked = IIf(dtCliente.Rows(0).Item("colate") = "0", False, True)
                chkMoneda.Checked = IIf(dtCliente.Rows(0).Item("moneda") = "0", False, True)
                chkCalidad.Checked = IIf(dtCliente.Rows(0).Item("control_calidad") = "S", True, False)
                chkRDimerc.Checked = IIf(dtCliente.Rows(0).Item("stock_dc") = "S", True, False)

                btn_ListaPrecio.Visible = True
                btn_ProvProd.Visible = True
                btn_Oci.Visible = True
                btn_Grabar.Visible = True

                txtUndEmb.Text = Globales.EsNuloConCero(dtCliente.Rows(0).Item("pagcat"))
                txtUndPed.Text = Globales.EsNuloConCero(dtCliente.Rows(0).Item("numfto"))
                txtConVence.Text = Globales.EsNulo(dtCliente.Rows(0).Item("convence"))

                PIntLinea = Val(dtCliente.Rows(0).Item("codlin"))
                PIntFamilia = Val(dtCliente.Rows(0).Item("codfam"))
                PIntMarca = Val(dtCliente.Rows(0).Item("codmar"))

                sql = "Select a.rutprv, a.Cosrep from "
                sql = sql & " ma_listapr a, ma_proveed b, re_provpro c "
                sql = sql & " where c.codpro = '" & txtCodigo.Text.ToString.Trim & "'"
                sql = sql & " and c.codemp = " & Globales.empresa.ToString
                sql = sql & " and a.codpro = c.codpro "
                sql = sql & " and a.codemp = c.codemp "
                sql = sql & " and b.rutprv = c.rutprv "
                sql = sql & " and a.rutprv = c.rutprv "
                sql = sql & " and b.rutprv = a.rutprv"
                sql = sql & " order by a.fecven desc"
                dtaux.Clear()
                dtaux = bd.sqlSelect(sql)
                If dtaux.Rows.Count > 0 Then
                    sql = "select desvol, descli, desf30, desf60 from ma_proveed where"
                    sql = sql & " rutprv = " & Val(dtaux.Rows(0).Item("rutprv"))
                    dtaux2.Clear()
                    dtaux2 = bd.sqlSelect(sql)
                    If dtaux2.Rows.Count > 0 Then
                        txtReposicion.Text = ValDes(dtaux2.Rows(0).Item("Descli"), dtaux2.Rows(0).Item("Desvol"), dtaux2.Rows(0).Item("Desf30"), 0, 0, 0, 0, 0, 0, dtaux.Rows(0).Item("cosrep"))
                    Else
                        txtReposicion.Text = "0"
                    End If
                Else
                    txtReposicion.Text = "0"
                End If

                sql = "select nvl(SAP_GET_STOCK(3, 1, '" & txtCodigo.Text.ToString.Trim & "', 3), 0) santiago, nvl(SAP_GET_STOCK(3, 66, '" & txtCodigo.Text.ToString.Trim & "', 3), 0) antofagasta, nvl(SAP_GET_STOCK(3, 93, '" & txtCodigo.Text.ToString.Trim & "', 3), 0) satelite from dual"
                dtaux.Clear()
                dtaux = bd.sqlSelect(sql)

                txtCDAntofagasta.Text = dtaux.Rows(0).Item("antofagasta")
                txtCDNoviciado.Text = dtaux.Rows(0).Item("satelite")
                txtCDSantiago.Text = dtaux.Rows(0).Item("santiago")

                sql = "Select nvl(to_char(GETULTIMACOMPRA(" & Globales.empresa.ToString & ",'" & txtCodigo.Text.ToString.Trim & "','F')),'11/11/1111') fecemi,nvl(GETULTIMACOMPRA(" & Globales.empresa.ToString & ",'" & txtCodigo.Text.ToString.Trim & "','V'),0) coscom from dual"
                dtaux.Clear()
                dtaux = bd.sqlSelect(sql)
                If Not dtaux.Rows.Count = 0 Then
                    'If dtaux.Rows(0).Item("fecemi").ToString = "11/11/1111" Then
                    '    txtFeUlCo.Text = ""
                    'Else
                    '    txtFeUlCo.Text = dtaux.Rows(0).Item("fecemi")
                    'End If
                    'txtUCompra.Text = Globales.EsNuloConCero(dtaux.Rows(0).Item("coscom"))
                    If dtaux.Rows.Count >= 2 Then
                        txtCompraAnt.Text = Globales.EsNuloConCero(dtaux.Rows(1).Item("coscom"))
                    Else
                        txtCompraAnt.Text = "0"
                    End If
                End If
                sql = "Select nvl(cospnb,0) cospnb , nvl(cosdmg,0) cosdmg from re_prodrela where codpro =  '" & txtCodigo.Text.ToString.Trim & "'"
                dtaux.Clear()
                dtaux = bd.sqlSelect(sql)
                If dtaux.Rows.Count > 0 Then
                    txtUCDMG.Text = Globales.EsNuloConCero(dtaux.Rows(0).Item("cosdmg"))
                    txtUcPNB.Text = Globales.EsNuloConCero(dtaux.Rows(0).Item("cospnb"))
                Else
                    txtUCDMG.Text = "0"
                    txtUcPNB.Text = "0"
                End If
                sql = "select * from  qv_marcapropia_dimerc "
                sql = sql & " WHERE codpro = '" & txtCodigo.Text.ToString.Trim & "'"
                'sql = sql & "   and tipo = 'NACIONAL' "
                dtaux.Clear()
                dtaux = bd.sqlSelect(sql)
                chkMPropia.Checked = IIf(dtaux.Rows.Count > 0, True, False)

                'sql = "select * from  qv_marcapropia_dimerc "
                'sql = sql & " WHERE codpro = '" & txtCodigo.Text.ToString.Trim & "'"
                'sql = sql & "   and tipo = 'IMPORTADO' "
                'dtaux.Clear()
                'dtaux = bd.sqlSelect(sql)
                'chkMPropiaInter.Checked = IIf(dtaux.Rows.Count > 0, True, False)

                sql = " select fecha ""Fecha"" ,nvl(categoria,'S/C') ""Tipo"", nvl(comision,0) ""%Comi""  "
                sql = sql & "from ma_producto_comision "
                sql = sql & "where codemp= " & Globales.empresa.ToString
                sql = sql & "  and codpro= '" & txtCodigo.Text.ToString.Trim & "'"
                sql = sql & "  and fecha>= to_date('01/01/2008','dd/mm/yyyy')"
                sql = sql & "order by fecha desc   "
                dt = bd.sqlSelect(sql)
                For i = 0 To dt.Rows.Count - 1
                    dtComisiones.Rows.Add(dt.Rows(i).ItemArray)
                Next
                dgvComision.DataSource = dtComisiones
                dgvComision.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
                dgvComision.Columns("%Comi").DefaultCellStyle.Format = "n0"
                dgvComision.Columns("%Comi").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgvComision.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

                sql = " select b.nombre ""Nombre"", a.stocks ""Stocks""  "
                sql = sql & " From re_bodprod_dimerc a, ma_bodegas b "
                sql = sql & " Where a.codemp= " & Globales.empresa.ToString
                sql = sql & "   and a.codpro= '" & txtCodigo.Text.ToString.Trim & "'"
                sql = sql & "   and b.codemp=a.codemp"
                sql = sql & "   and b.codbod=a.codbod"
                sql = sql & " Order by b.nombre "
                Dim dtBodega = bd.sqlSelect(sql)

                'sql = " select 'STKPEN-'||b.nombre ""Nombre"", a.stkpen  ""Stocks"" "
                'sql = sql & " From re_bodprod_stkpen a, ma_bodegas b "
                'sql = sql & " Where a.codemp= " & Globales.empresa.ToString
                'sql = sql & "   and a.codpro= '" & txtCodigo.Text.ToString.Trim & "'"
                'sql = sql & "   and b.codemp=a.codemp"
                'sql = sql & "   and b.codbod=a.codbod"
                'sql = sql & " Order by b.nombre "
                'dtBodega.Merge(bd.sqlSelect(sql))
                'dgvBodega.DataSource = dtBodega
                'dgvBodega.Columns("Nombre").Width = 255
                'dgvBodega.Columns("Stocks").Width = 100
                ''dgvBodega.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
                'dgvBodega.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                'dgvBodega.Columns("Stocks").DefaultCellStyle.Format = "n0"
                'dgvBodega.Columns("Stocks").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

                'cargaSucursal()

                sql = " select decode(codtip,'P','Porc.%',decode(codtip,'D','$','Costo')) tipo,valor  "
                sql = sql & " from ma_costo_comercial "
                sql = sql & " where codpro= '" & txtCodigo.Text.ToString.Trim & "'"
                sql = sql & "   and codemp= " & Globales.empresa.ToString
                dtaux2.Clear()
                dtaux2 = bd.sqlSelect(sql)
                If dtaux2.Rows.Count > 0 Then
                    txtTipoDelta.Text = Globales.EsNulo(dtaux2.Rows(0).Item("tipo"))
                    txtValorDelta.Text = Globales.EsNuloConCero(Val(dtaux2.Rows(0).Item("valor")))
                End If

                sql = "Select valor from re_producto_cm where codpro = '" & txtCodigo.Text.ToString.Trim & "'"
                sql = sql & " and codemp = 3" ''fijo por el momento
                dtaux2.Clear()
                dtaux2 = bd.sqlSelect(sql)
                If dtaux2.Rows.Count > 0 Then
                    cmbCM.SelectedValue = dtaux2.Rows(0).Item("valor")
                End If

                txtDescripcion.Focus()
                cargo = True
                txtPorCM.Text = Math.Round(((CDbl(txtCosto.Text) - CDbl(txtCosProm.Text)) / CDbl(txtCosto.Text)) * 100, 2)
            Else
                MessageBox.Show("La Busqueda No Ha Retornado Ningun Dato", "Dimerc S.A", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

            End If

            If txtPaleta.Text = "S" And rdbContraPedido.Checked = True Or txtPaleta.Text = "N" And rdbContraPedido.Checked = False Then
                lblMensaje.Visible = True
            Else
                lblMensaje.Visible = False
            End If

        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub
    'Private Sub cargaSucursal()
    '    Try
    '        bd = New Conexion()
    '        bd.open_dimerc()
    '        dgvSucursal.DataSource = Nothing
    '        dgvSucursal.Columns.Clear()
    '        dgvSucursal.Rows.Clear()
    '        cargo = False
    '        sql = "  Select nvl(a.sucursal,999999) selec,b.desval Sucursal, b.codval COdigo  "
    '        sql = sql & " From re_producto_sucursal a, de_dominio b  "
    '        sql = sql & " Where b.coddom = 591 "
    '        sql = sql & "   and b.activa = 'S'    "
    '        sql = sql & "   and a.codemp(+) = " & Globales.empresa.ToString
    '        sql = sql & "   and a.codpro(+) = '" & txtCodigo.Text.ToString.Trim & "'"
    '        sql = sql & "   and a.sucursal(+) = b.codval  "
    '        sql = sql & " Order by b.desval  "
    '        dgvSucursal.DataSource = bd.sqlSelect(sql)
    '        'Agregar columna check
    '        If cargo = False Then
    '            Dim check As New DataGridViewCheckBoxColumn
    '            check.HeaderText = "Sel"
    '            check.Name = "Sel"
    '            dgvSucursal.Columns.Insert(0, check)
    '            dgvSucursal.Columns(0).Visible = True
    '        End If
    '        dgvSucursal.Columns("SELEC").Visible = False
    '        dgvSucursal.Columns("CODIGO").Visible = False
    '        'dgvSucursal.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
    '        dgvSucursal.Columns("Sel").Width = 30
    '        dgvSucursal.Columns("Sucursal").Width = 145
    '        For Each fila As DataGridViewRow In dgvSucursal.Rows
    '            If Val(fila.Cells("selec").Value) = 999999 Then
    '                dgvSucursal.Item("Sel", fila.Index).Value = 0
    '            Else
    '                dgvSucursal.Item("Sel", fila.Index).Value = 1
    '            End If
    '        Next
    '    Catch ex As Exception
    '        MessageBox.Show(ex.ToString, "Ojo!", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '    Finally
    '        bd.close()
    '    End Try
    'End Sub
    Private Sub cmdAlter_Click(sender As System.Object, e As System.EventArgs) Handles cmdAlter.Click
        Dim frmRelacionado As New frmProductoAlt(txtCodigo.Text.ToString.Trim)
        frmRelacionado.ShowDialog()

    End Sub
    Public Function ValDes(num1, num2, num3, num4, num5, num6, num7, num8, num9, prec) As Double
        Dim Valor As Double
        Valor = Format(prec - (prec * (num1 / 100)), "#######0.00")
        ValDes = Format(prec - (prec * (num1 / 100)), "#######0.00")
        ValDes = Format(ValDes - (ValDes * (num2 / 100)), "#######0.00")
        ValDes = Format(ValDes - (ValDes * (num3 / 100)), "#######0.00")
        ValDes = Format(ValDes - (ValDes * (num4 / 100)), "#######0.00")
        ValDes = Format(ValDes - (ValDes * (num5 / 100)), "#######0.00")
        ValDes = Format(ValDes - (ValDes * (num6 / 100)), "#######0.00")
        ValDes = Format(ValDes - (ValDes * (num7 / 100)), "#######0.00")
        ValDes = Format(ValDes - (ValDes * (num8 / 100)), "#######0.00")
        ValDes = Format(ValDes - (ValDes * (num9 / 100)), "#######0.00")
        ValDes = Format(ValDes - Int(ValDes * (num6 / 100)), "#######0.00")
    End Function

    Private Sub txtStkAse_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtStkAse.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Val(txtStkAse.Text) > Val(txtMaxCd.Text) Then
                MessageBox.Show("Stock Asegurado no puede ser Mayor que el Maximo", "Dimerc S.A", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtMaxCd.Focus()
                Exit Sub
            End If
            txtMinCd.Focus()
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtUMinima_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtUMinima.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            txtUndEmb.Focus()
        End If
    End Sub

    Private Sub txtUndEmb_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtUndEmb.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            txtUndPed.Focus()
        End If
    End Sub

    Private Sub txtUndPed_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtUndPed.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            btn_Grabar.Visible = True
            txtConVence.Focus()
        End If
    End Sub

    Private Sub txtConVence_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtConVence.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Not Trim(txtConVence.Text) = "N" And Not Trim(txtConVence.Text) = "S" Then
                MessageBox.Show("Debe colocar una S o N", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtConVence.Focus()
            Else
                txtVidaUtil.Focus()
            End If
        End If
    End Sub

    Private Sub txtVidaUtil_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtVidaUtil.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            chkColateral.Focus()
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub btn_Salir_Click(sender As System.Object, e As System.EventArgs)
        Me.Close()
    End Sub

    Private Sub btn_Historia_Click(sender As System.Object, e As System.EventArgs) Handles btn_Historia.Click
        'WriteData("Mensaje de Prueba", "10.10.185.147")
        If cargo = True Then
            PuntoControl.RegistroUso("82")
            Dim historial As New frmConsulta_Hist(txtCodigo.Text.ToString.Trim, txtDescripcion.Text, cmbMarca.Text)
            historial.ShowDialog()
        Else
            MessageBox.Show("Cargue un código antes de revisar historial.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
    End Sub

    Private Sub txtCodBarra_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtCodBarra.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            txtUMinima.Focus()
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtCodigo_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtCodigo.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            buscarProducto()
            PuntoControl.RegistroUso("77")
        End If
    End Sub
    Private Sub limpiar()
        cargo = False
        rdbMPInventarioNO.Checked = False
        rdbMPInventarioSI.Checked = False
        dtpFechaIng.Value = Now
        dtpFechaIng.Value = Now
        txtTipoDelta.Text = ""
        txtValorDelta.Text = ""
        txtGrupo.Text = ""
        txtDescripcion.Text = ""
        txtUMinima.Text = " "
        txtStkAse.Text = ""
        txtDescripAlter.Text = ""
        txtTextoLibre.Text = ""
        txtPaleta.Text = ""
        txtXDelta.Text = ""
        txtPorAporte.Text = ""
        txtPorCM.Text = ""
        txtPeso.Text = ""
        txtVolumen.Text = ""
        cmdAlter.Visible = False
        rdbStock.Checked = True
        chkMoneda.Checked = False
        chkColateral.Checked = False
        chkReembalaje.Checked = False
        chkNumSer.Checked = False
        chkGobierno.Checked = False
        cmbEstado.SelectedIndex = -1
        cmbProvPrinc.SelectedIndex = -1
        cmbNegocio.SelectedIndex = -1
        cmbSubgrupo.SelectedIndex = -1
        cmbComision.SelectedIndex = -1
        cmbCM.SelectedIndex = -1
        cmbLinea.SelectedIndex = -1
        cmbFamilia.SelectedIndex = -1
        cmbCategoria.SelectedIndex = -1
        cmbMarca.SelectedIndex = -1
        cmbTipoProd.SelectedIndex = -1
        cmbTipoImp.SelectedIndex = -1
        txtCatAct.Text = ""
        txtCodBarra.Text = ""
        cmbUnidad.SelectedIndex = -1
        'txtSFisico.Text = ""
        'txtSComprometido.Text = ""
        'txtsPendiente.Text = ""
        txtStkPenDmg.Text = ""
        txtCosto.Text = ""
        txtUndPed.Text = ""
        txtUndEmb.Text = ""
        cmbTipoAlmacenamiento.SelectedIndex = -1
        cmbRotacion.SelectedIndex = -1
        rdbSobreVenta.Checked = True
        rdbAgotarStock.Checked = False
        txtSMinimo.Text = ""
        txtSMaximo.Text = ""
        txtStkCri.Text = ""
        txtStkInt.Text = ""
        txtMaxCd.Text = ""
        txtMinCd.Text = ""
        txtCriCd.Text = ""
        txtIntCd.Text = ""
        txtStkComAntof.Text = ""
        txtStkDimerc.Text = ""
        txtStkDimeiggs.Text = ""
        txtUltCos.Text = ""
        'txtUCompra.Text = ""
        'txtFeUlCo.Text = ""
        txtUCDMG.Text = ""
        txtUcPNB.Text = ""
        txtCFCosto.Text = ""
        txtCFFecha.Text = ""
        txtCompraAnt.Text = ""
        txtReposicion.Text = ""
        btn_ListaPrecio.Visible = False
        btn_ProvProd.Visible = False
        btn_Oci.Visible = False
        'txtDisponible.Text = ""
        txtUltInv.Text = ""
        txtFecInv.Text = ""
        txtFscLoc.Text = ""
        txtOcLoc.Text = ""
        txtTtoLoc.Text = ""
        txtCosProm.Text = ""
        txtUbicacion.Text = ""
        dgvComision.DataSource = Nothing
        dtComisiones.Rows.Clear()
        'dgvBodega.DataSource = Nothing
        'For i = 0 To dgvSucursal.Rows.Count - 1
        '    dgvSucursal.Item("SEL", i).Value = 0
        'Next
        txtCodigo.Focus()

        If Globales.detDerUsu(Globales.user, 317) Then
            txtStkAse.Enabled = True
        Else
            txtStkAse.Enabled = False
        End If
        txtCodUni.Text = ""
        txtProdDescripCorta.Text = ""
        txtConVence.Text = ""
        chkMPropia.Checked = False
        'chkMPropiaInter.Checked = False
        chkRDimerc.Checked = False
        chkImportado.Checked = False
        chkCalidad.Checked = False
        txtVidaUtil.Text = ""
        chkAsegura.Checked = False
        chkNoAplica.Checked = False
        chkToxico.Checked = False
        chkCorrosivo.Checked = False
        chkImflamable.Checked = False
    End Sub

    Private Sub txtCodigo_Click(sender As System.Object, e As System.EventArgs) Handles txtCodigo.Click
        limpiar()
    End Sub


    Private Sub chkAsegura_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles chkAsegura.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            chkNumSer.Focus()
        End If
    End Sub

    Private Sub chkColateral_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles chkColateral.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            chkMoneda.Focus()
        End If
    End Sub

    Private Sub chkGobierno_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles chkGobierno.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            txtMinCd.Focus()
        End If
    End Sub

    Private Sub chkMoneda_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles chkMoneda.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            chkReembalaje.Focus()
        End If
    End Sub

    Private Sub chkCalidad_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles chkCalidad.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            chkRDimerc.Focus()
        End If
    End Sub

    Private Sub chkMPropia_KeyPress(sender As Object, e As KeyPressEventArgs) Handles chkMPropia.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            'chkMPropiaInter.Focus()
        End If
    End Sub

    Private Sub chkNumSer_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles chkNumSer.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            chkMPropia.Focus()
        End If
    End Sub

    Private Sub chkMPropiaInter_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs)
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            chkCalidad.Focus()
        End If
    End Sub

    Private Sub chkReembalaje_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles chkReembalaje.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            chkAsegura.Focus()
        End If
    End Sub

    Private Sub chkRDimerc_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles chkRDimerc.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            chkImportado.Focus()
        End If
    End Sub

    Private Sub chkRDimeiggs_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles chkRDimeiggs.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            chkImportado.Focus()
        End If
    End Sub

    Private Sub chkImportado_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles chkImportado.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            chkGobierno.Focus()
        End If
    End Sub

    Private Sub cmbComision_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles cmbComision.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            dtpFchaVig.Focus()
        End If
    End Sub

    'Private Sub cmbFamilia_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles cmbFamilia.KeyPress
    '    If e.KeyChar = Convert.ToChar(Keys.Enter) Then
    '        If cmbFamilia.SelectedIndex <> -1 Then
    '            cmbGrupo.Focus()
    '        End If
    '    End If
    'End Sub

    Private Sub cmbLinea_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles cmbLinea.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If cmbLinea.SelectedIndex <> -1 Then
                cmbMarca.Focus()
            End If
        End If
    End Sub

    Private Sub cmbMarca_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles cmbMarca.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If cmbMarca.SelectedIndex <> -1 Then
                cmbFamilia.Focus()
            End If
        End If
    End Sub

    Private Sub cmbRotacion_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles cmbRotacion.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            txtCodBarra.Focus()
        End If
    End Sub

    Private Sub cmbTipoAlmacenamiento_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles cmbTipoAlmacenamiento.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If cmbTipoAlmacenamiento.SelectedIndex <> -1 Then
                cmbRotacion.Focus()
            End If
        End If
    End Sub

    Private Sub cmbTipoProd_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles cmbTipoProd.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If cmbTipoProd.SelectedIndex <> -1 Then
                cmbCM.Focus()
            End If
        End If
    End Sub

    Private Sub btn_Grabar_Click(sender As System.Object, e As System.EventArgs) Handles btn_Grabar.Click
        If MessageBox.Show("¿Desea grabar los registros?", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = System.Windows.Forms.DialogResult.Yes Then
            PuntoControl.RegistroUso("80")
            Using conn = New OracleConnection(Conexion.retornaConexion)
                Dim comando As OracleCommand
                Dim dataAdapter As OracleDataAdapter
                conn.Open()
                Using transaccion = conn.BeginTransaction
                    Try

                        sql = "set role rol_aplicacion"
                        comando = New OracleCommand(sql, conn)
                        comando.Transaction = transaccion
                        comando.ExecuteNonQuery()

                        Dim StrCodigo, StrMarcaPropia As String
                        Dim LngMargen As Double
                        Dim LngStkFisico, IntCanal As Double
                        Dim LngCosOrg, LngCosprom As Double
                        Dim StrStatus As String
                        Dim StrEstado As String
                        Dim lngPrecio As Long
                        Dim bolSucursal As Boolean
                        Dim indProd As Integer
                        Dim dt As New DataTable
                        bd.open_dimerc()
                        'If Globales.empresa <> 3 Then
                        '    MessageBox.Show("Solo Puede Modificar datos de la empresa Dimerc Office", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        '    Exit Sub
                        'End If
                        If Not Globales.detDerUsu(Globales.user, 314) Then
                            MessageBox.Show("No tiene derecho a grabar datos (Der. 314)", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            Exit Sub
                        End If
                        'If cmbCM.SelectedIndex = -1 Then
                        '    MessageBox.Show("ANTES DE GRABAR, DEBE INGRESAR CM ( Tipo de Corrección Monetaria ).", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        '    Exit Sub
                        'End If
                        'If dgvComision.RowCount = 0 Then
                        '    MessageBox.Show("ANTES DE GRABAR, DEBE INGRESAR COMISION DE VENTA PARA EL PRODUCTO", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        '    Exit Sub
                        'End If
                        'If rdbContraPedido.Checked = True And chkAsegura.Checked = True Then
                        '    MessageBox.Show("El producto No puede estar marcado como ASEGURADO y a su vez como CONTRA PEDIDO", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        '    Exit Sub
                        'End If
                        If rdbAgotarStock.Checked = False And rdbSobreVenta.Checked = False Then
                            MessageBox.Show("No tiene marcado hasta agotar stock o Sobreventa", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            'Exit Sub
                        End If
                        If rdbMPInventarioNO.Checked = False And rdbMPInventarioSI.Checked = False Then
                            MessageBox.Show("No tiene la Opción de Marca Propia Inventario: SI o NO", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            'Exit Sub
                        End If
                        'DEJARE PASAR ESTO POR AHORA HASTA IDENTIFICAR BIEN SU USO
                        'comando = New OracleCommand("Select codpro from re_codebar where codbar = '" & Trim(txtCodBarra.Text) & "'", conn, transaccion)
                        'dataAdapter = New OracleDataAdapter(comando)
                        'dataAdapter.Fill(dt)
                        'If dt.Rows.Count > 0 Then
                        '    If dt.Rows(0).Item("codpro") <> txtCodigo.Text Then
                        '        MessageBox.Show("Código de Barra Ya existe para el producto : " & CStr(dt.Rows(0).Item("codpro")), "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        '        Exit Sub
                        '    End If
                        'End If
                        ' If chkMPropia.Checked = True And chkMPropiaInter.Checked = True Then
                        'MessageBox.Show("El producto solo puede ser Marca Propia Nacional o Internacional, no ambos a la vez.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        'Exit Sub
                        'End If
                        'If Globales.actualizaFicha = False Then
                        '    MessageBox.Show("No esta autorizado a Modificar o Actualiza esta Ficha", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        '    Exit Sub
                        'End If
                        If txtDescripcion.Text <> desProOriginal Then
                            MessageBox.Show("No Tiene Derecho A Modificar La Descripción De Los Productos", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            txtDescripcion.Text = desProOriginal
                        End If

                        'bolSucursal = False
                        'For i = 0 To dgvSucursal.RowCount - 1
                        '    If dgvSucursal.Item("Sel", i).Value = 1 Then
                        '        bolSucursal = True
                        '        Exit For
                        '    End If
                        'Next
                        'If bolSucursal = False Then
                        '    MessageBox.Show("Al menos debe seleccionar una Sucursal para el producto.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        '    Exit Sub
                        'End If
                        'If Trim(txtConVence.Text) <> "N" And Trim(txtConVence.Text) <> "S" Then
                        '    MessageBox.Show("Debe colocar una S o N en el Vencimiento", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        '    txtConVence.Focus()
                        '    Exit Sub
                        'End If
                        'If Trim(txtConVence.Text) = "S" Then
                        '    If Trim(txtVidaUtil.Text) = "" Or Val(txtVidaUtil.Text) = 0 Then
                        '        MessageBox.Show("Debe ingresar Nro. de días en opción Vida Util.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        '        txtVidaUtil.Focus()
                        '        Exit Sub
                        '    End If
                        'ElseIf Trim(txtConVence.Text) = "N" Then
                        '    txtVidaUtil.Text = "0"
                        'End If
                        LngCosOrg = 0
                        StrMarcaPropia = "N"
                        LngCosprom = 0
                        LngStkFisico = 0
                        sql = "Select nvl(stkfsc,0) stkfsc, nvl(costo,0) costo,nvl(cosprom,0) cosprom, GETMARCAPROPIA_DIMERC(codpro) marcap,nvl(costo_corp,0) costo_corp,nvl(cosprom_corp,0) cosprom_corp from ma_product "
                        sql = sql & " where codpro = '" & txtCodigo.Text.ToString.Trim & "'"
                        dt.Clear()
                        comando = New OracleCommand(sql, conn, transaccion)
                        dataAdapter = New OracleDataAdapter(comando)
                        dataAdapter.Fill(dt)
                        If dt.Rows.Count > 0 Then
                            If Globales.empresa = 3 Then
                                LngCosOrg = Val(dt.Rows(0).Item("costo"))
                                LngCosprom = Val(dt.Rows(0).Item("cosprom"))
                            End If
                            If Globales.empresa = 6 Then
                                LngCosOrg = Val(dt.Rows(0).Item("costo_corp"))
                                LngCosprom = Val(dt.Rows(0).Item("cosprom_corp"))
                            End If
                            StrMarcaPropia = CStr(dt.Rows(0).Item("marcap"))
                            LngStkFisico = Val(dt.Rows(0).Item("stkfsc"))
                        End If

                        bolCambiaCosto = True
                        If Val(txtCosto.Text) <> LngCostoOriginal Then
                            If Globales.user <> "KSERRANO" Then
                                If cmbTipoProd.SelectedValue = 2 Then
                                    MessageBox.Show("NO puede cambiar un Costo Comercial para un producto Corporativo en esta opción.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                    bolCambiaCosto = False
                                End If
                            End If

                            If Globales.detDerUsu(Globales.user, 85) Then 'Tiene Derecho Para cambiar costo comercial no importando nada
                                If Globales.detDerUsu(Globales.user, 61) Then
                                    '    ' tiene derecho solo si stock=0
                                    '    If LngStkFisico > 0 And Globales.user <> "KSERRANO" Then
                                    '        MessageBox.Show("Producto Tiene Stock en La Empresa. No Puede Modificar EL COSTO COMERCIAL. (Der.85)", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                    '        txtCosto.Text = LngCostoOriginal
                                    '        Exit Sub
                                    '    End If
                                Else
                                    If StrMarcaPropia = "S" Then
                                        If Globales.detDerUsu(Globales.user, 228) Then
                                            ' Tiene derecho para cambiar costo marca propia
                                            If Val(txtCosto.Text) < LngCosprom Then
                                                MessageBox.Show("Nuevo Costo Comercial no puede ser inferior al Costo Promedio.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                                txtCosto.Text = LngCostoOriginal
                                                Exit Sub
                                            End If

                                        Else
                                            MessageBox.Show("Producto Tiene Stock y No es Marca Propia. No Puede Modificar EL COSTO COMERCIAL (Der.228)", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                            txtCosto.Text = LngCostoOriginal
                                            Exit Sub

                                        End If
                                    Else
                                        MessageBox.Show("Producto No es Marca Propia. No puede modificar costo comercial (Der.61)", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                        txtCosto.Text = LngCostoOriginal
                                        Exit Sub
                                    End If
                                End If
                            End If
                        End If

                        StrStatus = "OK"

                        'AQUI CONSULTARE EL WEB SEVICES
                        'lblRespuesta.Text = Globales.ConsultaOCrearProd()

                        If BolEstaProducto Then
                            If txtDescripcion.Text = "" Then
                                txtDescripcion.BackColor = Color.Aqua
                                StrStatus = "ERR"
                                txtDescripcion.Focus()
                            End If
                            If txtPaleta.Text = "" Then
                                txtPaleta.BackColor = Color.Aqua
                                StrStatus = "ERR"
                                txtPaleta.Focus()
                            Else
                                If txtPaleta.Text <> "S" Then
                                    If txtPaleta.Text <> "N" Then
                                        txtPaleta.BackColor = Color.Aqua
                                        StrStatus = "ERR"
                                        txtPaleta.Focus()
                                    End If
                                End If
                            End If
                            If txtXDelta.Text = "" Then
                                txtXDelta.BackColor = Color.Aqua
                                StrStatus = "ERR"
                                txtXDelta.Focus()
                            End If
                            'If cmbUnidad.SelectedIndex = -1 Then
                            '    cmbUnidad.BackColor = Color.Aqua
                            '    StrStatus = "ERR"
                            '    cmbUnidad.Focus()
                            'End If
                            If cmbTipoProd.SelectedIndex = -1 Then
                                cmbTipoProd.BackColor = Color.Aqua
                                StrStatus = "ERR"
                                cmbTipoProd.Focus()
                            End If
                            If cmbEstado.SelectedIndex = -1 Then
                                cmbEstado.BackColor = Color.Aqua
                                StrStatus = "ERR"
                                cmbEstado.Focus()
                            End If
                            If cmbNegocio.SelectedIndex = -1 Then
                                cmbNegocio.BackColor = Color.Aqua
                                StrStatus = "ERR"
                                cmbNegocio.Focus()
                            End If
                            If cmbLinea.SelectedIndex = -1 Then
                                StrStatus = "ERR"
                                cmbLinea.BackColor = Color.Aqua
                                cmbLinea.Focus()
                            End If
                            If cmbFamilia.SelectedIndex = -1 Then
                                StrStatus = "ERR"
                                cmbFamilia.BackColor = Color.Aqua
                                cmbFamilia.Focus()
                            End If
                            If cmbMarca.SelectedIndex = -1 Then
                                StrStatus = "ERR"
                                cmbMarca.BackColor = Color.Aqua
                                cmbMarca.Focus()
                            End If
                            If cmbNegDmc.SelectedIndex = -1 Then
                                StrStatus = "ERR"
                                cmbNegDmc.BackColor = Color.Aqua
                                cmbNegDmc.Focus()
                            End If
                            If cmbSubgrupo.SelectedIndex = -1 Then
                                StrStatus = "ERR"
                                cmbSubgrupo.BackColor = Color.Aqua
                                cmbSubgrupo.Focus()
                            End If

                            'If txtUndPed.Text = "" Then
                            '    StrStatus = "ERR"
                            '    txtUndPed.BackColor = Color.Aqua
                            '    txtUndPed.Focus()
                            'End If

                            'If Val(txtUMinima.Text) = 0 Then
                            '    txtUMinima.Focus()
                            '    txtUMinima.Text = "0"
                            'End If
                            'If StrStatus = "ERR" Then
                            '    MessageBox.Show(" Existen valores cero 0 en blanco que deben ser ingresados", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            '    Exit Sub
                            'End If

                            If txtStkAse.Text = "" Then
                                txtStkAse.Text = 0
                            End If
                            If txtStkDimeiggs.Text = "" Then
                                txtStkDimeiggs.Text = 0
                            End If

                            Cursor.Current = Cursors.WaitCursor

                            If LngCosOrg <> Val(txtCosto.Text) And bolCambiaCosto = True Then
                                sql = " INSERT into LG_COSTOS "
                                sql = sql & "(codpro,despro,cosant,cosact,preant,preact,codcnl,rutusu,fecmod,hormod,estado,usr_creac,fecha_creac,stkfsc"
                                sql = sql & ",fecha,codusu,stock_antes,costo_antes,cant_entrada,costo_entrada,costo_new,tipo,orden_compra,gmm_recepcion,obs,costo_com_antes,costo_com_new, codemp)"
                                sql = sql & " VALUES ( "
                                sql = sql & " '" & txtCodigo.Text.ToString.Trim & "', null,null,null,null,null,null,null,null,null,null,null,null,null"
                                sql = sql & ",sysdate, '" & Globales.user & "',nvl(getcostopromediocalculostock(" & Globales.empresa.ToString & ",'" & txtCodigo.Text.ToString.Trim & "'),0),nvl(getcostopromedio(" & Globales.empresa.ToString & ",'" & txtCodigo.Text.ToString.Trim & "'),0)"
                                sql = sql & ",nvl(getcostopromediocalculostock(" & Globales.empresa.ToString & ",'" & txtCodigo.Text.ToString.Trim & "'),0),nvl(getcostopromedio(" & Globales.empresa.ToString & ",'" & txtCodigo.Text.ToString.Trim & "'),0), nvl(getcostopromedio(" & Globales.empresa.ToString & ",'" & txtCodigo.Text.ToString.Trim & "'),0)"
                                sql = sql & ",'MAN',null,null,null"
                                sql = sql & "," & LngCosOrg
                                sql = sql & "," & Val(txtCosto.Text)
                                sql = sql & "," & Globales.empresa.ToString
                                sql = sql & " )"
                                comando = New OracleCommand(sql, conn)
                                comando.Transaction = transaccion
                                comando.ExecuteNonQuery()
                            End If

                            StrEstado = "ORG"

                            If ActualizaLogProducto(txtCodigo.Text.ToString.Trim, StrEstado, transaccion, conn) = False Then
                                MessageBox.Show("Ha ocurrido un error al grabar log de producto, se aborta el grabado de datos", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                If Not transaccion.Equals(Nothing) Then
                                    transaccion.Rollback()
                                End If
                                Exit Sub
                            End If

                            'SE SACA EL 21-11-2016 ya que no hay certeza de que el codigo anterior funcionara

                            'sql = "Select costo, costo_corp from ma_product where codpro = '" & txtCodigo.Text & "'"
                            'dt.Clear()
                            'comando = New OracleCommand(sql, conn, transaccion)
                            'dataAdapter = New OracleDataAdapter(comando)
                            'dataAdapter.Fill(dt)
                            'If dt.Rows.Count > 0 Then
                            '    If Globales.empresa = 3 Then
                            '        If Val(txtCosto.Text) <> LngCostoOriginal Then
                            '            'ENVIAR MAIL A CONTABILIDAD
                            '            enviaCorreo("careyes@dimerc.cl", "Cambio de Costo Producto", "Se Cambia Costo Comercial (OFFICE) del producto  " & txtCodigo.Text & " de " & CStr(dt.Rows(0).Item("costo")) & "$ a " & txtCosto.Text & Chr(10) & "$ Con un Stock de " & txtSFisico.Text & " Unidades")
                            '        End If
                            '    End If
                            '    If Globales.empresa = 6 Then
                            '        If Val(txtCosto.Text) <> Val(dt.Rows(0).Item("costo_corp")) Then
                            '            'ENVIAR MAIL A CONTABILIDAD
                            '            enviaCorreo("careyes@dimerc.cl", "Cambio de Costo Producto", "Se Cambia Costo Comercial (OFFICE) del producto  " & txtCodigo.Text & " de " & CStr(dt.Rows(0).Item("costo_corp")) & "$ a " & txtCosto.Text & Chr(10) & "$ Con un Stock de " & txtSFisico.Text & " Unidades")
                            '        End If
                            '    End If
                            'End If

                            sql = "DELETE FROM MA_PRODUCT_NEGOCIO
                                   WHERE CODPRO = '" & txtCodigo.Text & "'"
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()

                            If txtPorAporte.Text = "" Then
                                txtPorAporte.Text = 0
                            End If

                            If cmbSubgrupo.SelectedIndex > -1 And cmbNegocio.SelectedIndex > -1 Then
                                sql = " INSERT into MA_PRODUCT_NEGOCIO "
                                sql = sql & "(codpro,descrip_alter,porc_aporte,id_subgrupo,id_negocio,texto_libre) values ('" &
                                             txtCodigo.Text & "','" & txtDescripAlter.Text & "'," & Replace(txtPorAporte.Text, ",", ".") & "," &
                                            cmbSubgrupo.SelectedValue.ToString() & "," & cmbNegocio.SelectedValue.ToString() & ",'" &
                                            txtTextoLibre.Text & "')"
                                comando = New OracleCommand(sql, conn)
                                comando.Transaction = transaccion
                                comando.ExecuteNonQuery()
                            End If

                            sql = "SELECT aporte FROM ma_prod_aporte
                                   WHERE CODPRO = '" & txtCodigo.Text & "'"
                            Dim dtaporte = bd.sqlSelect(sql)
                            If dtaporte.Rows.Count > 0 Then
                                sql = "update ma_prod_aporte set aporte = " & Replace(txtPorAporte.Text, ",", ".") & "
                                   WHERE CODPRO = '" & txtCodigo.Text & "'"
                                comando = New OracleCommand(sql, conn)
                                comando.Transaction = transaccion
                                comando.ExecuteNonQuery()
                            Else
                                sql = "insert into ma_prod_aporte (codpro, aporte) values ('" & txtCodigo.Text & "', " & Replace(txtPorAporte.Text, ",", ".") & ")"
                                comando = New OracleCommand(sql, conn)
                                comando.Transaction = transaccion
                                comando.ExecuteNonQuery()
                            End If

                            sql = "update MA_PRODUCT set despro = getdelcar('" & txtDescripcion.Text & "')"
                            sql = sql & ", codlin = " & cmbLinea.SelectedValue.ToString
                            sql = sql & ", codfam = " & cmbFamilia.SelectedValue.ToString
                            'sql = sql & ", codsubfam = " & cmbGrupo.SelectedValue.ToString
                            'sql = sql & ", codsubfam2 = " & cmbSubGrupo.SelectedValue.ToString
                            'sql = sql & ", codmar = " & cmbMarca.SelectedValue.ToString
                            sql = sql & ", codmod = 0"
                            sql = sql & ", paleta_dc = '" & txtPaleta.Text & "'"
                            sql = sql & ", tipoprod = " & cmbTipoProd.SelectedValue.ToString
                            'If Trim(txtCodBarra.Text) <> "" Then
                            '    If Not txtCodBarra.Text = "0" Then
                            '        sql = sql & ", codbar = '" & txtCodBarra.Text & "'"
                            '    End If
                            'End If
                            'sql = sql & ", coduni = " & cmbUnidad.SelectedValue.ToString

                            If Globales.empresa = 3 Then
                                sql = sql & ", costo = " & IIf(Trim(txtCosto.Text) = "", 0, Val(txtCosto.Text))
                                'sql = sql & ", costo_corp = " & IIf(Trim(TxtCosto) = "", 0, Val(TxtCosto))
                            End If
                            If Globales.empresa = 6 Then
                                sql = sql & ", costo_corp = " & IIf(Trim(txtCosto.Text) = "", 0, Val(txtCosto.Text))
                            End If

                            If txtCosto.Text <> LngCostoOriginal Then
                                sql = sql & ", ultcos = " & LngCostoOriginal
                            Else
                                sql = sql & ", ultcos = " & txtUltCos.Text
                            End If
                            sql = sql & ", indsbr = " & IIf(rdbSobreVenta.Checked = True, "'S'", "'N'")
                            'sql = sql & ", unmivt = " & txtUMinima.Text
                            sql = sql & ", pedsto = " & IIf(rdbStock.Checked = True, "'S'", "'P'")
                            'sql = sql & ", colate = " & IIf(chkColateral.Checked = True, "'1'", "'0'")
                            'sql = sql & ", moneda = " & IIf(chkMoneda.Checked = True, "'1'", "'0'")
                            'sql = sql & ", maxcd = " & txtMaxCd.Text
                            'sql = sql & ", mincd = " & txtMinCd.Text
                            'sql = sql & ", cricd = " & txtCriCd.Text
                            'sql = sql & ", intcd = " & txtIntCd.Text
                            sql = sql & ", importado = " & IIf(chkImportado.Checked = False, "'N'", "'S'")
                            'sql = sql & ", gobierno = " & IIf(chkGobierno.Checked = False, "'N'", "'S'")
                            sql = sql & ", impuesto  = " & cmbTipoImp.SelectedValue.ToString
                            'sql = sql & ", asegur = " & IIf(chkAsegura.Checked = False, "'N'", "'S'")
                            sql = sql & ", xdelta = " & IIf(Trim(txtXDelta.Text) = "", 0, txtXDelta.Text)
                            'sql = sql & ", pagcat = " & IIf(Trim(txtUndEmb.Text) = "", 0, txtUndEmb.Text)
                            'sql = sql & ", numfto = " & IIf(Trim(txtUndPed.Text) = "", 0, txtUndPed.Text)
                            sql = sql & ", estpro = " & cmbEstado.SelectedValue.ToString
                            'sql = sql & ", numser = " & IIf(chkNumSer.Checked = False, "'N'", "'S'")
                            'sql = sql & ", tipalm = " & cmbTipoAlmacenamiento.SelectedValue.ToString
                            'sql = sql & ", reembj = " & IIf(chkReembalaje.Checked = True, "'1'", "'0'")
                            'sql = sql & ", codigo_uni = " & IIf(Trim(txtCodUni.Text) = "''", "' '", "'" & txtCodUni.Text & "'")
                            'sql = sql & ", con_vencimiento =  '" & txtConVence.Text & "'"
                            'sql = sql & ", control_calidad = " & IIf(chkCalidad.Checked = False, "'N'", "'S'")
                            'sql = sql & ", stock_dc = " & IIf(chkRDimerc.Checked = False, "'N'", "'S'")
                            sql = sql & ", codneg = " & cmbNegDmc.SelectedValue.ToString
                            '238         sql = sql & ", stock_dg = " & IIf(ChkRDimeiggs.Value = 0, "'N'", "'S'")
                            sql = sql & " where codpro = '" & txtCodigo.Text.ToString.Trim & "'"
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()


                            If Trim(txtCodBarra.Text) <> "" Then
                                sql = "Select * from re_codebar where codpro = '" & txtCodigo.Text.ToString.Trim & "'"
                                sql = sql & " and codbar = '" & txtCodBarra.Text & "'"
                                dt.Clear()
                                comando = New OracleCommand(sql, conn, transaccion)
                                dataAdapter = New OracleDataAdapter(comando)
                                dataAdapter.Fill(dt)
                                If dt.Rows.Count = 0 Then
                                    sql = "Select * from re_codebar where codbar = '" & txtCodBarra.Text & "'"
                                    dt.Clear()
                                    comando = New OracleCommand(sql, conn, transaccion)
                                    dataAdapter = New OracleDataAdapter(comando)
                                    dataAdapter.Fill(dt)
                                    If dt.Rows.Count > 0 Then
                                        MessageBox.Show("Codigo de barra ya existe para otro producto.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                    Else
                                        sql = "insert into  re_codebar (codbar,codpro) VALUES("
                                        sql = sql & "'" & txtCodBarra.Text & "',"
                                        sql = sql & "'" & txtCodigo.Text.ToString.Trim & "')"
                                        comando = New OracleCommand(sql, conn)
                                        comando.Transaction = transaccion
                                        comando.ExecuteNonQuery()
                                    End If
                                End If
                            End If

                            If cmbRotacion.SelectedIndex >= 0 Then
                                sql = "delete re_claspro where codpro = '" & txtCodigo.Text.ToString.Trim & "'"
                                sql = sql & " and codemp = " & Globales.empresa.ToString
                                comando = New OracleCommand(sql, conn)
                                comando.Transaction = transaccion
                                comando.ExecuteNonQuery()

                                sql = "insert into re_claspro (codpro,codval,codemp) values ('" & txtCodigo.Text.ToString.Trim & "',"
                                sql = sql & cmbRotacion.SelectedValue.ToString & ","
                                sql = sql & Globales.empresa.ToString & " ) "
                                comando = New OracleCommand(sql, conn)
                                comando.Transaction = transaccion
                                comando.ExecuteNonQuery()
                            End If

                            StrEstado = "MOD"

                            'If chkNoAplica.Checked = True Then
                            '    sql = "delete re_claprod "
                            '    sql = sql & " where codpro = '" & txtCodigo.Text.ToString.Trim & "'"
                            '    sql = sql & "   and codgen = 277 "
                            '    sql = sql & "   and codclasifica = 0"
                            '    comando = New OracleCommand(sql, conn)
                            '    comando.Transaction = transaccion
                            '    comando.ExecuteNonQuery()

                            '    sql = "insert into re_claprod (codpro,codgen,codclasifica,newcla,codemp) values ( "
                            '    sql = sql & "'" & txtCodigo.Text.ToString.Trim & "', 277, 0, 0,3 )"
                            '    comando = New OracleCommand(sql, conn)
                            '    comando.Transaction = transaccion
                            '    comando.ExecuteNonQuery()
                            'ElseIf chkCorrosivo.Checked = True Then
                            '    sql = "delete re_claprod "
                            '    sql = sql & " where codpro = '" & txtCodigo.Text.ToString.Trim & "'"
                            '    sql = sql & "   and codgen = 277 "
                            '    sql = sql & "   and codclasifica = 1"
                            '    comando = New OracleCommand(sql, conn)
                            '    comando.Transaction = transaccion
                            '    comando.ExecuteNonQuery()

                            '    sql = "insert into re_claprod (codpro,codgen,codclasifica,newcla,codemp) values ( "
                            '    sql = sql & "'" & txtCodigo.Text.ToString.Trim & "', 277, 1, 1,3 )"
                            '    comando = New OracleCommand(sql, conn)
                            '    comando.Transaction = transaccion
                            '    comando.ExecuteNonQuery()
                            'ElseIf chkImflamable.Checked = True Then
                            '    sql = "delete re_claprod "
                            '    sql = sql & " where codpro = '" & txtCodigo.Text.ToString.Trim & "'"
                            '    sql = sql & "   and codgen = 277 "
                            '    sql = sql & "   and codclasifica = 2"
                            '    comando = New OracleCommand(sql, conn)
                            '    comando.Transaction = transaccion
                            '    comando.ExecuteNonQuery()

                            '    sql = "insert into re_claprod (codpro,codgen,codclasifica,newcla,codemp) values ( "
                            '    sql = sql & "'" & txtCodigo.Text.ToString.Trim & "', 277, 2, 2,3 )"
                            '    comando = New OracleCommand(sql, conn)
                            '    comando.Transaction = transaccion
                            '    comando.ExecuteNonQuery()
                            'ElseIf chkToxico.Checked = True Then
                            '    sql = "delete re_claprod "
                            '    sql = sql & " where codpro = '" & txtCodigo.Text.ToString.Trim & "'"
                            '    sql = sql & "   and codgen = 277 "
                            '    sql = sql & "   and codclasifica = 3"
                            '    comando = New OracleCommand(sql, conn)
                            '    comando.Transaction = transaccion
                            '    comando.ExecuteNonQuery()

                            '    sql = "insert into re_claprod (codpro,codgen,codclasifica,newcla,codemp) values ( "
                            '    sql = sql & "'" & txtCodigo.Text.ToString.Trim & "', 277, 3, 3,3 )"
                            '    comando = New OracleCommand(sql, conn)
                            '    comando.Transaction = transaccion
                            '    comando.ExecuteNonQuery()
                            'ElseIf chkToxico.Checked = False And chkNoAplica.Checked = False Then
                            '    sql = "delete re_claprod "
                            '    sql = sql & " where codpro = '" & txtCodigo.Text.ToString.Trim & "'"
                            '    sql = sql & "   and codgen = 277 "
                            '    comando = New OracleCommand(sql, conn)
                            '    comando.Transaction = transaccion
                            '    comando.ExecuteNonQuery()
                            'End If

                            'If cmbCategoria.SelectedIndex <> -1 Then
                            '    sql = "delete re_claprod "
                            '    sql = sql & " where codpro = '" & txtCodigo.Text.ToString.Trim & "'"
                            '    sql = sql & "   and codgen = 246 "
                            '    comando = New OracleCommand(sql, conn)
                            '    comando.Transaction = transaccion
                            '    comando.ExecuteNonQuery()

                            '    sql = "insert into re_claprod (codpro,codgen,codclasifica,newcla,codemp) values ( "
                            '    sql = sql & "'" & txtCodigo.Text.ToString.Trim & "', 246, " & cmbCategoria.SelectedValue.ToString &
                            '    ", " & cmbCategoria.SelectedValue.ToString & ",3 )"
                            '    comando = New OracleCommand(sql, conn)
                            '    comando.Transaction = transaccion
                            '    comando.ExecuteNonQuery()
                            'Else
                            '    MessageBox.Show("Debe seleccionar categoría, se aborta el grabado de datos", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            '    'If Not transaccion.Equals(Nothing) Then
                            '    '    transaccion.Rollback()
                            '    'End If
                            '    'Exit Sub
                            'End If

                            If cmbProvPrinc.SelectedIndex <> -1 Then
                                sql = "update re_provpro "
                                sql = sql & "set priori = 0 "
                                sql = sql & " where codpro = '" & txtCodigo.Text.ToString.Trim & "'"
                                comando = New OracleCommand(sql, conn)
                                comando.Transaction = transaccion
                                comando.ExecuteNonQuery()

                                sql = "update re_provpro "
                                sql = sql & "set priori = 1 "
                                sql = sql & " where codpro = '" & txtCodigo.Text.ToString.Trim & "'
                                            and rutprv = " & cmbProvPrinc.SelectedValue.ToString

                                comando = New OracleCommand(sql, conn)
                                comando.Transaction = transaccion
                                comando.ExecuteNonQuery()
                            Else
                                MessageBox.Show("Debe seleccionar proveedor principal", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                'If Not transaccion.Equals(Nothing) Then
                                '    transaccion.Rollback()
                                'End If
                                'Exit Sub
                            End If

                            If ActualizaLogProducto(txtCodigo.Text.ToString.Trim, StrEstado, transaccion, conn) = False Then
                                MessageBox.Show("Ha ocurrido un error al grabar log de producto, se aborta el grabado de datos", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                'If Not transaccion.Equals(Nothing) Then
                                '    transaccion.Rollback()
                                'End If
                                'Exit Sub
                            End If

                            'sql = " Begin "
                            'sql = sql & "  UtlSubFam.upd( " & PIntLinea & ","
                            'sql = sql & PIntFamilia & ","
                            'sql = sql & PIntMarca & ","
                            'sql = sql & 0 & ","
                            'sql = sql & cmbLinea.SelectedValue.ToString & ","
                            'sql = sql & cmbFamilia.SelectedValue.ToString & ","
                            ''sql = sql & cmbMarca.SelectedValue.ToString & ","
                            'sql = sql & "0);"
                            'sql = sql & "End;"
                            'comando = New OracleCommand(sql, conn)
                            'comando.Transaction = transaccion
                            'comando.ExecuteNonQuery()
                            If Val(txtCosto.Text) <> LngCostoOriginal Then
                                sql = "select codpro,a.codcnl, MARGEN "
                                sql = sql & " from re_canprod a, ma_listaprecio b where a.codpro = '" & txtCodigo.Text.ToString.Trim & "' and a.codemp = " & Globales.empresa.ToString
                                sql = sql & " and b.codcnl=a.codcnl and b.activa='S' and b.actualiza='S' and b.codemp = a.codemp"
                                dt.Clear()
                                comando = New OracleCommand(sql, conn, transaccion)
                                dataAdapter = New OracleDataAdapter(comando)
                                dataAdapter.Fill(dt)
                                For i = 0 To dt.Rows.Count - 1
                                    StrCodigo = CStr(dt.Rows(i).Item("codpro"))
                                    IntCanal = Val(dt.Rows(i).Item("codcnl"))
                                    LngMargen = Val(dt.Rows(i).Item("margen"))
                                    lngPrecio = Val(txtCosto.Text) / (1 - (LngMargen / 100))

                                    sql = "update re_canprod set precio = " & lngPrecio
                                    sql = sql & " where codpro = '" & StrCodigo & "'"
                                    sql = sql & "   and codcnl = " & IntCanal
                                    sql = sql & "   and codemp = " & Globales.empresa.ToString
                                    comando = New OracleCommand(sql, conn)
                                    comando.Transaction = transaccion
                                    comando.ExecuteNonQuery()
                                Next
                                If Globales.empresa.ToString = 3 Then
                                    sql = " BEGIN PUT_RECALCULA_PRECIOFIJO_NEW(3,'1','" & txtCodigo.Text & "'); END; "
                                    comando = New OracleCommand(sql, conn)
                                    comando.Transaction = transaccion
                                    comando.ExecuteNonQuery()
                                End If
                                'If Globales.empresa.ToString = 6 Then
                                '    sql = " BEGIN PUT_RECALCULA_PRECIOFIJO(6,'1'); END; "
                                '    comando = New OracleCommand(sql, conn)
                                '    comando.Transaction = transaccion
                                '    comando.ExecuteNonQuery()
                                'End If
                            End If
                        Else
                            MsgBox("Solo está disponible modificación de productos existentes")
                            Exit Sub
                            '' si es ingreso , se validaran que el producto poseea todos los datos
                            '' iniciales que necesiten los productos
                            If txtDescripcion.Text = "" Then
                                ' TxtDescripcion.SetFocus
                                txtDescripcion.BackColor = Color.Aqua
                                StrStatus = "ERR"
                            End If

                            If cmbUnidad.SelectedIndex = -1 Then
                                ' CmbUnidad.SetFocus
                                cmbUnidad.BackColor = Color.Aqua
                                StrStatus = "ERR"
                            End If

                            If cmbLinea.SelectedIndex = -1 Then
                                'CmbLinea.SetFocus
                                StrStatus = "ERR"
                                cmbLinea.BackColor = Color.Aqua
                            End If
                            If cmbFamilia.SelectedIndex = -1 Then
                                'CmbFamilia.SetFocus
                                StrStatus = "ERR"
                                cmbFamilia.BackColor = Color.Aqua
                            End If
                            If cmbMarca.SelectedIndex = -1 Then
                                'CmbMarca.SetFocus
                                StrStatus = "ERR"
                                cmbMarca.BackColor = Color.Aqua
                            End If
                            If Val(txtUMinima.Text) = 0 Then
                                'TxtUMinima.SetFocus
                                StrStatus = "ERR"
                                txtUMinima.BackColor = Color.Aqua
                            End If
                            If Val(txtSMinimo.Text) < 0 Then
                                'TxtSMinimo.SetFocus
                                StrStatus = "ERR"
                                txtSMinimo.BackColor = Color.Aqua
                            End If

                            If Val(txtStkCri.Text) < 0 Then
                                'TxtSMinimo.SetFocus
                                StrStatus = "ERR"
                                txtStkCri.BackColor = Color.Aqua
                            End If

                            If Val(txtStkInt.Text) < 0 Then
                                'TxtSMinimo.SetFocus
                                StrStatus = "ERR"
                                txtStkInt.BackColor = Color.Aqua
                            End If

                            If StrStatus = "ERR" Then
                                MessageBox.Show(" Existen valores en blanco que deben ser ingresados", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                'btn_Grabar.Visible = False
                                txtDescripcion.Focus()
                                Exit Sub
                            End If
                            sql = "select * from ma_product where codpro = '" & txtCodigo.Text.ToString.Trim & "' "
                            dt.Clear()
                            comando = New OracleCommand(sql, conn, transaccion)
                            dataAdapter = New OracleDataAdapter(comando)
                            dataAdapter.Fill(dt)
                            If dt.Rows.Count > 0 Then
                                MessageBox.Show(" Codigo ha sido Creado con anterioridad, Verifique", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                txtCodigo.Focus()
                                Exit Sub
                            End If

                            'sql = " INSERT into MA_PRODUCT (codpro,despro,codlin, codsubfam, codsubfam2, codfam, codmar,tipoprod, "
                            sql = " INSERT into MA_PRODUCT (codpro,despro,codlin, codsubfam, codmar,tipoprod, "
                            sql = sql & " codmod,codbar,fecing,coduni, "
                            sql = sql & " costo, costo_corp, ultcos, indsbr,codbdg,unmivt,pedsto,moneda,colate,stkase, stkpen_dmg,"
                            ''382         sql = sql & " minimo,maximo,stkcri,stkint,maxcd,mincd,cricd,intcd, "
                            sql = sql & " maxcd,mincd,cricd,intcd, "
                            sql = sql & " estpro, tipalm, reembj,stkfsc,stkcom,stkpen,pagcat,numfto,numser,control_calidad, stock_dc, con_vencimiento, importado, gobierno, impuesto, codigo_uni, codneg) "
                            sql = sql & " VALUES('" & txtCodigo.Text.ToString.Trim & "', getdelcar('" & txtDescripcion.Text & "') ,"
                            sql = sql & cmbLinea.SelectedValue.ToString & ","
                            sql = sql & cmbFamilia.SelectedValue.ToString & ","
                            'sql = sql & cmbGrupo.SelectedValue.ToString & ","
                            'sql = sql & cmbSubGrupo.SelectedValue.ToString & ","
                            sql = sql & cmbMarca.SelectedValue.ToString & ","
                            sql = sql & cmbTipoProd.SelectedValue.ToString & ",0,"

                            sql = sql & "'" & txtCodBarra.Text & "',"
                            sql = sql & "sysdate " & ","
                            sql = sql & cmbUnidad.SelectedValue.ToString & ","
                            sql = sql & IIf(Trim(txtCosto.Text) = "", 0, Val(txtCosto.Text)) & "," & IIf(Trim(txtCosto.Text) = "", 0, Val(txtCosto.Text)) & ",0,'S',0, "
                            sql = sql & txtUMinima.Text & ","
                            sql = sql & IIf(rdbStock.Checked = True, "'S'", "'P'") & ","
                            sql = sql & IIf(chkMoneda.Checked = True, "'1'", "'0'") & ","
                            sql = sql & IIf(chkColateral.Checked = True, "'1'", "'0'") & ","
                            sql = sql & IIf(Trim(txtStkAse.Text) = "", 0, txtStkAse.Text) & ","
                            sql = sql & "0,"
                            sql = sql & IIf(Trim(txtMaxCd.Text) = "", 0, txtMaxCd.Text) & ", " & IIf(Trim(txtMinCd.Text) = "", 0, txtMinCd.Text) & ","
                            sql = sql & IIf(Trim(txtCriCd.Text) = "", 0, txtCriCd.Text) & ", " & IIf(Trim(txtIntCd.Text) = "", 0, txtIntCd.Text) & ","

                            sql = sql & cmbEstado.SelectedValue.ToString & ","
                            sql = sql & cmbTipoAlmacenamiento.SelectedValue.ToString & ","
                            sql = sql & IIf(chkReembalaje.Checked = True, "'1'", "'0'")
                            sql = sql & ",0,0,0,"
                            sql = sql & "0," & IIf(Trim(txtUndPed.Text) = "", 0, txtUndPed.Text) & ","
                            sql = sql & IIf(chkNumSer.Checked = False, "'N'", "'S'") & ","
                            sql = sql & IIf(chkCalidad.Checked = False, "'N'", "'S'") & ","

                            sql = sql & IIf(chkRDimerc.Checked = False, "'N'", "'S'") & ","
                            '409         sql = sql & IIf(ChkRDimeiggs.Value = 0, "'N'", "'S'") & ","

                            sql = sql & "'" & txtConVence.Text & "',"
                            sql = sql & IIf(chkImportado.Checked = False, "'N'", "'S'") & ","
                            sql = sql & IIf(chkGobierno.Checked = False, "'N'", "'S'") & ","
                            sql = sql & cmbTipoImp.SelectedValue.ToString & ","
                            sql = sql & IIf(txtCodUni.Text = "", "' '", "'" & txtCodUni.Text & "'") & ", 0)"
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()

                            If cmbRotacion.SelectedIndex >= 0 Then
                                sql = "delete re_claspro where codpro = '" & txtCodigo.Text.ToString.Trim & "'"
                                sql = sql & " and codemp = " & Globales.empresa.ToString
                                comando = New OracleCommand(sql, conn)
                                comando.Transaction = transaccion
                                comando.ExecuteNonQuery()

                                sql = "insert into re_claspro (codpro,codval,codemp) values ('" & txtCodigo.Text.ToString.Trim & "',"
                                sql = sql & cmbRotacion.SelectedValue.ToString & ","
                                sql = sql & Globales.empresa.ToString & " )"
                                comando = New OracleCommand(sql, conn)
                                comando.Transaction = transaccion
                                comando.ExecuteNonQuery()
                            End If

                            StrEstado = "ING"

                            If ActualizaLogProducto(txtCodigo.Text.ToString.Trim, StrEstado, transaccion, conn) = False Then
                                MessageBox.Show("Ha ocurrido un error al grabar log de producto, se aborta el grabado de datos", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                'If Not transaccion.Equals(Nothing) Then
                                '    transaccion.Rollback()
                                'End If
                                'Exit Sub
                            End If

                            sql = "           Begin "
                            sql = sql & "  UtlSubFam.Ins( " & cmbLinea.SelectedValue.ToString & ","
                            sql = sql & cmbFamilia.SelectedValue.ToString & ","
                            sql = sql & cmbMarca.SelectedValue.ToString & ","
                            sql = sql & 0 & ");"
                            sql = sql & "End;   "
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()



                            If chkMPropia.Checked = True Then
                                sql = "INSERT INTO qv_marcapropia_dimerc ( codpro, coduni)"
                                sql = sql & "  VALUES ("
                                sql = sql & "'" & txtCodigo.Text.ToString.Trim & "',"
                                sql = sql & " getcodigouni('" & txtCodigo.Text.ToString.Trim & "')"
                                comando = New OracleCommand(sql, conn)
                                comando.Transaction = transaccion
                                comando.ExecuteNonQuery()
                            End If

                            'If chkMPropiaInter.Checked = True Then
                            '    sql = "INSERT INTO qv_mpropia_dimerc_hst ( codpro, coduni, tipo, fechavig )"
                            ''    sql = sql & "  VALUES ("
                            '    sql = sql & "'" & txtCodigo.Text & "',"
                            '    sql = sql & " getcodigouni('" & txtCodigo.Text & "') " & ","
                            '    sql = sql & " 'IMPORTADO' , "
                            '    sql = sql & " to_date('" & Now.Date & "','DD/MM/YYYY')"
                            '    sql = sql & " )"
                            '    comando = New OracleCommand(sql, conn)
                            '    comando.Transaction = transaccion
                            '    comando.ExecuteNonQuery()
                            'End If

                        End If

                        If Trim(txtSMinimo.Text) = "" Then
                            txtSMinimo.Text = "0"
                        End If
                        If Trim(txtSMaximo.Text) = "" Then
                            txtSMaximo.Text = "0"
                        End If
                        If Trim(txtStkCri.Text) = "" Then
                            txtStkCri.Text = "0"
                        End If
                        If Trim(txtStkInt.Text) = "" Then
                            txtStkInt.Text = "0"
                        End If

                        If Trim(txtMinCd.Text) = "" Then
                            txtMinCd.Text = "0"
                        End If
                        If Trim(txtMaxCd.Text) = "" Then
                            txtMaxCd.Text = "0"
                        End If
                        If Trim(txtCriCd.Text) = "" Then
                            txtCriCd.Text = "0"
                        End If
                        If Trim(txtIntCd.Text) = "" Then
                            txtIntCd.Text = "0"
                        End If

                        '''''''''''' PARAMETROS DE BODEGA ANTOFAGASTA
                        '''''''sql = "Select 1 from re_bodprod_dimerc where codpro = '" & txtCodigo.Text.ToString.Trim & "'"
                        '''''''sql = sql & " and codemp = " & Globales.empresa.ToString
                        '''''''sql = sql & " and codbod = 66"
                        '''''''dt.Clear()
                        '''''''comando = New OracleCommand(sql, conn, transaccion)
                        '''''''dataAdapter = New OracleDataAdapter(comando)
                        '''''''dataAdapter.Fill(dt)
                        '''''''If dt.Rows.Count > 0 Then
                        '''''''    sql = "update RE_BODPROD_DIMERC set "
                        '''''''    sql = sql & "  stkmin = " & txtSMinimo.Text
                        '''''''    sql = sql & ", stkmax = " & txtSMaximo.Text
                        '''''''    sql = sql & ", stkcri = " & txtStkCri.Text
                        '''''''    sql = sql & ", stkint = " & txtStkInt.Text
                        '''''''    sql = sql & " where codemp = " & Globales.empresa.ToString
                        '''''''    sql = sql & "   and codbod = 66"
                        '''''''    sql = sql & "   and codpro = '" & txtCodigo.Text.ToString.Trim & "'"
                        '''''''    comando = New OracleCommand(sql, conn)
                        '''''''    comando.Transaction = transaccion
                        '''''''    comando.ExecuteNonQuery()
                        '''''''Else
                        '''''''    sql = "Insert into RE_BODPROD_DIMERC (codemp,codbod,stocks,codpro,"
                        '''''''    sql = sql & " stkmin,stkmax,stkcri,stkint ) VALUES ("
                        '''''''    sql = sql & "" & Globales.empresa.ToString & ","
                        '''''''    sql = sql & "66,0,"
                        '''''''    sql = sql & "'" & txtCodigo.Text.ToString.Trim & "',"
                        '''''''    sql = sql & txtSMinimo.Text & "," & txtSMaximo.Text & ","
                        '''''''    sql = sql & txtStkCri.Text & "," & txtStkInt.Text & " "
                        '''''''    sql = sql & " )"
                        '''''''    comando = New OracleCommand(sql, conn)
                        '''''''    comando.Transaction = transaccion
                        '''''''    comando.ExecuteNonQuery()
                        '''''''End If

                        '''''''''''' Maximos Solo Dimerc+Locales
                        '''''''sql = "Select 1 from re_bodprod where codpro = '" & txtCodigo.Text.ToString.Trim & "'"
                        '''''''sql = sql & " and codemp = " & Globales.empresa.ToString
                        '''''''sql = sql & " and codbod = 0"
                        '''''''dt.Clear()
                        '''''''comando = New OracleCommand(sql, conn, transaccion)
                        '''''''dataAdapter = New OracleDataAdapter(comando)
                        '''''''dataAdapter.Fill(dt)
                        '''''''If dt.Rows.Count > 0 Then
                        '''''''    sql = "update RE_BODPROD set "
                        '''''''    sql = sql & "  stkmin = " & txtSMinimo.Text
                        '''''''    sql = sql & ", stkmax = " & txtSMaximo.Text
                        '''''''    sql = sql & ", stkcri = " & txtStkCri.Text
                        '''''''    sql = sql & ", stkint = " & txtStkInt.Text
                        '''''''    sql = sql & " where codemp = " & Globales.empresa.ToString
                        '''''''    sql = sql & "   and codbod = 0"
                        '''''''    sql = sql & "   and codpro = '" & txtCodigo.Text.ToString.Trim & "'"
                        '''''''    comando = New OracleCommand(sql, conn)
                        '''''''    comando.Transaction = transaccion
                        '''''''    comando.ExecuteNonQuery()
                        '''''''Else
                        '''''''    sql = "insert into RE_BODPROD ( codemp,codbod,codpro,"
                        '''''''    sql = sql & " stkmin,stkmax,stkcri,stkint ) VALUES ("
                        '''''''    sql = sql & "" & Globales.empresa.ToString & ","
                        '''''''    sql = sql & "0,"
                        '''''''    sql = sql & "'" & txtCodigo.Text.ToString.Trim & "',"
                        '''''''    sql = sql & txtSMinimo.Text & "," & txtSMaximo.Text & ","
                        '''''''    sql = sql & txtStkCri.Text & "," & txtStkInt.Text & " "
                        '''''''    sql = sql & " )"
                        '''''''    comando = New OracleCommand(sql, conn)
                        '''''''    comando.Transaction = transaccion
                        '''''''    comando.ExecuteNonQuery()
                        '''''''End If

                        ''''''''''' Maximos Solo Dimerc
                        '''''''sql = "Select 1 from re_bodprod where codpro = '" & txtCodigo.Text.ToString.Trim & "'"
                        '''''''sql = sql & " and codemp = " & Globales.empresa.ToString
                        '''''''sql = sql & " and codbod = 1"
                        '''''''dt.Clear()
                        '''''''comando = New OracleCommand(sql, conn, transaccion)
                        '''''''dataAdapter = New OracleDataAdapter(comando)
                        '''''''dataAdapter.Fill(dt)
                        '''''''If dt.Rows.Count > 0 Then
                        '''''''    sql = "update RE_BODPROD set "
                        '''''''    sql = sql & "  stkmin = " & txtMinCd.Text
                        '''''''    sql = sql & ", stkmax = " & txtMaxCd.Text
                        '''''''    sql = sql & ", stkcri = " & txtCriCd.Text
                        '''''''    sql = sql & ", stkint = " & txtIntCd.Text
                        '''''''    sql = sql & " where codemp = " & Globales.empresa.ToString
                        '''''''    sql = sql & "   and codbod = 1"
                        '''''''    sql = sql & "   and codpro = '" & txtCodigo.Text.ToString.Trim & "'"
                        '''''''    comando = New OracleCommand(sql, conn)
                        '''''''    comando.Transaction = transaccion
                        '''''''    comando.ExecuteNonQuery()
                        '''''''Else
                        '''''''    sql = "insert into RE_BODPROD ( codemp, codbod, codpro,"
                        '''''''    sql = sql & " stkmin,stkmax,stkcri,stkint ) VALUES ("
                        '''''''    sql = sql & "" & Globales.empresa.ToString & ","
                        '''''''    sql = sql & "1,"
                        '''''''    sql = sql & "'" & txtCodigo.Text.ToString.Trim & "',"
                        '''''''    sql = sql & txtMinCd.Text & "," & txtMaxCd.Text & ","
                        '''''''    sql = sql & txtCriCd.Text & "," & txtIntCd.Text & " "
                        '''''''    sql = sql & " )"
                        '''''''    comando = New OracleCommand(sql, conn)
                        '''''''    comando.Transaction = transaccion
                        '''''''    comando.ExecuteNonQuery()
                        '''''''End If

                        sql = "select estpro,pedsto from ma_product where codpro = '" & txtCodigo.Text.ToString.Trim & "'"
                        dt.Clear()
                        comando = New OracleCommand(sql, conn, transaccion)
                        dataAdapter = New OracleDataAdapter(comando)
                        dataAdapter.Fill(dt)
                        indProd = 0
                        If dt.Rows.Count > 0 Then
                            Select Case CStr(dt.Rows(0).Item("estpro"))
                                Case 1  ' Activo
                                    If CStr(dt.Rows(0).Item("pedsto")) = "S" Then
                                        indProd = 1    'AA
                                    ElseIf CStr(dt.Rows(0).Item("pedsto")) = "P" Then
                                        indProd = 4    '
                                    End If
                                Case 2   ' Descontinuado
                                    If CStr(dt.Rows(0).Item("pedsto")) = "S" Then
                                        indProd = 5
                                    End If
                                Case 6  ' Borrado no visible en ventas
                                    indProd = 7
                                Case 4
                            End Select

                            If indProd = 1 Then
                                sql = "update re_canprod set indica = ' '"
                            Else
                                sql = "update re_canprod set indica = " & indProd.ToString
                            End If
                            sql = sql & " where codpro = '" & txtCodigo.Text.ToString.Trim & "'"
                            sql = sql & "   and codemp = " & Globales.empresa.ToString
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()
                        End If

                        '''''''''''''''''''''''''''' Borro y Grabo Comisiones de nuevo ''''''''''''''''''' BORRAR COMISIONES??
                        '''''''''sql = "DELETE  ma_producto_comision "
                        '''''''''sql = sql & " WHERE codemp =" & Globales.empresa.ToString
                        '''''''''sql = sql & "   and codpro = '" & txtCodigo.Text.ToString.Trim & "'"
                        '''''''''comando = New OracleCommand(sql, conn)
                        '''''''''comando.Transaction = transaccion
                        '''''''''comando.ExecuteNonQuery()


                        '''''''''For Itera = 0 To dgvComision.Rows.Count - 1
                        '''''''''    sql = "INSERT into ma_producto_comision (codemp, fecha, codpro, categoria, comision)"
                        '''''''''    sql = sql & " values( "
                        '''''''''    sql = sql & " " & Globales.empresa.ToString & ""
                        '''''''''    sql = sql & ", to_date('" & dgvComision.Item("Fecha", Itera).Value & "','dd/mm/yyyy HH24:mi:ss')"
                        '''''''''    sql = sql & ", '" & txtCodigo.Text.ToString.Trim & "'"
                        '''''''''    sql = sql & ", '" & dgvComision.Item("Tipo", Itera).Value.ToString & "'"
                        '''''''''    sql = sql & ", " & dgvComision.Item("%Comi", Itera).Value.ToString.Replace(",", ".") & ""
                        '''''''''    sql = sql & " )"
                        '''''''''    comando = New OracleCommand(sql, conn)
                        '''''''''    comando.Transaction = transaccion
                        '''''''''    comando.ExecuteNonQuery()
                        '''''''''Next

                        ''''''''''''''' ASIGNA VIDA UTIL DE PRODUCTO 'CONSULTAR QUE SUCEDE CON VIDA UTIL, VIENE DE SAP?
                        ''''''''''sql = "select * from  re_claprod "
                        ''''''''''sql = sql & " WHERE codgen = 247"
                        ''''''''''sql = sql & "   and codpro = '" & txtCodigo.Text.ToString.Trim & "'"
                        ''''''''''sql = sql & "   and codemp = " & Globales.empresa.ToString
                        ''''''''''dt.Clear()
                        ''''''''''comando = New OracleCommand(sql, conn, transaccion)
                        ''''''''''dataAdapter = New OracleDataAdapter(comando)
                        ''''''''''dataAdapter.Fill(dt)
                        ''''''''''If dt.Rows.Count > 0 Then
                        ''''''''''    sql = "Update re_claprod Set codclasifica = " & Val(txtVidaUtil.Text)
                        ''''''''''    sql = sql & " where codpro = '" & txtCodigo.Text.ToString.Trim & "'"
                        ''''''''''    sql = sql & "   and codgen = 247"
                        ''''''''''    sql = sql & "   and codemp = " & Globales.empresa.ToString
                        ''''''''''    comando = New OracleCommand(sql, conn)
                        ''''''''''    comando.Transaction = transaccion
                        ''''''''''    comando.ExecuteNonQuery()
                        ''''''''''Else
                        ''''''''''    sql = "INSERT into re_claprod (codpro, codgen, codclasifica, newcla, codemp)"
                        ''''''''''    sql = sql & " values( "
                        ''''''''''    sql = sql & "'" & txtCodigo.Text.ToString.Trim & "'"
                        ''''''''''    sql = sql & ",247"
                        ''''''''''    sql = sql & "," & Val(txtVidaUtil.Text)
                        ''''''''''    sql = sql & ",0"
                        ''''''''''    sql = sql & "," & Globales.empresa.ToString
                        ''''''''''    sql = sql & " )"
                        ''''''''''    comando = New OracleCommand(sql, conn)
                        ''''''''''    comando.Transaction = transaccion
                        ''''''''''    comando.ExecuteNonQuery()
                        ''''''''''End If
                        '''''''''''''''''''''''''''''''''''
                        'If Globales.detDerUsu(Globales.user, 19) Then
                        ' Corrección monetaria
                        sql = "Select * from  re_producto_cm "
                        sql = sql & " WHERE codpro = '" & txtCodigo.Text.ToString.Trim & "'"
                        sql = sql & "   and codemp = 3"  '' fijo por el momento
                        dt.Clear()
                        comando = New OracleCommand(sql, conn, transaccion)
                        dataAdapter = New OracleDataAdapter(comando)
                        dataAdapter.Fill(dt)

                        If dt.Rows.Count = 0 Then
                            sql = "INSERT INTO RE_PRODUCTO_CM (codemp, codpro, tipo, valor )"
                            sql = sql & "  VALUES ("
                            sql = sql & "3,"
                            sql = sql & "'" & txtCodigo.Text.ToString.Trim & "',"

                            If cmbCM.SelectedValue = 1 Then
                                sql = sql & " 'PRI', "
                            Else
                                sql = sql & " 'NEG', "
                            End If

                            If cmbCM.SelectedValue Is Nothing Then
                                sql = sql & "'0')"
                            Else
                                sql = sql & cmbCM.SelectedValue.ToString
                                sql = sql & " )"
                            End If

                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()
                        Else
                            If cmbCM.SelectedIndex > -1 Then
                                sql = "Update RE_PRODUCTO_CM set "
                                sql = sql & "  valor = " & cmbCM.SelectedValue.ToString
                                If cmbCM.SelectedValue = 1 Then
                                    sql = sql & " ,tipo  = 'PRI'"
                                Else
                                    sql = sql & " ,tipo  = 'NEG'"
                                End If
                                sql = sql & " where codemp = 3"
                                sql = sql & "   and codpro = '" & txtCodigo.Text.ToString.Trim & "'"
                                comando = New OracleCommand(sql, conn)
                                comando.Transaction = transaccion
                                comando.ExecuteNonQuery()
                            End If
                        End If
                        'End If

                        ''''''    If Globales.detDerUsu(Globales.user, 232) Then 'MODIFICAR SUCURSAL?
                        ''''''    ' SUCURSAL

                        ''''''    For I = 1 To dgvSucursal.RowCount - 1

                        ''''''        If dgvSucursal.Item("Sel", I).Value = 0 Then


                        ''''''            sql = "Select * from re_producto_sucursal "
                        ''''''            sql = sql & " WHERE codpro = '" & txtCodigo.Text.ToString.Trim & "'"
                        ''''''            sql = sql & "   and codemp = " & Globales.empresa.ToString
                        ''''''            sql = sql & "   and sucursal = " & dgvSucursal.Item("Código", I).Value.ToString
                        ''''''            dt.Clear()
                        ''''''            comando = New OracleCommand(sql, conn, transaccion)
                        ''''''            dataAdapter = New OracleDataAdapter(comando)
                        ''''''            dataAdapter.Fill(dt)
                        ''''''            If dt.Rows.Count > 0 Then

                        ''''''                sql = " DELETE RE_PRODUCTO_SUCURSAL "
                        ''''''                sql = sql & " WHERE codpro = '" & txtCodigo.Text.ToString.Trim & "'"
                        ''''''                sql = sql & "   and codemp = " & Globales.empresa.ToString
                        ''''''                sql = sql & "   and sucursal = " & dgvSucursal.Item("Código", I).Value.ToString
                        ''''''                comando = New OracleCommand(sql, conn)
                        ''''''                comando.Transaction = transaccion
                        ''''''                comando.ExecuteNonQuery()

                        ''''''            End If
                        ''''''        Else

                        ''''''            sql = "Select * from re_producto_sucursal "
                        ''''''            sql = sql & " WHERE codpro = '" & txtCodigo.Text.ToString.Trim & "'"
                        ''''''            sql = sql & "   and codemp = " & Globales.empresa.ToString
                        ''''''            sql = sql & "   and sucursal = " & dgvSucursal.Item("Código", I).Value.ToString
                        ''''''            dt.Clear()
                        ''''''            comando = New OracleCommand(sql, conn, transaccion)
                        ''''''            dataAdapter = New OracleDataAdapter(comando)
                        ''''''            dataAdapter.Fill(dt)
                        ''''''            If dt.Rows.Count = 0 Then


                        ''''''                sql = "INSERT INTO re_producto_sucursal (codemp, codpro, sucursal )"
                        ''''''                sql = sql & "  VALUES ("
                        ''''''                sql = sql & Globales.empresa.ToString
                        ''''''                sql = sql & ",'" & txtCodigo.Text.ToString.Trim & "'"
                        ''''''                sql = sql & ",'" & dgvSucursal.Item("Código", I).Value.ToString & "'"
                        ''''''                sql = sql & " ) "
                        ''''''                comando = New OracleCommand(sql, conn)
                        ''''''                comando.Transaction = transaccion
                        ''''''                comando.ExecuteNonQuery()

                        ''''''            End If


                        ''''''        End If
                        ''''''    Next I
                        ''''''End If

                        '''' MARCA PROPIA DIMERC (COMERCIAL) SE SEPARA MARCAS PROPIAS COMERCIAL E INVENTARIO 30-09-2016
                        'SOLICITADO POR JAVIER VILLARROEL
                        If chkMPropia.Checked = False Then
                            sql = " DELETE qv_marcapropia_dimerc "
                            sql = sql & " WHERE codpro = '" & txtCodigo.Text.ToString.Trim & "'"
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()
                        Else
                            sql = "Select * from  qv_marcapropia_dimerc "
                            sql = sql & " WHERE codpro = '" & txtCodigo.Text.ToString.Trim & "'"
                            ''sql = sql & "   and tipo   = 'NACIONAL'"
                            dt.Clear()
                            comando = New OracleCommand(sql, conn, transaccion)
                            dataAdapter = New OracleDataAdapter(comando)
                            dataAdapter.Fill(dt)
                            If dt.Rows.Count = 0 Then
                                sql = "INSERT INTO qv_marcapropia_dimerc ( codpro, coduni )"
                                sql = sql & "  VALUES ("
                                sql = sql & "'" & txtCodigo.Text.ToString.Trim & "',"
                                sql = sql & " getcodigouni('" & txtCodigo.Text.ToString.Trim & "') " & ")"
                                comando = New OracleCommand(sql, conn)
                                comando.Transaction = transaccion
                                comando.ExecuteNonQuery()
                            End If
                        End If

                        'MARCA PROPIA INVENTARIO 30-09-2016
                        'SOLICITADO POR JAVIER VILLARROEL
                        sql = " select MARCA_PROPIA  "
                        sql = sql & "   FROM QV_MARCAPROPIA_INVENTARIO "
                        sql = sql & "   WHERE CODPRO='" + txtCodigo.Text.ToString.Trim + "' "
                        'sql = sql & "   AND CODUNI=GETCODIGOUNI('" + txtCodigo.Text.ToString.Trim + "') "
                        dt.Clear()
                        comando = New OracleCommand(sql, conn, transaccion)
                        dataAdapter = New OracleDataAdapter(comando)
                        dataAdapter.Fill(dt)
                        If dt.Rows.Count = 0 Then
                            sql = " insert into qv_marcapropia_INVENTARIO(CODPRO,CODUNI,MARCA_PROPIA)  "
                            sql = sql & "values('" + txtCodigo.Text.ToString.Trim + "', "
                            sql = sql & "GETCODIGOUNI('" + txtCodigo.Text.ToString.Trim + "') "
                            If rdbMPInventarioSI.Checked Then
                                sql = sql & ",'S') "
                            Else
                                sql = sql & ",'N') "
                            End If

                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()
                        Else
                            sql = "update qv_marcapropia_inventario "
                            If rdbMPInventarioSI.Checked Then
                                sql += "set marca_propia = 'S' "
                            Else
                                sql += "set marca_propia = 'N' "
                            End If
                            sql += " where codpro = '" + txtCodigo.Text.ToString.Trim + "'"
                            sql += " and coduni = getcodigouni('" + txtCodigo.Text.ToString.Trim + "')"
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()
                        End If

                        '"CORPORATIVO"
                        If cmbTipoProd.SelectedValue = 2 Then
                            sql = "select codpro, nvl(a.margen,0) margen "
                            sql = sql & " from re_canprod a, ma_listaprecio b where a.codpro = '" & txtCodigo.Text.ToString.Trim & "' and a.codemp = " & Globales.empresa.ToString
                            sql = sql & "  and b.codcnl=a.codcnl and b.codemp = a.codemp and b.codcnl=100"
                            dt.Clear()
                            comando = New OracleCommand(sql, conn, transaccion)
                            dataAdapter = New OracleDataAdapter(comando)
                            dataAdapter.Fill(dt)
                            If dt.Rows.Count > 0 Then
                                LngMargen = Val(dt.Rows(0).Item("margen"))
                                lngPrecio = CLng(Val(txtCosto.Text) / (1 - (LngMargen / 100)))

                                sql = "Update re_canprod set precio = " & lngPrecio
                                sql = sql & " where codpro = '" & txtCodigo.Text.ToString.Trim & "'"
                                sql = sql & "   and codcnl = 100"
                                sql = sql & "   and codemp = " & Globales.empresa.ToString
                                comando = New OracleCommand(sql, conn)
                                comando.Transaction = transaccion
                                comando.ExecuteNonQuery()

                            Else
                                lngPrecio = Math.Round(Val(txtCosto.Text.ToString), 0)

                                sql = "INSERT INTO re_canprod (codcnl,codpro,margen,precio,factor,pordes,despro,indica,descue,ptorjo,prefij,dpesos,modsup,codigo_uni,multip,old_precio,costo_cajas_manual,codemp,mgprefijo )"
                                sql = sql & "  VALUES ("
                                sql = sql & "100,"
                                sql = sql & "'" & txtCodigo.Text.ToString.Trim & "',"
                                sql = sql & "0,"
                                sql = sql & "'" & lngPrecio & "',"
                                sql = sql & "0,0, null,0,null,null,0,0,'N',null,1,null,null," & Globales.empresa.ToString & ",0"
                                sql = sql & " )"
                                comando = New OracleCommand(sql, conn)
                                comando.Transaction = transaccion
                                comando.ExecuteNonQuery()
                            End If
                        End If

                        transaccion.Commit()

                        MessageBox.Show("Datos Grabados Correctamente", "Finalizado", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        limpiar()
                    Catch ex As Exception
                        If Not transaccion.Equals(Nothing) Then
                            transaccion.Rollback()
                        End If
                        PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                        MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Finally
                        conn.Close()
                    End Try
                End Using
            End Using
        End If
    End Sub

    Private Sub rdbAgotarStock_CheckedChanged(sender As System.Object, e As System.EventArgs)
        If Not Globales.detDerUsu(Globales.user, 51) And cargo Then
            MessageBox.Show("No tiene Derecho a asignar ""Hasta Agotar Stock"" (Der. 51)", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            If strSobreventa = "S" Then
                rdbSobreVenta.Checked = True
                rdbAgotarStock.Checked = False
            Else
                rdbSobreVenta.Checked = False
                rdbAgotarStock.Checked = True
            End If
            Exit Sub
        End If
    End Sub

    Private Sub txtCodUni_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtCodUni.KeyPress
        Globales.soloNumeros(sender, e)
    End Sub

    Private Sub txtCosto_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtCosto.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            PuntoControl.RegistroUso("79")
            If Trim(txtCosto.Text) = "" Then
                txtCosto.Text = "0"
            End If
            btn_Grabar.Visible = True
            btn_Grabar.Focus()
        End If
    End Sub

    Private Sub txtSMaximo_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtSMaximo.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Trim(txtSMaximo.Text) <> "" Then
                If Val(txtSMinimo.Text) <= Val(txtSMaximo.Text) Then
                    rdbSobreVenta.Focus()
                Else
                    MessageBox.Show("Stock Minimo es mayor al Máximo", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Exit Sub
                End If
            End If
            txtStkCri.Focus()
        Else
            Globales.soloNumeros(sender, e)

        End If
    End Sub

    Private Sub txtSMinimo_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtSMinimo.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Trim(txtSMinimo.Text) <> "" Then
                txtSMaximo.Focus()
            End If
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtStkCri_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtStkCri.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Trim(txtStkCri.Text) <> "" Then
                txtStkInt.Focus()
            End If
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtMinCd_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtMinCd.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Trim(txtMinCd.Text) <> "" Then
                txtMaxCd.Focus()
            End If
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtMaxCd_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtMaxCd.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Trim(txtMaxCd.Text) <> "" Then
                txtCriCd.Focus()
            End If
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtCriCd_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtCriCd.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Trim(txtCriCd.Text) <> "" Then
                txtIntCd.Focus()
            End If
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtIntCd_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtIntCd.KeyPress
        Globales.soloNumeros(sender, e)
    End Sub

    Private Sub txtStkInt_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtStkInt.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Trim(txtStkInt.Text) <> "" Then
                txtMinCd.Focus()
            End If
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub cmbEstado_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles cmbEstado.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            cmbLinea.Focus()
        End If
    End Sub

    Private Sub cmbEstado_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cmbEstado.SelectedIndexChanged
        If cargo = True Then
            If cmbEstado.SelectedIndex <> 0 And cmbEstado.SelectedIndex <> -1 Then
                If Not Globales.detDerUsu(Globales.user, 50) Then
                    MessageBox.Show("No Tiene Derecho A Asignar Otro Estado, Que No Sea ACTIVO (Der.50)", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    cmbEstado.SelectedValue = codEstado
                End If
            End If
        End If
    End Sub

    Private Sub txtFscLoc_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtUltInv.KeyPress,
        txtUcPNB.KeyPress, txtUCDMG.KeyPress, txtTtoLoc.KeyPress, txtStkPenDmg.KeyPress, txtStkDimerc.KeyPress, txtReposicion.KeyPress, txtOcLoc.KeyPress, txtValorDelta.KeyPress,
        txtFscLoc.KeyPress, txtFecInv.KeyPress, txtCompraAnt.KeyPress, txtTipoDelta.KeyPress
        e.Handled = True
    End Sub
    Private Sub txtFscLoc_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtUltInv.KeyDown,
        txtUcPNB.KeyDown, txtUCDMG.KeyDown, txtTtoLoc.KeyDown, txtStkPenDmg.KeyDown, txtStkDimerc.KeyDown, txtReposicion.KeyDown, txtOcLoc.KeyDown, txtFscLoc.KeyDown,
        txtFecInv.KeyDown, txtCompraAnt.KeyDown, txtTipoDelta.KeyDown, txtValorDelta.KeyDown
        If e.KeyData = Keys.Delete Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtCosProm_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtCosProm.KeyPress
        e.Handled = True
    End Sub

    Private Sub txtCatAct_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtCatAct.KeyPress
        e.Handled = True
    End Sub

    Private Sub btn_Oci_Click(sender As System.Object, e As System.EventArgs) Handles btn_Oci.Click
        Dim consultaOci As New frmConsultaOci(txtCodigo.Text.ToString.Trim, txtDescripcion.Text)
        consultaOci.ShowDialog()
    End Sub

    Private Sub CmdAgregar_Click(sender As System.Object, e As System.EventArgs) Handles CmdAgregar.Click
        Dim BolExis As Boolean = False
        If Not Globales.detDerUsu(Globales.user, 72) Then
            MessageBox.Show("No tiene derecho para procesar esta opción de programa. (Der. 72)", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If

        If cmbComision.SelectedIndex = -1 Then
            MessageBox.Show("Debe ingresar una categoría de comisión", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        If Mid(dtpFchaVig.Value, 1, 2) <> "01" Then
            MessageBox.Show("Fecha debe empezar con el dia 01 (primero) de mes", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            dtpFchaVig.Value = "01" & Mid(dtpFchaVig.Value, 3, 8)
            Exit Sub
        End If

        For i = 0 To dgvComision.RowCount - 1
            If dgvComision.Item("Fecha", i).Value = dtpFchaVig.Value.Date Then
                BolExis = True
                Exit For
            End If
        Next
        If BolExis = False Then
            dgvComision.DataSource = Nothing
            dtComisiones.Rows.Add(dtpFchaVig.Value.Date, cmbComision.Text, cmbComision.SelectedValue.ToString)
            dgvComision.DataSource = dtComisiones
            dgvComision.Refresh()
            dgvComision.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            dgvComision.Columns("%Comi").DefaultCellStyle.Format = "n0"
            dgvComision.Columns("%Comi").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvComision.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        End If
    End Sub

    'Private Sub dgvSucursal_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs)
    '    If e.RowIndex <> -1 Then
    '        If e.ColumnIndex = 0 Then
    '            If dgvSucursal.Item("Sel", e.RowIndex).Value = 0 Then
    '                dgvSucursal.Item("Sel", e.RowIndex).Value = 1

    '            Else
    '                dgvSucursal.Item("Sel", e.RowIndex).Value = 0
    '            End If
    '        End If
    '    End If
    'End Sub

    'REVISAR

    Public Function ActualizaLogProducto(TxtCodPro As String, StrEstado As String, trans As OracleTransaction, conexion As OracleConnection) As Boolean

        Try
            Dim comando As OracleCommand
            Dim dataAdapter As OracleDataAdapter
            Dim dt As New DataTable
            Dim sql As String
            Dim LngSequen As Long
            Dim Posicion As String = ""

            ' RESCATA CORRELATIVO SIGUIENTE DEL LOG
            sql = "select rowid,sequen from lg_product where codpro = '" & TxtCodPro & "'"
            sql = sql & "order by sequen desc"
            dt.Clear()
            comando = New OracleCommand(sql, conexion)
            comando.Transaction = trans
            dataAdapter = New OracleDataAdapter(comando)
            dataAdapter.Fill(dt)
            If Not dt.Rows.Count = 0 Then
                LngSequen = IIf(Globales.EsNulo(dt.Rows(0).Item("sequen")) = "", 1, Val(dt.Rows(0).Item("sequen")) + 1)
            Else
                LngSequen = 1
            End If
            '
            ' Crea Registro Log a partir del maestro de productos
            '
            If Trim(txtSMinimo.Text) = "" Then
                txtSMinimo.Text = 0
            End If
            If Trim(txtSMaximo.Text) = "" Then
                txtSMaximo.Text = 0
            End If
            If Trim(txtStkCri.Text) = "" Then
                txtStkCri.Text = 0
            End If
            If Trim(txtStkInt.Text) = "" Then
                txtStkInt.Text = 0
            End If

            sql = "SET ROLE ROL_APLICACION "
            comando = New OracleCommand(sql, conexion)
            comando.Transaction = trans
            comando.ExecuteNonQuery()

            sql = "insert into LG_PRODUCT(codpro, despro, codlin, codfam, "
            sql = sql & " codmar, codmod, codbar, coduni, "
            sql = sql & " costo, ultcos, indsbr, codbdg, unmivt, pedsto, moneda, colate, minimo, maximo, "
            sql = sql & " estpro, tipalm, reembj, stkfsc, stkcom, stkpen, pagcat, numfto, numser, sequen,feclog,userid,estlog,horlog, terminal) "
            sql = sql & " values('" & txtCodigo.Text.ToString.Trim & "',getdelcar('" & txtDescripcion.Text & "'),"
            sql = sql & cmbLinea.SelectedValue.ToString & ","
            sql = sql & cmbFamilia.SelectedValue.ToString & ","
            'sql = sql & cmbMarca.SelectedValue.ToString & ","
            sql = sql & dtCliente.Rows(0).Item("codmar").ToString & ","
            sql = sql & "0,'"
            sql = sql & txtCodBarra.Text & "',"
            If cmbUnidad.SelectedIndex = -1 Then
                sql = sql & "1," 'cmbUnidad.SelectedValue.ToString & ","
            Else
                sql = sql & cmbUnidad.SelectedValue.ToString & ","
            End If
            sql = sql & IIf(Trim(txtCosto.Text) = "", 0, Val(txtCosto.Text)) & ",0," & IIf(rdbSobreVenta.Checked = True, "'S'", "'N'") & ",0,"
            sql = sql & txtUMinima.Text & ","
            sql = sql & IIf(rdbStock.Checked = True, "'S'", "'P'") & ","
            sql = sql & IIf(chkMoneda.Checked = True, "'1'", "'0'") & ","
            sql = sql & IIf(chkColateral.Checked = True, "'1'", "'0'") & ","
            sql = sql & txtSMinimo.Text & "," & txtSMaximo.Text & ","
            sql = sql & cmbEstado.SelectedValue.ToString & ","
            sql = sql & cmbTipoAlmacenamiento.SelectedValue.ToString & ","
            sql = sql & IIf(chkReembalaje.Checked = True, "'1'", "'0'")
            sql = sql & ",0,0,0,"
            sql = sql & "0,0, "
            sql = sql & IIf(chkNumSer.Checked = False, "'N'", "'S'") & ","
            sql = sql & LngSequen & ","
            sql = sql & "'" & Now.Date & "',"
            sql = sql & "'" & Globales.user & "',"
            sql = sql & "'" & StrEstado & "',"
            sql = sql & "'" & Format(Now, "HH:mm:ss") & "','"
            sql = sql & Application.CompanyName.ToString
            sql = sql & " ')"
            comando = New OracleCommand(sql, conexion)
            comando.Transaction = trans
            comando.ExecuteNonQuery()

            Return True
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        End Try

    End Function

    Private Sub btn_Anotación_Click(sender As System.Object, e As System.EventArgs) Handles btn_Anotación.Click
        Dim frmAnotación As New frmAnotaciones(txtCodigo.Text.ToString.Trim)
        frmAnotación.ShowDialog()
    End Sub

    Private Sub btn_ListaPrecio_Click(sender As System.Object, e As System.EventArgs) Handles btn_ListaPrecio.Click
        PuntoControl.RegistroUso("83")
        Dim listpre As New frmCanalProducto(txtCodigo.Text.ToString.Trim, txtDescripcion.Text, txtCosto.Text)
        listpre.ShowDialog()
    End Sub

    Private Sub btn_ProvProd_Click(sender As System.Object, e As System.EventArgs) Handles btn_ProvProd.Click
        PuntoControl.RegistroUso("84")
        Dim Provpro As New frmRelProvPro(txtCodigo.Text.ToString.Trim, txtDescripcion.Text)
        Provpro.ShowDialog()
    End Sub

    Private Sub txtUbicacion_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtUbicacion.KeyPress
        e.Handled = True
    End Sub

    Private Sub chkNoAplica_Click(sender As Object, e As EventArgs) Handles chkNoAplica.Click
        If chkNoAplica.Checked = True Then
            chkToxico.Checked = False
            chkImflamable.Checked = False
            chkCorrosivo.Checked = False
        End If
    End Sub

    Private Sub chkToxico_Click(sender As Object, e As EventArgs) Handles chkToxico.Click
        If chkToxico.Checked = True Then
            chkNoAplica.Checked = False
            chkImflamable.Checked = False
            chkCorrosivo.Checked = False
        End If
    End Sub

    Private Sub chkImflamable_Click(sender As Object, e As EventArgs) Handles chkImflamable.Click
        If chkImflamable.Checked = True Then
            chkNoAplica.Checked = False
            chkToxico.Checked = False
            chkCorrosivo.Checked = False
        End If
    End Sub

    Private Sub chkCorrosivo_Click(sender As Object, e As EventArgs) Handles chkCorrosivo.Click
        If chkCorrosivo.Checked = True Then
            chkNoAplica.Checked = False
            chkImflamable.Checked = False
            chkToxico.Checked = False
        End If
    End Sub

    Private Sub btnNegocio_Click(sender As Object, e As EventArgs) Handles btnNegocio.Click
        PuntoControl.RegistroUso("85")
        Dim frmsubneg As New frmSubnegocioPri(1)
        frmsubneg.ShowDialog()
        cargaCombo()
        buscarProducto()
    End Sub

    Private Sub btnSubgrupo_Click(sender As Object, e As EventArgs) Handles btnSubgrupo.Click
        PuntoControl.RegistroUso("86")
        Dim frmsubneg As New frmSubnegocioPri(2)
        frmsubneg.ShowDialog()
        cargaCombo()
        buscarProducto()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        PuntoControl.RegistroUso("87")
        If cargo = True Then
            Dim historial As New frmConsulta_Hist(txtCodigo.Text.ToString.Trim, txtDescripcion.Text, cmbMarca.Text)
            historial.ShowDialog()
        Else
            MessageBox.Show("Cargue un código antes de revisar historial.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
    End Sub

    Private Sub TabControl2_TabIndexChanged(sender As Object, e As EventArgs) Handles TabControl2.TabIndexChanged
        PuntoControl.RegistroUso("76")
    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles txtCostoAnto.TextChanged

    End Sub

    Private Sub txtPaleta_TextChanged(sender As Object, e As EventArgs) Handles txtPaleta.TextChanged
        If txtPaleta.Text = "S" And rdbContraPedido.Checked = True Or txtPaleta.Text = "N" And rdbContraPedido.Checked = False Then
            lblMensaje.Visible = True
        Else
            lblMensaje.Visible = False
        End If
    End Sub

    Private Sub btnRelprod_Click(sender As Object, e As EventArgs) Handles btnRelprod.Click
        If txtCodigo.Text.Length > 0 And txtDescripcion.Text.Length > 0 Then
            PuntoControl.RegistroUso("81")
            Dim frmPR As New frmProductosRelacionados(txtCodigo.Text, txtDescripcion.Text)
            frmPR.ShowDialog()
        End If
    End Sub

    'Private Sub cmbGrupo_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs)
    '    If e.KeyChar = Convert.ToChar(Keys.Enter) Then
    '        If cmbGrupo.SelectedIndex <> -1 Then
    '            cmbSubGrupo.Focus()
    '        End If
    '    End If
    'End Sub

    'Private Sub cmbSubGrupo_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs)
    '    If e.KeyChar = Convert.ToChar(Keys.Enter) Then
    '        If cmbSubGrupo.SelectedIndex <> -1 Then
    '            cmbTipoAlmacenamiento.Focus()
    '        End If
    '    End If
    'End Sub

    Private Sub frmFichaProducto_Shown(sender As System.Object, e As System.EventArgs) Handles MyBase.Shown
        Cursor.Current = Cursors.Arrow
    End Sub
    Private Function enviaCorreo(para As String, asunto As String, mensaje As String) As Boolean
        Try
            Dim Smtp_Server As New SmtpClient
            Dim e_mail As New MailMessage()
            Smtp_Server.UseDefaultCredentials = False
            Smtp_Server.Credentials = New Net.NetworkCredential("controlinventario@translogic.cl", "password_123")
            Smtp_Server.Host = "mta.gtdinternet.com"

            e_mail = New MailMessage()
            e_mail.From = New MailAddress("controlinventario@translogic.cl")
            e_mail.To.Add(para)
            e_mail.Subject = asunto
            e_mail.IsBodyHtml = False
            e_mail.Body = mensaje
            Smtp_Server.Send(e_mail)

            Return True
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        End Try
    End Function
    'Public Sub WriteData(ByVal data As String, ByRef IP As String)
    '    Console.WriteLine("Sending message """ & data & """ to " & IP)
    '    Dim client As TcpClient = New TcpClient()
    '    client.Connect(New IPEndPoint(IPAddress.Parse(IP), 5405))
    '    Dim valor As Boolean = client.Connected
    '    Dim stream As NetworkStream = client.GetStream()
    '    Dim sendBytes As Byte() = Encoding.ASCII.GetBytes(data)
    '    stream.Write(sendBytes, 0, sendBytes.Length)
    'End Sub
End Class