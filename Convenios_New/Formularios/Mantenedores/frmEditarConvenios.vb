﻿Imports System.Data.OracleClient
Public Class frmEditarConvenios
    Dim bd As New Conexion
    Dim dtGrilla As New DataTable
    Dim flagGrillaModificar As Boolean
    Dim flagGrillaEliminar As Boolean
    Dim flagemprel As Boolean
    Dim indice As Integer
    Dim convenio, costo As String
    Dim PuntoControl As New PuntoControl
    Private Sub btn_Salir_Click(sender As Object, e As EventArgs) Handles btn_Salir.Click
        Close()
    End Sub
    Private Sub creaGrilla()
        dtGrilla.Columns.Add("Codigo", Type.GetType("System.String"))
        dtGrilla.Columns.Add("Descripcion", Type.GetType("System.String"))
        dtGrilla.Columns.Add("Linea", Type.GetType("System.String"))
        dtGrilla.Columns.Add("Precio", Type.GetType("System.Double"))
        dtGrilla.Columns.Add("Costo Especial", Type.GetType("System.Double"))
        dtGrilla.Columns.Add("Fecven", Type.GetType("System.String"))
        dtGrilla.Columns.Add("codlin", Type.GetType("System.Double"))
        dtGrilla.Columns.Add("Tipo", Type.GetType("System.String"))
        dtGrilla.Columns.Add("coscom", Type.GetType("System.Double"))
    End Sub
    Private Sub frmEditarConvenios_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        PuntoControl.RegistroUso("158")
        StartPosition = FormStartPosition.Manual
        Location = New Point(0, 0)
        creaGrilla()
        grbAcciones.Enabled = False
        grbRut.Enabled = False

        If Globales.user = "ICADENA" Or Globales.user = "ALOPEZG" Or Globales.user = "KSERRANO" Then
            rbtReemplazar.Visible = True
            rbtAgregar.Visible = True
            cbxReversaReem.Visible = True
        End If

        If Globales.user = "ALOPEZG" Or Globales.user = "KSERRANO" Then
            rbtQuitar.Visible = True
        End If

        If Globales.detDerUsu(Globales.user, 142) = True Then
            btnProcesar.Enabled = True
            btnProcesarManual.Enabled = True
        End If
    End Sub
    Private Sub carganegociosrelacionados()
        Try
            bd.open_dimerc()

            Dim Sql = " select rutcli,getrazonsocial(codemp,rutcli) razons "
            Sql = Sql & " FROM re_emprela  "
            Sql = Sql & " where numrel in(select numrel from re_emprela where rutcli=" + txtRutcli.Text + " and codemp=3)  "
            Sql = Sql & " and rutcli not in(select rutcli from re_emprela where rutcli=" + txtRutcli.Text + " and codemp=3)and estado=2  "
            Sql = Sql & " order by rutcli"

        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub
    Private Sub txtCodpro_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCodpro.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Trim(txtCodpro.Text) <> "" Then
                txtCodpro.Text = Globales.BuscaCodigoDimerc(txtCodpro.Text)
                If Trim(txtCodpro.Text) = "" Then
                    MessageBox.Show("Producto No Existe", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtCodpro.Focus()
                    Exit Sub
                End If
                traeDatos()
            Else
                txtDescripcion.Focus()
            End If
        End If
    End Sub
    Private Sub traeDatos()
        Try
            bd.open_dimerc()
            Dim StrQuery = "select a.codpro, a.despro ,a.codlin, getlinea(a.codpro) linea, getcostopromedio(3,codpro) coscom "
            StrQuery = StrQuery & " from MA_PRODUCT a "
            StrQuery = StrQuery & " where a.codpro = '" & Trim(txtCodpro.Text) & "'"
            Dim dt = bd.sqlSelect(StrQuery)
            If dt.Rows.Count > 0 Then
                txtDescripcion.Text = dt.Rows(0).Item("despro")
                txtCodlinAgrega.Text = dt.Rows(0).Item("codlin")
                txtLinea.Text = dt.Rows(0).Item("linea")
                txtcoscom.Text = dt.Rows(0).Item("coscom")
                txtPrecio.Focus()
                txtPrecio.SelectAll()
            Else
                MessageBox.Show("Producto no Existe,verificar por favor.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            End If
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub
    Private Sub txtDescripcion_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDescripcion.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If txtDescripcion.Text.Trim <> "" Then
                Dim frmBuscar As New frmBuscarProducto(txtDescripcion.Text, False)
                frmBuscar.ShowDialog()
                If Not frmBuscar.dgvDatos.CurrentRow Is Nothing And frmBuscar.ok = True Then
                    txtDescripcion.Text = frmBuscar.dgvDatos.Item("Descripcion", frmBuscar.dgvDatos.CurrentRow.Index).Value.ToString
                    txtCodpro.Text = frmBuscar.dgvDatos.Item("Codigo", frmBuscar.dgvDatos.CurrentRow.Index).Value.ToString
                    traeDatos()
                    txtPrecio.Focus()
                    txtPrecio.SelectAll()
                End If
            End If
        End If
    End Sub
    Private Sub btn_AgregaProducto_Click(sender As Object, e As EventArgs) Handles btn_AgregaProducto.Click
        Try
            PuntoControl.RegistroUso("162")
            For i = 0 To dgvProductosaAgregar.Rows.Count - 1
                If dgvProductosaAgregar.Item("Codigo", i).Value = txtCodpro.Text Then
                    MessageBox.Show("Código ya Existe", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtCodpro.Focus()
                    Exit Sub
                End If
            Next
            If Trim(txtPrecio.Text) = "" Then
                txtPrecio.Text = 0
            End If
            If Trim(txtCosto.Text) = "" Then
                txtCosto.Text = 0
            End If

            dtGrilla.Rows.Add(txtCodpro.Text, txtDescripcion.Text, txtLinea.Text, Math.Round(Convert.ToDouble(txtPrecio.Text)), Math.Round(Convert.ToDouble(txtCosto.Text)), dtpFecvenAgrega.Value.Date.ToShortDateString, txtCodlinAgrega.Text, IIf(rdbTemporal.Checked = True, "TEMPORAL", "DEFINITIVO"), txtcoscom.Text)
            dgvProductosaAgregar.DataSource = dtGrilla

            formateagrilla()
            txtCodpro.Focus()
            LimpiaTodo()
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub formateagrilla()
        dgvProductosaAgregar.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvProductosaAgregar.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
        dgvProductosaAgregar.Columns("Precio").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvProductosaAgregar.Columns("Precio").DefaultCellStyle.Format = "n0"
        dgvProductosaAgregar.Columns("Costo Especial").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvProductosaAgregar.Columns("Costo Especial").DefaultCellStyle.Format = "n0"
        dgvProductosaAgregar.Columns("coscom").Visible = False
    End Sub
    Private Sub LimpiaTodo()
        txtDescripcion.Text = ""
        txtPrecio.Text = ""
        txtCosto.Text = ""
    End Sub

    Private Sub btn_QuitaAgrega_Click(sender As Object, e As EventArgs) Handles btn_QuitaAgrega.Click
        Try
            PuntoControl.RegistroUso("164")
            If dgvProductosaAgregar.CurrentRow Is Nothing Then
                MessageBox.Show("Seleccione una fila antes de eliminar", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            End If
            Dim indice = dgvProductosaAgregar.CurrentRow.Index

            dgvProductosaAgregar.DataSource = Nothing
            dtGrilla.Rows(indice).Delete()
            dgvProductosaAgregar.DataSource = dtGrilla
            formateagrilla()
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub txtPrecio_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPrecio.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If txtPrecio.Text <> "" Then
                txtCosto.Focus()
                txtCosto.SelectAll()
            End If
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtCosto_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCosto.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If txtCosto.Text <> "" Then
                dtpFecvenAgrega.Focus()
            End If
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub rdbCliente_CheckedChanged(sender As Object, e As EventArgs) Handles rdbCliente.CheckedChanged
        If rdbCliente.Checked = True Then
            grbRut.Enabled = True
        End If
    End Sub

    Private Sub rdbTodos_CheckedChanged(sender As Object, e As EventArgs) Handles rdbTodos.CheckedChanged
        If rdbTodos.Checked = True Then
            grbRut.Enabled = False
            grbAcciones.Enabled = True
        Else
            grbAcciones.Enabled = False
        End If
    End Sub

    Private Sub txtRutcli_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtRutcli.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Trim(txtRutcli.Text) <> "" Then
                PuntoControl.RegistroUso("159")
                buscaCliente()
            Else
                txtRazons.Focus()
            End If
        End If
    End Sub

    Private Sub txtRazons_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtRazons.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If txtRazons.Text.ToString.Trim <> "" Then

                Dim buscador As New frmBuscaCliente(txtRazons.Text)
                buscador.ShowDialog()
                If buscador.dgvDatos.CurrentRow Is Nothing Then
                    Exit Sub
                End If
                txtRutcli.Text = buscador.dgvDatos.Item("Rut", buscador.dgvDatos.CurrentRow.Index).Value.ToString
                txtRazons.Text = buscador.dgvDatos.Item("Razon Social", buscador.dgvDatos.CurrentRow.Index).Value.ToString
                buscaConvenio()

            Else
                If txtRutcli.Text = "" Then
                    txtRutcli.Focus()
                End If
            End If

        End If
    End Sub
    Private Sub buscaCliente()
        Try
            bd.open_dimerc()
            Dim StrQuery = "select razons nombre from en_cliente "
            StrQuery = StrQuery & " where rutcli = '" & txtRutcli.Text & "'"
            StrQuery = StrQuery & "   and codemp = 3"
            Dim OdnCliente = bd.sqlSelect(StrQuery)

            If Not OdnCliente.Rows.Count = 0 Then
                txtRazons.Text = CStr(OdnCliente.Rows(0).Item("nombre").ToString)
                buscaConvenio()
            Else
                MessageBox.Show("Cliente No Existe. Debe ser Ingresado Previamente.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtRutcli.Text = ""
                txtRutcli.Focus()
                'Exit Sub
            End If

        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub buscaProductoConvenio(tipo As Int64)
        Try
            bd.open_dimerc()
            If tipo = 1 Then
                Dim dt As New DataTable

                Dim sql = "select razons from en_cliente where rutcli = " & txtRutcliM.Text
                dt = bd.sqlSelect(sql)

                txtRsocialM.Text = dt.Rows(0).Item("razons").ToString

                sql = "select a.numcot, a.rutcli, a.fecemi, a.fecven, b.precio, b.costos, b.margen, b.codlin from en_conveni a, de_conveni b
                        where a.fecven > trunc(sysdate)
                          and a.numcot = b. numcot
                          and a.rutcli = " & txtRutcliM.Text.Trim & "
                          and b.codpro = '" & txtskuOriginalM.Text.Trim & "'"
                dt = bd.sqlSelect(sql)

                dgvConOriM.DataSource = dt

                txtFechaIniM.Text = dt.Rows(0).Item("fecemi").ToString
                txtFechaFinM.Text = dt.Rows(0).Item("fecven").ToString
                txtPrecioM.Text = dt.Rows(0).Item("precio").ToString
                txtCostoM.Text = dt.Rows(0).Item("costos").ToString
                txtMargenM.Text = dt.Rows(0).Item("margen").ToString

                sql = "select a.codpro, a.despro, b.deslin from ma_product a, ma_lineapr b
                    where a.codpro = '" & txtskuOriginalM.Text & "'
                      and a.codlin = b.codlin"
                dt = bd.sqlSelect(sql)

                txtDescripSkuM.Text = dt.Rows(0).Item("despro")
                txtLineaM.Text = dt.Rows(0).Item("deslin")

            ElseIf tipo = 2 Then
                Dim dt As New DataTable
                If cbxReversaReem.Checked = True Then
                    Dim sql = "select a.numcot, a.rutcli, a.fecemi, a.fecven, b.precio, b.costos, b.margen, b.codlin from en_conveni a, de_conveni b
                        where a.fecven > trunc(sysdate)
                          and a.numcot = b. numcot
                          and b.suc = '" & txtSkuOri.Text & "'
                          and b.codpro = '" & txtSkuReem.Text & "'"
                    dt = bd.sqlSelect(sql)

                    txtConvenioTotales.Text = dt.Rows.Count
                    dgvOriginal.DataSource = dt

                    sql = "select a.codpro, a.despro, b.deslin from ma_product a, ma_lineapr b
                    where a.codpro = '" & txtSkuOri.Text & "'
                      and a.codlin = b.codlin"
                    dt = bd.sqlSelect(sql)

                    txtDescripOri.Text = dt.Rows(0).Item("despro")
                    txtLineaOri.Text = dt.Rows(0).Item("deslin")
                Else
                    Dim sql = "select a.numcot, a.rutcli, a.fecemi, a.fecven, b.precio, b.costos, b.margen, b.codlin from en_conveni a, de_conveni b
                        where a.fecven > trunc(sysdate)
                          and a.numcot = b. numcot
                          and b.codpro = '" & txtSkuOri.Text & "'"
                    dt = bd.sqlSelect(sql)

                    txtConvenioTotales.Text = dt.Rows.Count
                    dgvOriginal.DataSource = dt

                    sql = "select a.codpro, a.despro, b.deslin from ma_product a, ma_lineapr b
                    where a.codpro = '" & txtSkuOri.Text & "'
                      and a.codlin = b.codlin"
                    dt = bd.sqlSelect(sql)

                    txtDescripOri.Text = dt.Rows(0).Item("despro")
                    txtLineaOri.Text = dt.Rows(0).Item("deslin")
                End If
            End If

        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub buscaProductoReemplazo(tipo As Int64)
        Try
            bd.open_dimerc()
            Dim dt As New DataTable

            If tipo = 1 Then
                Dim sql = "select a.codpro, a.despro, b.deslin, cosprom from ma_product a, ma_lineapr b
                    where a.codpro = '" & txtSkuRemM.Text & "'
                      and a.codlin = b.codlin"
                dt = bd.sqlSelect(sql)

                costo = dt.Rows(0).Item("cosprom")
                txtdescripReemM.Text = dt.Rows(0).Item("despro")
                txtLineaReemM.Text = dt.Rows(0).Item("deslin")

                If cbxReversaReem.Checked = True Then
                    sql = "select a.numcot, a.rutcli, a.fecemi, a.fecven, b.precio, b.costos, b.margen, b.codlin from en_conveni a, de_conveni b
                        where a.fecven > trunc(sysdate)
                          and a.numcot = b. numcot
                          and b.codpro = '" & txtskuOriginalM.Text & "'
                          and b.suc = '" & txtSkuRemM.Text & "'"
                    dt = bd.sqlSelect(sql)

                    txtConvenioReem.Text = dt.Rows.Count
                    dgvConReemM.DataSource = dt
                Else
                    sql = "select a.numcot, a.rutcli, a.fecemi, a.fecven, b.precio, b.costos, b.margen, b.codlin from en_conveni a, de_conveni b
                        where a.fecven > trunc(sysdate)
                          and a.numcot = b. numcot
                          and b.codpro = '" & txtskuOriginalM.Text & "'
                          and not exists (select * from de_conveni x where x.numcot = b.numcot and x.codpro = '" & txtSkuRemM.Text & "')"
                    dt = bd.sqlSelect(sql)

                    txtConvenioReem.Text = dt.Rows.Count
                    dgvConReemM.DataSource = dt

                End If
            ElseIf tipo = 2 Then
                Dim sql = "select a.codpro, a.despro, b.deslin, cosprom from ma_product a, ma_lineapr b
                    where a.codpro = '" & txtSkuReem.Text & "'
                      and a.codlin = b.codlin"
                dt = bd.sqlSelect(sql)

                costo = dt.Rows(0).Item("cosprom")
                txtDescripReem.Text = dt.Rows(0).Item("despro")
                txtLineaReem.Text = dt.Rows(0).Item("deslin")

                'If cbxReversaReem.Checked = True Then
                '    sql = "select a.numcot, a.rutcli, a.fecemi, a.fecven, b.precio, b.costos, b.margen, b.codlin from en_conveni a, de_conveni b
                '        where a.fecven > trunc(sysdate)
                '          and a.numcot = b. numcot
                '          and b.codpro = '" & txtSkuOri.Text & "'
                '          and b.suc = '" & txtSkuReem.Text & "'"
                '    dt = bd.sqlSelect(sql)

                '    txtConvenioReem.Text = dt.Rows.Count
                '    dgvReemplazo.DataSource = dt
                'Else
                sql = "select a.numcot, a.rutcli, a.fecemi, a.fecven, b.precio, b.costos, b.margen, b.codlin from en_conveni a, de_conveni b 
                        where a.fecven > trunc(sysdate)
                          and a.numcot = b. numcot
                          and b.codpro = '" & txtSkuOri.Text & "' 
                          and not exists (select * from de_conveni x where x.numcot = b.numcot and x.codpro = '" & txtSkuReem.Text & "')"
                dt = bd.sqlSelect(sql)

                    txtConvenioReem.Text = dt.Rows.Count
                    dgvReemplazo.DataSource = dt
                'End If
            End If

        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub buscaConvenio()
        Try
            bd.open_dimerc()
            dgvProductosaAgregar.DataSource = Nothing
            Dim Sql = " select a.numcot,nvl(b.numrel,'') numrel, nvl(ticonv, 'M') ticonv from en_conveni a,re_emprela b "
            Sql = Sql & " where a.codemp=3  "
            Sql = Sql & " and a.rutcli= " + txtRutcli.Text
            Sql = Sql & " and sysdate between a.fecemi-1 and a.fecven+1 "
            Sql = Sql & " and a.codemp=b.codemp(+) "
            Sql = Sql & " And a.rutcli = b.rutcli(+)  "
            Dim dt = bd.sqlSelect(Sql)
            If dt.Rows.Count = 0 Then
                MessageBox.Show("No existe un convenio vigente para este cliente", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                limpiar()
                Exit Sub
            End If

            If dt.Rows(0).Item("ticonv") = "M" Then
                cmbTipoConvenio.SelectedIndex = 1
            ElseIf dt.Rows(0).Item("ticonv") = "T" Then
                cmbTipoConvenio.SelectedIndex = 0
            End If

            convenio = dt.Rows(0).Item("numcot").ToString

            If dt.Rows.Count = 0 Then
                MessageBox.Show("No existe un convenio vigente para este cliente", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                limpiar()
                Exit Sub
            Else
                txtRelCom.Text = dt.Rows(0).Item("numrel").ToString
                grbAcciones.Enabled = True
                buscaDetalleConvenio(dt.Rows(0).Item("numcot").ToString)
            End If

        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub
    Private Sub limpiar()
        dgvProductoEliminar.DataSource = Nothing
        dgvProductosaAgregar.DataSource = Nothing
        dgvProductosModificar.DataSource = Nothing
        dtGrilla.Rows.Clear()
        txtCostoModif.Text = ""
        txtPrecioModif.Text = ""
        txtCodproModif.Text = ""
        txtDescripcionModif.Text = ""
        txtCosto.Text = ""
        txtPrecio.Text = ""
        txtDescripcion.Text = ""
        txtCodpro.Text = ""
        txtRelCom.Text = ""
        txtRazons.Text = ""
        txtRutcli.Text = ""
        txtRutcli.SelectAll()

        rdbCliente.Checked = True
        grbAcciones.Enabled = False

    End Sub
    Private Sub buscaDetalleConvenio(numcot As String)
        Try
            bd.open_dimerc()
            Dim Sql = " Select codpro ""Codigo"",getdescripcion(codpro) ""Descripcion"", "
            Sql = Sql & " getlinea(codpro) ""Linea"",precio ""Precio"",costos ""Costo Especial"", "
            Sql = Sql & " to_date(fecemi,'dd/mm/yyyy') ""Inicia"",to_date(fecven,'dd/mm/yyyy') ""Termina"",to_date(fecven,'dd/mm/yyyy') ""FinOriginal"", "
            Sql = Sql & " 0 cambio,getcostopromedio(3,codpro) cosprom "
            Sql = Sql & "from de_conveni where "
            Sql = Sql & " numcot= " + numcot
            Dim dt = bd.sqlSelect(Sql)
            Dim dtelimina = dt.Copy
            Dim dtFechas = dt.Copy
            dgvProductosModificar.DataSource = dt
            dgvProductoEliminar.DataSource = dtelimina
            dgvCambiaFechas.DataSource = dtFechas
            formatoGrillaFechas()
            formatoGrillaModifica()
            formatoGrillaElimina()
            flagGrillaModificar = True
            Sql = " select getlinea(codpro) linea,max(to_date(fecven,'dd/mm/yyyy')) fecven  "
            Sql = Sql & "from de_conveni where numcot= " + numcot
            Sql = Sql & "group by getlinea(codpro) "
            Dim dt2 = bd.sqlSelect(Sql)
            dgvLineasReajuste.DataSource = dt2
            dgvLineasReajuste.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub
    Private Sub formatoGrillaElimina()
        If flagemprel = False Then
            Dim check, check2 As New DataGridViewCheckBoxColumn
            check.HeaderText = "Sel"
            check.Name = "Sel"
            check2.HeaderText = "Sel"
            check2.Name = "Sel"
            dgvProductoEliminar.Columns.Insert(0, check)
            dgvProductoEliminar.Columns(0).Visible = True
            dgvLineasReajuste.Columns.Insert(0, check2)
            dgvLineasReajuste.Columns(0).Visible = True
            flagemprel = True
        End If
        dgvProductoEliminar.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
        dgvProductoEliminar.Columns("Precio").DefaultCellStyle.Format = "n0"
        dgvProductoEliminar.Columns("Precio").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvProductoEliminar.Columns("Costo Especial").DefaultCellStyle.Format = "n0"
        dgvProductoEliminar.Columns("Costo Especial").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvProductoEliminar.Columns("cambio").Visible = False
        dgvProductoEliminar.Columns("cosprom").Visible = False
        dgvProductoEliminar.Columns("FinOriginal").Visible = False
    End Sub
    Private Sub formatoGrillaModifica()
        dgvProductosModificar.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
        dgvProductosModificar.Columns("Precio").DefaultCellStyle.Format = "n0"
        dgvProductosModificar.Columns("Precio").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvProductosModificar.Columns("Costo Especial").DefaultCellStyle.Format = "n0"
        dgvProductosModificar.Columns("Costo Especial").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvProductosModificar.Columns("cambio").Visible = False
        dgvProductosModificar.Columns("cosprom").Visible = False
        dgvProductosModificar.Columns("FinOriginal").Visible = False
    End Sub
    Private Sub formatoGrillaFechas()
        dgvCambiaFechas.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
        dgvCambiaFechas.Columns("Precio").DefaultCellStyle.Format = "n0"
        dgvCambiaFechas.Columns("Precio").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvCambiaFechas.Columns("Costo Especial").DefaultCellStyle.Format = "n0"
        dgvCambiaFechas.Columns("Costo Especial").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        'dgvCambiaFechas.Columns("cambio").Visible = False
        dgvCambiaFechas.Columns("cosprom").Visible = False
        dgvCambiaFechas.Columns("FinOriginal").Visible = False
    End Sub
    Private Sub dgvProductosModificar_CurrentCellChanged(sender As Object, e As EventArgs) Handles dgvProductosModificar.CurrentCellChanged
        If flagGrillaModificar = True And Not dgvProductosModificar.CurrentRow Is Nothing Then
            txtCodproModif.Text = dgvProductosModificar.Item("Codigo", dgvProductosModificar.CurrentRow.Index).Value.ToString
            txtDescripcionModif.Text = dgvProductosModificar.Item("Descripcion", dgvProductosModificar.CurrentRow.Index).Value.ToString
            txtPrecioModif.Text = dgvProductosModificar.Item("Precio", dgvProductosModificar.CurrentRow.Index).Value.ToString
            txtCostoModif.Text = dgvProductosModificar.Item("Costo Especial", dgvProductosModificar.CurrentRow.Index).Value.ToString
            indice = dgvProductosModificar.CurrentRow.Index
        End If
    End Sub

    Private Sub txtCodproModif_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDescripcionModif.KeyPress, txtCodproModif.KeyPress
        e.Handled = True
    End Sub

    Private Sub dgvProductosModificar_KeyDown(sender As Object, e As KeyEventArgs) Handles dgvProductosModificar.KeyDown
        If e.KeyValue = Keys.Enter Then
            txtPrecioModif.Focus()
            txtPrecioModif.SelectAll()
            indice = dgvProductosModificar.CurrentRow.Index
            e.Handled = True
        End If
    End Sub

    Private Sub btn_Cambio_Click(sender As Object, e As EventArgs) Handles btn_Cambio.Click
        If txtCostoModif.Text = "" Or txtCostoModif.Text = "0" Then
            MessageBox.Show("Costo no puede ser vacio o tener valor 0,revisar antes de guardar", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        ElseIf txtPrecioModif.Text = "" Or txtPrecioModif.Text = "0" Then
            MessageBox.Show("Costo no puede ser vacio o tener valor 0,revisar antes de guardar", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        dgvProductosModificar.Item("precio", indice).Value = txtPrecioModif.Text
        dgvProductosModificar.Item("costo Especial", indice).Value = txtCostoModif.Text
        dgvProductosModificar.Item("cambio", indice).Value = 1
        PuntoControl.RegistroUso("166")

        formatoGrillaModifica()
    End Sub

    Private Sub txtPrecioModif_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPrecioModif.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If txtPrecioModif.Text <> "" Then
                txtCostoModif.Focus()
                txtCostoModif.SelectAll()
            End If
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtCostoModif_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCostoModif.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If txtCostoModif.Text <> "" Then
                btn_Cambio.Focus()
            End If
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub dgvProductoEliminar_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvProductoEliminar.CellContentClick
        Try
            If e.RowIndex <> -1 Then
                If e.ColumnIndex = 0 Then
                    If dgvProductoEliminar.Item("SEL", e.RowIndex).Value = 0 Then
                        dgvProductoEliminar.Item("SEL", e.RowIndex).Value = 1
                    Else
                        dgvProductoEliminar.Item("SEL", e.RowIndex).Value = 0
                    End If
                End If
            End If
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub btn_AgregaConvenio_Click(sender As Object, e As EventArgs) Handles btn_AgregaConvenio.Click
        PuntoControl.RegistroUso("162")
        If dgvProductosaAgregar.RowCount = 0 Then
            MessageBox.Show("No hay productos para agregar", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        If rdbCliente.Checked = True Then
            agregarProductoCliente()
            dtGrilla.Rows.Clear()
        Else
            'agregarProductoTodos
        End If
    End Sub
    Private Sub agregarProductoCliente()
        Using conn = New OracleConnection(Conexion.retornaConexion)
            conn.Open()
            Using transaccion = conn.BeginTransaction
                Dim sql As String
                Cursor.Current = Cursors.WaitCursor
                Dim comando As OracleCommand
                Dim dataAdapter As OracleDataAdapter
                Dim dtClientes, dt As New DataTable
                Try
                    sql = "Set Role Rol_Aplicacion"
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()
                    'REPASA COSTOS
                    For i = 0 To dgvProductosaAgregar.RowCount - 1
                        If dgvProductosaAgregar.Item("Costo Especial", i).Value = "0" Then
                            dt.Clear()
                            sql = " select getcostocomercial(3,'" + dgvProductosaAgregar.Item("Codigo", i).Value + "') costo from dual"
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            dataAdapter = New OracleDataAdapter(comando)
                            dataAdapter.Fill(dt)
                            If dt.Rows.Count > 0 Then
                                dgvProductosaAgregar.Item("Costo Especial", i).Value = dt.Rows(0).Item("costo").ToString
                            End If
                        End If
                    Next
                    'DETERMINAR CANTIDAD DE CLIENTES, SI NO TIENE RELACION COMERCIAL CARGO SOLO EL CLIENTE CONSULTADO
                    If txtRelCom.Text = "" Then
                        sql = " select rutcli from en_cliente where codemp=3 and rutcli= " + txtRutcli.Text
                        comando = New OracleCommand(sql, conn)
                        comando.Transaction = transaccion
                        dataAdapter = New OracleDataAdapter(comando)
                        dataAdapter.Fill(dtClientes)
                    Else
                        sql = " select distinct rutcli  "
                        sql = sql & " FROM re_emprela   "
                        sql = sql & " where numrel in(" + txtRelCom.Text + ")   "
                        'sql = sql & " and estado=2   "
                        sql = sql & " order by rutcli "
                        comando = New OracleCommand(sql, conn)
                        comando.Transaction = transaccion
                        dataAdapter = New OracleDataAdapter(comando)
                        dataAdapter.Fill(dtClientes)
                    End If
                    For i = 0 To dtClientes.Rows.Count - 1
                        'POR CADA CLIENTE REVISAR SI TIENE CONVENIO ACTIVO
                        sql = " select numcot,fecemi,fecven from en_conveni  "
                        sql = sql & "where codemp=3  "
                        sql = sql & "and sysdate between fecemi and fecven+1 "
                        sql = sql & "and rutcli= " + dtClientes.Rows(i).Item("rutcli").ToString
                        Dim dtConvenio = bd.sqlSelect(sql)
                        If dtConvenio.Rows.Count > 0 Then
                            Dim fechaVencimiento = dtConvenio.Rows(0).Item("fecven")

                            'SACAR ULTIMO NUMERO DE SECUENCIA PARA PODER AGREGAR
                            sql = " select max(sequen)+1 numero from de_conveni where numcot= " + dtConvenio.Rows(0).Item("numcot").ToString
                            Dim secuencia As String
                            secuencia = bd.sqlSelectUnico(sql, "numero")

                            If secuencia = "" Then
                                secuencia = 1
                            End If

                            For x = 0 To dgvProductosaAgregar.RowCount - 1
                                'PREGUNTAR POR SI EL PRODUCTO YA ESTA EN EL CONVENIO, SI NO EXISTE SE AGREGA

                                sql = " select * from de_conveni where codemp=3  "
                                sql = sql & "and numcot = " + dtConvenio.Rows(0).Item("numcot").ToString
                                sql = sql & "and codpro = '" + dgvProductosaAgregar.Item("Codigo", x).Value.ToString + "'"
                                Dim dtValida As New DataTable
                                comando = New OracleCommand(sql, conn)
                                comando.Transaction = transaccion
                                dataAdapter = New OracleDataAdapter(comando)
                                dataAdapter.Fill(dtValida)

                                If dtValida.Rows.Count = 0 Then

                                    sql = " insert into de_cotizac(numcot,sequen,codpro,cantid,precio,costos,totnet,pordes, "
                                    sql += "mondes,margen,codlis,codlin,codemp,fecemi,fecven) values("
                                    sql += dtConvenio.Rows(0).Item("numcot").ToString + ","
                                    sql += secuencia + ","
                                    sql += "'" + dgvProductosaAgregar.Item("Codigo", x).Value.ToString + "',"
                                    sql += "1,"
                                    sql += dgvProductosaAgregar.Item("Precio", x).Value.ToString + ","
                                    sql += dgvProductosaAgregar.Item("Costo Especial", x).Value.ToString + ","
                                    sql += dgvProductosaAgregar.Item("Precio", x).Value.ToString + ","
                                    sql += "0,0,"
                                    sql += Math.Round((((dgvProductosaAgregar.Item("Precio", x).Value - dgvProductosaAgregar.Item("Costo Especial", x).Value) / dgvProductosaAgregar.Item("Precio", x).Value) * 100), 2).ToString.Replace(",", ".") + ","
                                    sql += "0," + dgvProductosaAgregar.Item("codlin", x).Value.ToString + ",3,"
                                    sql += "to_date('" + Now.Date + "','dd/mm/yyyy'),"
                                    sql += "to_date('" + dgvProductosaAgregar.Item("Fecven", x).Value + "','dd/mm/yyyy')"
                                    sql += ") "
                                    comando = New OracleCommand(sql, conn)
                                    comando.Transaction = transaccion
                                    comando.ExecuteNonQuery()

                                    sql = " insert into de_conveni(numcot,sequen,codpro,cantid,precio,costos,totnet,pordes, "
                                    sql += "mondes,margen,codlis,codlin,codemp,fecemi,fecven) values("
                                    sql += dtConvenio.Rows(0).Item("numcot").ToString + ","
                                    sql += secuencia + ","
                                    sql += "'" + dgvProductosaAgregar.Item("Codigo", x).Value.ToString + "',"
                                    sql += "1,"
                                    sql += dgvProductosaAgregar.Item("Precio", x).Value.ToString + ","
                                    sql += dgvProductosaAgregar.Item("Costo Especial", x).Value.ToString + ","
                                    sql += dgvProductosaAgregar.Item("Precio", x).Value.ToString + ","
                                    sql += "0,0,"
                                    sql += Math.Round((((dgvProductosaAgregar.Item("Precio", x).Value - dgvProductosaAgregar.Item("Costo Especial", x).Value) / dgvProductosaAgregar.Item("Precio", x).Value) * 100), 2).ToString.Replace(",", ".") + ","
                                    sql += "0," + dgvProductosaAgregar.Item("codlin", x).Value.ToString + ",3,"
                                    sql += "to_date('" + Now.Date + "','dd/mm/yyyy'),"
                                    sql += "to_date('" + dgvProductosaAgregar.Item("Fecven", x).Value + "','dd/mm/yyyy')"
                                    sql += ") "
                                    comando = New OracleCommand(sql, conn)
                                    comando.Transaction = transaccion
                                    comando.ExecuteNonQuery()
                                    If dgvProductosaAgregar.Item("Tipo", x).Value = "TEMPORAL" Then
                                        sql = " insert into producto_temporal_convenio(numcot,rutcli,codpro,cantid,precio,costo, "
                                        sql += "margen,codlin,codemp,fecemi,fecven) values("
                                        sql += dtConvenio.Rows(0).Item("numcot").ToString + ","
                                        sql += txtRutcli.Text + ","
                                        sql += "'" + dgvProductosaAgregar.Item("Codigo", x).Value.ToString + "',"
                                        sql += "1,"
                                        sql += dgvProductosaAgregar.Item("Precio", x).Value.ToString + ","
                                        sql += dgvProductosaAgregar.Item("Costo Especial", x).Value.ToString + ","
                                        sql += Math.Round((((dgvProductosaAgregar.Item("Precio", x).Value - dgvProductosaAgregar.Item("Costo Especial", x).Value) / dgvProductosaAgregar.Item("Precio", x).Value) * 100), 2).ToString.Replace(",", ".") + ","
                                        sql += dgvProductosaAgregar.Item("codlin", x).Value.ToString + ",3,"
                                        sql += "to_date('" + Now.Date + "','dd/mm/yyyy'),"
                                        sql += "to_date('" + dgvProductosaAgregar.Item("Fecven", x).Value + "','dd/mm/yyyy')"
                                        sql += ") "
                                        comando = New OracleCommand(sql, conn)
                                        comando.Transaction = transaccion
                                        comando.ExecuteNonQuery()
                                    End If

                                    'ACTUALIZA LA FECHA SI EL PRODUCTO VA MAS ALLA

                                    'REVISA SI TIENE COSTO ESPECIAL PARA AGREGAR O MODIFICAR DE SER NECESARIO
                                    Dim dtCostoEspecial As New DataTable

                                    Dim sequen As String

                                    sql = " Select EN_CLIENTE_COSTO_SEQ.nextval seq from dual "
                                    dt.Clear()
                                    comando = New OracleCommand(sql, conn)
                                    comando.Transaction = transaccion
                                    dataAdapter = New OracleDataAdapter(comando)
                                    dataAdapter.Fill(dt)
                                    If dt.Rows.Count = 0 Then
                                        If Not transaccion.Equals(Nothing) Then
                                            transaccion.Rollback()
                                        End If
                                        MessageBox.Show("NO se encontró una secuencia. Consulte a Sistemas...", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                        Exit Sub
                                    Else
                                        sequen = dt.Rows(0).Item("seq").ToString
                                    End If
                                    sql = " SELECT B.codpro,b.rutcli FROM en_cliente_costo B "
                                    sql = sql & "WHERE B.codpro= '" & dgvProductosaAgregar.Item("Codigo", x).Value.ToString & "'"
                                    sql = sql & "  and B.rutcli= " & dtClientes.Rows(i).Item("rutcli").ToString
                                    sql = sql & "  and B.codemp= 3"
                                    sql = sql & "  and ( "
                                    sql = sql & "        (to_date('" & dtConvenio.Rows(0).Item("fecemi") & "','DD/MM/YYYY') between B.FECINI AND B.FECFIN) "
                                    sql = sql & "     or (to_date('" & dgvProductosaAgregar.Item("Fecven", x).Value & "','DD/MM/YYYY') between B.FECINI AND B.FECFIN) "
                                    sql = sql & "     or (B.FECINI between to_date('" & dtConvenio.Rows(0).Item("fecemi") & "','DD/MM/YYYY') and to_date('" & dgvProductosaAgregar.Item("Fecven", x).Value & "','DD/MM/YYYY')) "
                                    sql = sql & "     or (B.FECFIN between to_date('" & dtConvenio.Rows(0).Item("fecemi") & "','DD/MM/YYYY') and to_date('" & dgvProductosaAgregar.Item("Fecven", x).Value & "','DD/MM/YYYY')) "
                                    sql = sql & "      ) "
                                    comando = New OracleCommand(sql, conn)
                                    comando.Transaction = transaccion
                                    dataAdapter = New OracleDataAdapter(comando)
                                    dataAdapter.Fill(dtCostoEspecial)
                                    If dtCostoEspecial.Rows.Count = 0 Then

                                        sql = " INSERT INTO en_cliente_costo (ID_NUMERO, RUTCLI, CODPRO, PRECIO, COSTO, MARGEN, FECINI, FECFIN, CANTID, USR_CREAC, CODEMP, TIPO,CODMON,ESTADO) VALUES( "
                                        sql = sql & sequen & ", "
                                        sql = sql & dtClientes.Rows(i).Item("rutcli").ToString & " , "
                                        sql = sql & "'" & dgvProductosaAgregar.Item("Codigo", x).Value.ToString & "',"
                                        sql = sql & dgvProductosaAgregar.Item("Precio", x).Value.ToString & ","
                                        sql = sql & dgvProductosaAgregar.Item("Costo Especial", x).Value.ToString & ", "
                                        sql = sql & Math.Round((((dgvProductosaAgregar.Item("Precio", x).Value - dgvProductosaAgregar.Item("Costo Especial", x).Value) / dgvProductosaAgregar.Item("Precio", x).Value) * 100), 2).ToString.Replace(",", ".") & ", "
                                        sql = sql & "TO_DATE('" & Now.Date & "','dd/mm/yyyy'), "
                                        sql = sql & "TO_DATE('" & dgvProductosaAgregar.Item("Fecven", x).Value & "','dd/mm/yyyy'), "
                                        sql = sql & "1,'"
                                        sql = sql & Globales.user & "',"
                                        sql = sql & "3,"
                                        If dgvProductosaAgregar.Item("coscom", x).Value > dgvProductosaAgregar.Item("Costo Especial", x).Value Then
                                            sql = sql & "0, "
                                        Else
                                            sql = sql & "2, "
                                        End If

                                        sql = sql & "0,'V')"
                                        comando = New OracleCommand(sql, conn)
                                        comando.Transaction = transaccion
                                        comando.ExecuteNonQuery()

                                    Else
                                        sql = " update en_cliente_costo set  "
                                        sql = sql & " fecfin=TO_DATE('" + Now.Date.AddDays(-1).Date + "','dd/mm/yyyy'), "
                                        sql = sql & " estado='C' "
                                        sql = sql & " where codemp=3 and (id_numero,rutcli,codpro) in( "
                                        sql = sql & " SELECT b.id_numero,b.rutcli,b.codpro FROM en_cliente_costo B  "
                                        sql = sql & " WHERE  B.rutcli= " + dtClientes.Rows(i).Item("rutcli").ToString
                                        sql = sql & "  and B.codemp= 3 "
                                        sql = sql & "  and b.codpro='" + dgvProductosaAgregar.Item("Codigo", x).Value.ToString + "' "
                                        sql = sql & "  and estado='V' "
                                        sql = sql & "  and ((to_date('" + Now.Date + "','DD/MM/YYYY') between B.FECINI AND B.FECFIN)  "
                                        sql = sql & "     or (to_date('" + dgvProductosaAgregar.Item("Fecven", x).Value + "','DD/MM/YYYY') between B.FECINI AND B.FECFIN)  "
                                        sql = sql & "     or (B.FECINI between to_date('" + Now.Date + "','DD/MM/YYYY') and to_date('" + dgvProductosaAgregar.Item("Fecven", x).Value + "','DD/MM/YYYY'))  "
                                        sql = sql & "     or (B.FECFIN between to_date('" + Now.Date + "','DD/MM/YYYY') and to_date('" + dgvProductosaAgregar.Item("Fecven", x).Value + "','DD/MM/YYYY'))  "
                                        sql = sql & "      )) "
                                        comando = New OracleCommand(sql, conn)
                                        comando.Transaction = transaccion
                                        comando.ExecuteNonQuery()

                                        sql = " INSERT INTO en_cliente_costo (ID_NUMERO, RUTCLI, CODPRO, PRECIO, COSTO, MARGEN, FECINI, FECFIN, CANTID, USR_CREAC, CODEMP, TIPO,CODMON,ESTADO) VALUES( "
                                        sql = sql & sequen & ", "
                                        sql = sql & dtClientes.Rows(i).Item("rutcli").ToString & " , "
                                        sql = sql & "'" & dgvProductosaAgregar.Item("Codigo", x).Value.ToString & "',"
                                        sql = sql & dgvProductosaAgregar.Item("Precio", x).Value.ToString & ","
                                        sql = sql & dgvProductosaAgregar.Item("Costo Especial", x).Value.ToString & ", "
                                        sql = sql & Math.Round((((dgvProductosaAgregar.Item("Precio", x).Value - dgvProductosaAgregar.Item("Costo Especial", x).Value) / dgvProductosaAgregar.Item("Precio", x).Value) * 100), 2).ToString.Replace(",", ".") & ", "
                                        sql = sql & "TO_DATE('" & Now.Date & "','dd/mm/yyyy'), "
                                        sql = sql & "TO_DATE('" & dgvProductosaAgregar.Item("Fecven", x).Value & "','dd/mm/yyyy'), "
                                        sql = sql & "1,'"
                                        sql = sql & Globales.user & "',"
                                        sql = sql & "3,"
                                        If dgvProductosaAgregar.Item("coscom", x).Value > dgvProductosaAgregar.Item("Costo Especial", x).Value Then
                                            sql = sql & "0, "
                                        Else
                                            sql = sql & "2, "
                                        End If
                                        sql = sql & "0,'V')"
                                        comando = New OracleCommand(sql, conn)
                                        comando.Transaction = transaccion
                                        comando.ExecuteNonQuery()
                                    End If

                                    secuencia += 1
                                Else
                                    'AVISA QUE PRODUCTO YA ESTA AGREGADO
                                    'dgvProductosaAgregar.Item("Descripcion", x).Value = "PRODUCTO YA ESTA EN EL CONVENIO, NO SE AGREGO"
                                    'dgvProductosaAgregar.Item("Linea", x).Value = ""
                                    'dgvProductosaAgregar.Item("Precio", x).Value = 0
                                    'dgvProductosaAgregar.Item("Costo Especial", x).Value = 0
                                End If
                            Next

                            'LUEGO DE AGREGAR PRODUCTOS CORREGIR ENCABEZADO
                            sql = " select sum(cantid*precio) totnet, "
                            sql = sql & "round((sum(cantid*precio)*1.19) - sum(cantid*precio)) totiva, "
                            sql = sql & "round(sum(cantid*precio)*1.19) totgen, "
                            sql = sql & "round(avg(margen),2) margen "
                            sql = sql & "from de_cotizac where codemp=3 "
                            sql = sql & "and numcot= " + dtConvenio.Rows(0).Item("numcot").ToString

                            Dim valores = bd.sqlSelect(sql)

                            If valores.Rows(0).Item("totnet").ToString <> "" Then
                                sql = " update en_cotizac set "
                                sql = sql & " subtot=" + valores.Rows(0).Item("totnet").ToString + ", "
                                sql = sql & " totnet=" + valores.Rows(0).Item("totnet").ToString + ", "
                                sql = sql & " totiva=" + valores.Rows(0).Item("totiva").ToString + ", "
                                sql = sql & " totgen=" + valores.Rows(0).Item("totgen").ToString + ", "
                                sql = sql & " margen=" + valores.Rows(0).Item("margen").ToString.Replace(",", ".") + " "
                                sql = sql & " where codemp=3 "
                                sql = sql & " and numcot= " + dtConvenio.Rows(0).Item("numcot").ToString
                                sql = sql & " and rutcli= " + dtClientes.Rows(i).Item("rutcli").ToString
                                comando = New OracleCommand(sql, conn)
                                comando.Transaction = transaccion
                                comando.ExecuteNonQuery()

                                sql = " update en_conveni set "
                                sql = sql & " subtot=" + valores.Rows(0).Item("totnet").ToString + ", "
                                sql = sql & " totnet=" + valores.Rows(0).Item("totnet").ToString + ", "
                                sql = sql & " totiva=" + valores.Rows(0).Item("totiva").ToString + ", "
                                sql = sql & " totgen=" + valores.Rows(0).Item("totgen").ToString + ", "
                                sql = sql & " margen=" + valores.Rows(0).Item("margen").ToString.Replace(",", ".") + " "
                                sql = sql & " where codemp=3 "
                                sql = sql & " and numcot= " + dtConvenio.Rows(0).Item("numcot").ToString
                                sql = sql & " and rutcli= " + dtClientes.Rows(i).Item("rutcli").ToString
                                comando = New OracleCommand(sql, conn)
                                comando.Transaction = transaccion
                                comando.ExecuteNonQuery()
                            End If
                        Else
                            Continue For
                        End If
                    Next

                    transaccion.Commit()

                    MessageBox.Show("Producto agregado con éxito", "Finalizado", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    buscaConvenio()
                Catch ex As Exception
                    If Not transaccion.Equals(Nothing) Then
                        transaccion.Rollback()
                    End If
                    PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                    MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Finally
                    conn.Close()
                End Try
            End Using
        End Using
    End Sub

    Private Sub dtpFecvenAgrega_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFecvenAgrega.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            btn_AgregaProducto.Focus()
        End If
    End Sub

    Private Sub btn_ModificarConvenio_Click(sender As Object, e As EventArgs) Handles btn_ModificarConvenio.Click
        Using conn = New OracleConnection(Conexion.retornaConexion)
            conn.Open()
            Using transaccion = conn.BeginTransaction
                PuntoControl.RegistroUso("167")
                Dim sql As String
                Cursor.Current = Cursors.WaitCursor
                Dim comando As OracleCommand
                Dim dataAdapter As OracleDataAdapter
                Dim dtClientes, dt As New DataTable
                Try
                    sql = "Set Role Rol_Aplicacion"
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    If txtRelCom.Text = "" Then
                        sql = " select rutcli from en_cliente where codemp=3 and rutcli= " + txtRutcli.Text
                        comando = New OracleCommand(sql, conn)
                        comando.Transaction = transaccion
                        dataAdapter = New OracleDataAdapter(comando)
                        dataAdapter.Fill(dtClientes)
                    Else
                        sql = " select distinct rutcli  "
                        sql = sql & " FROM re_emprela   "
                        sql = sql & " where numrel in(" + txtRelCom.Text + ")   "
                        'sql = sql & " and estado=2   "
                        sql = sql & " order by rutcli "
                        comando = New OracleCommand(sql, conn)
                        comando.Transaction = transaccion
                        dataAdapter = New OracleDataAdapter(comando)
                        dataAdapter.Fill(dtClientes)
                    End If
                    For i = 0 To dtClientes.Rows.Count - 1
                        'POR CADA CLIENTE REVISAR SI TIENE CONVENIO ACTIVO
                        sql = " select numcot,fecemi from en_conveni  "
                        sql = sql & "where codemp=3  "
                        sql = sql & "and sysdate between fecemi and fecven+1 "
                        sql = sql & "and rutcli= " + dtClientes.Rows(i).Item("rutcli").ToString
                        Dim dtConvenio = bd.sqlSelect(sql)
                        If dtConvenio.Rows.Count > 0 Then

                            For x = 0 To dgvProductosModificar.RowCount - 1
                                If dgvProductosModificar.Item("cambio", x).Value = 1 Then
                                    sql = " select * from de_conveni where codemp=3  "
                                    sql = sql & "and numcot = " + dtConvenio.Rows(0).Item("numcot").ToString
                                    sql = sql & "and codpro = '" + dgvProductosModificar.Item("Codigo", x).Value.ToString + "'"
                                    Dim dtValida = bd.sqlSelect(sql)

                                    If dtValida.Rows.Count > 0 Then
                                        sql = " update de_cotizac  "
                                        sql = sql & " set precio=" + dgvProductosModificar.Item("Precio", x).Value.ToString + ", "
                                        If dgvProductosModificar.Item("Costo Especial", x).Value > 0 Then
                                            sql = sql & " costos = " + dgvProductosModificar.Item("Costo Especial", x).Value.ToString + ", "
                                        End If
                                        sql = sql & " totnet= cantid * " + dgvProductosModificar.Item("Precio", x).Value.ToString
                                        sql = sql & " where codemp=3  "
                                        sql = sql & " and numcot= " + dtConvenio.Rows(0).Item("numcot").ToString
                                        sql = sql & " and codpro='" + dgvProductosModificar.Item("Codigo", x).Value.ToString + "' "
                                        comando = New OracleCommand(sql, conn)
                                        comando.Transaction = transaccion
                                        comando.ExecuteNonQuery()

                                        sql = " update de_conveni  "
                                        sql = sql & " set precio=" + dgvProductosModificar.Item("Precio", x).Value.ToString + ", "
                                        If dgvProductosModificar.Item("Costo Especial", x).Value > 0 Then
                                            sql = sql & " costos = " + dgvProductosModificar.Item("Costo Especial", x).Value.ToString + ", "
                                        End If
                                        sql = sql & " totnet= cantid * " + dgvProductosModificar.Item("Precio", x).Value.ToString
                                        sql = sql & " where codemp=3  "
                                        sql = sql & " and numcot= " + dtConvenio.Rows(0).Item("numcot").ToString
                                        sql = sql & " and codpro='" + dgvProductosModificar.Item("Codigo", x).Value.ToString + "' "
                                        comando = New OracleCommand(sql, conn)
                                        comando.Transaction = transaccion
                                        comando.ExecuteNonQuery()

                                        'If dgvProductosModificar.Item("Costo Especial", x).Value > 0 Then
                                        'REVISA SI TIENE COSTO ESPECIAL PARA AGREGAR O MODIFICAR DE SER NECESARIO
                                        Dim dtCostoEspecial As New DataTable

                                        Dim sequen As String

                                        sql = " Select EN_CLIENTE_COSTO_SEQ.nextval seq from dual "
                                        dt.Clear()
                                        comando = New OracleCommand(sql, conn)
                                        comando.Transaction = transaccion
                                        dataAdapter = New OracleDataAdapter(comando)
                                        dataAdapter.Fill(dt)
                                        If dt.Rows.Count = 0 Then
                                            If Not transaccion.Equals(Nothing) Then
                                                transaccion.Rollback()
                                            End If
                                            MessageBox.Show("NO se encontró una secuencia. Consulte a Sistemas...", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                            Exit Sub
                                        Else
                                            sequen = dt.Rows(0).Item("seq").ToString
                                        End If
                                        sql = " SELECT B.codpro,b.rutcli FROM en_cliente_costo B "
                                        sql = sql & "WHERE B.codpro= '" & dgvProductosModificar.Item("Codigo", x).Value.ToString & "'"
                                        sql = sql & "  and B.rutcli= " & dtClientes.Rows(i).Item("rutcli").ToString
                                        sql = sql & "  and B.codemp= 3"
                                        sql = sql & "  and ( "
                                        sql = sql & "        (to_date('" & dgvProductosModificar.Item("Inicia", x).Value & "','DD/MM/YYYY') between B.FECINI AND B.FECFIN) "
                                        sql = sql & "     or (to_date('" & dgvProductosModificar.Item("Termina", x).Value & "','DD/MM/YYYY') between B.FECINI AND B.FECFIN) "
                                        sql = sql & "     or (B.FECINI between to_date('" & dgvProductosModificar.Item("Inicia", x).Value & "','DD/MM/YYYY') and to_date('" & dgvProductosModificar.Item("Termina", x).Value & "','DD/MM/YYYY')) "
                                        sql = sql & "     or (B.FECFIN between to_date('" & dgvProductosModificar.Item("Inicia", x).Value & "','DD/MM/YYYY') and to_date('" & dgvProductosModificar.Item("Termina", x).Value & "','DD/MM/YYYY')) "
                                        sql = sql & "      ) "
                                        comando = New OracleCommand(sql, conn)
                                        comando.Transaction = transaccion
                                        dataAdapter = New OracleDataAdapter(comando)
                                        dataAdapter.Fill(dtCostoEspecial)
                                        If dtCostoEspecial.Rows.Count = 0 Then

                                            sql = " INSERT INTO en_cliente_costo (ID_NUMERO, RUTCLI, CODPRO, PRECIO, COSTO, MARGEN, FECINI, FECFIN, CANTID, USR_CREAC, CODEMP, TIPO,CODMON,ESTADO) VALUES( "
                                            sql = sql & sequen & ", "
                                            sql = sql & dtClientes.Rows(i).Item("rutcli").ToString & " , "
                                            sql = sql & "'" & dgvProductosModificar.Item("Codigo", x).Value.ToString & "',"
                                            sql = sql & dgvProductosModificar.Item("Precio", x).Value.ToString & ","
                                            If dgvProductosModificar.Item("Costo Especial", x).Value > 0 Then
                                                sql = sql & dgvProductosModificar.Item("costo Especial", x).Value.ToString & ", "
                                            Else
                                                sql = sql & " round(getcostocomercial(3,'" + dgvProductosModificar.Item("Codigo", x).Value.ToString + "')),"
                                            End If
                                            sql = sql & Math.Round((((dgvProductosModificar.Item("Precio", x).Value - dgvProductosModificar.Item("costo Especial", x).Value) / dgvProductosModificar.Item("Precio", x).Value) * 100), 2).ToString.Replace(",", ".") & ", "
                                            sql = sql & "TO_DATE('" & dgvProductosModificar.Item("Inicia", x).Value & "','dd/mm/yyyy'), "
                                            sql = sql & "TO_DATE('" & dgvProductosModificar.Item("Termina", x).Value & "','dd/mm/yyyy'), "
                                            sql = sql & "1,'"
                                            sql = sql & Globales.user & "',"
                                            sql = sql & "3,"
                                            If dgvProductosModificar.Item("costo Especial", x).Value >= dgvProductosModificar.Item("cosprom", x).Value Then
                                                sql = sql & "2, "
                                            Else
                                                sql = sql & "0, "
                                            End If
                                            sql = sql & "0,'V')"
                                            comando = New OracleCommand(sql, conn)
                                            comando.Transaction = transaccion
                                            comando.ExecuteNonQuery()

                                        Else
                                            sql = " update en_cliente_costo set  "
                                            sql = sql & " fecfin=TO_DATE('" + Now.Date.AddDays(-1).Date.ToShortDateString + "','dd/mm/yyyy'), "
                                            sql = sql & " estado='C' "
                                            sql = sql & " where codemp=3 and (id_numero,rutcli,codpro) in( "
                                            sql = sql & " SELECT b.id_numero,b.rutcli,b.codpro FROM en_cliente_costo B  "
                                            sql = sql & " WHERE  B.rutcli= " + dtClientes.Rows(i).Item("rutcli").ToString
                                            sql = sql & "  and B.codemp= 3 "
                                            sql = sql & "  and b.codpro='" + dgvProductosModificar.Item("Codigo", x).Value.ToString + "' "
                                            sql = sql & "  and estado='V' "
                                            sql = sql & "  and ((to_date('" + dgvProductosModificar.Item("Inicia", x).Value + "','DD/MM/YYYY') between B.FECINI AND B.FECFIN)  "
                                            sql = sql & "     or (to_date('" + dgvProductosModificar.Item("Termina", x).Value + "','DD/MM/YYYY') between B.FECINI AND B.FECFIN)  "
                                            sql = sql & "     or (B.FECINI between to_date('" + dgvProductosModificar.Item("Inicia", x).Value + "','DD/MM/YYYY') and to_date('" + dgvProductosModificar.Item("Termina", x).Value + "','DD/MM/YYYY'))  "
                                            sql = sql & "     or (B.FECFIN between to_date('" + dgvProductosModificar.Item("Inicia", x).Value + "','DD/MM/YYYY') and to_date('" + dgvProductosModificar.Item("Termina", x).Value + "','DD/MM/YYYY'))  "
                                            sql = sql & "      )) "
                                            comando = New OracleCommand(sql, conn)
                                            comando.Transaction = transaccion
                                            comando.ExecuteNonQuery()

                                            sql = " INSERT INTO en_cliente_costo (ID_NUMERO, RUTCLI, CODPRO, PRECIO, COSTO, MARGEN, FECINI, FECFIN, CANTID, USR_CREAC, CODEMP, TIPO,CODMON,ESTADO) VALUES( "
                                            sql = sql & sequen & ", "
                                            sql = sql & dtClientes.Rows(i).Item("rutcli").ToString & " , "
                                            sql = sql & "'" & dgvProductosModificar.Item("Codigo", x).Value.ToString & "',"
                                            sql = sql & dgvProductosModificar.Item("Precio", x).Value.ToString & ","
                                            If dgvProductosModificar.Item("Costo Especial", x).Value > 0 Then
                                                sql = sql & dgvProductosModificar.Item("costo Especial", x).Value.ToString & ", "
                                            Else
                                                sql = sql & " round(getcostocomercial(3,'" + dgvProductosModificar.Item("Codigo", x).Value.ToString + "')),"
                                            End If
                                            sql = sql & Math.Round((((dgvProductosModificar.Item("Precio", x).Value - dgvProductosModificar.Item("costo Especial", x).Value) / dgvProductosModificar.Item("Precio", x).Value) * 100), 2).ToString.Replace(",", ".") & ", "
                                            sql = sql & "TO_DATE('" & dgvProductosModificar.Item("Inicia", x).Value & "','dd/mm/yyyy'), "
                                            sql = sql & "TO_DATE('" & dgvProductosModificar.Item("Termina", x).Value & "','dd/mm/yyyy'), "
                                            sql = sql & "1,'"
                                            sql = sql & Globales.user & "',"
                                            sql = sql & "3,"
                                            If dgvProductosModificar.Item("costo Especial", x).Value >= dgvProductosModificar.Item("cosprom", x).Value Then
                                                sql = sql & "2, "
                                            Else
                                                sql = sql & "0, "
                                            End If
                                            sql = sql & "0,'V')"
                                            comando = New OracleCommand(sql, conn)
                                            comando.Transaction = transaccion
                                            comando.ExecuteNonQuery()
                                        End If
                                    Else
                                        Continue For
                                    End If
                                    'LUEGO DE Modificar PRODUCTOS CORREGIR ENCABEZADO
                                    sql = " select sum(cantid*precio) totnet, "
                                    sql = sql & "round((sum(cantid*precio)*1.19) - sum(cantid*precio)) totiva, "
                                    sql = sql & "round(sum(cantid*precio)*1.19) totgen, "
                                    sql = sql & "round(avg(margen),2) margen "
                                    sql = sql & "from de_cotizac where codemp=3 "
                                    sql = sql & "and numcot= " + dtConvenio.Rows(0).Item("numcot").ToString

                                    Dim valores As New DataTable
                                    valores = bd.sqlSelect(sql)

                                    If (valores.Rows.Count) > 0 Then
                                        If valores.Rows(0).Item("totnet").ToString <> "" Then
                                            sql = " update en_cotizac set "
                                            sql = sql & " subtot=" + valores.Rows(0).Item("totnet").ToString + ", "
                                            sql = sql & " totnet=" + valores.Rows(0).Item("totnet").ToString + ", "
                                            sql = sql & " totiva=" + valores.Rows(0).Item("totiva").ToString + ", "
                                            sql = sql & " totgen=" + valores.Rows(0).Item("totgen").ToString + ", "
                                            sql = sql & " margen=" + valores.Rows(0).Item("margen").ToString.Replace(",", ".") + " "
                                            sql = sql & " where codemp=3 "
                                            sql = sql & " and numcot= " + dtConvenio.Rows(0).Item("numcot").ToString
                                            sql = sql & " and rutcli= " + dtClientes.Rows(i).Item("rutcli").ToString
                                            comando = New OracleCommand(sql, conn)
                                            comando.Transaction = transaccion
                                            comando.ExecuteNonQuery()

                                            sql = " update en_conveni set "
                                            sql = sql & "subtot=" + valores.Rows(0).Item("totnet").ToString + ", "
                                            sql = sql & "totnet=" + valores.Rows(0).Item("totnet").ToString + ", "
                                            sql = sql & "totiva=" + valores.Rows(0).Item("totiva").ToString + ", "
                                            sql = sql & "totgen=" + valores.Rows(0).Item("totgen").ToString + ", "
                                            sql = sql & "margen=" + valores.Rows(0).Item("margen").ToString.Replace(",", ".") + " "
                                            sql = sql & "where codemp=3 "
                                            sql = sql & "and numcot= " + dtConvenio.Rows(0).Item("numcot").ToString
                                            sql = sql & "and rutcli= " + dtClientes.Rows(i).Item("rutcli").ToString
                                            comando = New OracleCommand(sql, conn)
                                            comando.Transaction = transaccion
                                            comando.ExecuteNonQuery()
                                        End If
                                    End If
                                End If
                            Next
                        End If
                    Next

                    transaccion.Commit()

                    MessageBox.Show("Productos modificados con éxito", "Finalizado", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    buscaConvenio()
                Catch ex As Exception
                    If Not transaccion.Equals(Nothing) Then
                        transaccion.Rollback()
                    End If
                    PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                    MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Finally
                    conn.Close()
                End Try
            End Using
        End Using
    End Sub
    Private Sub btn_EliminardeConvenio_Click(sender As Object, e As EventArgs) Handles btn_EliminardeConvenio.Click
        Using conn = New OracleConnection(Conexion.retornaConexion)
            conn.Open()
            PuntoControl.RegistroUso("169")
            Using transaccion = conn.BeginTransaction
                Dim sql As String
                Cursor.Current = Cursors.WaitCursor
                Dim comando As OracleCommand
                Dim dataAdapter As OracleDataAdapter
                Dim dtClientes, dt As New DataTable
                Try
                    sql = "Set Role Rol_Aplicacion"
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    If txtRelCom.Text = "" Then
                        sql = " select rutcli from en_cliente where codemp=3 and rutcli= " + txtRutcli.Text
                        comando = New OracleCommand(sql, conn)
                        comando.Transaction = transaccion
                        dataAdapter = New OracleDataAdapter(comando)
                        dataAdapter.Fill(dtClientes)
                    Else
                        sql = " select distinct rutcli  "
                        sql = sql & " FROM re_emprela   "
                        sql = sql & " where numrel in(" + txtRelCom.Text + ")   "
                        'sql = sql & " and estado=2   "
                        sql = sql & " order by rutcli "
                        comando = New OracleCommand(sql, conn)
                        comando.Transaction = transaccion
                        dataAdapter = New OracleDataAdapter(comando)
                        dataAdapter.Fill(dtClientes)
                    End If
                    For i = 0 To dtClientes.Rows.Count - 1
                        'POR CADA CLIENTE REVISAR SI TIENE CONVENIO ACTIVO
                        sql = " select numcot,fecemi from en_conveni  "
                        sql = sql & "where codemp=3  "
                        sql = sql & "and sysdate between fecemi and fecven+1 "
                        sql = sql & "and rutcli= " + dtClientes.Rows(i).Item("rutcli").ToString
                        Dim dtConvenio = bd.sqlSelect(sql)
                        If dtConvenio.Rows.Count > 0 Then

                            For x = 0 To dgvProductoEliminar.RowCount - 1
                                If dgvProductoEliminar.Item("Sel", x).Value = 1 Then

                                    sql = "  insert into prod_elim_convenio_log (numcot,sequen,codpro,cantid,precio,costos,totnet,pordes,mondes,margen,  "
                                    sql = sql & " codlis,codlin,oferta,prelis,estpre,costocte,codemp,moneda,codimp,porimp,suc,fecemi,fecven,usuario,  "
                                    sql = sql & " fecha_elim)  "
                                    sql = sql & " select numcot,sequen,codpro,cantid,precio,costos,totnet,pordes,mondes,margen,codlis,codlin,  "
                                    sql = sql & " oferta,prelis,estpre,costocte,codemp,moneda,codimp,porimp,suc,fecemi,fecven,  "
                                    sql = sql & " SUBSTR(USER,1,20),sysdate from de_conveni  "
                                    sql = sql & " where codemp=3  "
                                    sql = sql & " and numcot= " + dtConvenio.Rows(0).Item("numcot").ToString
                                    sql = sql & " and codpro='" + dgvProductoEliminar.Item("Codigo", x).Value + "' "
                                    comando = New OracleCommand(sql, conn)
                                    comando.Transaction = transaccion
                                    comando.ExecuteNonQuery()


                                    sql = " delete from de_cotizac "
                                    sql = sql & " where codemp=3 "
                                    sql = sql & " and numcot= " + dtConvenio.Rows(0).Item("numcot").ToString
                                    sql = sql & " and codpro='" + dgvProductoEliminar.Item("Codigo", x).Value + "' "
                                    comando = New OracleCommand(sql, conn)
                                    comando.Transaction = transaccion
                                    comando.ExecuteNonQuery()

                                    sql = " delete from de_conveni "
                                    sql = sql & " where codemp=3 "
                                    sql = sql & " and numcot= " + dtConvenio.Rows(0).Item("numcot").ToString
                                    sql = sql & " and codpro='" + dgvProductoEliminar.Item("Codigo", x).Value + "' "
                                    comando = New OracleCommand(sql, conn)
                                    comando.Transaction = transaccion
                                    comando.ExecuteNonQuery()

                                    Dim dtCostoEspecial As New DataTable

                                    sql = " SELECT B.codpro,b.rutcli FROM en_cliente_costo B "
                                    sql = sql & "WHERE B.codpro= '" & dgvProductoEliminar.Item("Codigo", x).Value.ToString & "'"
                                    sql = sql & "  and B.rutcli= " & dtClientes.Rows(i).Item("rutcli").ToString
                                    sql = sql & "  and B.codemp= 3"
                                    sql = sql & " and b.estado='V' "
                                    sql = sql & "  and ( "
                                    sql = sql & "        (to_date('" & dgvProductoEliminar.Item("Inicia", x).Value & "','DD/MM/YYYY') between B.FECINI AND B.FECFIN) "
                                    sql = sql & "     or (to_date('" & dgvProductoEliminar.Item("Termina", x).Value & "','DD/MM/YYYY') between B.FECINI AND B.FECFIN) "
                                    sql = sql & "     or (B.FECINI between to_date('" & dgvProductoEliminar.Item("Inicia", x).Value & "','DD/MM/YYYY') and to_date('" & dgvProductoEliminar.Item("Termina", x).Value & "','DD/MM/YYYY')) "
                                    sql = sql & "     or (B.FECFIN between to_date('" & dgvProductoEliminar.Item("Inicia", x).Value & "','DD/MM/YYYY') and to_date('" & dgvProductoEliminar.Item("Termina", x).Value & "','DD/MM/YYYY')) "
                                    sql = sql & "      ) "
                                    comando = New OracleCommand(sql, conn)
                                    comando.Transaction = transaccion
                                    dataAdapter = New OracleDataAdapter(comando)
                                    dataAdapter.Fill(dtCostoEspecial)
                                    If dtCostoEspecial.Rows.Count > 0 Then
                                        sql = " update en_cliente_costo set  "
                                        sql = sql & " fecfin=TO_DATE('" + Now.Date.AddDays(-1).Date + "','dd/mm/yyyy'), "
                                        sql = sql & " estado='C' "
                                        sql = sql & " where codemp=3 and (id_numero,rutcli,codpro) in( "
                                        sql = sql & " SELECT b.id_numero,b.rutcli,b.codpro FROM en_cliente_costo B  "
                                        sql = sql & " WHERE  B.rutcli= " + dtClientes.Rows(i).Item("rutcli").ToString
                                        sql = sql & "  and B.codemp= 3 "
                                        sql = sql & "  and b.codpro='" + dgvProductoEliminar.Item("Codigo", x).Value.ToString + "' "
                                        sql = sql & "  and estado='V' "
                                        sql = sql & "  and ((to_date('" + dgvProductoEliminar.Item("Inicia", x).Value + "','DD/MM/YYYY') between B.FECINI AND B.FECFIN)  "
                                        sql = sql & "     or (to_date('" + dgvProductoEliminar.Item("Termina", x).Value + "','DD/MM/YYYY') between B.FECINI AND B.FECFIN)  "
                                        sql = sql & "     or (B.FECINI between to_date('" + dgvProductoEliminar.Item("Inicia", x).Value + "','DD/MM/YYYY') and to_date('" + dgvProductoEliminar.Item("Termina", x).Value + "','DD/MM/YYYY'))  "
                                        sql = sql & "     or (B.FECFIN between to_date('" + dgvProductoEliminar.Item("Inicia", x).Value + "','DD/MM/YYYY') and to_date('" + dgvProductoEliminar.Item("Termina", x).Value + "','DD/MM/YYYY'))  "
                                        sql = sql & "      )) "
                                        comando = New OracleCommand(sql, conn)
                                        comando.Transaction = transaccion
                                        comando.ExecuteNonQuery()

                                    End If
                                End If
                            Next

                            'LUEGO DE ELIMINAR PRODUCTOS CORREGIR ENCABEZADO Y GUARDAR EN LOG

                            sql = " select sum(cantid*precio) totnet, "
                            sql = sql & "round((sum(cantid*precio)*1.19) - sum(cantid*precio)) totiva, "
                            sql = sql & "round(sum(cantid*precio)*1.19) totgen, "
                            sql = sql & "round(avg(margen),2) margen "
                            sql = sql & "from de_cotizac where codemp=3 "
                            sql = sql & "and numcot= " + dtConvenio.Rows(0).Item("numcot").ToString

                            Dim valores = bd.sqlSelect(sql)

                            If valores.Rows.Count > 0 Then
                                If valores.Rows(0).Item("totnet").ToString <> "" Then
                                    sql = " update en_cotizac set "
                                    sql = sql & " subtot=" + valores.Rows(0).Item("totnet").ToString + ", "
                                    sql = sql & " totnet=" + valores.Rows(0).Item("totnet").ToString + ", "
                                    sql = sql & " totiva=" + valores.Rows(0).Item("totiva").ToString + ", "
                                    sql = sql & " totgen=" + valores.Rows(0).Item("totgen").ToString + ", "
                                    sql = sql & " margen=" + valores.Rows(0).Item("margen").ToString.Replace(",", ".") + " "
                                    sql = sql & " where codemp=3 "
                                    sql = sql & " and numcot= " + dtConvenio.Rows(0).Item("numcot").ToString
                                    sql = sql & " and rutcli= " + dtClientes.Rows(i).Item("rutcli").ToString
                                    comando = New OracleCommand(sql, conn)
                                    comando.Transaction = transaccion
                                    comando.ExecuteNonQuery()

                                    sql = " update en_conveni set "
                                    sql = sql & " subtot=" + valores.Rows(0).Item("totnet").ToString + ", "
                                    sql = sql & " totnet=" + valores.Rows(0).Item("totnet").ToString + ", "
                                    sql = sql & " totiva=" + valores.Rows(0).Item("totiva").ToString + ", "
                                    sql = sql & " totgen=" + valores.Rows(0).Item("totgen").ToString + ", "
                                    sql = sql & " margen=" + valores.Rows(0).Item("margen").ToString.Replace(",", ".") + " "
                                    sql = sql & " where codemp=3 "
                                    sql = sql & " and numcot= " + dtConvenio.Rows(0).Item("numcot").ToString
                                    sql = sql & " and rutcli= " + dtClientes.Rows(i).Item("rutcli").ToString
                                    comando = New OracleCommand(sql, conn)
                                    comando.Transaction = transaccion
                                    comando.ExecuteNonQuery()
                                End If
                            End If
                        End If
                    Next
                    transaccion.Commit()
                    MessageBox.Show("Productos eliminados con éxito", "Finalizado", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    buscaConvenio()
                Catch ex As Exception
                    If Not transaccion.Equals(Nothing) Then
                        transaccion.Rollback()
                    End If
                    PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                    MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Finally
                    conn.Close()
                End Try
            End Using
        End Using
    End Sub

    Private Sub dgvLineasReajuste_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvLineasReajuste.CellContentClick
        Try
            If e.RowIndex <> -1 Then
                If e.ColumnIndex = 0 Then
                    If dgvLineasReajuste.Item("SEL", e.RowIndex).Value = 0 Then
                        dgvLineasReajuste.Item("SEL", e.RowIndex).Value = 1
                    Else
                        dgvLineasReajuste.Item("SEL", e.RowIndex).Value = 0
                    End If
                End If
            End If
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub


    Private Sub cargafechasengrilla()
        Try
            Cursor.Current = Cursors.WaitCursor
            For i = 0 To dgvLineasReajuste.RowCount - 1
                For x = 0 To dgvCambiaFechas.RowCount - 1
                    If dgvLineasReajuste.Item("SEL", i).Value = 1 Then
                        If dgvCambiaFechas.Item("Linea", x).Value = dgvLineasReajuste.Item("Linea", i).Value Then
                            If Not dgvCambiaFechas.Item("FinOriginal", x).Value = dgvLineasReajuste.Item("Fecven", i).Value Then
                                dgvCambiaFechas.Item("Termina", x).Value = dgvLineasReajuste.Item("Fecven", i).Value
                                dgvCambiaFechas.Item("cambio", x).Value = 1
                            Else
                                dgvCambiaFechas.Item("cambio", x).Value = 0
                            End If
                        End If
                    End If
                Next
            Next

            If dtpFechaLineaReajuste.Value.Date > dtpFecVenConvenio.Value.Date Then
                dtpFecVenConvenio.Value = dtpFechaLineaReajuste.Value.Date
            End If

            Cursor.Current = Cursors.Arrow

        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub btn_CambiaFechas_Click(sender As Object, e As EventArgs) Handles btn_CambiaFechas.Click
        Using conn = New OracleConnection(Conexion.retornaConexion)
            conn.Open()
            PuntoControl.RegistroUso("170")
            Using transaccion = conn.BeginTransaction
                Cursor.Current = Cursors.WaitCursor
                Dim comando As OracleCommand
                Dim dataAdapter As OracleDataAdapter
                Dim dtClientes, dt As New DataTable
                Try
                    Dim Sql = "Set Role Rol_Aplicacion"
                    comando = New OracleCommand(Sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    If txtRelCom.Text = "" Then
                        Sql = " select rutcli from en_cliente where codemp=3 and rutcli= " + txtRutcli.Text
                        comando = New OracleCommand(Sql, conn)
                        comando.Transaction = transaccion
                        dataAdapter = New OracleDataAdapter(comando)
                        dataAdapter.Fill(dtClientes)
                    Else
                        Sql = " select distinct rutcli  "
                        Sql = Sql & " FROM re_emprela   "
                        Sql = Sql & " where numrel in(" + txtRelCom.Text + ")   "
                        'Sql = Sql & " and estado=2   "
                        Sql = Sql & " order by rutcli "
                        comando = New OracleCommand(Sql, conn)
                        comando.Transaction = transaccion
                        dataAdapter = New OracleDataAdapter(comando)
                        dataAdapter.Fill(dtClientes)
                    End If
                    For i = 0 To dtClientes.Rows.Count - 1
                        'POR CADA CLIENTE REVISAR SI TIENE CONVENIO ACTIVO
                        Sql = " select numcot,fecemi from en_conveni  "
                        Sql = Sql & "where codemp=3 "
                        Sql = Sql & "and sysdate between fecemi and fecven+1 "
                        Sql = Sql & "and rutcli= " + dtClientes.Rows(i).Item("rutcli").ToString
                        Dim dtConvenio = bd.sqlSelect(Sql)
                        If dtConvenio.Rows.Count > 0 Then

                            For x = 0 To dgvCambiaFechas.RowCount - 1
                                If dgvCambiaFechas.Item("cambio", x).Value = 1 Then
                                    Sql = "update de_cotizac set fecven=to_date('" + dgvCambiaFechas.Item("Termina", x).Value + "','dd/mm/yyyy') "
                                    Sql += " where codemp=3 and codpro ='" + dgvCambiaFechas.Item("Codigo", x).Value + "'"
                                    Sql += " and numcot=" + dtConvenio.Rows(0).Item("numcot").ToString
                                    comando = New OracleCommand(Sql, conn)
                                    comando.Transaction = transaccion
                                    comando.ExecuteNonQuery()

                                    Sql = "update de_conveni set fecven=to_date('" + dgvCambiaFechas.Item("Termina", x).Value + "','dd/mm/yyyy') "
                                    Sql += " where codemp=3 and codpro ='" + dgvCambiaFechas.Item("Codigo", x).Value + "'"
                                    Sql += " and numcot=" + dtConvenio.Rows(0).Item("numcot").ToString
                                    comando = New OracleCommand(Sql, conn)
                                    comando.Transaction = transaccion
                                    comando.ExecuteNonQuery()

                                    Sql = " SELECT id_numero,rutcli,codpro FROM en_cliente_costo B  "
                                    Sql = Sql & " WHERE  B.rutcli= " + dtClientes.Rows(i).Item("rutcli").ToString
                                    Sql = Sql & "  and B.codemp= 3 "
                                    Sql = Sql & "  and estado='V' "
                                    Sql = Sql & "  and codpro='" + dgvCambiaFechas.Item("Codigo", x).Value + "' "
                                    Dim dtCostoespecial = bd.sqlSelect(Sql)
                                    If dtCostoespecial.Rows.Count > 0 Then
                                        For j = 0 To dtCostoespecial.Rows.Count - 1
                                            Sql = " update en_cliente_costo set  "
                                            Sql = Sql & " fecfin=TO_DATE('" + dgvCambiaFechas.Item("Termina", x).Value + "','dd/mm/yyyy') "
                                            Sql = Sql & " where codemp=3 "
                                            Sql = Sql & " and codpro='" + dtCostoespecial.Rows(j).Item("codpro").ToString + "' "
                                            Sql = Sql & " and rutcli=" + dtCostoespecial.Rows(j).Item("rutcli").ToString + " "
                                            Sql = Sql & " and id_numero=" + dtCostoespecial.Rows(j).Item("id_numero").ToString + " "
                                            comando = New OracleCommand(Sql, conn)
                                            comando.Transaction = transaccion
                                            comando.ExecuteNonQuery()
                                        Next
                                    End If
                                End If
                            Next

                            Sql = "update en_cotizac set fecven=to_date('" + dtpFecVenConvenio.Value.Date + "','dd/mm/yyyy') "
                            Sql += " where codemp=3 and numcot=" + dtConvenio.Rows(0).Item("numcot").ToString
                            Sql += " and rutcli=" + dtClientes.Rows(i).Item("rutcli").ToString
                            comando = New OracleCommand(Sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()

                            Sql = "update en_conveni set fecven=to_date('" + dtpFecVenConvenio.Value.Date + "','dd/mm/yyyy') "
                            Sql += " where codemp=3 and numcot=" + dtConvenio.Rows(0).Item("numcot").ToString
                            Sql += " and rutcli=" + dtClientes.Rows(i).Item("rutcli").ToString
                            comando = New OracleCommand(Sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()

                            Sql = "update en_conveni_historias set fecven=to_date('" + dtpFecVenConvenio.Value.Date + "','dd/mm/yyyy') "
                            Sql += " where codemp=3 and numcot=" + dtConvenio.Rows(0).Item("numcot").ToString
                            Sql += " and rutcli=" + dtClientes.Rows(i).Item("rutcli").ToString
                            comando = New OracleCommand(Sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()

                        Else
                            Continue For
                        End If
                    Next

                    transaccion.Commit()
                    MessageBox.Show("Cambio de fechas finalizado con éxito", "Finalizado", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    buscaConvenio()
                Catch ex As Exception
                    If Not transaccion.Equals(Nothing) Then
                        transaccion.Rollback()
                    End If
                    PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                    MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Finally
                    conn.Close()
                End Try
            End Using
        End Using
    End Sub


    Private Sub btn_InputAgrega_Click(sender As Object, e As EventArgs) Handles btn_InputAgrega.Click
        PuntoControl.RegistroUso("161")
        Dim openFile As New OpenFileDialog
        If openFile.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            Globales.Importar(dgvInput, openFile.FileName)
            dgvInput.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            cargaInputAgrega()
        End If
    End Sub
    Private Sub cargaInputAgrega()
        Try
            bd.open_dimerc()
            Dim car As New frmCargando

            car.ProgressBar1.Minimum = 0
            car.ProgressBar1.Value = 0
            car.ProgressBar1.Maximum = dgvInput.RowCount
            car.StartPosition = FormStartPosition.CenterScreen
            car.Show()
            Cursor.Current = Cursors.WaitCursor
            dtGrilla.Rows.Clear()
            For i = 0 To dgvInput.RowCount - 1
                If dgvInput.Item("Codpro", i).Value Is Nothing Or dgvInput.Item("Codpro", i).Value.ToString = "" Then
                    car.ProgressBar1.Value += 1
                    Continue For
                End If
                Dim codpro = dgvInput.Item("Codpro", i).Value.ToString
                Dim Precio = dgvInput.Item("Precio", i).Value.ToString
                Dim CostoEspecial = dgvInput.Item("Costo Especial", i).Value.ToString
                Dim Fecven As Date = dgvInput.Item("Fecven", i).Value
                Dim temporal = IIf(rdbTemporal.Checked = True, "TEMPORAL", "DEFINITIVO")
                Dim sql = "select codpro,despro,getlinea(codpro) linea,codlin,getcostopromedio(3,codpro) costo from ma_product where codpro='" + dgvInput.Item("Codpro", i).Value.ToString + "'"
                Dim dt = bd.sqlSelect(sql)
                If dt.Rows.Count > 0 Then
                    dtGrilla.Rows.Add(codpro, dt.Rows(0).Item("despro"),
                                      dt.Rows(0).Item("linea"), Precio, CostoEspecial,
                                      Fecven.ToShortDateString, dt.Rows(0).Item("codlin"), temporal, dt.Rows(0).Item("costo"))

                Else
                    Continue For
                End If

                car.ProgressBar1.Value += 1
            Next
            car.Close()
            dgvProductosaAgregar.DataSource = dtGrilla
            formateagrilla()
            MessageBox.Show("Input cargado con éxito", "Listo", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub btn_CargaInputModifica_Click(sender As Object, e As EventArgs) Handles btn_CargaInputModifica.Click
        buscaConvenio()
        PuntoControl.RegistroUso("165")
        Dim openFile As New OpenFileDialog
        If openFile.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            Globales.Importar(dgvInput, openFile.FileName)
            dgvInput.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            cargaInputModifica()
        End If
    End Sub
    Private Sub cargaInputModifica()
        Try
            bd.open_dimerc()
            Dim car As New frmCargando

            car.ProgressBar1.Minimum = 0
            car.ProgressBar1.Value = 0
            car.ProgressBar1.Maximum = dgvInput.RowCount
            car.StartPosition = FormStartPosition.CenterScreen
            car.Show()
            Cursor.Current = Cursors.WaitCursor

            For i = 0 To dgvInput.RowCount - 1
                If dgvInput.Item("Codpro", i).Value Is Nothing Or dgvInput.Item("Codpro", i).Value.ToString = "" Then
                    car.ProgressBar1.Value += 1
                    Continue For
                End If
                Dim codpro = dgvInput.Item("Codpro", i).Value.ToString
                Dim Precio = dgvInput.Item("Precio", i).Value.ToString
                Dim CostoEspecial = dgvInput.Item("Costo Especial", i).Value.ToString
                For x = 0 To dgvProductosModificar.RowCount - 1
                    If codpro = dgvProductosModificar.Item("Codigo", x).Value Then
                        dgvProductosModificar.Item("Precio", x).Value = Precio
                        dgvProductosModificar.Item("Costo Especial", x).Value = CostoEspecial
                        dgvProductosModificar.Item("cambio", x).Value = 1

                        Exit For
                    End If
                Next

                car.ProgressBar1.Value += 1
            Next
            car.Close()

            MessageBox.Show("Input cargado con éxito", "Listo", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub btn_InputElimina_Click(sender As Object, e As EventArgs) Handles btn_InputElimina.Click
        buscaConvenio()
        PuntoControl.RegistroUso("168")
        Dim openFile As New OpenFileDialog
        If openFile.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            Globales.Importar(dgvInput, openFile.FileName)
            dgvInput.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            cargaInputElimina()
        End If
    End Sub
    Private Sub cargaInputElimina()
        Try
            bd.open_dimerc()
            Dim car As New frmCargando

            car.ProgressBar1.Minimum = 0
            car.ProgressBar1.Value = 0
            car.ProgressBar1.Maximum = dgvInput.RowCount
            car.StartPosition = FormStartPosition.CenterScreen
            car.Show()
            Cursor.Current = Cursors.WaitCursor

            For i = 0 To dgvInput.RowCount - 1
                If dgvInput.Item("Codpro", i).Value Is Nothing Or dgvInput.Item("Codpro", i).Value.ToString = "" Then
                    car.ProgressBar1.Value += 1
                    Continue For
                End If
                Dim codpro = dgvInput.Item("Codpro", i).Value.ToString
                For x = 0 To dgvProductoEliminar.RowCount - 1
                    If codpro = dgvProductoEliminar.Item("Codigo", x).Value Then
                        dgvProductoEliminar.Item("Sel", x).Value = 1

                        Exit For
                    End If
                Next

                car.ProgressBar1.Value += 1
            Next
            car.Close()

            MessageBox.Show("Input cargado con éxito", "Listo", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub dtpFechaLineaReajuste_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFechaLineaReajuste.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            dtpFechaLineaReajuste.Enabled = False
            ProgressBarCargando.Visible = True
            ProgressBarCargando.Maximum = dgvLineasReajuste.RowCount
            ProgressBarCargando.Value = 0
            Application.DoEvents()

            For i = 0 To dgvLineasReajuste.RowCount - 1
                ProgressBarCargando.Value = (i + 1)
                Application.DoEvents()
                If dgvLineasReajuste.Item("SEL", i).Value = 1 Then
                    dgvLineasReajuste.Item("Fecven", i).Value = dtpFechaLineaReajuste.Value.Date
                End If
            Next

            cargafechasengrilla()
            dtpFechaLineaReajuste.Enabled = True
            ProgressBarCargando.Visible = False
        End If
    End Sub

    Private Sub dtpFecVenConvenio_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFecVenConvenio.KeyPress
        cargafechasengrilla()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnTipoConvenio.Click
        Using conn = New OracleConnection(Conexion.retornaConexion)
            conn.Open()
            Using transaccion = conn.BeginTransaction
                PuntoControl.RegistroUso("160")
                Dim sql As String
                Cursor.Current = Cursors.WaitCursor
                Dim comando As OracleCommand
                Dim dataAdapter As OracleDataAdapter
                Dim dtClientes, dt As New DataTable
                Try
                    sql = "Set Role Rol_Aplicacion"
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    If cmbTipoConvenio.SelectedItem = "Convenio Tissue" Then
                        sql = "Update en_conveni set ticonv = 'T' where numcot = " & convenio
                    Else
                        sql = "Update en_conveni set ticonv = 'M' where numcot = " & convenio
                    End If
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    transaccion.Commit()

                    MessageBox.Show("Convenio Modificado.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)

                Catch ex As Exception
                    If Not transaccion.Equals(Nothing) Then
                        transaccion.Rollback()
                    End If
                    PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                    MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Finally
                    conn.Close()
                End Try
            End Using
        End Using
    End Sub

    Private Sub chkMarcaTodoConvenio_CheckedChanged(sender As Object, e As EventArgs) Handles chkMarcaTodoConvenio.CheckedChanged
        If chkMarcaTodoConvenio.Checked = True Then
            For i = 0 To dgvLineasReajuste.RowCount - 1
                dgvLineasReajuste.Item("SEL", i).Value = 1
            Next
        Else
            For i = 0 To dgvLineasReajuste.RowCount - 1
                dgvLineasReajuste.Item("SEL", i).Value = 0
            Next
        End If
    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles btnProcesar.Click
        btnProcesar.Enabled = False
        Using conn = New OracleConnection(Conexion.retornaConexion)
            conn.Open()
            PuntoControl.RegistroUso("173")
            Using transaccion = conn.BeginTransaction
                Dim sql As String
                Cursor.Current = Cursors.WaitCursor
                Dim comando As OracleCommand
                Dim dataAdapter As OracleDataAdapter
                Dim dt1, dt As New DataTable
                Dim secuencia As String
                bd.open_dimerc()
                Try
                    If cbxReversaReem.Checked = True Then
                        If MessageBox.Show("Desea reversar los codigos reemplazados", "Atención", MessageBoxButtons.YesNo) = 7 Then
                            Exit Sub
                        End If

                        ProgressBar1.Visible = True
                        ProgressBar1.Maximum = dgvReemplazo.RowCount

                        sql = "Set Role Rol_Aplicacion"
                        comando = New OracleCommand(sql, conn)
                        comando.Transaction = transaccion
                        comando.ExecuteNonQuery()

                        'sql = "SELECT EN_CLIENTE_COSTO_SEQ.nextval seq from dual"
                        'dt = bd.sqlSelect(sql)

                        'secuencia = dt.Rows(0).Item("seq")

                        For i = 0 To dgvReemplazo.RowCount - 1
                            ProgressBar1.Value = (i + 1)
                            Application.DoEvents()
                            sql = " insert into de_conveni(numcot,sequen,codpro,cantid,precio,costos,totnet,pordes, "
                            sql += "mondes,margen,codlis,codlin,codemp,fecemi,fecven) values("
                            sql += dgvReemplazo.Item("numcot", i).Value.ToString + ","
                            sql += "0,"
                            sql += "'" + txtSkuOri.Text + "',"
                            sql += "1,"
                            sql += dgvReemplazo.Item("precio", i).Value.ToString + ","
                            sql += dgvReemplazo.Item("costos", i).Value.ToString + ","
                            sql += dgvReemplazo.Item("precio", i).Value.ToString + ","
                            sql += "0,0,"
                            sql += Math.Round((((dgvReemplazo.Item("precio", i).Value - dgvReemplazo.Item("costos", i).Value) / dgvReemplazo.Item("precio", i).Value) * 100), 2).ToString.Replace(",", ".") + ","
                            sql += "0," + dgvReemplazo.Item("codlin", i).Value.ToString + ",3,"
                            sql += "to_date('" + Now.Date + "','dd/mm/yyyy'),"
                            sql += "to_date('" + dtpFecha.Value.ToShortDateString + "','dd/mm/yyyy')"
                            sql += ") "
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()
                        Next

                        For i = 0 To dgvReemplazo.RowCount - 1
                            ProgressBar1.Value = (i + 1)
                            Application.DoEvents()
                            sql = "delete de_conveni where numcot = " & dgvOriginal.Item("numcot", i).Value.ToString & " and codpro = '" & txtSkuOri.Text.Trim & "'"
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()
                        Next

                        MessageBox.Show("Productos Agregados con éxito", "Finalizado", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        transaccion.Commit()
                    Else
                        If rbtAgregar.Checked = True Then
                            If MessageBox.Show("Desea agregar " & txtConvenioReem.Text & " convenios de " & txtConvenioTotales.Text, "Atención", MessageBoxButtons.YesNo) = 7 Then
                                Exit Sub
                            End If
                            ProgressBar1.Visible = True
                            ProgressBar1.Maximum = dgvReemplazo.RowCount

                            sql = "Set Role Rol_Aplicacion"
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()

                            sql = "SELECT EN_CLIENTE_COSTO_SEQ.nextval seq from dual"
                            dt = bd.sqlSelect(sql)

                            secuencia = dt.Rows(0).Item("seq")

                            For i = 0 To dgvReemplazo.RowCount - 1
                                ProgressBar1.Value = (i + 1)
                                Application.DoEvents()
                                sql = " insert into de_conveni(numcot,sequen,codpro,cantid,precio,costos,totnet,pordes, "
                                sql += "mondes,margen,codlis,codlin,codemp,suc,fecemi,fecven) values("
                                sql += dgvReemplazo.Item("numcot", i).Value.ToString + ","
                                sql += "0,"
                                sql += "'" + txtSkuReem.Text + "',"
                                sql += "1,"
                                sql += dgvReemplazo.Item("precio", i).Value.ToString + ","
                                sql += dgvReemplazo.Item("costos", i).Value.ToString + ","
                                sql += dgvReemplazo.Item("precio", i).Value.ToString + ","
                                sql += "0,0,"
                                sql += Math.Round((((dgvReemplazo.Item("precio", i).Value - dgvReemplazo.Item("costos", i).Value) / dgvReemplazo.Item("precio", i).Value) * 100), 2).ToString.Replace(",", ".") + ","
                                sql += "0," + dgvReemplazo.Item("codlin", i).Value.ToString + ",3,'" & txtSkuOri.Text & "',"
                                sql += "to_date('" + Now.Date + "','dd/mm/yyyy'),"
                                If dtpFecha.Value > dgvReemplazo.Item("fecven", i).Value Then
                                    sql += "to_date('" + dgvReemplazo.Item("fecven", i).Value.ToString + "','dd/mm/yyyy')"
                                Else
                                    sql += "to_date('" + dtpFecha.Value.ToShortDateString + "','dd/mm/yyyy')"
                                End If
                                sql += ") "
                                comando = New OracleCommand(sql, conn)
                                comando.Transaction = transaccion
                                comando.ExecuteNonQuery()

                                sql = "Select id_numero From EN_CLIENTE_COSTO
                               Where RUTCLI = " & dgvReemplazo.Item("RUTCLI", i).Value.ToString & "
                                 And CODPRO = '" & txtSkuReem.Text & "'
                                 And ESTADO = 'V'"
                                dt = bd.sqlSelect(sql)

                                If dt.Rows.Count > 0 Then
                                    sql = "UPDATE EN_CLIENTE_COSTO SET FECFIN = (SYSDATE-1), ESTADO = 'C' WHERE RUTCLI = " & dgvReemplazo.Item("RUTCLI", i).Value.ToString & " AND ID_NUMERO = " & dt.Rows(0).Item("ID_NUMERO") & " AND CODPRO = '" & txtSkuReem.Text & "'"
                                    comando = New OracleCommand(sql, conn)
                                    comando.Transaction = transaccion
                                    comando.ExecuteNonQuery()
                                End If

                                sql = "insert into en_cliente_costo
                                   select " & secuencia & " ID_NUMERO,
                                       " & dgvReemplazo.Item("RUTCLI", i).Value.ToString & " RUTCLI,
                                       '" & txtSkuReem.Text & "' CODPRO,
                                       B.PRECIO,
                                       B.COSTO,
                                       B.MARGEN,
                                       TO_DATE('" & Date.Now.ToShortDateString & " 00:00:00', 'DD/MM/YYYY HH24:MI:SS') FECINI,
                                       TO_DATE('" & dtpFecha.Value.ToShortDateString & " 00:00:00', 'DD/MM/YYYY HH24:MI:SS') FECFIN,
                                       B.CANTID,
                                       B.USR_CREAC,
                                       B.CODEMP,
                                       B.TIPO,
                                       B.CODMON,
                                       B.FECHA_CREAC,
                                       B.FECHA_MODIF,
                                       B.ESTADO from de_conveni A, EN_CLIENTE_COSTO B
                                WHERE A.NUMCOT = " & dgvReemplazo.Item("numcot", i).Value.ToString & "
                                  AND A.CODPRO = '" & txtSkuOri.Text & "'                              
                                  AND A.CODPRO = B.CODPRO
                                  AND B.ESTADO = 'V'
                                  AND B.RUTCLI = " & dgvReemplazo.Item("RUTCLI", i).Value.ToString & "
                                GROUP BY B.CODPRO, B.PRECIO,
                                       B.COSTO, B.MARGEN, B.FECFIN,
                                       B.CANTID, B.USR_CREAC,
                                       B.CODEMP, B.TIPO,
                                       B.CODMON, B.FECHA_CREAC,
                                       B.FECHA_MODIF, B.ESTADO"
                                comando = New OracleCommand(sql, conn)
                                comando.Transaction = transaccion
                                comando.ExecuteNonQuery()
                            Next

                            sql = "Update EN_CLIENTE_COSTO SET TIPO = 0
                            WHERE CODPRO = '" & txtSkuReem.Text & "'
                              And ESTADO = 'V'
                              And costo < " & costo & "
                              And TIPO <> 0 "
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()

                            MessageBox.Show("Productos Agregados con éxito", "Finalizado", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            transaccion.Commit()
                        ElseIf rbtQuitar.Checked = True Then
                            If MessageBox.Show("Desea quitar el producto " & txtSkuOri.Text & " de " & txtConvenioTotales.Text & " convenios ", "Atención", MessageBoxButtons.YesNo) = 7 Then
                                Exit Sub
                            End If
                            ProgressBar1.Visible = True
                            ProgressBar1.Maximum = dgvReemplazo.RowCount

                            sql = "Set Role Rol_Aplicacion"
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()

                            For i = 0 To dgvOriginal.RowCount - 1
                                ProgressBar1.Value = (i + 1)
                                Application.DoEvents()
                                sql = "delete de_conveni where numcot = " & dgvOriginal.Item("numcot", i).Value.ToString & " and codpro = '" & txtSkuOri.Text.Trim & "'"
                                comando = New OracleCommand(sql, conn)
                                comando.Transaction = transaccion
                                comando.ExecuteNonQuery()
                            Next

                            MessageBox.Show("Productos Eliminados con éxito", "Finalizado", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            transaccion.Commit()

                        ElseIf rbtReemplazar.Checked = True Then
                            If MessageBox.Show("Desea agregar " & txtConvenioReem.Text & " convenios de " & txtConvenioTotales.Text, "Atención", MessageBoxButtons.YesNo) = 7 Then
                                Exit Sub
                            End If
                            ProgressBar1.Visible = True
                            ProgressBar1.Maximum = dgvReemplazo.RowCount

                            sql = "Set Role Rol_Aplicacion"
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()

                            'RESERVA UN ID DE COSTO ESPECIAL
                            sql = "SELECT EN_CLIENTE_COSTO_SEQ.nextval seq from dual"
                            dt = bd.sqlSelect(sql)

                            secuencia = dt.Rows(0).Item("seq")

                            For i = 0 To dgvReemplazo.RowCount - 1
                                ProgressBar1.Value = (i + 1)
                                Application.DoEvents()
                                sql = " insert into de_conveni(numcot,sequen,codpro,cantid,precio,costos,totnet,pordes, "
                                sql += "mondes,margen,codlis,codlin,codemp,suc,fecemi,fecven) values("
                                sql += dgvReemplazo.Item("numcot", i).Value.ToString + ","
                                sql += "0,"
                                sql += "'" + txtSkuReem.Text + "',"
                                sql += "1,"
                                sql += dgvReemplazo.Item("precio", i).Value.ToString + ","
                                sql += dgvReemplazo.Item("costos", i).Value.ToString + ","
                                sql += dgvReemplazo.Item("precio", i).Value.ToString + ","
                                sql += "0,0,"
                                sql += Math.Round((((dgvReemplazo.Item("precio", i).Value - dgvReemplazo.Item("costos", i).Value) / dgvReemplazo.Item("precio", i).Value) * 100), 2).ToString.Replace(",", ".") + ","
                                sql += "0," + dgvReemplazo.Item("codlin", i).Value.ToString + ",3,'" & txtSkuOri.Text & "',"
                                sql += "to_date('" + Now.Date + "','dd/mm/yyyy'),"
                                If dtpFecha.Value > dgvReemplazo.Item("fecven", i).Value Then
                                    sql += "to_date('" + dgvReemplazo.Item("fecven", i).Value.ToString + "','dd/mm/yyyy')"
                                Else
                                    sql += "to_date('" + dtpFecha.Value.ToShortDateString + "','dd/mm/yyyy')"
                                End If
                                sql += ") "
                                comando = New OracleCommand(sql, conn)
                                comando.Transaction = transaccion
                                comando.ExecuteNonQuery()

                                'BUSCA COSTO ESPECIAL VIGENTE (RUT - CODIGO)
                                sql = "Select id_numero From EN_CLIENTE_COSTO
                                   Where RUTCLI = " & dgvReemplazo.Item("RUTCLI", i).Value.ToString & "
                                     And CODPRO = '" & txtSkuReem.Text & "'
                                     And ESTADO = 'V'"
                                dt = bd.sqlSelect(sql)

                                'CADUCA COSTO ESPECIAL EXISTENTE
                                If dt.Rows.Count > 0 Then
                                    sql = "UPDATE EN_CLIENTE_COSTO SET FECFIN = (SYSDATE-1), ESTADO = 'C' WHERE RUTCLI = " & dgvReemplazo.Item("RUTCLI", i).Value.ToString & " AND ID_NUMERO = " & dt.Rows(0).Item("ID_NUMERO") & " AND CODPRO = '" & txtSkuReem.Text & "'"
                                    comando = New OracleCommand(sql, conn)
                                    comando.Transaction = transaccion
                                    comando.ExecuteNonQuery()
                                End If

                                sql = "insert into en_cliente_costo
                                   select " & secuencia & " ID_NUMERO,
                                       " & dgvReemplazo.Item("RUTCLI", i).Value.ToString & " RUTCLI,
                                       '" & txtSkuReem.Text & "' CODPRO,
                                       B.PRECIO,
                                       B.COSTO,
                                       B.MARGEN,
                                       TO_DATE('" & Date.Now.ToShortDateString & " 00:00:00', 'DD/MM/YYYY HH24:MI:SS') FECINI,
                                       TO_DATE('" & dtpFecha.Value.ToShortDateString & " 00:00:00', 'DD/MM/YYYY HH24:MI:SS') FECFIN,
                                       B.CANTID,
                                       B.USR_CREAC,
                                       B.CODEMP,
                                       B.TIPO,
                                       B.CODMON,
                                       B.FECHA_CREAC,
                                       B.FECHA_MODIF,
                                       B.ESTADO from de_conveni A, EN_CLIENTE_COSTO B
                                WHERE A.NUMCOT = " & dgvReemplazo.Item("numcot", i).Value.ToString & "
                                  AND A.CODPRO = '" & txtSkuOri.Text & "'                              
                                  AND A.CODPRO = B.CODPRO
                                  AND B.ESTADO = 'V'
                                  AND B.RUTCLI = " & dgvReemplazo.Item("RUTCLI", i).Value.ToString & "
                                GROUP BY B.CODPRO, B.PRECIO,
                                       B.COSTO, B.MARGEN, B.FECFIN,
                                       B.CANTID, B.USR_CREAC,
                                       B.CODEMP, B.TIPO,
                                       B.CODMON, B.FECHA_CREAC,
                                       B.FECHA_MODIF, B.ESTADO"
                                comando = New OracleCommand(sql, conn)
                                comando.Transaction = transaccion
                                comando.ExecuteNonQuery()
                            Next

                            sql = "Update EN_CLIENTE_COSTO SET TIPO = 0
                            WHERE CODPRO = '" & txtSkuReem.Text & "'
                              And ESTADO = 'V'
                              And costo < " & costo & "
                              And TIPO <> 0 "
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()

                            For i = 0 To dgvOriginal.RowCount - 1
                                ProgressBar1.Value = (i + 1)
                                Application.DoEvents()
                                sql = "delete de_conveni where numcot = " & dgvOriginal.Item("numcot", i).Value.ToString & " and codpro = '" & txtSkuOri.Text.Trim & "'"
                                comando = New OracleCommand(sql, conn)
                                comando.Transaction = transaccion
                                comando.ExecuteNonQuery()
                            Next

                            MessageBox.Show("Productos Agregados con éxito", "Finalizado", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            transaccion.Commit()
                        End If
                    End If

                Catch ex As Exception
                    If Not transaccion.Equals(Nothing) Then
                        transaccion.Rollback()
                    End If
                    PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                    MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Finally
                    ProgressBar1.Visible = False
                    conn.Close()
                    btnProcesar.Enabled = True
                End Try
            End Using
        End Using
    End Sub

    Private Sub txtSkuOri_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtSkuOri.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Trim(txtSkuOri.Text) <> "" Then
                buscaProductoConvenio(2)
            End If
        End If
    End Sub

    Private Sub txtSkuReem_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtSkuReem.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Trim(txtSkuOri.Text) <> "" Then
                buscaProductoReemplazo(2)
            End If
        End If
    End Sub

    Private Sub btnxlsxOriginal_Click(sender As Object, e As EventArgs) Handles btnxlsxOriginal.Click
        PuntoControl.RegistroUso("171")
        Dim save As New SaveFileDialog
        If dgvOriginal.RowCount = 0 Then
            MessageBox.Show("No hay datos consultados en la grilla, procese datos antes de exportar", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        Else
            save.Filter = "Archivo Excel | *.xlsx"
            If save.ShowDialog = System.Windows.Forms.DialogResult.OK Then
                Globales.exporta(save.FileName, dgvOriginal)
            End If
        End If
    End Sub

    Private Sub btnxlsxReemplazo_Click(sender As Object, e As EventArgs) Handles btnxlsxReemplazo.Click
        PuntoControl.RegistroUso("172")
        Dim save As New SaveFileDialog
        If dgvReemplazo.RowCount = 0 Then
            MessageBox.Show("No hay datos consultados en la grilla, procese datos antes de exportar", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        Else
            save.Filter = "Archivo Excel | *.xlsx"
            If save.ShowDialog = System.Windows.Forms.DialogResult.OK Then
                Globales.exporta(save.FileName, dgvReemplazo)
            End If
        End If
    End Sub

    Private Sub txtSkuModifica_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtSkuModifica.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            For i = 0 To dgvProductosModificar.RowCount - 1
                If dgvProductosModificar.Item("Codigo", i).Value = txtSkuModifica.Text Then
                    dgvProductosModificar.Rows(i).Selected = True
                    dgvProductosModificar.CurrentCell = Me.dgvProductosModificar.Item(0, i)
                    dgvProductosModificar.FirstDisplayedCell = dgvProductosModificar.CurrentCell
                    Exit Sub
                End If
            Next
        End If
    End Sub

    Private Sub CheckBox1_Click(sender As Object, e As EventArgs) Handles cbxReversaReem.Click
        If cbxReversaReem.Checked = True Then
            rbtAgregar.Enabled = False
            rbtQuitar.Enabled = False
            rbtReemplazar.Enabled = False
        Else
            rbtAgregar.Enabled = True
            rbtQuitar.Enabled = True
            rbtReemplazar.Enabled = True
        End If
    End Sub

    Private Sub txtskuOriginalM_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtskuOriginalM.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Trim(txtskuOriginalM.Text) <> "" Then
                buscaProductoConvenio(1)
            End If
        End If
    End Sub

    Private Sub txtSkuRemM_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtSkuRemM.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Trim(txtSkuRemM.Text) <> "" Then
                buscaProductoReemplazo(1)
            End If
        End If
    End Sub

    Private Sub btnProcesarManual_Click(sender As Object, e As EventArgs) Handles btnProcesarManual.Click
        btnProcesarManual.Enabled = False
        Using conn = New OracleConnection(Conexion.retornaConexion)
            conn.Open()
            Using transaccion = conn.BeginTransaction
                PuntoControl.RegistroUso("174")
                Dim sql As String
                Cursor.Current = Cursors.WaitCursor
                Dim comando As OracleCommand
                Dim dataAdapter As OracleDataAdapter
                Dim dt1, dt As New DataTable
                Dim secuencia As String
                bd.open_dimerc()
                Try
                    If cbxReversaM.Checked = True Then
                        If MessageBox.Show("Desea reversar los codigos reemplazados", "Atención", MessageBoxButtons.YesNo) = 7 Then
                            Exit Sub
                        End If

                        sql = "Set Role Rol_Aplicacion"
                        comando = New OracleCommand(sql, conn)
                        comando.Transaction = transaccion
                        comando.ExecuteNonQuery()

                        For i = 0 To dgvConReemM.RowCount - 1
                            sql = " insert into de_conveni(numcot,sequen,codpro,cantid,precio,costos,totnet,pordes, "
                            sql += "mondes,margen,codlis,codlin,codemp,fecemi,fecven) values("
                            sql += dgvConReemM.Item("numcot", i).Value.ToString + ","
                            sql += "0,"
                            sql += "'" + txtSkuRemM.Text + "',"
                            sql += "1,"
                            sql += dgvConReemM.Item("precio", i).Value.ToString + ","
                            sql += dgvConReemM.Item("costos", i).Value.ToString + ","
                            sql += dgvConReemM.Item("precio", i).Value.ToString + ","
                            sql += "0,0,"
                            sql += Math.Round((((dgvConReemM.Item("precio", i).Value - dgvConReemM.Item("costos", i).Value) / dgvConReemM.Item("precio", i).Value) * 100), 2).ToString.Replace(",", ".") + ","
                            sql += "0," + dgvConReemM.Item("codlin", i).Value.ToString + ",3,"
                            sql += "to_date('" + Now.Date + "','dd/mm/yyyy'),"
                            sql += "to_date('" + dtpFechaManual.Value.ToShortDateString + "','dd/mm/yyyy')"
                            sql += ") "
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()

                        Next
                        For i = 0 To dgvConOriM.RowCount - 1
                            sql = "delete de_conveni where numcot = " & dgvConOriM.Item("numcot", i).Value.ToString & " and codpro = '" & txtskuOriginalM.Text.Trim & "'"
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()
                        Next

                        MessageBox.Show("Productos Agregados con éxito", "Finalizado", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        transaccion.Commit()
                    Else
                        If rbtAgregarM.Checked = True Then
                            If MessageBox.Show("Desea agregar el producto " & txtskuOriginalM.Text & " a los convenios", "Atención", MessageBoxButtons.YesNo) = 7 Then
                                Exit Sub
                            End If

                            sql = "Set Role Rol_Aplicacion"
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()

                            sql = "SELECT EN_CLIENTE_COSTO_SEQ.nextval seq from dual"
                            dt = bd.sqlSelect(sql)

                            secuencia = dt.Rows(0).Item("seq")

                            For i = 0 To dgvConReemM.RowCount - 1
                                sql = " insert into de_conveni(numcot,sequen,codpro,cantid,precio,costos,totnet,pordes, "
                                sql += "mondes,margen,codlis,codlin,codemp,suc,fecemi,fecven) values("
                                sql += dgvConReemM.Item("numcot", i).Value.ToString + ","
                                sql += "0,"
                                sql += "'" + txtSkuRemM.Text + "',"
                                sql += "1,"
                                sql += dgvConReemM.Item("precio", i).Value.ToString + ","
                                sql += dgvConReemM.Item("costos", i).Value.ToString + ","
                                sql += dgvConReemM.Item("precio", i).Value.ToString + ","
                                sql += "0,0,"
                                sql += Math.Round((((dgvConReemM.Item("precio", i).Value - dgvConReemM.Item("costos", i).Value) / dgvConReemM.Item("precio", i).Value) * 100), 2).ToString.Replace(",", ".") + ","
                                sql += "0," + dgvConReemM.Item("codlin", i).Value.ToString + ",3,'" & txtskuOriginalM.Text & "',"
                                sql += "to_date('" + Now.Date + "','dd/mm/yyyy'),"
                                If dtpFechaManual.Value > dgvConReemM.Item("fecven", i).Value Then
                                    sql += "to_date('" + dgvConReemM.Item("fecven", i).Value.ToString + "','dd/mm/yyyy')"
                                Else
                                    sql += "to_date('" + dtpFechaManual.Value.ToShortDateString + "','dd/mm/yyyy')"
                                End If
                                sql += ") "
                                comando = New OracleCommand(sql, conn)
                                comando.Transaction = transaccion
                                comando.ExecuteNonQuery()

                                sql = "Select id_numero From EN_CLIENTE_COSTO
                                   Where RUTCLI = " & dgvConReemM.Item("RUTCLI", i).Value.ToString & "
                                     And CODPRO = '" & txtSkuRemM.Text & "'
                                     And ESTADO = 'V'"
                                dt = bd.sqlSelect(sql)

                                If dt.Rows.Count > 0 Then
                                    sql = "UPDATE EN_CLIENTE_COSTO SET FECFIN = (SYSDATE-1), ESTADO = 'C' WHERE RUTCLI = " & dgvConReemM.Item("RUTCLI", i).Value.ToString & " AND ID_NUMERO = " & dt.Rows(0).Item("ID_NUMERO") & " AND CODPRO = '" & txtSkuRemM.Text & "'"
                                    comando = New OracleCommand(sql, conn)
                                    comando.Transaction = transaccion
                                    comando.ExecuteNonQuery()
                                End If

                                sql = "insert into en_cliente_costo
                                   select " & secuencia & " ID_NUMERO,
                                       " & dgvConReemM.Item("RUTCLI", i).Value.ToString & " RUTCLI,
                                       '" & txtSkuRemM.Text & "' CODPRO,
                                       B.PRECIO,
                                       B.COSTO,
                                       B.MARGEN,
                                       TO_DATE('" & Date.Now.ToShortDateString & " 00:00:00', 'DD/MM/YYYY HH24:MI:SS') FECINI,
                                       TO_DATE('" & dtpFechaManual.Value.ToShortDateString & " 00:00:00', 'DD/MM/YYYY HH24:MI:SS') FECFIN,
                                       B.CANTID,
                                       B.USR_CREAC,
                                       B.CODEMP,
                                       B.TIPO,
                                       B.CODMON,
                                       B.FECHA_CREAC,
                                       B.FECHA_MODIF,
                                       B.ESTADO from de_conveni A, EN_CLIENTE_COSTO B
                                WHERE A.NUMCOT = " & dgvConReemM.Item("numcot", i).Value.ToString & "
                                  AND A.CODPRO = '" & txtskuOriginalM.Text & "'                              
                                  AND A.CODPRO = B.CODPRO
                                  AND B.ESTADO = 'V'
                                  AND B.RUTCLI = " & dgvConReemM.Item("RUTCLI", i).Value.ToString & "
                                GROUP BY B.CODPRO, B.PRECIO,
                                       B.COSTO, B.MARGEN, B.FECFIN,
                                       B.CANTID, B.USR_CREAC,
                                       B.CODEMP, B.TIPO,
                                       B.CODMON, B.FECHA_CREAC,
                                       B.FECHA_MODIF, B.ESTADO"
                                comando = New OracleCommand(sql, conn)
                                comando.Transaction = transaccion
                                comando.ExecuteNonQuery()
                            Next

                            sql = "Update EN_CLIENTE_COSTO SET TIPO = 0
                            WHERE CODPRO = '" & txtSkuRemM.Text & "'
                              And ESTADO = 'V'
                              And costo < " & costo & "
                              And TIPO <> 0 "
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()

                            MessageBox.Show("Productos Agregados con éxito", "Finalizado", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            transaccion.Commit()
                        ElseIf rbtQuitarM.Checked = True Then
                            If MessageBox.Show("Desea quitar el producto " & txtskuOriginalM.Text & " de los convenios.", "Atención", MessageBoxButtons.YesNo) = 7 Then
                                Exit Sub
                            End If

                            sql = "Set Role Rol_Aplicacion"
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()

                            For i = 0 To dgvConOriM.RowCount - 1
                                sql = "delete de_conveni where numcot = " & dgvConOriM.Item("numcot", i).Value.ToString & " and codpro = '" & txtskuOriginalM.Text.Trim & "'"
                                comando = New OracleCommand(sql, conn)
                                comando.Transaction = transaccion
                                comando.ExecuteNonQuery()
                            Next

                            MessageBox.Show("Productos Eliminados con éxito", "Finalizado", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            transaccion.Commit()

                        ElseIf rbtReemplazarM.Checked = True Then
                            If MessageBox.Show("Desea agregar el producto " & txtSkuRemM.Text & " a los convenios ", "Atención", MessageBoxButtons.YesNo) = 7 Then
                                Exit Sub
                            End If

                            sql = "Set Role Rol_Aplicacion"
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()

                            'RESERVA UN ID DE COSTO ESPECIAL
                            sql = "SELECT EN_CLIENTE_COSTO_SEQ.nextval seq from dual"
                            dt = bd.sqlSelect(sql)

                            secuencia = dt.Rows(0).Item("seq")

                            For i = 0 To dgvConReemM.RowCount - 1
                                sql = " insert into de_conveni(numcot,sequen,codpro,cantid,precio,costos,totnet,pordes, "
                                sql += "mondes,margen,codlis,codlin,codemp,suc,fecemi,fecven) values("
                                sql += dgvConReemM.Item("numcot", i).Value.ToString + ","
                                sql += "0,"
                                sql += "'" + txtSkuRemM.Text + "',"
                                sql += "1,"
                                sql += dgvConReemM.Item("precio", i).Value.ToString + ","
                                sql += dgvConReemM.Item("costos", i).Value.ToString + ","
                                sql += dgvConReemM.Item("precio", i).Value.ToString + ","
                                sql += "0,0,"
                                sql += Math.Round((((dgvConReemM.Item("precio", i).Value - dgvConReemM.Item("costos", i).Value) / dgvConReemM.Item("precio", i).Value) * 100), 2).ToString.Replace(",", ".") + ","
                                sql += "0," + dgvConReemM.Item("codlin", i).Value.ToString + ",3,'" & txtSkuOri.Text & "',"
                                sql += "to_date('" + Now.Date + "','dd/mm/yyyy'),"
                                If dtpFechaManual.Value > dgvConReemM.Item("fecven", i).Value Then
                                    sql += "to_date('" + dgvConReemM.Item("fecven", i).Value.ToString + "','dd/mm/yyyy')"
                                Else
                                    sql += "to_date('" + dtpFechaManual.Value.ToShortDateString + "','dd/mm/yyyy')"
                                End If
                                sql += ") "
                                comando = New OracleCommand(sql, conn)
                                comando.Transaction = transaccion
                                comando.ExecuteNonQuery()

                                'BUSCA COSTO ESPECIAL VIGENTE (RUT - CODIGO)
                                sql = "Select id_numero From EN_CLIENTE_COSTO
                                   Where RUTCLI = " & dgvConReemM.Item("RUTCLI", i).Value.ToString & "
                                     And CODPRO = '" & txtSkuRemM.Text & "'
                                     And ESTADO = 'V'"
                                dt = bd.sqlSelect(sql)

                                'CADUCA COSTO ESPECIAL EXISTENTE
                                If dt.Rows.Count > 0 Then
                                    sql = "UPDATE EN_CLIENTE_COSTO SET FECFIN = (SYSDATE-1), ESTADO = 'C' WHERE RUTCLI = " & dgvConReemM.Item("RUTCLI", i).Value.ToString & " AND ID_NUMERO = " & dt.Rows(0).Item("ID_NUMERO") & " AND CODPRO = '" & txtSkuRemM.Text & "'"
                                    comando = New OracleCommand(sql, conn)
                                    comando.Transaction = transaccion
                                    comando.ExecuteNonQuery()
                                End If

                                sql = "insert into en_cliente_costo
                                   select " & secuencia & " ID_NUMERO,
                                       " & dgvConReemM.Item("RUTCLI", i).Value.ToString & " RUTCLI,
                                       '" & txtSkuRemM.Text & "' CODPRO,
                                       B.PRECIO,
                                       B.COSTO,
                                       B.MARGEN,
                                       TO_DATE('" & Date.Now.ToShortDateString & " 00:00:00', 'DD/MM/YYYY HH24:MI:SS') FECINI,
                                       TO_DATE('" & dtpFechaManual.Value.ToShortDateString & " 00:00:00', 'DD/MM/YYYY HH24:MI:SS') FECFIN,
                                       B.CANTID,
                                       B.USR_CREAC,
                                       B.CODEMP,
                                       B.TIPO,
                                       B.CODMON,
                                       B.FECHA_CREAC,
                                       B.FECHA_MODIF,
                                       B.ESTADO from de_conveni A, EN_CLIENTE_COSTO B
                                WHERE A.NUMCOT = " & dgvConReemM.Item("numcot", i).Value.ToString & "
                                  AND A.CODPRO = '" & txtskuOriginalM.Text & "'                              
                                  AND A.CODPRO = B.CODPRO
                                  AND B.ESTADO = 'V'
                                  AND B.RUTCLI = " & dgvConReemM.Item("RUTCLI", i).Value.ToString & "
                                GROUP BY B.CODPRO, B.PRECIO,
                                       B.COSTO, B.MARGEN, B.FECFIN,
                                       B.CANTID, B.USR_CREAC,
                                       B.CODEMP, B.TIPO,
                                       B.CODMON, B.FECHA_CREAC,
                                       B.FECHA_MODIF, B.ESTADO"
                                comando = New OracleCommand(sql, conn)
                                comando.Transaction = transaccion
                                comando.ExecuteNonQuery()
                            Next

                            sql = "Update EN_CLIENTE_COSTO SET TIPO = 0
                            WHERE CODPRO = '" & txtSkuRemM.Text & "'
                              And ESTADO = 'V'
                              And costo < " & costo & "
                              And TIPO <> 0 "
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()

                            For i = 0 To dgvConOriM.RowCount - 1
                                sql = "delete de_conveni where numcot = " & dgvConOriM.Item("numcot", i).Value.ToString & " and codpro = '" & txtskuOriginalM.Text.Trim & "'"
                                comando = New OracleCommand(sql, conn)
                                comando.Transaction = transaccion
                                comando.ExecuteNonQuery()
                            Next

                            MessageBox.Show("Productos Agregados con éxito", "Finalizado", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            transaccion.Commit()
                        End If
                    End If

                Catch ex As Exception
                    If Not transaccion.Equals(Nothing) Then
                        transaccion.Rollback()
                    End If
                    PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                    MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Finally
                    conn.Close()
                    btnProcesarManual.Enabled = True
                End Try
            End Using
        End Using
    End Sub

    Private Sub txtSkuElimina_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtSkuElimina.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            For i = 0 To dgvProductoEliminar.RowCount - 1
                If dgvProductoEliminar.Item("Codigo", i).Value = txtSkuElimina.Text Then
                    dgvProductoEliminar.Rows(i).Selected = True
                    dgvProductoEliminar.CurrentCell = Me.dgvProductoEliminar.Item(0, i)
                    dgvProductoEliminar.FirstDisplayedCell = dgvProductoEliminar.CurrentCell
                    Exit Sub
                End If
            Next
        End If
    End Sub
End Class