﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MantenedorListasPrecios
    Inherits MetroFramework.Forms.MetroForm   'System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MantenedorListasPrecios))
        Me.dgvListaCodigo = New System.Windows.Forms.DataGridView()
        Me.dgvCodigoLista = New System.Windows.Forms.DataGridView()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.btnCorrigeNegativos = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.btnImportar = New System.Windows.Forms.Button()
        Me.btnExportar2 = New System.Windows.Forms.Button()
        Me.btnExportar1 = New System.Windows.Forms.Button()
        Me.btnGuardarImpor = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.sdc = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.cbxCodigos = New System.Windows.Forms.CheckBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtCodigo = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtPrecioCodigo = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtMargenCodigo = New System.Windows.Forms.TextBox()
        Me.txtPrecioMinCodigo = New System.Windows.Forms.TextBox()
        Me.txtMargenMinCodigo = New System.Windows.Forms.TextBox()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.cmbListas = New System.Windows.Forms.ComboBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtCodLista = New System.Windows.Forms.TextBox()
        Me.txtPrecioLista = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtMargenLista = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtPrecioMinLista = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtMargenMinLista = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.dgvCambiosListas = New System.Windows.Forms.DataGridView()
        CType(Me.dgvListaCodigo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvCodigoLista, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.sdc.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        CType(Me.dgvCambiosListas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvListaCodigo
        '
        Me.dgvListaCodigo.AllowUserToAddRows = False
        Me.dgvListaCodigo.AllowUserToDeleteRows = False
        Me.dgvListaCodigo.AllowUserToResizeColumns = False
        Me.dgvListaCodigo.AllowUserToResizeRows = False
        Me.dgvListaCodigo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvListaCodigo.Location = New System.Drawing.Point(13, 66)
        Me.dgvListaCodigo.Name = "dgvListaCodigo"
        Me.dgvListaCodigo.ReadOnly = True
        Me.dgvListaCodigo.RowHeadersVisible = False
        Me.dgvListaCodigo.Size = New System.Drawing.Size(837, 344)
        Me.dgvListaCodigo.TabIndex = 4
        '
        'dgvCodigoLista
        '
        Me.dgvCodigoLista.AllowUserToAddRows = False
        Me.dgvCodigoLista.AllowUserToDeleteRows = False
        Me.dgvCodigoLista.AllowUserToResizeColumns = False
        Me.dgvCodigoLista.AllowUserToResizeRows = False
        Me.dgvCodigoLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCodigoLista.Location = New System.Drawing.Point(6, 72)
        Me.dgvCodigoLista.Name = "dgvCodigoLista"
        Me.dgvCodigoLista.ReadOnly = True
        Me.dgvCodigoLista.RowHeadersVisible = False
        Me.dgvCodigoLista.Size = New System.Drawing.Size(828, 338)
        Me.dgvCodigoLista.TabIndex = 5
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(13, 50)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(90, 13)
        Me.Label9.TabIndex = 8
        Me.Label9.Text = "Listas del Codigo "
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(6, 56)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(99, 13)
        Me.Label10.TabIndex = 9
        Me.Label10.Text = "Codigos de la Lista "
        '
        'btnCorrigeNegativos
        '
        Me.btnCorrigeNegativos.Image = CType(resources.GetObject("btnCorrigeNegativos.Image"), System.Drawing.Image)
        Me.btnCorrigeNegativos.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnCorrigeNegativos.Location = New System.Drawing.Point(13, 416)
        Me.btnCorrigeNegativos.Name = "btnCorrigeNegativos"
        Me.btnCorrigeNegativos.Size = New System.Drawing.Size(106, 53)
        Me.btnCorrigeNegativos.TabIndex = 37
        Me.btnCorrigeNegativos.Text = "Corrige Negativos"
        Me.btnCorrigeNegativos.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnCorrigeNegativos.UseVisualStyleBackColor = True
        Me.btnCorrigeNegativos.Visible = False
        '
        'btnGuardar
        '
        Me.btnGuardar.Image = Global.Convenios_New.My.Resources.Resources.Floppy
        Me.btnGuardar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnGuardar.Location = New System.Drawing.Point(744, 416)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(106, 53)
        Me.btnGuardar.TabIndex = 8
        Me.btnGuardar.Text = "Confirmar Cambios"
        Me.btnGuardar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(365, 24)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(451, 23)
        Me.ProgressBar1.TabIndex = 13
        Me.ProgressBar1.Visible = False
        '
        'btnImportar
        '
        Me.btnImportar.Image = CType(resources.GetObject("btnImportar.Image"), System.Drawing.Image)
        Me.btnImportar.Location = New System.Drawing.Point(707, 415)
        Me.btnImportar.Name = "btnImportar"
        Me.btnImportar.Size = New System.Drawing.Size(53, 53)
        Me.btnImportar.TabIndex = 38
        Me.btnImportar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnImportar.UseVisualStyleBackColor = True
        '
        'btnExportar2
        '
        Me.btnExportar2.Image = CType(resources.GetObject("btnExportar2.Image"), System.Drawing.Image)
        Me.btnExportar2.Location = New System.Drawing.Point(669, 416)
        Me.btnExportar2.Name = "btnExportar2"
        Me.btnExportar2.Size = New System.Drawing.Size(53, 53)
        Me.btnExportar2.TabIndex = 39
        Me.btnExportar2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnExportar2.UseVisualStyleBackColor = True
        '
        'btnExportar1
        '
        Me.btnExportar1.Image = CType(resources.GetObject("btnExportar1.Image"), System.Drawing.Image)
        Me.btnExportar1.Location = New System.Drawing.Point(685, 416)
        Me.btnExportar1.Name = "btnExportar1"
        Me.btnExportar1.Size = New System.Drawing.Size(53, 53)
        Me.btnExportar1.TabIndex = 40
        Me.btnExportar1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnExportar1.UseVisualStyleBackColor = True
        '
        'btnGuardarImpor
        '
        Me.btnGuardarImpor.Image = Global.Convenios_New.My.Resources.Resources.Floppy
        Me.btnGuardarImpor.Location = New System.Drawing.Point(841, 20)
        Me.btnGuardarImpor.Name = "btnGuardarImpor"
        Me.btnGuardarImpor.Size = New System.Drawing.Size(53, 49)
        Me.btnGuardarImpor.TabIndex = 41
        Me.btnGuardarImpor.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnGuardarImpor.UseVisualStyleBackColor = True
        Me.btnGuardarImpor.Visible = False
        '
        'sdc
        '
        Me.sdc.Controls.Add(Me.TabPage1)
        Me.sdc.Controls.Add(Me.TabPage2)
        Me.sdc.Controls.Add(Me.TabPage3)
        Me.sdc.Location = New System.Drawing.Point(6, 53)
        Me.sdc.Name = "sdc"
        Me.sdc.SelectedIndex = 0
        Me.sdc.Size = New System.Drawing.Size(895, 501)
        Me.sdc.TabIndex = 43
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.cbxCodigos)
        Me.TabPage1.Controls.Add(Me.btnCorrigeNegativos)
        Me.TabPage1.Controls.Add(Me.Label15)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Controls.Add(Me.Label4)
        Me.TabPage1.Controls.Add(Me.txtCodigo)
        Me.TabPage1.Controls.Add(Me.Label3)
        Me.TabPage1.Controls.Add(Me.txtPrecioCodigo)
        Me.TabPage1.Controls.Add(Me.btnGuardar)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.txtMargenCodigo)
        Me.TabPage1.Controls.Add(Me.txtPrecioMinCodigo)
        Me.TabPage1.Controls.Add(Me.txtMargenMinCodigo)
        Me.TabPage1.Controls.Add(Me.dgvListaCodigo)
        Me.TabPage1.Controls.Add(Me.btnExportar1)
        Me.TabPage1.Controls.Add(Me.Label9)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(887, 475)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Cambio Codigo"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'cbxCodigos
        '
        Me.cbxCodigos.AutoSize = True
        Me.cbxCodigos.Location = New System.Drawing.Point(748, 46)
        Me.cbxCodigos.Name = "cbxCodigos"
        Me.cbxCodigos.Size = New System.Drawing.Size(102, 17)
        Me.cbxCodigos.TabIndex = 51
        Me.cbxCodigos.Text = "Todas las Listas"
        Me.cbxCodigos.UseVisualStyleBackColor = True
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(10, 8)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(46, 13)
        Me.Label15.TabIndex = 50
        Me.Label15.Text = "Codigo"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(146, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(37, 13)
        Me.Label1.TabIndex = 46
        Me.Label1.Text = "Precio"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(531, 32)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(66, 13)
        Me.Label4.TabIndex = 49
        Me.Label4.Text = "Margen Min."
        '
        'txtCodigo
        '
        Me.txtCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodigo.Location = New System.Drawing.Point(13, 27)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.Size = New System.Drawing.Size(100, 20)
        Me.txtCodigo.TabIndex = 41
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(403, 32)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(60, 13)
        Me.Label3.TabIndex = 48
        Me.Label3.Text = "Precio Min."
        '
        'txtPrecioCodigo
        '
        Me.txtPrecioCodigo.Location = New System.Drawing.Point(189, 29)
        Me.txtPrecioCodigo.Name = "txtPrecioCodigo"
        Me.txtPrecioCodigo.Size = New System.Drawing.Size(64, 20)
        Me.txtPrecioCodigo.TabIndex = 42
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(270, 32)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(43, 13)
        Me.Label2.TabIndex = 47
        Me.Label2.Text = "Margen"
        '
        'txtMargenCodigo
        '
        Me.txtMargenCodigo.Location = New System.Drawing.Point(319, 29)
        Me.txtMargenCodigo.Name = "txtMargenCodigo"
        Me.txtMargenCodigo.Size = New System.Drawing.Size(64, 20)
        Me.txtMargenCodigo.TabIndex = 43
        '
        'txtPrecioMinCodigo
        '
        Me.txtPrecioMinCodigo.Location = New System.Drawing.Point(464, 29)
        Me.txtPrecioMinCodigo.Name = "txtPrecioMinCodigo"
        Me.txtPrecioMinCodigo.Size = New System.Drawing.Size(64, 20)
        Me.txtPrecioMinCodigo.TabIndex = 44
        '
        'txtMargenMinCodigo
        '
        Me.txtMargenMinCodigo.Location = New System.Drawing.Point(597, 29)
        Me.txtMargenMinCodigo.Name = "txtMargenMinCodigo"
        Me.txtMargenMinCodigo.Size = New System.Drawing.Size(64, 20)
        Me.txtMargenMinCodigo.TabIndex = 45
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.Button1)
        Me.TabPage2.Controls.Add(Me.cmbListas)
        Me.TabPage2.Controls.Add(Me.Label16)
        Me.TabPage2.Controls.Add(Me.Label14)
        Me.TabPage2.Controls.Add(Me.txtCodLista)
        Me.TabPage2.Controls.Add(Me.txtPrecioLista)
        Me.TabPage2.Controls.Add(Me.Label5)
        Me.TabPage2.Controls.Add(Me.txtMargenLista)
        Me.TabPage2.Controls.Add(Me.Label6)
        Me.TabPage2.Controls.Add(Me.txtPrecioMinLista)
        Me.TabPage2.Controls.Add(Me.Label7)
        Me.TabPage2.Controls.Add(Me.txtMargenMinLista)
        Me.TabPage2.Controls.Add(Me.Label8)
        Me.TabPage2.Controls.Add(Me.Label10)
        Me.TabPage2.Controls.Add(Me.dgvCodigoLista)
        Me.TabPage2.Controls.Add(Me.btnExportar2)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(887, 475)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Cambio Lista"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Image = Global.Convenios_New.My.Resources.Resources.Floppy
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button1.Location = New System.Drawing.Point(728, 416)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(106, 53)
        Me.Button1.TabIndex = 55
        Me.Button1.Text = "Confirmar Cambios"
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button1.UseVisualStyleBackColor = True
        '
        'cmbListas
        '
        Me.cmbListas.FormattingEnabled = True
        Me.cmbListas.Location = New System.Drawing.Point(14, 27)
        Me.cmbListas.Name = "cmbListas"
        Me.cmbListas.Size = New System.Drawing.Size(107, 21)
        Me.cmbListas.TabIndex = 52
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(12, 11)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(40, 13)
        Me.Label16.TabIndex = 51
        Me.Label16.Text = "Listas"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(124, 30)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(29, 13)
        Me.Label14.TabIndex = 50
        Me.Label14.Text = "Cod."
        '
        'txtCodLista
        '
        Me.txtCodLista.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodLista.Location = New System.Drawing.Point(155, 27)
        Me.txtCodLista.Name = "txtCodLista"
        Me.txtCodLista.Size = New System.Drawing.Size(100, 20)
        Me.txtCodLista.TabIndex = 49
        '
        'txtPrecioLista
        '
        Me.txtPrecioLista.Location = New System.Drawing.Point(322, 27)
        Me.txtPrecioLista.Name = "txtPrecioLista"
        Me.txtPrecioLista.Size = New System.Drawing.Size(64, 20)
        Me.txtPrecioLista.TabIndex = 40
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(666, 31)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(66, 13)
        Me.Label5.TabIndex = 47
        Me.Label5.Text = "Margen Min."
        '
        'txtMargenLista
        '
        Me.txtMargenLista.Location = New System.Drawing.Point(452, 27)
        Me.txtMargenLista.Name = "txtMargenLista"
        Me.txtMargenLista.Size = New System.Drawing.Size(64, 20)
        Me.txtMargenLista.TabIndex = 41
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(531, 31)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(60, 13)
        Me.Label6.TabIndex = 46
        Me.Label6.Text = "Precio Min."
        '
        'txtPrecioMinLista
        '
        Me.txtPrecioMinLista.Location = New System.Drawing.Point(597, 28)
        Me.txtPrecioMinLista.Name = "txtPrecioMinLista"
        Me.txtPrecioMinLista.Size = New System.Drawing.Size(64, 20)
        Me.txtPrecioMinLista.TabIndex = 42
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(403, 30)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(43, 13)
        Me.Label7.TabIndex = 45
        Me.Label7.Text = "Margen"
        '
        'txtMargenMinLista
        '
        Me.txtMargenMinLista.Location = New System.Drawing.Point(732, 28)
        Me.txtMargenMinLista.Name = "txtMargenMinLista"
        Me.txtMargenMinLista.Size = New System.Drawing.Size(64, 20)
        Me.txtMargenMinLista.TabIndex = 43
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(279, 30)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(37, 13)
        Me.Label8.TabIndex = 44
        Me.Label8.Text = "Precio"
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.Button2)
        Me.TabPage3.Controls.Add(Me.Label17)
        Me.TabPage3.Controls.Add(Me.dgvCambiosListas)
        Me.TabPage3.Controls.Add(Me.btnImportar)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(887, 475)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Carga Masiva"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Image = Global.Convenios_New.My.Resources.Resources.Floppy
        Me.Button2.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button2.Location = New System.Drawing.Point(766, 415)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(106, 53)
        Me.Button2.TabIndex = 53
        Me.Button2.Text = "Confirmar Cambios"
        Me.Button2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(13, 14)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(680, 17)
        Me.Label17.TabIndex = 42
        Me.Label17.Text = "Formato: ( LISTA - CODIGO - PRECIO - MARGEN - DESCTO - MARGEN_MIN - PRECIO_MIN )"
        '
        'dgvCambiosListas
        '
        Me.dgvCambiosListas.AllowUserToAddRows = False
        Me.dgvCambiosListas.AllowUserToDeleteRows = False
        Me.dgvCambiosListas.AllowUserToResizeColumns = False
        Me.dgvCambiosListas.AllowUserToResizeRows = False
        Me.dgvCambiosListas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCambiosListas.Location = New System.Drawing.Point(13, 40)
        Me.dgvCambiosListas.Name = "dgvCambiosListas"
        Me.dgvCambiosListas.ReadOnly = True
        Me.dgvCambiosListas.RowHeadersVisible = False
        Me.dgvCambiosListas.Size = New System.Drawing.Size(859, 366)
        Me.dgvCambiosListas.TabIndex = 10
        '
        'MantenedorListasPrecios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(909, 559)
        Me.Controls.Add(Me.btnGuardarImpor)
        Me.Controls.Add(Me.sdc)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Name = "MantenedorListasPrecios"
        Me.Text = "Mantenedor de Listas de Precios"
        CType(Me.dgvListaCodigo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvCodigoLista, System.ComponentModel.ISupportInitialize).EndInit()
        Me.sdc.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        CType(Me.dgvCambiosListas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents dgvListaCodigo As DataGridView
    Friend WithEvents dgvCodigoLista As DataGridView
    Friend WithEvents Label9 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents btnGuardar As Button
    Friend WithEvents ProgressBar1 As ProgressBar
    Friend WithEvents btnCorrigeNegativos As Button
    Friend WithEvents btnImportar As Button
    Friend WithEvents btnExportar2 As Button
    Friend WithEvents btnExportar1 As Button
    Friend WithEvents btnGuardarImpor As Button
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents sdc As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents Label15 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents txtCodigo As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txtPrecioCodigo As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents txtMargenCodigo As TextBox
    Friend WithEvents txtPrecioMinCodigo As TextBox
    Friend WithEvents txtMargenMinCodigo As TextBox
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents cmbListas As ComboBox
    Friend WithEvents Label16 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents txtCodLista As TextBox
    Friend WithEvents txtPrecioLista As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents txtMargenLista As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents txtPrecioMinLista As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents txtMargenMinLista As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents Label17 As Label
    Friend WithEvents dgvCambiosListas As DataGridView
    Friend WithEvents cbxCodigos As CheckBox
End Class
