﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class creaCostoEsp
    Inherits MetroFramework.Forms.MetroForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.txtRutcli = New System.Windows.Forms.TextBox()
        Me.txtCosto = New System.Windows.Forms.TextBox()
        Me.txtPrecio = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblDespro = New System.Windows.Forms.Label()
        Me.lblPrecio = New System.Windows.Forms.Label()
        Me.dtDFecini = New System.Windows.Forms.DateTimePicker()
        Me.dtFecfin = New System.Windows.Forms.DateTimePicker()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Codpro = New System.Windows.Forms.Label()
        Me.txtCodpro = New System.Windows.Forms.TextBox()
        Me.cmbTipo = New System.Windows.Forms.ComboBox()
        Me.lblTipo = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.label10 = New System.Windows.Forms.Label()
        Me.lblRazons = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.lblCosprom = New System.Windows.Forms.Label()
        Me.lblCoscom = New System.Windows.Forms.Label()
        Me.lblStock = New System.Windows.Forms.Label()
        Me.lblEstado = New System.Windows.Forms.Label()
        Me.lblMargen = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'btnGuardar
        '
        Me.btnGuardar.BackColor = System.Drawing.Color.BlueViolet
        Me.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGuardar.ForeColor = System.Drawing.Color.White
        Me.btnGuardar.Location = New System.Drawing.Point(53, 530)
        Me.btnGuardar.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(87, 30)
        Me.btnGuardar.TabIndex = 0
        Me.btnGuardar.Text = "Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = False
        '
        'btnCancelar
        '
        Me.btnCancelar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancelar.ForeColor = System.Drawing.Color.White
        Me.btnCancelar.Location = New System.Drawing.Point(178, 530)
        Me.btnCancelar.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(160, 30)
        Me.btnCancelar.TabIndex = 1
        Me.btnCancelar.Text = "Grabar en Relacionados"
        Me.btnCancelar.UseVisualStyleBackColor = False
        '
        'txtRutcli
        '
        Me.txtRutcli.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtRutcli.Location = New System.Drawing.Point(105, 56)
        Me.txtRutcli.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtRutcli.Name = "txtRutcli"
        Me.txtRutcli.Size = New System.Drawing.Size(138, 25)
        Me.txtRutcli.TabIndex = 2
        '
        'txtCosto
        '
        Me.txtCosto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCosto.Location = New System.Drawing.Point(105, 319)
        Me.txtCosto.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtCosto.Name = "txtCosto"
        Me.txtCosto.Size = New System.Drawing.Size(92, 25)
        Me.txtCosto.TabIndex = 3
        '
        'txtPrecio
        '
        Me.txtPrecio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtPrecio.Location = New System.Drawing.Point(105, 284)
        Me.txtPrecio.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtPrecio.Name = "txtPrecio"
        Me.txtPrecio.Size = New System.Drawing.Size(141, 25)
        Me.txtPrecio.TabIndex = 4
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(17, 56)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(32, 17)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "RUT"
        '
        'lblDespro
        '
        Me.lblDespro.AutoSize = True
        Me.lblDespro.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.lblDespro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblDespro.Location = New System.Drawing.Point(105, 141)
        Me.lblDespro.Name = "lblDespro"
        Me.lblDespro.Size = New System.Drawing.Size(2, 19)
        Me.lblDespro.TabIndex = 6
        '
        'lblPrecio
        '
        Me.lblPrecio.AutoSize = True
        Me.lblPrecio.Location = New System.Drawing.Point(17, 284)
        Me.lblPrecio.Name = "lblPrecio"
        Me.lblPrecio.Size = New System.Drawing.Size(51, 17)
        Me.lblPrecio.TabIndex = 7
        Me.lblPrecio.Text = "PRECIO"
        '
        'dtDFecini
        '
        Me.dtDFecini.Location = New System.Drawing.Point(105, 421)
        Me.dtDFecini.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.dtDFecini.Name = "dtDFecini"
        Me.dtDFecini.Size = New System.Drawing.Size(250, 25)
        Me.dtDFecini.TabIndex = 8
        '
        'dtFecfin
        '
        Me.dtFecfin.Location = New System.Drawing.Point(105, 468)
        Me.dtFecfin.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.dtFecfin.Name = "dtFecfin"
        Me.dtFecfin.Size = New System.Drawing.Size(250, 25)
        Me.dtFecfin.TabIndex = 9
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(17, 421)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(47, 17)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "DESDE"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(18, 468)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(46, 17)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "HASTA"
        '
        'Codpro
        '
        Me.Codpro.AutoSize = True
        Me.Codpro.Location = New System.Drawing.Point(15, 113)
        Me.Codpro.Name = "Codpro"
        Me.Codpro.Size = New System.Drawing.Size(60, 17)
        Me.Codpro.TabIndex = 12
        Me.Codpro.Text = "CODPRO"
        '
        'txtCodpro
        '
        Me.txtCodpro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCodpro.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodpro.Location = New System.Drawing.Point(105, 111)
        Me.txtCodpro.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtCodpro.Name = "txtCodpro"
        Me.txtCodpro.Size = New System.Drawing.Size(138, 25)
        Me.txtCodpro.TabIndex = 13
        '
        'cmbTipo
        '
        Me.cmbTipo.FormattingEnabled = True
        Me.cmbTipo.Location = New System.Drawing.Point(105, 376)
        Me.cmbTipo.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.cmbTipo.Name = "cmbTipo"
        Me.cmbTipo.Size = New System.Drawing.Size(189, 25)
        Me.cmbTipo.TabIndex = 14
        '
        'lblTipo
        '
        Me.lblTipo.AutoSize = True
        Me.lblTipo.Location = New System.Drawing.Point(17, 384)
        Me.lblTipo.Name = "lblTipo"
        Me.lblTipo.Size = New System.Drawing.Size(35, 17)
        Me.lblTipo.TabIndex = 15
        Me.lblTipo.Text = "TIPO"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(17, 319)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(49, 17)
        Me.Label2.TabIndex = 16
        Me.Label2.Text = "COSTO"
        '
        'label10
        '
        Me.label10.AutoSize = True
        Me.label10.Location = New System.Drawing.Point(17, 354)
        Me.label10.Name = "label10"
        Me.label10.Size = New System.Drawing.Size(62, 17)
        Me.label10.TabIndex = 17
        Me.label10.Text = "MARGEN"
        '
        'lblRazons
        '
        Me.lblRazons.AutoSize = True
        Me.lblRazons.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblRazons.Location = New System.Drawing.Point(105, 85)
        Me.lblRazons.Name = "lblRazons"
        Me.lblRazons.Size = New System.Drawing.Size(66, 19)
        Me.lblRazons.TabIndex = 18
        Me.lblRazons.Text = "lblRazons"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(15, 167)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(70, 17)
        Me.Label3.TabIndex = 19
        Me.Label3.Text = "COSPROM"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(15, 196)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(63, 17)
        Me.Label6.TabIndex = 20
        Me.Label6.Text = "COSCOM"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(17, 223)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(47, 17)
        Me.Label7.TabIndex = 21
        Me.Label7.Text = "STOCK"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(15, 252)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(55, 17)
        Me.Label8.TabIndex = 22
        Me.Label8.Text = "ESTADO"
        '
        'lblCosprom
        '
        Me.lblCosprom.AutoSize = True
        Me.lblCosprom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblCosprom.Location = New System.Drawing.Point(105, 167)
        Me.lblCosprom.Name = "lblCosprom"
        Me.lblCosprom.Size = New System.Drawing.Size(2, 19)
        Me.lblCosprom.TabIndex = 23
        '
        'lblCoscom
        '
        Me.lblCoscom.AutoSize = True
        Me.lblCoscom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblCoscom.Location = New System.Drawing.Point(105, 196)
        Me.lblCoscom.Name = "lblCoscom"
        Me.lblCoscom.Size = New System.Drawing.Size(2, 19)
        Me.lblCoscom.TabIndex = 24
        '
        'lblStock
        '
        Me.lblStock.AutoSize = True
        Me.lblStock.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblStock.Location = New System.Drawing.Point(105, 223)
        Me.lblStock.Name = "lblStock"
        Me.lblStock.Size = New System.Drawing.Size(2, 19)
        Me.lblStock.TabIndex = 25
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblEstado.Location = New System.Drawing.Point(105, 252)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(2, 19)
        Me.lblEstado.TabIndex = 26
        '
        'lblMargen
        '
        Me.lblMargen.AutoSize = True
        Me.lblMargen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblMargen.Location = New System.Drawing.Point(105, 352)
        Me.lblMargen.Name = "lblMargen"
        Me.lblMargen.Size = New System.Drawing.Size(2, 19)
        Me.lblMargen.TabIndex = 27
        '
        'creaCostoEsp
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(382, 582)
        Me.Controls.Add(Me.lblMargen)
        Me.Controls.Add(Me.lblEstado)
        Me.Controls.Add(Me.lblStock)
        Me.Controls.Add(Me.lblCoscom)
        Me.Controls.Add(Me.lblCosprom)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lblRazons)
        Me.Controls.Add(Me.label10)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblTipo)
        Me.Controls.Add(Me.cmbTipo)
        Me.Controls.Add(Me.txtCodpro)
        Me.Controls.Add(Me.Codpro)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.dtFecfin)
        Me.Controls.Add(Me.dtDFecini)
        Me.Controls.Add(Me.lblPrecio)
        Me.Controls.Add(Me.lblDespro)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtPrecio)
        Me.Controls.Add(Me.txtCosto)
        Me.Controls.Add(Me.txtRutcli)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "creaCostoEsp"
        Me.ShowIcon = False
        Me.Text = "Creación Costo Especial"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnGuardar As Button
    Friend WithEvents btnCancelar As Button
    Friend WithEvents txtRutcli As TextBox
    Friend WithEvents txtCosto As TextBox
    Friend WithEvents txtPrecio As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents lblDespro As Label
    Friend WithEvents lblPrecio As Label
    Friend WithEvents dtDFecini As DateTimePicker
    Friend WithEvents dtFecfin As DateTimePicker
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Codpro As Label
    Friend WithEvents txtCodpro As TextBox
    Friend WithEvents cmbTipo As ComboBox
    Friend WithEvents lblTipo As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents label10 As Label
    Friend WithEvents lblRazons As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents lblCosprom As Label
    Friend WithEvents lblCoscom As Label
    Friend WithEvents lblStock As Label
    Friend WithEvents lblEstado As Label
    Friend WithEvents lblMargen As Label
End Class
