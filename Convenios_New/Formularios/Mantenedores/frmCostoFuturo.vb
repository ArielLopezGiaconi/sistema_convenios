﻿Imports System.Data.OracleClient

Public Class frmCostoFuturo
    Dim dtpaso, dtpifias As New DataTable
    Dim flag As Boolean = False
    Dim bd As New Conexion
    Dim total As Integer = 0
    Dim PuntoControl As New PuntoControl
    Private Sub frmCostoFuturo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.StartPosition = FormStartPosition.Manual
        Me.Location = New Point(0, 0)
        PuntoControl.RegistroUso("150")
        cargaGrilla()
    End Sub
    Private Sub cargaGrilla()
        Try
            bd.open_dimerc()

            Dim SQL = " select codpro ""Codigo"", GETDESCRIPCION(codpro) ""Descripción"","
            SQL = SQL & "alza ""Porcentaje Alza"", fecini ""Fecha Inicio"",fecfin ""Fecha Termino"" "
            SQL = SQL & "from ma_costo_futuro order by codpro"
            dgvProductos.DataSource = bd.sqlSelect(SQL)
            dgvProductos.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells

            dgvProductos.Columns("Porcentaje Alza").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvProductos.Columns("Porcentaje Alza").DefaultCellStyle.Format = "n2"
            If flag = False Then
                Dim check As New DataGridViewCheckBoxColumn
                check.HeaderText = "¿Elimina?"
                check.Name = "¿Elimina?"
                dgvProductos.Columns.Insert(0, check)
                dgvProductos.Columns(0).Visible = True
                flag = True
            End If

        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles btn_Elimina.Click
        If MessageBox.Show("¿Esta seguro de eliminar los productos marcados?", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            Using conn = New OracleConnection(Conexion.retornaConexion)
                conn.Open()
                Dim comando As OracleCommand
                Dim sql As String
                PuntoControl.RegistroUso("152")
                Using transaccion = conn.BeginTransaction
                    Try
                        For i = 0 To dgvProductos.RowCount - 1
                            If dgvProductos.Item("¿Elimina?", i).Value = 1 Then
                                sql = " delete from ma_costo_futuro "
                                sql += " where codpro='" + dgvProductos.Item("Codigo", i).Value + "'"
                                sql += " and fecini=to_date('" + dgvProductos.Item("Fecha Inicio", i).Value + "','dd/mm/yyyy')"
                                sql += " and fecfin=to_date('" + dgvProductos.Item("Fecha Termino", i).Value + "','dd/mm/yyyy')"
                                comando = New OracleCommand(sql, conn)
                                comando.Transaction = transaccion
                                comando.ExecuteNonQuery()
                            End If
                        Next
                        transaccion.Commit()

                        cargaGrilla()

                        MessageBox.Show("Productos eliminados con éxito", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Catch ex As Exception
                        If Not transaccion.Equals(Nothing) Then
                            transaccion.Rollback()
                        End If
                        PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                        MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Finally
                        bd.close()
                    End Try
                End Using
            End Using
        End If

    End Sub

    Private Sub btn_Excel_Click(sender As Object, e As EventArgs) Handles btn_Excel.Click
        PuntoControl.RegistroUso("151")
        Dim save As New SaveFileDialog
        If dgvProductos.RowCount = 0 Then
            MessageBox.Show("No hay datos consultados en la grilla, procese datos antes de exportar", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        Else
            save.Filter = "Archivo Excel | *.xlsx"
            If save.ShowDialog = System.Windows.Forms.DialogResult.OK Then
                Globales.exporta(save.FileName, dgvProductos)
            End If
        End If

    End Sub

    Private Sub txtDescripcion_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDescripcion.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If txtDescripcion.Text.Trim <> "" Then
                Dim frmBuscar As New frmBuscarProducto(txtDescripcion.Text, False)
                frmBuscar.ShowDialog()
                If Not frmBuscar.dgvDatos.CurrentRow Is Nothing And frmBuscar.ok = True Then
                    txtDescripcion.Text = frmBuscar.dgvDatos.Item("Descripcion", frmBuscar.dgvDatos.CurrentRow.Index).Value.ToString
                    txtCodpro.Text = frmBuscar.dgvDatos.Item("Codigo", frmBuscar.dgvDatos.CurrentRow.Index).Value.ToString
                    txtCosto.Focus()
                End If
            End If
        End If
    End Sub

    Private Sub txtCosto_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCosto.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Trim(txtCosto.Text) <> "," Then
                txtCosto.Text = Format(CDec(txtCosto.Text), "N2")
                dtpFecini.Focus()
            End If
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub btn_Agregar_Click(sender As Object, e As EventArgs) Handles btn_Agregar.Click
        If dtpFecFin.Value.Date < dtpFecini.Value.Date Then
            MessageBox.Show("Fecha de inicio no puede ser mayor a la fecha de termino", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        PuntoControl.RegistroUso("153")
        Using conn = New OracleConnection(Conexion.retornaConexion)
            conn.Open()
            Dim comando As OracleCommand
            Dim dataAdapter As OracleDataAdapter
            Dim sql As String
            Dim dt As New DataTable
            Using transaccion = conn.BeginTransaction
                Try
                    sql = "  SELECT B.codpro FROM ma_costo_futuro B  "
                    sql = sql & "WHERE B.codpro= '" + txtCodpro.Text + "' "
                    sql = sql & "  and B.codemp= 3 "
                    sql = sql & "  and (  "
                    sql = sql & "        (to_date('" + dtpFecini.Value.Date + "','DD/MM/YYYY') between B.FECINI AND B.FECFIN)  "
                    sql = sql & "     or (to_date('" + dtpFecFin.Value.Date + "','DD/MM/YYYY') between B.FECINI AND B.FECFIN)  "
                    sql = sql & "     or (B.FECINI between to_date('" + dtpFecini.Value.Date + "','DD/MM/YYYY') and to_date('" + dtpFecFin.Value.Date + "','DD/MM/YYYY'))  "
                    sql = sql & "     or (B.FECFIN between to_date('" + dtpFecini.Value.Date + "','DD/MM/YYYY') and to_date('" + dtpFecFin.Value.Date + "','DD/MM/YYYY'))  "
                    sql = sql & "      )  "
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    dataAdapter = New OracleDataAdapter(comando)
                    dataAdapter.Fill(dt)
                    If dt.Rows.Count > 0 Then
                        MessageBox.Show("Producto ya esta agregado en esa fecha, puede eliminarlo, o agregarlo en una fecha diferente", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Exit Sub
                    Else
                        sql = "select nvl(cosprom,0) cosprom from ma_product where codpro = '" & txtCodpro.Text & "'"
                        comando = New OracleCommand(sql, conn)
                        comando.Transaction = transaccion
                        dataAdapter = New OracleDataAdapter(comando)
                        dataAdapter.Fill(dt)

                        sql = " insert into ma_costo_futuro(codemp,codpro,alza,cosfut,anterior,fecini,fecfin)  "
                        sql = sql & "values (3,'" + txtCodpro.Text + "',"
                        sql += "'" + txtCosto.Text.Replace(",", ".") + "'," & dt.Rows(0).Item("cosprom") * ((100 + txtCosto.Text.Replace(",", ".")) / 100) & "," & dt.Rows(0).Item("cosprom") & ","
                        sql += " to_date('" + dtpFecini.Value.Date + "','dd/mm/yyyy'),"
                        sql += " to_date('" + dtpFecFin.Value.Date + "','dd/mm/yyyy')"
                        sql += ") "
                        comando = New OracleCommand(sql, conn)
                        comando.Transaction = transaccion
                        comando.ExecuteNonQuery()
                    End If

                    transaccion.Commit()

                    MessageBox.Show("Datos agregados con éxito", "Listo", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    cargaGrilla()
                Catch ex As Exception
                    If Not transaccion.Equals(Nothing) Then
                        transaccion.Rollback()
                    End If
                    PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                    MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Finally
                    conn.Close()
                End Try
            End Using
        End Using
    End Sub

    Private Sub txtCodpro_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCodpro.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If e.KeyChar = Convert.ToChar(Keys.Enter) Then
                If Trim(txtCodpro.Text) <> "" Then
                    txtCodpro.Text = Globales.BuscaCodigoDimerc(txtCodpro.Text)
                    If Trim(txtCodpro.Text) = "" Then
                        MessageBox.Show("Producto No Existe", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        txtCodpro.Focus()
                        Exit Sub
                    End If
                    traeDatos()
                Else
                    txtDescripcion.Focus()
                End If
            End If
        End If
    End Sub
    Private Sub traeDatos()
        Try
            bd.open_dimerc()
            Dim StrQuery = "select a.codpro,a.despro"
            StrQuery = StrQuery & " from MA_PRODUCT a "
            StrQuery = StrQuery & " where a.codpro = '" & Trim(txtCodpro.Text) & "'"
            Dim dt = bd.sqlSelect(StrQuery)
            If dt.Rows.Count > 0 Then
                txtDescripcion.Text = dt.Rows(0).Item("despro")
                txtCosto.Focus()
            Else
                MessageBox.Show("Producto no Existe,verificar por favor.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            End If
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub
    Private Sub btn_Modificar_Click(sender As Object, e As EventArgs) Handles btn_Modificar.Click
        Using conn = New OracleConnection(Conexion.retornaConexion)
            conn.Open()
            Dim comando As OracleCommand
            Dim sql As String
            Using transaccion = conn.BeginTransaction
                PuntoControl.RegistroUso("154")
                Try
                    sql = " update ma_costo_futuro  "
                    sql = sql & " set "
                    sql = sql & " alza=" + txtCosto.Text.Replace(",", ".")
                    sql = sql & " where codpro='" + txtCodpro.Text + "' "
                    sql = sql & " and fecini=to_date('" + dtpFecini.Value.Date + "','dd/mm/yyyy')"
                    sql = sql & " and fecfin=to_date('" + dtpFecFin.Value.Date + "','dd/mm/yyyy')"
                    sql = sql & " and codemp=3 "
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    transaccion.Commit()

                    MessageBox.Show("Datos modificados con éxito", "Listo", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    cargaGrilla()
                Catch ex As Exception
                    If Not transaccion.Equals(Nothing) Then
                        transaccion.Rollback()
                    End If
                    PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                    MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Finally
                    conn.Close()
                End Try
            End Using
        End Using

    End Sub

    Private Sub btnProcesar_Click(sender As Object, e As EventArgs) Handles btnProcesar.Click
        Using conn = New OracleConnection(Conexion.retornaConexion)
            conn.Open()
            Dim comando As OracleCommand
            Dim dataAdapter As OracleDataAdapter
            Dim dt, dt1 As New DataTable
            Dim esta As Boolean = False
            Dim numero As Double = 0
            Dim fecha As Date
            Using transaccion = conn.BeginTransaction
                Try
                    PuntoControl.RegistroUso("156")
                    For i = 0 To dgvDatos.RowCount - 1
                        lblTotales.Text = "Validando " + (i + 1).ToString + " de " + dgvDatos.RowCount.ToString + " Registros"
                        Application.DoEvents()

                        Dim Sql = "SELECT codpro FROM ma_product "
                        Sql = Sql & " WHERE  codpro = '" & Trim(dgvDatos.Item("CODPRO", i).Value) & "'"
                        dt.Clear()
                        comando = New OracleCommand(Sql, conn)
                        comando.Transaction = transaccion
                        dataAdapter = New OracleDataAdapter(comando)
                        dataAdapter.Fill(dt)

                        If dt.Rows.Count = 0 Then
                            dtpifias.Rows.Add(dgvDatos.Item("CODPRO", i).Value, dgvDatos.Item("ALZA", i).Value, dgvDatos.Item("FECINI", i).Value, dgvDatos.Item("FECFIN", i).Value, "Codigo de producto no existe")
                            Continue For
                        End If

                        If Double.TryParse(dgvDatos.Item("ALZA", i).Value, numero) = False Then
                            dtpifias.Rows.Add(dgvDatos.Item("CODPRO", i).Value, dgvDatos.Item("ALZA", i).Value, dgvDatos.Item("FECINI", i).Value, dgvDatos.Item("FECFIN", i).Value, "Costo no tiene formato de número")
                            Continue For
                        End If

                        If Date.TryParse(dgvDatos.Item("fecini", i).Value, fecha) = False Then
                            dtpifias.Rows.Add(dgvDatos.Item("CODPRO", i).Value, dgvDatos.Item("ALZA", i).Value, dgvDatos.Item("FECINI", i).Value, dgvDatos.Item("FECFIN", i).Value, "Fecha de Inicio no tiene formato de fecha")
                            Continue For
                        End If

                        If Date.TryParse(dgvDatos.Item("fecfin", i).Value, fecha) = False Then
                            dtpifias.Rows.Add(dgvDatos.Item("CODPRO", i).Value, dgvDatos.Item("ALZA", i).Value, dgvDatos.Item("FECINI", i).Value, dgvDatos.Item("FECFIN", i).Value, "Fecha de Inicio no tiene formato de fecha")
                            Continue For
                        End If

                        Sql = "  SELECT B.codpro FROM ma_costo_futuro B  "
                        Sql = Sql & "WHERE B.codpro= '" + dgvDatos.Item("CODPRO", i).Value + "' "
                        Sql = Sql & "  and B.codemp= 3 "
                        Sql = Sql & "  and (  "
                        Sql = Sql & "        (to_date('" + dgvDatos.Item("FECINI", i).Value + "','DD/MM/YYYY') between B.FECINI AND B.FECFIN)  "
                        Sql = Sql & "     or (to_date('" + dgvDatos.Item("FECFIN", i).Value + "','DD/MM/YYYY') between B.FECINI AND B.FECFIN)  "
                        Sql = Sql & "     or (B.FECINI between to_date('" + dgvDatos.Item("FECINI", i).Value + "','DD/MM/YYYY') and to_date('" + dgvDatos.Item("FECFIN", i).Value + "','DD/MM/YYYY'))  "
                        Sql = Sql & "     or (B.FECFIN between to_date('" + dgvDatos.Item("FECINI", i).Value + "','DD/MM/YYYY') and to_date('" + dgvDatos.Item("FECFIN", i).Value + "','DD/MM/YYYY'))  "
                        Sql = Sql & "      )  "
                        dt.Clear()
                        comando = New OracleCommand(Sql, conn)
                        comando.Transaction = transaccion
                        dataAdapter = New OracleDataAdapter(comando)
                        dataAdapter.Fill(dt)
                        If dt.Rows.Count > 0 Then
                            dtpifias.Rows.Add(dgvDatos.Item("CODPRO", i).Value, dgvDatos.Item("ALZA", i).Value, dgvDatos.Item("FECINI", i).Value, dgvDatos.Item("FECFIN", i).Value, "Fechas estan cruzadas con algun registro existente")
                            Continue For
                        Else
                            Sql = " insert into ma_costo_futuro(codemp,codpro,alza,cosfut,anterior,fecini,fecfin)  "
                            Sql = Sql & "values (3,'" + dgvDatos.Item("CODPRO", i).Value + "',"
                            Sql += "'" + dgvDatos.Item("ALZA", i).Value.ToString.Replace(",", ".") + "',0,0,"
                            Sql += "to_date('" + dgvDatos.Item("FECINI", i).Value + "','dd/mm/yyyy'),"
                            Sql += "to_date('" + dgvDatos.Item("FECFIN", i).Value + "','dd/mm/yyyy')"
                            Sql += ") "
                            comando = New OracleCommand(Sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()
                        End If
                    Next

                    transaccion.Commit()
                    dgvPifias.DataSource = dtpifias
                    dgvPifias.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells

                    MessageBox.Show("Carga lista, al costado derecho estan los registros con errores")
                    cargaGrilla()

                Catch ex As Exception
                    If Not transaccion.Equals(Nothing) Then
                        transaccion.Rollback()
                    End If
                    PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                    MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Finally
                    conn.Close()
                End Try
            End Using
        End Using
    End Sub

    Private Sub btn_Errores_Click(sender As Object, e As EventArgs) Handles btn_Errores.Click
        PuntoControl.RegistroUso("157")
        Dim save As New SaveFileDialog
        If dgvProductos.RowCount = 0 Then
            MessageBox.Show("No hay datos consultados en la grilla, procese datos antes de exportar", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        Else
            save.Filter = "Archivo Excel | *.xlsx"
            If save.ShowDialog = System.Windows.Forms.DialogResult.OK Then
                Globales.exporta(save.FileName, dgvPifias)
            End If
        End If
    End Sub

    Private Sub btn_Salir_Click(sender As Object, e As EventArgs) Handles btn_Salir.Click
        Close()
    End Sub

    Private Sub dgvProductos_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvProductos.CellContentClick
        Try
            If e.RowIndex <> -1 Then
                If e.ColumnIndex = 0 Then
                    If dgvProductos.Item("¿Elimina?", e.RowIndex).Value = 0 Then
                        dgvProductos.Item("¿Elimina?", e.RowIndex).Value = 1
                    Else
                        dgvProductos.Item("¿Elimina?", e.RowIndex).Value = 0
                    End If

                End If
            End If
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub dtpFecFin_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFecFin.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            btn_Agregar.Focus()
        End If
    End Sub

    Private Sub dtpFecini_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFecini.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            dtpFecFin.Focus()
        End If
    End Sub

    Private Sub btn_Masivo_Click(sender As Object, e As EventArgs) Handles btn_Masivo.Click
        Dim openFile As New OpenFileDialog
        PuntoControl.RegistroUso("155")
        If openFile.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            Globales.Importar(dgvDatos, openFile.FileName)
            dtpaso = dgvDatos.DataSource
            dtpifias = dtpaso.Copy
            dtpifias.Columns.Add("Motivo")
            dtpifias.Clear()
            lblTotales.Text = dgvDatos.Rows.Count.ToString

            dgvDatos.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            total = dgvDatos.RowCount
        End If
    End Sub
End Class