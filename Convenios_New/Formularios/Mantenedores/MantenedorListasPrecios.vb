﻿Imports System.Data.OracleClient

Public Class MantenedorListasPrecios
    Dim sql As String
    Dim bd As New Conexion
    Dim dtPrevisualizacion As New DataTable
    Dim costo1, costo2, indice, indice1, indice2 As Integer
    Dim importado As Boolean = False
    Dim PuntoControl As New PuntoControl

    Private Sub MantenedorListasPrecios_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        PuntoControl.RegistroUso("178")
        bd.open_dimerc()
        Dim dt As New DataTable

        sql = "Select codcnl, nomcnl from ma_listaprecio where activa = 'S' and codemp = 3 "
        dt = bd.sqlSelect(sql)

        cmbListas.DataSource = dt
        cmbListas.ValueMember = "codcnl"
        cmbListas.DisplayMember = "nomcnl"
        cmbListas.SelectedIndex = -1
        cmbListas.Refresh()

        dtPrevisualizacion.Columns.Add("CODIGO")
        dtPrevisualizacion.Columns.Add("PRECIO")
        dtPrevisualizacion.Columns.Add("MARGEN")
        dtPrevisualizacion.Columns.Add("DESCTO")
        dtPrevisualizacion.Columns.Add("MARGEN_MIN")
        dtPrevisualizacion.Columns.Add("PRECIO_MIN")
        dtPrevisualizacion.Columns.Add("COSTO")
        dtPrevisualizacion.Columns.Add("COSTO_CORP")
    End Sub

    Private Sub verListasdelProducto()
        Try
            ProgressBar1.Visible = True
            ProgressBar1.Maximum = 3

            ProgressBar1.Value = 0
            Application.DoEvents()

            bd.open_dimerc()
            Dim Sql = " select a.codcnl ""Lista"", nvl(b.precio,0) ""Precio"", 
            nvl(b.margen, 0) ""Margen"", decode(b.mgprefijo,null,0,b.mgprefijo) ""Margen Min"", nvl(b.prefij,0) ""P.Minimo"",  
            nvl(GETCOSTOCOMERCIAL(3,'" + txtCodigo.Text + "'),0) costo,nvl(GETCOSTOCOMERCIAL(6,'" + txtCodigo.Text + "'),0) costo_corp 
            from ma_listaprecio a, re_canprod b,ma_product c 
            where b.codpro(+) ='" & txtCodigo.Text & "'   
              And a.codemp(+) = " + Globales.empresa.ToString & "
              And  a.codemp = b.codemp(+) 
              And  a.codcnl = b.codcnl
              And  a.activa='S'  
              And b.codpro=c.codpro(+)  
            order by a.codcnl "

            ProgressBar1.Value = 1
            Application.DoEvents()

            dgvListaCodigo.DataSource = bd.sqlSelect(Sql)

            ProgressBar1.Value = 2
            Application.DoEvents()

            dgvListaCodigo.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            dgvListaCodigo.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvListaCodigo.RowsDefaultCellStyle.BackColor = Color.White

            ProgressBar1.Value = 3
            Application.DoEvents()

            ValidaMargenNegativo(dgvListaCodigo)

            dgvListaCodigo.Focus()
            ProgressBar1.Visible = False

            txtPrecioCodigo.Text = ""
            txtPrecioMinCodigo.Text = ""
            txtMargenCodigo.Text = ""
            txtMargenMinCodigo.Text = ""

        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            ProgressBar1.Visible = False
            bd.close()
        End Try
    End Sub

    Private Sub ValidaMargenNegativo(tabla As DataGridView)
        For i = 0 To tabla.RowCount - 1
            If tabla.Item("Margen", i).Value <= 0 Or tabla.Item("Margen Min", i).Value <= 0 Then
                tabla.Rows(i).DefaultCellStyle.BackColor = Color.Red
            End If
        Next
    End Sub

    Private Sub verProductosenLista()
        Try
            ProgressBar1.Visible = True
            ProgressBar1.Maximum = 3

            ProgressBar1.Value = 0
            Application.DoEvents()
            bd.open_dimerc()
            Dim Sql = " SELECT b.codpro ""CODIGO"", NVL (b.precio, 0) ""Precio"",
                         NVL (b.margen, 0) ""Margen"", NVL (b.pordes, 0) ""Descto."",
                         DECODE (b.mgprefijo, NULL, 0, b.mgprefijo) ""Margen Min"",
                         NVL (b.prefij, 0) ""P.Minimo"",
                         NVL (getcostocomercial (3, b.codpro), 0) costo,
                         NVL (getcostocomercial (6, b.codpro), 0) costo_corp
                    FROM ma_listaprecio a, re_canprod b, ma_product c
                   WHERE a.codemp(+) = " & Globales.empresa.ToString & "
             AND a.codemp = b.codemp(+)
             AND a.codcnl = b.codcnl
             AND a.activa = 'S'
             And b.codpro = c.codpro(+)
             AND b.codcnl = " & cmbListas.SelectedValue & " order by a.codcnl "

            ProgressBar1.Value = 1
            Application.DoEvents()

            dgvCodigoLista.DataSource = bd.sqlSelect(Sql)
            ProgressBar1.Value = 2
            Application.DoEvents()

            dgvCodigoLista.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            dgvCodigoLista.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

            dgvCodigoLista.Focus()

            ProgressBar1.Value = 3
            Application.DoEvents()

            ValidaMargenNegativo(dgvCodigoLista)

            dtPrevisualizacion.Clear()
            dgvCambiosListas.Refresh()

            txtPrecioLista.Text = ""
            txtPrecioMinLista.Text = ""
            txtMargenLista.Text = ""
            txtMargenMinLista.Text = ""


        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
            ProgressBar1.Visible = False
        End Try

    End Sub

    Private Sub txtCodigo_KeyPress(sender As Object, e As KeyPressEventArgs)
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            verListasdelProducto()
        End If
    End Sub

    Private Sub txtListas_KeyPress(sender As Object, e As KeyPressEventArgs)
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            verProductosenLista()
        End If
    End Sub

    Private Sub dgvListaCodigo_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvListaCodigo.CellDoubleClick
        If dgvListaCodigo.RowCount > 0 Then
            txtPrecioCodigo.Text = dgvListaCodigo.Item("Precio", e.RowIndex).Value
            txtMargenCodigo.Text = dgvListaCodigo.Item("Margen", e.RowIndex).Value
            txtPrecioMinCodigo.Text = dgvListaCodigo.Item("P.Minimo", e.RowIndex).Value
            txtMargenMinCodigo.Text = dgvListaCodigo.Item("Margen Min", e.RowIndex).Value
            costo1 = dgvListaCodigo.Item("costo", e.RowIndex).Value
            txtPrecioCodigo.Focus()
            indice1 = e.RowIndex
        End If
    End Sub

    Private Sub dgvCodigoLista_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvCodigoLista.CellDoubleClick
        If dgvCodigoLista.RowCount > 0 Then
            txtPrecioLista.Text = dgvCodigoLista.Item("Precio", e.RowIndex).Value
            txtMargenLista.Text = dgvCodigoLista.Item("Margen", e.RowIndex).Value
            txtPrecioMinLista.Text = dgvCodigoLista.Item("P.Minimo", e.RowIndex).Value
            txtMargenMinLista.Text = dgvCodigoLista.Item("Margen Min", e.RowIndex).Value
            costo2 = dgvCodigoLista.Item("costo", e.RowIndex).Value
            txtPrecioLista.Focus()
            indice2 = e.RowIndex
        End If
    End Sub

    'Private Sub txtPrecioCodigo_KeyPress(sender As Object, e As KeyPressEventArgs)
    '    If e.KeyChar = Convert.ToChar(Keys.Enter) Then
    '        If txtPrecioCodigo.Text.Trim <> "" Then
    '            txtMargenCodigo.Text = Math.Round((((txtPrecioCodigo.Text - costo1) / txtPrecioCodigo.Text) * 100), 2)
    '            dgvListaCodigo.Item("Margen", indice1).Value = txtMargenCodigo.Text
    '            dgvListaCodigo.Item("Precio", indice1).Value = txtPrecioCodigo.Text
    '            dgvListaCodigo.Rows(indice1).DefaultCellStyle.BackColor = Color.Aqua
    '            dgvListaCodigo.Rows(indice1).Cells(1).Style.BackColor = Color.Orange
    '            dgvListaCodigo.Rows(indice1).Cells(2).Style.BackColor = Color.Orange
    '            txtMargenCodigo.Focus()
    '        End If
    '    End If
    'End Sub

    'Private Sub txtPrecioMinCodigo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPrecioMinCodigo.KeyPress
    '    If e.KeyChar = Convert.ToChar(Keys.Enter) Then
    '        If txtPrecioMinCodigo.Text.Trim <> "" Then
    '            txtMargenMinCodigo.Text = Math.Round((((txtPrecioMinCodigo.Text - costo1) / txtPrecioMinCodigo.Text) * 100), 2)
    '            dgvCodigoLista.Item("Margen", Integer.Parse(indice1)).Value = txtMargenMinCodigo.Text
    '            dgvCodigoLista.Rows(indice2).DefaultCellStyle.BackColor = Color.Aqua
    '            txtMargenMinCodigo.Focus()
    '        End If
    '    End If
    'End Sub

    'Private Sub txtPrecioLista_KeyPress(sender As Object, e As KeyPressEventArgs)
    '    If e.KeyChar = Convert.ToChar(Keys.Enter) Then
    '        If txtPrecioLista.Text.Trim <> "" Then
    '            txtMargenLista.Text = Math.Round((((txtPrecioLista.Text - costo2) / txtPrecioLista.Text) * 100), 2)
    '            dgvCodigoLista.Item("P.Minimo", Integer.Parse(indice2)).Value = txtMargenLista.Text
    '            dgvCodigoLista.Rows(indice2).DefaultCellStyle.BackColor = Color.Aqua
    '            txtMargenLista.Focus()
    '        End If
    '    End If
    'End Sub

    'Private Sub txtPrecioMinLista_KeyPress(sender As Object, e As KeyPressEventArgs)
    '    If e.KeyChar = Convert.ToChar(Keys.Enter) Then
    '        If txtPrecioMinLista.Text.Trim <> "" Then
    '            txtMargenMinLista.Text = Math.Round((((txtPrecioMinLista.Text - costo2) / txtPrecioMinLista.Text) * 100), 2)
    '            dgvCodigoLista.Item("Margen Min", Integer.Parse(indice2)).Value = txtMargenMinLista.Text
    '            dgvCodigoLista.Item("P.Minimo", Integer.Parse(indice2)).Value = txtPrecioMinLista.Text
    '            dgvCodigoLista.Rows(indice2).DefaultCellStyle.BackColor = Color.Aqua
    '            dgvCodigoLista.Rows(indice2).Cells(3).Style.BackColor = Color.Orange
    '            dgvCodigoLista.Rows(indice2).Cells(4).Style.BackColor = Color.Orange
    '            txtMargenMinLista.Focus()
    '        End If
    '    End If
    'End Sub

    'Private Sub txtMargenCodigo_KeyPress(sender As Object, e As KeyPressEventArgs)
    '    If e.KeyChar = Convert.ToChar(Keys.Enter) Then
    '        If txtMargenCodigo.Text.Trim <> "" Then
    '            txtPrecioCodigo.Text = Math.Round(costo1 / (1 - (txtMargenCodigo.Text / 100)), 0)
    '            dgvListaCodigo.Item("Precio", indice1).Value = txtPrecioCodigo.Text
    '            dgvListaCodigo.Item("Margen", indice1).Value = txtMargenCodigo.Text
    '            dgvListaCodigo.Rows(indice1).DefaultCellStyle.BackColor = Color.Aqua
    '            dgvListaCodigo.Rows(indice1).Cells(1).Style.BackColor = Color.Orange
    '            dgvListaCodigo.Rows(indice1).Cells(2).Style.BackColor = Color.Orange
    '            txtPrecioCodigo.Focus()
    '        End If
    '    End If
    'End Sub

    'Private Sub txtMargenMinCodigo_KeyPress(sender As Object, e As KeyPressEventArgs)
    '    If e.KeyChar = Convert.ToChar(Keys.Enter) Then
    '        If txtMargenMinCodigo.Text.Trim <> "" Then
    '            txtPrecioMinCodigo.Text = Math.Round(costo1 / (1 - (txtMargenMinCodigo.Text / 100)), 0)
    '            dgvListaCodigo.Item("P.Minimo", indice1).Value = txtPrecioMinCodigo.Text
    '            dgvListaCodigo.Item("Margen Min", indice1).Value = txtMargenMinCodigo.Text
    '            dgvListaCodigo.Rows(indice1).DefaultCellStyle.BackColor = Color.Aqua
    '            dgvListaCodigo.Rows(indice1).Cells(3).Style.BackColor = Color.Orange
    '            dgvListaCodigo.Rows(indice1).Cells(4).Style.BackColor = Color.Orange
    '            txtPrecioMinCodigo.Focus()
    '        End If
    '    End If
    'End Sub

    'Private Sub txtMargenLista_KeyPress(sender As Object, e As KeyPressEventArgs)
    '    If e.KeyChar = Convert.ToChar(Keys.Enter) Then
    '        If txtMargenLista.Text.Trim <> "" Then
    '            txtPrecioLista.Text = Math.Round(costo2 / (1 - (txtMargenLista.Text / 100)), 0)
    '            dgvCodigoLista.Item("Precio", indice2).Value = txtPrecioLista.Text
    '            dgvCodigoLista.Item("Margen", indice2).Value = txtMargenLista.Text
    '            dgvCodigoLista.Rows(indice2).DefaultCellStyle.BackColor = Color.Aqua
    '            dgvCodigoLista.Rows(indice2).Cells(1).Style.BackColor = Color.Orange
    '            dgvCodigoLista.Rows(indice2).Cells(2).Style.BackColor = Color.Orange
    '            txtPrecioLista.Focus()
    '        End If
    '    End If
    'End Sub

    'Private Sub txtMargenMinLista_KeyPress(sender As Object, e As KeyPressEventArgs)
    '    If e.KeyChar = Convert.ToChar(Keys.Enter) Then
    '        If txtMargenMinLista.Text.Trim <> "" Then
    '            txtPrecioMinLista.Text = Math.Round(costo2 / (1 - (txtMargenMinLista.Text / 100)), 0)
    '            dgvCodigoLista.Item("P.Minimo", Integer.Parse(indice2)).Value = txtPrecioMinLista.Text
    '            dgvCodigoLista.Item("Margen Min", Integer.Parse(indice2)).Value = txtMargenMinLista.Text
    '            dgvCodigoLista.Rows(indice2).DefaultCellStyle.BackColor = Color.Aqua
    '            dgvCodigoLista.Rows(indice2).Cells(3).Style.BackColor = Color.Orange
    '            dgvCodigoLista.Rows(indice2).Cells(4).Style.BackColor = Color.Orange
    '            txtPrecioMinLista.Focus()
    '        End If
    '    End If
    'End Sub

    Private Sub dgvListaCodigo_CellMouseDown(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvListaCodigo.CellMouseDown
        If e.Button = MouseButtons.Right Then
            Dim puntos As New Point
            puntos.X = MousePosition.X
            puntos.Y = MousePosition.Y
            indice = e.RowIndex
        End If
    End Sub

    Private Sub ContextMenuStrip1_Click(sender As Object, e As EventArgs)
        guardarFila(dgvListaCodigo.Item("Precio", indice).Value,
                    dgvListaCodigo.Item("Margen", indice).Value,
                    dgvListaCodigo.Item("P.Minimo", indice).Value,
                    dgvListaCodigo.Item("Margen Min", indice).Value,
                    txtCodigo.Text,
                    dgvListaCodigo.Item("Lista", indice).Value)
    End Sub

    Private Sub cmbListas_KeyPress(sender As Object, e As KeyPressEventArgs)
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            verProductosenLista()
        End If
    End Sub

    Private Sub txtModifMargen_KeyPress(sender As Object, e As KeyPressEventArgs)
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then

        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        guardartodo(1)
        PuntoControl.RegistroUso("180")
    End Sub

    Private Sub txtPrecioLista_KeyPress_1(sender As Object, e As KeyPressEventArgs)
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If txtPrecioLista.Text.Trim <> "" Then
                txtMargenLista.Text = Math.Round((((txtPrecioLista.Text - costo2) / txtPrecioLista.Text) * 100), 2)
                dgvCodigoLista.Item("Margen", indice2).Value = txtMargenLista.Text
                dgvCodigoLista.Item("Precio", indice2).Value = txtPrecioLista.Text
                dgvCodigoLista.Rows(indice2).DefaultCellStyle.BackColor = Color.Aqua
                dgvCodigoLista.Rows(indice2).Cells(1).Style.BackColor = Color.Orange
                dgvCodigoLista.Rows(indice2).Cells(2).Style.BackColor = Color.Orange
                txtMargenMinCodigo.Focus()
            End If
        End If
    End Sub

    Private Sub btnImportar_Click(sender As Object, e As EventArgs) Handles btnImportar.Click
        Try
            Dim openFile As New OpenFileDialog
            PuntoControl.RegistroUso("184")
            If openFile.ShowDialog = System.Windows.Forms.DialogResult.OK Then
                Globales.Importar(dgvCambiosListas, openFile.FileName)
                dgvCambiosListas.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
                importado = True
            End If

        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub btnExportar2_Click(sender As Object, e As EventArgs) Handles btnExportar2.Click
        Dim save As New SaveFileDialog
        If dgvCodigoLista.RowCount = 0 Then
            MessageBox.Show("No hay datos consultados en la grilla", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        Else
            save.Filter = "Archivo Excel | *.xlsx"
            If save.ShowDialog = System.Windows.Forms.DialogResult.OK Then
                Globales.exporta(save.FileName, dgvCodigoLista)
            End If
        End If
    End Sub

    Private Sub btnExportar1_Click(sender As Object, e As EventArgs) Handles btnExportar1.Click
        Dim save As New SaveFileDialog
        If dgvListaCodigo.RowCount = 0 Then
            PuntoControl.RegistroUso("181")
            MessageBox.Show("No hay datos consultados en la grilla", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        Else
            save.Filter = "Archivo Excel | *.xlsx"
            If save.ShowDialog = System.Windows.Forms.DialogResult.OK Then
                Globales.exporta(save.FileName, dgvListaCodigo)
            End If
        End If
    End Sub

    Private Sub btnGuardarImpor_Click(sender As Object, e As EventArgs) Handles btnGuardarImpor.Click
        Using conn = New OracleConnection(Conexion.retornaConexion)
            conn.Open()
            Using transaccion = conn.BeginTransaction
                Dim sql As String
                Cursor.Current = Cursors.WaitCursor
                Dim comando As OracleCommand
                Try

                    If True Then
                        For i = 0 To dgvCambiosListas.RowCount - 1
                            sql = " Update Re_Canprod set precio = " & dgvCambiosListas.Item("PRECIO", i).Value & ",
                                                          margen = " & Replace(dgvCambiosListas.Item("MARGEN", i).Value, ",", ".") & ",
                                                          prefij = " & dgvCambiosListas.Item("PRECIO_MIN", i).Value & ",
                                                          mgprefijo = " & Replace(dgvCambiosListas.Item("MARGEN_MIN", i).Value, ",", ".") & "
                                    where codcnl = " & cmbListas.SelectedValue & "
                                      and codpro = '" & dgvCambiosListas.Item("CODIGO", i).Value & "'
                                      and codemp = 3"
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()
                        Next
                    End If

                Catch ex As Exception
                    If Not transaccion.Equals(Nothing) Then
                        transaccion.Rollback()
                    End If
                    PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                    MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Finally
                    conn.Close()
                End Try
            End Using
        End Using
    End Sub

    Private Sub txtCodigo_KeyPress_1(sender As Object, e As KeyPressEventArgs) Handles txtCodigo.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            verListasdelProducto()
            PuntoControl.RegistroUso("179")
        End If
    End Sub

    Private Sub cmbListas_KeyPress_1(sender As Object, e As KeyPressEventArgs) Handles cmbListas.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            verProductosenLista()
            PuntoControl.RegistroUso("182")
        End If
    End Sub

    Private Sub txtPrecioCodigo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPrecioCodigo.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If txtPrecioCodigo.Text.Trim <> "" Then
                If cbxCodigos.Checked = True Or cbxCodigos.Checked = 1 Then
                    For i = 0 To dgvListaCodigo.RowCount - 1
                        txtMargenCodigo.Text = Math.Round((((txtPrecioCodigo.Text - costo1) / txtPrecioCodigo.Text) * 100), 2)
                        dgvListaCodigo.Item("Margen", i).Value = txtMargenCodigo.Text
                        dgvListaCodigo.Item("Precio", i).Value = txtPrecioCodigo.Text
                        dgvListaCodigo.Rows(i).DefaultCellStyle.BackColor = Color.Aqua
                        dgvListaCodigo.Rows(i).Cells(1).Style.BackColor = Color.Orange
                        dgvListaCodigo.Rows(i).Cells(2).Style.BackColor = Color.Orange
                        txtMargenCodigo.Focus()
                    Next
                Else
                    txtMargenCodigo.Text = Math.Round((((txtPrecioCodigo.Text - costo1) / txtPrecioCodigo.Text) * 100), 2)
                    dgvListaCodigo.Item("Margen", indice1).Value = txtMargenCodigo.Text
                    dgvListaCodigo.Item("Precio", indice1).Value = txtPrecioCodigo.Text
                    dgvListaCodigo.Rows(indice1).DefaultCellStyle.BackColor = Color.Aqua
                    dgvListaCodigo.Rows(indice1).Cells(1).Style.BackColor = Color.Orange
                    dgvListaCodigo.Rows(indice1).Cells(2).Style.BackColor = Color.Orange
                    txtMargenCodigo.Focus()
                End If
            End If
        End If
    End Sub

    Private Sub txtPrecioMinCodigo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPrecioMinCodigo.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If txtPrecioMinCodigo.Text.Trim <> "" Then
                If cbxCodigos.Checked = True Or cbxCodigos.Checked = 1 Then
                    For i = 0 To dgvListaCodigo.RowCount - 1
                        txtMargenMinCodigo.Text = Math.Round((((txtPrecioMinCodigo.Text - costo1) / txtPrecioMinCodigo.Text) * 100), 2)
                        dgvListaCodigo.Item("P.Minimo", i).Value = txtPrecioMinCodigo.Text
                        dgvListaCodigo.Item("Margen Min", i).Value = txtMargenMinCodigo.Text
                        dgvListaCodigo.Rows(i).DefaultCellStyle.BackColor = Color.Aqua
                        dgvListaCodigo.Rows(i).Cells(3).Style.BackColor = Color.Orange
                        dgvListaCodigo.Rows(i).Cells(4).Style.BackColor = Color.Orange
                        txtMargenMinCodigo.Focus()
                    Next
                Else
                    txtMargenMinCodigo.Text = Math.Round((((txtPrecioMinCodigo.Text - costo1) / txtPrecioMinCodigo.Text) * 100), 2)
                    dgvListaCodigo.Item("P.Minimo", indice1).Value = txtPrecioMinCodigo.Text
                    dgvListaCodigo.Item("Margen Min", indice1).Value = txtMargenMinCodigo.Text
                    dgvListaCodigo.Rows(indice1).DefaultCellStyle.BackColor = Color.Aqua
                    dgvListaCodigo.Rows(indice1).Cells(3).Style.BackColor = Color.Orange
                    dgvListaCodigo.Rows(indice1).Cells(4).Style.BackColor = Color.Orange
                    txtMargenMinCodigo.Focus()
                End If
            End If
        End If
    End Sub

    Private Sub txtMargenMinCodigo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMargenMinCodigo.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If txtMargenMinCodigo.Text.Trim <> "" Then
                If cbxCodigos.Checked = True Or cbxCodigos.Checked = 1 Then
                    For i = 0 To dgvListaCodigo.RowCount - 1
                        txtPrecioMinCodigo.Text = Math.Round(costo1 / (1 - (txtMargenMinCodigo.Text / 100)), 0)
                        dgvListaCodigo.Item("P.Minimo", i).Value = txtPrecioMinCodigo.Text
                        dgvListaCodigo.Item("Margen Min", i).Value = txtMargenMinCodigo.Text
                        dgvListaCodigo.Rows(i).DefaultCellStyle.BackColor = Color.Aqua
                        dgvListaCodigo.Rows(i).Cells(3).Style.BackColor = Color.Orange
                        dgvListaCodigo.Rows(i).Cells(4).Style.BackColor = Color.Orange
                        txtPrecioMinCodigo.Focus()
                    Next
                Else
                    txtPrecioMinCodigo.Text = Math.Round(costo1 / (1 - (txtMargenMinCodigo.Text / 100)), 0)
                    dgvListaCodigo.Item("P.Minimo", indice1).Value = txtPrecioMinCodigo.Text
                    dgvListaCodigo.Item("Margen Min", indice1).Value = txtMargenMinCodigo.Text
                    dgvListaCodigo.Rows(indice1).DefaultCellStyle.BackColor = Color.Aqua
                    dgvListaCodigo.Rows(indice1).Cells(3).Style.BackColor = Color.Orange
                    dgvListaCodigo.Rows(indice1).Cells(4).Style.BackColor = Color.Orange
                    txtPrecioMinCodigo.Focus()
                End If
            End If
        End If
    End Sub

    Private Sub txtCodLista_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCodLista.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            BuscarProducto()
        End If
    End Sub

    Private Sub BuscarProducto()
        If dgvCodigoLista.RowCount > 0 Then
            For i = 0 To dgvCodigoLista.RowCount - 1
                If txtCodLista.Text.ToString = dgvCodigoLista.Item("Codigo", i).Value.ToString Then
                    txtPrecioLista.Text = dgvCodigoLista.Item("Precio", i).Value
                    txtMargenLista.Text = dgvCodigoLista.Item("Margen", i).Value
                    txtPrecioMinLista.Text = dgvCodigoLista.Item("P.Minimo", i).Value
                    txtMargenMinLista.Text = dgvCodigoLista.Item("Margen Min", i).Value
                    costo2 = dgvCodigoLista.Item("costo", i).Value
                    txtPrecioLista.Focus()
                    indice2 = i
                End If
            Next
        End If
    End Sub

    Private Sub txtPrecioLista_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPrecioLista.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If txtPrecioLista.Text.Trim <> "" Then
                txtMargenLista.Text = Math.Round((((txtPrecioLista.Text - costo2) / txtPrecioLista.Text) * 100), 2)
                dgvCodigoLista.Item("Precio", indice2).Value = txtPrecioLista.Text
                dgvCodigoLista.Item("Margen", indice2).Value = txtMargenLista.Text
                dgvCodigoLista.Rows(indice2).DefaultCellStyle.BackColor = Color.Aqua
                dgvCodigoLista.Rows(indice2).Cells(1).Style.BackColor = Color.Orange
                dgvCodigoLista.Rows(indice2).Cells(2).Style.BackColor = Color.Orange
                txtMargenLista.Focus()
            End If
        End If
    End Sub

    Private Sub txtMargenLista_KeyPress_1(sender As Object, e As KeyPressEventArgs) Handles txtMargenLista.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If txtMargenLista.Text.Trim <> "" Then
                txtPrecioLista.Text = Math.Round(costo2 / (1 - (txtMargenLista.Text / 100)), 0)
                dgvCodigoLista.Item("Precio", indice2).Value = txtPrecioLista.Text
                dgvCodigoLista.Item("Margen", indice2).Value = txtMargenLista.Text
                dgvCodigoLista.Rows(indice2).DefaultCellStyle.BackColor = Color.Aqua
                dgvCodigoLista.Rows(indice2).Cells(1).Style.BackColor = Color.Orange
                dgvCodigoLista.Rows(indice2).Cells(2).Style.BackColor = Color.Orange
                txtPrecioLista.Focus()
            End If
        End If
    End Sub

    Private Sub MantenedorListasPrecios_KeyPress(sender As Object, e As KeyPressEventArgs) Handles MyBase.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If txtMargenMinLista.Text.Trim <> "" Then
                txtPrecioMinLista.Text = Math.Round(costo2 / (1 - (txtMargenMinLista.Text / 100)), 0)
                dgvCodigoLista.Item("P.Minimo", Integer.Parse(indice2)).Value = txtPrecioMinLista.Text
                dgvCodigoLista.Item("Margen Min", Integer.Parse(indice2)).Value = txtMargenMinLista.Text
                dgvCodigoLista.Rows(indice2).DefaultCellStyle.BackColor = Color.Aqua
                dgvCodigoLista.Rows(indice2).Cells(3).Style.BackColor = Color.Orange
                dgvCodigoLista.Rows(indice2).Cells(4).Style.BackColor = Color.Orange
                txtPrecioMinLista.Focus()
            End If
        End If
    End Sub

    Private Sub txtPrecioMinLista_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPrecioMinLista.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If txtPrecioMinLista.Text.Trim <> "" Then
                txtMargenMinLista.Text = Math.Round((((txtPrecioMinLista.Text - costo2) / txtPrecioMinLista.Text) * 100), 2)
                dgvCodigoLista.Item("Margen Min", Integer.Parse(indice2)).Value = txtMargenMinLista.Text
                dgvCodigoLista.Item("P.Minimo", Integer.Parse(indice2)).Value = txtPrecioMinLista.Text
                dgvCodigoLista.Rows(indice2).DefaultCellStyle.BackColor = Color.Aqua
                dgvCodigoLista.Rows(indice2).Cells(5).Style.BackColor = Color.Orange
                dgvCodigoLista.Rows(indice2).Cells(4).Style.BackColor = Color.Orange
                txtMargenMinLista.Focus()
            End If
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        guardartodo(2)
        PuntoControl.RegistroUso("183")
    End Sub

    Private Sub txtMargenMinLista_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMargenMinLista.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If txtMargenMinLista.Text.Trim <> "" Then
                txtPrecioLista.Text = Math.Round(costo2 / (1 - (txtMargenMinLista.Text / 100)), 0)
                dgvCodigoLista.Item("P.Minimo", indice2).Value = txtPrecioLista.Text
                dgvCodigoLista.Item("Margen Min", indice2).Value = txtMargenLista.Text
                dgvCodigoLista.Rows(indice2).DefaultCellStyle.BackColor = Color.Aqua
                dgvCodigoLista.Rows(indice2).Cells(5).Style.BackColor = Color.Orange
                dgvCodigoLista.Rows(indice2).Cells(4).Style.BackColor = Color.Orange
                txtPrecioLista.Focus()
            End If
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Using conn = New OracleConnection(Conexion.retornaConexion)
            conn.Open()
            Using transaccion = conn.BeginTransaction
                PuntoControl.RegistroUso("185")
                Dim sql As String
                Cursor.Current = Cursors.WaitCursor
                Dim comando As OracleCommand
                Dim dataAdapter As OracleDataAdapter
                Dim dtClientes, dt As New DataTable
                Try

                    For i = 0 To dgvCambiosListas.RowCount - 1
                        sql = " Update Re_Canprod set precio = " & dgvCodigoLista.Item("Precio", i).Value & ",
                                                          margen = " & Replace(dgvCodigoLista.Item("Margen", i).Value, ",", ".") & ",
                                                          prefij = " & dgvCodigoLista.Item("Precio_Min", i).Value & ",
                                                          mgprefijo = " & Replace(dgvCodigoLista.Item("Margen_Min", i).Value, ",", ".") & "
                                    where codcnl = " & dgvCodigoLista.Item("Lista", i).Value & "
                                      and codpro = '" & dgvCodigoLista.Item("Codigo", i).Value & "'"
                        comando = New OracleCommand(sql, conn)
                        comando.Transaction = transaccion
                        comando.ExecuteNonQuery()
                    Next

                    transaccion.Commit()

                    MessageBox.Show("Productos modificados con éxito", "Finalizado", MessageBoxButtons.OK, MessageBoxIcon.Information)

                Catch ex As Exception
                    If Not transaccion.Equals(Nothing) Then
                        transaccion.Rollback()
                    End If
                    PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                    MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Finally
                    conn.Close()
                End Try
            End Using
        End Using
    End Sub

    Private Sub btnCorrigeNegativos_Click(sender As Object, e As EventArgs) Handles btnCorrigeNegativos.Click

    End Sub

    Private Sub txtMargenCodigo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMargenCodigo.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If cbxCodigos.Checked = True Or cbxCodigos.Checked = 1 Then
                For i = 0 To dgvListaCodigo.RowCount - 1
                    txtPrecioCodigo.Text = Math.Round(costo1 / (1 - (txtMargenCodigo.Text / 100)), 0)
                    dgvListaCodigo.Item("Precio", i).Value = txtPrecioCodigo.Text
                    dgvListaCodigo.Item("Margen", i).Value = txtMargenCodigo.Text
                    dgvListaCodigo.Rows(i).DefaultCellStyle.BackColor = Color.Aqua
                    dgvListaCodigo.Rows(i).Cells(1).Style.BackColor = Color.Orange
                    dgvListaCodigo.Rows(i).Cells(2).Style.BackColor = Color.Orange
                    txtPrecioCodigo.Focus()
                Next
            Else
                txtPrecioCodigo.Text = Math.Round(costo1 / (1 - (txtMargenCodigo.Text / 100)), 0)
                dgvListaCodigo.Item("Precio", indice1).Value = txtPrecioCodigo.Text
                dgvListaCodigo.Item("Margen", indice1).Value = txtMargenCodigo.Text
                dgvListaCodigo.Rows(indice1).DefaultCellStyle.BackColor = Color.Aqua
                dgvListaCodigo.Rows(indice1).Cells(1).Style.BackColor = Color.Orange
                dgvListaCodigo.Rows(indice1).Cells(2).Style.BackColor = Color.Orange
                txtPrecioCodigo.Focus()
            End If
        End If
    End Sub

    Private Sub txtModifMargenMin_KeyPress(sender As Object, e As KeyPressEventArgs)
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then

        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub guardarFila(precio As String, margen As String, precioMin As String, margenMin As String, codigo As String, lista As String)
        Using conn = New OracleConnection(Conexion.retornaConexion)
            conn.Open()
            Using transaccion = conn.BeginTransaction
                Dim sql As String
                Cursor.Current = Cursors.WaitCursor
                Dim comando As OracleCommand
                Dim dataAdapter As OracleDataAdapter
                Dim dtClientes, dt As New DataTable
                Try

                    sql = " Update Re_Canprod set precio = " & precio & ",
                                                          margen = " & Replace(margen, ",", ".") & ",
                                                          prefij = " & precioMin & ",
                                                          mgprefijo = " & Replace(margenMin, ",", ".") & "
                                    where codcnl = " & lista & "
                                      and codpro = '" & codigo & "'"
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()
                    
                    transaccion.Commit()

                    MessageBox.Show("Producto modificado con éxito", "Finalizado", MessageBoxButtons.OK, MessageBoxIcon.Information)

                    verListasdelProducto()
                Catch ex As Exception
                    If Not transaccion.Equals(Nothing) Then
                        transaccion.Rollback()
                    End If
                    PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                    MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Finally
                    conn.Close()
                End Try
            End Using
        End Using
    End Sub

    Private Sub guardartodo(numero As Int16)
        Using conn = New OracleConnection(Conexion.retornaConexion)
            conn.Open()
            Using transaccion = conn.BeginTransaction
                Dim sql As String
                Cursor.Current = Cursors.WaitCursor
                Dim comando As OracleCommand
                Dim dataAdapter As OracleDataAdapter
                Dim dtClientes, dt As New DataTable
                Try
                    If numero = 1 Then
                        For i = 0 To dgvListaCodigo.RowCount - 1
                            If dgvListaCodigo.Rows(i).DefaultCellStyle.BackColor = Color.Aqua Then
                                sql = " Update Re_Canprod set precio = " & dgvListaCodigo.Item("Precio", i).Value & ",
                                                          margen = " & Replace(dgvListaCodigo.Item("Margen", i).Value, ",", ".") & ",
                                                          prefij = " & dgvListaCodigo.Item("P.Minimo", i).Value & ",
                                                          mgprefijo = " & Replace(dgvListaCodigo.Item("Margen Min", i).Value, ",", ".") & "
                                    where codcnl = " & dgvListaCodigo.Item("Lista", i).Value & "
                                      and codpro = '" & txtCodigo.Text & "'"
                                comando = New OracleCommand(sql, conn)
                                comando.Transaction = transaccion
                                comando.ExecuteNonQuery()
                            End If
                        Next

                        verListasdelProducto()
                    ElseIf numero = 2 Then
                        For i = 0 To dgvCodigoLista.RowCount - 1
                            If dgvCodigoLista.Rows(i).DefaultCellStyle.BackColor = Color.Aqua Then
                                sql = " Update Re_Canprod set precio = " & dgvCodigoLista.Item("Precio", i).Value & ",
                                                          margen = " & Replace(dgvCodigoLista.Item("Margen", i).Value, ",", ".") & ",
                                                          prefij = " & dgvCodigoLista.Item("P.Minimo", i).Value & ",
                                                          mgprefijo = " & Replace(dgvCodigoLista.Item("Margen Min", i).Value, ",", ".") & "
                                    where codcnl = " & cmbListas.SelectedValue & "
                                      and codpro = '" & dgvCodigoLista.Item("Codigo", i).Value & "'"
                                comando = New OracleCommand(sql, conn)
                                comando.Transaction = transaccion
                                comando.ExecuteNonQuery()
                            End If
                        Next

                        verProductosenLista()
                    End If

                    transaccion.Commit()

                    MessageBox.Show("Productos modificados con éxito", "Finalizado", MessageBoxButtons.OK, MessageBoxIcon.Information)

                Catch ex As Exception
                    If Not transaccion.Equals(Nothing) Then
                        transaccion.Rollback()
                    End If
                    PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                    MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Finally
                    conn.Close()
                End Try
            End Using
        End Using
    End Sub

End Class