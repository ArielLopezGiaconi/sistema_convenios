﻿Public Class frmReportesVarios
    Dim dt As New DataTable
    Dim bd As New Conexion
    Dim sql As String
    Private Sub btnActualizar_Click(sender As Object, e As EventArgs) Handles btnActualizar.Click
        Try
            bd.open_dimerc()

            sql = "SELECT CODPRO, 
                       GETCATEGORIA_PRODUCTO(3, CODPRO) categoria,
                       ROUND(((precio - costos) / precio) * 100, 2) margen_frontal,
                       ROUND(((precio - COSTOCTE) / precio) * 100, 2) margen_frontal_cte, 
                       TRANSACCIONABILIDAD,
                       GETLINEA(CODPRO) LINEA,
                       GETLINEA_PRINCIPAL(CODPRO) LINEA_PRINCIPAL
                       FROM 
                (select b.codpro, 
                       ROUND(sum(nvl(b.precio,0)) / sum(NVL(b.cantid,0))) PRECIO,
                       ROUND(sum(nvl(b.costos,0)) / sum(NVL(b.cantid,0))) costos ,
                       ROUND(sum(nvl(b.costocte,0)) / sum(NVL(b.cantid,0))) costocte,
                       count(b.cantid) transaccionabilidad
                        from en_notavta a, 
                             de_notavta b
                        where a.fecha_creac 
                        between to_Date('" & dtpDesde.Value.ToShortDateString & " 00:00:00','dd/mm/yyyy hh24:mi:ss')
                            and to_Date('" & dtpHasta.Value.ToShortDateString & " 23:59:59','dd/mm/yyyy hh24:mi:ss')
                          and a.numnvt = b.numnvt
                          and a.numnvt <> 5
                          and b.cantid > 0
                          and get_negocio(a.codemp, a.rutcli) not in ('GOBIERNO') 
                        group by b.codpro)
                WHERE PRECIO > 0"
            dt = bd.sqlSelect(sql)
            dgvTransaccionabilidad.DataSource = dt
            dgvTransaccionabilidad.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells

        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Error.", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub MetroButton1_Click(sender As Object, e As EventArgs) Handles btnbuscar.Click
        Try
            bd.open_dimerc()

            sql = "SELECT A.CODPRO, 
                       A.DESPRO, 
                       A.COSTO, 
                       A.COSPROM,  
                       B.CODTIP,
                       B.VALOR DELTA,
                       ROUND(((A.COSTO - NVL(A.COSPROM,0)) / A.COSTO) * 100, 2) CORRECCION_M,
                       GETCATEGORIA_PRODUCTO(3, A.CODPRO) CATEGORIA
                FROM MA_PRODUCT A, MA_COSTO_COMERCIAL B
                WHERE A.CODPRO = B.CODPRO
                  AND A.ESTPRO IN(1,11)
                  AND A.CODNEG = 0
                  AND B.CODEMP = 3
                  AND A.COSTO <> 0"
            dt = bd.sqlSelect(sql)
            dgvDelta.DataSource = dt
            dgvDelta.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells

        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Error.", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

    End Sub

    Private Sub frmReportesVarios_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        PuntoControl.RegistroUso("120")
    End Sub
End Class