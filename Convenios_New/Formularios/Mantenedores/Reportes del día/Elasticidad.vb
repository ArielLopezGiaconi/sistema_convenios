﻿Imports System.Collections.Generic
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.OracleClient
Imports System.Data.SqlClient

Public Class Elasticidad
    Public sql As String
    Public bd As New Conexion
    Dim Datos_grafico, dtPuntosGrafico As New DataTable
    Dim ToolTipProvider As New ToolTip
    Dim Ci, Cf, Pi, Pf As Double
    Dim conexion As String = "data source=ALOPEZ_RENNTB\SQLEXPRESS; Initial Catalog=Elasticidad; integrated security=true" 'User Id=ALOPEZ_RENNTB\ArielLopez; Password=alg1234"
    Dim myCommand As Object
    Dim fecha1, fecha2 As Date

    Private Sub Elasticidad_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            PuntoControl.RegistroUso("118")
            bd.open_dimerc()
            Dim dt, dt1 As New DataTable

            sql = "select codcnl, nomcnl from ma_listaprecio where activa = 'S' and codemp = 3 "
            dt = bd.sqlSelect(sql)

            dt.Rows.Add(0, "Sin lista")

            cmbListas.DataSource = dt
            cmbListas.ValueMember = "codcnl"
            cmbListas.DisplayMember = "nomcnl"
            cmbListas.SelectedIndex = -1
            cmbListas.Refresh()

            sql = "select desval, desval from de_dominio where coddom = 110"
            dt1 = bd.sqlSelect(sql)

            dt1.Rows.Add("Sin negocio", "Sin negocio")

            cmbNegocio.DataSource = dt1
            cmbNegocio.ValueMember = "desval"
            cmbNegocio.DisplayMember = "desval"
            cmbNegocio.SelectedIndex = -1
            cmbNegocio.Refresh()

            dtPuntosGrafico.Columns.Add("precio")
            dtPuntosGrafico.Columns.Add("cantidad")
            dtPuntosGrafico.Columns.Add("fecha")

        Catch ex As Exception

        End Try
    End Sub

    Private Function consultar(sql As String) As DataTable
        Try
            Dim tabla As DataTable = New DataTable
            Dim conex As New SqlClient.SqlConnection(conexion)
            Dim coman As New SqlClient.SqlCommand(sql, conex)
            Dim adapter As New SqlDataAdapter(coman)
            adapter.Fill(tabla)
            conex.Close()
            Return tabla
        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Function

    Private Sub guardar(CODCNL, CODPRO, PRECIO, CANTIDAD, FECHA, ELASTICIDAD)
        Dim con As New SqlConnection
        Dim cmd As New SqlCommand
        Try
            Dim sent As String = ("INSERT INTO ELASTICIDAD (CODCNL, CODPRO, PRECIO, CANTIDAD, FECHA, ELASTICIDAD) 
                          VALUES ('" & CODCNL & "','" & CODPRO & "', '" & PRECIO & "','" & CANTIDAD & "','" & FECHA & "','" & ELASTICIDAD & "')")

            con.ConnectionString = conexion
            con.Open()
            cmd.Connection = con
            cmd.CommandText = sent
            cmd.ExecuteNonQuery()

        Catch ex As Exception
            MessageBox.Show("Error while inserting record on table..." & ex.Message, "Insert Records")
        Finally
            con.Close()
        End Try
    End Sub
    Private Sub CargaDatos()
        Try
            bd.open_dimerc()
            Dim dt, dt1 As New DataTable
            'Dim codcnl, codpro As String

            ProgressBarX1.Visible = True
            ProgressBarX1.Maximum = 7
            ProgressBarX1.Value = 0
            Application.DoEvents()

            ProgressBarX1.Value = 1
            Application.DoEvents()

            If txtCodpro.Text.Trim <> "" Then
                'PRODUCTOS CON CAMBIO DE PRECIO
                sql = "select a.codcnl, a.codpro, a.rutcli, a.precio_ori precio_venta, a.precio_new precio_lista, sum(a.cantidad) cantidad, a.fecha
                    from cambios_precios a, ma_product b
                    where a.codpro = b.codpro "
                If txtCodpro.Text.Trim <> "" Then
                    sql = sql & " And a.codpro = '" & txtCodpro.Text & "'"
                End If
                If txtRutcli.Text.Trim <> "" Then
                    sql = sql & " And a.rutcli = " & txtRutcli.Text
                End If
                If cmbListas.SelectedValue <> 0 Then
                    sql = sql & " And a.codcnl = " & cmbListas.SelectedValue
                End If
                If cmbNegocio.SelectedIndex <> -1 Then
                    If cmbNegocio.SelectedValue.ToString <> "Sin negocio" Then
                        sql = sql & " and GETCLASIFICACION_CLIENTE(3, A.RUTCLI) = '" & cmbNegocio.SelectedValue & "'"
                    End If
                End If
                sql = sql & " And a.fecha between to_Date('" & dtpFechaDesde.Value.ToShortDateString & " 00:00:00','dd/mm/yyyy hh24:mi:ss') and (sysdate-1)
                      and b.estpro in(1,11,3,2,5,8)
                    group by a.codcnl, a.codpro, a.rutcli, a.precio_ori, a.precio_new, a.fecha "
                Datos_grafico = bd.sqlSelect(sql)

                ProgressBarX1.Value = 2
                Application.DoEvents()

                dgvDatos.DataSource = Datos_grafico
                dgvDatos.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells

            End If

            ProgressBarX1.Value = 3
            Application.DoEvents()

            sql = "select a.codcnl 
                   ,a.codpro 
                   ,a.rutcli
                   ,count(a.codpro) cantidad from (
            select a.codcnl, a.codpro, a.rutcli, a.precio_ori precio_venta, a.precio_new precio_lista, sum(a.cantidad) cantidad
            from cambios_precios a, ma_product b
            where a.codpro = b.codpro  
              and b.estpro in(1,11,3,2,5,8)"
            If cmbListas.SelectedValue <> 0 Then
                sql = sql & " And a.codcnl = " & cmbListas.SelectedValue
            End If
            If cmbNegocio.SelectedIndex <> -1 Then
                If cmbNegocio.SelectedValue.ToString <> "Sin negocio" Then
                    sql = sql & " and GETCLASIFICACION_CLIENTE(3, A.RUTCLI) = '" & cmbNegocio.SelectedValue & "'"
                End If
            End If
            sql = sql & " And a.fecha between to_Date('" & dtpFechaDesde.Value.ToShortDateString & " 00:00:00','dd/mm/yyyy hh24:mi:ss') and (sysdate-1)
            group by a.codcnl, a.codpro, a.rutcli, a.precio_ori, a.precio_new, a.fecha) a
            group by a.codcnl, a.codpro, a.rutcli
            order by cantidad desc"

            'sql = "select a.codcnl, a.codpro, a.rutcli, count(a.codpro) cantidad from cambios_precios a, ma_product b
            '        where a.codpro = b.codpro
            '          and b.estpro in(1,11,3,2,5,8)"
            'If cmbListas.SelectedValue <> 0 Then
            '    sql = sql & " And a.codcnl = " & cmbListas.SelectedValue
            'End If
            'sql = sql & " And a.fecha between to_Date('" & dtpFechaDesde.Value.ToShortDateString & " 00:00:00','dd/mm/yyyy hh24:mi:ss') and (sysdate-1)
            'group by a.codcnl, a.codpro, a.rutcli
            'order by cantidad desc"
            dt = bd.sqlSelect(sql)

            ProgressBarX1.Value = 4
            Application.DoEvents()

            dgvProductos.DataSource = dt
            dgvProductos.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells

            ProgressBarX1.Value = 5
            Application.DoEvents()

            'sql = "select a.codcnl, a.rutcli, a.codpro, count(a.rutcli) cantidad from cambios_precios a, ma_product b
            '        where a.codpro = b.codpro
            '          and b.estpro in(1,11,3,2,5,8)"
            'If cmbListas.SelectedValue <> 0 Then
            '    sql = sql & " And a.codcnl = " & cmbListas.SelectedValue
            'End If
            'sql = sql & " And a.fecha between to_Date('" & dtpFechaDesde.Value.ToShortDateString & " 00:00:00','dd/mm/yyyy hh24:mi:ss') and (sysdate-1)
            'group by a.codcnl, a.rutcli, a.codpro
            'order by cantidad desc"
            'dt1 = bd.sqlSelect(sql)

            'ProgressBarX1.Value = 6
            'Application.DoEvents()

            'dgvPuntos.DataSource = dt1
            'dgvPuntos.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells

            ProgressBarX1.Value = 7
            Application.DoEvents()

            MessageBox.Show("Proceso terminado", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            ProgressBarX1.Visible = False
            Application.DoEvents()
            bd.close()
        End Try
    End Sub

    Private Sub CargaDatosDobleClick()
        Try
            bd.open_dimerc()
            Dim dt, dt1 As New DataTable
            'Dim codcnl, codpro As String

            ProgressBarX1.Visible = True
            ProgressBarX1.Maximum = 3
            ProgressBarX1.Value = 0
            Application.DoEvents()

            ProgressBarX1.Value = 1
            Application.DoEvents()

            'PRODUCTOS CON CAMBIO DE PRECIO
            sql = "select a.codcnl, a.codpro, a.rutcli, a.precio_ori precio_venta, a.precio_new precio_lista, a.cantidad, a.fecha
                    from cambios_precios a, ma_product b
                    where a.codpro = b.codpro "
            If txtCodpro.Text.Trim <> "" Then
                sql = sql & " And a.codpro = '" & txtCodpro.Text & "'"
            End If
            If txtRutcli.Text.Trim <> "" Then
                sql = sql & " And a.rutcli = " & txtRutcli.Text
            End If
            If cmbListas.SelectedValue <> 0 Then
                sql = sql & "And a.codcnl = " & cmbListas.SelectedValue
            End If
            If cmbNegocio.SelectedIndex <> -1 Then
                If cmbNegocio.SelectedValue.ToString <> "Sin negocio" Then
                    sql = sql & " and GETCLASIFICACION_CLIENTE(3, A.RUTCLI) = '" & cmbNegocio.SelectedValue & "'"
                End If
            End If
            sql = sql & " And a.fecha between to_Date('" & dtpFechaDesde.Value.ToShortDateString & " 00:00:00','dd/mm/yyyy hh24:mi:ss') and (sysdate-1)
                      and b.estpro in(1,11,3,2,5,8) 
                    group by a.codcnl, a.codpro, a.rutcli, a.precio_ori, a.precio_new, a.cantidad, a.fecha"
            Datos_grafico = bd.sqlSelect(sql)

            ProgressBarX1.Value = 2
            Application.DoEvents()

            dgvDatos.DataSource = Datos_grafico
            dgvDatos.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells

            ProgressBarX1.Value = 3
            Application.DoEvents()

            MessageBox.Show("Proceso terminado", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            ProgressBarX1.Visible = False
            Application.DoEvents()
            bd.close()
        End Try
    End Sub

    Private Sub actualizaProducto(CODCNL As String, CODPRO As String, PRECIO_ORI As String, PRECIO_NEW As String, MARGEN_ORI As String, MARGEN_NEW As String, FECHA As Date, FECHA_FIN As Date, RUTCLI As String, CANTIDAD As String)
        Using conn = New OracleConnection(bd.retornaConexion)
            conn.Open()
            Dim comando As OracleCommand
            Dim dataAdapter As OracleDataAdapter
            Dim Sql As String
            Dim dt, dtaux As New DataTable
            Using transaccion = conn.BeginTransaction
                Try
                    Sql = "Set Role Rol_Aplicacion"
                    comando = New OracleCommand(Sql, conn, transaccion)
                    comando.ExecuteNonQuery()

                    Sql = "INSERT INTO CAMBIOS_PRECIOS (CODCNL, CODPRO, PRECIO_ORI, PRECIO_NEW, MARGEN_ORI, MARGEN_NEW, FECHA, FECHA_FIN, RUTCLI, CANTIDAD) 
                           VALUES(" & CODCNL & ", '" & CODPRO & "', " & PRECIO_ORI & ", " & PRECIO_NEW & ", " & MARGEN_ORI & ", " & MARGEN_NEW &
                           ", TO_DATE('" & FECHA.ToShortDateString & "','DD/MM/YYYY'), TO_DATE('" & FECHA_FIN.ToShortDateString & "','DD/MM/YYYY'), " & RUTCLI & ", " & CANTIDAD & ")"
                    comando = New OracleCommand(Sql, conn, transaccion)
                    comando.ExecuteNonQuery()

                    transaccion.Commit()

                Catch ex As Exception
                    If Not transaccion.Equals(Nothing) Then
                        transaccion.Rollback()
                    End If
                Finally
                    conn.Close()
                End Try
            End Using
        End Using
    End Sub

    Private Sub btnProcesar_Click(sender As Object, e As EventArgs) Handles btnProcesar.Click
        CargaDatos()
    End Sub

    Private Sub Grafica()
        Dim limite As Int32 = 0
        Grafico.Series.Clear()

        Dim elasticidad As Double

        'Dim valor As Int64 = Datos_grafico.Rows(0).Item("precio_ori")
        If cbxDosPuntos.Checked = True Then

            If lblPointOne.Text = "Punto 1: " And lblPointOne.Text = "Punto 2: " Then
                MessageBox.Show("Atención", "Debe tener puntos para graficar.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            ElseIf lblPointOne.Text <> "Punto 1: " And lblPointOne.Text = "Punto 2: " Then
                MessageBox.Show("Atención", "Debe tener los 2 puntos para graficar.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            ElseIf lblPointOne.Text = "Punto 1: " And lblPointOne.Text <> "Punto 2: " Then
                MessageBox.Show("Atención", "Debe tener los 2 puntos para graficar.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If

            Grafico.Series.Add("Linea Venta (P/C)")
            Grafico.Series(0).ChartType = DataVisualization.Charting.SeriesChartType.Area
            Grafico.Series(0).Color = Color.DarkSeaGreen
            Grafico.Series(0).Font = New System.Drawing.Font("Times New Roman", 11.0F, System.Drawing.FontStyle.Bold)
            Grafico.Series(0).IsValueShownAsLabel = True

            Grafico.Series.Add("Columna Venta (P/C)")
            Grafico.Series(1).ChartType = DataVisualization.Charting.SeriesChartType.RangeColumn
            Grafico.Series(1).Color = Color.Red

            'Grafico.Series(0).Points.AddXY(0, 0)

            Grafico.Series(0).Points.AddXY(Pi, Ci)
            Grafico.Series(0).Points.AddXY(Pf, Cf)
            Grafico.Series(0).Points.AddXY(Pi, Ci)
            Grafico.Series(0).Points.AddXY(Pf, Cf)
            Grafico.Series(1).Points.AddXY(Pi, Ci)
            Grafico.Series(1).Points.AddXY(Pf, Cf)
            Grafico.Series(1).Points.AddXY(Pi, Ci)
            Grafico.Series(1).Points.AddXY(Pf, Cf)

            MicroChart1.DataPoints = New List(Of Double)(New Double() {Ci, Cf})

            elasticidad = (((Cf - Ci) / Ci) - 100) / (((Pf - Pi) / Pi) - 100)
            If elasticidad = 0 Then
                lblElasticidad.Text = "resul. Perfecta"
            ElseIf elasticidad <= 1 Then
                lblElasticidad.Text = "resul. Inelastica"
            ElseIf elasticidad > 1 Then
                lblElasticidad.Text = "resul. Elastica"
            ElseIf elasticidad = 1 Then
                lblElasticidad.Text = "resul. Inelastica Unitaria"
            Else
                lblElasticidad.Text = "resul. PErfecta Elastica"
            End If

            'For i = (Datos_grafico.Rows.Count - 2) To (Datos_grafico.Rows.Count - 1)
            '    If Datos_grafico.Rows(i).Item("fecha") >= fecha Then
            '        'If Datos_grafico.Rows(i).Item("precio_ori") < valor Then
            '        '    valor = Datos_grafico.Rows(i).Item("precio_ori")
            '        'End If
            '        Grafico.Series(0).Points.AddXY(Datos_grafico.Rows(i).Item("precio_ori"), Datos_grafico.Rows(i).Item("cantidad"))
            '        Grafico.Series(1).Points.AddXY(Datos_grafico.Rows(i).Item("precio_ori"), Datos_grafico.Rows(i).Item("cantidad"))
            '    End If
            'Next

            ''Grafico.Series(1).Points.AddXY(valor, 0)
        ElseIf cbxGraficaTodo.Checked = True Then
            Grafico.Series.Clear()

            Grafico.Series.Add("Linea Venta ")
            Grafico.Series(0).ChartType = DataVisualization.Charting.SeriesChartType.Area
            Grafico.Series(0).Color = Color.DarkSeaGreen
            Grafico.Series(0).Font = New System.Drawing.Font("Times New Roman", 11.0F, System.Drawing.FontStyle.Bold)
            'Grafico.Series(0).IsValueShownAsLabel = True

            Grafico.Series.Add("Columna Venta ")
            Grafico.Series(1).ChartType = DataVisualization.Charting.SeriesChartType.RangeColumn
            Grafico.Series(1).Color = Color.Red

            Grafico.Series.Add("precio lista ")
            Grafico.Series(2).ChartType = DataVisualization.Charting.SeriesChartType.RangeColumn
            Grafico.Series(2).Color = Color.Blue

            Grafico2.Series.Add("Linea Venta ")
            Grafico2.Series(0).ChartType = DataVisualization.Charting.SeriesChartType.Area
            Grafico2.Series(0).Color = Color.DarkSeaGreen
            Grafico2.Series(0).Font = New System.Drawing.Font("Times New Roman", 11.0F, System.Drawing.FontStyle.Bold)
            'Grafico.Series(0).IsValueShownAsLabel = True

            Grafico2.Series.Add("Columna Venta ")
            Grafico2.Series(1).ChartType = DataVisualization.Charting.SeriesChartType.RangeColumn
            Grafico2.Series(1).Color = Color.Red

            Grafico2.Series.Add("precio lista ")
            Grafico2.Series(2).ChartType = DataVisualization.Charting.SeriesChartType.RangeColumn
            Grafico2.Series(2).Color = Color.Blue

            Dim lista As New List(Of Double)

            For i = 0 To dgvDatos.RowCount - 1
                Grafico.Series(0).Points.AddXY(dgvDatos.Item("precio_venta", i).Value, dgvDatos.Item("cantidad", i).Value)
                Grafico.Series(1).Points.AddXY(dgvDatos.Item("precio_venta", i).Value, dgvDatos.Item("cantidad", i).Value)
                Grafico.Series(2).Points.AddXY(dgvDatos.Item("precio_lista", i).Value, dgvDatos.Item("cantidad", i).Value)
                Grafico.Series(0).Points.AddXY(dgvDatos.Item("precio_venta", i).Value, dgvDatos.Item("cantidad", i).Value)
                Grafico.Series(1).Points.AddXY(dgvDatos.Item("precio_venta", i).Value, dgvDatos.Item("cantidad", i).Value)
                Grafico.Series(2).Points.AddXY(dgvDatos.Item("precio_lista", i).Value, dgvDatos.Item("cantidad", i).Value)
                lista.Add(Double.Parse(dgvDatos.Item("cantidad", i).Value))
            Next

            For i = 0 To dgvDatos.RowCount - 1
                Grafico2.Series(0).Points.AddXY(dgvDatos.Item("precio_venta", i).Value, dgvDatos.Item("fecha", i).Value)
                Grafico2.Series(1).Points.AddXY(dgvDatos.Item("precio_venta", i).Value, dgvDatos.Item("fecha", i).Value)
                Grafico2.Series(2).Points.AddXY(dgvDatos.Item("precio_lista", i).Value, dgvDatos.Item("fecha", i).Value)
                Grafico2.Series(0).Points.AddXY(dgvDatos.Item("precio_venta", i).Value, dgvDatos.Item("fecha", i).Value)
                Grafico2.Series(1).Points.AddXY(dgvDatos.Item("precio_venta", i).Value, dgvDatos.Item("fecha", i).Value)
                Grafico2.Series(2).Points.AddXY(dgvDatos.Item("precio_lista", i).Value, dgvDatos.Item("fecha", i).Value)
            Next

            'Grafico.Series(0).Points.AddXY(Int64.Parse(dgvPuntos.Item("precio", 0).Value), Int64.Parse(dgvPuntos.Item("cantidad", 0).Value))
            'Grafico.Series(1).Points.AddXY(Int64.Parse(dgvPuntos.Item("precio", 0).Value), Int64.Parse(dgvPuntos.Item("cantidad", 0).Value))
            'MicroChart1.DataPoints = New List(Of Double)(New Double() {Ci, Cf})

            MicroChart1.DataPoints = lista
            MicroChart2.DataPoints = lista

            elasticidad = (((dgvDatos.Item("cantidad", (dgvDatos.RowCount - 1)).Value - dgvDatos.Item("cantidad", 0).Value) / dgvDatos.Item("cantidad", 0).Value) - 100) / (((dgvDatos.Item("precio_venta", (dgvDatos.RowCount - 1)).Value - dgvDatos.Item("precio_venta", 0).Value) / dgvDatos.Item("precio_venta", 0).Value) - 100)

            If elasticidad = 0 Then
                lblElasticidad.Text = "resul. Perfecta"
                lblElasticidad2.Text = "resul. Perfecta"
            ElseIf elasticidad < 1 Then
                lblElasticidad.Text = "resul. Inelastica"
                lblElasticidad2.Text = "resul. Inelastica"
            ElseIf elasticidad > 1 Then
                lblElasticidad.Text = "resul. Elastica"
                lblElasticidad2.Text = "resul. Elastica"
            ElseIf elasticidad = 1 Then
                lblElasticidad.Text = "resul. Inelastica Unitaria"
                lblElasticidad2.Text = "resul. Inelastica Unitaria"
            Else
                lblElasticidad.Text = "resul. PErfecta Elastica"
                lblElasticidad2.Text = "resul. PErfecta Elastica"
            End If

        ElseIf cbxPuntos.Checked = True Then
            Grafico.Series.Clear()

            Grafico.Series.Add("Linea Venta (P/C)")
            Grafico.Series(0).ChartType = DataVisualization.Charting.SeriesChartType.Area
            Grafico.Series(0).Color = Color.DarkSeaGreen
            Grafico.Series(0).Font = New System.Drawing.Font("Times New Roman", 11.0F, System.Drawing.FontStyle.Bold)
            Grafico.Series(0).IsValueShownAsLabel = True

            Grafico.Series.Add("Columna Venta (P/C)")
            Grafico.Series(0).ChartType = DataVisualization.Charting.SeriesChartType.Area
            Grafico.Series(1).ChartType = DataVisualization.Charting.SeriesChartType.RangeColumn
            Grafico.Series(1).Color = Color.Red

            'Grafico.Series(0).Points.AddXY(0, 0)

            Dim lista As New List(Of Double)

            For i = 0 To dgvPuntos.RowCount - 1
                Grafico.Series(0).Points.AddXY(Int64.Parse(dgvPuntos.Item("precio", i).Value), Int64.Parse(dgvPuntos.Item("cantidad", i).Value))
                Grafico.Series(1).Points.AddXY(Int64.Parse(dgvPuntos.Item("precio", i).Value), Int64.Parse(dgvPuntos.Item("cantidad", i).Value))
                Grafico.Series(0).Points.AddXY(Int64.Parse(dgvPuntos.Item("precio", i).Value), Int64.Parse(dgvPuntos.Item("cantidad", i).Value))
                Grafico.Series(1).Points.AddXY(Int64.Parse(dgvPuntos.Item("precio", i).Value), Int64.Parse(dgvPuntos.Item("cantidad", i).Value))
                lista.Add(Double.Parse(dgvPuntos.Item("cantidad", i).Value))
            Next

            'Grafico.Series(0).Points.AddXY(Int64.Parse(dgvPuntos.Item("precio", 0).Value), Int64.Parse(dgvPuntos.Item("cantidad", 0).Value))
            'Grafico.Series(1).Points.AddXY(Int64.Parse(dgvPuntos.Item("precio", 0).Value), Int64.Parse(dgvPuntos.Item("cantidad", 0).Value))
            'MicroChart1.DataPoints = New List(Of Double)(New Double() {Ci, Cf})

            MicroChart1.DataPoints = lista

            elasticidad = (((dgvPuntos.Item("cantidad", (dgvPuntos.RowCount - 1)).Value - dgvPuntos.Item("cantidad", 0).Value) / dgvPuntos.Item("cantidad", 0).Value) - 100) / (((dgvPuntos.Item("precio", (dgvPuntos.RowCount - 1)).Value - dgvPuntos.Item("precio", 0).Value) / dgvPuntos.Item("precio", 0).Value) - 100)

            If elasticidad = 0 Then
                lblElasticidad.Text = "resul. Perfecta"
            ElseIf elasticidad < 1 Then
                lblElasticidad.Text = "resul. Inelastica"
            ElseIf elasticidad > 1 Then
                lblElasticidad.Text = "resul. Elastica"
            ElseIf elasticidad = 1 Then
                lblElasticidad.Text = "resul. Inelastica Unitaria"
            Else
                lblElasticidad.Text = "resul. PErfecta Elastica"
            End If

        End If
    End Sub

    Private Sub Chart1_MouseMove(sender As Object, e As MouseEventArgs) Handles Grafico.MouseMove
        Dim h As System.Windows.Forms.DataVisualization.Charting.HitTestResult = Grafico.HitTest(e.X, e.Y)
        If h.ChartElementType = DataVisualization.Charting.ChartElementType.DataPoint Then
            ToolTipProvider.SetToolTip(Grafico, h.Series.Points(h.PointIndex).XValue & " x " & h.Series.Points(h.PointIndex).YValues(0))
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs)
        dgvDatos.DataSource = consultar("Select * from Elasticidad")
    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles btnGraficar.Click
        TabControl1.SelectedIndex = 1
        Grafica()
    End Sub

    Private Sub dgvProductos_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvProductos.CellDoubleClick
        txtCodpro.Text = dgvProductos.Item("codpro", e.RowIndex).Value
        txtRutcli.Text = dgvProductos.Item("rutcli", e.RowIndex).Value

        CargaDatosDobleClick()
    End Sub

    Private Sub cbxGraficaTodo_Click(sender As Object, e As EventArgs) Handles cbxGraficaTodo.Click
        If cbxGraficaTodo.Checked = True Then
            cbxPuntos.Checked = False
            cbxDosPuntos.Checked = False
        End If
    End Sub

    Private Sub cbxPuntos_Click(sender As Object, e As EventArgs) Handles cbxPuntos.Click
        If cbxPuntos.Checked = True Then
            cbxGraficaTodo.Checked = False
            cbxDosPuntos.Checked = False
        End If
    End Sub

    Private Sub cbxDosPuntos_Click(sender As Object, e As EventArgs) Handles cbxDosPuntos.Click
        If cbxDosPuntos.Checked = True Then
            cbxPuntos.Checked = False
            cbxGraficaTodo.Checked = False
        End If
    End Sub

    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        System.Diagnostics.Process.Start("https://app.powerbi.com/view?r=eyJrIjoiYjFhOTg3MzAtYzhjMi00OGExLTgyMjQtZTYyYWU4YzEzNzBiIiwidCI6IjIzYmMzNThkLTc1ZGUtNGIzOS05OGZhLWQ5N2I2ZDBjYzFmMyIsImMiOjR9")
    End Sub

    Private Sub lblPointOne_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles lblPointOne.MouseDoubleClick
        lblPointOne.Text = "Punto 1: "
        Pi = New Double
        Pi = New Double
        fecha1 = New Date
    End Sub

    Private Sub lblPointTwo_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles lblPointTwo.MouseDoubleClick
        lblPointTwo.Text = "Punto 2: "
        Pf = New Double
        Pf = New Double
        fecha2 = New Date
    End Sub

    Private Sub dgvDatos_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvDatos.CellDoubleClick
        agregaPunto(Datos_grafico.Rows(e.RowIndex).Item("precio_venta"), Datos_grafico.Rows(e.RowIndex).Item("cantidad"), Datos_grafico.Rows(e.RowIndex).Item("fecha"))
    End Sub

    Private Sub agregaPunto(precio As String, cantidad As String, fecha As Date)
        If cbxPuntos.Checked = False Then
            'CUANDO LOS PUNTOS ESTAN VACIOS
            If lblPointOne.Text = "Punto 1: " And lblPointTwo.Text = "Punto 2: " Then
                lblPointOne.Text = "Punto 1: Precio=" & precio & " cantidad=" & cantidad & " fecha=" & fecha
                Pi = precio
                Ci = cantidad
                fecha1 = fecha
                Exit Sub
            End If
            'CUANDO SOLO EL SEGUNDO PUNTO ESTA VACIO
            If lblPointOne.Text <> "Punto 1: " And lblPointTwo.Text = "Punto 2: " Then
                If fecha > fecha1 Then
                    If precio = Pi Then
                        MessageBox.Show("No puede comparar dos precios iguales", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Exit Sub
                    End If
                    lblPointTwo.Text = "Punto 2: Precio=" & precio & " cantidad=" & cantidad & " fecha=" & fecha
                    Pf = precio
                    Cf = cantidad
                    fecha2 = fecha
                ElseIf fecha < fecha1 Then
                    lblPointTwo.Text = "Punto 2: Precio=" & Pi & " cantidad=" & Ci & " fecha=" & fecha1
                    Pf = Pi
                    Cf = Ci
                    fecha2 = fecha1
                    lblPointOne.Text = "Punto 1: Precio=" & precio & " cantidad=" & cantidad & " fecha=" & fecha
                    Pi = precio
                    Ci = cantidad
                    fecha1 = fecha
                End If
                Exit Sub
            End If
            'CUANDO SOLO EL PRIMER PUNTO ESTA VACIO 
            If lblPointOne.Text = "Punto 1: " And lblPointTwo.Text <> "Punto 2: " Then
                If fecha < fecha2 Then
                    If precio = Pf Then
                        MessageBox.Show("No puede comparar dos precios iguales", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Exit Sub
                    End If
                    lblPointOne.Text = "Punto 1: Precio=" & precio & " cantidad=" & cantidad & " fecha=" & fecha
                    Pi = precio
                    Ci = cantidad
                    fecha1 = fecha
                ElseIf fecha > fecha2 Then
                    lblPointOne.Text = "Punto 1: Precio=" & Pf & " cantidad=" & Cf & " fecha=" & fecha2
                    Pi = Pf
                    Ci = Cf
                    fecha1 = fecha2
                    lblPointTwo.Text = "Punto 1: Precio=" & precio & " cantidad=" & cantidad & " fecha=" & fecha
                    Pf = precio
                    Cf = cantidad
                    fecha2 = fecha
                End If
                Exit Sub
            End If
            'CUANDO NINGUN PUNTO ESTA VACIO
            If lblPointOne.Text <> "Punto 1: " And lblPointTwo.Text <> "Punto 2: " Then
                If fecha < fecha1 Then
                    lblPointOne.Text = "Punto 1: Precio=" & precio & " cantidad=" & cantidad & " fecha=" & fecha
                    Pi = precio
                    Ci = cantidad
                    fecha1 = fecha
                ElseIf fecha > fecha1 Then
                    lblPointTwo.Text = "Punto 2: Precio=" & precio & " cantidad=" & cantidad & " fecha=" & fecha
                    Pf = precio
                    Cf = cantidad
                    fecha2 = fecha
                End If
                Exit Sub
            End If
        Else
            dtPuntosGrafico.Rows.Add(precio, cantidad, fecha.Date.ToShortDateString)
            dgvPuntos.DataSource = dtPuntosGrafico
            dgvPuntos.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
        End If
    End Sub

    Private Sub dgvRut_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvPuntos.CellDoubleClick
        dtPuntosGrafico.Rows.RemoveAt(e.RowIndex)
        dgvPuntos.Refresh()
    End Sub

End Class