﻿Public Class frmAuditoriaProductos
    Public sql As String
    Public bd As New Conexion

    Private Sub frmProductosValidacion_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CargaDatos()
        PuntoControl.RegistroUso("119")
    End Sub

    Private Sub CargaDatos()
        Try
            bd.open_dimerc()
            Dim dt1, dt2, dt3, dt4, dt5 As New DataTable
            Loading.Visible = True
            Loading.Maximum = 5
            Loading.Value = 0
            Application.DoEvents()

            'PRODUCTOS CON COSTO INVENTARIO IGUAL O MENOR A 0
            sql = "SELECT A.CODPRO, 
                           A.DESPRO, 
                           GETDOMINIO(1002, A.CODNEG) NEGOCIO,
                           GETLINEA(CODPRO) LINEA,
                           GETMARCA(CODPRO) MARCA, 
                           GETCATEGORIA_PRODUCTO(3, A.CODPRO) CATEGORIA,
                           A.COSPROM ""COSTO INVENTARIO""
                    FROM MA_PRODUCT A
                    WHERE A.ESTPRO IN(1,11)
                      AND A.CODNEG = 0
                      AND (A.cosprom <= 0 OR A.cosprom IS NULL)
                      AND A.costo = 0 "
            dt1 = bd.sqlSelect(sql)
            dgvSkuSinCI.DataSource = dt1

            Loading.Value = 1
            Application.DoEvents()

            'PRODUCTOS CON COSTO COMERCIAL IGUAL O MENOR A 0
            sql = "SELECT A.CODPRO, 
                           A.DESPRO, 
                           GETDOMINIO(1002, A.CODNEG) NEGOCIO,
                           GETLINEA(CODPRO) LINEA,
                           GETMARCA(CODPRO) MARCA, 
                           GETCATEGORIA_PRODUCTO(3, A.CODPRO) CATEGORIA,
                           A.COSTO ""COSTO COMERCIAL""
                    FROM MA_PRODUCT A
                    WHERE A.ESTPRO IN(1,11)
                      AND A.CODNEG = 0
                      AND (A.costo <= 0 OR A.costo IS NULL)"
            dt2 = bd.sqlSelect(sql)
            dgvSkuSinCC.DataSource = dt2

            Loading.Value = 2
            Application.DoEvents()

            'PRODUCTOS CON PRECIO DIFERENTE AL CALCULADO
            sql = "Select a.codcnl,
                          a.codpro,
                          a.despro,
                          a.precio, 
                          round((nvl(c.costo,0) / (1 - (a.margen / 100))),0) precio_calculado 
                         ,Round((nvl(c.costo,0) / (1 - (a.margen / 100))),0) newprecio
                         ,getlinea(a.codpro) linea
                         ,a.margen,
                         c.costo,
                         (a.precio - round((nvl(c.costo,0) / (1 - (a.margen / 100))),0)) diferencia
                    From ma_product c, re_canprod a, ma_listaprecio b
                    Where nvl(c.costo,0)>0
                      and a.codpro=c.codpro 
                      and a.precio<>round((nvl(c.costo,0) / (1 - (a.margen / 100))),0)
                      and a.codemp=3
                      and b.codcnl=a.codcnl
                      and b.activa='S'
                      and b.actualiza='S'
                      and b.codcnl between 600 and 940
                      and b.codemp=a.codemp
                      and c.estpro in(1, 11)
                      and c.codneg = 0
                      and not exists (select 1 from ma_relaprod z where z.codemp=a.codemp and z.tiporela=4 and z.codpro2=a.codpro)"
            dt3 = bd.sqlSelect(sql)
            dgvSkuPrecioDC.DataSource = dt3

            Loading.Value = 3
            Application.DoEvents()

            'PRODUCTOS CON PRECIOS IGUAL O MENOS A 0
            sql = "select B.CODPRO, 
                           B.DESPRO, 
                           GETDOMINIO(1002, B.CODNEG) NEGOCIO,
                           GETLINEA(B.CODPRO) LINEA,
                           GETMARCA(B.CODPRO) MARCA, 
                           GETCATEGORIA_PRODUCTO(3, B.CODPRO) CATEGORIA,
                           NVL(B.COSTO,0) ""COSTO COMERCIAL"",
                           NVL(B.COSPROM,0) ""COSTO INVENTARIO"",
                           NVL(A.PRECIO,0) PRECIO  
                    from re_Canprod a, ma_product b
                    where a.codcnl between 900 and 933
                      and a.precio <= 0 
                      and a.codpro = b.codpro
                      and b.estpro in(1,11)
                      and b.codpro <> '0000'
                      AND B.CODNEG = 0
                    group by B.CODPRO, B.DESPRO, B.COSTO, B.COSPROM, A.PRECIO, B.CODNEG"
            dt4 = bd.sqlSelect(sql)
            dgvSkuSinPrecio.DataSource = dt4

            Loading.Value = 4
            Application.DoEvents()

            'PRODUCTOS SIN DELTA
            sql = "select B.CODPRO, 
                           B.DESPRO, 
                           GETDOMINIO(20, B.CODPRO) ESTADO,
                           GETDOMINIO(1002, B.CODNEG) NEGOCIO,
                           GETLINEA(B.CODPRO) LINEA,
                           GETMARCA(B.CODPRO) MARCA, 
                           GETCATEGORIA_PRODUCTO(3, B.CODPRO) CATEGORIA,
                           B.COSTO ""COSTO COMERCIAL"",
                           B.COSPROM ""COSTO INVENTARIO""
                    from ma_product b
                    where b.estpro in(1,11)
                      and not exists (select 1 from ma_costo_comercial x where x.codpro = b.codpro)"
            dt5 = bd.sqlSelect(sql)
            dgvSkuSinDelta.DataSource = dt5

            Loading.Value = 5
            Application.DoEvents()

            dgvSkuSinCI.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            dgvSkuSinCC.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            dgvSkuPrecioDC.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            dgvSkuSinPrecio.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            dgvSkuSinDelta.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells

            lblTotal1.Text = "Total: " & dgvSkuSinCI.RowCount
            lblTotal2.Text = "Total: " & dgvSkuSinCC.RowCount
            lblTotal3.Text = "Total: " & dgvSkuPrecioDC.RowCount
            lblTotal4.Text = "Total: " & dgvSkuSinPrecio.RowCount
            lblTotal5.Text = "Total: " & dgvSkuSinDelta.RowCount

            Loading.Visible = False

        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            bd.close()
            Loading.Visible = False
        End Try
    End Sub

    Private Sub MetroButton1_Click(sender As Object, e As EventArgs) Handles btnActualizar.Click
        btnActualizar.Enabled = False
        CargaDatos()
        btnActualizar.Enabled = True
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If MetroTabControl1.SelectedIndex = 0 Then
            exportar(dgvSkuSinCI)
        ElseIf MetroTabControl1.SelectedIndex = 1 Then
            exportar(dgvSkuSinCC)
        ElseIf MetroTabControl1.SelectedIndex = 2 Then
            exportar(dgvSkuPrecioDC)
        ElseIf MetroTabControl1.SelectedIndex = 3 Then
            exportar(dgvSkuSinPrecio)
        ElseIf MetroTabControl1.SelectedIndex = 4 Then
            exportar(dgvSkuSinDelta)
        End If
    End Sub

    Private Sub exportar(tabla As DataGridView)
        Dim save As New SaveFileDialog
        If tabla.RowCount = 0 Then
            MessageBox.Show("No hay datos consultados en la grilla.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        Else
            save.Filter = "Archivo Excel | *.xlsx"
            If save.ShowDialog = System.Windows.Forms.DialogResult.OK Then
                Globales.exporta(save.FileName, tabla)
            End If
        End If
    End Sub

End Class