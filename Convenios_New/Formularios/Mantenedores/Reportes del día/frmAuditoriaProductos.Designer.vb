﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmAuditoriaProductos
    Inherits MetroFramework.Forms.MetroForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.MetroTabControl1 = New MetroFramework.Controls.MetroTabControl()
        Me.MetroTabPage1 = New MetroFramework.Controls.MetroTabPage()
        Me.lblTotal1 = New System.Windows.Forms.Label()
        Me.dgvSkuSinCI = New System.Windows.Forms.DataGridView()
        Me.MetroTabPage2 = New MetroFramework.Controls.MetroTabPage()
        Me.lblTotal2 = New System.Windows.Forms.Label()
        Me.dgvSkuSinCC = New System.Windows.Forms.DataGridView()
        Me.MetroTabPage3 = New MetroFramework.Controls.MetroTabPage()
        Me.lblTotal3 = New System.Windows.Forms.Label()
        Me.dgvSkuPrecioDC = New System.Windows.Forms.DataGridView()
        Me.MetroTabPage4 = New MetroFramework.Controls.MetroTabPage()
        Me.lblTotal4 = New System.Windows.Forms.Label()
        Me.dgvSkuSinPrecio = New System.Windows.Forms.DataGridView()
        Me.MetroTabPage5 = New MetroFramework.Controls.MetroTabPage()
        Me.lblTotal5 = New System.Windows.Forms.Label()
        Me.dgvSkuSinDelta = New System.Windows.Forms.DataGridView()
        Me.btnActualizar = New MetroFramework.Controls.MetroButton()
        Me.Loading = New MetroFramework.Controls.MetroProgressSpinner()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.MetroTabControl1.SuspendLayout()
        Me.MetroTabPage1.SuspendLayout()
        CType(Me.dgvSkuSinCI, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MetroTabPage2.SuspendLayout()
        CType(Me.dgvSkuSinCC, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MetroTabPage3.SuspendLayout()
        CType(Me.dgvSkuPrecioDC, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MetroTabPage4.SuspendLayout()
        CType(Me.dgvSkuSinPrecio, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MetroTabPage5.SuspendLayout()
        CType(Me.dgvSkuSinDelta, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MetroTabControl1
        '
        Me.MetroTabControl1.Controls.Add(Me.MetroTabPage1)
        Me.MetroTabControl1.Controls.Add(Me.MetroTabPage2)
        Me.MetroTabControl1.Controls.Add(Me.MetroTabPage3)
        Me.MetroTabControl1.Controls.Add(Me.MetroTabPage4)
        Me.MetroTabControl1.Controls.Add(Me.MetroTabPage5)
        Me.MetroTabControl1.Location = New System.Drawing.Point(12, 63)
        Me.MetroTabControl1.Name = "MetroTabControl1"
        Me.MetroTabControl1.SelectedIndex = 4
        Me.MetroTabControl1.Size = New System.Drawing.Size(800, 559)
        Me.MetroTabControl1.TabIndex = 1
        Me.MetroTabControl1.UseSelectable = True
        '
        'MetroTabPage1
        '
        Me.MetroTabPage1.Controls.Add(Me.lblTotal1)
        Me.MetroTabPage1.Controls.Add(Me.dgvSkuSinCI)
        Me.MetroTabPage1.HorizontalScrollbarBarColor = True
        Me.MetroTabPage1.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroTabPage1.HorizontalScrollbarSize = 10
        Me.MetroTabPage1.Location = New System.Drawing.Point(4, 38)
        Me.MetroTabPage1.Name = "MetroTabPage1"
        Me.MetroTabPage1.Size = New System.Drawing.Size(792, 517)
        Me.MetroTabPage1.TabIndex = 0
        Me.MetroTabPage1.Text = "Productos Sin CI"
        Me.MetroTabPage1.VerticalScrollbarBarColor = True
        Me.MetroTabPage1.VerticalScrollbarHighlightOnWheel = False
        Me.MetroTabPage1.VerticalScrollbarSize = 10
        '
        'lblTotal1
        '
        Me.lblTotal1.AutoSize = True
        Me.lblTotal1.Location = New System.Drawing.Point(673, 497)
        Me.lblTotal1.Name = "lblTotal1"
        Me.lblTotal1.Size = New System.Drawing.Size(37, 13)
        Me.lblTotal1.TabIndex = 8
        Me.lblTotal1.Text = "Total: "
        '
        'dgvSkuSinCI
        '
        Me.dgvSkuSinCI.AllowUserToAddRows = False
        Me.dgvSkuSinCI.AllowUserToDeleteRows = False
        Me.dgvSkuSinCI.AllowUserToResizeRows = False
        Me.dgvSkuSinCI.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSkuSinCI.Location = New System.Drawing.Point(0, 14)
        Me.dgvSkuSinCI.Name = "dgvSkuSinCI"
        Me.dgvSkuSinCI.ReadOnly = True
        Me.dgvSkuSinCI.RowHeadersVisible = False
        Me.dgvSkuSinCI.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvSkuSinCI.Size = New System.Drawing.Size(790, 480)
        Me.dgvSkuSinCI.TabIndex = 5
        '
        'MetroTabPage2
        '
        Me.MetroTabPage2.Controls.Add(Me.lblTotal2)
        Me.MetroTabPage2.Controls.Add(Me.dgvSkuSinCC)
        Me.MetroTabPage2.HorizontalScrollbarBarColor = True
        Me.MetroTabPage2.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroTabPage2.HorizontalScrollbarSize = 10
        Me.MetroTabPage2.Location = New System.Drawing.Point(4, 38)
        Me.MetroTabPage2.Name = "MetroTabPage2"
        Me.MetroTabPage2.Size = New System.Drawing.Size(792, 517)
        Me.MetroTabPage2.TabIndex = 1
        Me.MetroTabPage2.Text = "Productos Sin CC"
        Me.MetroTabPage2.VerticalScrollbarBarColor = True
        Me.MetroTabPage2.VerticalScrollbarHighlightOnWheel = False
        Me.MetroTabPage2.VerticalScrollbarSize = 10
        '
        'lblTotal2
        '
        Me.lblTotal2.AutoSize = True
        Me.lblTotal2.Location = New System.Drawing.Point(694, 491)
        Me.lblTotal2.Name = "lblTotal2"
        Me.lblTotal2.Size = New System.Drawing.Size(37, 13)
        Me.lblTotal2.TabIndex = 8
        Me.lblTotal2.Text = "Total: "
        '
        'dgvSkuSinCC
        '
        Me.dgvSkuSinCC.AllowUserToAddRows = False
        Me.dgvSkuSinCC.AllowUserToDeleteRows = False
        Me.dgvSkuSinCC.AllowUserToResizeRows = False
        Me.dgvSkuSinCC.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSkuSinCC.Location = New System.Drawing.Point(1, 8)
        Me.dgvSkuSinCC.Name = "dgvSkuSinCC"
        Me.dgvSkuSinCC.ReadOnly = True
        Me.dgvSkuSinCC.RowHeadersVisible = False
        Me.dgvSkuSinCC.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvSkuSinCC.Size = New System.Drawing.Size(790, 480)
        Me.dgvSkuSinCC.TabIndex = 6
        '
        'MetroTabPage3
        '
        Me.MetroTabPage3.Controls.Add(Me.lblTotal3)
        Me.MetroTabPage3.Controls.Add(Me.dgvSkuPrecioDC)
        Me.MetroTabPage3.HorizontalScrollbarBarColor = True
        Me.MetroTabPage3.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroTabPage3.HorizontalScrollbarSize = 10
        Me.MetroTabPage3.Location = New System.Drawing.Point(4, 38)
        Me.MetroTabPage3.Name = "MetroTabPage3"
        Me.MetroTabPage3.Size = New System.Drawing.Size(792, 517)
        Me.MetroTabPage3.TabIndex = 2
        Me.MetroTabPage3.Text = "SKU Precio DC"
        Me.MetroTabPage3.VerticalScrollbarBarColor = True
        Me.MetroTabPage3.VerticalScrollbarHighlightOnWheel = False
        Me.MetroTabPage3.VerticalScrollbarSize = 10
        '
        'lblTotal3
        '
        Me.lblTotal3.AutoSize = True
        Me.lblTotal3.Location = New System.Drawing.Point(682, 493)
        Me.lblTotal3.Name = "lblTotal3"
        Me.lblTotal3.Size = New System.Drawing.Size(37, 13)
        Me.lblTotal3.TabIndex = 8
        Me.lblTotal3.Text = "Total: "
        '
        'dgvSkuPrecioDC
        '
        Me.dgvSkuPrecioDC.AllowUserToAddRows = False
        Me.dgvSkuPrecioDC.AllowUserToDeleteRows = False
        Me.dgvSkuPrecioDC.AllowUserToResizeRows = False
        Me.dgvSkuPrecioDC.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSkuPrecioDC.Location = New System.Drawing.Point(1, 8)
        Me.dgvSkuPrecioDC.Name = "dgvSkuPrecioDC"
        Me.dgvSkuPrecioDC.ReadOnly = True
        Me.dgvSkuPrecioDC.RowHeadersVisible = False
        Me.dgvSkuPrecioDC.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvSkuPrecioDC.Size = New System.Drawing.Size(790, 480)
        Me.dgvSkuPrecioDC.TabIndex = 6
        '
        'MetroTabPage4
        '
        Me.MetroTabPage4.Controls.Add(Me.lblTotal4)
        Me.MetroTabPage4.Controls.Add(Me.dgvSkuSinPrecio)
        Me.MetroTabPage4.HorizontalScrollbarBarColor = True
        Me.MetroTabPage4.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroTabPage4.HorizontalScrollbarSize = 10
        Me.MetroTabPage4.Location = New System.Drawing.Point(4, 38)
        Me.MetroTabPage4.Name = "MetroTabPage4"
        Me.MetroTabPage4.Size = New System.Drawing.Size(792, 517)
        Me.MetroTabPage4.TabIndex = 3
        Me.MetroTabPage4.Text = "SKU Precio 0"
        Me.MetroTabPage4.VerticalScrollbarBarColor = True
        Me.MetroTabPage4.VerticalScrollbarHighlightOnWheel = False
        Me.MetroTabPage4.VerticalScrollbarSize = 10
        '
        'lblTotal4
        '
        Me.lblTotal4.AutoSize = True
        Me.lblTotal4.Location = New System.Drawing.Point(702, 491)
        Me.lblTotal4.Name = "lblTotal4"
        Me.lblTotal4.Size = New System.Drawing.Size(37, 13)
        Me.lblTotal4.TabIndex = 8
        Me.lblTotal4.Text = "Total: "
        '
        'dgvSkuSinPrecio
        '
        Me.dgvSkuSinPrecio.AllowUserToAddRows = False
        Me.dgvSkuSinPrecio.AllowUserToDeleteRows = False
        Me.dgvSkuSinPrecio.AllowUserToResizeRows = False
        Me.dgvSkuSinPrecio.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSkuSinPrecio.Location = New System.Drawing.Point(1, 8)
        Me.dgvSkuSinPrecio.Name = "dgvSkuSinPrecio"
        Me.dgvSkuSinPrecio.ReadOnly = True
        Me.dgvSkuSinPrecio.RowHeadersVisible = False
        Me.dgvSkuSinPrecio.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvSkuSinPrecio.Size = New System.Drawing.Size(790, 480)
        Me.dgvSkuSinPrecio.TabIndex = 6
        '
        'MetroTabPage5
        '
        Me.MetroTabPage5.Controls.Add(Me.lblTotal5)
        Me.MetroTabPage5.Controls.Add(Me.dgvSkuSinDelta)
        Me.MetroTabPage5.HorizontalScrollbarBarColor = True
        Me.MetroTabPage5.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroTabPage5.HorizontalScrollbarSize = 10
        Me.MetroTabPage5.Location = New System.Drawing.Point(4, 38)
        Me.MetroTabPage5.Name = "MetroTabPage5"
        Me.MetroTabPage5.Size = New System.Drawing.Size(792, 517)
        Me.MetroTabPage5.TabIndex = 4
        Me.MetroTabPage5.Text = "SKU sin Delta"
        Me.MetroTabPage5.VerticalScrollbarBarColor = True
        Me.MetroTabPage5.VerticalScrollbarHighlightOnWheel = False
        Me.MetroTabPage5.VerticalScrollbarSize = 10
        '
        'lblTotal5
        '
        Me.lblTotal5.AutoSize = True
        Me.lblTotal5.Location = New System.Drawing.Point(671, 493)
        Me.lblTotal5.Name = "lblTotal5"
        Me.lblTotal5.Size = New System.Drawing.Size(37, 13)
        Me.lblTotal5.TabIndex = 7
        Me.lblTotal5.Text = "Total: "
        '
        'dgvSkuSinDelta
        '
        Me.dgvSkuSinDelta.AllowUserToAddRows = False
        Me.dgvSkuSinDelta.AllowUserToDeleteRows = False
        Me.dgvSkuSinDelta.AllowUserToResizeRows = False
        Me.dgvSkuSinDelta.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSkuSinDelta.Location = New System.Drawing.Point(1, 8)
        Me.dgvSkuSinDelta.Name = "dgvSkuSinDelta"
        Me.dgvSkuSinDelta.ReadOnly = True
        Me.dgvSkuSinDelta.RowHeadersVisible = False
        Me.dgvSkuSinDelta.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvSkuSinDelta.Size = New System.Drawing.Size(790, 480)
        Me.dgvSkuSinDelta.TabIndex = 6
        '
        'btnActualizar
        '
        Me.btnActualizar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnActualizar.FontSize = MetroFramework.MetroButtonSize.Medium
        Me.btnActualizar.Location = New System.Drawing.Point(514, 20)
        Me.btnActualizar.Name = "btnActualizar"
        Me.btnActualizar.Size = New System.Drawing.Size(75, 23)
        Me.btnActualizar.Style = MetroFramework.MetroColorStyle.Black
        Me.btnActualizar.TabIndex = 2
        Me.btnActualizar.Text = "Actualizar"
        Me.btnActualizar.UseSelectable = True
        '
        'Loading
        '
        Me.Loading.Location = New System.Drawing.Point(595, 20)
        Me.Loading.Maximum = 100
        Me.Loading.Name = "Loading"
        Me.Loading.Size = New System.Drawing.Size(40, 37)
        Me.Loading.TabIndex = 3
        Me.Loading.UseSelectable = True
        Me.Loading.Visible = False
        '
        'Button1
        '
        Me.Button1.Image = Global.Convenios_New.My.Resources.Resources.Excel
        Me.Button1.Location = New System.Drawing.Point(680, 20)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(46, 45)
        Me.Button1.TabIndex = 4
        Me.Button1.UseVisualStyleBackColor = True
        '
        'frmProductosValidacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(822, 627)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Loading)
        Me.Controls.Add(Me.btnActualizar)
        Me.Controls.Add(Me.MetroTabControl1)
        Me.Name = "frmProductosValidacion"
        Me.Text = "Auditoria Productos"
        Me.MetroTabControl1.ResumeLayout(False)
        Me.MetroTabPage1.ResumeLayout(False)
        Me.MetroTabPage1.PerformLayout()
        CType(Me.dgvSkuSinCI, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MetroTabPage2.ResumeLayout(False)
        Me.MetroTabPage2.PerformLayout()
        CType(Me.dgvSkuSinCC, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MetroTabPage3.ResumeLayout(False)
        Me.MetroTabPage3.PerformLayout()
        CType(Me.dgvSkuPrecioDC, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MetroTabPage4.ResumeLayout(False)
        Me.MetroTabPage4.PerformLayout()
        CType(Me.dgvSkuSinPrecio, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MetroTabPage5.ResumeLayout(False)
        Me.MetroTabPage5.PerformLayout()
        CType(Me.dgvSkuSinDelta, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents MetroTabControl1 As MetroFramework.Controls.MetroTabControl
    Friend WithEvents MetroTabPage1 As MetroFramework.Controls.MetroTabPage
    Friend WithEvents MetroTabPage2 As MetroFramework.Controls.MetroTabPage
    Friend WithEvents MetroTabPage3 As MetroFramework.Controls.MetroTabPage
    Friend WithEvents MetroTabPage4 As MetroFramework.Controls.MetroTabPage
    Friend WithEvents MetroTabPage5 As MetroFramework.Controls.MetroTabPage
    Friend WithEvents dgvSkuSinCI As DataGridView
    Friend WithEvents dgvSkuSinCC As DataGridView
    Friend WithEvents dgvSkuPrecioDC As DataGridView
    Friend WithEvents dgvSkuSinPrecio As DataGridView
    Friend WithEvents dgvSkuSinDelta As DataGridView
    Friend WithEvents btnActualizar As MetroFramework.Controls.MetroButton
    Friend WithEvents Loading As MetroFramework.Controls.MetroProgressSpinner
    Friend WithEvents Button1 As Button
    Friend WithEvents lblTotal1 As Label
    Friend WithEvents lblTotal2 As Label
    Friend WithEvents lblTotal3 As Label
    Friend WithEvents lblTotal4 As Label
    Friend WithEvents lblTotal5 As Label
End Class
