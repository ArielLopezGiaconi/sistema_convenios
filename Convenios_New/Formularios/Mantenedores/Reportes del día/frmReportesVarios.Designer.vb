﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReportesVarios
    Inherits MetroFramework.Forms.MetroForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MetroTabControl1 = New MetroFramework.Controls.MetroTabControl()
        Me.MetroTabPage1 = New MetroFramework.Controls.MetroTabPage()
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.dtpHasta = New MetroFramework.Controls.MetroDateTime()
        Me.dtpDesde = New MetroFramework.Controls.MetroDateTime()
        Me.btnActualizar = New MetroFramework.Controls.MetroButton()
        Me.lblTotal1 = New System.Windows.Forms.Label()
        Me.dgvTransaccionabilidad = New System.Windows.Forms.DataGridView()
        Me.MetroTabPage2 = New MetroFramework.Controls.MetroTabPage()
        Me.btnbuscar = New MetroFramework.Controls.MetroButton()
        Me.lblTotal2 = New System.Windows.Forms.Label()
        Me.dgvDelta = New System.Windows.Forms.DataGridView()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.MetroTabControl1.SuspendLayout()
        Me.MetroTabPage1.SuspendLayout()
        CType(Me.dgvTransaccionabilidad, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MetroTabPage2.SuspendLayout()
        CType(Me.dgvDelta, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MetroTabControl1
        '
        Me.MetroTabControl1.Controls.Add(Me.MetroTabPage1)
        Me.MetroTabControl1.Controls.Add(Me.MetroTabPage2)
        Me.MetroTabControl1.Location = New System.Drawing.Point(7, 63)
        Me.MetroTabControl1.Name = "MetroTabControl1"
        Me.MetroTabControl1.SelectedIndex = 0
        Me.MetroTabControl1.Size = New System.Drawing.Size(912, 559)
        Me.MetroTabControl1.TabIndex = 2
        Me.MetroTabControl1.UseSelectable = True
        '
        'MetroTabPage1
        '
        Me.MetroTabPage1.Controls.Add(Me.MetroLabel2)
        Me.MetroTabPage1.Controls.Add(Me.MetroLabel1)
        Me.MetroTabPage1.Controls.Add(Me.dtpHasta)
        Me.MetroTabPage1.Controls.Add(Me.dtpDesde)
        Me.MetroTabPage1.Controls.Add(Me.btnActualizar)
        Me.MetroTabPage1.Controls.Add(Me.lblTotal1)
        Me.MetroTabPage1.Controls.Add(Me.dgvTransaccionabilidad)
        Me.MetroTabPage1.HorizontalScrollbarBarColor = True
        Me.MetroTabPage1.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroTabPage1.HorizontalScrollbarSize = 10
        Me.MetroTabPage1.Location = New System.Drawing.Point(4, 38)
        Me.MetroTabPage1.Name = "MetroTabPage1"
        Me.MetroTabPage1.Size = New System.Drawing.Size(904, 517)
        Me.MetroTabPage1.TabIndex = 0
        Me.MetroTabPage1.Text = "Transaccionabilidad"
        Me.MetroTabPage1.VerticalScrollbarBarColor = True
        Me.MetroTabPage1.VerticalScrollbarHighlightOnWheel = False
        Me.MetroTabPage1.VerticalScrollbarSize = 10
        '
        'MetroLabel2
        '
        Me.MetroLabel2.AutoSize = True
        Me.MetroLabel2.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel2.Location = New System.Drawing.Point(380, 17)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(58, 25)
        Me.MetroLabel2.TabIndex = 13
        Me.MetroLabel2.Text = "Hasta:"
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel1.Location = New System.Drawing.Point(12, 17)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(63, 25)
        Me.MetroLabel1.TabIndex = 12
        Me.MetroLabel1.Text = "Desde:"
        '
        'dtpHasta
        '
        Me.dtpHasta.Location = New System.Drawing.Point(444, 13)
        Me.dtpHasta.MinimumSize = New System.Drawing.Size(0, 29)
        Me.dtpHasta.Name = "dtpHasta"
        Me.dtpHasta.Size = New System.Drawing.Size(296, 29)
        Me.dtpHasta.TabIndex = 11
        '
        'dtpDesde
        '
        Me.dtpDesde.Location = New System.Drawing.Point(75, 13)
        Me.dtpDesde.MinimumSize = New System.Drawing.Size(0, 29)
        Me.dtpDesde.Name = "dtpDesde"
        Me.dtpDesde.Size = New System.Drawing.Size(290, 29)
        Me.dtpDesde.TabIndex = 10
        '
        'btnActualizar
        '
        Me.btnActualizar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnActualizar.FontSize = MetroFramework.MetroButtonSize.Tall
        Me.btnActualizar.Location = New System.Drawing.Point(796, 16)
        Me.btnActualizar.Name = "btnActualizar"
        Me.btnActualizar.Size = New System.Drawing.Size(75, 23)
        Me.btnActualizar.Style = MetroFramework.MetroColorStyle.Black
        Me.btnActualizar.TabIndex = 9
        Me.btnActualizar.Text = "Buscar"
        Me.btnActualizar.UseSelectable = True
        '
        'lblTotal1
        '
        Me.lblTotal1.AutoSize = True
        Me.lblTotal1.Location = New System.Drawing.Point(781, 497)
        Me.lblTotal1.Name = "lblTotal1"
        Me.lblTotal1.Size = New System.Drawing.Size(37, 13)
        Me.lblTotal1.TabIndex = 8
        Me.lblTotal1.Text = "Total: "
        '
        'dgvTransaccionabilidad
        '
        Me.dgvTransaccionabilidad.AllowUserToAddRows = False
        Me.dgvTransaccionabilidad.AllowUserToDeleteRows = False
        Me.dgvTransaccionabilidad.AllowUserToResizeRows = False
        Me.dgvTransaccionabilidad.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTransaccionabilidad.Location = New System.Drawing.Point(0, 54)
        Me.dgvTransaccionabilidad.Name = "dgvTransaccionabilidad"
        Me.dgvTransaccionabilidad.ReadOnly = True
        Me.dgvTransaccionabilidad.RowHeadersVisible = False
        Me.dgvTransaccionabilidad.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvTransaccionabilidad.Size = New System.Drawing.Size(894, 440)
        Me.dgvTransaccionabilidad.TabIndex = 5
        '
        'MetroTabPage2
        '
        Me.MetroTabPage2.Controls.Add(Me.btnbuscar)
        Me.MetroTabPage2.Controls.Add(Me.lblTotal2)
        Me.MetroTabPage2.Controls.Add(Me.dgvDelta)
        Me.MetroTabPage2.HorizontalScrollbarBarColor = True
        Me.MetroTabPage2.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroTabPage2.HorizontalScrollbarSize = 10
        Me.MetroTabPage2.Location = New System.Drawing.Point(4, 38)
        Me.MetroTabPage2.Name = "MetroTabPage2"
        Me.MetroTabPage2.Size = New System.Drawing.Size(904, 517)
        Me.MetroTabPage2.TabIndex = 1
        Me.MetroTabPage2.Text = "Delta vs C.Monetaria"
        Me.MetroTabPage2.VerticalScrollbarBarColor = True
        Me.MetroTabPage2.VerticalScrollbarHighlightOnWheel = False
        Me.MetroTabPage2.VerticalScrollbarSize = 10
        '
        'btnbuscar
        '
        Me.btnbuscar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnbuscar.FontSize = MetroFramework.MetroButtonSize.Tall
        Me.btnbuscar.Location = New System.Drawing.Point(3, 8)
        Me.btnbuscar.Name = "btnbuscar"
        Me.btnbuscar.Size = New System.Drawing.Size(75, 23)
        Me.btnbuscar.Style = MetroFramework.MetroColorStyle.Black
        Me.btnbuscar.TabIndex = 10
        Me.btnbuscar.Text = "Buscar"
        Me.btnbuscar.UseSelectable = True
        '
        'lblTotal2
        '
        Me.lblTotal2.AutoSize = True
        Me.lblTotal2.Location = New System.Drawing.Point(778, 491)
        Me.lblTotal2.Name = "lblTotal2"
        Me.lblTotal2.Size = New System.Drawing.Size(37, 13)
        Me.lblTotal2.TabIndex = 8
        Me.lblTotal2.Text = "Total: "
        '
        'dgvDelta
        '
        Me.dgvDelta.AllowUserToAddRows = False
        Me.dgvDelta.AllowUserToDeleteRows = False
        Me.dgvDelta.AllowUserToResizeRows = False
        Me.dgvDelta.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDelta.Location = New System.Drawing.Point(1, 37)
        Me.dgvDelta.Name = "dgvDelta"
        Me.dgvDelta.ReadOnly = True
        Me.dgvDelta.RowHeadersVisible = False
        Me.dgvDelta.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDelta.Size = New System.Drawing.Size(903, 451)
        Me.dgvDelta.TabIndex = 6
        '
        'Button1
        '
        Me.Button1.Image = Global.Convenios_New.My.Resources.Resources.Excel
        Me.Button1.Location = New System.Drawing.Point(671, 26)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(46, 45)
        Me.Button1.TabIndex = 5
        Me.Button1.UseVisualStyleBackColor = True
        '
        'frmReportesVarios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(928, 620)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.MetroTabControl1)
        Me.Name = "frmReportesVarios"
        Me.Text = "Reportes"
        Me.MetroTabControl1.ResumeLayout(False)
        Me.MetroTabPage1.ResumeLayout(False)
        Me.MetroTabPage1.PerformLayout()
        CType(Me.dgvTransaccionabilidad, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MetroTabPage2.ResumeLayout(False)
        Me.MetroTabPage2.PerformLayout()
        CType(Me.dgvDelta, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents MetroTabControl1 As MetroFramework.Controls.MetroTabControl
    Friend WithEvents MetroTabPage1 As MetroFramework.Controls.MetroTabPage
    Friend WithEvents lblTotal1 As Label
    Friend WithEvents dgvTransaccionabilidad As DataGridView
    Friend WithEvents MetroTabPage2 As MetroFramework.Controls.MetroTabPage
    Friend WithEvents lblTotal2 As Label
    Friend WithEvents dgvDelta As DataGridView
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents dtpHasta As MetroFramework.Controls.MetroDateTime
    Friend WithEvents dtpDesde As MetroFramework.Controls.MetroDateTime
    Friend WithEvents btnActualizar As MetroFramework.Controls.MetroButton
    Friend WithEvents Button1 As Button
    Friend WithEvents btnbuscar As MetroFramework.Controls.MetroButton
End Class
