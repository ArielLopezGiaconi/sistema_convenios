﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Elasticidad
    Inherits MetroFramework.Forms.MetroForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim ChartArea1 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Legend1 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
        Dim Series1 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim ChartArea2 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Legend2 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
        Dim Series2 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim ChartArea3 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Legend3 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
        Dim Series3 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.cbxDosPuntos = New System.Windows.Forms.CheckBox()
        Me.cbxGraficaTodo = New System.Windows.Forms.CheckBox()
        Me.cmbNegocio = New System.Windows.Forms.ComboBox()
        Me.LabelX6 = New DevComponents.DotNetBar.LabelX()
        Me.cbxPuntos = New System.Windows.Forms.CheckBox()
        Me.lblPointTwo = New System.Windows.Forms.Label()
        Me.lblPointOne = New System.Windows.Forms.Label()
        Me.MetroLabel3 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.dgvPuntos = New System.Windows.Forms.DataGridView()
        Me.dgvProductos = New System.Windows.Forms.DataGridView()
        Me.btnGraficar = New System.Windows.Forms.Button()
        Me.cmbListas = New System.Windows.Forms.ComboBox()
        Me.LabelX5 = New DevComponents.DotNetBar.LabelX()
        Me.btnProcesar = New System.Windows.Forms.Button()
        Me.LabelX4 = New DevComponents.DotNetBar.LabelX()
        Me.dtpFechaHasta = New System.Windows.Forms.DateTimePicker()
        Me.LabelX3 = New DevComponents.DotNetBar.LabelX()
        Me.dtpFechaDesde = New System.Windows.Forms.DateTimePicker()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.txtRutcli = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtCodpro = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.dgvDatos = New System.Windows.Forms.DataGridView()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblElasticidad = New System.Windows.Forms.Label()
        Me.MicroChart1 = New DevComponents.DotNetBar.MicroChart()
        Me.Grafico = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.lblElasticidad2 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.MicroChart2 = New DevComponents.DotNetBar.MicroChart()
        Me.Grafico2 = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.Chart1 = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.ProgressBarX1 = New DevComponents.DotNetBar.Controls.ProgressBarX()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.dgvPuntos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvProductos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvDatos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.Grafico, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        CType(Me.Grafico2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Chart1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Location = New System.Drawing.Point(5, 58)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1009, 419)
        Me.TabControl1.TabIndex = 8
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.cbxDosPuntos)
        Me.TabPage1.Controls.Add(Me.cbxGraficaTodo)
        Me.TabPage1.Controls.Add(Me.cmbNegocio)
        Me.TabPage1.Controls.Add(Me.LabelX6)
        Me.TabPage1.Controls.Add(Me.cbxPuntos)
        Me.TabPage1.Controls.Add(Me.lblPointTwo)
        Me.TabPage1.Controls.Add(Me.lblPointOne)
        Me.TabPage1.Controls.Add(Me.MetroLabel3)
        Me.TabPage1.Controls.Add(Me.MetroLabel2)
        Me.TabPage1.Controls.Add(Me.MetroLabel1)
        Me.TabPage1.Controls.Add(Me.dgvPuntos)
        Me.TabPage1.Controls.Add(Me.dgvProductos)
        Me.TabPage1.Controls.Add(Me.btnGraficar)
        Me.TabPage1.Controls.Add(Me.cmbListas)
        Me.TabPage1.Controls.Add(Me.LabelX5)
        Me.TabPage1.Controls.Add(Me.btnProcesar)
        Me.TabPage1.Controls.Add(Me.LabelX4)
        Me.TabPage1.Controls.Add(Me.dtpFechaHasta)
        Me.TabPage1.Controls.Add(Me.LabelX3)
        Me.TabPage1.Controls.Add(Me.dtpFechaDesde)
        Me.TabPage1.Controls.Add(Me.LabelX2)
        Me.TabPage1.Controls.Add(Me.LabelX1)
        Me.TabPage1.Controls.Add(Me.txtRutcli)
        Me.TabPage1.Controls.Add(Me.txtCodpro)
        Me.TabPage1.Controls.Add(Me.dgvDatos)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1001, 393)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Datos"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'cbxDosPuntos
        '
        Me.cbxDosPuntos.AutoSize = True
        Me.cbxDosPuntos.Location = New System.Drawing.Point(749, 289)
        Me.cbxDosPuntos.Name = "cbxDosPuntos"
        Me.cbxDosPuntos.Size = New System.Drawing.Size(108, 17)
        Me.cbxDosPuntos.TabIndex = 32
        Me.cbxDosPuntos.Text = "Graficar 2 Puntos"
        Me.cbxDosPuntos.UseVisualStyleBackColor = True
        '
        'cbxGraficaTodo
        '
        Me.cbxGraficaTodo.AutoSize = True
        Me.cbxGraficaTodo.Location = New System.Drawing.Point(918, 343)
        Me.cbxGraficaTodo.Name = "cbxGraficaTodo"
        Me.cbxGraficaTodo.Size = New System.Drawing.Size(87, 17)
        Me.cbxGraficaTodo.TabIndex = 31
        Me.cbxGraficaTodo.Text = "Graficar todo"
        Me.cbxGraficaTodo.UseVisualStyleBackColor = True
        '
        'cmbNegocio
        '
        Me.cmbNegocio.FormattingEnabled = True
        Me.cmbNegocio.Location = New System.Drawing.Point(428, 309)
        Me.cmbNegocio.Name = "cmbNegocio"
        Me.cmbNegocio.Size = New System.Drawing.Size(162, 21)
        Me.cmbNegocio.TabIndex = 30
        '
        'LabelX6
        '
        '
        '
        '
        Me.LabelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX6.Location = New System.Drawing.Point(428, 290)
        Me.LabelX6.Name = "LabelX6"
        Me.LabelX6.Size = New System.Drawing.Size(78, 20)
        Me.LabelX6.TabIndex = 29
        Me.LabelX6.Text = "Negocio"
        '
        'cbxPuntos
        '
        Me.cbxPuntos.AutoSize = True
        Me.cbxPuntos.Location = New System.Drawing.Point(613, 289)
        Me.cbxPuntos.Name = "cbxPuntos"
        Me.cbxPuntos.Size = New System.Drawing.Size(130, 17)
        Me.cbxPuntos.TabIndex = 28
        Me.cbxPuntos.Text = "Graficar varios Puntos"
        Me.cbxPuntos.UseVisualStyleBackColor = True
        '
        'lblPointTwo
        '
        Me.lblPointTwo.AutoSize = True
        Me.lblPointTwo.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPointTwo.Location = New System.Drawing.Point(426, 366)
        Me.lblPointTwo.Name = "lblPointTwo"
        Me.lblPointTwo.Size = New System.Drawing.Size(74, 17)
        Me.lblPointTwo.TabIndex = 27
        Me.lblPointTwo.Text = "Punto 2: "
        '
        'lblPointOne
        '
        Me.lblPointOne.AutoSize = True
        Me.lblPointOne.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPointOne.Location = New System.Drawing.Point(426, 343)
        Me.lblPointOne.Name = "lblPointOne"
        Me.lblPointOne.Size = New System.Drawing.Size(74, 17)
        Me.lblPointOne.TabIndex = 26
        Me.lblPointOne.Text = "Punto 1: "
        '
        'MetroLabel3
        '
        Me.MetroLabel3.AutoSize = True
        Me.MetroLabel3.Location = New System.Drawing.Point(613, 9)
        Me.MetroLabel3.Name = "MetroLabel3"
        Me.MetroLabel3.Size = New System.Drawing.Size(119, 19)
        Me.MetroLabel3.TabIndex = 25
        Me.MetroLabel3.Text = "Productos por lista"
        '
        'MetroLabel2
        '
        Me.MetroLabel2.AutoSize = True
        Me.MetroLabel2.Location = New System.Drawing.Point(613, 151)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(109, 19)
        Me.MetroLabel2.TabIndex = 24
        Me.MetroLabel2.Text = "Puntos a Graficar"
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.Location = New System.Drawing.Point(7, 9)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(126, 19)
        Me.MetroLabel1.TabIndex = 23
        Me.MetroLabel1.Text = "Cambios de Precios"
        '
        'dgvPuntos
        '
        Me.dgvPuntos.AllowUserToAddRows = False
        Me.dgvPuntos.AllowUserToDeleteRows = False
        Me.dgvPuntos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPuntos.Location = New System.Drawing.Point(613, 173)
        Me.dgvPuntos.Name = "dgvPuntos"
        Me.dgvPuntos.ReadOnly = True
        Me.dgvPuntos.RowHeadersVisible = False
        Me.dgvPuntos.Size = New System.Drawing.Size(381, 110)
        Me.dgvPuntos.TabIndex = 22
        '
        'dgvProductos
        '
        Me.dgvProductos.AllowUserToAddRows = False
        Me.dgvProductos.AllowUserToDeleteRows = False
        Me.dgvProductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvProductos.Location = New System.Drawing.Point(613, 31)
        Me.dgvProductos.Name = "dgvProductos"
        Me.dgvProductos.ReadOnly = True
        Me.dgvProductos.RowHeadersVisible = False
        Me.dgvProductos.Size = New System.Drawing.Size(381, 110)
        Me.dgvProductos.TabIndex = 21
        '
        'btnGraficar
        '
        Me.btnGraficar.Location = New System.Drawing.Point(919, 364)
        Me.btnGraficar.Name = "btnGraficar"
        Me.btnGraficar.Size = New System.Drawing.Size(75, 23)
        Me.btnGraficar.TabIndex = 10
        Me.btnGraficar.Text = "Graficar"
        Me.btnGraficar.UseVisualStyleBackColor = True
        '
        'cmbListas
        '
        Me.cmbListas.FormattingEnabled = True
        Me.cmbListas.Location = New System.Drawing.Point(260, 308)
        Me.cmbListas.Name = "cmbListas"
        Me.cmbListas.Size = New System.Drawing.Size(162, 21)
        Me.cmbListas.TabIndex = 20
        '
        'LabelX5
        '
        '
        '
        '
        Me.LabelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX5.Enabled = False
        Me.LabelX5.Location = New System.Drawing.Point(110, 340)
        Me.LabelX5.Name = "LabelX5"
        Me.LabelX5.Size = New System.Drawing.Size(78, 20)
        Me.LabelX5.TabIndex = 13
        Me.LabelX5.Text = "Fecha Hasta"
        '
        'btnProcesar
        '
        Me.btnProcesar.Location = New System.Drawing.Point(919, 296)
        Me.btnProcesar.Name = "btnProcesar"
        Me.btnProcesar.Size = New System.Drawing.Size(75, 23)
        Me.btnProcesar.TabIndex = 9
        Me.btnProcesar.Text = "Procesar"
        Me.btnProcesar.UseVisualStyleBackColor = True
        '
        'LabelX4
        '
        '
        '
        '
        Me.LabelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX4.Location = New System.Drawing.Point(10, 340)
        Me.LabelX4.Name = "LabelX4"
        Me.LabelX4.Size = New System.Drawing.Size(78, 20)
        Me.LabelX4.TabIndex = 13
        Me.LabelX4.Text = "Fecha Desde"
        '
        'dtpFechaHasta
        '
        Me.dtpFechaHasta.Enabled = False
        Me.dtpFechaHasta.Location = New System.Drawing.Point(110, 366)
        Me.dtpFechaHasta.Name = "dtpFechaHasta"
        Me.dtpFechaHasta.Size = New System.Drawing.Size(217, 20)
        Me.dtpFechaHasta.TabIndex = 19
        '
        'LabelX3
        '
        '
        '
        '
        Me.LabelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX3.Location = New System.Drawing.Point(260, 289)
        Me.LabelX3.Name = "LabelX3"
        Me.LabelX3.Size = New System.Drawing.Size(78, 20)
        Me.LabelX3.TabIndex = 13
        Me.LabelX3.Text = "Listas"
        '
        'dtpFechaDesde
        '
        Me.dtpFechaDesde.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaDesde.Location = New System.Drawing.Point(10, 366)
        Me.dtpFechaDesde.Name = "dtpFechaDesde"
        Me.dtpFechaDesde.Size = New System.Drawing.Size(94, 20)
        Me.dtpFechaDesde.TabIndex = 18
        '
        'LabelX2
        '
        '
        '
        '
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.Location = New System.Drawing.Point(135, 289)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(78, 20)
        Me.LabelX2.TabIndex = 12
        Me.LabelX2.Text = "Rut"
        '
        'LabelX1
        '
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.Location = New System.Drawing.Point(10, 289)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(78, 20)
        Me.LabelX1.TabIndex = 10
        Me.LabelX1.Text = "Codigo"
        '
        'txtRutcli
        '
        '
        '
        '
        Me.txtRutcli.Border.Class = "TextBoxBorder"
        Me.txtRutcli.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtRutcli.Location = New System.Drawing.Point(135, 309)
        Me.txtRutcli.Name = "txtRutcli"
        Me.txtRutcli.PreventEnterBeep = True
        Me.txtRutcli.Size = New System.Drawing.Size(119, 20)
        Me.txtRutcli.TabIndex = 11
        '
        'txtCodpro
        '
        '
        '
        '
        Me.txtCodpro.Border.Class = "TextBoxBorder"
        Me.txtCodpro.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtCodpro.Location = New System.Drawing.Point(10, 309)
        Me.txtCodpro.Name = "txtCodpro"
        Me.txtCodpro.PreventEnterBeep = True
        Me.txtCodpro.Size = New System.Drawing.Size(119, 20)
        Me.txtCodpro.TabIndex = 9
        '
        'dgvDatos
        '
        Me.dgvDatos.AllowUserToAddRows = False
        Me.dgvDatos.AllowUserToDeleteRows = False
        Me.dgvDatos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDatos.Location = New System.Drawing.Point(3, 31)
        Me.dgvDatos.Name = "dgvDatos"
        Me.dgvDatos.ReadOnly = True
        Me.dgvDatos.RowHeadersVisible = False
        Me.dgvDatos.Size = New System.Drawing.Size(604, 252)
        Me.dgvDatos.TabIndex = 8
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.Label3)
        Me.TabPage2.Controls.Add(Me.Label2)
        Me.TabPage2.Controls.Add(Me.Label1)
        Me.TabPage2.Controls.Add(Me.lblElasticidad)
        Me.TabPage2.Controls.Add(Me.MicroChart1)
        Me.TabPage2.Controls.Add(Me.Grafico)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1001, 393)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Grafico Cant x Precio"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(838, 177)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 17)
        Me.Label3.TabIndex = 31
        Me.Label3.Text = "Lista: "
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(838, 160)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(68, 17)
        Me.Label2.TabIndex = 30
        Me.Label2.Text = "Cliente: "
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(838, 141)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(49, 17)
        Me.Label1.TabIndex = 29
        Me.Label1.Text = "SKU: "
        '
        'lblElasticidad
        '
        Me.lblElasticidad.AutoSize = True
        Me.lblElasticidad.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblElasticidad.Location = New System.Drawing.Point(848, 89)
        Me.lblElasticidad.Name = "lblElasticidad"
        Me.lblElasticidad.Size = New System.Drawing.Size(0, 17)
        Me.lblElasticidad.TabIndex = 28
        '
        'MicroChart1
        '
        '
        '
        '
        Me.MicroChart1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.MicroChart1.Location = New System.Drawing.Point(872, 231)
        Me.MicroChart1.Name = "MicroChart1"
        Me.MicroChart1.Size = New System.Drawing.Size(97, 37)
        Me.MicroChart1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.MicroChart1.TabIndex = 0
        Me.MicroChart1.Text = "MicroChart1"
        '
        'Grafico
        '
        Me.Grafico.BackHatchStyle = System.Windows.Forms.DataVisualization.Charting.ChartHatchStyle.LightUpwardDiagonal
        Me.Grafico.BorderSkin.BackImageAlignment = System.Windows.Forms.DataVisualization.Charting.ChartImageAlignmentStyle.Top
        ChartArea1.Name = "ChartArea1"
        Me.Grafico.ChartAreas.Add(ChartArea1)
        Legend1.Name = "Legend1"
        Me.Grafico.Legends.Add(Legend1)
        Me.Grafico.Location = New System.Drawing.Point(3, 6)
        Me.Grafico.Name = "Grafico"
        Series1.ChartArea = "ChartArea1"
        Series1.Legend = "Legend1"
        Series1.Name = "Series1"
        Me.Grafico.Series.Add(Series1)
        Me.Grafico.Size = New System.Drawing.Size(989, 381)
        Me.Grafico.TabIndex = 1
        Me.Grafico.Text = "Chart1"
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.lblElasticidad2)
        Me.TabPage3.Controls.Add(Me.Label4)
        Me.TabPage3.Controls.Add(Me.Label5)
        Me.TabPage3.Controls.Add(Me.Label6)
        Me.TabPage3.Controls.Add(Me.MicroChart2)
        Me.TabPage3.Controls.Add(Me.Grafico2)
        Me.TabPage3.Controls.Add(Me.Chart1)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(1001, 393)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Grafico Cant x Fecha"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'lblElasticidad2
        '
        Me.lblElasticidad2.AutoSize = True
        Me.lblElasticidad2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblElasticidad2.Location = New System.Drawing.Point(845, 90)
        Me.lblElasticidad2.Name = "lblElasticidad2"
        Me.lblElasticidad2.Size = New System.Drawing.Size(0, 17)
        Me.lblElasticidad2.TabIndex = 37
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(841, 177)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(53, 17)
        Me.Label4.TabIndex = 36
        Me.Label4.Text = "Lista: "
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(841, 160)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(68, 17)
        Me.Label5.TabIndex = 35
        Me.Label5.Text = "Cliente: "
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(841, 141)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(49, 17)
        Me.Label6.TabIndex = 34
        Me.Label6.Text = "SKU: "
        '
        'MicroChart2
        '
        '
        '
        '
        Me.MicroChart2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.MicroChart2.Location = New System.Drawing.Point(875, 231)
        Me.MicroChart2.Name = "MicroChart2"
        Me.MicroChart2.Size = New System.Drawing.Size(97, 37)
        Me.MicroChart2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.MicroChart2.TabIndex = 32
        Me.MicroChart2.Text = "MicroChart2"
        '
        'Grafico2
        '
        Me.Grafico2.BackHatchStyle = System.Windows.Forms.DataVisualization.Charting.ChartHatchStyle.LightUpwardDiagonal
        Me.Grafico2.BorderSkin.BackImageAlignment = System.Windows.Forms.DataVisualization.Charting.ChartImageAlignmentStyle.Top
        ChartArea2.Name = "ChartArea1"
        Me.Grafico2.ChartAreas.Add(ChartArea2)
        Legend2.Name = "Legend1"
        Me.Grafico2.Legends.Add(Legend2)
        Me.Grafico2.Location = New System.Drawing.Point(6, 6)
        Me.Grafico2.Name = "Grafico2"
        Series2.ChartArea = "ChartArea1"
        Series2.Legend = "Legend1"
        Series2.Name = "Series1"
        Me.Grafico2.Series.Add(Series2)
        Me.Grafico2.Size = New System.Drawing.Size(989, 381)
        Me.Grafico2.TabIndex = 33
        Me.Grafico2.Text = "Chart1"
        '
        'Chart1
        '
        Me.Chart1.BackHatchStyle = System.Windows.Forms.DataVisualization.Charting.ChartHatchStyle.LightUpwardDiagonal
        Me.Chart1.BorderSkin.BackImageAlignment = System.Windows.Forms.DataVisualization.Charting.ChartImageAlignmentStyle.Top
        ChartArea3.Name = "ChartArea1"
        Me.Chart1.ChartAreas.Add(ChartArea3)
        Legend3.Name = "Legend1"
        Me.Chart1.Legends.Add(Legend3)
        Me.Chart1.Location = New System.Drawing.Point(6, 6)
        Me.Chart1.Name = "Chart1"
        Series3.ChartArea = "ChartArea1"
        Series3.Legend = "Legend1"
        Series3.Name = "Series1"
        Me.Chart1.Series.Add(Series3)
        Me.Chart1.Size = New System.Drawing.Size(989, 381)
        Me.Chart1.TabIndex = 2
        Me.Chart1.Text = "Chart1"
        '
        'ProgressBarX1
        '
        '
        '
        '
        Me.ProgressBarX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ProgressBarX1.Location = New System.Drawing.Point(367, 29)
        Me.ProgressBarX1.Name = "ProgressBarX1"
        Me.ProgressBarX1.Size = New System.Drawing.Size(643, 23)
        Me.ProgressBarX1.TabIndex = 9
        Me.ProgressBarX1.Text = "ProgressBarX1"
        Me.ProgressBarX1.Visible = False
        '
        'LinkLabel1
        '
        Me.LinkLabel1.AutoSize = True
        Me.LinkLabel1.Location = New System.Drawing.Point(364, 13)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(58, 13)
        Me.LinkLabel1.TabIndex = 10
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "Reporte BI"
        '
        'Elasticidad
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1026, 479)
        Me.Controls.Add(Me.LinkLabel1)
        Me.Controls.Add(Me.ProgressBarX1)
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "Elasticidad"
        Me.Text = "Elasticidad"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.dgvPuntos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvProductos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvDatos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        CType(Me.Grafico, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        CType(Me.Grafico2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Chart1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents btnProcesar As Button
    Friend WithEvents MicroChart1 As DevComponents.DotNetBar.MicroChart
    Friend WithEvents dtpFechaDesde As DateTimePicker
    Friend WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtRutcli As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtCodpro As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents dgvDatos As DataGridView
    Friend WithEvents LabelX3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents dtpFechaHasta As DateTimePicker
    Friend WithEvents cmbListas As ComboBox
    Friend WithEvents btnGraficar As Button
    Friend WithEvents Grafico As DataVisualization.Charting.Chart
    Friend WithEvents MetroLabel3 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents dgvPuntos As DataGridView
    Friend WithEvents dgvProductos As DataGridView
    Friend WithEvents ProgressBarX1 As DevComponents.DotNetBar.Controls.ProgressBarX
    Friend WithEvents lblPointTwo As Label
    Friend WithEvents lblPointOne As Label
    Friend WithEvents lblElasticidad As Label
    Friend WithEvents cbxPuntos As CheckBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents cmbNegocio As ComboBox
    Friend WithEvents LabelX6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cbxGraficaTodo As CheckBox
    Friend WithEvents cbxDosPuntos As CheckBox
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents lblElasticidad2 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents MicroChart2 As DevComponents.DotNetBar.MicroChart
    Friend WithEvents Grafico2 As DataVisualization.Charting.Chart
    Friend WithEvents Chart1 As DataVisualization.Charting.Chart
    Friend WithEvents LinkLabel1 As LinkLabel
End Class
