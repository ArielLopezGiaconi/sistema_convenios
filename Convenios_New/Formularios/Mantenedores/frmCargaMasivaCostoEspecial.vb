﻿Imports ClosedXML.Excel
Imports System.Data.OracleClient
Public Class frmCargaMasivaCostoEspecial
    Dim bd As New Conexion
    Dim strTabUser = "TM_" & UCase(Globales.user) & "_CLICOS"
    Dim dtErrores, dtRepetidos, dtCorrectos As New DataTable
    Dim PuntoControl As New PuntoControl

    Private Sub btn_Salir_Click(sender As System.Object, e As System.EventArgs) Handles btn_Salir.Click
        Me.Close()
    End Sub

    Private Sub frmCargaMasivaCostoEspecial_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        PuntoControl.RegistroUso("142")
        If Not Globales.detDerUsu(Globales.user, 109) Then
            MessageBox.Show("No tiene derecho a cargar ofertas (Der. 109)", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Close()
        End If
        Cursor.Current = Cursors.WaitCursor
        Me.StartPosition = FormStartPosition.Manual
        Me.Location = New Point(0, 100)
        creaTablaDatos()
    End Sub

    Public Sub ExportarDgvExcel(ruta As String)
        Try
            Dim WorkBook As New XLWorkbook
            ' Dim worksheet = WorkBook.Worksheets.Add("Sheet 1")

            If rdbErrores.Checked = True Then
                dtErrores.TableName = "Registros con Errores"
                WorkBook.Worksheets.Add(dtErrores)
            Else
                dtRepetidos.TableName = "Costos repetidos"
                WorkBook.Worksheets.Add(dtRepetidos)
            End If

            WorkBook.SaveAs(ruta)

        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub creaTablaDatos()
        dtRepetidos.Columns.Add("ID", Type.GetType("System.String"))
        dtRepetidos.Columns.Add("Rutcli", Type.GetType("System.String"))
        dtRepetidos.Columns.Add("tipo", Type.GetType("System.String"))
        dtRepetidos.Columns.Add("Razón Social", Type.GetType("System.String"))
        dtRepetidos.Columns.Add("Codpro", Type.GetType("System.String"))
        dtRepetidos.Columns.Add("Descripción", Type.GetType("System.String"))
        dtRepetidos.Columns.Add("Precio", Type.GetType("System.String"))
        dtRepetidos.Columns.Add("Costo", Type.GetType("System.String"))
        dtRepetidos.Columns.Add("%Mg.", Type.GetType("System.String"))
        dtRepetidos.Columns.Add("FechaIni", Type.GetType("System.String"))
        dtRepetidos.Columns.Add("FechaFin", Type.GetType("System.String"))
        dtRepetidos.Columns.Add("Cantid", Type.GetType("System.String"))
        dtRepetidos.Columns.Add("UserId", Type.GetType("System.String"))
        dtRepetidos.Columns.Add("Marca", Type.GetType("System.String"))

        dtCorrectos.Columns.Add("CODPRO", Type.GetType("System.String"))
        dtCorrectos.Columns.Add("RUTCLI", Type.GetType("System.String"))
        dtCorrectos.Columns.Add("PRECIO", Type.GetType("System.String"))
        dtCorrectos.Columns.Add("COSTO", Type.GetType("System.String"))
        dtCorrectos.Columns.Add("FECHAINI", Type.GetType("System.String"))
        dtCorrectos.Columns.Add("FECHAFIN", Type.GetType("System.String"))
        dtCorrectos.Columns.Add("CANTIDAD", Type.GetType("System.String"))
        dtCorrectos.Columns.Add("TIPO", Type.GetType("System.String"))
        dtCorrectos.Columns.Add("CODMON", Type.GetType("System.String"))
        dtCorrectos.Columns.Add("FILA", Type.GetType("System.String"))

        dtErrores.Columns.Add("CODPRO", Type.GetType("System.String"))
        dtErrores.Columns.Add("RUTCLI", Type.GetType("System.String"))
        dtErrores.Columns.Add("PRECIO", Type.GetType("System.String"))
        dtErrores.Columns.Add("COSTO", Type.GetType("System.String"))
        dtErrores.Columns.Add("FECHAINI", Type.GetType("System.String"))
        dtErrores.Columns.Add("FECHAFIN", Type.GetType("System.String"))
        dtErrores.Columns.Add("CANTIDAD", Type.GetType("System.String"))
        dtErrores.Columns.Add("TIPO", Type.GetType("System.String"))
        dtErrores.Columns.Add("CODMON", Type.GetType("System.String"))
        dtErrores.Columns.Add("FILA", Type.GetType("System.String"))
        dtErrores.Columns.Add("ERROR", Type.GetType("System.String"))

    End Sub

    Private Sub frmCargaMasivaClienteCosto_Shown(sender As System.Object, e As System.EventArgs) Handles MyBase.Shown
        Cursor.Current = Cursors.Arrow
    End Sub

    Private Sub btn_Importar_Click(sender As System.Object, e As System.EventArgs) Handles btn_Importar.Click
        Dim openFile As New OpenFileDialog
        Dim dtpaso As New DataTable()
        PuntoControl.RegistroUso("143")
        If openFile.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            Globales.Importar(dgvDatos, openFile.FileName)
            lblTotales.Text = "Cargando... "
            dgvDatos.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            dtpaso = dgvDatos.DataSource
            dtErrores = dtpaso.Clone
            dtErrores.TableName = "Errores"
            dtErrores.Clear()
            dgvErrores.DataSource = dtErrores
            lblTotales.Text = "Total Registros : " + dgvDatos.Rows.Count.ToString
        End If
        TabControl1.SelectedIndex = 0
    End Sub

    Private Sub btn_Grabar_Click(sender As System.Object, e As System.EventArgs) Handles btn_Grabar.Click
        'If Not Globales.detDerUsu(Globales.user, 109) Then
        '    MessageBox.Show("No tiene derecho a cargar Ofertas Cliente-Costo (Der. 109)", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error)
        '    Exit Sub
        'End If
        PuntoControl.RegistroUso("144")
        If dgvDatos.RowCount = 0 Then
            MessageBox.Show("No se ha cargado un archivo para Procesar.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        Else
            If MessageBox.Show("¿Desea grabar los siguientes datos?", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = System.Windows.Forms.DialogResult.Yes Then
                grabar()
            End If
        End If
    End Sub
    Private Sub grabar()
        Using conn = New OracleConnection(Conexion.retornaConexion)
            conn.Open()
            Dim comando As OracleCommand
            Dim dataAdapter As OracleDataAdapter
            Dim dt, dt1 As New DataTable
            Dim esta As Boolean = False
            Dim sql As String
            Dim aux As Int32
            Using transaccion = conn.BeginTransaction
                Try
                    dtCorrectos.Rows.Clear()

                    dgvRepetidos.DataSource = dtRepetidos
                    dgvRepetidos.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells

                    Crea_Tabla_Temporal(strTabUser)
                    Cursor.Current = Cursors.WaitCursor
                    For i = 0 To dgvDatos.RowCount - 1
                        lblTotales.Text = "Validando " + (i + 1).ToString + " de " + dgvDatos.RowCount.ToString + " Registros"
                        Application.DoEvents()

                        'VALIDO EL CODIGO
                        sql = "SELECT codpro FROM ma_product "
                        sql = sql & " WHERE codpro = '" & Trim(dgvDatos.Item("CODPRO", i).Value) & "'"
                        dt.Clear()
                        comando = New OracleCommand(sql, conn)
                        comando.Transaction = transaccion
                        dataAdapter = New OracleDataAdapter(comando)
                        dataAdapter.Fill(dt)
                        If dt.Rows.Count = 0 Then
                            dtErrores.Rows.Add(dgvDatos.Item("CODPRO", i).Value,
                                               dgvDatos.Item("RUTCLI", i).Value,
                                               dgvDatos.Item("PRECIO", i).Value,
                                               dgvDatos.Item("COSTO", i).Value,
                                               dgvDatos.Item("FECHAINI", i).Value,
                                               dgvDatos.Item("FECHAFIN", i).Value,
                                               dgvDatos.Item("CANTIDAD", i).Value,
                                               dgvDatos.Item("TIPO", i).Value,
                                               dgvDatos.Item("CODMON", i).Value,
                                               i,
                                               "Codigo No existe")
                            Continue For
                        End If

                        'VALIDO EL CLIENTE
                        sql = "SELECT rutcli FROM en_cliente "
                        sql = sql & " WHERE  rutcli = '" & Trim(dgvDatos.Item("RUTCLI", i).Value) & "'"
                        dt.Clear()
                        comando = New OracleCommand(sql, conn)
                        comando.Transaction = transaccion
                        dataAdapter = New OracleDataAdapter(comando)
                        dataAdapter.Fill(dt)
                        If dt.Rows.Count = 0 Then
                            dtErrores.Rows.Add(dgvDatos.Item("CODPRO", i).Value,
                                               dgvDatos.Item("RUTCLI", i).Value,
                                               dgvDatos.Item("PRECIO", i).Value,
                                               dgvDatos.Item("COSTO", i).Value,
                                               dgvDatos.Item("FECHAINI", i).Value,
                                               dgvDatos.Item("FECHAFIN", i).Value,
                                               dgvDatos.Item("CANTIDAD", i).Value,
                                               dgvDatos.Item("TIPO", i).Value,
                                               dgvDatos.Item("CODMON", i).Value,
                                               i,
                                               "Cliente No existe")
                            Continue For
                        End If

                        'Dominio 401, VALIDO EL TIPO DE CONVENIO
                        sql = "SELECT desval FROM de_dominio "
                        sql = sql & " WHERE codval = '" & Trim(dgvDatos.Item("TIPO", i).Value) & "'"
                        sql = sql & " and coddom = 401"
                        dt.Clear()
                        comando = New OracleCommand(sql, conn)
                        comando.Transaction = transaccion
                        dataAdapter = New OracleDataAdapter(comando)
                        dataAdapter.Fill(dt)
                        If dt.Rows.Count = 0 Then
                            dtErrores.Rows.Add(dgvDatos.Item("CODPRO", i).Value,
                                               dgvDatos.Item("RUTCLI", i).Value,
                                               dgvDatos.Item("PRECIO", i).Value,
                                               dgvDatos.Item("COSTO", i).Value,
                                               dgvDatos.Item("FECHAINI", i).Value,
                                               dgvDatos.Item("FECHAFIN", i).Value,
                                               dgvDatos.Item("CANTIDAD", i).Value,
                                               dgvDatos.Item("TIPO", i).Value,
                                               dgvDatos.Item("CODMON", i).Value,
                                               i,
                                               "Tipo No existe")
                            Continue For
                        End If

                        'VALIDANDO COSTOS EN DOLAR
                        If dgvDatos.Item("CODMON", i).Value.ToString = 1 Then
                            sql = "select getclasificacion_cliente(" & Globales.empresa & "," & dgvDatos.Item("RUTCLI", i).Value.ToString.Trim & ") SEGMENTO from dual"
                            dt1.Clear()
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            dataAdapter = New OracleDataAdapter(comando)
                            dataAdapter.Fill(dt1)
                            If dt1.Rows(0).Item("SEGMENTO") <> "GOBIERNO" Then
                                dtErrores.Rows.Add(dgvDatos.Item("CODPRO", i).Value,
                                               dgvDatos.Item("RUTCLI", i).Value,
                                               dgvDatos.Item("PRECIO", i).Value,
                                               dgvDatos.Item("COSTO", i).Value,
                                               dgvDatos.Item("FECHAINI", i).Value,
                                               dgvDatos.Item("FECHAFIN", i).Value,
                                               dgvDatos.Item("CANTIDAD", i).Value,
                                               dgvDatos.Item("TIPO", i).Value,
                                               dgvDatos.Item("CODMON", i).Value,
                                               i,
                                               "Moneda erronea")
                                MessageBox.Show("No se puede ingresar un costo en dolar si el cliente no es de Gobierno.", "Finalizado", MessageBoxButtons.OK, MessageBoxIcon.Information)
                                Continue For
                            End If

                            sql = "Select * From de_conveni "
                            sql = sql & " Where numcot = 5503121 "
                            sql = sql & "  And moneda = 'D' "
                            dt1.Clear()
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            dataAdapter = New OracleDataAdapter(comando)
                            dataAdapter.Fill(dt1)
                            For a = 0 To dt1.Rows.Count - 1
                                If dt1.Rows(a).Item("CODPRO") = dgvDatos.Item("CODPRO", i).Value.ToString.Trim Then
                                    esta = True
                                    Exit For
                                End If
                            Next
                            If esta = False Then
                                dtErrores.Rows.Add(dgvDatos.Item("CODPRO", i).Value,
                                               dgvDatos.Item("RUTCLI", i).Value,
                                               dgvDatos.Item("PRECIO", i).Value,
                                               dgvDatos.Item("COSTO", i).Value,
                                               dgvDatos.Item("FECHAINI", i).Value,
                                               dgvDatos.Item("FECHAFIN", i).Value,
                                               dgvDatos.Item("CANTIDAD", i).Value,
                                               dgvDatos.Item("TIPO", i).Value,
                                               dgvDatos.Item("CODMON", i).Value,
                                               i,
                                               "Moneda erronea")
                                MessageBox.Show("El producto no puede tener costo en dolar ya que no esta en el convenio MARCO.", "Finalizado", MessageBoxButtons.OK, MessageBoxIcon.Information)
                                Continue For
                            End If
                        ElseIf dgvDatos.Item("CODMON", i).Value.ToString = 0 Then
                            sql = "select moneda from ma_product where codpro = '" & dgvDatos.Item("CODPRO", i).Value.ToString.Trim & "'"
                            dt1.Clear()
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            dataAdapter = New OracleDataAdapter(comando)
                            dataAdapter.Fill(dt1)
                            If dt1.Rows(0).Item("MONEDA") = 1 Then
                                dtErrores.Rows.Add(dgvDatos.Item("CODPRO", i).Value,
                                               dgvDatos.Item("RUTCLI", i).Value,
                                               dgvDatos.Item("PRECIO", i).Value,
                                               dgvDatos.Item("COSTO", i).Value,
                                               dgvDatos.Item("FECHAINI", i).Value,
                                               dgvDatos.Item("FECHAFIN", i).Value,
                                               dgvDatos.Item("CANTIDAD", i).Value,
                                               dgvDatos.Item("TIPO", i).Value,
                                               dgvDatos.Item("CODMON", i).Value,
                                               i,
                                               "Moneda Erronea")
                                MessageBox.Show("No puede agregar el costo del producto " & dgvDatos.Item("CODPRO", i).Value.ToString & " en pesos ya que esta publicado en DOLAR", "Finalizado", MessageBoxButtons.OK, MessageBoxIcon.Information)
                                Continue For
                            End If
                        End If

                        'VALIDO LA MONEDA
                        sql = "SELECT desmon FROM ma_monedas "
                        sql = sql & " WHERE  codmon = '" & Trim(dgvDatos.Item("CODMON", i).Value) & "'"
                        dt.Clear()
                        comando = New OracleCommand(sql, conn)
                        comando.Transaction = transaccion
                        dataAdapter = New OracleDataAdapter(comando)
                        dataAdapter.Fill(dt)
                        If dt.Rows.Count = 0 Then
                            dtErrores.Rows.Add(dgvDatos.Item("CODPRO", i).Value,
                                               dgvDatos.Item("RUTCLI", i).Value,
                                               dgvDatos.Item("PRECIO", i).Value,
                                               dgvDatos.Item("COSTO", i).Value,
                                               dgvDatos.Item("FECHAINI", i).Value,
                                               dgvDatos.Item("FECHAFIN", i).Value,
                                               dgvDatos.Item("CANTIDAD", i).Value,
                                               dgvDatos.Item("TIPO", i).Value,
                                               dgvDatos.Item("CODMON", i).Value,
                                               i,
                                               "Moneda No existe")
                            Continue For
                        End If

                        Dim numero As Double = 0
                        'VALIDO EL COSTO
                        If Double.TryParse(dgvDatos.Item("PRECIO", i).Value, numero) = False Then
                            dtErrores.Rows.Add(dgvDatos.Item("CODPRO", i).Value,
                                               dgvDatos.Item("RUTCLI", i).Value,
                                               dgvDatos.Item("PRECIO", i).Value,
                                               dgvDatos.Item("COSTO", i).Value,
                                               dgvDatos.Item("FECHAINI", i).Value,
                                               dgvDatos.Item("FECHAFIN", i).Value,
                                               dgvDatos.Item("CANTIDAD", i).Value,
                                               dgvDatos.Item("TIPO", i).Value,
                                               dgvDatos.Item("CODMON", i).Value,
                                               i,
                                               "Precio con problemas")
                            Continue For
                        End If

                        If Double.TryParse(dgvDatos.Item("COSTO", i).Value, numero) = False Then
                            dtErrores.Rows.Add(dgvDatos.Item("CODPRO", i).Value,
                                               dgvDatos.Item("RUTCLI", i).Value,
                                               dgvDatos.Item("PRECIO", i).Value,
                                               dgvDatos.Item("COSTO", i).Value,
                                               dgvDatos.Item("FECHAINI", i).Value,
                                               dgvDatos.Item("FECHAFIN", i).Value,
                                               dgvDatos.Item("CANTIDAD", i).Value,
                                               dgvDatos.Item("TIPO", i).Value,
                                               dgvDatos.Item("CODMON", i).Value,
                                               i,
                                               "Costo con Problemas")
                            Continue For
                        End If

                        'VALIDO LA CANTIDAD
                        If Double.TryParse(dgvDatos.Item("CANTIDAD", i).Value, numero) = False Then
                            dtErrores.Rows.Add(dgvDatos.Item("CODPRO", i).Value,
                                               dgvDatos.Item("RUTCLI", i).Value,
                                               dgvDatos.Item("PRECIO", i).Value,
                                               dgvDatos.Item("COSTO", i).Value,
                                               dgvDatos.Item("FECHAINI", i).Value,
                                               dgvDatos.Item("FECHAFIN", i).Value,
                                               dgvDatos.Item("CANTIDAD", i).Value,
                                               dgvDatos.Item("TIPO", i).Value,
                                               dgvDatos.Item("CODMON", i).Value,
                                               i,
                                               "Cantidad con errores")
                            Continue For
                        End If

                        'VALIDO LA FECHA DE INICIO
                        Dim fecha As Date
                        If Date.TryParse(dgvDatos.Item("FECHAINI", i).Value, fecha) = False Then
                            dtErrores.Rows.Add(dgvDatos.Item("CODPRO", i).Value,
                                               dgvDatos.Item("RUTCLI", i).Value,
                                               dgvDatos.Item("PRECIO", i).Value,
                                               dgvDatos.Item("COSTO", i).Value,
                                               dgvDatos.Item("FECHAINI", i).Value,
                                               dgvDatos.Item("FECHAFIN", i).Value,
                                               dgvDatos.Item("CANTIDAD", i).Value,
                                               dgvDatos.Item("TIPO", i).Value,
                                               dgvDatos.Item("CODMON", i).Value,
                                               i,
                                               "Fecha inicio con problemas")
                            Continue For
                        End If

                        'VALIDO LA FECHA DE TERMINO
                        If Date.TryParse(dgvDatos.Item("FECHAFIN", i).Value, fecha) = False Then
                            dtErrores.Rows.Add(dgvDatos.Item("CODPRO", i).Value,
                                               dgvDatos.Item("RUTCLI", i).Value,
                                               dgvDatos.Item("PRECIO", i).Value,
                                               dgvDatos.Item("COSTO", i).Value,
                                               dgvDatos.Item("FECHAINI", i).Value,
                                               dgvDatos.Item("FECHAFIN", i).Value,
                                               dgvDatos.Item("CANTIDAD", i).Value,
                                               dgvDatos.Item("TIPO", i).Value,
                                               dgvDatos.Item("CODMON", i).Value,
                                               i,
                                               "fecha fin con problemas")
                            Continue For
                        End If

                        sql = "SELECT id_numero ""ID"", rutcli ""Rutcli"", tipo, "
                        sql = sql & " getrazonsocial (codemp, rutcli) ""Razón Social"", codpro ""Codpro"","
                        sql = sql & " getdescripcion (codpro) ""Descripción"", NVL (precio, 0) ""Precio"","
                        sql = sql & " NVL (costo, 0) ""Costo"", NVL (margen, 0) ""%Mg."", fecini ""FechaIni"","
                        sql = sql & " fecfin ""FechaFin"", cantid ""Cantid"", usr_creac ""UserId"","
                        sql = sql & " getmarca (codpro) ""Marca"""
                        sql = sql & " FROM en_cliente_costo"
                        sql = sql & " where rutcli = " & dgvDatos.Item("RUTCLI", i).Value.ToString
                        sql = sql & "   and codpro = '" & dgvDatos.Item("CODPRO", i).Value.ToString & "'"
                        sql = sql & "   and to_Date(sysdate) between fecini and fecfin "
                        sql = sql & "   and estado = 'V' "
                        comando = New OracleCommand(sql, conn)
                        comando.Transaction = transaccion
                        dataAdapter = New OracleDataAdapter(comando)
                        dt.Clear()
                        dataAdapter.Fill(dt)
                        If dt.Rows.Count > 0 Then
                            If rdbActualiza.Checked = False And rdbNuevo.Checked = True Then
                                dtRepetidos.Rows.Add(dt.Rows(0).Item("ID"),
                            dt.Rows(0).Item("Rutcli"),
                            dt.Rows(0).Item("tipo"),
                            dt.Rows(0).Item("Razón Social"),
                            dt.Rows(0).Item("Codpro"),
                            dt.Rows(0).Item("Descripción"),
                            dt.Rows(0).Item("Precio"),
                            dt.Rows(0).Item("Costo"),
                            dt.Rows(0).Item("%Mg."),
                            dt.Rows(0).Item("FechaIni"),
                            dt.Rows(0).Item("FechaFin"),
                            dt.Rows(0).Item("Cantid"),
                            dt.Rows(0).Item("UserId"),
                            dt.Rows(0).Item("Marca"))
                                Continue For
                            End If
                        End If

                        dtCorrectos.Rows.Add(dgvDatos.Item("CODPRO", i).Value, dgvDatos.Item("RUTCLI", i).Value, dgvDatos.Item("PRECIO", i).Value, dgvDatos.Item("COSTO", i).Value,
                                           dgvDatos.Item("FECHAINI", i).Value, dgvDatos.Item("FECHAFIN", i).Value, dgvDatos.Item("CANTIDAD", i).Value, dgvDatos.Item("TIPO", i).Value, dgvDatos.Item("CODMON", i).Value, i.ToString)
                    Next

                    lblerrores.Text = "VALIDANDO DATOS..."

                    aux = dtCorrectos.Rows.Count

                    'For i = 0 To dtCorrectos.Rows.Count - 1
                    '    For a = (i + 1) To dtCorrectos.Rows.Count - 1
                    '        If a > (dtCorrectos.Rows.Count - 1) Then
                    '            Exit For
                    '        End If
                    '        If dtCorrectos.Rows(i).Item("CODPRO") = dtCorrectos.Rows(a).Item("CODPRO") And dtCorrectos.Rows(i).Item("RUTCLI") = dtCorrectos.Rows(a).Item("RUTCLI") And dtCorrectos.Rows(i).Item("TIPO") = dtCorrectos.Rows(a).Item("TIPO") Then
                    '            dtErrores.Rows.Add(dtCorrectos.Rows(i).Item("CODPRO"),
                    '                           dtCorrectos.Rows(i).Item("RUTCLI"),
                    '                           dtCorrectos.Rows(i).Item("PRECIO"),
                    '                           dtCorrectos.Rows(i).Item("COSTO"),
                    '                           dtCorrectos.Rows(i).Item("FECHAINI"),
                    '                           dtCorrectos.Rows(i).Item("FECHAFIN"),
                    '                           dtCorrectos.Rows(i).Item("CANTIDAD"),
                    '                           dtCorrectos.Rows(i).Item("TIPO"),
                    '                           dtCorrectos.Rows(i).Item("CODMON"))
                    '            dtCorrectos.Rows.RemoveAt(a)
                    '            If a < (dtCorrectos.Rows.Count - 1) Then
                    '                a = a - 1
                    '            ElseIf a = (dtCorrectos.Rows.Count - 1) And i = (dtCorrectos.Rows.Count - 2) Then
                    '                a = a - 1
                    '            Else
                    '                Exit For
                    '            End If
                    '        End If
                    '    Next
                    'Next

                    lblerrores.Text = "Total Registros : " & dgvErrores.RowCount

                    Grabar_CliCos()

                    Cursor.Current = Cursors.Arrow

                    lblTotRepetidos.Text = "Total Registros : " & dgvRepetidos.RowCount
                Catch ex As Exception
                    If Not transaccion.Equals(Nothing) Then
                        transaccion.Rollback()
                    End If
                    PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                    MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Finally
                    lblTotales.Text = "Total Registros : " + dgvDatos.RowCount.ToString
                    conn.Close()
                End Try
            End Using
        End Using
    End Sub

    Private Function ObtenerSeq()
        Using conn = New OracleConnection(Conexion.retornaConexion)
            conn.Open()
            Dim comando As OracleCommand
            Dim dataAdapter As OracleDataAdapter
            Dim dt As New DataTable
            Dim sql, sequen As String

            Using transaccion = conn.BeginTransaction
                Try
                    sql = " Select EN_CLIENTE_COSTO_SEQ.nextval seq from dual "
                    dt.Clear()
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    dataAdapter = New OracleDataAdapter(comando)
                    dataAdapter.Fill(dt)
                    If dt.Rows.Count = 0 Then
                        If Not transaccion.Equals(Nothing) Then
                            transaccion.Rollback()
                        End If
                        MessageBox.Show("NO se encontró una secuencia. Consulte a Sistemas...", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Return 0
                    Else
                        sequen = dt.Rows(0).Item("seq").ToString
                    End If

                    transaccion.Commit()

                    Return sequen
                Catch ex As Exception
                    If Not transaccion.Equals(Nothing) Then
                        transaccion.Rollback()
                    End If
                    PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                    MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Finally
                    conn.Close()
                End Try
            End Using
        End Using
    End Function

    Private Sub Grabar_CliCos()
        Using conn = New OracleConnection(Conexion.retornaConexion)
            conn.Open()
            Dim comando As OracleCommand
            Dim dataAdapter As OracleDataAdapter
            Dim dt As New DataTable
            Dim yaEsta As Boolean = False
            Dim caca As String

            Using transaccion = conn.BeginTransaction
                Try
                    Dim lngSequen = ""
                    Dim sql = "Set Role Rol_Aplicacion"
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()
                    Dim aux As Int32

                    If rdbNuevo.Checked = True And rdbActualiza.Checked = False And rbdCorrige.Checked = False Then
                        lblTotales.Text = "Grabando Registros"

                        If dtCorrectos.Rows.Count = 0 Then
                            'MessageBox.Show("No hay datos para cargar.", "Finalizado", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            If dgvErrores.RowCount > 0 Then
                                'MessageBox.Show("Hay registros con errores (" & dgvErrores.RowCount & ").", "Finalizado", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            End If
                            If dgvRepetidos.RowCount > 0 Then
                                'MessageBox.Show("Existen registros ya Cargados (" & dgvRepetidos.RowCount & ").", "Finalizado", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            End If
                            Exit Sub
                        End If

                        lngSequen = ObtenerSeq()

                        For i = 0 To dtCorrectos.Rows.Count - 1
                            aux = i
                            lblCarga.Text = (dtCorrectos.Rows(i).Item("FILA") + 1).ToString & " de " & dgvDatos.RowCount.ToString & " procesado"
                            dgvDatos.Rows(dtCorrectos.Rows(i).Item("FILA")).DefaultCellStyle.BackColor = Color.GreenYellow
                            Application.DoEvents()
                            sql = " INSERT INTO en_cliente_costo (ID_NUMERO, RUTCLI, CODPRO, PRECIO, COSTO, MARGEN, FECINI, FECFIN, CANTID, USR_CREAC, CODEMP, TIPO,CODMON, ESTADO) VALUES( "
                            sql = sql & lngSequen & ", "
                            sql = sql & dtCorrectos.Rows(i).Item("RUTCLI") & " , "
                            sql = sql & "'" & dtCorrectos.Rows(i).Item("CODPRO") & "',"
                            sql = sql & dtCorrectos.Rows(i).Item("PRECIO") & ","
                            sql = sql & dtCorrectos.Rows(i).Item("COSTO") & ", "
                            sql = sql & Replace((Math.Round(Double.Parse(((1 - (dtCorrectos.Rows(i).Item("COSTO") / dtCorrectos.Rows(i).Item("PRECIO"))) * 100)), 2).ToString.Replace(",", ".")).ToString, ",", ".") & ", "
                            sql = sql & "TO_DATE('" & Date.Parse(dtCorrectos.Rows(i).Item("FECHAINI")).ToShortDateString & "','DD/MM/YYYY HH24:MI:SS'), "
                            sql = sql & "TO_DATE('" & Date.Parse(dtCorrectos.Rows(i).Item("FECHAFIN")).ToShortDateString & "','DD/MM/YYYY HH24:MI:SS'), "
                            sql = sql & dtCorrectos.Rows(i).Item("CANTIDAD") & ",'"
                            sql = sql & Globales.user & "',"
                            sql = sql & Globales.empresa & ","
                            sql = sql & dtCorrectos.Rows(i).Item("TIPO") & ","
                            sql = sql & dtCorrectos.Rows(i).Item("CODMON") & ", 'V')"
                            caca = sql
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()
                        Next
                    End If

                    If rdbActualiza.Checked = True And rdbNuevo.Checked = False And rbdCorrige.Checked = False Then
                        lblTotales.Text = "Actualizando Registros"
                        Application.DoEvents()

                        lngSequen = ObtenerSeq()

                        For i = 0 To dtCorrectos.Rows.Count - 1
                            lblCarga.Text = (dtCorrectos.Rows(i).Item("FILA") + 1).ToString & " de " & dgvDatos.RowCount.ToString & " procesado"
                            sql = "SELECT * FROM en_cliente_costo"
                            sql = sql & " where rutcli = " & dtCorrectos.Rows(i).Item("RUTCLI")
                            sql = sql & "   And codpro = '" & dtCorrectos.Rows(i).Item("CODPRO") & "'"
                            sql = sql & "   and tipo = " & dtCorrectos.Rows(i).Item("TIPO").ToString
                            sql = sql & "   and ESTADO = 'V'"
                            sql = sql & "   and codemp = " & Globales.empresa.ToString
                            dt.Clear()
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            dataAdapter = New OracleDataAdapter(comando)
                            dataAdapter.Fill(dt)
                            If dt.Rows.Count > 0 Then
                                dgvDatos.Rows(dtCorrectos.Rows(i).Item("FILA")).DefaultCellStyle.BackColor = Color.GreenYellow
                                Application.DoEvents()
                                sql = "          update en_cliente_costo   "
                                sql = sql & "                set precio = " & dtCorrectos.Rows(i).Item("PRECIO").ToString
                                sql = sql & "                   ,costo = " & dtCorrectos.Rows(i).Item("COSTO").ToString
                                sql = sql & "                   ,tipo = " & dtCorrectos.Rows(i).Item("TIPO").ToString
                                sql = sql & "                   ,margen = " & Math.Round(Double.Parse((1 - (dtCorrectos.Rows(i).Item("COSTO") / dtCorrectos.Rows(i).Item("PRECIO"))) * 100), 2).ToString.Replace(",", ".")
                                sql = sql & "                   ,fecini =  TO_DATE('" & Date.Parse(dtCorrectos.Rows(i).Item("FECHAINI")).ToShortDateString & "', 'dd/mm/yyyy')"
                                sql = sql & "                   ,fecfin =  TO_DATE('" & Date.Parse(dtCorrectos.Rows(i).Item("FECHAFIN")).ToShortDateString & "', 'dd/mm/yyyy')"
                                sql = sql & "                   ,estado = 'V' "
                                sql = sql & "                where rutcli = " & dtCorrectos.Rows(i).Item("RUTCLI")
                                sql = sql & "                  And codpro = '" & dtCorrectos.Rows(i).Item("CODPRO") & "'"
                                sql = sql & "                  and tipo = " & dtCorrectos.Rows(i).Item("TIPO").ToString
                                sql = sql & "                  and ESTADO = 'V'"
                                sql = sql & "                  and codemp = " & Globales.empresa.ToString
                                comando = New OracleCommand(sql, conn)
                                comando.Transaction = transaccion
                                comando.ExecuteNonQuery()
                            Else
                                dgvDatos.Rows(dtCorrectos.Rows(i).Item("FILA")).DefaultCellStyle.BackColor = Color.Red
                                Application.DoEvents()
                            End If
                        Next
                    End If

                    If rdbActualiza.Checked = False And rdbNuevo.Checked = False And rbdCorrige.Checked = True Then
                        lblTotales.Text = "Modificando Registros..."
                        Application.DoEvents()

                        lngSequen = ObtenerSeq()

                        For i = 0 To dtCorrectos.Rows.Count - 1
                            lblCarga.Text = (dtCorrectos.Rows(i).Item("FILA") + 1).ToString & " de " & dgvDatos.RowCount.ToString & " procesado"
                            sql = "SELECT * FROM en_cliente_costo"
                            sql = sql & " where rutcli = " & dtCorrectos.Rows(i).Item("RUTCLI")
                            sql = sql & "   And codpro = '" & dtCorrectos.Rows(i).Item("CODPRO") & "'"
                            sql = sql & "   and codemp = " & Globales.empresa.ToString
                            dt.Clear()
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            dataAdapter = New OracleDataAdapter(comando)
                            dataAdapter.Fill(dt)
                            If dt.Rows.Count > 0 Then
                                dgvDatos.Rows(dtCorrectos.Rows(i).Item("FILA")).DefaultCellStyle.BackColor = Color.GreenYellow
                                Application.DoEvents()
                                sql = "          update en_cliente_costo   "
                                sql = sql & "                set "
                                sql = sql & "                   tipo = " & dtCorrectos.Rows(i).Item("TIPO").ToString
                                sql = sql & "                where rutcli = " & dtCorrectos.Rows(i).Item("RUTCLI")
                                sql = sql & "                  And codpro = '" & dtCorrectos.Rows(i).Item("CODPRO") & "'"
                                sql = sql & "                  and codemp = " & Globales.empresa.ToString
                                comando = New OracleCommand(sql, conn)
                                comando.Transaction = transaccion
                                comando.ExecuteNonQuery()
                            Else
                                dgvDatos.Rows(dtCorrectos.Rows(i).Item("FILA")).DefaultCellStyle.BackColor = Color.Red
                                Application.DoEvents()
                            End If
                        Next
                    End If

                    lblCarga.Text = ""

                    transaccion.Commit()

                    If rdbNuevo.Checked = True Then
                        MessageBox.Show("Grabación Finalizada con Exito, para información el numero de ingreso de estas ofertas es el " + lngSequen.ToString, "Finalizado", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Else
                        MessageBox.Show("Grabación Finalizada con Exito", "Finalizado", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If

                Catch ex As Exception
                    If Not transaccion.Equals(Nothing) Then
                        transaccion.Rollback()
                    End If
                    PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                    MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Finally
                    conn.Close()
                End Try
            End Using
        End Using
    End Sub
    Private Function ValidaAntesDeGrabar() As Boolean
        Try
            bd.open_dimerc()


            Dim sql = "set role rol_aplicacion"
            bd.ejecutar(sql)
            sql = " SELECT B.codpro "
            sql = sql & " FROM " & strTabUser
            sql = sql & " A, en_cliente_costo B "
            sql = sql & "WHERE B.codpro=a.codpro "
            sql = sql & "  and B.rutcli=a.rutcli "
            sql = sql & "  and B.codemp=" & Globales.empresa.ToString
            sql = sql & "  and ( "
            sql = sql & "        (A.FECHAI between B.FECINI AND B.FECFIN) "
            sql = sql & "     or (A.FECHAT between B.FECINI AND B.FECFIN) "
            sql = sql & "     or (B.FECINI between FECHAI and FECHAT) "
            sql = sql & "     or (B.FECFIN between FECHAI and FECHAT) "
            sql = sql & "      ) and rownum<=1 "
            Dim dt = bd.sqlSelect(sql)
            If dt.Rows.Count > 0 Then
                Return False
            End If

            sql = " SELECT B.codpro "
            sql = sql & " FROM " & strTabUser
            sql = sql & " A, en_producto_costo B "
            sql = sql & "WHERE B.codpro=a.codpro "
            sql = sql & "  and B.codemp=" & Globales.empresa.ToString
            sql = sql & "  and ( "
            sql = sql & "        (A.FECHAI between B.FECINI AND B.FECFIN) "
            sql = sql & "     or (A.FECHAT between B.FECINI AND B.FECFIN) "
            sql = sql & "     or (B.FECINI between FECHAI and FECHAT) "
            sql = sql & "     or (B.FECFIN between FECHAI and FECHAT) "
            sql = sql & "      ) and rownum<=1 "
            Dim dt3 = bd.sqlSelect(sql)
            If dt3.Rows.Count > 0 Then
                Return False
            End If

            sql = " SELECT codpro "
            sql = sql & " FROM " & strTabUser
            sql = sql & " WHERE fechai>fechat "
            Dim dt2 = bd.sqlSelect(sql)
            If dt2.Rows.Count > 0 Then
                Return False
            End If

            Return True

        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        Finally
            bd.close()
        End Try
    End Function

    Private Sub Crea_Tabla_Temporal(StrTabUser As String)
        Try
            bd.open_dimerc()
            If bd.sqlCheck("select * from tab where tname = '" + StrTabUser + "'") > 0 Then
                bd.ejecutar("drop table " + StrTabUser)
            End If
            Dim SQL = " CREATE TABLE " & StrTabUser & "("
            SQL = SQL & "   CODPRO     VARCHAR2 (10),"
            SQL = SQL & "   RUTCLI     NUMBER   (10),"
            SQL = SQL & "   PRECIO     NUMBER   (10),"
            SQL = SQL & "   COSTO      NUMBER   (10),"
            SQL = SQL & "   FECHAI     DATE,"
            SQL = SQL & "   FECHAT     DATE,"
            SQL = SQL & "   CANTID     NUMBER   (10), "
            SQL = SQL & "   TIPO       NUMBER   (03), "
            SQL = SQL & "   CODMON     NUMBER   (03) "
            SQL = SQL & "   )"
            bd.ejecutar(SQL)


            SQL = " CREATE INDEX " & StrTabUser & "_IDX01 "
            SQL = SQL & " ON " & StrTabUser & " (CODPRO)"
            bd.ejecutar(SQL)

        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If rdbErrores.Checked = True Then
            PuntoControl.RegistroUso("145")
            If dgvErrores.RowCount = 0 Then
                MessageBox.Show("No hay datos consultados en la grilla de cargos", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            Else
                Dim save As New SaveFileDialog
                save.Filter = "Archivo Excel | *.xlsx"
                If save.ShowDialog = System.Windows.Forms.DialogResult.OK Then
                    ExportarDgvExcel(save.FileName)
                    MessageBox.Show("Exportado a " & save.FileName, "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            End If
        Else
            If dgvRepetidos.RowCount = 0 Then
                MessageBox.Show("No hay datos consultados en la grilla de cargos", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            Else
                Dim save As New SaveFileDialog
                save.Filter = "Archivo Excel | *.xlsx"
                If save.ShowDialog = System.Windows.Forms.DialogResult.OK Then
                    ExportarDgvExcel(save.FileName)
                    MessageBox.Show("Exportado a " & save.FileName, "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            End If
        End If
    End Sub
End Class