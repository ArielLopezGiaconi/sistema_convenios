﻿Imports System.Data.OracleClient

Public Class frmReversaPropuestas
    Dim bd As New Conexion
    Dim flaggrilla As Boolean
    Dim PuntoControl As New PuntoControl
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Close()
    End Sub

    Private Sub frmReversaPropuestas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        StartPosition = FormStartPosition.Manual
        Location = New Point(0, 0)
        PuntoControl.RegistroUso("175")
        buscar()
    End Sub

    Private Sub btn_Buscar_Click(sender As Object, e As EventArgs) Handles btn_Buscar.Click
        buscar()
        PuntoControl.RegistroUso("177")
    End Sub
    Private Sub buscar()
        Try
            bd.open_dimerc()
            Dim Sql = " select num_reajuste ""Nro. Reajuste"",numcot ""Nro. Cotizacion"",rutcli ""Rut Cliente"", "
            Sql = Sql & " getrazonsocial(codemp,rutcli) ""Razon Social"", observacion ""Observacion"", "
            Sql = Sql & " usr_creac ""Usuario"",fecha_creac ""Fecha Creacion"" ,decode(tipopropuesta,0,'PROPUESTA NUEVA','REAJUSTE') ""Tipo"", "
            Sql = Sql & " fecini_convenio ""Fecha Inicio Reajuste"",fecfin_convenio ""Fecha Vencimiento Reajuste"", "
            Sql = Sql & " tipoconsumo ""Tipo de Consumo"" from en_reajuste_convenio  "
            Sql = Sql & " where estado=2 "
            Sql = Sql & " and codemp=3 "
            dgvConsulta.DataSource = bd.sqlSelect(Sql)
            dgvConsulta.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            If flaggrilla = False Then
                Dim check As New DataGridViewCheckBoxColumn
                check.HeaderText = "¿Reversa?"
                check.Name = "¿Reversa?"
                dgvConsulta.Columns.Insert(0, check)
                dgvConsulta.Columns(0).Visible = True
            End If
            flaggrilla = True


        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub dgvConsulta_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvConsulta.CellContentClick
        Try
            If e.RowIndex <> -1 Then
                If e.ColumnIndex = 0 Then
                    If dgvConsulta.Item("¿Reversa?", e.RowIndex).Value = 0 Then
                        dgvConsulta.Item("¿Reversa?", e.RowIndex).Value = 1
                    Else
                        dgvConsulta.Item("¿Reversa?", e.RowIndex).Value = 0
                    End If
                End If
            End If
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub btn_Grabar_Click(sender As Object, e As EventArgs) Handles btn_Grabar.Click
        Using conn = New OracleConnection(Conexion.retornaConexion)
            conn.Open()
            Dim comando As OracleCommand
            Dim dataAdapter As OracleDataAdapter
            Using transaccion = conn.BeginTransaction
                Try
                    PuntoControl.RegistroUso("176")
                    For i = 0 To dgvConsulta.RowCount - 1
                        If dgvConsulta.Item("¿Reversa?", i).Value = 1 Then
                            Dim sql = "update en_reajuste_convenio set "
                            sql += " estado = 1 "
                            sql += " where codemp=3 "
                            sql += " and num_reajuste=" + dgvConsulta.Item("Nro. Reajuste", i).Value.ToString
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()
                        End If
                    Next

                    transaccion.Commit()

                    MessageBox.Show("Reversa realizada con éxito", "Finalizado", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    buscar()
                Catch ex As Exception
                    If Not transaccion.Equals(Nothing) Then
                        transaccion.Rollback()
                    End If
                    PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                    MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Finally
                    conn.Close()
                End Try
            End Using
        End Using
    End Sub

    Private Sub Panel1_Paint(sender As Object, e As PaintEventArgs) Handles Panel1.Paint

    End Sub
End Class