﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmCanalProducto
    Inherits MetroFramework.Forms.MetroForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.dgvListas = New System.Windows.Forms.DataGridView()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btn_Ubicar = New System.Windows.Forms.Button()
        Me.txtPrefijo = New System.Windows.Forms.TextBox()
        Me.txtMgPrefijo = New System.Windows.Forms.TextBox()
        Me.txtMultiplo = New System.Windows.Forms.TextBox()
        Me.txtPrecio = New System.Windows.Forms.TextBox()
        Me.txtListaPrecio = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtDescuento = New System.Windows.Forms.TextBox()
        Me.txtMargen = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtDescripcion = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtCodigo = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbxSelectTodo = New System.Windows.Forms.CheckBox()
        Me.btn_Grabar = New System.Windows.Forms.Button()
        Me.btn_Salir = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvListas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dgvListas)
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Controls.Add(Me.txtDescripcion)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtCodigo)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.cbxSelectTodo)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 54)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(838, 426)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Producto"
        '
        'dgvListas
        '
        Me.dgvListas.AllowUserToAddRows = False
        Me.dgvListas.AllowUserToDeleteRows = False
        Me.dgvListas.AllowUserToResizeColumns = False
        Me.dgvListas.AllowUserToResizeRows = False
        Me.dgvListas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvListas.Location = New System.Drawing.Point(7, 202)
        Me.dgvListas.Name = "dgvListas"
        Me.dgvListas.ReadOnly = True
        Me.dgvListas.RowHeadersVisible = False
        Me.dgvListas.Size = New System.Drawing.Size(825, 200)
        Me.dgvListas.TabIndex = 3
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btn_Ubicar)
        Me.GroupBox2.Controls.Add(Me.txtPrefijo)
        Me.GroupBox2.Controls.Add(Me.txtMgPrefijo)
        Me.GroupBox2.Controls.Add(Me.txtMultiplo)
        Me.GroupBox2.Controls.Add(Me.txtPrecio)
        Me.GroupBox2.Controls.Add(Me.txtListaPrecio)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.txtDescuento)
        Me.GroupBox2.Controls.Add(Me.txtMargen)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Location = New System.Drawing.Point(6, 93)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(826, 103)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Lista de Precio"
        '
        'btn_Ubicar
        '
        Me.btn_Ubicar.Image = Global.Convenios_New.My.Resources.Resources.search
        Me.btn_Ubicar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_Ubicar.Location = New System.Drawing.Point(189, 17)
        Me.btn_Ubicar.Name = "btn_Ubicar"
        Me.btn_Ubicar.Size = New System.Drawing.Size(67, 23)
        Me.btn_Ubicar.TabIndex = 2
        Me.btn_Ubicar.Text = "Ubicar"
        Me.btn_Ubicar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_Ubicar.UseVisualStyleBackColor = True
        '
        'txtPrefijo
        '
        Me.txtPrefijo.BackColor = System.Drawing.SystemColors.Info
        Me.txtPrefijo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPrefijo.Location = New System.Drawing.Point(549, 73)
        Me.txtPrefijo.Name = "txtPrefijo"
        Me.txtPrefijo.Size = New System.Drawing.Size(96, 21)
        Me.txtPrefijo.TabIndex = 1
        '
        'txtMgPrefijo
        '
        Me.txtMgPrefijo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMgPrefijo.Location = New System.Drawing.Point(444, 73)
        Me.txtMgPrefijo.MaxLength = 5
        Me.txtMgPrefijo.Name = "txtMgPrefijo"
        Me.txtMgPrefijo.Size = New System.Drawing.Size(96, 21)
        Me.txtMgPrefijo.TabIndex = 1
        '
        'txtMultiplo
        '
        Me.txtMultiplo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMultiplo.Location = New System.Drawing.Point(336, 73)
        Me.txtMultiplo.Name = "txtMultiplo"
        Me.txtMultiplo.Size = New System.Drawing.Size(96, 21)
        Me.txtMultiplo.TabIndex = 1
        '
        'txtPrecio
        '
        Me.txtPrecio.BackColor = System.Drawing.SystemColors.Info
        Me.txtPrecio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPrecio.Location = New System.Drawing.Point(23, 73)
        Me.txtPrecio.Name = "txtPrecio"
        Me.txtPrecio.Size = New System.Drawing.Size(96, 21)
        Me.txtPrecio.TabIndex = 1
        '
        'txtListaPrecio
        '
        Me.txtListaPrecio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtListaPrecio.Location = New System.Drawing.Point(83, 19)
        Me.txtListaPrecio.Name = "txtListaPrecio"
        Me.txtListaPrecio.Size = New System.Drawing.Size(100, 21)
        Me.txtListaPrecio.TabIndex = 1
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(26, 22)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(29, 13)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "Lista"
        '
        'txtDescuento
        '
        Me.txtDescuento.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescuento.Location = New System.Drawing.Point(232, 73)
        Me.txtDescuento.Name = "txtDescuento"
        Me.txtDescuento.Size = New System.Drawing.Size(96, 21)
        Me.txtDescuento.TabIndex = 1
        '
        'txtMargen
        '
        Me.txtMargen.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMargen.Location = New System.Drawing.Point(125, 73)
        Me.txtMargen.MaxLength = 5
        Me.txtMargen.Name = "txtMargen"
        Me.txtMargen.Size = New System.Drawing.Size(96, 21)
        Me.txtMargen.TabIndex = 1
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(566, 57)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(56, 13)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Precio Fijo"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(466, 57)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(61, 13)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Mg. PreFijo"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(361, 57)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(43, 13)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Multiplo"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(54, 57)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(36, 13)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Precio"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(249, 57)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(58, 13)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "Descuento"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(152, 57)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(43, 13)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Margen"
        '
        'txtDescripcion
        '
        Me.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripcion.Location = New System.Drawing.Point(89, 58)
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(516, 21)
        Me.txtDescripcion.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(20, 61)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(61, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Descripcion"
        '
        'txtCodigo
        '
        Me.txtCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodigo.Location = New System.Drawing.Point(89, 32)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.Size = New System.Drawing.Size(100, 21)
        Me.txtCodigo.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(20, 35)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(40, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Código"
        '
        'cbxSelectTodo
        '
        Me.cbxSelectTodo.AutoSize = True
        Me.cbxSelectTodo.Location = New System.Drawing.Point(727, 403)
        Me.cbxSelectTodo.Name = "cbxSelectTodo"
        Me.cbxSelectTodo.Size = New System.Drawing.Size(105, 17)
        Me.cbxSelectTodo.TabIndex = 87
        Me.cbxSelectTodo.Text = "Seleccionar todo"
        Me.cbxSelectTodo.UseVisualStyleBackColor = True
        '
        'btn_Grabar
        '
        Me.btn_Grabar.Image = Global.Convenios_New.My.Resources.Resources.Floppy
        Me.btn_Grabar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btn_Grabar.Location = New System.Drawing.Point(850, 118)
        Me.btn_Grabar.Name = "btn_Grabar"
        Me.btn_Grabar.Size = New System.Drawing.Size(52, 52)
        Me.btn_Grabar.TabIndex = 1
        Me.btn_Grabar.Text = "Grabar"
        Me.btn_Grabar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btn_Grabar.UseVisualStyleBackColor = True
        '
        'btn_Salir
        '
        Me.btn_Salir.Image = Global.Convenios_New.My.Resources.Resources.salir
        Me.btn_Salir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btn_Salir.Location = New System.Drawing.Point(850, 60)
        Me.btn_Salir.Name = "btn_Salir"
        Me.btn_Salir.Size = New System.Drawing.Size(52, 52)
        Me.btn_Salir.TabIndex = 1
        Me.btn_Salir.Text = "Salir"
        Me.btn_Salir.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btn_Salir.UseVisualStyleBackColor = True
        '
        'frmCanalProducto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(903, 491)
        Me.ControlBox = False
        Me.Controls.Add(Me.btn_Grabar)
        Me.Controls.Add(Me.btn_Salir)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmCanalProducto"
        Me.ShowIcon = False
        Me.Text = "Relación Lista de Precio - Producto"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgvListas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btn_Salir As System.Windows.Forms.Button
    Friend WithEvents btn_Grabar As System.Windows.Forms.Button
    Friend WithEvents txtDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtCodigo As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dgvListas As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtMargen As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtPrefijo As System.Windows.Forms.TextBox
    Friend WithEvents txtMgPrefijo As System.Windows.Forms.TextBox
    Friend WithEvents txtMultiplo As System.Windows.Forms.TextBox
    Friend WithEvents txtPrecio As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtDescuento As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents btn_Ubicar As System.Windows.Forms.Button
    Friend WithEvents txtListaPrecio As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents cbxSelectTodo As CheckBox
End Class
