﻿Imports System.Data.OracleClient
Public Class frmCanalProducto
    Dim bd As New Conexion
    Dim cargado As Boolean = False
    Dim indprod As Integer
    Dim costo = 0
    Dim PuntoControl As New PuntoControl
    Public Sub New()

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub
    Public Sub New(codigo As String, descripcion As String, costos As Int64)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()
        txtCodigo.Text = codigo
        txtDescripcion.Text = descripcion
        costo = costos
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub
    Private Sub frmCanalProducto_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        PuntoControl.RegistroUso("98")
        Me.StartPosition = FormStartPosition.Manual
        Me.Location = New Point(100, 50)

        If txtCodigo.Text <> "" And txtDescripcion.Text <> "" Then
            buscaCanalProducto()
            dgvListas.Focus()
        End If
        If Not Globales.detDerUsu(Globales.user, 66) Then
            btn_Grabar.Enabled = False
        End If

        Me.Show()
        Application.DoEvents()
        txtCodigo.Focus()
    End Sub
    Private Sub btn_Salir_Click(sender As System.Object, e As System.EventArgs) Handles btn_Salir.Click
        Me.Close()
    End Sub

    Private Sub txtCodigo_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtCodigo.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Trim(txtCodigo.Text) <> "" Then
                txtCodigo.Text = Globales.BuscaCodigoDimerc(txtCodigo.Text)
                If Trim(txtCodigo.Text) = "" Then
                    MessageBox.Show("Producto no existe", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Exit Sub
                End If
                'cargado = False
                buscaCanalProducto()
                'cargado = True
            Else
                txtDescripcion.Focus()
            End If
        End If
    End Sub
    Private Sub buscaCanalProducto()
        Try
            bd.open_dimerc()
            Dim Sql = "SELECT codpro1 FROM ma_relaprod"
            Sql = Sql & " WHERE codpro2  = '" & txtCodigo.Text & "'"
            Sql = Sql & "   and tiporela = 4"
            Sql = Sql & "   and codemp   = " & Globales.empresa.ToString
            Sql = Sql & "   and codpro1 <> codpro2"
            Dim dt = bd.sqlSelect(Sql)
            If dt.Rows.Count > 0 Then
                MsgBox("El Item Principal es " & CStr(dt.Rows(0).Item("codpro1")))
                txtCodigo.Text = CStr(dt.Rows(0).Item("codpro1"))
                txtCodigo.Focus()
                Exit Sub
            End If

            Sql = "select codpro,despro "
            Sql = Sql & "from MA_PRODUCT "
            Sql = Sql & "where codpro = '" & txtCodigo.Text & "'"
            dt.Clear()
            dt = bd.sqlSelect(Sql)
            If dt.Rows.Count > 0 Then
                txtDescripcion.Text = CStr(dt.Rows(0).Item("despro"))
            Else
                MessageBox.Show("Producto no Existe. Debe estar Previamente Registrado", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            End If
            verCanales()
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub txtDescripción_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtDescripcion.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Trim(txtDescripcion.Text) <> "" Then
                Dim frmbusca As New frmBuscarProducto(txtDescripcion.Text, False)
                frmbusca.ShowDialog()
                txtCodigo.Text = frmbusca.dgvDatos.Item("Código", frmbusca.dgvDatos.CurrentRow.Index).Value.ToString
                txtDescripcion.Text = frmbusca.dgvDatos.Item("Descripción", frmbusca.dgvDatos.CurrentRow.Index).Value.ToString
                buscaCanalProducto()
            End If
        End If
    End Sub
    Public Sub formatear()
        Try
            'transforma los enteros en numeros con 2 decimales
            If txtMargen.Text.Trim <> "" Then
                txtMargen.Text = Format(CDec(txtMargen.Text), "N2")
            End If
            If txtDescuento.Text.Trim <> "" Then
                txtDescuento.Text = Format(CDec(txtDescuento.Text), "N0")
            End If
            If txtPrecio.Text.Trim <> "" Then
                txtPrecio.Text = Format(CDec(txtPrecio.Text), "N0")
            End If
            If txtMgPrefijo.Text.Trim <> "" Then
                txtMgPrefijo.Text = Format(CDec(txtMgPrefijo.Text), "N2")
            End If
            If txtPrefijo.Text.Trim <> "" Then
                txtPrefijo.Text = Format(CDec(txtPrefijo.Text), "N0")
            End If

        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub txtMargen_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtMargen.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            PuntoControl.RegistroUso("102")
            If Not Trim(txtMargen.Text) = "" Then
                If Val(txtMargen.Text) <> 0 And Val(txtMargen.Text) <= 99 Then
                    txtPrecio.Text = Math.Round(costo / (1 - (Val(txtMargen.Text) / 100)), 0)
                End If
                formatear()
                txtDescuento.Focus()
                PutDatos()
            End If
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtDescuento_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtDescuento.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            PuntoControl.RegistroUso("103")
            If Not Trim(txtDescuento.Text) = "" Then
                formatear()
                For i = 0 To dgvListas.RowCount - 1
                    dgvListas.Item("descto.", i).Value = txtDescuento.Text
                    dgvListas.Item("cambio", i).Value = 1
                Next
                txtMultiplo.Focus()
                PutDatos()
            End If
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtPrecio_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtPrecio.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            PuntoControl.RegistroUso("101")
            If Not Trim(txtPrecio.Text) = "" Then
                formatear()
                txtMargen.Text = Math.Round(((txtPrecio.Text - costo) / txtPrecio.Text) * 100, 2)

                txtMargen.Focus()
                PutDatos()
            End If
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtMultiplo_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtMultiplo.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            PuntoControl.RegistroUso("104")
            If Not Trim(txtMultiplo.Text) = "" Then
                formatear()
                txtMgPrefijo.Focus()
                PutDatos()
            End If
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtMgPrefijo_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtMgPrefijo.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            PuntoControl.RegistroUso("105")
            If Not Trim(txtMgPrefijo.Text) = "" Then
                If Val(txtMgPrefijo.Text) = 0 Then
                    txtPrefijo.Text = "0"
                End If
                If Val(txtMgPrefijo.Text) <> 0 And Val(txtMgPrefijo.Text) <= 99 Then
                    txtPrefijo.Text = Math.Round(costo / (1 - (Val(txtMgPrefijo.Text) / 100)), 0)
                End If
                formatear()
                PutDatos()
            End If
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub
    Private Sub verCanales()
        Try
            bd.open_dimerc()
            Dim Sql = " select a.nomcnl ""Canal"", nvl(b.precio,0) ""Precio"", "
            Sql = Sql & " nvl(b.margen,0) ""Margen"", nvl(b.pordes,0) ""Descto."", decode(b.mgprefijo,null,0,b.mgprefijo) ""Mg.PFijo"", nvl(b.prefij,0) ""P.Fijo"",  "
            Sql = Sql & " decode(modsup,'','S',modsup) ""Sup"",  "
            Sql = Sql & " nvl(b.multip,1) ""Multiplo"", "
            Sql = Sql & " nvl(GETCOSTOCOMERCIAL(3,'" + txtCodigo.Text + "'),0) costo,nvl(GETCOSTOCOMERCIAL(6,'" + txtCodigo.Text + "'),0) costo_corp, 0 cambio,a.codcnl   "
            Sql = Sql & " from ma_listaprecio a, re_canprod b,ma_product c "
            Sql = Sql & "  where    "
            Sql = Sql & "   b.codpro(+)='" & txtCodigo.Text & "'   "
            Sql = Sql & "   and a.codemp(+) = " + Globales.empresa.ToString
            Sql = Sql & "   and  a.codemp = b.codemp(+) "
            Sql = Sql & "   and  a.codcnl = b.codcnl(+) "
            Sql = Sql & "   and  a.activa='S'  "
            Sql = Sql & "   and b.codpro=c.codpro(+)  "
            Sql = Sql & "   order by a.codcnl "

            dgvListas.DataSource = bd.sqlSelect(Sql)
            dgvListas.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            dgvListas.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvListas.Columns("Canal").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            dgvListas.Columns("costo").Visible = False
            dgvListas.Columns("costo_corp").Visible = False
            dgvListas.Columns("cambio").Visible = False
            dgvListas.Columns("codcnl").Visible = False
            dgvListas.Columns("Precio").DefaultCellStyle.Format = "n0"
            dgvListas.Columns("Margen").DefaultCellStyle.Format = "n2"
            dgvListas.Columns("Descto.").DefaultCellStyle.Format = "n2"
            dgvListas.Columns("Mg.PFijo").DefaultCellStyle.Format = "n2"
            dgvListas.Columns("P.Fijo").DefaultCellStyle.Format = "n0"
            dgvListas.Columns("Multiplo").DefaultCellStyle.Format = "n0"
            If cargado = False Then
                Dim check As New DataGridViewCheckBoxColumn
                check.HeaderText = "¿APLICA?"
                check.Name = "¿APLICA?"
                dgvListas.Columns.Insert(0, check)
                dgvListas.Columns(0).Visible = True
                cargado = True
            End If
            dgvListas.Focus()
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub dgvListas_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvListas.CellContentClick
        If e.RowIndex <> -1 Then
            If e.ColumnIndex = 0 Then
                If dgvListas.Item("¿APLICA?", e.RowIndex).Value = 0 Then
                    dgvListas.Item("¿APLICA?", e.RowIndex).Value = 1
                    dgvListas.Item("cambio", dgvListas.CurrentRow.Index).Value = 1
                Else
                    dgvListas.Item("¿APLICA?", e.RowIndex).Value = 0
                    dgvListas.Item("cambio", dgvListas.CurrentRow.Index).Value = 0
                End If
            End If
        End If
    End Sub

    Private Sub PutDatos()
        If cargado = True And dgvListas.CurrentRow IsNot Nothing Then
            Try
                For i = 0 To dgvListas.RowCount - 1
                    If txtListaPrecio.Text = dgvListas.Item("codcnl", i).Value.ToString Then
                        dgvListas.Item("Margen", i).Value = txtMargen.Text
                        dgvListas.Item("Descto.", i).Value = txtDescuento.Text
                        dgvListas.Item("Precio", i).Value = Replace(txtPrecio.Text, ".", "")
                        dgvListas.Item("Multiplo", i).Value = txtMultiplo.Text
                        dgvListas.Item("Mg.PFijo", i).Value = txtMgPrefijo.Text
                        dgvListas.Item("P.Fijo", i).Value = Replace(txtPrefijo.Text, ".", "")

                        dgvListas.Rows(i).Selected = True
                        dgvListas.CurrentCell = dgvListas.Rows(i).Cells(1)
                        Application.DoEvents()
                        'dgvListas.Focus()
                        Exit Sub
                    End If
                Next

            Catch ex As Exception
                PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Try
        End If
    End Sub

    Private Sub dgvListas_CurrentCellChanged(sender As System.Object, e As System.EventArgs) Handles dgvListas.CurrentCellChanged
        If cargado = True And dgvListas.CurrentRow IsNot Nothing Then
            Try
                txtListaPrecio.Text = dgvListas.Item("codcnl", dgvListas.CurrentRow.Index).Value.ToString
                txtMargen.Text = dgvListas.Item("Margen", dgvListas.CurrentRow.Index).Value.ToString
                txtDescuento.Text = dgvListas.Item("Descto.", dgvListas.CurrentRow.Index).Value.ToString
                txtPrecio.Text = dgvListas.Item("Precio", dgvListas.CurrentRow.Index).Value.ToString
                txtMultiplo.Text = dgvListas.Item("Multiplo", dgvListas.CurrentRow.Index).Value.ToString
                txtMgPrefijo.Text = dgvListas.Item("Mg.PFijo", dgvListas.CurrentRow.Index).Value.ToString
                txtPrefijo.Text = dgvListas.Item("P.Fijo", dgvListas.CurrentRow.Index).Value.ToString
                If Globales.empresa = 3 Then
                    costo = Val(dgvListas.Item("costo", dgvListas.CurrentRow.Index).Value)
                Else
                    costo = Val(dgvListas.Item("costo_corp", dgvListas.CurrentRow.Index).Value)
                End If
            Catch ex As Exception
                PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Try
        End If
    End Sub

    Private Sub dgvListas_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles dgvListas.KeyDown
        If e.KeyCode = Keys.Enter Then
            PuntoControl.RegistroUso("100")
            txtMargen.Enabled = True
            txtDescuento.Enabled = True
            txtPrecio.Enabled = True
            txtMultiplo.Enabled = True
            txtMgPrefijo.Enabled = True
            txtPrefijo.Enabled = True
            txtPrecio.Focus()
            e.Handled = True
        End If
    End Sub

    Private Sub btn_Grabar_Click(sender As System.Object, e As System.EventArgs) Handles btn_Grabar.Click
        If Trim(txtCodigo.Text) = "" Then
            MessageBox.Show("Debe cargar un código antes de grabar datos", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        Dim paso As Boolean = False
        For i = 0 To dgvListas.RowCount - 1
            If dgvListas.Item("¿APLICA?", i).Value = 1 Then
                paso = True
            End If
        Next
        If Not Globales.detDerUsu(Globales.user, 66) Then
            MessageBox.Show("No Tiene Derecho a Grabar los Cambios en Esta Opción de Programa (Der.66)", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        If MessageBox.Show("NOTA: Si el producto tiene Items Asociados. Se modificarán todos sus items con los valores del principal. ¿Desea Continuar?",
                           "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = System.Windows.Forms.DialogResult.Yes Then
            PuntoControl.RegistroUso("107")
            grabar()
        End If
    End Sub
    Private Sub grabar()
        Using conn = New OracleConnection(Conexion.retornaConexion)
            conn.Open()
            Dim comando As OracleCommand
            Dim dataAdapter As OracleDataAdapter
            Dim Sql As String
            Dim dt, dtaux As New DataTable
            Using transaccion = conn.BeginTransaction
                Try
                    For i = 0 To dgvListas.RowCount - 1
                        If dgvListas.Item("cambio", i).Value = 1 Then

                            Sql = " select costo from ma_product  "
                            Sql = Sql & "where codpro='" & txtCodigo.Text & "' "
                            dt.Clear()
                            comando = New OracleCommand(Sql, conn, transaccion)
                            dataAdapter = New OracleDataAdapter(comando)
                            dataAdapter.Fill(dt)
                            If dt.Rows(0).Item("COSTO") = 0 Then
                                MessageBox.Show("Producto tiene costo comercial 0 , no sera modificado.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                'If Not transaccion.Equals(Nothing) Then
                                '    transaccion.Rollback()
                                'End If
                                'Exit Sub
                                Continue For
                            End If
                            If (dgvListas.Item("Precio", i).Value = 0 And dgvListas.Item("codcnl", i).Value >= 610) Or (dgvListas.Item("Precio", i).Value = 0 And dgvListas.Item("codcnl", i).Value <= 680) Then
                                MessageBox.Show("Al producto se le esta asignando precio 0 en la lista " + dgvListas.Item("codcnl", i).Value.ToString +
                                     " , no sera modificado.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                'If Not transaccion.Equals(Nothing) Then
                                '    transaccion.Rollback()
                                'End If
                                'Exit Sub
                                Continue For
                            End If

                            Sql = "SELECT codpro2 FROM ma_relaprod"
                            Sql = Sql & " WHERE codpro1 = '" & txtCodigo.Text & "'"
                            Sql = Sql & "   and tiporela = 4"
                            Sql = Sql & "   and codemp = " & Globales.empresa.ToString
                            dt.Clear()
                            comando = New OracleCommand(Sql, conn, transaccion)
                            dataAdapter = New OracleDataAdapter(comando)
                            dataAdapter.Fill(dt)
                            If dt.Rows.Count > 0 Then
                                For Each fila As DataRow In dt.Rows
                                    Sql = "select multip, codpro, codcnl, margen, precio, nvl(pordes,0) pordes, nvl(prefij,0) prefij, decode(MODSUP,'','S','N') xx "
                                    Sql = Sql & " from re_canprod "
                                    Sql = Sql & " where codpro = '" & CStr(fila.Item("codpro2")) & "' AND codcnl between 40 and 49 "
                                    Sql = Sql & "   and codcnl = " & dgvListas.Item("codcnl", i).Value
                                    Sql = Sql & "   and codemp = " & Globales.empresa.ToString
                                    dtaux.Clear()
                                    comando = New OracleCommand(Sql, conn, transaccion)
                                    dataAdapter = New OracleDataAdapter(comando)
                                    dataAdapter.Fill(dtaux)
                                    If dtaux.Rows.Count = 0 Then
                                        Sql = "Insert Into re_canprod  (codpro,codemp,codcnl,margen,mgprefijo,precio,multip,factor,pordes,prefij,MODSUP) values("
                                        Sql = Sql & "'" & CStr(fila.Item("codpro2")) & "',"
                                        Sql = Sql & Globales.empresa.ToString & ","
                                        Sql = Sql & dgvListas.Item("codcnl", i).Value.ToString & ","
                                        Sql = Sql & dgvListas.Item("Margen", i).Value.ToString.Replace(",", ".") & ","
                                        Sql = Sql & dgvListas.Item("Mg.PFijo", i).Value.ToString.Replace(",", ".") & ","
                                        Sql = Sql & dgvListas.Item("Precio", i).Value.ToString.Replace(",", ".") & ","
                                        Sql = Sql & dgvListas.Item("Multiplo", i).Value.ToString & ","
                                        Sql = Sql & "0,"
                                        Sql = Sql & dgvListas.Item("Descto.", i).Value.ToString.Replace(",", ".") & ","
                                        Sql = Sql & dgvListas.Item("P.Fijo", i).Value.ToString.Replace(",", ".")
                                        Sql = Sql & ",'S')"
                                        comando = New OracleCommand(Sql, conn, transaccion)
                                        comando.ExecuteNonQuery()
                                    Else
                                        Sql = "update re_canprod set margen = " & dgvListas.Item("Margen", i).Value.ToString.Replace(",", ".")
                                        Sql = Sql & "," & "mgprefijo = " & dgvListas.Item("Mg.PFijo", i).Value.ToString.Replace(",", ".")
                                        Sql = Sql & "," & "precio = " & dgvListas.Item("Precio", i).Value.ToString.Replace(",", ".")
                                        Sql = Sql & "," & "multip = " & dgvListas.Item("Multiplo", i).Value.ToString
                                        Sql = Sql & "," & "pordes = " & dgvListas.Item("Descto.", i).Value.ToString.Replace(",", ".")
                                        Sql = Sql & "," & "prefij = " & dgvListas.Item("P.Fijo", i).Value.ToString.Replace(",", ".")
                                        Sql = Sql & "," & "modsup = 'S' "
                                        Sql = Sql & " where codpro = '" & CStr(fila.Item("codpro2")) & "'"
                                        Sql = Sql & " and codcnl = " & dgvListas.Item("codcnl", i).Value.ToString
                                        Sql = Sql & " and codemp = " & Globales.empresa.ToString
                                        comando = New OracleCommand(Sql, conn, transaccion)
                                        comando.ExecuteNonQuery()
                                    End If

                                Next
                            Else
                                Sql = "SELECT codpro FROM RE_CANPROD WHERE CODCNL= " & dgvListas.Item("codcnl", i).Value.ToString
                                Sql = Sql & " AND CODPRO = '" & txtCodigo.Text & "'"
                                Sql = Sql & " AND CODEMP = " & Globales.empresa.ToString
                                dtaux.Clear()
                                comando = New OracleCommand(Sql, conn, transaccion)
                                dataAdapter = New OracleDataAdapter(comando)
                                dataAdapter.Fill(dtaux)
                                If dtaux.Rows.Count > 0 Then
                                    Sql = "update re_canprod set margen = " & dgvListas.Item("Margen", i).Value.ToString.Replace(",", ".")
                                    Sql = Sql & "," & "mgprefijo = " & dgvListas.Item("Mg.PFijo", i).Value.ToString.Replace(",", ".")
                                    Sql = Sql & "," & "precio = " & dgvListas.Item("Precio", i).Value.ToString.Replace(",", ".")
                                    Sql = Sql & "," & "multip = " & dgvListas.Item("Multiplo", i).Value.ToString
                                    Sql = Sql & "," & "pordes = " & dgvListas.Item("Descto.", i).Value.ToString.Replace(",", ".")
                                    Sql = Sql & "," & "prefij = " & dgvListas.Item("P.Fijo", i).Value.ToString.Replace(",", ".")
                                    Sql = Sql & "," & "modsup = 'S' "
                                    Sql = Sql & " where codpro = '" & CStr(dtaux.Rows(0).Item("codpro")) & "'"
                                    Sql = Sql & "  and codcnl = " & dgvListas.Item("codcnl", i).Value.ToString
                                    Sql = Sql & "  and codemp = " & Globales.empresa.ToString
                                    comando = New OracleCommand(Sql, conn, transaccion)
                                    comando.ExecuteNonQuery()
                                Else
                                    Sql = "Insert Into re_canprod  (codpro,codemp,codcnl,margen,mgprefijo,precio,multip,factor,pordes,prefij,MODSUP) values("
                                    Sql = Sql & "'" & txtCodigo.Text & "',"
                                    Sql = Sql & Globales.empresa.ToString & ","
                                    Sql = Sql & dgvListas.Item("codcnl", i).Value.ToString & ","
                                    Sql = Sql & dgvListas.Item("Margen", i).Value.ToString.Replace(",", ".") & ","
                                    Sql = Sql & dgvListas.Item("Mg.PFijo", i).Value.ToString.Replace(",", ".") & ","
                                    Sql = Sql & dgvListas.Item("Precio", i).Value.ToString.Replace(",", ".") & ","
                                    Sql = Sql & dgvListas.Item("Multiplo", i).Value.ToString & ","
                                    Sql = Sql & "0,"
                                    Sql = Sql & dgvListas.Item("Descto.", i).Value.ToString.Replace(",", ".") & ","
                                    Sql = Sql & dgvListas.Item("P.Fijo", i).Value.ToString.Replace(",", ".")
                                    Sql = Sql & ",'S')"
                                    comando = New OracleCommand(Sql, conn, transaccion)
                                    comando.ExecuteNonQuery()
                                End If
                            End If
                        End If
                    Next

                    Sql = "select estpro,pedsto from ma_product where codpro = '" & txtCodigo.Text & "'"
                    dt.Clear()
                    comando = New OracleCommand(Sql, conn, transaccion)
                    dataAdapter = New OracleDataAdapter(comando)
                    dataAdapter.Fill(dt)

                    If dt.Rows.Count > 0 Then
                        Select Case dt.Rows(0).Item("estpro")
                            Case 1  ' Activo
                                If CStr(dt.Rows(0).Item("pedsto")) = "S" Then
                                    indprod = 0    '   1    'AA
                                ElseIf CStr(dt.Rows(0).Item("pedsto")) = "P" Then
                                    indprod = 4    '
                                End If
                            Case 2   ' Descontinuado
                                If CStr(dt.Rows(0).Item("pedsto")) = "S" Then
                                    indprod = 5
                                End If
                            Case 6  ' Borrado no visible en ventas
                                indprod = 7
                        End Select
                        If indprod = 0 Then
                            Sql = "update re_canprod set indica = ' ' "
                        Else
                            Sql = "update re_canprod set indica = " & indprod
                        End If
                        Sql = Sql & " where codpro = '" & txtCodigo.Text & "'"
                        comando = New OracleCommand(Sql, conn, transaccion)
                        comando.ExecuteNonQuery()
                    End If


                    Sql = " BEGIN PUT_RECALCULA_PRECIO_PROD(3,'" & txtCodigo.Text & "'); END; "
                    comando = New OracleCommand(Sql, conn, transaccion)
                    comando.ExecuteNonQuery()

                    Sql = " BEGIN PUT_RECALCULA_PRECIO_PROD(6,'" & txtCodigo.Text & "'); END; "
                    comando = New OracleCommand(Sql, conn, transaccion)
                    comando.ExecuteNonQuery()

                    Sql = " BEGIN PUT_RECALCULA_PRECIOFIJO(3,'" & txtCodigo.Text & "'); END; "
                    comando = New OracleCommand(Sql, conn, transaccion)
                    comando.ExecuteNonQuery()

                    Sql = " BEGIN PUT_RECALCULA_PRECIOFIJO(6,'" & txtCodigo.Text & "'); END; "
                    comando = New OracleCommand(Sql, conn, transaccion)
                    comando.ExecuteNonQuery()

                    limpiar()

                    transaccion.Commit()
                    MessageBox.Show("Datos Grabados con Éxito", "Finalizado", MessageBoxButtons.OK, MessageBoxIcon.Information)

                Catch ex As Exception
                    If Not transaccion.Equals(Nothing) Then
                        transaccion.Rollback()
                    End If
                    PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                    MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Finally
                    conn.Close()
                End Try
            End Using
        End Using
    End Sub
    Private Sub limpiar()
        dgvListas.DataSource = False
        txtMargen.Text = ""
        txtDescuento.Text = ""
        txtPrecio.Text = ""
        txtPrefijo.Text = ""
        txtMgPrefijo.Text = ""
        txtMultiplo.Text = ""
        dgvListas.Columns.Clear()
        cargado = False
        txtDescripcion.Text = ""
        txtListaPrecio.Text = ""
        txtCodigo.Text = ""
        txtMargen.Enabled = False
        txtDescuento.Enabled = False
        txtPrecio.Enabled = False
        txtMultiplo.Enabled = False
        txtMgPrefijo.Enabled = False
        txtPrefijo.Enabled = False
    End Sub

    Private Sub btn_Ubicar_Click(sender As System.Object, e As System.EventArgs) Handles btn_Ubicar.Click
        Try
            Dim encontro As Boolean = False
            PuntoControl.RegistroUso("99")
            For i = 0 To dgvListas.RowCount - 1
                If txtListaPrecio.Text = dgvListas.Item("codcnl", i).Value.ToString Then
                    dgvListas.Rows(i).Selected = True
                    dgvListas.CurrentCell = dgvListas.Rows(i).Cells(1)
                    Application.DoEvents()
                    dgvListas.Focus()
                    encontro = True
                    Exit Sub
                End If
            Next
            If encontro = False Then
                MessageBox.Show("El numero de lista no se encuentra, reintente con otro por favor.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
            dgvListas.Focus()
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub txtListaPrecio_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtListaPrecio.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Trim(txtListaPrecio.Text) <> "" Then
                btn_Ubicar.PerformClick()
            End If
        End If
    End Sub

    Private Sub dgvListas_ColumnHeaderMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvListas.ColumnHeaderMouseClick
        For Each fila As DataGridViewRow In dgvListas.Rows
            If dgvListas.Item("cambio", fila.Index).Value = 1 Then
                dgvListas.Item("¿APLICA?", fila.Index).Value = 1
            Else
                dgvListas.Item("¿APLICA?", fila.Index).Value = 0
            End If
        Next
    End Sub

    Private Sub cbxSelectTodo_Click(sender As Object, e As EventArgs) Handles cbxSelectTodo.Click
        For i = 0 To dgvListas.RowCount - 1
            If cbxSelectTodo.Checked = 0 Or cbxSelectTodo.Checked = False Then
                dgvListas.Item("¿APLICA?", i).Value = 0
            Else
                dgvListas.Item("¿APLICA?", i).Value = 1
            End If
        Next
    End Sub

    Private Sub txtPrefijo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPrefijo.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            PuntoControl.RegistroUso("106")
            If Not Trim(txtPrefijo.Text) = "" Then
                formatear()
                txtMgPrefijo.Text = Math.Round(((txtPrefijo.Text - costo) / txtPrefijo.Text) * 100, 2)

                PutDatos()
            End If
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    'Private Sub cboxCambiaTodo_CheckedChanged(sender As Object, e As EventArgs)
    '    If cboxCambiaTodo.Checked = True Or cboxCambiaTodo.Checked = 1 Then
    '        txtPrecio.BackColor = Color.Orange
    '        txtMultiplo.BackColor = Color.Orange
    '        txtDescuento.BackColor = Color.Orange
    '        txtMargen.BackColor = Color.Orange
    '        txtMgPrefijo.BackColor = Color.Orange
    '        txtPrefijo.BackColor = Color.Orange
    '    Else
    '        txtPrecio.BackColor = Color.White
    '        txtMultiplo.BackColor = Color.White
    '        txtDescuento.BackColor = Color.White
    '        txtMargen.BackColor = Color.White
    '        txtMgPrefijo.BackColor = Color.White
    '        txtPrefijo.BackColor = Color.White
    '    End If
    '    Application.DoEvents()
    'End Sub

    'Private Sub txtPrefijo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPrefijo.KeyPress
    '    If e.KeyChar = Convert.ToChar(Keys.Enter) Then
    '        If Not Trim(txtPrefijo.Text) = "" Then
    '            formatear()
    '            txtMgPrefijo.Text = Math.Round(((txtPrefijo.Text - costo) / txtPrefijo.Text) * 100, 2)
    '            If cboxCambiaTodo.Checked = True Or cboxCambiaTodo.Checked = 1 Then
    '                For i = 0 To dgvListas.RowCount - 1
    '                    dgvListas.Item("P.Fijo", i).Value = txtPrefijo.Text
    '                    dgvListas.Item("Mg.PFijo", i).Value = txtMgPrefijo.Text
    '                    dgvListas.Item("cambio", i).Value = 1
    '                Next
    '            End If
    '            'txtMargen.Focus()
    '        End If
    '    ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
    '        e.KeyChar = Convert.ToChar(Asc(","))
    '    Else
    '        Globales.soloNumeros(sender, e)
    '    End If
    'End Sub
End Class