﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCargaMasivaCostoEspecial
    Inherits MetroFramework.Forms.MetroForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCargaMasivaCostoEspecial))
        Me.rbdCorrige = New System.Windows.Forms.RadioButton()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.lblCarga = New System.Windows.Forms.Label()
        Me.dgvDatos = New System.Windows.Forms.DataGridView()
        Me.lblTotales = New System.Windows.Forms.Label()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.dgvErrores = New System.Windows.Forms.DataGridView()
        Me.lblerrores = New System.Windows.Forms.Label()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.lblTotRepetidos = New System.Windows.Forms.Label()
        Me.dgvRepetidos = New System.Windows.Forms.DataGridView()
        Me.rdbActualiza = New System.Windows.Forms.RadioButton()
        Me.rdbNuevo = New System.Windows.Forms.RadioButton()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lblNota = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.rdbRepetidos = New MetroFramework.Controls.MetroRadioButton()
        Me.rdbErrores = New MetroFramework.Controls.MetroRadioButton()
        Me.btn_Salir = New MetroFramework.Controls.MetroButton()
        Me.Button1 = New MetroFramework.Controls.MetroButton()
        Me.btn_Grabar = New MetroFramework.Controls.MetroButton()
        Me.btn_Importar = New MetroFramework.Controls.MetroButton()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.dgvDatos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.dgvErrores, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        CType(Me.dgvRepetidos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'rbdCorrige
        '
        Me.rbdCorrige.AutoSize = True
        Me.rbdCorrige.Location = New System.Drawing.Point(14, 101)
        Me.rbdCorrige.Name = "rbdCorrige"
        Me.rbdCorrige.Size = New System.Drawing.Size(141, 17)
        Me.rbdCorrige.TabIndex = 18
        Me.rbdCorrige.Text = "Corregir Tipo de negocio"
        Me.rbdCorrige.UseVisualStyleBackColor = True
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Location = New System.Drawing.Point(13, 126)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1105, 372)
        Me.TabControl1.TabIndex = 16
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.lblCarga)
        Me.TabPage1.Controls.Add(Me.dgvDatos)
        Me.TabPage1.Controls.Add(Me.lblTotales)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1097, 346)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "EXCEL"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'lblCarga
        '
        Me.lblCarga.AutoSize = True
        Me.lblCarga.Location = New System.Drawing.Point(916, 6)
        Me.lblCarga.Name = "lblCarga"
        Me.lblCarga.Size = New System.Drawing.Size(0, 13)
        Me.lblCarga.TabIndex = 7
        '
        'dgvDatos
        '
        Me.dgvDatos.AllowUserToAddRows = False
        Me.dgvDatos.AllowUserToDeleteRows = False
        Me.dgvDatos.AllowUserToResizeColumns = False
        Me.dgvDatos.AllowUserToResizeRows = False
        Me.dgvDatos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDatos.Location = New System.Drawing.Point(6, 22)
        Me.dgvDatos.Name = "dgvDatos"
        Me.dgvDatos.ReadOnly = True
        Me.dgvDatos.RowHeadersVisible = False
        Me.dgvDatos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDatos.Size = New System.Drawing.Size(1082, 318)
        Me.dgvDatos.TabIndex = 3
        '
        'lblTotales
        '
        Me.lblTotales.AutoSize = True
        Me.lblTotales.Location = New System.Drawing.Point(3, 6)
        Me.lblTotales.Name = "lblTotales"
        Me.lblTotales.Size = New System.Drawing.Size(84, 13)
        Me.lblTotales.TabIndex = 6
        Me.lblTotales.Text = "Total Registros :"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.dgvErrores)
        Me.TabPage2.Controls.Add(Me.lblerrores)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1097, 346)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "ERRORES"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'dgvErrores
        '
        Me.dgvErrores.AllowUserToAddRows = False
        Me.dgvErrores.AllowUserToDeleteRows = False
        Me.dgvErrores.AllowUserToResizeColumns = False
        Me.dgvErrores.AllowUserToResizeRows = False
        Me.dgvErrores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvErrores.Location = New System.Drawing.Point(6, 22)
        Me.dgvErrores.Name = "dgvErrores"
        Me.dgvErrores.ReadOnly = True
        Me.dgvErrores.RowHeadersVisible = False
        Me.dgvErrores.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvErrores.Size = New System.Drawing.Size(1082, 318)
        Me.dgvErrores.TabIndex = 7
        '
        'lblerrores
        '
        Me.lblerrores.AutoSize = True
        Me.lblerrores.Location = New System.Drawing.Point(3, 6)
        Me.lblerrores.Name = "lblerrores"
        Me.lblerrores.Size = New System.Drawing.Size(84, 13)
        Me.lblerrores.TabIndex = 8
        Me.lblerrores.Text = "Total Registros :"
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.lblTotRepetidos)
        Me.TabPage3.Controls.Add(Me.dgvRepetidos)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(1097, 346)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Info. Repetidos"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'lblTotRepetidos
        '
        Me.lblTotRepetidos.AutoSize = True
        Me.lblTotRepetidos.Location = New System.Drawing.Point(3, 6)
        Me.lblTotRepetidos.Name = "lblTotRepetidos"
        Me.lblTotRepetidos.Size = New System.Drawing.Size(84, 13)
        Me.lblTotRepetidos.TabIndex = 9
        Me.lblTotRepetidos.Text = "Total Registros :"
        '
        'dgvRepetidos
        '
        Me.dgvRepetidos.AllowUserToAddRows = False
        Me.dgvRepetidos.AllowUserToDeleteRows = False
        Me.dgvRepetidos.AllowUserToResizeColumns = False
        Me.dgvRepetidos.AllowUserToResizeRows = False
        Me.dgvRepetidos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvRepetidos.Location = New System.Drawing.Point(6, 22)
        Me.dgvRepetidos.Name = "dgvRepetidos"
        Me.dgvRepetidos.ReadOnly = True
        Me.dgvRepetidos.RowHeadersVisible = False
        Me.dgvRepetidos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvRepetidos.Size = New System.Drawing.Size(1066, 318)
        Me.dgvRepetidos.TabIndex = 8
        '
        'rdbActualiza
        '
        Me.rdbActualiza.AutoSize = True
        Me.rdbActualiza.Location = New System.Drawing.Point(13, 83)
        Me.rdbActualiza.Name = "rdbActualiza"
        Me.rdbActualiza.Size = New System.Drawing.Size(196, 17)
        Me.rdbActualiza.TabIndex = 14
        Me.rdbActualiza.Text = "Actualización de datos ya existentes"
        Me.rdbActualiza.UseVisualStyleBackColor = True
        '
        'rdbNuevo
        '
        Me.rdbNuevo.AutoSize = True
        Me.rdbNuevo.Checked = True
        Me.rdbNuevo.Location = New System.Drawing.Point(13, 65)
        Me.rdbNuevo.Name = "rdbNuevo"
        Me.rdbNuevo.Size = New System.Drawing.Size(148, 17)
        Me.rdbNuevo.TabIndex = 15
        Me.rdbNuevo.TabStop = True
        Me.rdbNuevo.Text = "Ingreso de nuevas ofertas"
        Me.rdbNuevo.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.lblNota)
        Me.Panel1.Location = New System.Drawing.Point(226, 16)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(621, 92)
        Me.Panel1.TabIndex = 13
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(110, -2)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(408, 14)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "El Formato del Archivo debe contener los siguientes titulos de columnas :"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Blue
        Me.Label5.Location = New System.Drawing.Point(58, 15)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(556, 14)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "CODPRO - RUTCLI - PRECIO - COSTO - FECHAINI - FECHAFIN - CANTIDAD - TIPO - CODMON" &
    ""
        '
        'lblNota
        '
        Me.lblNota.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNota.Location = New System.Drawing.Point(5, 38)
        Me.lblNota.Name = "lblNota"
        Me.lblNota.Size = New System.Drawing.Size(611, 47)
        Me.lblNota.TabIndex = 0
        Me.lblNota.Text = resources.GetString("lblNota.Text")
        Me.lblNota.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.rdbRepetidos)
        Me.GroupBox1.Controls.Add(Me.rdbErrores)
        Me.GroupBox1.Controls.Add(Me.btn_Salir)
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Location = New System.Drawing.Point(855, 8)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(261, 102)
        Me.GroupBox1.TabIndex = 17
        Me.GroupBox1.TabStop = False
        '
        'rdbRepetidos
        '
        Me.rdbRepetidos.AutoSize = True
        Me.rdbRepetidos.Location = New System.Drawing.Point(6, 37)
        Me.rdbRepetidos.Name = "rdbRepetidos"
        Me.rdbRepetidos.Size = New System.Drawing.Size(75, 15)
        Me.rdbRepetidos.TabIndex = 8
        Me.rdbRepetidos.Text = "Repetidos"
        Me.rdbRepetidos.UseSelectable = True
        '
        'rdbErrores
        '
        Me.rdbErrores.AutoSize = True
        Me.rdbErrores.Location = New System.Drawing.Point(6, 14)
        Me.rdbErrores.Name = "rdbErrores"
        Me.rdbErrores.Size = New System.Drawing.Size(59, 15)
        Me.rdbErrores.TabIndex = 8
        Me.rdbErrores.Text = "Errores"
        Me.rdbErrores.UseSelectable = True
        '
        'btn_Salir
        '
        Me.btn_Salir.BackColor = System.Drawing.Color.Red
        Me.btn_Salir.Location = New System.Drawing.Point(177, 14)
        Me.btn_Salir.Name = "btn_Salir"
        Me.btn_Salir.Size = New System.Drawing.Size(77, 43)
        Me.btn_Salir.TabIndex = 10
        Me.btn_Salir.Text = "Salir"
        Me.btn_Salir.UseCustomBackColor = True
        Me.btn_Salir.UseCustomForeColor = True
        Me.btn_Salir.UseSelectable = True
        Me.btn_Salir.UseStyleColors = True
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Green
        Me.Button1.Location = New System.Drawing.Point(87, 14)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(84, 43)
        Me.Button1.TabIndex = 9
        Me.Button1.Text = "Exportar"
        Me.Button1.UseCustomBackColor = True
        Me.Button1.UseCustomForeColor = True
        Me.Button1.UseSelectable = True
        Me.Button1.UseStyleColors = True
        '
        'btn_Grabar
        '
        Me.btn_Grabar.BackColor = System.Drawing.Color.DodgerBlue
        Me.btn_Grabar.Location = New System.Drawing.Point(119, 19)
        Me.btn_Grabar.Name = "btn_Grabar"
        Me.btn_Grabar.Size = New System.Drawing.Size(99, 43)
        Me.btn_Grabar.TabIndex = 8
        Me.btn_Grabar.Text = "Grabar"
        Me.btn_Grabar.UseCustomBackColor = True
        Me.btn_Grabar.UseCustomForeColor = True
        Me.btn_Grabar.UseSelectable = True
        Me.btn_Grabar.UseStyleColors = True
        '
        'btn_Importar
        '
        Me.btn_Importar.BackColor = System.Drawing.Color.Green
        Me.btn_Importar.ForeColor = System.Drawing.Color.Black
        Me.btn_Importar.Location = New System.Drawing.Point(13, 19)
        Me.btn_Importar.Name = "btn_Importar"
        Me.btn_Importar.Size = New System.Drawing.Size(99, 43)
        Me.btn_Importar.TabIndex = 19
        Me.btn_Importar.Text = "Importar"
        Me.btn_Importar.UseCustomBackColor = True
        Me.btn_Importar.UseCustomForeColor = True
        Me.btn_Importar.UseSelectable = True
        Me.btn_Importar.UseStyleColors = True
        '
        'frmCargaMasivaCostoEspecial
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1126, 510)
        Me.Controls.Add(Me.btn_Importar)
        Me.Controls.Add(Me.btn_Grabar)
        Me.Controls.Add(Me.rbdCorrige)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.rdbActualiza)
        Me.Controls.Add(Me.rdbNuevo)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "frmCargaMasivaCostoEspecial"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.dgvDatos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        CType(Me.dgvErrores, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        CType(Me.dgvRepetidos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents rbdCorrige As RadioButton
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents lblCarga As Label
    Friend WithEvents dgvDatos As DataGridView
    Friend WithEvents lblTotales As Label
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents dgvErrores As DataGridView
    Friend WithEvents lblerrores As Label
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents lblTotRepetidos As Label
    Friend WithEvents dgvRepetidos As DataGridView
    Friend WithEvents rdbActualiza As RadioButton
    Friend WithEvents rdbNuevo As RadioButton
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents lblNota As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents rdbRepetidos As MetroFramework.Controls.MetroRadioButton
    Friend WithEvents rdbErrores As MetroFramework.Controls.MetroRadioButton
    Friend WithEvents btn_Salir As MetroFramework.Controls.MetroButton
    Friend WithEvents Button1 As MetroFramework.Controls.MetroButton
    Friend WithEvents btn_Grabar As MetroFramework.Controls.MetroButton
    Public WithEvents btn_Importar As MetroFramework.Controls.MetroButton
End Class
