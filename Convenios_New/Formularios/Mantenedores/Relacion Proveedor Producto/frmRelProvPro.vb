﻿Imports System.Data.OracleClient
Public Class frmRelProvPro
    Dim bd As New Conexion
    Dim bolestaProducto As Boolean
    Dim flagCombo = True
    Dim PuntoControl As New PuntoControl
    Public Sub New(codigo As String, descripcion As String)
        ' Llamada necesaria para el diseñador.
        InitializeComponent()
        txtCodigo.Text = codigo
        txtDescripcion.Text = descripcion
        cargaProvPrincipal(codigo, False)
    End Sub
    Public Sub New()
        InitializeComponent()
    End Sub
    Private Sub frmRelProvPro_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        PuntoControl.RegistroUso("112")
        Cursor.Current = Cursors.WaitCursor
        Me.StartPosition = FormStartPosition.Manual
        Me.Location = New Point(100, 50)
        cargaCombo()
        If Globales.detDerUsu(Globales.user, 57) Then
            lblAutorización.Visible = False
        Else
            lblAutorización.Visible = True
        End If
        Me.Show()
        Application.DoEvents()
        txtCodigo.Focus()
    End Sub
    Private Sub cargaCombo()
        Try
            bd.open_dimerc()
            Dim Sql = "SELECT codval, desval "
            Sql = Sql & " FROM de_dominio where coddom=28"
            Sql = Sql & " Order by desval "
            Dim dt = bd.sqlSelect(Sql)
            cmbUniCom.DataSource = dt
            cmbUniCom.ValueMember = "codval"
            cmbUniCom.DisplayMember = "desval"
            cmbUniCom.SelectedIndex = -1
            cmbUniCom.Refresh()

            Sql = " select codval,desval  "
            Sql = Sql & "from de_dominio  "
            Sql = Sql & "where coddom = 18"
            Sql = Sql & "order by desval "
            dt = bd.sqlSelect(Sql)
            cmbUnidad.DataSource = dt
            cmbUnidad.ValueMember = "codval"
            cmbUnidad.DisplayMember = "desval"
            cmbUnidad.SelectedIndex = -1
            cmbUnidad.Refresh()

            Sql = "select codlin,deslin from ma_lineapr order by deslin "
            dt = bd.sqlSelect(Sql)
            cmbLinea.DataSource = dt
            cmbLinea.ValueMember = "codlin"
            cmbLinea.DisplayMember = "deslin"
            cmbLinea.SelectedIndex = -1
            cmbLinea.Refresh()

            Sql = "select codfam,desfam from ma_familia order by desfam "
            dt = bd.sqlSelect(Sql)
            cmbFamilia.DataSource = dt
            cmbFamilia.ValueMember = "codfam"
            cmbFamilia.DisplayMember = "desfam"
            cmbFamilia.SelectedIndex = -1
            cmbFamilia.Refresh()

            Sql = "select codmar,desmar from ma_marcapr order by desmar "
            dt = bd.sqlSelect(Sql)
            cmbMarca.DataSource = dt
            cmbMarca.ValueMember = "codmar"
            cmbMarca.DisplayMember = "desmar"
            cmbMarca.SelectedIndex = -1
            cmbMarca.Refresh()

            Sql = "select codmod,desmod from Ma_modelpr order by desmod"
            dt = bd.sqlSelect(Sql)
            cmbModelo.DataSource = dt
            cmbModelo.ValueMember = "codmod"
            cmbModelo.DisplayMember = "desmod"
            cmbModelo.SelectedIndex = -1
            cmbModelo.Refresh()

            flagCombo = True
            If Globales.detDerUsu(Globales.user, 57) Then
                lblAutorización.Visible = False
            Else
                lblAutorización.Visible = True
            End If
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub
    Private Function cargaProvPrincipal(codigo As String, cargaNuevo As Boolean) As Boolean
        Try
            cmbProvPri.DataSource = Nothing
            bd.open_dimerc()
            Dim StrQuery = "select a.rutprv,a.nombre, nvl(b.priori,0) priori from ma_proveed a, re_provpro b"
            StrQuery = StrQuery & " where b.codpro ='" & codigo & "'"
            StrQuery = StrQuery & "   and b.codemp = " & Globales.empresa.ToString
            StrQuery = StrQuery & "   and a.rutprv = b.rutprv "
            Dim dt = bd.sqlSelect(StrQuery)
            Dim dtaux As New DataTable
            If cargaNuevo = True Then
                StrQuery = "select rutprv,nombre,0 priori "
                StrQuery = StrQuery & "from ma_proveed "
                StrQuery = StrQuery & "where rutprv = '" & txtNumRut.Text & "'"
                dtaux = bd.sqlSelect(StrQuery)
                If dtaux.Rows.Count > 0 Then
                    dt.Merge(dtaux)
                Else
                    MessageBox.Show("Proveedor no Existe. Debe ser ingresado Previamente.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If
            flagCombo = False
            cmbProvPri.DataSource = dt
            cmbProvPri.ValueMember = "rutprv"
            cmbProvPri.DisplayMember = "nombre"
            cmbProvPri.SelectedIndex = -1
            cmbProvPri.Refresh()
            For Each fila As DataRow In dt.Rows
                If Val(fila.Item("priori")) = 1 Then
                    txtPriori.Text = fila.Item("nombre").ToString
                End If
            Next
            flagCombo = True
            Return True
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        Finally
            bd.close()
        End Try
    End Function
    Private Sub btn_Salir_Click(sender As System.Object, e As System.EventArgs) Handles btn_Salir.Click
        Me.Close()
    End Sub

    Private Sub btn_Limpiar_Click(sender As System.Object, e As System.EventArgs) Handles btn_Limpiar.Click

        txtCodigo.Text = ""
        txtDescripcion.Text = ""
        txtNumRut.Text = ""
        txtRSocial.Text = ""
        txtCodProveedor.Text = ""
        txtDescProveedor.Text = ""
        txtLineaProveedor.Text = ""
        cmbLinea.SelectedIndex = -1
        cmbFamilia.SelectedIndex = -1
        cmbMarca.SelectedIndex = -1
        cmbModelo.SelectedIndex = -1
        txtCBarra.Text = ""
        cmbUnidad.SelectedIndex = -1
        cmbUniCom.SelectedIndex = -1
        txtCosto.Text = ""
        cmbLinea.SelectedIndex = -1
        cmbFamilia.SelectedIndex = -1
        cmbMarca.SelectedIndex = -1
        cmbUnidad.SelectedIndex = -1
        cmbModelo.SelectedIndex = -1
        cmbProvPri.DataSource = Nothing
        lblAutorización.Visible = False
        btn_Grabar.Visible = False

        txtCodProveedor.Focus()

        PuntoControl.RegistroUso("116")
    End Sub

    Private Sub txtCodigo_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtCodigo.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Trim(txtCodigo.Text) <> "" Then
                txtCodigo.Text = Globales.BuscaCodigoDimerc(txtCodigo.Text)
                If Trim(txtCodigo.Text) = "" Then
                    MessageBox.Show("Producto No Exsite.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtCodigo.Focus()
                    Exit Sub
                End If

                PuntoControl.RegistroUso("113")

                cargaDatos()
                cargaProvPrincipal(txtCodigo.Text, False)

            Else
                txtDescripcion.Focus()
            End If
        End If
    End Sub

    Private Sub txtCodProveedor_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtCodProveedor.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Trim(txtCodProveedor.Text) <> "" Then
                validaExistencia()
            Else
                txtDescProveedor.Focus()
            End If
        End If
    End Sub

    Private Sub cargaDatos()
        If Trim(txtCodigo.Text) <> "" Then
            Try
                bd.open_dimerc()
                Dim Sql = "select codpro,despro,codlin,codfam, "
                Sql = Sql & "codmar,nvl(codmod,0) codmod,nvl(codbar,0) codbar,coduni,costo "
                Sql = Sql & "from MA_PRODUCT "
                Sql = Sql & "where codpro = '" & txtCodigo.Text & "'"
                Dim dtCliente = bd.sqlSelect(Sql)
                If dtCliente.Rows.Count > 0 Then
                    txtDescripcion.Text = dtCliente.Rows(0).Item("despro").ToString
                    cmbLinea.SelectedValue = dtCliente.Rows(0).Item("codlin")
                    cmbFamilia.SelectedValue = dtCliente.Rows(0).Item("codfam")
                    cmbMarca.SelectedValue = dtCliente.Rows(0).Item("codmar")
                    cmbModelo.SelectedValue = dtCliente.Rows(0).Item("codmod")
                    cmbUnidad.SelectedValue = dtCliente.Rows(0).Item("coduni")

                    txtCBarra.Text = IIf(dtCliente.Rows(0).Item("codbar") Is Nothing, 0, CStr(dtCliente.Rows(0).Item("codbar")))

                    txtCosto.Text = IIf(dtCliente.Rows(0).Item("costo") Is Nothing, 0, CStr(dtCliente.Rows(0).Item("costo")))

                    txtNumRut.Focus()
                    btn_Grabar.Visible = True
                Else
                    MessageBox.Show("Producto no Existe. Debe ser ingresado previamente", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Exit Sub
                End If

            Catch ex As Exception
                PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Finally
                bd.close()
            End Try
        Else
            MessageBox.Show("Debe ingresar Codigo de Producto para consulta.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
    End Sub



    Private Sub validaExistencia()
        Try
            bd.open_dimerc()
            Dim sql = "select a.codpro,b.despro,a.rutprv,c.nombre,a.deprpv from re_provpro a, ma_product b , ma_proveed c where "
            If Trim(txtNumRut.Text) <> "" Then
                sql = sql & "     a.rutprv = " & txtNumRut.Text
                sql = sql & " and a.codemp = " & Globales.empresa.ToString
                sql = sql & " and "
            End If
            If Trim(txtCodProveedor.Text) <> "" Then
                sql = sql & "    a.coprpv = '" & txtCodProveedor.Text & "'"
                sql = sql & "and a.codemp = " & Globales.empresa.ToString
            End If
            sql = sql & " and c.rutprv = a.rutprv and b.codpro = a.codpro "
            Dim dt = bd.sqlSelect(sql)
            If dt.Rows.Count > 0 Then
                If Trim(txtCodigo.Text) = "" Then
                    txtCodigo.Text = CStr(dt.Rows(0).Item("codpro"))
                    txtDescripcion.Text = CStr(dt.Rows(0).Item("despro"))
                    txtNumRut.Text = dt.Rows(0).Item("rutprv")
                    txtRSocial.Text = CStr(dt.Rows(0).Item("nombre"))
                    txtDescProveedor.Text = CStr(dt.Rows(0).Item("deprpv"))
                    cargaDatos()
                Else
                    If txtCodigo.Text <> dt.Rows(0).Item("codpro") Then
                        MessageBox.Show("Cod. Proveedor ya existe", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        txtCodProveedor.Focus()
                        Exit Sub
                    End If
                End If
            Else
                If Trim(txtCodigo.Text) <> "" Then
                    MessageBox.Show("No existe código de Proveedor", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtDescProveedor.Focus()
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub txtCosto_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtCosto.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Trim(txtCosto.Text) <> "" Then
                btn_Grabar.Focus()
            End If
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtDescProveedor_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtDescProveedor.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Trim(txtDescProveedor.Text) <> "" Then
                txtLineaProveedor.Focus()
            End If
        End If
    End Sub

    Private Sub txtDescripcion_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtDescripcion.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Trim(txtDescripcion.Text) <> "" Then
                Dim busca As New frmBuscarProducto(txtDescripcion.Text, False)
                txtCodigo.Text = busca.dgvDatos.Item("Código", busca.dgvDatos.CurrentRow.Index).Value
                cargaDatos()
                If bolestaProducto Then
                    txtNumRut.Focus()
                Else
                    txtDescripcion.Focus()
                End If
            Else
                txtCodigo.Focus()
            End If
        End If
    End Sub

    Private Sub txtLineaProveedor_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtLineaProveedor.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            btn_Grabar.Focus()
        End If
    End Sub

    Private Sub txtRSocial_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtRSocial.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Trim(txtRSocial.Text) <> "" Then
                If txtNumRut.Text = "" Then
                    Dim buscador As New frmBusca(txtRSocial.Text, True, txtCodigo.Text)
                    buscador.Tag = "RazonSocial"
                    buscador.ShowDialog()

                    txtNumRut.Text = buscador.dgvDatos.Item("Rut", buscador.dgvDatos.CurrentRow.Index).Value.ToString
                    If txtNumRut.Text <> "" Then
                        datosRelacion()
                        txtCodProveedor.Focus()
                    Else
                        txtRSocial.Focus()
                    End If
                End If
            End If
        End If
    End Sub
    Private Sub datosRelacion()
        Try
            bd.open_dimerc()
            Dim StrQuery As String
            StrQuery = "select rutprv,nombre "
            StrQuery = StrQuery & "from ma_proveed "
            StrQuery = StrQuery & "where rutprv = '" & txtNumRut.Text & "'"
            Dim dt = bd.sqlSelect(StrQuery)
            If dt.Rows.Count > 0 Then
                txtRSocial.Text = dt.Rows(0).Item("nombre")
                If cargaProvPrincipal(txtCodigo.Text, True) = True Then
                    bolestaProducto = False

                    StrQuery = "select a.coprpv,a.deprpv,a.liprpv,b.costo,nvl(a.unicom,0) unicom "
                    StrQuery = StrQuery & " from re_provpro a , ma_product b"
                    StrQuery = StrQuery & " where a.rutprv = '" & txtNumRut.Text & "'"
                    StrQuery = StrQuery & " and a.codpro = '" & txtCodigo.Text & "'"
                    StrQuery = StrQuery & " and a.codemp = " & Globales.empresa.ToString
                    StrQuery = StrQuery & " and a.codpro = b.codpro "
                    dt.Clear()
                    dt = bd.sqlSelect(StrQuery)
                    If dt.Rows.Count > 0 Then

                        txtCodProveedor.Text = CStr(dt.Rows(0).Item("coprpv"))
                        txtDescProveedor.Text = CStr(dt.Rows(0).Item("deprpv"))
                        txtLineaProveedor.Text = CStr(dt.Rows(0).Item("liprpv"))
                        txtCosto.Text = CStr(dt.Rows(0).Item("costo"))

                        cmbUniCom.SelectedValue = dt.Rows(0).Item("unicom")

                        bolestaProducto = True
                        txtCodProveedor.Focus()
                    Else
                        MessageBox.Show("Relacion Codigo proveedor no existe,puede crearla ahora", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        txtCodProveedor.Focus()
                    End If

                End If
            Else
                MessageBox.Show("Proveedor no Existe. Debe ser ingresado Previamente.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtNumRut.Focus()
            End If

        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub cmbProvPri_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cmbProvPri.SelectedIndexChanged
        If flagCombo = True Then
            txtPriori.Text = cmbProvPri.Text
        End If
    End Sub

    Private Sub btn_Borrar_Click(sender As System.Object, e As System.EventArgs) Handles btn_Borrar.Click
        If Not Globales.detDerUsu(Globales.user, 89) Then
            MessageBox.Show("No tiene derecho a Eliminar Relación (Der. 89)", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        If Trim(txtNumRut.Text) = "" Then
            MessageBox.Show("Debe ingresar un Rut de proveedor", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        If Trim(txtCodigo.Text) = "" Then
            MessageBox.Show("Debe ingresar un Código Dimerc", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        If Trim(txtCodProveedor.Text) = "" Then
            MessageBox.Show("Debe ingresar el código de producto del proveedor", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        If MessageBox.Show("¿Desea borrar la relación?", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = System.Windows.Forms.DialogResult.Yes Then
            PuntoControl.RegistroUso("117")
            borrar()
        End If
    End Sub

    Private Sub borrar()
        Using conn = New OracleConnection(Conexion.retornaConexion)
            conn.Open()
            Dim comando As OracleCommand
            Dim dataadapter As OracleDataAdapter
            Dim dt As New DataTable
            Using transaccion = conn.BeginTransaction
                Try
                    Dim sql = "select * from en_recepci a, de_recepci b where"
                    sql = sql & "     a.rutprv = " & txtNumRut.Text
                    sql = sql & " and a.codemp = " & Globales.empresa.ToString
                    sql = sql & " and a.numrec = b.numrec "
                    sql = sql & " and b.coddim = '" & txtCodigo.Text & "'"
                    sql = sql & " and b.codemp = a.codemp "
                    dt.Clear()
                    comando = New OracleCommand(Sql, conn)
                    comando.Transaction = transaccion
                    dataadapter = New OracleDataAdapter(comando)
                    dataadapter.Fill(dt)

                    If dt.Rows.Count > 0 Then
                        MessageBox.Show("Existen Ordenes de Compras realizadas a este Proveedor. " & Chr(10) & "No es posible eliminar la relación", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Exit Sub
                    End If

                    sql = "select * from en_ordcomp a, de_ordcomp b"
                    sql = sql & " WHERE a.rutprv = " & txtNumRut.Text
                    sql = sql & "   and a.codemp = " & Globales.empresa.ToString
                    sql = sql & "   and b.codpro = '" & txtCodigo.Text & "'"
                    sql = sql & "   and b.numord = a.numord "
                    sql = sql & "   and b.codemp = a.codemp "
                    dt.Clear()
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    dataadapter = New OracleDataAdapter(comando)
                    dataadapter.Fill(dt)

                    If dt.Rows.Count > 0 Then
                        MessageBox.Show("Existen Ordenes de Compra " & Chr(10) & "No es posible eliminar la relación", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Exit Sub
                    End If

                    sql = "Delete re_provpro where coprpv =  '" & txtCodProveedor.Text & "'"
                    sql = sql & " and rutprv = " & txtNumRut.Text
                    sql = sql & " and codpro = '" & txtCodigo.Text & "'"
                    sql = sql & " and codemp = " & Globales.empresa.ToString
                    comando = New OracleCommand(sql, conn, transaccion)
                    comando.ExecuteNonQuery()

                    transaccion.Commit()
                    btn_Limpiar.PerformClick()
                    txtCodigo.Focus()

                    MessageBox.Show("Relación eliminada con éxito", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information)

                Catch ex As Exception
                    PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                    MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Finally
                    conn.Close()
                End Try
            End Using
        End Using
    End Sub

    Private Sub btn_Grabar_Click(sender As System.Object, e As System.EventArgs) Handles btn_Grabar.Click
        If Trim(txtNumRut.Text) = "" Then
            MessageBox.Show("Debe ingresar rut proveedor", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        If Trim(txtCodigo.Text) = "" Then
            MessageBox.Show("Debe ingresar codigo producto", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        If Trim(txtDescProveedor.Text) = "" Then
            MessageBox.Show("Debe ingresar Descripcion Proveedor", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        If Trim(txtCodProveedor.Text) = "" Then
            MessageBox.Show("Debe ingresar Cod Proveedor", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        If cmbUniCom.SelectedIndex = -1 Then
            MessageBox.Show("Recuerde Asignar Unidad de compra", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        If MessageBox.Show("¿Desea grabar los datos?", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = System.Windows.Forms.DialogResult.Yes Then
            PuntoControl.RegistroUso("115")
            grabar()
        End If
    End Sub
    Private Sub grabar()
        Using conn = New OracleConnection(Conexion.retornaConexion)
            conn.Open()
            Dim comando As OracleCommand
            Dim dataAdapter As OracleDataAdapter
            Using transaccion = conn.BeginTransaction
                Try
                    Dim sql As String
                    Dim dt As New DataTable
                    Dim cuenta = 0
                    sql = " Select codpro From re_provpro "
                    sql = sql & " Where rutprv = " & txtNumRut.Text
                    sql = sql & "   and coprpv = '" & txtCodProveedor.Text & "'"
                    sql = sql & "   and codpro <> '" & txtCodigo.Text & "'"
                    sql = sql & "   and codemp = " & Globales.empresa.ToString
                    dt.Clear()
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    dataAdapter = New OracleDataAdapter(comando)
                    dataAdapter.Fill(dt)

                    If dt.Rows.Count > 0 Then
                        MessageBox.Show("Código producto de proveedor ya existe para " + dt.Rows(0).Item("codpro"), "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Exit Sub
                    End If

                    sql = "select * from re_provpro where codpro = '" & txtCodigo.Text & "'"
                    sql = sql & " and codemp = " & Globales.empresa.ToString
                    sql = sql & " and rutprv = " & txtNumRut.Text
                    dt.Clear()
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    dataAdapter = New OracleDataAdapter(comando)
                    dataAdapter.Fill(dt)
                    If dt.Rows.Count > 0 Then
                        bolestaProducto = True
                    Else
                        bolestaProducto = False
                    End If

                    sql = "select count(*) contar from re_provpro where codpro = '" & txtCodigo.Text & "'"
                    sql = sql & "   and codemp = " & Globales.empresa.ToString
                    dt.Clear()
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    dataAdapter = New OracleDataAdapter(comando)
                    dataAdapter.Fill(dt)

                    If dt.Rows.Count > 0 Then
                        cuenta = Val(dt.Rows(0).Item("contar"))
                    End If

                    If bolestaProducto Then
                        sql = "update re_provpro set coprpv = '" & txtCodProveedor.Text & "',"
                        sql = sql & "  deprpv = '" & txtDescProveedor.Text & "',"
                        sql = sql & "  liprpv = '" & IIf(Trim(txtLineaProveedor.Text) = "", " ", txtLineaProveedor.Text) & "',"
                        sql = sql & "  costos = " & txtCosto.Text
                        sql = sql & " ,unicom = " & cmbUniCom.SelectedValue.ToString
                        sql = sql & " where rutprv = " & txtNumRut.Text
                        sql = sql & " and codpro = '" & txtCodigo.Text & "'"
                        sql = sql & " and codemp = " & Globales.empresa.ToString
                        comando = New OracleCommand(sql, conn, transaccion)
                        comando.ExecuteNonQuery()
                    Else
                        If cuenta > 0 Then
                            sql = "insert into re_provpro(rutprv,codpro,coprpv,deprpv,unicom, "
                            sql = sql & "liprpv,costos,desc01,desc02,desc03,desc04,desc05,codemp) "
                            sql = sql & "values(" & txtNumRut.Text & ",'" & txtCodigo.Text & "','"
                            sql = sql & txtCodProveedor.Text & "','" & txtDescProveedor.Text & "',"
                            sql = sql & cmbUniCom.SelectedValue.ToString
                            sql = sql & ",'" & IIf(Trim(txtLineaProveedor.Text) = "", " ", txtLineaProveedor.Text) & "'," & txtCosto.Text & ",0,0,0,0,0," & Globales.empresa.ToString & ")"
                        Else
                            ' Primera relacion proveedor-producto. Lo dejará como prov.principal.
                            sql = "insert into re_provpro(rutprv,codpro,coprpv,deprpv,unicom, "
                            sql = sql & "liprpv,costos,desc01,desc02,desc03,desc04,desc05,priori,codemp) "
                            sql = sql & "values(" & txtNumRut.Text & ",'" & txtCodigo.Text & "','"
                            sql = sql & txtCodProveedor.Text & "','" & txtDescProveedor.Text & "',"
                            sql = sql & cmbUniCom.SelectedValue.ToString & ","
                            sql = sql & "'" & IIf(Trim(txtLineaProveedor.Text) = "", " ", txtLineaProveedor.Text) & "'," & txtCosto.Text & ",0,0,0,0,0,1," & Globales.empresa.ToString & " )"
                        End If
                        comando = New OracleCommand(sql, conn, transaccion)
                        comando.ExecuteNonQuery()
                    End If
                    ' Cambia Proveedor Principal Si Cuenta con el Derecho
                    If Globales.detDerUsu(Globales.user, 57) = True And cmbProvPri.SelectedValue <> Nothing Then
                        sql = "update re_provpro set priori = 0 where codpro = '" & txtCodigo.Text & "'"
                        sql = sql & "   and codemp = " & Globales.empresa.ToString
                        comando = New OracleCommand(sql, conn, transaccion)
                        comando.ExecuteNonQuery()

                        sql = "update re_provpro set priori = 1 where codpro = '" & txtCodigo.Text & "' "
                        sql = sql & " and rutprv = " & cmbProvPri.SelectedValue.ToString  'txtnumrut
                        sql = sql & " and codemp = " & Globales.empresa.ToString
                        comando = New OracleCommand(sql, conn, transaccion)
                        comando.ExecuteNonQuery()
                    End If

                    transaccion.Commit()

                    btn_Limpiar.PerformClick()

                    txtCodigo.Focus()

                    MessageBox.Show("Datos Grabados con Éxito", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information)

                Catch ex As Exception
                    If Not transaccion.Equals(Nothing) Then
                        transaccion.Rollback()
                    End If
                    PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                    MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Finally
                    conn.Close()
                End Try
            End Using
        End Using
    End Sub

    Private Sub txtNumRut_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtNumRut.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            PuntoControl.RegistroUso("114")
            If Trim(txtNumRut.Text) <> "" Then
                datosRelacion()
            Else
                txtRSocial.Focus()
                'MessageBox.Show("Debe ingresar Rut del Proveedor para consulta.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub frmRelProvPro_Shown(sender As System.Object, e As System.EventArgs) Handles MyBase.Shown
        Cursor.Current = Cursors.Arrow
    End Sub

End Class