﻿Imports System.Data.OracleClient

Public Class frmCargaMaestra
    Private Sub btnCargar_Click(sender As Object, e As EventArgs) Handles btnCargar.Click
        Dim dialogo = New OpenFileDialog
        dialogo.Filter = "Excel|*.xls"
        PuntoControl.RegistroUso("138")
        Dim ruta As DialogResult = dialogo.ShowDialog()
        If ruta = DialogResult.OK Then
            If dialogo.FileName <> "" Then
                Globales.Importar(dgvProds, dialogo.FileName)
            Else
                MsgBox("Ingrese nombre de archivo")
            End If
        End If
    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Dim datos As New Conexion
        Dim sql As String = ""
        Dim codRota As String
        Dim codMar As String
        Dim estpro As String
        Dim subgrupo As String
        Dim proyecto As String
        Dim valorCM As String
        Dim tablita As New DataTable

        datos.open_dimerc()
        Using transac = datos.sqlConn.BeginTransaction
            Try
                PuntoControl.RegistroUso("139")
                Using command = New OracleCommand()
                    command.Transaction = transac
                    command.Connection = datos.sqlConn
                    Dim datafiller As OracleDataAdapter = New OracleDataAdapter(command)

                    For Each fila As DataGridViewRow In dgvProds.Rows
                        codRota = ""
                        codMar = ""
                        estpro = ""
                        subgrupo = ""
                        proyecto = ""
                        valorCM = ""
                        If fila.Cells(0).Value.ToString.Length > 0 Then

                            If fila.Cells(1).Value.ToString.Length > 0 Then 'estpro
                                sql = "SELECT CODVAL FROM DE_DOMINIO
                                        WHERE CODDOM = 20
                                        AND DESVAL = '" & fila.Cells(1).Value & "'"
                                command.CommandText = sql
                                tablita = New DataTable
                                datafiller.Fill(tablita)
                                If tablita.Rows.Count > 0 Then
                                    estpro = tablita.Rows(0)(0).ToString()
                                Else
                                    MsgBox("Estado producto inválido, codpro " & fila.Cells(0).Value)
                                    transac.Rollback()
                                    Exit Sub
                                End If
                            End If
                            If fila.Cells(2).Value.ToString.Length > 0 Then 'marca
                                sql = "SELECT CODMAR FROM MA_MARCAPR
                                        WHERE DESMAR = '" & fila.Cells(2).Value & "'"
                                command.CommandText = sql
                                tablita = New DataTable
                                datafiller.Fill(tablita)
                                If tablita.Rows.Count > 0 Then
                                    codMar = tablita.Rows(0)(0).ToString()
                                Else
                                    MsgBox("Marca producto inválida, codpro " & fila.Cells(0).Value)
                                    transac.Rollback()
                                    Exit Sub
                                End If
                            End If


                            sql = "UPDATE MA_PRODUCT SET
                               ESTPRO = " & IIf(estpro.Length = 0, "ESTPRO", estpro) & ",
                               CODMAR = " & IIf(codMar.Length = 0, "CODMAR", codMar) & ",
                               DESGRU = " & IIf(fila.Cells(5).Value.ToString.Length = 0, "DESGRU", "'" & fila.Cells(5).Value & "'") & ",
                               PEDSTO = " & IIf(fila.Cells(11).Value.ToString.Length = 0, "PEDSTO", "'" & fila.Cells(11).Value & "'") & ",
                               COSTO = " & IIf(fila.Cells(12).Value.ToString.Length = 0, "COSTO", fila.Cells(12).Value) & ",
                               XDELTA = " & IIf(fila.Cells(15).Value.ToString.Length = 0, "XDELTA", fila.Cells(15).Value) & "
                               WHERE CODPRO = '" & fila.Cells(0).Value & "'"
                            command.CommandText = sql
                            command.ExecuteNonQuery()

                            If fila.Cells(3).Value.ToString.Length > 0 Then 'rotacion
                                sql = "DELETE FROM RE_CLASPRO
                                       WHERE CODEMP = 3
                                       AND CODPRO = '" & fila.Cells(0).Value & "'"
                                command.CommandText = sql
                                command.ExecuteNonQuery()
                                sql = "SELECT CODVAL FROM DE_DOMINIO
                                       WHERE CODDOM = 201
                                       AND VALNUM = 1
                                       AND TRIM(DESVAL) = '" & fila.Cells(3).Value & "'"
                                command.CommandText = sql
                                tablita = New DataTable()
                                datafiller.Fill(tablita)
                                If tablita.Rows.Count > 0 Then
                                    codRota = tablita.Rows(0)(0).ToString()
                                Else
                                    MsgBox("Rotación inválida, codpro " & fila.Cells(0).Value)
                                    transac.Rollback()
                                    Exit Sub
                                End If
                                sql = "INSERT INTO RE_CLASPRO(CODPRO, CODVAL, CODEMP)
                                       VALUES('" & fila.Cells(0).Value & "', " & codRota & ",3)"
                                command.CommandText = sql
                                command.ExecuteNonQuery()
                            End If

                            If fila.Cells(4).Value.ToString.Length > 0 Then 'subgrupo
                                sql = "SELECT ID FROM MA_NEGOCIO_SUBGRUPO
                                        WHERE DESCRIPCION = '" & fila.Cells(4).Value & "'"
                                command.CommandText = sql
                                tablita = New DataTable
                                datafiller.Fill(tablita)
                                If tablita.Rows.Count > 0 Then
                                    subgrupo = tablita.Rows(0)(0).ToString()
                                Else
                                    MsgBox("Subgrupo inválido, codpro " & fila.Cells(0).Value)
                                    transac.Rollback()
                                    Exit Sub
                                End If
                            End If

                            If fila.Cells(7).Value.ToString.Length > 0 Then 'proyecto
                                sql = "SELECT ID FROM MA_NEGOCIO_PRICING
                                        WHERE DESCRIPCION = '" & fila.Cells(7).Value & "'"
                                command.CommandText = sql
                                tablita = New DataTable
                                datafiller.Fill(tablita)
                                If tablita.Rows.Count > 0 Then
                                    proyecto = tablita.Rows(0)(0).ToString()
                                Else
                                    MsgBox("Proyecto inválido, codpro " & fila.Cells(0).Value)
                                    transac.Rollback()
                                    Exit Sub
                                End If
                            End If

                            If subgrupo.Length > 0 Or proyecto.Length > 0 Or fila.Cells(14).Value.ToString.Length > 0 Or fila.Cells(16).Value.ToString.Length > 0 Then
                                sql = "SELECT 1 FROM MA_PRODUCT_NEGOCIO WHERE CODPRO = '" & fila.Cells(0).Value() & "'"
                                command.CommandText = sql
                                tablita = New DataTable
                                datafiller.Fill(tablita)
                                If tablita.Rows.Count > 0 Then
                                    sql = "UPDATE MA_PRODUCT_NEGOCIO SET
                                           ID_SUBGRUPO = " & IIf(subgrupo.Length = 0, "ID_SUBGRUPO", subgrupo) & ",
                                           ID_NEGOCIO = " & IIf(proyecto.Length = 0, "ID_NEGOCIO", proyecto) & ",
                                           PORC_APORTE = " & IIf(fila.Cells(14).Value.ToString.Length = 0, "PORC_APORTE", Replace(fila.Cells(14).Value.ToString, ",", ".")) & ",
                                           DESCRIP_ALTER = " & IIf(fila.Cells(16).Value.ToString.Length = 0, "DESCRIP_ALTER", "'" & fila.Cells(16).Value & "'") & "
                                           WHERE CODPRO = '" & fila.Cells(0).Value & "'"

                                Else
                                    sql = "INSERT INTO MA_PRODUCT_NEGOCIO(CODPRO, DESCRIP_ALTER, ID_SUBGRUPO, ID_NEGOCIO, PORC_APORTE)
                                           VALUES('" & fila.Cells(0).Value & "',
                                                  '" & IIf(fila.Cells(16).Value.ToString.Length = 0, "", fila.Cells(16).Value) & "',
                                           " & IIf(subgrupo.Length = 0, "NULL", subgrupo) & "," & IIf(proyecto.Length = 0, "NULL", proyecto) & "," &
                                           IIf(fila.Cells(14).Value.ToString.Length = 0, "NULL", Replace(fila.Cells(14).Value.ToString, ",", ".")) & ")"
                                End If
                                command.CommandText = sql
                                command.ExecuteNonQuery()
                            End If

                            If fila.Cells(6).Value.ToString.Length > 0 Then 'proveedor
                                sql = "UPDATE RE_PROVPRO SET PRIORI = 0
                               WHERE CODPRO = '" & fila.Cells(0).Value & "'"
                                command.CommandText = sql
                                command.ExecuteNonQuery()

                                sql = "UPDATE RE_PROVPRO SET PRIORI = 1
                               WHERE CODPRO = '" & fila.Cells(0).Value & "'
                               AND RUTPRV = " & fila.Cells(6).Value
                                command.CommandText = sql
                                command.ExecuteNonQuery()
                            End If

                            If fila.Cells(8).Value.ToString.Length > 0 Then 'cm
                                sql = "DELETE FROM RE_PRODUCTO_CM
                               WHERE CODEMP = 3
                               AND CODPRO = '" & fila.Cells(0).Value & "'"
                                command.CommandText = sql
                                command.ExecuteNonQuery()
                                Dim tipo As String
                                If fila.Cells(8).Value.ToString.Equals("PRICING") Then
                                    tipo = "PRI"
                                    valorCM = 1
                                Else
                                    tipo = "NEG"
                                    valorCM = 2
                                End If
                                sql = "INSERT INTO RE_PRODUCTO_CM(CODEMP,CODPRO,TIPO,VALOR)
                               VALUES (3, '" & fila.Cells(0).Value & "', '" & tipo & "'," & valorCM & ")"
                                command.CommandText = sql
                                command.ExecuteNonQuery()
                            End If

                            If fila.Cells(9).Value.ToString.Length > 0 Then 'mp_dmc
                                sql = "DELETE FROM QV_MARCAPROPIA_DIMERC
                               WHERE CODPRO = '" & fila.Cells(0).Value & "'"
                                command.CommandText = sql
                                command.ExecuteNonQuery()

                                If fila.Cells(9).Value = "S" Then
                                    sql = "INSERT INTO qv_marcapropia_dimerc ( codpro, coduni)
                               VALUES('" & fila.Cells(0).Value & "',getcodigouni('" & fila.Cells(0).Value & "') )"
                                    command.CommandText = sql
                                    command.ExecuteNonQuery()
                                End If
                            End If
                            If fila.Cells(10).Value.ToString.Length > 0 Then 'mp_inventario
                                sql = "DELETE FROM qv_marcapropia_inventario
                               WHERE CODPRO = '" & fila.Cells(0).Value & "'"
                                command.CommandText = sql
                                command.ExecuteNonQuery()
                                If fila.Cells(10).Value = "S" Then
                                    sql = " insert into qv_marcapropia_inventario(CODPRO,CODUNI,MARCA_PROPIA)
                                            values('" & fila.Cells(0).Value & "', GETCODIGOUNI('" & fila.Cells(0).Value & "'),'" & fila.Cells(10).Value & "') "
                                    command.CommandText = sql
                                    command.ExecuteNonQuery()
                                End If
                            End If

                            'delta
                            sql = "UPDATE MA_COSTO_COMERCIAL
                               SET VALOR = " & IIf(fila.Cells(13).Value.ToString.Length = 0, "VALOR", Replace(fila.Cells(13).Value.ToString(), ",", ".")) & "
                               WHERE CODPRO = '" & fila.Cells(0).Value & "'
                               AND CODEMP = 3"
                            command.CommandText = sql
                            command.ExecuteNonQuery()
                        End If
                    Next
                    transac.Commit()
                End Using
                MsgBox("Datos cargados exitosamente")
            Catch ex As Exception
                MsgBox(ex.ToString)
                transac.Rollback()
            End Try

        End Using
    End Sub

    Private Sub frmCargaMaestra_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        Dim datos As New Conexion
        datos.open_dimerc()
        Dim tabla = datos.sqlSelect("SELECT DESVAL, CODVAL
                                     FROM DE_DOMINIO
                                     WHERE CODDOM = 20")
        cmbEstpro.DataSource = tabla
        cmbEstpro.ValueMember = "CODVAL"
        cmbEstpro.DisplayMember = "DESVAL"
        cmbEstpro.SelectedIndex = -1
        cmbEstpro.Refresh()
    End Sub

    Private Sub btnLeer_Click(sender As Object, e As EventArgs) Handles btnLeer.Click
        If cmbEstpro.SelectedIndex >= 0 Then
            Dim datos As New Conexion
            datos.open_dimerc()
            Dim sql As String
            PuntoControl.RegistroUso("140")
            sql = "select a.codpro, sap_get_dominio(20,estpro) estpro,
                    b.desmar marca, sap_get_rotacion(3,a.codpro) rotacion,
                    d.DESCRIPCION subgrupo, a.DESGRU grupo, e.rutprv proveedor,
                    f.DESCRIPCION proyecto, decode(g.valor,1,'PRICING','NEGOCIOS') CM,
                    DECODE(NVL(h.MPROPIA,0),1,'S','N') MP_DIMERC,GET_MPROPIA_INVENTARIO_DIMERC(a.codpro) mp_inventario,
                    A.PEDSTO,A.COSTO, i.VALOR delta, c.PORC_APORTE,  a.xdelta, c.DESCRIP_ALTER
                    from ma_product a, ma_marcapr b, ma_product_negocio c,
                    ma_negocio_subgrupo d, re_provpro e, ma_negocio_pricing f, re_producto_cm g,
                    dm_ventas.qv_marca_propia h, ma_costo_comercial i
                    where a.codmar = b.codmar(+)
                    and a.codpro = c.codpro(+)
                    and c.ID_SUBGRUPO = d.id(+)
                    and a.codpro = e.codpro(+)
                    and e.priori(+) = 1
                    and c.ID_NEGOCIO = f.id(+)
                    and g.codemp(+) = 3
                    and a.codpro = g.codpro(+)
                    and h.codemp(+) = 3
                    and a.codpro = h.coddim(+)
                    and i.codemp(+) = 3
                    and a.codpro = i.codpro(+)
                    AND a.estpro = " & cmbEstpro.SelectedValue
            Dim tabla = datos.sqlSelect(sql)
            dgvLeer.DataSource = tabla
        Else
            MsgBox("Seleccione estado")
        End If
    End Sub

    Private Sub btnExportar_Click(sender As Object, e As EventArgs) Handles btnExportar.Click
        If dgvLeer.Rows.Count > 0 Then
            Globales.DataTableToExcel(CType(dgvLeer.DataSource, DataTable).Copy(), dgvProds) 'exporta(dialogo.FileName, dgvProds)
            PuntoControl.RegistroUso("141")
            'Dim dialogo = New SaveFileDialog
            'dialogo.Filter = "Excel|*.xls"
            'Dim ruta As DialogResult = dialogo.ShowDialog()
            'If ruta = DialogResult.OK Then
            '    If dialogo.FileName <> "" Then
            '        Globales.DataTableToExcel(CType(dgvProds.DataSource, DataTable).Copy(), dgvProds) 'exporta(dialogo.FileName, dgvProds)
            '    Else
            '        MsgBox("Ingrese nombre de archivo")
            '    End If
            'End If
        Else
            MsgBox("Cargar Productos antes de exportar")
        End If
    End Sub

    Private Sub frmCargaMaestra_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Location = New Point(100, 50)
        PuntoControl.RegistroUso("137")
    End Sub
End Class