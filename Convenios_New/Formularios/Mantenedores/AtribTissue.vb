﻿Public Class AtribTissue

    Private Sub TextBox1_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCodpro.KeyPress
        If Convert.ToInt32(e.KeyChar) = Convert.ToInt32(Keys.Enter) And txtCodpro.Text.Length > 0 Then
            Dim datos As New Conexion()
            PuntoControl.RegistroUso("122")
            Dim sql As String = "SELECT CODPRO, ALTURA_MM, LONGITUD_MM, GOFRADO, EAN13, PESO_GR, PRESENTACION, HOJAS," & vbCrLf &
                                "PAQUETE_EMB, PESO_GR_EMB, BULTO_PAQ_EMB,PALETIZADO_EMB,CODIGO_DUN_EMB" & vbCrLf &
                                "FROM MA_PRODUCTO_ATRIB" & vbCrLf &
                                "WHERE CODPRO = '" & txtCodpro.Text & "'"
            datos.open_dimerc()
            Dim tabla = datos.sqlSelect(sql)
            datos.close()

            If tabla.Rows.Count > 0 Then
                txtAltura.Text = tabla.Rows(0)("ALTURA_MM").ToString()
                txtLongitud.Text = tabla.Rows(0)("LONGITUD_MM").ToString()
                If tabla.Rows(0)("GOFRADO").ToString().Equals("1") Then
                    rbGofradoSi.Checked = True
                Else
                    rbGofradoNo.Checked = True
                End If
                txtEAN13.Text = tabla.Rows(0)("EAN13").ToString()
                txtPeso.Text = tabla.Rows(0)("PESO_GR").ToString()
                txtPresentacion.Text = tabla.Rows(0)("PRESENTACION").ToString()
                txtHojas.Text = tabla.Rows(0)("HOJAS").ToString()

                txtPaquete.Text = tabla.Rows(0)("PAQUETE_EMB").ToString()
                txtBulto.Text = tabla.Rows(0)("BULTO_PAQ_EMB").ToString()
                txtPaletizado.Text = tabla.Rows(0)("PALETIZADO_EMB").ToString()
                txtDUN.Text = tabla.Rows(0)("CODIGO_DUN_EMB").ToString()

                sql = "SELECT CODPRO, CODPRO_ALT, TIPO" & vbCrLf &
                                "FROM RE_PRODUCTO_TISSUE" & vbCrLf &
                                "WHERE CODPRO = '" & txtCodpro.Text & "'"
                datos.open_dimerc()
                tabla = datos.sqlSelect(sql)
                datos.close()

                For Each fila In tabla.Rows
                    If fila("TIPO").ToString().Equals("1") Then
                        lbAlternativas.Items.Add(fila("CODPRO").ToString())
                    Else
                        lbDispensadores.Items.Add(fila("CODPRO").ToString())
                    End If
                Next

                grpControl.Enabled = True
                grpEmbalaje.Enabled = True
                grpAlterDisp.Enabled = True
                txtCodpro.Enabled = False
            Else
                grpControl.Enabled = True
                txtCodpro.Enabled = False
            End If
            btnGuardar.Enabled = True

        End If
    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        If txtAltura.Text = "" Or txtLongitud.Text = "" Or txtEAN13.Text = "" Or txtPeso.Text = "" Or (Not rbGofradoSi.Checked And Not rbGofradoNo.Checked) Then
            MsgBox("Falta información, favor revisar")
        Else
            Dim datos As New Conexion()
            PuntoControl.RegistroUso("123")
            Dim sql As String
            Dim gofrado As Integer
            If rbGofradoSi.Checked Then
                gofrado = 1
            Else
                gofrado = 0
            End If
            datos.open_dimerc()
            sql = "DELETE FROM MA_PRODUCTO_ATRIB WHERE CODPRO = '" & txtCodpro.Text & "'"
            datos.ejecutar(sql)
            sql = "DELETE FROM RE_PRODUCTO_TISSUE WHERE CODPRO = '" & txtCodpro.Text & "'"
            datos.ejecutar(sql)
            sql = "INSERT INTO MA_PRODUCTO_ATRIB(CODPRO, ALTURA_MM, LONGITUD_MM, GOFRADO, EAN13, PESO_GR,PRESENTACION, HOJAS," & vbCrLf &
                                "PAQUETE_EMB, BULTO_PAQ_EMB,PALETIZADO_EMB,CODIGO_DUN_EMB)" & vbCrLf &
                 "VALUES ('" & txtCodpro.Text & "'," & txtAltura.Text & "," & txtLongitud.Text & "," & gofrado & ",'" & txtEAN13.Text & "'," & txtPeso.Text & "," & vbCrLf &
                 "'" & txtPresentacion.Text & "'," & txtHojas.Text & ",'" & txtPaquete.Text & "'," & vbCrLf &
                 txtBulto.Text & ",'" & txtPaletizado.Text & "','" & txtDUN.Text & "')"
            datos.ejecutar(sql)

            For Each alternativa In lbAlternativas.Items
                sql = "INSERT INTO RE_PRODUCTO_TISSUE(CODPRO,CODPRO_ALT,TIPO) VALUES" & vbCrLf &
                      "('" & txtCodpro.Text & "','" & alternativa.ToString() & "',1)"
                datos.ejecutar(sql)
            Next
            For Each dispensador In lbDispensadores.Items
                sql = "INSERT INTO RE_PRODUCTO_TISSUE(CODPRO,CODPRO_ALT,TIPO) VALUES" & vbCrLf &
                      "('" & txtCodpro.Text & "','" & dispensador.ToString() & "',2)"
                datos.ejecutar(sql)
            Next
            datos.close()
            MsgBox("Datos ingresados exitosamente")
            limpiar()
        End If
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        limpiar()
        PuntoControl.RegistroUso("124")
    End Sub

    Private Sub limpiar()
        txtCodpro.Text = ""
        txtCodpro.Enabled = True
        txtAltura.Text = ""
        txtLongitud.Text = ""
        rbGofradoNo.Checked = False
        rbGofradoSi.Checked = False
        txtEAN13.Text = ""
        txtPeso.Text = ""
        txtPresentacion.Text = ""
        txtHojas.Text = ""
        txtPaquete.Text = ""
        txtBulto.Text = ""
        txtPaletizado.Text = ""
        txtDUN.Text = ""
        lbAlternativas.Items.Clear()
        lbDispensadores.Items.Clear()
        grpControl.Enabled = False
        grpAlterDisp.Enabled = False
        grpEmbalaje.Enabled = False

        btnGuardar.Enabled = False

    End Sub

    Private Sub btnAgregaAlternativa_Click(sender As Object, e As EventArgs) Handles btnAgregaAlternativa.Click
        lbAlternativas.Items.Add(txtAlternativa.Text)
        txtAlternativa.Text = ""
        PuntoControl.RegistroUso("125")
    End Sub

    Private Sub lbAlternativas_DoubleClick(sender As Object, e As EventArgs) Handles lbAlternativas.DoubleClick
        If sender.selectedItem IsNot Nothing Then
            sender.Items.Remove(sender.selectedItem)
        End If
    End Sub

    Private Sub btnAgregaDispensador_Click(sender As Object, e As EventArgs) Handles btnAgregaDispensador.Click
        lbDispensadores.Items.Add(txtDispensador.Text)
        txtDispensador.Text = ""
        PuntoControl.RegistroUso("126")
    End Sub

    Private Sub lbDispensadores_DoubleClick(sender As Object, e As EventArgs) Handles lbDispensadores.DoubleClick
        If sender.selectedItem IsNot Nothing Then
            sender.Items.Remove(sender.selectedItem)
        End If
    End Sub

    Private Sub btnExpAtr_Click(sender As Object, e As EventArgs) Handles btnExpAtr.Click
        Dim datos As New Conexion
        PuntoControl.RegistroUso("127")
        datos.open_dimerc()
        Dim tabla = datos.sqlSelect("SELECT CODPRO,ALTURA_MM,LONGITUD_MM,GOFRADO,EAN13,PESO_GR,PRESENTACION,HOJAS," & vbCrLf &
                                    "PAQUETE_EMB, PESO_GR_EMB,BULTO_PAQ_EMB,PALETIZADO_EMB,CODIGO_DUN_EMB" & vbCrLf &
                                    "FROM MA_PRODUCTO_ATRIB")
        datos.close()
        Dim dgv = New DataGridView
        dgv.DataSource = tabla
        Globales.DataTableToExcel(tabla, dgv)
    End Sub

    Private Sub btnExpRel_Click(sender As Object, e As EventArgs) Handles btnExpRel.Click
        Dim datos As New Conexion
        PuntoControl.RegistroUso("128")
        datos.open_dimerc()
        Dim tabla = datos.sqlSelect("SELECT CODPRO, CODPRO_ALT, TIPO" & vbCrLf &
                                    "FROM RE_PRODUCTO_TISSUE")
        datos.close()
        Dim dgv = New DataGridView
        dgv.DataSource = tabla
        Globales.DataTableToExcel(tabla, dgv)
    End Sub

    Private Sub btnImpAtr_Click(sender As Object, e As EventArgs) Handles btnImpAtr.Click
        Dim dialogo = New OpenFileDialog
        PuntoControl.RegistroUso("129")
        Dim tabla As New DataGridView
        Dim datos As New Conexion
        dialogo.Filter = "Archivos Excel | (*.xlsx)"
        If dialogo.ShowDialog = DialogResult.OK Then
            Globales.Importar(tabla, dialogo.FileName)
            datos.open_dimerc()
            Dim sql = "DELETE FROM MA_PRODUCTO_ATRIB"
            datos.ejecutar(sql)
            For Each fila In tabla.Rows
                sql = "INSERT INTO MA_PRODUCTO_ATRIB(CODPRO, ALTURA_MM, LONGITUD_MM, GOFRADO, EAN13, PESO_GR,PRESENTACION, HOJAS," & vbCrLf &
                                    "PAQUETE_EMB, BULTO_PAQ_EMB,PALETIZADO_EMB,CODIGO_DUN_EMB)" & vbCrLf &
                     "VALUES ('" & fila("CODPRO").ToString & "'," & fila("ALTURA_MM").ToString & "," & fila("LONGITUD_MM").ToString & "," &
                     fila("GOFRADO").ToString & ",'" & fila("EAN13").ToString & "'," & fila("PESO_GR").ToString & "," & vbCrLf &
                     "'" & fila("PRESENTACION").ToString & "'," & fila("HOJAS").ToString & ",'" & fila("PAQUETE_EMB").ToString & "'," & vbCrLf &
                     fila("BULTO_PAQ_EMB").ToString & ",'" & fila("PALETIZADO_EMB").ToString & "','" & fila("CODIGO_DUN_EMB").ToString & "')"
                datos.ejecutar(sql)
            Next
            datos.close()
        End If
    End Sub

    Private Sub btnImpRel_Click(sender As Object, e As EventArgs) Handles btnImpRel.Click
        Dim dialogo = New OpenFileDialog
        PuntoControl.RegistroUso("130")
        Dim tabla As New DataGridView
        Dim datos As New Conexion
        dialogo.Filter = "Archivos Excel | (*.xlsx)"
        If dialogo.ShowDialog = DialogResult.OK Then
            Globales.Importar(tabla, dialogo.FileName)
            datos.open_dimerc()
            Dim sql = "DELETE FROM RE_PRODUCTO_TISSUE"
            datos.ejecutar(sql)
            For Each fila In tabla.Rows
                sql = "INSERT INTO RE_PRODUCTO_TISSUE(CODPRO, CODPRO_ALT, TIPO)" & vbCrLf &
                     "VALUES ('" & fila("CODPRO").ToString & "','" & fila("CODPRO_ALT").ToString & "'," & fila("TIPO").ToString & "')"
                datos.ejecutar(sql)
            Next
            datos.close()
        End If
    End Sub

    Private Sub AtribTissue_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Location = New Point(0, 0)
        PuntoControl.RegistroUso("121")
    End Sub
End Class