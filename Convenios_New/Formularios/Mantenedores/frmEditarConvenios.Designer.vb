﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmEditarConvenios
    Inherits MetroFramework.Forms.MetroForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEditarConvenios))
        Me.btn_Salir = New System.Windows.Forms.Button()
        Me.grbRut = New System.Windows.Forms.GroupBox()
        Me.btnTipoConvenio = New System.Windows.Forms.Button()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.cmbTipoConvenio = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtRelCom = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtRazons = New System.Windows.Forms.TextBox()
        Me.txtRutcli = New System.Windows.Forms.TextBox()
        Me.grbAgregar = New System.Windows.Forms.GroupBox()
        Me.rdbDefinitivo = New System.Windows.Forms.RadioButton()
        Me.rdbTemporal = New System.Windows.Forms.RadioButton()
        Me.dgvInput = New System.Windows.Forms.DataGridView()
        Me.Label85 = New System.Windows.Forms.Label()
        Me.Label82 = New System.Windows.Forms.Label()
        Me.txtcoscom = New System.Windows.Forms.TextBox()
        Me.txtLinea = New System.Windows.Forms.TextBox()
        Me.txtCodlinAgrega = New System.Windows.Forms.TextBox()
        Me.dtpFecvenAgrega = New System.Windows.Forms.DateTimePicker()
        Me.btn_InputAgrega = New System.Windows.Forms.Button()
        Me.btn_AgregaConvenio = New System.Windows.Forms.Button()
        Me.dgvProductosaAgregar = New System.Windows.Forms.DataGridView()
        Me.btn_QuitaAgrega = New System.Windows.Forms.Button()
        Me.btn_AgregaProducto = New System.Windows.Forms.Button()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.txtDescripcion = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.txtCosto = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txtPrecio = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.txtCodpro = New System.Windows.Forms.TextBox()
        Me.rdbCliente = New System.Windows.Forms.RadioButton()
        Me.grbAcciones = New System.Windows.Forms.GroupBox()
        Me.tabAcciones = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.grbModificar = New System.Windows.Forms.GroupBox()
        Me.txtSkuModifica = New System.Windows.Forms.TextBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.btn_CargaInputModifica = New System.Windows.Forms.Button()
        Me.btn_Cambio = New System.Windows.Forms.Button()
        Me.btn_ModificarConvenio = New System.Windows.Forms.Button()
        Me.dgvProductosModificar = New System.Windows.Forms.DataGridView()
        Me.txtDescripcionModif = New System.Windows.Forms.TextBox()
        Me.txtCodproModif = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtPrecioModif = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtCostoModif = New System.Windows.Forms.TextBox()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.txtSkuElimina = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.btn_InputElimina = New System.Windows.Forms.Button()
        Me.btn_EliminardeConvenio = New System.Windows.Forms.Button()
        Me.dgvProductoEliminar = New System.Windows.Forms.DataGridView()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.chkMarcaTodoConvenio = New System.Windows.Forms.CheckBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.dgvLineasReajuste = New System.Windows.Forms.DataGridView()
        Me.Label84 = New System.Windows.Forms.Label()
        Me.dtpFecVenConvenio = New System.Windows.Forms.DateTimePicker()
        Me.dtpFechaLineaReajuste = New System.Windows.Forms.DateTimePicker()
        Me.btn_CambiaFechas = New System.Windows.Forms.Button()
        Me.Label73 = New System.Windows.Forms.Label()
        Me.dgvCambiaFechas = New System.Windows.Forms.DataGridView()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.btnProcesar = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.dtpFecha = New System.Windows.Forms.DateTimePicker()
        Me.rbtAgregar = New System.Windows.Forms.RadioButton()
        Me.rbtReemplazar = New System.Windows.Forms.RadioButton()
        Me.rbtQuitar = New System.Windows.Forms.RadioButton()
        Me.btnxlsxReemplazo = New System.Windows.Forms.Button()
        Me.dgvReemplazo = New System.Windows.Forms.DataGridView()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.txtConvenioReem = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtLineaReem = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.txtDescripReem = New System.Windows.Forms.TextBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.txtSkuReem = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cbxReversaReem = New System.Windows.Forms.CheckBox()
        Me.btnxlsxOriginal = New System.Windows.Forms.Button()
        Me.dgvOriginal = New System.Windows.Forms.DataGridView()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtLineaOri = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtDescripOri = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.txtConvenioTotales = New System.Windows.Forms.TextBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.txtSkuOri = New System.Windows.Forms.TextBox()
        Me.TabPage6 = New System.Windows.Forms.TabPage()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.dgvConReemM = New System.Windows.Forms.DataGridView()
        Me.dgvConOriM = New System.Windows.Forms.DataGridView()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.txtDescripSkuM = New System.Windows.Forms.TextBox()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.txtMargenM = New System.Windows.Forms.TextBox()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.txtCostoM = New System.Windows.Forms.TextBox()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.txtPrecioM = New System.Windows.Forms.TextBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.txtFechaFinM = New System.Windows.Forms.TextBox()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.txtFechaIniM = New System.Windows.Forms.TextBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.txtRutcliM = New System.Windows.Forms.TextBox()
        Me.btnProcesarManual = New System.Windows.Forms.Button()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.dtpFechaManual = New System.Windows.Forms.DateTimePicker()
        Me.rbtAgregarM = New System.Windows.Forms.RadioButton()
        Me.rbtReemplazarM = New System.Windows.Forms.RadioButton()
        Me.rbtQuitarM = New System.Windows.Forms.RadioButton()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.txtLineaReemM = New System.Windows.Forms.TextBox()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.txtdescripReemM = New System.Windows.Forms.TextBox()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.txtSkuRemM = New System.Windows.Forms.TextBox()
        Me.cbxReversaM = New System.Windows.Forms.CheckBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.txtLineaM = New System.Windows.Forms.TextBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.txtRsocialM = New System.Windows.Forms.TextBox()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.txtskuOriginalM = New System.Windows.Forms.TextBox()
        Me.rdbTodos = New System.Windows.Forms.RadioButton()
        Me.ProgressBarCargando = New System.Windows.Forms.ProgressBar()
        Me.grbRut.SuspendLayout()
        Me.grbAgregar.SuspendLayout()
        CType(Me.dgvInput, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvProductosaAgregar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grbAcciones.SuspendLayout()
        Me.tabAcciones.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.grbModificar.SuspendLayout()
        CType(Me.dgvProductosModificar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        CType(Me.dgvProductoEliminar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage4.SuspendLayout()
        CType(Me.dgvLineasReajuste, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvCambiaFechas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage5.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvReemplazo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvOriginal, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage6.SuspendLayout()
        CType(Me.dgvConReemM, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvConOriM, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'btn_Salir
        '
        Me.btn_Salir.Image = Global.Convenios_New.My.Resources.Resources.salir
        Me.btn_Salir.Location = New System.Drawing.Point(1031, 58)
        Me.btn_Salir.Name = "btn_Salir"
        Me.btn_Salir.Size = New System.Drawing.Size(50, 43)
        Me.btn_Salir.TabIndex = 74
        Me.btn_Salir.UseVisualStyleBackColor = True
        '
        'grbRut
        '
        Me.grbRut.Controls.Add(Me.btnTipoConvenio)
        Me.grbRut.Controls.Add(Me.Label14)
        Me.grbRut.Controls.Add(Me.cmbTipoConvenio)
        Me.grbRut.Controls.Add(Me.Label1)
        Me.grbRut.Controls.Add(Me.txtRelCom)
        Me.grbRut.Controls.Add(Me.Label2)
        Me.grbRut.Controls.Add(Me.Label3)
        Me.grbRut.Controls.Add(Me.txtRazons)
        Me.grbRut.Controls.Add(Me.txtRutcli)
        Me.grbRut.Location = New System.Drawing.Point(12, 83)
        Me.grbRut.Name = "grbRut"
        Me.grbRut.Size = New System.Drawing.Size(950, 69)
        Me.grbRut.TabIndex = 77
        Me.grbRut.TabStop = False
        '
        'btnTipoConvenio
        '
        Me.btnTipoConvenio.Location = New System.Drawing.Point(334, 40)
        Me.btnTipoConvenio.Name = "btnTipoConvenio"
        Me.btnTipoConvenio.Size = New System.Drawing.Size(100, 23)
        Me.btnTipoConvenio.TabIndex = 73
        Me.btnTipoConvenio.Text = "Cambiar Tipo"
        Me.btnTipoConvenio.UseVisualStyleBackColor = True
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(7, 44)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(89, 14)
        Me.Label14.TabIndex = 72
        Me.Label14.Text = "Tipo Convenio:"
        '
        'cmbTipoConvenio
        '
        Me.cmbTipoConvenio.FormattingEnabled = True
        Me.cmbTipoConvenio.Items.AddRange(New Object() {"Convenio Tissue", "Convenio Mixto"})
        Me.cmbTipoConvenio.Location = New System.Drawing.Point(102, 40)
        Me.cmbTipoConvenio.Name = "cmbTipoConvenio"
        Me.cmbTipoConvenio.Size = New System.Drawing.Size(226, 22)
        Me.cmbTipoConvenio.TabIndex = 71
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(67, 14)
        Me.Label1.TabIndex = 69
        Me.Label1.Text = "Rut Cliente"
        '
        'txtRelCom
        '
        Me.txtRelCom.BackColor = System.Drawing.SystemColors.Info
        Me.txtRelCom.Location = New System.Drawing.Point(804, 12)
        Me.txtRelCom.Name = "txtRelCom"
        Me.txtRelCom.Size = New System.Drawing.Size(113, 22)
        Me.txtRelCom.TabIndex = 65
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(207, 15)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(73, 14)
        Me.Label2.TabIndex = 70
        Me.Label2.Text = "Razón Social"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(714, 18)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(84, 14)
        Me.Label3.TabIndex = 68
        Me.Label3.Text = "Num. Relación"
        '
        'txtRazons
        '
        Me.txtRazons.BackColor = System.Drawing.SystemColors.Window
        Me.txtRazons.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRazons.Location = New System.Drawing.Point(286, 12)
        Me.txtRazons.Name = "txtRazons"
        Me.txtRazons.Size = New System.Drawing.Size(422, 22)
        Me.txtRazons.TabIndex = 67
        '
        'txtRutcli
        '
        Me.txtRutcli.BackColor = System.Drawing.SystemColors.Window
        Me.txtRutcli.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRutcli.Location = New System.Drawing.Point(102, 12)
        Me.txtRutcli.Name = "txtRutcli"
        Me.txtRutcli.Size = New System.Drawing.Size(99, 22)
        Me.txtRutcli.TabIndex = 66
        '
        'grbAgregar
        '
        Me.grbAgregar.Controls.Add(Me.rdbDefinitivo)
        Me.grbAgregar.Controls.Add(Me.rdbTemporal)
        Me.grbAgregar.Controls.Add(Me.dgvInput)
        Me.grbAgregar.Controls.Add(Me.Label85)
        Me.grbAgregar.Controls.Add(Me.Label82)
        Me.grbAgregar.Controls.Add(Me.txtcoscom)
        Me.grbAgregar.Controls.Add(Me.txtLinea)
        Me.grbAgregar.Controls.Add(Me.txtCodlinAgrega)
        Me.grbAgregar.Controls.Add(Me.dtpFecvenAgrega)
        Me.grbAgregar.Controls.Add(Me.btn_InputAgrega)
        Me.grbAgregar.Controls.Add(Me.btn_AgregaConvenio)
        Me.grbAgregar.Controls.Add(Me.dgvProductosaAgregar)
        Me.grbAgregar.Controls.Add(Me.btn_QuitaAgrega)
        Me.grbAgregar.Controls.Add(Me.btn_AgregaProducto)
        Me.grbAgregar.Controls.Add(Me.Label24)
        Me.grbAgregar.Controls.Add(Me.txtDescripcion)
        Me.grbAgregar.Controls.Add(Me.Label8)
        Me.grbAgregar.Controls.Add(Me.Label21)
        Me.grbAgregar.Controls.Add(Me.txtCosto)
        Me.grbAgregar.Controls.Add(Me.Label20)
        Me.grbAgregar.Controls.Add(Me.txtPrecio)
        Me.grbAgregar.Controls.Add(Me.Label23)
        Me.grbAgregar.Controls.Add(Me.txtCodpro)
        Me.grbAgregar.Location = New System.Drawing.Point(4, 6)
        Me.grbAgregar.Name = "grbAgregar"
        Me.grbAgregar.Size = New System.Drawing.Size(1036, 347)
        Me.grbAgregar.TabIndex = 78
        Me.grbAgregar.TabStop = False
        '
        'rdbDefinitivo
        '
        Me.rdbDefinitivo.AutoSize = True
        Me.rdbDefinitivo.Location = New System.Drawing.Point(850, 39)
        Me.rdbDefinitivo.Name = "rdbDefinitivo"
        Me.rdbDefinitivo.Size = New System.Drawing.Size(75, 18)
        Me.rdbDefinitivo.TabIndex = 69
        Me.rdbDefinitivo.Text = "Definitivo"
        Me.rdbDefinitivo.UseVisualStyleBackColor = True
        '
        'rdbTemporal
        '
        Me.rdbTemporal.AutoSize = True
        Me.rdbTemporal.Checked = True
        Me.rdbTemporal.Location = New System.Drawing.Point(850, 18)
        Me.rdbTemporal.Name = "rdbTemporal"
        Me.rdbTemporal.Size = New System.Drawing.Size(76, 18)
        Me.rdbTemporal.TabIndex = 69
        Me.rdbTemporal.TabStop = True
        Me.rdbTemporal.Text = "Temporal"
        Me.rdbTemporal.UseVisualStyleBackColor = True
        '
        'dgvInput
        '
        Me.dgvInput.AllowUserToAddRows = False
        Me.dgvInput.AllowUserToDeleteRows = False
        Me.dgvInput.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvInput.Location = New System.Drawing.Point(511, 303)
        Me.dgvInput.Name = "dgvInput"
        Me.dgvInput.ReadOnly = True
        Me.dgvInput.Size = New System.Drawing.Size(33, 17)
        Me.dgvInput.TabIndex = 68
        Me.dgvInput.Visible = False
        '
        'Label85
        '
        Me.Label85.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label85.Location = New System.Drawing.Point(166, 323)
        Me.Label85.Name = "Label85"
        Me.Label85.Size = New System.Drawing.Size(554, 23)
        Me.Label85.TabIndex = 66
        Me.Label85.Text = "Columnas deben llamarse  CODPRO - PRECIO - COSTO ESPECIAL - FECVEN"
        '
        'Label82
        '
        Me.Label82.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label82.Location = New System.Drawing.Point(166, 301)
        Me.Label82.Name = "Label82"
        Me.Label82.Size = New System.Drawing.Size(458, 12)
        Me.Label82.TabIndex = 67
        Me.Label82.Text = "Archivo debe ser excel con formato xls. o xlsx."
        '
        'txtcoscom
        '
        Me.txtcoscom.Location = New System.Drawing.Point(496, 12)
        Me.txtcoscom.Name = "txtcoscom"
        Me.txtcoscom.Size = New System.Drawing.Size(12, 22)
        Me.txtcoscom.TabIndex = 65
        Me.txtcoscom.Visible = False
        '
        'txtLinea
        '
        Me.txtLinea.Location = New System.Drawing.Point(514, 12)
        Me.txtLinea.Name = "txtLinea"
        Me.txtLinea.Size = New System.Drawing.Size(12, 22)
        Me.txtLinea.TabIndex = 65
        Me.txtLinea.Visible = False
        '
        'txtCodlinAgrega
        '
        Me.txtCodlinAgrega.Location = New System.Drawing.Point(532, 12)
        Me.txtCodlinAgrega.Name = "txtCodlinAgrega"
        Me.txtCodlinAgrega.Size = New System.Drawing.Size(12, 22)
        Me.txtCodlinAgrega.TabIndex = 65
        Me.txtCodlinAgrega.Visible = False
        '
        'dtpFecvenAgrega
        '
        Me.dtpFecvenAgrega.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecvenAgrega.Location = New System.Drawing.Point(736, 35)
        Me.dtpFecvenAgrega.Name = "dtpFecvenAgrega"
        Me.dtpFecvenAgrega.Size = New System.Drawing.Size(107, 22)
        Me.dtpFecvenAgrega.TabIndex = 64
        '
        'btn_InputAgrega
        '
        Me.btn_InputAgrega.Image = Global.Convenios_New.My.Resources.Resources.Excel
        Me.btn_InputAgrega.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_InputAgrega.Location = New System.Drawing.Point(6, 301)
        Me.btn_InputAgrega.Name = "btn_InputAgrega"
        Me.btn_InputAgrega.Size = New System.Drawing.Size(154, 46)
        Me.btn_InputAgrega.TabIndex = 63
        Me.btn_InputAgrega.Text = "Carga Input"
        Me.btn_InputAgrega.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_InputAgrega.UseVisualStyleBackColor = True
        '
        'btn_AgregaConvenio
        '
        Me.btn_AgregaConvenio.Image = Global.Convenios_New.My.Resources.Resources.Tips
        Me.btn_AgregaConvenio.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_AgregaConvenio.Location = New System.Drawing.Point(871, 298)
        Me.btn_AgregaConvenio.Name = "btn_AgregaConvenio"
        Me.btn_AgregaConvenio.Size = New System.Drawing.Size(154, 46)
        Me.btn_AgregaConvenio.TabIndex = 63
        Me.btn_AgregaConvenio.Text = "Agregar Productos"
        Me.btn_AgregaConvenio.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_AgregaConvenio.UseVisualStyleBackColor = True
        '
        'dgvProductosaAgregar
        '
        Me.dgvProductosaAgregar.AllowUserToAddRows = False
        Me.dgvProductosaAgregar.AllowUserToDeleteRows = False
        Me.dgvProductosaAgregar.AllowUserToResizeRows = False
        Me.dgvProductosaAgregar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvProductosaAgregar.Location = New System.Drawing.Point(5, 67)
        Me.dgvProductosaAgregar.Name = "dgvProductosaAgregar"
        Me.dgvProductosaAgregar.ReadOnly = True
        Me.dgvProductosaAgregar.RowHeadersVisible = False
        Me.dgvProductosaAgregar.Size = New System.Drawing.Size(1025, 225)
        Me.dgvProductosaAgregar.TabIndex = 62
        '
        'btn_QuitaAgrega
        '
        Me.btn_QuitaAgrega.Image = Global.Convenios_New.My.Resources.Resources.menos
        Me.btn_QuitaAgrega.Location = New System.Drawing.Point(987, 21)
        Me.btn_QuitaAgrega.Name = "btn_QuitaAgrega"
        Me.btn_QuitaAgrega.Size = New System.Drawing.Size(41, 40)
        Me.btn_QuitaAgrega.TabIndex = 61
        Me.btn_QuitaAgrega.UseVisualStyleBackColor = True
        '
        'btn_AgregaProducto
        '
        Me.btn_AgregaProducto.Image = Global.Convenios_New.My.Resources.Resources.mas
        Me.btn_AgregaProducto.Location = New System.Drawing.Point(940, 21)
        Me.btn_AgregaProducto.Name = "btn_AgregaProducto"
        Me.btn_AgregaProducto.Size = New System.Drawing.Size(41, 40)
        Me.btn_AgregaProducto.TabIndex = 61
        Me.btn_AgregaProducto.UseVisualStyleBackColor = True
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(278, 15)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(68, 14)
        Me.Label24.TabIndex = 57
        Me.Label24.Text = "Descripción"
        '
        'txtDescripcion
        '
        Me.txtDescripcion.BackColor = System.Drawing.SystemColors.HighlightText
        Me.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripcion.Location = New System.Drawing.Point(99, 35)
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(445, 22)
        Me.txtDescripcion.TabIndex = 59
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(732, 15)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(111, 14)
        Me.Label8.TabIndex = 58
        Me.Label8.Text = "Fecha Vencimiento"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(664, 15)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(38, 14)
        Me.Label21.TabIndex = 58
        Me.Label21.Text = "Costo"
        '
        'txtCosto
        '
        Me.txtCosto.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtCosto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCosto.Location = New System.Drawing.Point(641, 35)
        Me.txtCosto.Name = "txtCosto"
        Me.txtCosto.Size = New System.Drawing.Size(88, 22)
        Me.txtCosto.TabIndex = 60
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(574, 15)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(40, 14)
        Me.Label20.TabIndex = 58
        Me.Label20.Text = "Precio"
        '
        'txtPrecio
        '
        Me.txtPrecio.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtPrecio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPrecio.Location = New System.Drawing.Point(550, 35)
        Me.txtPrecio.Name = "txtPrecio"
        Me.txtPrecio.Size = New System.Drawing.Size(87, 22)
        Me.txtPrecio.TabIndex = 60
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(21, 15)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(57, 14)
        Me.Label23.TabIndex = 58
        Me.Label23.Text = "Producto"
        '
        'txtCodpro
        '
        Me.txtCodpro.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtCodpro.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodpro.Location = New System.Drawing.Point(6, 35)
        Me.txtCodpro.Name = "txtCodpro"
        Me.txtCodpro.Size = New System.Drawing.Size(87, 22)
        Me.txtCodpro.TabIndex = 60
        '
        'rdbCliente
        '
        Me.rdbCliente.AutoSize = True
        Me.rdbCliente.Location = New System.Drawing.Point(13, 58)
        Me.rdbCliente.Name = "rdbCliente"
        Me.rdbCliente.Size = New System.Drawing.Size(131, 18)
        Me.rdbCliente.TabIndex = 79
        Me.rdbCliente.Text = "Modificar Un cliente"
        Me.rdbCliente.UseVisualStyleBackColor = True
        '
        'grbAcciones
        '
        Me.grbAcciones.Controls.Add(Me.tabAcciones)
        Me.grbAcciones.Location = New System.Drawing.Point(7, 158)
        Me.grbAcciones.Name = "grbAcciones"
        Me.grbAcciones.Size = New System.Drawing.Size(1069, 409)
        Me.grbAcciones.TabIndex = 80
        Me.grbAcciones.TabStop = False
        Me.grbAcciones.Text = "Acciones"
        '
        'tabAcciones
        '
        Me.tabAcciones.Controls.Add(Me.TabPage1)
        Me.tabAcciones.Controls.Add(Me.TabPage2)
        Me.tabAcciones.Controls.Add(Me.TabPage3)
        Me.tabAcciones.Controls.Add(Me.TabPage4)
        Me.tabAcciones.Controls.Add(Me.TabPage5)
        Me.tabAcciones.Controls.Add(Me.TabPage6)
        Me.tabAcciones.Location = New System.Drawing.Point(9, 15)
        Me.tabAcciones.Name = "tabAcciones"
        Me.tabAcciones.SelectedIndex = 0
        Me.tabAcciones.Size = New System.Drawing.Size(1054, 388)
        Me.tabAcciones.TabIndex = 81
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.SystemColors.Control
        Me.TabPage1.Controls.Add(Me.grbAgregar)
        Me.TabPage1.Location = New System.Drawing.Point(4, 23)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1046, 361)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Agregar Productos"
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.SystemColors.Control
        Me.TabPage2.Controls.Add(Me.grbModificar)
        Me.TabPage2.Location = New System.Drawing.Point(4, 23)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1046, 361)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Modificar Productos"
        '
        'grbModificar
        '
        Me.grbModificar.Controls.Add(Me.txtSkuModifica)
        Me.grbModificar.Controls.Add(Me.Label27)
        Me.grbModificar.Controls.Add(Me.Label9)
        Me.grbModificar.Controls.Add(Me.Label10)
        Me.grbModificar.Controls.Add(Me.btn_CargaInputModifica)
        Me.grbModificar.Controls.Add(Me.btn_Cambio)
        Me.grbModificar.Controls.Add(Me.btn_ModificarConvenio)
        Me.grbModificar.Controls.Add(Me.dgvProductosModificar)
        Me.grbModificar.Controls.Add(Me.txtDescripcionModif)
        Me.grbModificar.Controls.Add(Me.txtCodproModif)
        Me.grbModificar.Controls.Add(Me.Label7)
        Me.grbModificar.Controls.Add(Me.Label4)
        Me.grbModificar.Controls.Add(Me.txtPrecioModif)
        Me.grbModificar.Controls.Add(Me.Label6)
        Me.grbModificar.Controls.Add(Me.Label5)
        Me.grbModificar.Controls.Add(Me.txtCostoModif)
        Me.grbModificar.Location = New System.Drawing.Point(6, 3)
        Me.grbModificar.Name = "grbModificar"
        Me.grbModificar.Size = New System.Drawing.Size(1034, 347)
        Me.grbModificar.TabIndex = 80
        Me.grbModificar.TabStop = False
        '
        'txtSkuModifica
        '
        Me.txtSkuModifica.BackColor = System.Drawing.SystemColors.Info
        Me.txtSkuModifica.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSkuModifica.Location = New System.Drawing.Point(781, 318)
        Me.txtSkuModifica.Name = "txtSkuModifica"
        Me.txtSkuModifica.Size = New System.Drawing.Size(87, 22)
        Me.txtSkuModifica.TabIndex = 78
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(797, 298)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(57, 14)
        Me.Label27.TabIndex = 77
        Me.Label27.Text = "Producto"
        '
        'Label9
        '
        Me.Label9.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(171, 317)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(554, 23)
        Me.Label9.TabIndex = 70
        Me.Label9.Text = "Columnas deben llamarse  CODPRO - PRECIO - COSTO ESPECIAL"
        '
        'Label10
        '
        Me.Label10.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(171, 295)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(458, 12)
        Me.Label10.TabIndex = 71
        Me.Label10.Text = "Archivo debe ser excel con formato xls. o xlsx."
        '
        'btn_CargaInputModifica
        '
        Me.btn_CargaInputModifica.Image = Global.Convenios_New.My.Resources.Resources.Excel
        Me.btn_CargaInputModifica.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_CargaInputModifica.Location = New System.Drawing.Point(11, 295)
        Me.btn_CargaInputModifica.Name = "btn_CargaInputModifica"
        Me.btn_CargaInputModifica.Size = New System.Drawing.Size(154, 46)
        Me.btn_CargaInputModifica.TabIndex = 69
        Me.btn_CargaInputModifica.Text = "Carga Input"
        Me.btn_CargaInputModifica.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_CargaInputModifica.UseVisualStyleBackColor = True
        '
        'btn_Cambio
        '
        Me.btn_Cambio.Image = Global.Convenios_New.My.Resources.Resources.Tips
        Me.btn_Cambio.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_Cambio.Location = New System.Drawing.Point(874, 21)
        Me.btn_Cambio.Name = "btn_Cambio"
        Me.btn_Cambio.Size = New System.Drawing.Size(154, 40)
        Me.btn_Cambio.TabIndex = 64
        Me.btn_Cambio.Text = "Cambiar Producto"
        Me.btn_Cambio.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_Cambio.UseVisualStyleBackColor = True
        '
        'btn_ModificarConvenio
        '
        Me.btn_ModificarConvenio.Image = Global.Convenios_New.My.Resources.Resources.editar
        Me.btn_ModificarConvenio.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_ModificarConvenio.Location = New System.Drawing.Point(874, 295)
        Me.btn_ModificarConvenio.Name = "btn_ModificarConvenio"
        Me.btn_ModificarConvenio.Size = New System.Drawing.Size(154, 46)
        Me.btn_ModificarConvenio.TabIndex = 63
        Me.btn_ModificarConvenio.Text = "Modificar Productos"
        Me.btn_ModificarConvenio.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_ModificarConvenio.UseVisualStyleBackColor = True
        '
        'dgvProductosModificar
        '
        Me.dgvProductosModificar.AllowUserToAddRows = False
        Me.dgvProductosModificar.AllowUserToDeleteRows = False
        Me.dgvProductosModificar.AllowUserToResizeRows = False
        Me.dgvProductosModificar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvProductosModificar.Location = New System.Drawing.Point(11, 68)
        Me.dgvProductosModificar.Name = "dgvProductosModificar"
        Me.dgvProductosModificar.ReadOnly = True
        Me.dgvProductosModificar.RowHeadersVisible = False
        Me.dgvProductosModificar.Size = New System.Drawing.Size(1017, 221)
        Me.dgvProductosModificar.TabIndex = 62
        '
        'txtDescripcionModif
        '
        Me.txtDescripcionModif.BackColor = System.Drawing.SystemColors.Info
        Me.txtDescripcionModif.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripcionModif.Location = New System.Drawing.Point(97, 34)
        Me.txtDescripcionModif.Name = "txtDescripcionModif"
        Me.txtDescripcionModif.Size = New System.Drawing.Size(365, 22)
        Me.txtDescripcionModif.TabIndex = 59
        '
        'txtCodproModif
        '
        Me.txtCodproModif.BackColor = System.Drawing.SystemColors.Info
        Me.txtCodproModif.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodproModif.Location = New System.Drawing.Point(7, 34)
        Me.txtCodproModif.Name = "txtCodproModif"
        Me.txtCodproModif.Size = New System.Drawing.Size(87, 22)
        Me.txtCodproModif.TabIndex = 60
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(248, 14)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(68, 14)
        Me.Label7.TabIndex = 57
        Me.Label7.Text = "Descripción"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(23, 14)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(57, 14)
        Me.Label4.TabIndex = 58
        Me.Label4.Text = "Producto"
        '
        'txtPrecioModif
        '
        Me.txtPrecioModif.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtPrecioModif.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPrecioModif.Location = New System.Drawing.Point(468, 34)
        Me.txtPrecioModif.Name = "txtPrecioModif"
        Me.txtPrecioModif.Size = New System.Drawing.Size(87, 22)
        Me.txtPrecioModif.TabIndex = 60
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(584, 14)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(38, 14)
        Me.Label6.TabIndex = 58
        Me.Label6.Text = "Costo"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(490, 14)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(40, 14)
        Me.Label5.TabIndex = 58
        Me.Label5.Text = "Precio"
        '
        'txtCostoModif
        '
        Me.txtCostoModif.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtCostoModif.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCostoModif.Location = New System.Drawing.Point(561, 34)
        Me.txtCostoModif.Name = "txtCostoModif"
        Me.txtCostoModif.Size = New System.Drawing.Size(88, 22)
        Me.txtCostoModif.TabIndex = 60
        '
        'TabPage3
        '
        Me.TabPage3.BackColor = System.Drawing.SystemColors.Control
        Me.TabPage3.Controls.Add(Me.txtSkuElimina)
        Me.TabPage3.Controls.Add(Me.Label15)
        Me.TabPage3.Controls.Add(Me.Label11)
        Me.TabPage3.Controls.Add(Me.Label12)
        Me.TabPage3.Controls.Add(Me.btn_InputElimina)
        Me.TabPage3.Controls.Add(Me.btn_EliminardeConvenio)
        Me.TabPage3.Controls.Add(Me.dgvProductoEliminar)
        Me.TabPage3.Location = New System.Drawing.Point(4, 23)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(1046, 361)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Eliminar Productos"
        '
        'txtSkuElimina
        '
        Me.txtSkuElimina.BackColor = System.Drawing.SystemColors.Info
        Me.txtSkuElimina.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSkuElimina.Location = New System.Drawing.Point(786, 331)
        Me.txtSkuElimina.Name = "txtSkuElimina"
        Me.txtSkuElimina.Size = New System.Drawing.Size(87, 22)
        Me.txtSkuElimina.TabIndex = 76
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(802, 311)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(57, 14)
        Me.Label15.TabIndex = 75
        Me.Label15.Text = "Producto"
        '
        'Label11
        '
        Me.Label11.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(166, 331)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(554, 23)
        Me.Label11.TabIndex = 73
        Me.Label11.Text = "Columnas deben llamarse  CODPRO"
        '
        'Label12
        '
        Me.Label12.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(166, 309)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(458, 12)
        Me.Label12.TabIndex = 74
        Me.Label12.Text = "Archivo debe ser excel con formato xls. o xlsx."
        '
        'btn_InputElimina
        '
        Me.btn_InputElimina.Image = Global.Convenios_New.My.Resources.Resources.Excel
        Me.btn_InputElimina.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_InputElimina.Location = New System.Drawing.Point(6, 309)
        Me.btn_InputElimina.Name = "btn_InputElimina"
        Me.btn_InputElimina.Size = New System.Drawing.Size(154, 46)
        Me.btn_InputElimina.TabIndex = 72
        Me.btn_InputElimina.Text = "Carga Input"
        Me.btn_InputElimina.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_InputElimina.UseVisualStyleBackColor = True
        '
        'btn_EliminardeConvenio
        '
        Me.btn_EliminardeConvenio.Image = Global.Convenios_New.My.Resources.Resources.Delete
        Me.btn_EliminardeConvenio.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_EliminardeConvenio.Location = New System.Drawing.Point(886, 309)
        Me.btn_EliminardeConvenio.Name = "btn_EliminardeConvenio"
        Me.btn_EliminardeConvenio.Size = New System.Drawing.Size(154, 46)
        Me.btn_EliminardeConvenio.TabIndex = 63
        Me.btn_EliminardeConvenio.Text = "Eliminar Productos"
        Me.btn_EliminardeConvenio.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_EliminardeConvenio.UseVisualStyleBackColor = True
        '
        'dgvProductoEliminar
        '
        Me.dgvProductoEliminar.AllowUserToAddRows = False
        Me.dgvProductoEliminar.AllowUserToDeleteRows = False
        Me.dgvProductoEliminar.AllowUserToResizeRows = False
        Me.dgvProductoEliminar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvProductoEliminar.Location = New System.Drawing.Point(6, 6)
        Me.dgvProductoEliminar.Name = "dgvProductoEliminar"
        Me.dgvProductoEliminar.ReadOnly = True
        Me.dgvProductoEliminar.RowHeadersVisible = False
        Me.dgvProductoEliminar.Size = New System.Drawing.Size(1034, 300)
        Me.dgvProductoEliminar.TabIndex = 62
        '
        'TabPage4
        '
        Me.TabPage4.BackColor = System.Drawing.SystemColors.Control
        Me.TabPage4.Controls.Add(Me.chkMarcaTodoConvenio)
        Me.TabPage4.Controls.Add(Me.Label13)
        Me.TabPage4.Controls.Add(Me.dgvLineasReajuste)
        Me.TabPage4.Controls.Add(Me.Label84)
        Me.TabPage4.Controls.Add(Me.dtpFecVenConvenio)
        Me.TabPage4.Controls.Add(Me.dtpFechaLineaReajuste)
        Me.TabPage4.Controls.Add(Me.btn_CambiaFechas)
        Me.TabPage4.Controls.Add(Me.Label73)
        Me.TabPage4.Controls.Add(Me.dgvCambiaFechas)
        Me.TabPage4.Location = New System.Drawing.Point(4, 23)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(1046, 361)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Modificar Fechas"
        '
        'chkMarcaTodoConvenio
        '
        Me.chkMarcaTodoConvenio.AutoSize = True
        Me.chkMarcaTodoConvenio.Location = New System.Drawing.Point(715, 157)
        Me.chkMarcaTodoConvenio.Name = "chkMarcaTodoConvenio"
        Me.chkMarcaTodoConvenio.Size = New System.Drawing.Size(90, 18)
        Me.chkMarcaTodoConvenio.TabIndex = 40
        Me.chkMarcaTodoConvenio.Text = "Marca Todo"
        Me.chkMarcaTodoConvenio.UseVisualStyleBackColor = True
        '
        'Label13
        '
        Me.Label13.Location = New System.Drawing.Point(712, 199)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(302, 66)
        Me.Label13.TabIndex = 39
        Me.Label13.Text = resources.GetString("Label13.Text")
        '
        'dgvLineasReajuste
        '
        Me.dgvLineasReajuste.AllowUserToAddRows = False
        Me.dgvLineasReajuste.AllowUserToDeleteRows = False
        Me.dgvLineasReajuste.AllowUserToResizeRows = False
        Me.dgvLineasReajuste.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLineasReajuste.Location = New System.Drawing.Point(715, 7)
        Me.dgvLineasReajuste.Name = "dgvLineasReajuste"
        Me.dgvLineasReajuste.ReadOnly = True
        Me.dgvLineasReajuste.RowHeadersVisible = False
        Me.dgvLineasReajuste.Size = New System.Drawing.Size(325, 144)
        Me.dgvLineasReajuste.TabIndex = 38
        '
        'Label84
        '
        Me.Label84.AutoSize = True
        Me.Label84.Location = New System.Drawing.Point(740, 265)
        Me.Label84.Name = "Label84"
        Me.Label84.Size = New System.Drawing.Size(143, 14)
        Me.Label84.TabIndex = 34
        Me.Label84.Text = "Fecha Vencimiento Línea"
        '
        'dtpFecVenConvenio
        '
        Me.dtpFecVenConvenio.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecVenConvenio.Location = New System.Drawing.Point(906, 283)
        Me.dtpFecVenConvenio.Name = "dtpFecVenConvenio"
        Me.dtpFecVenConvenio.Size = New System.Drawing.Size(130, 22)
        Me.dtpFecVenConvenio.TabIndex = 36
        '
        'dtpFechaLineaReajuste
        '
        Me.dtpFechaLineaReajuste.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaLineaReajuste.Location = New System.Drawing.Point(743, 282)
        Me.dtpFechaLineaReajuste.Name = "dtpFechaLineaReajuste"
        Me.dtpFechaLineaReajuste.Size = New System.Drawing.Size(130, 22)
        Me.dtpFechaLineaReajuste.TabIndex = 33
        '
        'btn_CambiaFechas
        '
        Me.btn_CambiaFechas.Image = Global.Convenios_New.My.Resources.Resources.editar
        Me.btn_CambiaFechas.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_CambiaFechas.Location = New System.Drawing.Point(899, 319)
        Me.btn_CambiaFechas.Name = "btn_CambiaFechas"
        Me.btn_CambiaFechas.Size = New System.Drawing.Size(140, 40)
        Me.btn_CambiaFechas.TabIndex = 35
        Me.btn_CambiaFechas.Text = "Modificar Fechas"
        Me.btn_CambiaFechas.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_CambiaFechas.UseVisualStyleBackColor = True
        '
        'Label73
        '
        Me.Label73.AutoSize = True
        Me.Label73.Location = New System.Drawing.Point(903, 265)
        Me.Label73.Name = "Label73"
        Me.Label73.Size = New System.Drawing.Size(99, 14)
        Me.Label73.TabIndex = 37
        Me.Label73.Text = "Convenio Hasta :"
        '
        'dgvCambiaFechas
        '
        Me.dgvCambiaFechas.AllowUserToAddRows = False
        Me.dgvCambiaFechas.AllowUserToDeleteRows = False
        Me.dgvCambiaFechas.AllowUserToResizeRows = False
        Me.dgvCambiaFechas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCambiaFechas.Location = New System.Drawing.Point(7, 7)
        Me.dgvCambiaFechas.Name = "dgvCambiaFechas"
        Me.dgvCambiaFechas.ReadOnly = True
        Me.dgvCambiaFechas.RowHeadersVisible = False
        Me.dgvCambiaFechas.Size = New System.Drawing.Size(702, 348)
        Me.dgvCambiaFechas.TabIndex = 0
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.ProgressBar1)
        Me.TabPage5.Controls.Add(Me.btnProcesar)
        Me.TabPage5.Controls.Add(Me.GroupBox2)
        Me.TabPage5.Controls.Add(Me.GroupBox1)
        Me.TabPage5.Location = New System.Drawing.Point(4, 23)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage5.Size = New System.Drawing.Size(1046, 361)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "Reemplazo Codigo"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(6, 317)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(882, 35)
        Me.ProgressBar1.TabIndex = 75
        Me.ProgressBar1.Visible = False
        '
        'btnProcesar
        '
        Me.btnProcesar.Enabled = False
        Me.btnProcesar.Location = New System.Drawing.Point(902, 317)
        Me.btnProcesar.Name = "btnProcesar"
        Me.btnProcesar.Size = New System.Drawing.Size(138, 38)
        Me.btnProcesar.TabIndex = 74
        Me.btnProcesar.Text = "Procesar"
        Me.btnProcesar.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.dtpFecha)
        Me.GroupBox2.Controls.Add(Me.rbtAgregar)
        Me.GroupBox2.Controls.Add(Me.rbtReemplazar)
        Me.GroupBox2.Controls.Add(Me.rbtQuitar)
        Me.GroupBox2.Controls.Add(Me.btnxlsxReemplazo)
        Me.GroupBox2.Controls.Add(Me.dgvReemplazo)
        Me.GroupBox2.Controls.Add(Me.Label26)
        Me.GroupBox2.Controls.Add(Me.txtConvenioReem)
        Me.GroupBox2.Controls.Add(Me.Label18)
        Me.GroupBox2.Controls.Add(Me.txtLineaReem)
        Me.GroupBox2.Controls.Add(Me.Label25)
        Me.GroupBox2.Controls.Add(Me.txtDescripReem)
        Me.GroupBox2.Controls.Add(Me.Label29)
        Me.GroupBox2.Controls.Add(Me.txtSkuReem)
        Me.GroupBox2.Location = New System.Drawing.Point(6, 162)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(1034, 149)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Codigo Reemplazo"
        '
        'dtpFecha
        '
        Me.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecha.Location = New System.Drawing.Point(932, 69)
        Me.dtpFecha.Name = "dtpFecha"
        Me.dtpFecha.Size = New System.Drawing.Size(96, 22)
        Me.dtpFecha.TabIndex = 100
        '
        'rbtAgregar
        '
        Me.rbtAgregar.AutoSize = True
        Me.rbtAgregar.Checked = True
        Me.rbtAgregar.Location = New System.Drawing.Point(943, 126)
        Me.rbtAgregar.Name = "rbtAgregar"
        Me.rbtAgregar.Size = New System.Drawing.Size(68, 18)
        Me.rbtAgregar.TabIndex = 99
        Me.rbtAgregar.TabStop = True
        Me.rbtAgregar.Text = "Agregar"
        Me.rbtAgregar.UseVisualStyleBackColor = True
        Me.rbtAgregar.Visible = False
        '
        'rbtReemplazar
        '
        Me.rbtReemplazar.AutoSize = True
        Me.rbtReemplazar.Location = New System.Drawing.Point(851, 126)
        Me.rbtReemplazar.Name = "rbtReemplazar"
        Me.rbtReemplazar.Size = New System.Drawing.Size(86, 18)
        Me.rbtReemplazar.TabIndex = 98
        Me.rbtReemplazar.TabStop = True
        Me.rbtReemplazar.Text = "Reemplazar"
        Me.rbtReemplazar.UseVisualStyleBackColor = True
        Me.rbtReemplazar.Visible = False
        '
        'rbtQuitar
        '
        Me.rbtQuitar.AutoSize = True
        Me.rbtQuitar.Location = New System.Drawing.Point(790, 124)
        Me.rbtQuitar.Name = "rbtQuitar"
        Me.rbtQuitar.Size = New System.Drawing.Size(58, 18)
        Me.rbtQuitar.TabIndex = 97
        Me.rbtQuitar.TabStop = True
        Me.rbtQuitar.Text = "Quitar"
        Me.rbtQuitar.UseVisualStyleBackColor = True
        Me.rbtQuitar.Visible = False
        '
        'btnxlsxReemplazo
        '
        Me.btnxlsxReemplazo.Image = Global.Convenios_New.My.Resources.Resources.Excel
        Me.btnxlsxReemplazo.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnxlsxReemplazo.Location = New System.Drawing.Point(790, 69)
        Me.btnxlsxReemplazo.Name = "btnxlsxReemplazo"
        Me.btnxlsxReemplazo.Size = New System.Drawing.Size(136, 46)
        Me.btnxlsxReemplazo.TabIndex = 96
        Me.btnxlsxReemplazo.Text = "Exportar excel"
        Me.btnxlsxReemplazo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnxlsxReemplazo.UseVisualStyleBackColor = True
        '
        'dgvReemplazo
        '
        Me.dgvReemplazo.AllowUserToAddRows = False
        Me.dgvReemplazo.AllowUserToDeleteRows = False
        Me.dgvReemplazo.AllowUserToResizeRows = False
        Me.dgvReemplazo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvReemplazo.Location = New System.Drawing.Point(12, 69)
        Me.dgvReemplazo.Name = "dgvReemplazo"
        Me.dgvReemplazo.ReadOnly = True
        Me.dgvReemplazo.RowHeadersVisible = False
        Me.dgvReemplazo.Size = New System.Drawing.Size(767, 73)
        Me.dgvReemplazo.TabIndex = 95
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(679, 24)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(62, 14)
        Me.Label26.TabIndex = 93
        Me.Label26.Text = "Convenios"
        '
        'txtConvenioReem
        '
        Me.txtConvenioReem.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtConvenioReem.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtConvenioReem.Location = New System.Drawing.Point(682, 41)
        Me.txtConvenioReem.Name = "txtConvenioReem"
        Me.txtConvenioReem.ReadOnly = True
        Me.txtConvenioReem.Size = New System.Drawing.Size(97, 22)
        Me.txtConvenioReem.TabIndex = 94
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(782, 24)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(35, 14)
        Me.Label18.TabIndex = 91
        Me.Label18.Text = "Linea"
        '
        'txtLineaReem
        '
        Me.txtLineaReem.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtLineaReem.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtLineaReem.Location = New System.Drawing.Point(785, 41)
        Me.txtLineaReem.Name = "txtLineaReem"
        Me.txtLineaReem.ReadOnly = True
        Me.txtLineaReem.Size = New System.Drawing.Size(243, 22)
        Me.txtLineaReem.TabIndex = 92
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(102, 24)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(68, 14)
        Me.Label25.TabIndex = 87
        Me.Label25.Text = "Descripción"
        '
        'txtDescripReem
        '
        Me.txtDescripReem.BackColor = System.Drawing.SystemColors.HighlightText
        Me.txtDescripReem.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripReem.Location = New System.Drawing.Point(105, 41)
        Me.txtDescripReem.Name = "txtDescripReem"
        Me.txtDescripReem.ReadOnly = True
        Me.txtDescripReem.Size = New System.Drawing.Size(571, 22)
        Me.txtDescripReem.TabIndex = 89
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(9, 21)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(57, 14)
        Me.Label29.TabIndex = 70
        Me.Label29.Text = "Producto"
        '
        'txtSkuReem
        '
        Me.txtSkuReem.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtSkuReem.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSkuReem.Location = New System.Drawing.Point(12, 41)
        Me.txtSkuReem.Name = "txtSkuReem"
        Me.txtSkuReem.Size = New System.Drawing.Size(87, 22)
        Me.txtSkuReem.TabIndex = 74
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cbxReversaReem)
        Me.GroupBox1.Controls.Add(Me.btnxlsxOriginal)
        Me.GroupBox1.Controls.Add(Me.dgvOriginal)
        Me.GroupBox1.Controls.Add(Me.Label17)
        Me.GroupBox1.Controls.Add(Me.txtLineaOri)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Controls.Add(Me.txtDescripOri)
        Me.GroupBox1.Controls.Add(Me.Label19)
        Me.GroupBox1.Controls.Add(Me.txtConvenioTotales)
        Me.GroupBox1.Controls.Add(Me.Label22)
        Me.GroupBox1.Controls.Add(Me.txtSkuOri)
        Me.GroupBox1.Location = New System.Drawing.Point(6, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1034, 160)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Codigo Original"
        '
        'cbxReversaReem
        '
        Me.cbxReversaReem.AutoSize = True
        Me.cbxReversaReem.Location = New System.Drawing.Point(790, 136)
        Me.cbxReversaReem.Name = "cbxReversaReem"
        Me.cbxReversaReem.Size = New System.Drawing.Size(130, 18)
        Me.cbxReversaReem.TabIndex = 99
        Me.cbxReversaReem.Text = "Reversa Reemplazo"
        Me.cbxReversaReem.UseVisualStyleBackColor = True
        Me.cbxReversaReem.Visible = False
        '
        'btnxlsxOriginal
        '
        Me.btnxlsxOriginal.Image = Global.Convenios_New.My.Resources.Resources.Excel
        Me.btnxlsxOriginal.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnxlsxOriginal.Location = New System.Drawing.Point(790, 71)
        Me.btnxlsxOriginal.Name = "btnxlsxOriginal"
        Me.btnxlsxOriginal.Size = New System.Drawing.Size(136, 46)
        Me.btnxlsxOriginal.TabIndex = 98
        Me.btnxlsxOriginal.Text = "Exportar excel"
        Me.btnxlsxOriginal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnxlsxOriginal.UseVisualStyleBackColor = True
        '
        'dgvOriginal
        '
        Me.dgvOriginal.AllowUserToAddRows = False
        Me.dgvOriginal.AllowUserToDeleteRows = False
        Me.dgvOriginal.AllowUserToResizeRows = False
        Me.dgvOriginal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvOriginal.Location = New System.Drawing.Point(12, 71)
        Me.dgvOriginal.Name = "dgvOriginal"
        Me.dgvOriginal.ReadOnly = True
        Me.dgvOriginal.RowHeadersVisible = False
        Me.dgvOriginal.Size = New System.Drawing.Size(767, 79)
        Me.dgvOriginal.TabIndex = 97
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(782, 26)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(35, 14)
        Me.Label17.TabIndex = 85
        Me.Label17.Text = "Linea"
        '
        'txtLineaOri
        '
        Me.txtLineaOri.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtLineaOri.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtLineaOri.Location = New System.Drawing.Point(785, 43)
        Me.txtLineaOri.Name = "txtLineaOri"
        Me.txtLineaOri.ReadOnly = True
        Me.txtLineaOri.Size = New System.Drawing.Size(243, 22)
        Me.txtLineaOri.TabIndex = 86
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(102, 26)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(68, 14)
        Me.Label16.TabIndex = 76
        Me.Label16.Text = "Descripción"
        '
        'txtDescripOri
        '
        Me.txtDescripOri.BackColor = System.Drawing.SystemColors.HighlightText
        Me.txtDescripOri.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripOri.Location = New System.Drawing.Point(105, 43)
        Me.txtDescripOri.Name = "txtDescripOri"
        Me.txtDescripOri.ReadOnly = True
        Me.txtDescripOri.Size = New System.Drawing.Size(571, 22)
        Me.txtDescripOri.TabIndex = 81
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(679, 26)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(62, 14)
        Me.Label19.TabIndex = 79
        Me.Label19.Text = "Convenios"
        '
        'txtConvenioTotales
        '
        Me.txtConvenioTotales.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtConvenioTotales.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtConvenioTotales.Location = New System.Drawing.Point(682, 43)
        Me.txtConvenioTotales.Name = "txtConvenioTotales"
        Me.txtConvenioTotales.ReadOnly = True
        Me.txtConvenioTotales.Size = New System.Drawing.Size(97, 22)
        Me.txtConvenioTotales.TabIndex = 83
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(9, 23)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(57, 14)
        Me.Label22.TabIndex = 80
        Me.Label22.Text = "Producto"
        '
        'txtSkuOri
        '
        Me.txtSkuOri.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtSkuOri.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSkuOri.Location = New System.Drawing.Point(12, 43)
        Me.txtSkuOri.Name = "txtSkuOri"
        Me.txtSkuOri.Size = New System.Drawing.Size(87, 22)
        Me.txtSkuOri.TabIndex = 84
        '
        'TabPage6
        '
        Me.TabPage6.Controls.Add(Me.Label37)
        Me.TabPage6.Controls.Add(Me.Label31)
        Me.TabPage6.Controls.Add(Me.dgvConReemM)
        Me.TabPage6.Controls.Add(Me.dgvConOriM)
        Me.TabPage6.Controls.Add(Me.GroupBox3)
        Me.TabPage6.Location = New System.Drawing.Point(4, 23)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage6.Size = New System.Drawing.Size(1046, 361)
        Me.TabPage6.TabIndex = 5
        Me.TabPage6.Text = "Reemplazo Manual"
        Me.TabPage6.UseVisualStyleBackColor = True
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(530, 221)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(124, 14)
        Me.Label37.TabIndex = 104
        Me.Label37.Text = "Convenios Reemplazo"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(6, 221)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(105, 14)
        Me.Label31.TabIndex = 103
        Me.Label31.Text = "Convenios Original"
        '
        'dgvConReemM
        '
        Me.dgvConReemM.AllowUserToAddRows = False
        Me.dgvConReemM.AllowUserToDeleteRows = False
        Me.dgvConReemM.AllowUserToResizeRows = False
        Me.dgvConReemM.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvConReemM.Location = New System.Drawing.Point(533, 238)
        Me.dgvConReemM.Name = "dgvConReemM"
        Me.dgvConReemM.ReadOnly = True
        Me.dgvConReemM.RowHeadersVisible = False
        Me.dgvConReemM.Size = New System.Drawing.Size(507, 114)
        Me.dgvConReemM.TabIndex = 99
        '
        'dgvConOriM
        '
        Me.dgvConOriM.AllowUserToAddRows = False
        Me.dgvConOriM.AllowUserToDeleteRows = False
        Me.dgvConOriM.AllowUserToResizeRows = False
        Me.dgvConOriM.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvConOriM.Location = New System.Drawing.Point(6, 238)
        Me.dgvConOriM.Name = "dgvConOriM"
        Me.dgvConOriM.ReadOnly = True
        Me.dgvConOriM.RowHeadersVisible = False
        Me.dgvConOriM.Size = New System.Drawing.Size(521, 114)
        Me.dgvConOriM.TabIndex = 98
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Label44)
        Me.GroupBox3.Controls.Add(Me.txtDescripSkuM)
        Me.GroupBox3.Controls.Add(Me.Label43)
        Me.GroupBox3.Controls.Add(Me.txtMargenM)
        Me.GroupBox3.Controls.Add(Me.Label42)
        Me.GroupBox3.Controls.Add(Me.txtCostoM)
        Me.GroupBox3.Controls.Add(Me.Label36)
        Me.GroupBox3.Controls.Add(Me.txtPrecioM)
        Me.GroupBox3.Controls.Add(Me.Label35)
        Me.GroupBox3.Controls.Add(Me.txtFechaFinM)
        Me.GroupBox3.Controls.Add(Me.Label34)
        Me.GroupBox3.Controls.Add(Me.txtFechaIniM)
        Me.GroupBox3.Controls.Add(Me.Label33)
        Me.GroupBox3.Controls.Add(Me.txtRutcliM)
        Me.GroupBox3.Controls.Add(Me.btnProcesarManual)
        Me.GroupBox3.Controls.Add(Me.Label41)
        Me.GroupBox3.Controls.Add(Me.dtpFechaManual)
        Me.GroupBox3.Controls.Add(Me.rbtAgregarM)
        Me.GroupBox3.Controls.Add(Me.rbtReemplazarM)
        Me.GroupBox3.Controls.Add(Me.rbtQuitarM)
        Me.GroupBox3.Controls.Add(Me.Label38)
        Me.GroupBox3.Controls.Add(Me.txtLineaReemM)
        Me.GroupBox3.Controls.Add(Me.Label39)
        Me.GroupBox3.Controls.Add(Me.txtdescripReemM)
        Me.GroupBox3.Controls.Add(Me.Label40)
        Me.GroupBox3.Controls.Add(Me.txtSkuRemM)
        Me.GroupBox3.Controls.Add(Me.cbxReversaM)
        Me.GroupBox3.Controls.Add(Me.Label28)
        Me.GroupBox3.Controls.Add(Me.txtLineaM)
        Me.GroupBox3.Controls.Add(Me.Label30)
        Me.GroupBox3.Controls.Add(Me.txtRsocialM)
        Me.GroupBox3.Controls.Add(Me.Label32)
        Me.GroupBox3.Controls.Add(Me.txtskuOriginalM)
        Me.GroupBox3.Location = New System.Drawing.Point(6, 6)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(1034, 210)
        Me.GroupBox3.TabIndex = 1
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Codigo Original"
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Location = New System.Drawing.Point(385, 69)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(68, 14)
        Me.Label44.TabIndex = 125
        Me.Label44.Text = "Descripción"
        '
        'txtDescripSkuM
        '
        Me.txtDescripSkuM.BackColor = System.Drawing.SystemColors.HighlightText
        Me.txtDescripSkuM.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripSkuM.Location = New System.Drawing.Point(385, 86)
        Me.txtDescripSkuM.Name = "txtDescripSkuM"
        Me.txtDescripSkuM.ReadOnly = True
        Me.txtDescripSkuM.Size = New System.Drawing.Size(642, 22)
        Me.txtDescripSkuM.TabIndex = 126
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Location = New System.Drawing.Point(565, 26)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(47, 14)
        Me.Label43.TabIndex = 123
        Me.Label43.Text = "Margen"
        '
        'txtMargenM
        '
        Me.txtMargenM.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtMargenM.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMargenM.Location = New System.Drawing.Point(568, 43)
        Me.txtMargenM.Name = "txtMargenM"
        Me.txtMargenM.Size = New System.Drawing.Size(90, 22)
        Me.txtMargenM.TabIndex = 124
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Location = New System.Drawing.Point(472, 26)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(38, 14)
        Me.Label42.TabIndex = 121
        Me.Label42.Text = "Costo"
        '
        'txtCostoM
        '
        Me.txtCostoM.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtCostoM.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCostoM.Location = New System.Drawing.Point(475, 43)
        Me.txtCostoM.Name = "txtCostoM"
        Me.txtCostoM.Size = New System.Drawing.Size(90, 22)
        Me.txtCostoM.TabIndex = 122
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(382, 26)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(40, 14)
        Me.Label36.TabIndex = 119
        Me.Label36.Text = "Precio"
        '
        'txtPrecioM
        '
        Me.txtPrecioM.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtPrecioM.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPrecioM.Location = New System.Drawing.Point(382, 43)
        Me.txtPrecioM.Name = "txtPrecioM"
        Me.txtPrecioM.Size = New System.Drawing.Size(90, 22)
        Me.txtPrecioM.TabIndex = 120
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(292, 26)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(58, 14)
        Me.Label35.TabIndex = 117
        Me.Label35.Text = "Fecha Fin"
        '
        'txtFechaFinM
        '
        Me.txtFechaFinM.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtFechaFinM.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFechaFinM.Location = New System.Drawing.Point(289, 43)
        Me.txtFechaFinM.Name = "txtFechaFinM"
        Me.txtFechaFinM.Size = New System.Drawing.Size(90, 22)
        Me.txtFechaFinM.TabIndex = 118
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(195, 26)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(71, 14)
        Me.Label34.TabIndex = 115
        Me.Label34.Text = "Fecha Inicio"
        '
        'txtFechaIniM
        '
        Me.txtFechaIniM.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtFechaIniM.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFechaIniM.Location = New System.Drawing.Point(196, 43)
        Me.txtFechaIniM.Name = "txtFechaIniM"
        Me.txtFechaIniM.Size = New System.Drawing.Size(90, 22)
        Me.txtFechaIniM.TabIndex = 116
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(8, 26)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(26, 14)
        Me.Label33.TabIndex = 113
        Me.Label33.Text = "Rut"
        '
        'txtRutcliM
        '
        Me.txtRutcliM.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtRutcliM.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRutcliM.Location = New System.Drawing.Point(11, 43)
        Me.txtRutcliM.Name = "txtRutcliM"
        Me.txtRutcliM.Size = New System.Drawing.Size(92, 22)
        Me.txtRutcliM.TabIndex = 114
        '
        'btnProcesarManual
        '
        Me.btnProcesarManual.Enabled = False
        Me.btnProcesarManual.Location = New System.Drawing.Point(889, 166)
        Me.btnProcesarManual.Name = "btnProcesarManual"
        Me.btnProcesarManual.Size = New System.Drawing.Size(138, 38)
        Me.btnProcesarManual.TabIndex = 112
        Me.btnProcesarManual.Text = "Procesar"
        Me.btnProcesarManual.UseVisualStyleBackColor = True
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Location = New System.Drawing.Point(906, 123)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(39, 14)
        Me.Label41.TabIndex = 111
        Me.Label41.Text = "Fecha"
        '
        'dtpFechaManual
        '
        Me.dtpFechaManual.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaManual.Location = New System.Drawing.Point(909, 140)
        Me.dtpFechaManual.Name = "dtpFechaManual"
        Me.dtpFechaManual.Size = New System.Drawing.Size(118, 22)
        Me.dtpFechaManual.TabIndex = 100
        '
        'rbtAgregarM
        '
        Me.rbtAgregarM.AutoSize = True
        Me.rbtAgregarM.Checked = True
        Me.rbtAgregarM.Location = New System.Drawing.Point(804, 175)
        Me.rbtAgregarM.Name = "rbtAgregarM"
        Me.rbtAgregarM.Size = New System.Drawing.Size(68, 18)
        Me.rbtAgregarM.TabIndex = 110
        Me.rbtAgregarM.TabStop = True
        Me.rbtAgregarM.Text = "Agregar"
        Me.rbtAgregarM.UseVisualStyleBackColor = True
        Me.rbtAgregarM.Visible = False
        '
        'rbtReemplazarM
        '
        Me.rbtReemplazarM.AutoSize = True
        Me.rbtReemplazarM.Location = New System.Drawing.Point(712, 175)
        Me.rbtReemplazarM.Name = "rbtReemplazarM"
        Me.rbtReemplazarM.Size = New System.Drawing.Size(86, 18)
        Me.rbtReemplazarM.TabIndex = 109
        Me.rbtReemplazarM.TabStop = True
        Me.rbtReemplazarM.Text = "Reemplazar"
        Me.rbtReemplazarM.UseVisualStyleBackColor = True
        Me.rbtReemplazarM.Visible = False
        '
        'rbtQuitarM
        '
        Me.rbtQuitarM.AutoSize = True
        Me.rbtQuitarM.Location = New System.Drawing.Point(648, 175)
        Me.rbtQuitarM.Name = "rbtQuitarM"
        Me.rbtQuitarM.Size = New System.Drawing.Size(58, 18)
        Me.rbtQuitarM.TabIndex = 108
        Me.rbtQuitarM.TabStop = True
        Me.rbtQuitarM.Text = "Quitar"
        Me.rbtQuitarM.UseVisualStyleBackColor = True
        Me.rbtQuitarM.Visible = False
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(678, 124)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(35, 14)
        Me.Label38.TabIndex = 104
        Me.Label38.Text = "Linea"
        '
        'txtLineaReemM
        '
        Me.txtLineaReemM.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtLineaReemM.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtLineaReemM.Location = New System.Drawing.Point(681, 140)
        Me.txtLineaReemM.Name = "txtLineaReemM"
        Me.txtLineaReemM.ReadOnly = True
        Me.txtLineaReemM.Size = New System.Drawing.Size(222, 22)
        Me.txtLineaReemM.TabIndex = 105
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Location = New System.Drawing.Point(101, 123)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(68, 14)
        Me.Label39.TabIndex = 102
        Me.Label39.Text = "Descripción"
        '
        'txtdescripReemM
        '
        Me.txtdescripReemM.BackColor = System.Drawing.SystemColors.HighlightText
        Me.txtdescripReemM.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtdescripReemM.Location = New System.Drawing.Point(104, 140)
        Me.txtdescripReemM.Name = "txtdescripReemM"
        Me.txtdescripReemM.ReadOnly = True
        Me.txtdescripReemM.Size = New System.Drawing.Size(571, 22)
        Me.txtdescripReemM.TabIndex = 103
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(8, 120)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(57, 14)
        Me.Label40.TabIndex = 100
        Me.Label40.Text = "Producto"
        '
        'txtSkuRemM
        '
        Me.txtSkuRemM.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtSkuRemM.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSkuRemM.Location = New System.Drawing.Point(11, 140)
        Me.txtSkuRemM.Name = "txtSkuRemM"
        Me.txtSkuRemM.Size = New System.Drawing.Size(87, 22)
        Me.txtSkuRemM.TabIndex = 101
        '
        'cbxReversaM
        '
        Me.cbxReversaM.Location = New System.Drawing.Point(935, 38)
        Me.cbxReversaM.Name = "cbxReversaM"
        Me.cbxReversaM.Size = New System.Drawing.Size(93, 33)
        Me.cbxReversaM.TabIndex = 99
        Me.cbxReversaM.Text = "Reversa Reemplazo"
        Me.cbxReversaM.UseVisualStyleBackColor = True
        Me.cbxReversaM.Visible = False
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(659, 26)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(35, 14)
        Me.Label28.TabIndex = 85
        Me.Label28.Text = "Linea"
        '
        'txtLineaM
        '
        Me.txtLineaM.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtLineaM.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtLineaM.Location = New System.Drawing.Point(662, 43)
        Me.txtLineaM.Name = "txtLineaM"
        Me.txtLineaM.ReadOnly = True
        Me.txtLineaM.Size = New System.Drawing.Size(266, 22)
        Me.txtLineaM.TabIndex = 86
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(8, 69)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(73, 14)
        Me.Label30.TabIndex = 76
        Me.Label30.Text = "Razon Social"
        '
        'txtRsocialM
        '
        Me.txtRsocialM.BackColor = System.Drawing.SystemColors.HighlightText
        Me.txtRsocialM.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRsocialM.Location = New System.Drawing.Point(11, 86)
        Me.txtRsocialM.Name = "txtRsocialM"
        Me.txtRsocialM.ReadOnly = True
        Me.txtRsocialM.Size = New System.Drawing.Size(368, 22)
        Me.txtRsocialM.TabIndex = 81
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(103, 26)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(57, 14)
        Me.Label32.TabIndex = 80
        Me.Label32.Text = "Producto"
        '
        'txtskuOriginalM
        '
        Me.txtskuOriginalM.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtskuOriginalM.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtskuOriginalM.Location = New System.Drawing.Point(106, 43)
        Me.txtskuOriginalM.Name = "txtskuOriginalM"
        Me.txtskuOriginalM.Size = New System.Drawing.Size(87, 22)
        Me.txtskuOriginalM.TabIndex = 84
        '
        'rdbTodos
        '
        Me.rdbTodos.AutoSize = True
        Me.rdbTodos.Location = New System.Drawing.Point(173, 58)
        Me.rdbTodos.Name = "rdbTodos"
        Me.rdbTodos.Size = New System.Drawing.Size(167, 18)
        Me.rdbTodos.TabIndex = 79
        Me.rdbTodos.Text = "Modificar Varios Convenios"
        Me.rdbTodos.UseVisualStyleBackColor = True
        '
        'ProgressBarCargando
        '
        Me.ProgressBarCargando.Location = New System.Drawing.Point(582, 29)
        Me.ProgressBarCargando.Name = "ProgressBarCargando"
        Me.ProgressBarCargando.Size = New System.Drawing.Size(499, 23)
        Me.ProgressBarCargando.TabIndex = 81
        Me.ProgressBarCargando.Visible = False
        '
        'frmEditarConvenios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1088, 571)
        Me.Controls.Add(Me.ProgressBarCargando)
        Me.Controls.Add(Me.grbAcciones)
        Me.Controls.Add(Me.rdbTodos)
        Me.Controls.Add(Me.rdbCliente)
        Me.Controls.Add(Me.grbRut)
        Me.Controls.Add(Me.btn_Salir)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmEditarConvenios"
        Me.ShowIcon = False
        Me.Text = "Agregar, Modificar o Eliminar productos de convenios"
        Me.grbRut.ResumeLayout(False)
        Me.grbRut.PerformLayout()
        Me.grbAgregar.ResumeLayout(False)
        Me.grbAgregar.PerformLayout()
        CType(Me.dgvInput, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvProductosaAgregar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grbAcciones.ResumeLayout(False)
        Me.tabAcciones.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.grbModificar.ResumeLayout(False)
        Me.grbModificar.PerformLayout()
        CType(Me.dgvProductosModificar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        CType(Me.dgvProductoEliminar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage4.PerformLayout()
        CType(Me.dgvLineasReajuste, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvCambiaFechas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage5.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.dgvReemplazo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgvOriginal, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage6.ResumeLayout(False)
        Me.TabPage6.PerformLayout()
        CType(Me.dgvConReemM, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvConOriM, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btn_Salir As Button
    Friend WithEvents grbRut As GroupBox
    Friend WithEvents Label1 As Label
    Friend WithEvents txtRelCom As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents txtRazons As TextBox
    Friend WithEvents txtRutcli As TextBox
    Friend WithEvents grbAgregar As GroupBox
    Friend WithEvents btn_AgregaConvenio As Button
    Friend WithEvents dgvProductosaAgregar As DataGridView
    Friend WithEvents btn_QuitaAgrega As Button
    Friend WithEvents btn_AgregaProducto As Button
    Friend WithEvents Label24 As Label
    Friend WithEvents txtDescripcion As TextBox
    Friend WithEvents Label21 As Label
    Friend WithEvents txtCosto As TextBox
    Friend WithEvents Label20 As Label
    Friend WithEvents txtPrecio As TextBox
    Friend WithEvents Label23 As Label
    Friend WithEvents txtCodpro As TextBox
    Friend WithEvents rdbCliente As RadioButton
    Friend WithEvents grbAcciones As GroupBox
    Friend WithEvents grbModificar As GroupBox
    Friend WithEvents dgvProductosModificar As DataGridView
    Friend WithEvents dgvProductoEliminar As DataGridView
    Friend WithEvents btn_EliminardeConvenio As Button
    Friend WithEvents btn_ModificarConvenio As Button
    Friend WithEvents txtDescripcionModif As TextBox
    Friend WithEvents txtCodproModif As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents txtPrecioModif As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents txtCostoModif As TextBox
    Friend WithEvents btn_Cambio As Button
    Friend WithEvents dtpFecvenAgrega As DateTimePicker
    Friend WithEvents Label8 As Label
    Friend WithEvents tabAcciones As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents txtLinea As TextBox
    Friend WithEvents txtCodlinAgrega As TextBox
    Friend WithEvents TabPage4 As TabPage
    Friend WithEvents rdbTodos As RadioButton
    Friend WithEvents dgvCambiaFechas As DataGridView
    Friend WithEvents Label84 As Label
    Friend WithEvents dtpFecVenConvenio As DateTimePicker
    Friend WithEvents dtpFechaLineaReajuste As DateTimePicker
    Friend WithEvents btn_CambiaFechas As Button
    Friend WithEvents Label73 As Label
    Friend WithEvents dgvLineasReajuste As DataGridView
    Friend WithEvents btn_InputAgrega As Button
    Friend WithEvents Label85 As Label
    Friend WithEvents Label82 As Label
    Friend WithEvents dgvInput As DataGridView
    Friend WithEvents Label9 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents btn_CargaInputModifica As Button
    Friend WithEvents Label11 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents btn_InputElimina As Button
    Friend WithEvents Label13 As Label
    Friend WithEvents rdbDefinitivo As RadioButton
    Friend WithEvents rdbTemporal As RadioButton
    Friend WithEvents txtcoscom As TextBox
    Friend WithEvents Label14 As Label
    Friend WithEvents cmbTipoConvenio As ComboBox
    Friend WithEvents btnTipoConvenio As Button
    Friend WithEvents chkMarcaTodoConvenio As CheckBox
    Friend WithEvents txtSkuElimina As TextBox
    Friend WithEvents Label15 As Label
    Friend WithEvents TabPage5 As TabPage
    Friend WithEvents btnProcesar As Button
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents Label29 As Label
    Friend WithEvents txtSkuReem As TextBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label16 As Label
    Friend WithEvents txtDescripOri As TextBox
    Friend WithEvents Label19 As Label
    Friend WithEvents txtConvenioTotales As TextBox
    Friend WithEvents Label22 As Label
    Friend WithEvents txtSkuOri As TextBox
    Friend WithEvents ProgressBar1 As ProgressBar
    Friend WithEvents Label17 As Label
    Friend WithEvents txtLineaOri As TextBox
    Friend WithEvents Label18 As Label
    Friend WithEvents txtLineaReem As TextBox
    Friend WithEvents Label25 As Label
    Friend WithEvents txtDescripReem As TextBox
    Friend WithEvents Label26 As Label
    Friend WithEvents txtConvenioReem As TextBox
    Friend WithEvents btnxlsxReemplazo As Button
    Friend WithEvents dgvReemplazo As DataGridView
    Friend WithEvents btnxlsxOriginal As Button
    Friend WithEvents dgvOriginal As DataGridView
    Friend WithEvents ProgressBarCargando As ProgressBar
    Friend WithEvents txtSkuModifica As TextBox
    Friend WithEvents Label27 As Label
    Friend WithEvents rbtAgregar As RadioButton
    Friend WithEvents rbtReemplazar As RadioButton
    Friend WithEvents rbtQuitar As RadioButton
    Friend WithEvents dtpFecha As DateTimePicker
    Friend WithEvents cbxReversaReem As CheckBox
    Friend WithEvents TabPage6 As TabPage
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents btnProcesarManual As Button
    Friend WithEvents Label41 As Label
    Friend WithEvents dtpFechaManual As DateTimePicker
    Friend WithEvents rbtAgregarM As RadioButton
    Friend WithEvents rbtReemplazarM As RadioButton
    Friend WithEvents rbtQuitarM As RadioButton
    Friend WithEvents Label38 As Label
    Friend WithEvents txtLineaReemM As TextBox
    Friend WithEvents Label39 As Label
    Friend WithEvents txtdescripReemM As TextBox
    Friend WithEvents Label40 As Label
    Friend WithEvents txtSkuRemM As TextBox
    Friend WithEvents cbxReversaM As CheckBox
    Friend WithEvents Label28 As Label
    Friend WithEvents txtLineaM As TextBox
    Friend WithEvents Label30 As Label
    Friend WithEvents txtRsocialM As TextBox
    Friend WithEvents Label32 As Label
    Friend WithEvents txtskuOriginalM As TextBox
    Friend WithEvents Label33 As Label
    Friend WithEvents txtRutcliM As TextBox
    Friend WithEvents Label35 As Label
    Friend WithEvents txtFechaFinM As TextBox
    Friend WithEvents Label34 As Label
    Friend WithEvents txtFechaIniM As TextBox
    Friend WithEvents Label44 As Label
    Friend WithEvents txtDescripSkuM As TextBox
    Friend WithEvents Label43 As Label
    Friend WithEvents txtMargenM As TextBox
    Friend WithEvents Label42 As Label
    Friend WithEvents txtCostoM As TextBox
    Friend WithEvents Label36 As Label
    Friend WithEvents txtPrecioM As TextBox
    Friend WithEvents Label37 As Label
    Friend WithEvents Label31 As Label
    Friend WithEvents dgvConReemM As DataGridView
    Friend WithEvents dgvConOriM As DataGridView
End Class
