﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmProductoAlternativo
    Inherits MetroFramework.Forms.MetroForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btn_Excel = New System.Windows.Forms.Button()
        Me.btn_Salir = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtOriginal = New System.Windows.Forms.TextBox()
        Me.txtAlternativo = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dgvProductos = New System.Windows.Forms.DataGridView()
        Me.btn_Elimina = New System.Windows.Forms.Button()
        Me.btn_Modificar = New System.Windows.Forms.Button()
        Me.btn_Agregar = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtAhorro = New System.Windows.Forms.TextBox()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvProductos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.btn_Excel)
        Me.Panel1.Controls.Add(Me.btn_Salir)
        Me.Panel1.Location = New System.Drawing.Point(4, 54)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(900, 50)
        Me.Panel1.TabIndex = 0
        '
        'btn_Excel
        '
        Me.btn_Excel.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Excel.Image = Global.Convenios_New.My.Resources.Resources.Excel
        Me.btn_Excel.Location = New System.Drawing.Point(4, 4)
        Me.btn_Excel.Name = "btn_Excel"
        Me.btn_Excel.Size = New System.Drawing.Size(48, 41)
        Me.btn_Excel.TabIndex = 1
        Me.btn_Excel.UseVisualStyleBackColor = True
        '
        'btn_Salir
        '
        Me.btn_Salir.Image = Global.Convenios_New.My.Resources.Resources.salir
        Me.btn_Salir.Location = New System.Drawing.Point(853, 3)
        Me.btn_Salir.Name = "btn_Salir"
        Me.btn_Salir.Size = New System.Drawing.Size(42, 43)
        Me.btn_Salir.TabIndex = 0
        Me.btn_Salir.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 110)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(116, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Ingrese Código Original"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(163, 110)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(131, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Ingrese Código Alternativo"
        '
        'txtOriginal
        '
        Me.txtOriginal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtOriginal.Location = New System.Drawing.Point(15, 127)
        Me.txtOriginal.Name = "txtOriginal"
        Me.txtOriginal.Size = New System.Drawing.Size(113, 20)
        Me.txtOriginal.TabIndex = 2
        '
        'txtAlternativo
        '
        Me.txtAlternativo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAlternativo.Location = New System.Drawing.Point(166, 127)
        Me.txtAlternativo.Name = "txtAlternativo"
        Me.txtAlternativo.Size = New System.Drawing.Size(113, 20)
        Me.txtAlternativo.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(140, 128)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(16, 16)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "="
        '
        'dgvProductos
        '
        Me.dgvProductos.AllowUserToAddRows = False
        Me.dgvProductos.AllowUserToDeleteRows = False
        Me.dgvProductos.AllowUserToResizeRows = False
        Me.dgvProductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvProductos.Location = New System.Drawing.Point(15, 174)
        Me.dgvProductos.Name = "dgvProductos"
        Me.dgvProductos.ReadOnly = True
        Me.dgvProductos.RowHeadersVisible = False
        Me.dgvProductos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvProductos.Size = New System.Drawing.Size(937, 281)
        Me.dgvProductos.TabIndex = 4
        '
        'btn_Elimina
        '
        Me.btn_Elimina.Image = Global.Convenios_New.My.Resources.Resources.Delete
        Me.btn_Elimina.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_Elimina.Location = New System.Drawing.Point(830, 110)
        Me.btn_Elimina.Name = "btn_Elimina"
        Me.btn_Elimina.Size = New System.Drawing.Size(75, 37)
        Me.btn_Elimina.TabIndex = 5
        Me.btn_Elimina.Text = "Elimina"
        Me.btn_Elimina.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_Elimina.UseVisualStyleBackColor = True
        '
        'btn_Modificar
        '
        Me.btn_Modificar.Image = Global.Convenios_New.My.Resources.Resources.editar
        Me.btn_Modificar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_Modificar.Location = New System.Drawing.Point(728, 110)
        Me.btn_Modificar.Name = "btn_Modificar"
        Me.btn_Modificar.Size = New System.Drawing.Size(85, 37)
        Me.btn_Modificar.TabIndex = 3
        Me.btn_Modificar.Text = "Modificar"
        Me.btn_Modificar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_Modificar.UseVisualStyleBackColor = True
        '
        'btn_Agregar
        '
        Me.btn_Agregar.Image = Global.Convenios_New.My.Resources.Resources.mas
        Me.btn_Agregar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_Agregar.Location = New System.Drawing.Point(623, 110)
        Me.btn_Agregar.Name = "btn_Agregar"
        Me.btn_Agregar.Size = New System.Drawing.Size(86, 37)
        Me.btn_Agregar.TabIndex = 3
        Me.btn_Agregar.Text = "Agregar"
        Me.btn_Agregar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_Agregar.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(391, 110)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(107, 13)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "Porcentaje de Ahorro"
        '
        'txtAhorro
        '
        Me.txtAhorro.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAhorro.Location = New System.Drawing.Point(385, 127)
        Me.txtAhorro.MaxLength = 3
        Me.txtAhorro.Name = "txtAhorro"
        Me.txtAhorro.Size = New System.Drawing.Size(113, 20)
        Me.txtAhorro.TabIndex = 2
        '
        'frmProductoAlternativo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(964, 465)
        Me.ControlBox = False
        Me.Controls.Add(Me.btn_Elimina)
        Me.Controls.Add(Me.dgvProductos)
        Me.Controls.Add(Me.btn_Modificar)
        Me.Controls.Add(Me.btn_Agregar)
        Me.Controls.Add(Me.txtAhorro)
        Me.Controls.Add(Me.txtAlternativo)
        Me.Controls.Add(Me.txtOriginal)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Panel1)
        Me.KeyPreview = True
        Me.Name = "frmProductoAlternativo"
        Me.Text = "Mantención Productos Alternativos"
        Me.Panel1.ResumeLayout(False)
        CType(Me.dgvProductos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents btn_Salir As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents txtOriginal As TextBox
    Friend WithEvents txtAlternativo As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents btn_Agregar As Button
    Friend WithEvents btn_Modificar As Button
    Friend WithEvents dgvProductos As DataGridView
    Friend WithEvents btn_Excel As Button
    Friend WithEvents btn_Elimina As Button
    Friend WithEvents Label4 As Label
    Friend WithEvents txtAhorro As TextBox
End Class
