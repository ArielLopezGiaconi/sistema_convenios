﻿Imports System.Data.OracleClient
Public Class frmProductoAlternativo
    Dim bd As New Conexion
    Dim codigoalternativoamodificar As String
    Dim flag As Boolean = False
    Dim PuntoControl As New PuntoControl
    Private Sub btn_Salir_Click(sender As Object, e As EventArgs) Handles btn_Salir.Click
        Me.Close()
    End Sub

    Private Sub frmProductoAlternativo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.StartPosition = FormStartPosition.Manual
        Me.Location = New Point(100, 50)

        PuntoControl.RegistroUso("108")

        cargaGrilla()
    End Sub
    Private Sub cargaGrilla()
        Try
            bd.open_dimerc()
            flag = False
            Dim SQL = " select codpro ""Cod. Original"", despro ""Desc. Original"",GETESTADOPROD(codpro) ""Estado Original"", GETSTOCK(codpro) ""Stock Fisico Original"", "
            SQL = SQL & "codpromp ""Cod. Alternativo"", despromp ""Desc. Alternativo"",getestadoprod(codpromp) ""Estado Aternativo"", GETSTOCK(codpromp) ""Stock Fisico Alternativo"", nvl(ahorro,0) ""Porcentaje Ahorro"" "
            SQL = SQL & "from re_producto_alternativa order by codpro"
            dgvProductos.DataSource = bd.sqlSelect(SQL)
            dgvProductos.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            dgvProductos.Columns("Stock Fisico Original").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvProductos.Columns("Stock Fisico Original").DefaultCellStyle.Format = "n0"
            dgvProductos.Columns("Stock Fisico Alternativo").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvProductos.Columns("Stock Fisico Alternativo").DefaultCellStyle.Format = "n0"
            txtAlternativo.Text = ""
            txtOriginal.Text = ""
            flag = True
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub txtOriginal_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtOriginal.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If txtOriginal.Text <> "" Then
                txtAlternativo.Focus()
            End If
        End If
    End Sub

    Private Function validado(modificando As Boolean) As Boolean
        Try
            bd.open_dimerc()
            Dim sql = "select 1 from re_producto_alternativa where codpro='" + txtOriginal.Text + "' and codpromp='" + txtAlternativo.Text + "'"
            Dim dt = bd.sqlSelect(sql)
            If dt.Rows.Count > 0 And modificando = False Then
                MessageBox.Show("Relación de producto ya existe, puede modificarla o eliminarla", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return False
            Else
                sql = "select 1 from ma_product where codpro='" + txtOriginal.Text + "'"
                Dim dt2 = bd.sqlSelect(sql)
                If dt2.Rows.Count > 0 Then
                    sql = "select 1 from ma_product where codpro='" + txtAlternativo.Text + "'"
                    Dim dt3 = bd.sqlSelect(sql)
                    If dt3.Rows.Count > 0 Then
                        If Convert.ToDouble(txtAhorro.Text) < 0 Or Convert.ToDouble(txtAhorro.Text) > 100 Then
                            MessageBox.Show("Porcentaje de Ahorro debe ser mayor a 0 y menor a 100", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            Return False
                        Else
                            sql = "select GETCOSTOCOMERCIAL(3,'" + txtOriginal.Text + "') cosoriginal,GETCOSTOCOMERCIAL(3,'" + txtAlternativo.Text + "') cosalternativo from dual"
                            dt2.Clear()
                            dt2 = bd.sqlSelect(sql)
                            If dt2.Rows.Count > 0 Then
                                If dt2.Rows(0).Item("cosalternativo") > dt2.Rows(0).Item("cosoriginal") Then
                                    MessageBox.Show("Costo Alternativo no puede ser mayor a costo de producto original", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                    Return False
                                Else
                                    Return True
                                End If
                            End If
                        End If
                    Else
                        MessageBox.Show("Codigo Alternativo no existe", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Return False
                    End If
                Else
                    MessageBox.Show("Codigo Original no existe", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Return False
                End If

            End If
            Return False
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        Finally
            bd.close()
        End Try
    End Function

    Private Sub btn_Agregar_Click(sender As Object, e As EventArgs) Handles btn_Agregar.Click
        Try
            If validado(False) = True Then
                agrega()
                PuntoControl.RegistroUso("109")
            End If
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub agrega()
        Using conn = New OracleConnection(Conexion.retornaConexion)
            conn.Open()
            Dim comando As OracleCommand
            Dim sql As String
            Using transaccion = conn.BeginTransaction
                Try
                    sql = " insert into re_producto_alternativa (codpro,despro,codpromp,despromp,ahorro)  "
                    sql = sql & "values ('" + txtOriginal.Text + "',getdescripcion('" + txtOriginal.Text + "'),"
                    sql += "'" + txtAlternativo.Text + "',getdescripcion('" + txtAlternativo.Text + "')," + txtAhorro.Text + ") "
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()


                    transaccion.Commit()

                    MessageBox.Show("Datos agregados con éxito", "Listo", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    cargaGrilla()
                Catch ex As Exception
                    If Not transaccion.Equals(Nothing) Then
                        transaccion.Rollback()
                    End If
                    PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                    MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Finally
                    conn.Close()
                End Try
            End Using
        End Using
    End Sub

    Private Sub btn_Elimina_Click(sender As Object, e As EventArgs) Handles btn_Elimina.Click
        Using conn = New OracleConnection(Conexion.retornaConexion)
            conn.Open()
            Dim comando As OracleCommand
            Dim sql As String
            Using transaccion = conn.BeginTransaction
                Try
                    PuntoControl.RegistroUso("111")
                    sql = " delete from re_producto_alternativa "
                    sql = sql & "where codpro='" + txtOriginal.Text + "' "
                    sql = sql & "and codpromp='" + txtAlternativo.Text + "' "

                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()


                    transaccion.Commit()

                    MessageBox.Show("Datos eliminados con éxito", "Listo", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    cargaGrilla()
                Catch ex As Exception
                    If Not transaccion.Equals(Nothing) Then
                        transaccion.Rollback()
                    End If
                    PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                    MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Finally
                    conn.Close()
                End Try
            End Using
        End Using
    End Sub

    Private Sub btn_Modificar_Click(sender As Object, e As EventArgs) Handles btn_Modificar.Click
        If validado(True) = True Then
            PuntoControl.RegistroUso("110")
            Using conn = New OracleConnection(Conexion.retornaConexion)
                conn.Open()
                Dim comando As OracleCommand
                Dim sql As String
                Using transaccion = conn.BeginTransaction
                    Try
                        sql = " update re_producto_alternativa  "
                        sql = sql & " set codpromp='" + txtAlternativo.Text + "', "
                        sql = sql & " ahorro=" + txtAhorro.Text
                        sql = sql & " where codpro='" + txtOriginal.Text + "' "
                        sql = sql & " and codpromp='" + codigoalternativoamodificar + "' "


                        comando = New OracleCommand(sql, conn)
                        comando.Transaction = transaccion
                        comando.ExecuteNonQuery()


                        transaccion.Commit()

                        MessageBox.Show("Datos modificados con éxito", "Listo", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        cargaGrilla()
                    Catch ex As Exception
                        If Not transaccion.Equals(Nothing) Then
                            transaccion.Rollback()
                        End If
                        PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                        MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Finally
                        conn.Close()
                    End Try
                End Using
            End Using
        End If

    End Sub

    Private Sub dgvProductos_SelectionChanged(sender As Object, e As EventArgs) Handles dgvProductos.CurrentCellChanged
        If flag = True And Not dgvProductos.CurrentRow Is Nothing Then
            txtOriginal.Text = dgvProductos.Item("Cod. Original", dgvProductos.CurrentRow.Index).Value.ToString
            txtAlternativo.Text = dgvProductos.Item("Cod. Alternativo", dgvProductos.CurrentRow.Index).Value.ToString
            codigoalternativoamodificar = dgvProductos.Item("Cod. Alternativo", dgvProductos.CurrentRow.Index).Value.ToString
            txtAhorro.Text = dgvProductos.Item("Porcentaje Ahorro", dgvProductos.CurrentRow.Index).Value.ToString
        End If
    End Sub

    Private Sub txtOriginal_Click(sender As Object, e As EventArgs) Handles txtOriginal.Click
        txtOriginal.Text = ""
        txtAlternativo.Text = ""
    End Sub

    Private Sub btn_Excel_Click(sender As Object, e As EventArgs) Handles btn_Excel.Click
        Dim save As New SaveFileDialog
        If dgvProductos.RowCount = 0 Then
            MessageBox.Show("No hay datos consultados en la grilla, procese datos antes de exportar", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        Else
            save.Filter = "Archivo Excel | *.xlsx"
            If save.ShowDialog = System.Windows.Forms.DialogResult.OK Then
                Globales.exporta(save.FileName, dgvProductos)
            End If
        End If
    End Sub

    Private Sub txtAhorro_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtAhorro.KeyPress
        Globales.soloNumeros(sender, e)
    End Sub
End Class