﻿Imports System.Data.OracleClient

Public Class CargaAporteXdelta
    Dim PuntoControl As New PuntoControl
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim openFile As New OpenFileDialog
        Dim dtpaso As New DataTable()
        PuntoControl.RegistroUso("132")
        If openFile.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            Globales.Importar(dgvDatos, openFile.FileName)
            lblProceso.Text = "Cargando... "
            dgvDatos.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
        End If
        TabControl1.SelectedIndex = 0
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        grabar()
        PuntoControl.RegistroUso("133")
    End Sub

    Private Sub grabar()
        Using conn = New OracleConnection(Conexion.retornaConexion)
            conn.Open()
            Dim comando As OracleCommand
            Dim dataAdapter As OracleDataAdapter
            Dim dt As New DataTable
            Dim sql As String
            Using transaccion = conn.BeginTransaction
                Try

                    For i = 0 To dgvDatos.RowCount - 1
                        lblProceso.Text = "Procesados " + (i + 1).ToString + " de " + dgvDatos.RowCount.ToString + " Registros"
                        Application.DoEvents()

                        'VALIDO EL CODIGO
                        sql = "SELECT codpro FROM ma_product "
                        sql = sql & " WHERE codpro = '" & Trim(dgvDatos.Item("CODPRO", i).Value) & "'"
                        dt.Clear()
                        comando = New OracleCommand(sql, conn)
                        comando.Transaction = transaccion
                        dataAdapter = New OracleDataAdapter(comando)
                        dataAdapter.Fill(dt)

                        If dt.Rows.Count > 0 Then
                            sql = "select 1 ma_prod_aporte WHERE codpro = '" & Trim(dgvDatos.Item("CODPRO", i).Value) & "'"
                            dt.Clear()
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            dataAdapter = New OracleDataAdapter(comando)
                            dataAdapter.Fill(dt)

                            If dt.Rows.Count > 0 Then
                                sql = "update ma_prod_aporte set aporte = " & Replace(Trim(dgvDatos.Item("APORTE", i).Value), ",", ".") & "
                                   WHERE CODPRO = '" & Trim(dgvDatos.Item("CODPRO", i).Value) & "'"
                                comando = New OracleCommand(sql, conn)
                                comando.Transaction = transaccion
                                comando.ExecuteNonQuery()
                            Else
                                sql = "insert into ma_prod_aporte (codpro, aporte) values ('" & Trim(dgvDatos.Item("CODPRO", i).Value) & "', " & Replace(Trim(dgvDatos.Item("APORTE", i).Value), ",", ".") & ")"
                                comando = New OracleCommand(sql, conn)
                                comando.Transaction = transaccion
                                comando.ExecuteNonQuery()
                            End If

                            sql = "update ma_product set xdelta = " & Trim(dgvDatos.Item("DELTA", i).Value) & "
                                   WHERE CODPRO = '" & Trim(dgvDatos.Item("CODPRO", i).Value) & "'"
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()
                        End If

                    Next

                Catch ex As Exception
                    If Not transaccion.Equals(Nothing) Then
                        transaccion.Rollback()
                    End If
                    PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                    MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Finally
                    lblTotales.Text = "Total Registros : " + dgvDatos.RowCount.ToString
                    conn.Close()
                End Try
            End Using
        End Using
    End Sub

    Private Sub CargaAporteXdelta_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        PuntoControl.RegistroUso("131")
    End Sub
End Class