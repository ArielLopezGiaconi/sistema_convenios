﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCostoFuturo
    Inherits MetroFramework.Forms.MetroForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btn_Excel = New System.Windows.Forms.Button()
        Me.btn_Salir = New System.Windows.Forms.Button()
        Me.dgvProductos = New System.Windows.Forms.DataGridView()
        Me.btn_Elimina = New System.Windows.Forms.Button()
        Me.btn_Masivo = New System.Windows.Forms.Button()
        Me.btn_Modificar = New System.Windows.Forms.Button()
        Me.btn_Agregar = New System.Windows.Forms.Button()
        Me.txtCosto = New System.Windows.Forms.TextBox()
        Me.txtCodpro = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtDescripcion = New System.Windows.Forms.TextBox()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.btnProcesar = New System.Windows.Forms.Button()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblSeleccion = New System.Windows.Forms.Label()
        Me.lblTotales = New System.Windows.Forms.Label()
        Me.lblErrores = New System.Windows.Forms.Label()
        Me.dgvPifias = New System.Windows.Forms.DataGridView()
        Me.dgvDatos = New System.Windows.Forms.DataGridView()
        Me.btn_Errores = New System.Windows.Forms.Button()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.dtpFecini = New System.Windows.Forms.DateTimePicker()
        Me.dtpFecFin = New System.Windows.Forms.DateTimePicker()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvProductos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        CType(Me.dgvPifias, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvDatos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.btn_Excel)
        Me.Panel1.Controls.Add(Me.btn_Salir)
        Me.Panel1.Location = New System.Drawing.Point(12, 53)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1021, 50)
        Me.Panel1.TabIndex = 1
        '
        'btn_Excel
        '
        Me.btn_Excel.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Excel.Image = Global.Convenios_New.My.Resources.Resources.Excel
        Me.btn_Excel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_Excel.Location = New System.Drawing.Point(3, 3)
        Me.btn_Excel.Name = "btn_Excel"
        Me.btn_Excel.Size = New System.Drawing.Size(100, 41)
        Me.btn_Excel.TabIndex = 1
        Me.btn_Excel.Text = "Exportar"
        Me.btn_Excel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_Excel.UseVisualStyleBackColor = True
        '
        'btn_Salir
        '
        Me.btn_Salir.Image = Global.Convenios_New.My.Resources.Resources.salir
        Me.btn_Salir.Location = New System.Drawing.Point(974, 3)
        Me.btn_Salir.Name = "btn_Salir"
        Me.btn_Salir.Size = New System.Drawing.Size(42, 43)
        Me.btn_Salir.TabIndex = 0
        Me.btn_Salir.UseVisualStyleBackColor = True
        '
        'dgvProductos
        '
        Me.dgvProductos.AllowUserToAddRows = False
        Me.dgvProductos.AllowUserToDeleteRows = False
        Me.dgvProductos.AllowUserToResizeRows = False
        Me.dgvProductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvProductos.Location = New System.Drawing.Point(3, 6)
        Me.dgvProductos.Name = "dgvProductos"
        Me.dgvProductos.ReadOnly = True
        Me.dgvProductos.RowHeadersVisible = False
        Me.dgvProductos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvProductos.Size = New System.Drawing.Size(991, 276)
        Me.dgvProductos.TabIndex = 5
        '
        'btn_Elimina
        '
        Me.btn_Elimina.Image = Global.Convenios_New.My.Resources.Resources.Delete
        Me.btn_Elimina.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_Elimina.Location = New System.Drawing.Point(12, 109)
        Me.btn_Elimina.Name = "btn_Elimina"
        Me.btn_Elimina.Size = New System.Drawing.Size(90, 46)
        Me.btn_Elimina.TabIndex = 7
        Me.btn_Elimina.Text = "Eliminar Marcados"
        Me.btn_Elimina.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_Elimina.UseVisualStyleBackColor = True
        '
        'btn_Masivo
        '
        Me.btn_Masivo.Image = Global.Convenios_New.My.Resources.Resources.Excel
        Me.btn_Masivo.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_Masivo.Location = New System.Drawing.Point(763, 10)
        Me.btn_Masivo.Name = "btn_Masivo"
        Me.btn_Masivo.Size = New System.Drawing.Size(100, 46)
        Me.btn_Masivo.TabIndex = 8
        Me.btn_Masivo.Text = "Importar"
        Me.btn_Masivo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_Masivo.UseVisualStyleBackColor = True
        '
        'btn_Modificar
        '
        Me.btn_Modificar.Image = Global.Convenios_New.My.Resources.Resources.editar
        Me.btn_Modificar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_Modificar.Location = New System.Drawing.Point(948, 114)
        Me.btn_Modificar.Name = "btn_Modificar"
        Me.btn_Modificar.Size = New System.Drawing.Size(85, 37)
        Me.btn_Modificar.TabIndex = 13
        Me.btn_Modificar.Text = "Modificar"
        Me.btn_Modificar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_Modificar.UseVisualStyleBackColor = True
        Me.btn_Modificar.Visible = False
        '
        'btn_Agregar
        '
        Me.btn_Agregar.Image = Global.Convenios_New.My.Resources.Resources.mas
        Me.btn_Agregar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_Agregar.Location = New System.Drawing.Point(856, 114)
        Me.btn_Agregar.Name = "btn_Agregar"
        Me.btn_Agregar.Size = New System.Drawing.Size(86, 37)
        Me.btn_Agregar.TabIndex = 14
        Me.btn_Agregar.Text = "Agregar"
        Me.btn_Agregar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_Agregar.UseVisualStyleBackColor = True
        '
        'txtCosto
        '
        Me.txtCosto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCosto.Location = New System.Drawing.Point(570, 130)
        Me.txtCosto.MaxLength = 20
        Me.txtCosto.Name = "txtCosto"
        Me.txtCosto.Size = New System.Drawing.Size(78, 21)
        Me.txtCosto.TabIndex = 11
        '
        'txtCodpro
        '
        Me.txtCodpro.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodpro.Location = New System.Drawing.Point(116, 130)
        Me.txtCodpro.Name = "txtCodpro"
        Me.txtCodpro.Size = New System.Drawing.Size(91, 21)
        Me.txtCodpro.TabIndex = 12
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(567, 114)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(82, 13)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "Porcentaje Alza"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(149, 114)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(40, 13)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Código"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(354, 114)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(61, 13)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "Descripción"
        '
        'txtDescripcion
        '
        Me.txtDescripcion.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripcion.Location = New System.Drawing.Point(213, 130)
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(351, 21)
        Me.txtDescripcion.TabIndex = 12
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Location = New System.Drawing.Point(12, 162)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1008, 399)
        Me.TabControl1.TabIndex = 15
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.dgvProductos)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1000, 373)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Consulta"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.btnProcesar)
        Me.TabPage2.Controls.Add(Me.Label10)
        Me.TabPage2.Controls.Add(Me.Label7)
        Me.TabPage2.Controls.Add(Me.Label6)
        Me.TabPage2.Controls.Add(Me.Label5)
        Me.TabPage2.Controls.Add(Me.Label3)
        Me.TabPage2.Controls.Add(Me.lblSeleccion)
        Me.TabPage2.Controls.Add(Me.lblTotales)
        Me.TabPage2.Controls.Add(Me.lblErrores)
        Me.TabPage2.Controls.Add(Me.dgvPifias)
        Me.TabPage2.Controls.Add(Me.dgvDatos)
        Me.TabPage2.Controls.Add(Me.btn_Errores)
        Me.TabPage2.Controls.Add(Me.btn_Masivo)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1000, 373)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Vista Carga Masiva"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'btnProcesar
        '
        Me.btnProcesar.Image = Global.Convenios_New.My.Resources.Resources.Floppy
        Me.btnProcesar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnProcesar.Location = New System.Drawing.Point(869, 12)
        Me.btnProcesar.Name = "btnProcesar"
        Me.btnProcesar.Size = New System.Drawing.Size(93, 42)
        Me.btnProcesar.TabIndex = 39
        Me.btnProcesar.Text = "Procesar"
        Me.btnProcesar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnProcesar.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(215, 57)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(518, 14)
        Me.Label10.TabIndex = 38
        Me.Label10.Text = "El programa AGREGARA los productos que no esten registrados EN LAS FECHAS AGREGAD" &
    "AS"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(279, 73)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(379, 14)
        Me.Label7.TabIndex = 38
        Me.Label7.Text = "Si existen fechas que ya estan registradas, NO SERAN GUARDADAS."
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(340, 43)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(271, 14)
        Me.Label6.TabIndex = 38
        Me.Label6.Text = "Nota: El archivo puede ser de formato xls o xlsx." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Blue
        Me.Label5.Location = New System.Drawing.Point(233, 29)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(482, 14)
        Me.Label5.TabIndex = 36
        Me.Label5.Text = "CODPRO, ALZA (Como Número), FECINI (DD/MM/YYYY), FECFIN(DD/MM/YYYY)"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(279, 12)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(404, 14)
        Me.Label3.TabIndex = 37
        Me.Label3.Text = "El Formato del Archivo debe contener los siguientes titulos de columnas."
        '
        'lblSeleccion
        '
        Me.lblSeleccion.AutoSize = True
        Me.lblSeleccion.Location = New System.Drawing.Point(6, 92)
        Me.lblSeleccion.Name = "lblSeleccion"
        Me.lblSeleccion.Size = New System.Drawing.Size(0, 13)
        Me.lblSeleccion.TabIndex = 33
        '
        'lblTotales
        '
        Me.lblTotales.AutoSize = True
        Me.lblTotales.Location = New System.Drawing.Point(6, 353)
        Me.lblTotales.Name = "lblTotales"
        Me.lblTotales.Size = New System.Drawing.Size(98, 13)
        Me.lblTotales.TabIndex = 34
        Me.lblTotales.Text = "Total de Registros:"
        '
        'lblErrores
        '
        Me.lblErrores.AutoSize = True
        Me.lblErrores.Location = New System.Drawing.Point(480, 353)
        Me.lblErrores.Name = "lblErrores"
        Me.lblErrores.Size = New System.Drawing.Size(88, 13)
        Me.lblErrores.TabIndex = 35
        Me.lblErrores.Text = "Total de Errores:"
        '
        'dgvPifias
        '
        Me.dgvPifias.AllowUserToAddRows = False
        Me.dgvPifias.AllowUserToDeleteRows = False
        Me.dgvPifias.AllowUserToResizeColumns = False
        Me.dgvPifias.AllowUserToResizeRows = False
        Me.dgvPifias.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPifias.Location = New System.Drawing.Point(483, 109)
        Me.dgvPifias.Name = "dgvPifias"
        Me.dgvPifias.ReadOnly = True
        Me.dgvPifias.RowHeadersVisible = False
        Me.dgvPifias.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPifias.Size = New System.Drawing.Size(479, 241)
        Me.dgvPifias.TabIndex = 32
        '
        'dgvDatos
        '
        Me.dgvDatos.AllowUserToAddRows = False
        Me.dgvDatos.AllowUserToDeleteRows = False
        Me.dgvDatos.AllowUserToResizeColumns = False
        Me.dgvDatos.AllowUserToResizeRows = False
        Me.dgvDatos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDatos.Location = New System.Drawing.Point(6, 109)
        Me.dgvDatos.Name = "dgvDatos"
        Me.dgvDatos.ReadOnly = True
        Me.dgvDatos.RowHeadersVisible = False
        Me.dgvDatos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDatos.Size = New System.Drawing.Size(463, 241)
        Me.dgvDatos.TabIndex = 31
        '
        'btn_Errores
        '
        Me.btn_Errores.AccessibleRole = System.Windows.Forms.AccessibleRole.TitleBar
        Me.btn_Errores.Image = Global.Convenios_New.My.Resources.Resources.Excel
        Me.btn_Errores.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_Errores.Location = New System.Drawing.Point(827, 57)
        Me.btn_Errores.Name = "btn_Errores"
        Me.btn_Errores.Size = New System.Drawing.Size(135, 46)
        Me.btn_Errores.TabIndex = 8
        Me.btn_Errores.Text = "Exportar Errores"
        Me.btn_Errores.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_Errores.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(667, 114)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(64, 13)
        Me.Label8.TabIndex = 9
        Me.Label8.Text = "Fecha Inicio"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(760, 114)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(77, 13)
        Me.Label9.TabIndex = 9
        Me.Label9.Text = "Fecha Termino"
        '
        'dtpFecini
        '
        Me.dtpFecini.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecini.Location = New System.Drawing.Point(654, 130)
        Me.dtpFecini.Name = "dtpFecini"
        Me.dtpFecini.Size = New System.Drawing.Size(95, 21)
        Me.dtpFecini.TabIndex = 16
        '
        'dtpFecFin
        '
        Me.dtpFecFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecFin.Location = New System.Drawing.Point(755, 130)
        Me.dtpFecFin.Name = "dtpFecFin"
        Me.dtpFecFin.Size = New System.Drawing.Size(95, 21)
        Me.dtpFecFin.TabIndex = 16
        '
        'frmCostoFuturo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1054, 568)
        Me.Controls.Add(Me.dtpFecFin)
        Me.Controls.Add(Me.dtpFecini)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.btn_Modificar)
        Me.Controls.Add(Me.btn_Agregar)
        Me.Controls.Add(Me.txtCosto)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Controls.Add(Me.txtCodpro)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btn_Elimina)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmCostoFuturo"
        Me.ShowIcon = False
        Me.Text = "Mantanción de Costo Futuro de Productos"
        Me.Panel1.ResumeLayout(False)
        CType(Me.dgvProductos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        CType(Me.dgvPifias, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvDatos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents btn_Excel As Button
    Friend WithEvents btn_Salir As Button
    Friend WithEvents dgvProductos As DataGridView
    Friend WithEvents btn_Elimina As Button
    Friend WithEvents btn_Masivo As Button
    Friend WithEvents btn_Modificar As Button
    Friend WithEvents btn_Agregar As Button
    Friend WithEvents txtCosto As TextBox
    Friend WithEvents txtCodpro As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents txtDescripcion As TextBox
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents lblSeleccion As Label
    Friend WithEvents lblTotales As Label
    Friend WithEvents lblErrores As Label
    Friend WithEvents dgvPifias As DataGridView
    Friend WithEvents dgvDatos As DataGridView
    Friend WithEvents btnProcesar As Button
    Friend WithEvents Label7 As Label
    Friend WithEvents btn_Errores As Button
    Friend WithEvents Label8 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents dtpFecini As DateTimePicker
    Friend WithEvents dtpFecFin As DateTimePicker
    Friend WithEvents Label10 As Label
End Class
