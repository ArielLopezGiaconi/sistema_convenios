﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCostoEspecial
    Inherits MetroFramework.Forms.MetroForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dgvCosto = New System.Windows.Forms.DataGridView()
        Me.ROWID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CODPRO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DESCRIP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COSTOESP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PRECIO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MARGEN = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COSPROM = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COSCOM = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TIPO_COSTO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FECINI = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FECFIN = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.USR_CREAC = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.accion = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.txtRutCli = New System.Windows.Forms.TextBox()
        Me.lblRutcli = New System.Windows.Forms.Label()
        Me.btnAgregaCosto = New System.Windows.Forms.Button()
        Me.dtFecIni = New System.Windows.Forms.DateTimePicker()
        Me.txtCodpro = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblNumrel = New System.Windows.Forms.Label()
        Me.btnExportar = New System.Windows.Forms.Button()
        Me.lblDespro = New System.Windows.Forms.Label()
        CType(Me.dgvCosto, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvCosto
        '
        Me.dgvCosto.AllowUserToAddRows = False
        Me.dgvCosto.AllowUserToDeleteRows = False
        Me.dgvCosto.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvCosto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCosto.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ROWID, Me.ID, Me.CODPRO, Me.DESCRIP, Me.COSTOESP, Me.PRECIO, Me.MARGEN, Me.COSPROM, Me.COSCOM, Me.TIPO_COSTO, Me.FECINI, Me.FECFIN, Me.USR_CREAC, Me.accion})
        Me.dgvCosto.Location = New System.Drawing.Point(11, 105)
        Me.dgvCosto.Name = "dgvCosto"
        Me.dgvCosto.RowHeadersVisible = False
        Me.dgvCosto.Size = New System.Drawing.Size(843, 374)
        Me.dgvCosto.TabIndex = 1
        '
        'ROWID
        '
        Me.ROWID.HeaderText = "ROWID"
        Me.ROWID.Name = "ROWID"
        Me.ROWID.Visible = False
        '
        'ID
        '
        Me.ID.HeaderText = "ID"
        Me.ID.Name = "ID"
        '
        'CODPRO
        '
        Me.CODPRO.HeaderText = "CODPRO"
        Me.CODPRO.Name = "CODPRO"
        '
        'DESCRIP
        '
        Me.DESCRIP.HeaderText = "DESCRIP"
        Me.DESCRIP.Name = "DESCRIP"
        '
        'COSTOESP
        '
        Me.COSTOESP.HeaderText = "COSTOESP"
        Me.COSTOESP.Name = "COSTOESP"
        '
        'PRECIO
        '
        Me.PRECIO.HeaderText = "PRECIO"
        Me.PRECIO.Name = "PRECIO"
        '
        'MARGEN
        '
        Me.MARGEN.HeaderText = "MARGEN"
        Me.MARGEN.Name = "MARGEN"
        '
        'COSPROM
        '
        Me.COSPROM.HeaderText = "COSPROM"
        Me.COSPROM.Name = "COSPROM"
        '
        'COSCOM
        '
        Me.COSCOM.HeaderText = "COSCOM"
        Me.COSCOM.Name = "COSCOM"
        '
        'TIPO_COSTO
        '
        Me.TIPO_COSTO.HeaderText = "TIPO_COSTO"
        Me.TIPO_COSTO.Name = "TIPO_COSTO"
        '
        'FECINI
        '
        Me.FECINI.HeaderText = "FECINI"
        Me.FECINI.Name = "FECINI"
        '
        'FECFIN
        '
        Me.FECFIN.HeaderText = "FECFIN"
        Me.FECFIN.Name = "FECFIN"
        '
        'USR_CREAC
        '
        Me.USR_CREAC.HeaderText = "USR_CREAC"
        Me.USR_CREAC.Name = "USR_CREAC"
        '
        'accion
        '
        Me.accion.HeaderText = "ACCION"
        Me.accion.Name = "accion"
        Me.accion.ReadOnly = True
        '
        'txtRutCli
        '
        Me.txtRutCli.Location = New System.Drawing.Point(54, 53)
        Me.txtRutCli.Name = "txtRutCli"
        Me.txtRutCli.Size = New System.Drawing.Size(100, 20)
        Me.txtRutCli.TabIndex = 2
        '
        'lblRutcli
        '
        Me.lblRutcli.AutoSize = True
        Me.lblRutcli.Location = New System.Drawing.Point(6, 57)
        Me.lblRutcli.Name = "lblRutcli"
        Me.lblRutcli.Size = New System.Drawing.Size(30, 13)
        Me.lblRutcli.TabIndex = 3
        Me.lblRutcli.Text = "RUT"
        '
        'btnAgregaCosto
        '
        Me.btnAgregaCosto.Location = New System.Drawing.Point(304, 485)
        Me.btnAgregaCosto.Name = "btnAgregaCosto"
        Me.btnAgregaCosto.Size = New System.Drawing.Size(115, 23)
        Me.btnAgregaCosto.TabIndex = 4
        Me.btnAgregaCosto.Text = "Crear Costo Especial"
        Me.btnAgregaCosto.UseVisualStyleBackColor = True
        '
        'dtFecIni
        '
        Me.dtFecIni.Location = New System.Drawing.Point(54, 76)
        Me.dtFecIni.Name = "dtFecIni"
        Me.dtFecIni.Size = New System.Drawing.Size(200, 20)
        Me.dtFecIni.TabIndex = 5
        '
        'txtCodpro
        '
        Me.txtCodpro.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodpro.Location = New System.Drawing.Point(319, 76)
        Me.txtCodpro.Name = "txtCodpro"
        Me.txtCodpro.Size = New System.Drawing.Size(100, 20)
        Me.txtCodpro.TabIndex = 7
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(262, 80)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(51, 13)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "BUSCAR"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 76)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(42, 13)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "FECHA"
        '
        'lblNumrel
        '
        Me.lblNumrel.AutoSize = True
        Me.lblNumrel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNumrel.Location = New System.Drawing.Point(184, 56)
        Me.lblNumrel.Name = "lblNumrel"
        Me.lblNumrel.Size = New System.Drawing.Size(2, 15)
        Me.lblNumrel.TabIndex = 12
        '
        'btnExportar
        '
        Me.btnExportar.Location = New System.Drawing.Point(462, 485)
        Me.btnExportar.Name = "btnExportar"
        Me.btnExportar.Size = New System.Drawing.Size(100, 23)
        Me.btnExportar.TabIndex = 13
        Me.btnExportar.Text = "Exportar a Excel"
        Me.btnExportar.UseVisualStyleBackColor = True
        '
        'lblDespro
        '
        Me.lblDespro.AutoSize = True
        Me.lblDespro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblDespro.Location = New System.Drawing.Point(425, 79)
        Me.lblDespro.Name = "lblDespro"
        Me.lblDespro.Size = New System.Drawing.Size(2, 15)
        Me.lblDespro.TabIndex = 14
        '
        'frmCostoEspecial
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(865, 513)
        Me.Controls.Add(Me.lblDespro)
        Me.Controls.Add(Me.btnExportar)
        Me.Controls.Add(Me.lblNumrel)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtCodpro)
        Me.Controls.Add(Me.dtFecIni)
        Me.Controls.Add(Me.btnAgregaCosto)
        Me.Controls.Add(Me.lblRutcli)
        Me.Controls.Add(Me.txtRutCli)
        Me.Controls.Add(Me.dgvCosto)
        Me.Name = "frmCostoEspecial"
        Me.ShowIcon = False
        Me.Text = "Costos Especiales"
        CType(Me.dgvCosto, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents dgvCosto As DataGridView
    Friend WithEvents txtRutCli As TextBox
    Friend WithEvents lblRutcli As Label
    Friend WithEvents btnAgregaCosto As Button
    Friend WithEvents dtFecIni As DateTimePicker
    Friend WithEvents txtCodpro As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents lblNumrel As Label
    Friend WithEvents btnExportar As Button
    Friend WithEvents ROWID As DataGridViewTextBoxColumn
    Friend WithEvents ID As DataGridViewTextBoxColumn
    Friend WithEvents CODPRO As DataGridViewTextBoxColumn
    Friend WithEvents DESCRIP As DataGridViewTextBoxColumn
    Friend WithEvents COSTOESP As DataGridViewTextBoxColumn
    Friend WithEvents PRECIO As DataGridViewTextBoxColumn
    Friend WithEvents MARGEN As DataGridViewTextBoxColumn
    Friend WithEvents COSPROM As DataGridViewTextBoxColumn
    Friend WithEvents COSCOM As DataGridViewTextBoxColumn
    Friend WithEvents TIPO_COSTO As DataGridViewTextBoxColumn
    Friend WithEvents FECINI As DataGridViewTextBoxColumn
    Friend WithEvents FECFIN As DataGridViewTextBoxColumn
    Friend WithEvents USR_CREAC As DataGridViewTextBoxColumn
    Friend WithEvents accion As DataGridViewButtonColumn
    Friend WithEvents lblDespro As Label
End Class
