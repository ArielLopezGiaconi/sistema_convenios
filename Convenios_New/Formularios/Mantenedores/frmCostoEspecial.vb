﻿Public Class frmCostoEspecial
    Dim tablazo As DataTable
    Dim PuntoControl As New PuntoControl

    Private Sub txtRut_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtRutCli.KeyPress
        Globales.solonumerosyEnter(e)
        If e.KeyChar = Convert.ToChar(Keys.Enter) And txtRutCli.Text.Length > 0 Then
            cargaDatos()
            PuntoControl.RegistroUso("149")
        End If
    End Sub

    Private Sub btnAgregaCosto_Click(sender As Object, e As EventArgs) Handles btnAgregaCosto.Click
        PuntoControl.RegistroUso("147")
        Dim frmCostoEsp As New creaCostoEsp()
        frmCostoEsp.txtRutcli.Text = txtRutCli.Text
        frmCostoEsp.lblRazons.Text = lblNumrel.Text
        frmCostoEsp.ShowDialog()
        'cargaDatos()
    End Sub

    Private Sub cargaDatos()
        Dim datos As New Conexion
        Dim sql As String
        datos.open_dimerc()
        lblNumrel.Text = datos.sqlSelectUnico("SELECT RAZONS FROM EN_CLIENTE WHERE CODEMP = 3 AND RUTCLI = " & txtRutCli.Text, "RAZONS") & ", NUMREL:" &
                         datos.sqlSelectUnico("SELECT SAP_GET_NUMREL(CODEMP, RUTCLI) NUMREL FROM EN_CLIENTE WHERE CODEMP = 3 AND RUTCLI = " & txtRutCli.Text, "NUMREL")
        sql = "SELECT ROWID, ID_NUMERO, CODPRO,SAP_GET_DESCRIPCION(CODPRO) DESCRIP , COSTO COSTOESP, MARGEN, SAP_GET_COSPROM_UNICO(CODPRO) COSPROM,
                   SAP_GET_COSTOCOMERCIAL(CODEMP,1,CODPRO) COSCOM, PRECIO, SAP_GET_DOMINIO(401,TIPO) TIPO_COSTO,
                   FECINI, FECFIN, USR_CREAC
                   FROM EN_CLIENTE_COSTO
                   WHERE to_date('" & dtFecIni.Value.ToShortDateString & "','dd/mm/yyyy')
                   between fecini and fecfin
                   AND RUTCLI = " & txtRutCli.Text

        tablazo = datos.sqlSelect(sql)
        dgvCosto.Rows.Clear()
        For Each fila As DataRow In tablazo.Rows
            dgvCosto.Rows.Add(fila("ROWID").ToString(), fila("ID_NUMERO").ToString(), fila("CODPRO").ToString(), fila("DESCRIP").ToString(),
                              fila("COSTOESP").ToString(), fila("PRECIO").ToString(), fila("MARGEN").ToString(),
                              fila("COSPROM").ToString(), fila("COSCOM").ToString(),
                              fila("TIPO_COSTO").ToString(), fila("FECINI").ToString(), fila("FECFIN").ToString(), fila("USR_CREAC").ToString(), "Caducar")
        Next
        datos.close()
    End Sub

    Private Sub TextBox1_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCodpro.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) And txtRutCli.Text.Length > 0 Then
            Dim datos As New Conexion
            Dim sql As String
            sql = "SELECT SAP_GET_DESCRIPCION('" & txtCodpro.Text & "') DESPRO FROM DUAL"
            datos.open_dimerc()
            lblDespro.Text = datos.sqlSelectUnico(sql, "DESPRO")
            dgvCosto.Rows.Clear()
            For Each fila As DataRow In tablazo.Rows
                If fila("CODPRO").ToString.Equals(txtCodpro.Text) Then
                    dgvCosto.Rows.Add(fila("ROWID").ToString(), fila("ID_NUMERO").ToString(), fila("CODPRO").ToString(), fila("DESCRIP").ToString(),
                              fila("COSTOESP").ToString(), fila("PRECIO").ToString(), fila("MARGEN").ToString(),
                              fila("COSPROM").ToString(), fila("COSCOM").ToString(),
                              fila("TIPO_COSTO").ToString(), fila("FECINI").ToString(), fila("FECFIN").ToString(), fila("USR_CREAC").ToString(), "Caducar")
                End If
            Next
        End If
    End Sub

    Private Sub btnCargar_Click(sender As Object, e As EventArgs)
        cargaDatos()
    End Sub

    Private Sub dgvCosto_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvCosto.CellClick
        Dim datos As New Conexion
        If sender.GetType.Equals(GetType(DataGridView)) Then
            If CType(sender, DataGridView).CurrentCell.GetType.Equals(GetType(DataGridViewButtonCell)) Then
                Dim fila = CType(sender, DataGridView).CurrentRow
                datos.open_dimerc()
                datos.ejecutar("UPDATE EN_CLIENTE_COSTO SET ESTADO = 'C',
                                fecfin = trunc(sysdate) - 1
                                WHERE ROWID ='" & fila.Cells.Item("ROWID").Value.ToString() & "'")
                datos.close()
                cargaDatos()
            End If
        End If
    End Sub

    Private Sub btnExportar_Click(sender As Object, e As EventArgs) Handles btnExportar.Click
        Globales.exporta("costosEspeciales", dgvCosto)
        PuntoControl.RegistroUso("140")
    End Sub

    Private Sub frmCostoEspecial_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        txtRutCli.Select()
    End Sub

    Private Sub frmCostoEspecial_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        PuntoControl.RegistroUso("146")
        Me.Location = New Point(0, 0)
    End Sub
End Class