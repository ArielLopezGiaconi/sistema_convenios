﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AtribTissue
    Inherits MetroFramework.Forms.MetroForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.txtCodpro = New System.Windows.Forms.TextBox()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtAltura = New System.Windows.Forms.TextBox()
        Me.txtLongitud = New System.Windows.Forms.TextBox()
        Me.txtEAN13 = New System.Windows.Forms.TextBox()
        Me.txtPeso = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.rbGofradoNo = New System.Windows.Forms.RadioButton()
        Me.rbGofradoSi = New System.Windows.Forms.RadioButton()
        Me.grpControl = New System.Windows.Forms.GroupBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtHojas = New System.Windows.Forms.TextBox()
        Me.txtPresentacion = New System.Windows.Forms.TextBox()
        Me.grpEmbalaje = New System.Windows.Forms.GroupBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtDUN = New System.Windows.Forms.TextBox()
        Me.txtPaletizado = New System.Windows.Forms.TextBox()
        Me.txtBulto = New System.Windows.Forms.TextBox()
        Me.txtPaquete = New System.Windows.Forms.TextBox()
        Me.grpAlterDisp = New System.Windows.Forms.GroupBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtDispensador = New System.Windows.Forms.TextBox()
        Me.txtAlternativa = New System.Windows.Forms.TextBox()
        Me.btnAgregaDispensador = New System.Windows.Forms.Button()
        Me.btnAgregaAlternativa = New System.Windows.Forms.Button()
        Me.lbDispensadores = New System.Windows.Forms.ListBox()
        Me.lbAlternativas = New System.Windows.Forms.ListBox()
        Me.btnExpAtr = New System.Windows.Forms.Button()
        Me.btnExpRel = New System.Windows.Forms.Button()
        Me.btnImpAtr = New System.Windows.Forms.Button()
        Me.btnImpRel = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.grpControl.SuspendLayout()
        Me.grpEmbalaje.SuspendLayout()
        Me.grpAlterDisp.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnGuardar
        '
        Me.btnGuardar.Enabled = False
        Me.btnGuardar.Location = New System.Drawing.Point(35, 476)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 0
        Me.btnGuardar.Text = "Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'txtCodpro
        '
        Me.txtCodpro.AcceptsTab = True
        Me.txtCodpro.Location = New System.Drawing.Point(82, 74)
        Me.txtCodpro.MaxLength = 10
        Me.txtCodpro.Name = "txtCodpro"
        Me.txtCodpro.Size = New System.Drawing.Size(100, 20)
        Me.txtCodpro.TabIndex = 1
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(153, 476)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 3
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(89, 55)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(86, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Codigo Producto"
        '
        'txtAltura
        '
        Me.txtAltura.Location = New System.Drawing.Point(65, 36)
        Me.txtAltura.Name = "txtAltura"
        Me.txtAltura.Size = New System.Drawing.Size(100, 20)
        Me.txtAltura.TabIndex = 5
        '
        'txtLongitud
        '
        Me.txtLongitud.Location = New System.Drawing.Point(65, 76)
        Me.txtLongitud.Name = "txtLongitud"
        Me.txtLongitud.Size = New System.Drawing.Size(100, 20)
        Me.txtLongitud.TabIndex = 6
        '
        'txtEAN13
        '
        Me.txtEAN13.Location = New System.Drawing.Point(65, 194)
        Me.txtEAN13.Name = "txtEAN13"
        Me.txtEAN13.Size = New System.Drawing.Size(100, 20)
        Me.txtEAN13.TabIndex = 8
        '
        'txtPeso
        '
        Me.txtPeso.Location = New System.Drawing.Point(65, 239)
        Me.txtPeso.Name = "txtPeso"
        Me.txtPeso.Size = New System.Drawing.Size(100, 20)
        Me.txtPeso.TabIndex = 9
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(87, 20)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 13)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Altura(mm)"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(80, 59)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(70, 13)
        Me.Label3.TabIndex = 11
        Me.Label3.Text = "Longitud(mm)"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(92, 176)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(41, 13)
        Me.Label5.TabIndex = 13
        Me.Label5.Text = "EAN13"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(92, 220)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(46, 13)
        Me.Label6.TabIndex = 14
        Me.Label6.Text = "Peso(gr)"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.rbGofradoNo)
        Me.GroupBox1.Controls.Add(Me.rbGofradoSi)
        Me.GroupBox1.Location = New System.Drawing.Point(85, 101)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(60, 67)
        Me.GroupBox1.TabIndex = 15
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Gofrado"
        '
        'rbGofradoNo
        '
        Me.rbGofradoNo.AutoSize = True
        Me.rbGofradoNo.Location = New System.Drawing.Point(10, 42)
        Me.rbGofradoNo.Name = "rbGofradoNo"
        Me.rbGofradoNo.Size = New System.Drawing.Size(39, 17)
        Me.rbGofradoNo.TabIndex = 1
        Me.rbGofradoNo.TabStop = True
        Me.rbGofradoNo.Text = "No"
        Me.rbGofradoNo.UseVisualStyleBackColor = True
        '
        'rbGofradoSi
        '
        Me.rbGofradoSi.AutoSize = True
        Me.rbGofradoSi.Location = New System.Drawing.Point(10, 19)
        Me.rbGofradoSi.Name = "rbGofradoSi"
        Me.rbGofradoSi.Size = New System.Drawing.Size(36, 17)
        Me.rbGofradoSi.TabIndex = 0
        Me.rbGofradoSi.TabStop = True
        Me.rbGofradoSi.Text = "Sí"
        Me.rbGofradoSi.UseVisualStyleBackColor = True
        '
        'grpControl
        '
        Me.grpControl.Controls.Add(Me.Label7)
        Me.grpControl.Controls.Add(Me.Label4)
        Me.grpControl.Controls.Add(Me.txtHojas)
        Me.grpControl.Controls.Add(Me.txtPresentacion)
        Me.grpControl.Controls.Add(Me.GroupBox1)
        Me.grpControl.Controls.Add(Me.Label6)
        Me.grpControl.Controls.Add(Me.Label5)
        Me.grpControl.Controls.Add(Me.Label3)
        Me.grpControl.Controls.Add(Me.Label2)
        Me.grpControl.Controls.Add(Me.txtPeso)
        Me.grpControl.Controls.Add(Me.txtEAN13)
        Me.grpControl.Controls.Add(Me.txtLongitud)
        Me.grpControl.Controls.Add(Me.txtAltura)
        Me.grpControl.Enabled = False
        Me.grpControl.Location = New System.Drawing.Point(17, 105)
        Me.grpControl.Name = "grpControl"
        Me.grpControl.Size = New System.Drawing.Size(231, 365)
        Me.grpControl.TabIndex = 16
        Me.grpControl.TabStop = False
        Me.grpControl.Text = "Datos Producto"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(98, 309)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(34, 13)
        Me.Label7.TabIndex = 19
        Me.Label7.Text = "Hojas"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(81, 264)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(69, 13)
        Me.Label4.TabIndex = 18
        Me.Label4.Text = "Presentación"
        '
        'txtHojas
        '
        Me.txtHojas.Location = New System.Drawing.Point(63, 324)
        Me.txtHojas.Name = "txtHojas"
        Me.txtHojas.Size = New System.Drawing.Size(102, 20)
        Me.txtHojas.TabIndex = 17
        '
        'txtPresentacion
        '
        Me.txtPresentacion.Location = New System.Drawing.Point(67, 283)
        Me.txtPresentacion.Name = "txtPresentacion"
        Me.txtPresentacion.Size = New System.Drawing.Size(97, 20)
        Me.txtPresentacion.TabIndex = 16
        '
        'grpEmbalaje
        '
        Me.grpEmbalaje.Controls.Add(Me.Label13)
        Me.grpEmbalaje.Controls.Add(Me.Label12)
        Me.grpEmbalaje.Controls.Add(Me.Label11)
        Me.grpEmbalaje.Controls.Add(Me.Label10)
        Me.grpEmbalaje.Controls.Add(Me.txtDUN)
        Me.grpEmbalaje.Controls.Add(Me.txtPaletizado)
        Me.grpEmbalaje.Controls.Add(Me.txtBulto)
        Me.grpEmbalaje.Controls.Add(Me.txtPaquete)
        Me.grpEmbalaje.Enabled = False
        Me.grpEmbalaje.Location = New System.Drawing.Point(276, 106)
        Me.grpEmbalaje.Name = "grpEmbalaje"
        Me.grpEmbalaje.Size = New System.Drawing.Size(253, 167)
        Me.grpEmbalaje.TabIndex = 17
        Me.grpEmbalaje.TabStop = False
        Me.grpEmbalaje.Text = "Datos Embalaje"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(26, 132)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(67, 13)
        Me.Label13.TabIndex = 7
        Me.Label13.Text = "Codigo DUN"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(26, 99)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(56, 13)
        Me.Label12.TabIndex = 6
        Me.Label12.Text = "Paletizado"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(23, 63)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(85, 13)
        Me.Label11.TabIndex = 5
        Me.Label11.Text = "Bulto (Paquetes)"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(23, 27)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(47, 13)
        Me.Label10.TabIndex = 4
        Me.Label10.Text = "Paquete"
        '
        'txtDUN
        '
        Me.txtDUN.Location = New System.Drawing.Point(116, 129)
        Me.txtDUN.Name = "txtDUN"
        Me.txtDUN.Size = New System.Drawing.Size(110, 20)
        Me.txtDUN.TabIndex = 3
        '
        'txtPaletizado
        '
        Me.txtPaletizado.Location = New System.Drawing.Point(115, 99)
        Me.txtPaletizado.Name = "txtPaletizado"
        Me.txtPaletizado.Size = New System.Drawing.Size(111, 20)
        Me.txtPaletizado.TabIndex = 2
        '
        'txtBulto
        '
        Me.txtBulto.Location = New System.Drawing.Point(117, 63)
        Me.txtBulto.Name = "txtBulto"
        Me.txtBulto.Size = New System.Drawing.Size(109, 20)
        Me.txtBulto.TabIndex = 1
        '
        'txtPaquete
        '
        Me.txtPaquete.Location = New System.Drawing.Point(115, 24)
        Me.txtPaquete.Name = "txtPaquete"
        Me.txtPaquete.Size = New System.Drawing.Size(109, 20)
        Me.txtPaquete.TabIndex = 0
        '
        'grpAlterDisp
        '
        Me.grpAlterDisp.Controls.Add(Me.Label9)
        Me.grpAlterDisp.Controls.Add(Me.Label8)
        Me.grpAlterDisp.Controls.Add(Me.txtDispensador)
        Me.grpAlterDisp.Controls.Add(Me.txtAlternativa)
        Me.grpAlterDisp.Controls.Add(Me.btnAgregaDispensador)
        Me.grpAlterDisp.Controls.Add(Me.btnAgregaAlternativa)
        Me.grpAlterDisp.Controls.Add(Me.lbDispensadores)
        Me.grpAlterDisp.Controls.Add(Me.lbAlternativas)
        Me.grpAlterDisp.Enabled = False
        Me.grpAlterDisp.Location = New System.Drawing.Point(276, 284)
        Me.grpAlterDisp.Name = "grpAlterDisp"
        Me.grpAlterDisp.Size = New System.Drawing.Size(402, 206)
        Me.grpAlterDisp.TabIndex = 18
        Me.grpAlterDisp.TabStop = False
        Me.grpAlterDisp.Text = "Alternativas y dispensadores"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(219, 22)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(77, 13)
        Me.Label9.TabIndex = 7
        Me.Label9.Text = "Dispensadores"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(26, 20)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(62, 13)
        Me.Label8.TabIndex = 6
        Me.Label8.Text = "Alternativas"
        '
        'txtDispensador
        '
        Me.txtDispensador.Location = New System.Drawing.Point(216, 165)
        Me.txtDispensador.Name = "txtDispensador"
        Me.txtDispensador.Size = New System.Drawing.Size(86, 20)
        Me.txtDispensador.TabIndex = 5
        '
        'txtAlternativa
        '
        Me.txtAlternativa.Location = New System.Drawing.Point(26, 166)
        Me.txtAlternativa.Name = "txtAlternativa"
        Me.txtAlternativa.Size = New System.Drawing.Size(83, 20)
        Me.txtAlternativa.TabIndex = 4
        '
        'btnAgregaDispensador
        '
        Me.btnAgregaDispensador.Location = New System.Drawing.Point(308, 155)
        Me.btnAgregaDispensador.Name = "btnAgregaDispensador"
        Me.btnAgregaDispensador.Size = New System.Drawing.Size(79, 40)
        Me.btnAgregaDispensador.TabIndex = 3
        Me.btnAgregaDispensador.Text = "Agrega Dispensador"
        Me.btnAgregaDispensador.UseVisualStyleBackColor = True
        '
        'btnAgregaAlternativa
        '
        Me.btnAgregaAlternativa.Location = New System.Drawing.Point(118, 155)
        Me.btnAgregaAlternativa.Name = "btnAgregaAlternativa"
        Me.btnAgregaAlternativa.Size = New System.Drawing.Size(67, 40)
        Me.btnAgregaAlternativa.TabIndex = 2
        Me.btnAgregaAlternativa.Text = "Agrega Alternativa"
        Me.btnAgregaAlternativa.UseVisualStyleBackColor = True
        '
        'lbDispensadores
        '
        Me.lbDispensadores.FormattingEnabled = True
        Me.lbDispensadores.Location = New System.Drawing.Point(214, 38)
        Me.lbDispensadores.Name = "lbDispensadores"
        Me.lbDispensadores.Size = New System.Drawing.Size(168, 108)
        Me.lbDispensadores.TabIndex = 1
        '
        'lbAlternativas
        '
        Me.lbAlternativas.FormattingEnabled = True
        Me.lbAlternativas.Location = New System.Drawing.Point(25, 37)
        Me.lbAlternativas.Name = "lbAlternativas"
        Me.lbAlternativas.Size = New System.Drawing.Size(160, 108)
        Me.lbAlternativas.TabIndex = 0
        '
        'btnExpAtr
        '
        Me.btnExpAtr.Location = New System.Drawing.Point(557, 73)
        Me.btnExpAtr.Name = "btnExpAtr"
        Me.btnExpAtr.Size = New System.Drawing.Size(75, 36)
        Me.btnExpAtr.TabIndex = 19
        Me.btnExpAtr.Text = "Exportar Atributos"
        Me.btnExpAtr.UseVisualStyleBackColor = True
        '
        'btnExpRel
        '
        Me.btnExpRel.Location = New System.Drawing.Point(557, 115)
        Me.btnExpRel.Name = "btnExpRel"
        Me.btnExpRel.Size = New System.Drawing.Size(75, 46)
        Me.btnExpRel.TabIndex = 20
        Me.btnExpRel.Text = "Exportar relaciones"
        Me.btnExpRel.UseVisualStyleBackColor = True
        '
        'btnImpAtr
        '
        Me.btnImpAtr.Location = New System.Drawing.Point(557, 175)
        Me.btnImpAtr.Name = "btnImpAtr"
        Me.btnImpAtr.Size = New System.Drawing.Size(75, 43)
        Me.btnImpAtr.TabIndex = 21
        Me.btnImpAtr.Text = "Importar Atributos"
        Me.btnImpAtr.UseVisualStyleBackColor = True
        '
        'btnImpRel
        '
        Me.btnImpRel.Location = New System.Drawing.Point(557, 231)
        Me.btnImpRel.Name = "btnImpRel"
        Me.btnImpRel.Size = New System.Drawing.Size(75, 43)
        Me.btnImpRel.TabIndex = 22
        Me.btnImpRel.Text = "Importar Relaciones"
        Me.btnImpRel.UseVisualStyleBackColor = True
        '
        'AtribTissue
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(690, 509)
        Me.Controls.Add(Me.btnImpRel)
        Me.Controls.Add(Me.btnImpAtr)
        Me.Controls.Add(Me.btnExpRel)
        Me.Controls.Add(Me.btnExpAtr)
        Me.Controls.Add(Me.grpAlterDisp)
        Me.Controls.Add(Me.grpEmbalaje)
        Me.Controls.Add(Me.grpControl)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.txtCodpro)
        Me.Controls.Add(Me.btnGuardar)
        Me.Name = "AtribTissue"
        Me.Text = "Atributo Tissue"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.grpControl.ResumeLayout(False)
        Me.grpControl.PerformLayout()
        Me.grpEmbalaje.ResumeLayout(False)
        Me.grpEmbalaje.PerformLayout()
        Me.grpAlterDisp.ResumeLayout(False)
        Me.grpAlterDisp.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnGuardar As Button
    Friend WithEvents txtCodpro As TextBox
    Friend WithEvents btnCancelar As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents txtAltura As TextBox
    Friend WithEvents txtLongitud As TextBox
    Friend WithEvents txtEAN13 As TextBox
    Friend WithEvents txtPeso As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents rbGofradoNo As RadioButton
    Friend WithEvents rbGofradoSi As RadioButton
    Friend WithEvents grpControl As GroupBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents txtHojas As TextBox
    Friend WithEvents txtPresentacion As TextBox
    Friend WithEvents grpEmbalaje As GroupBox
    Friend WithEvents grpAlterDisp As GroupBox
    Friend WithEvents Label9 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents txtDispensador As TextBox
    Friend WithEvents txtAlternativa As TextBox
    Friend WithEvents btnAgregaDispensador As Button
    Friend WithEvents btnAgregaAlternativa As Button
    Friend WithEvents lbDispensadores As ListBox
    Friend WithEvents lbAlternativas As ListBox
    Friend WithEvents Label13 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents txtDUN As TextBox
    Friend WithEvents txtPaletizado As TextBox
    Friend WithEvents txtBulto As TextBox
    Friend WithEvents txtPaquete As TextBox
    Public WithEvents Label11 As Label
    Friend WithEvents btnExpAtr As Button
    Friend WithEvents btnExpRel As Button
    Friend WithEvents btnImpAtr As Button
    Friend WithEvents btnImpRel As Button
End Class
