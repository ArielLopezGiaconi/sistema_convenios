﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCargaMaestra
    Inherits MetroFramework.Forms.MetroForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnCargar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.dgvProds = New System.Windows.Forms.DataGridView()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmbEstpro = New System.Windows.Forms.ComboBox()
        Me.dgvLeer = New System.Windows.Forms.DataGridView()
        Me.btnExportar = New System.Windows.Forms.Button()
        Me.btnLeer = New System.Windows.Forms.Button()
        CType(Me.dgvProds, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvLeer, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnCargar
        '
        Me.btnCargar.Location = New System.Drawing.Point(22, 56)
        Me.btnCargar.Name = "btnCargar"
        Me.btnCargar.Size = New System.Drawing.Size(75, 23)
        Me.btnCargar.TabIndex = 0
        Me.btnCargar.Text = "Cargar"
        Me.btnCargar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(122, 56)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 1
        Me.btnGuardar.Text = "Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'dgvProds
        '
        Me.dgvProds.AllowUserToAddRows = False
        Me.dgvProds.AllowUserToDeleteRows = False
        Me.dgvProds.AllowUserToOrderColumns = True
        Me.dgvProds.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvProds.Location = New System.Drawing.Point(22, 85)
        Me.dgvProds.Name = "dgvProds"
        Me.dgvProds.Size = New System.Drawing.Size(840, 268)
        Me.dgvProds.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(22, 359)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(86, 13)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Estado Producto"
        '
        'cmbEstpro
        '
        Me.cmbEstpro.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbEstpro.FormattingEnabled = True
        Me.cmbEstpro.Location = New System.Drawing.Point(22, 378)
        Me.cmbEstpro.Name = "cmbEstpro"
        Me.cmbEstpro.Size = New System.Drawing.Size(121, 21)
        Me.cmbEstpro.TabIndex = 5
        '
        'dgvLeer
        '
        Me.dgvLeer.AllowUserToAddRows = False
        Me.dgvLeer.AllowUserToDeleteRows = False
        Me.dgvLeer.AllowUserToOrderColumns = True
        Me.dgvLeer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLeer.Location = New System.Drawing.Point(22, 405)
        Me.dgvLeer.Name = "dgvLeer"
        Me.dgvLeer.Size = New System.Drawing.Size(840, 268)
        Me.dgvLeer.TabIndex = 7
        '
        'btnExportar
        '
        Me.btnExportar.Location = New System.Drawing.Point(255, 378)
        Me.btnExportar.Name = "btnExportar"
        Me.btnExportar.Size = New System.Drawing.Size(75, 23)
        Me.btnExportar.TabIndex = 8
        Me.btnExportar.Text = "Exportar"
        Me.btnExportar.UseVisualStyleBackColor = True
        '
        'btnLeer
        '
        Me.btnLeer.Location = New System.Drawing.Point(162, 378)
        Me.btnLeer.Name = "btnLeer"
        Me.btnLeer.Size = New System.Drawing.Size(75, 23)
        Me.btnLeer.TabIndex = 9
        Me.btnLeer.Text = "Cargar"
        Me.btnLeer.UseVisualStyleBackColor = True
        '
        'frmCargaMaestra
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(886, 688)
        Me.Controls.Add(Me.btnLeer)
        Me.Controls.Add(Me.btnExportar)
        Me.Controls.Add(Me.dgvLeer)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cmbEstpro)
        Me.Controls.Add(Me.dgvProds)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnCargar)
        Me.Name = "frmCargaMaestra"
        Me.Text = "Carga Maestra"
        CType(Me.dgvProds, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvLeer, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnCargar As Button
    Friend WithEvents btnGuardar As Button
    Friend WithEvents dgvProds As DataGridView
    Friend WithEvents Label1 As Label
    Friend WithEvents cmbEstpro As ComboBox
    Friend WithEvents dgvLeer As DataGridView
    Friend WithEvents btnExportar As Button
    Friend WithEvents btnLeer As Button
End Class
