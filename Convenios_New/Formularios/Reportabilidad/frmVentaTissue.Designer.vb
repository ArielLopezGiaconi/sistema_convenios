﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmVentaTissue
    Inherits MetroFramework.Forms.MetroForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.tabVtaProd = New System.Windows.Forms.TabPage()
        Me.chkTodosProd = New System.Windows.Forms.CheckBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btnLimpiar = New System.Windows.Forms.Button()
        Me.btnCargar = New System.Windows.Forms.Button()
        Me.dgvVentas = New System.Windows.Forms.DataGridView()
        Me.dtFecfin = New System.Windows.Forms.DateTimePicker()
        Me.dtFecini = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmbCodpro = New System.Windows.Forms.ComboBox()
        Me.TabControl1.SuspendLayout()
        Me.tabVtaProd.SuspendLayout()
        CType(Me.dgvVentas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.tabVtaProd)
        Me.TabControl1.Location = New System.Drawing.Point(12, 12)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(776, 426)
        Me.TabControl1.TabIndex = 0
        '
        'tabVtaProd
        '
        Me.tabVtaProd.Controls.Add(Me.chkTodosProd)
        Me.tabVtaProd.Controls.Add(Me.Button1)
        Me.tabVtaProd.Controls.Add(Me.btnLimpiar)
        Me.tabVtaProd.Controls.Add(Me.btnCargar)
        Me.tabVtaProd.Controls.Add(Me.dgvVentas)
        Me.tabVtaProd.Controls.Add(Me.dtFecfin)
        Me.tabVtaProd.Controls.Add(Me.dtFecini)
        Me.tabVtaProd.Controls.Add(Me.Label3)
        Me.tabVtaProd.Controls.Add(Me.Label2)
        Me.tabVtaProd.Controls.Add(Me.Label1)
        Me.tabVtaProd.Controls.Add(Me.cmbCodpro)
        Me.tabVtaProd.Location = New System.Drawing.Point(4, 22)
        Me.tabVtaProd.Name = "tabVtaProd"
        Me.tabVtaProd.Padding = New System.Windows.Forms.Padding(3)
        Me.tabVtaProd.Size = New System.Drawing.Size(768, 400)
        Me.tabVtaProd.TabIndex = 0
        Me.tabVtaProd.Text = "Por Producto"
        Me.tabVtaProd.UseVisualStyleBackColor = True
        '
        'chkTodosProd
        '
        Me.chkTodosProd.AutoSize = True
        Me.chkTodosProd.Location = New System.Drawing.Point(134, 21)
        Me.chkTodosProd.Name = "chkTodosProd"
        Me.chkTodosProd.Size = New System.Drawing.Size(122, 17)
        Me.chkTodosProd.TabIndex = 10
        Me.chkTodosProd.Text = "Todos los productos"
        Me.chkTodosProd.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(659, 6)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(90, 34)
        Me.Button1.TabIndex = 9
        Me.Button1.Text = "Exportar a Excel"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'btnLimpiar
        '
        Me.btnLimpiar.Location = New System.Drawing.Point(578, 17)
        Me.btnLimpiar.Name = "btnLimpiar"
        Me.btnLimpiar.Size = New System.Drawing.Size(75, 23)
        Me.btnLimpiar.TabIndex = 8
        Me.btnLimpiar.Text = "Limpiar"
        Me.btnLimpiar.UseVisualStyleBackColor = True
        '
        'btnCargar
        '
        Me.btnCargar.Location = New System.Drawing.Point(496, 17)
        Me.btnCargar.Name = "btnCargar"
        Me.btnCargar.Size = New System.Drawing.Size(75, 23)
        Me.btnCargar.TabIndex = 7
        Me.btnCargar.Text = "Cargar"
        Me.btnCargar.UseVisualStyleBackColor = True
        '
        'dgvVentas
        '
        Me.dgvVentas.AllowUserToAddRows = False
        Me.dgvVentas.AllowUserToDeleteRows = False
        Me.dgvVentas.AllowUserToOrderColumns = True
        Me.dgvVentas.AllowUserToResizeRows = False
        Me.dgvVentas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvVentas.Location = New System.Drawing.Point(7, 48)
        Me.dgvVentas.Name = "dgvVentas"
        Me.dgvVentas.Size = New System.Drawing.Size(755, 346)
        Me.dgvVentas.TabIndex = 6
        '
        'dtFecfin
        '
        Me.dtFecfin.Location = New System.Drawing.Point(371, 20)
        Me.dtFecfin.Name = "dtFecfin"
        Me.dtFecfin.Size = New System.Drawing.Size(119, 20)
        Me.dtFecfin.TabIndex = 5
        '
        'dtFecini
        '
        Me.dtFecini.Location = New System.Drawing.Point(266, 21)
        Me.dtFecini.Name = "dtFecini"
        Me.dtFecini.Size = New System.Drawing.Size(86, 20)
        Me.dtFecini.TabIndex = 4
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(402, 3)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(62, 13)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Fecha Final"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(276, 4)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(65, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Fecha Inicio"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(7, 4)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(50, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Producto"
        '
        'cmbCodpro
        '
        Me.cmbCodpro.FormattingEnabled = True
        Me.cmbCodpro.Location = New System.Drawing.Point(6, 20)
        Me.cmbCodpro.Name = "cmbCodpro"
        Me.cmbCodpro.Size = New System.Drawing.Size(121, 21)
        Me.cmbCodpro.TabIndex = 0
        '
        'frmVentaTissue
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "frmVentaTissue"
        Me.TabControl1.ResumeLayout(False)
        Me.tabVtaProd.ResumeLayout(False)
        Me.tabVtaProd.PerformLayout()
        CType(Me.dgvVentas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents tabVtaProd As TabPage
    Friend WithEvents dtFecfin As DateTimePicker
    Friend WithEvents dtFecini As DateTimePicker
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents cmbCodpro As ComboBox
    Friend WithEvents btnLimpiar As Button
    Friend WithEvents btnCargar As Button
    Friend WithEvents dgvVentas As DataGridView
    Friend WithEvents Button1 As Button
    Friend WithEvents chkTodosProd As CheckBox
End Class
