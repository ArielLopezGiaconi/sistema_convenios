﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmFichaClienteConvenio
    Inherits MetroFramework.Forms.MetroForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btn_Salir = New System.Windows.Forms.Button()
        Me.tabReajuste = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.txtmgant = New System.Windows.Forms.TextBox()
        Me.txtmgdif = New System.Windows.Forms.TextBox()
        Me.txtsumaant = New System.Windows.Forms.TextBox()
        Me.txtsumDif = New System.Windows.Forms.TextBox()
        Me.txtrebant = New System.Windows.Forms.TextBox()
        Me.txtrebdif = New System.Windows.Forms.TextBox()
        Me.txtgasant = New System.Windows.Forms.TextBox()
        Me.txtgasdif = New System.Windows.Forms.TextBox()
        Me.txtvenant = New System.Windows.Forms.TextBox()
        Me.txtventdif = New System.Windows.Forms.TextBox()
        Me.txtmgnew = New System.Windows.Forms.TextBox()
        Me.txtsumanew = New System.Windows.Forms.TextBox()
        Me.txttranant = New System.Windows.Forms.TextBox()
        Me.txtrebnew = New System.Windows.Forms.TextBox()
        Me.txttrandif = New System.Windows.Forms.TextBox()
        Me.txtgasnew = New System.Windows.Forms.TextBox()
        Me.txtcontant = New System.Windows.Forms.TextBox()
        Me.txtvennew = New System.Windows.Forms.TextBox()
        Me.txtcontdif = New System.Windows.Forms.TextBox()
        Me.txttrannew = New System.Windows.Forms.TextBox()
        Me.txtingAnt = New System.Windows.Forms.TextBox()
        Me.txtcontnew = New System.Windows.Forms.TextBox()
        Me.txtingdif = New System.Windows.Forms.TextBox()
        Me.txtingnew = New System.Windows.Forms.TextBox()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.Label66 = New System.Windows.Forms.Label()
        Me.Label67 = New System.Windows.Forms.Label()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.Label61 = New System.Windows.Forms.Label()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtRazons = New System.Windows.Forms.TextBox()
        Me.txtRelCom = New System.Windows.Forms.TextBox()
        Me.txtRutcli = New System.Windows.Forms.TextBox()
        Me.dgvReajuste = New System.Windows.Forms.DataGridView()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.btn_Base = New System.Windows.Forms.Button()
        Me.btn_Cliente = New System.Windows.Forms.Button()
        Me.btn_Ejecutivo = New System.Windows.Forms.Button()
        Me.dgvDetalle = New System.Windows.Forms.DataGridView()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkEliminaD = New System.Windows.Forms.CheckBox()
        Me.chkEliminaC = New System.Windows.Forms.CheckBox()
        Me.chkEliminaB = New System.Windows.Forms.CheckBox()
        Me.chkEliminaA = New System.Windows.Forms.CheckBox()
        Me.btn_GrabarPolinomio = New System.Windows.Forms.Button()
        Me.chkRelacionado = New System.Windows.Forms.CheckBox()
        Me.txtCelulosaD = New System.Windows.Forms.TextBox()
        Me.txtCelulosaC = New System.Windows.Forms.TextBox()
        Me.txtCelulosaB = New System.Windows.Forms.TextBox()
        Me.txtCelulosaA = New System.Windows.Forms.TextBox()
        Me.txtDolarD = New System.Windows.Forms.TextBox()
        Me.txtDolarC = New System.Windows.Forms.TextBox()
        Me.txtDolarB = New System.Windows.Forms.TextBox()
        Me.txtDolarA = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtIpcD = New System.Windows.Forms.TextBox()
        Me.txtIpcC = New System.Windows.Forms.TextBox()
        Me.txtIpcB = New System.Windows.Forms.TextBox()
        Me.txtIpcA = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cmbLineaD = New System.Windows.Forms.ComboBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.cmbLineaC = New System.Windows.Forms.ComboBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.cmbLineaB = New System.Windows.Forms.ComboBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.cmbLineaA = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.tabReajuste.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.dgvReajuste, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage4.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.btn_Salir)
        Me.Panel1.Location = New System.Drawing.Point(5, 13)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1162, 47)
        Me.Panel1.TabIndex = 1
        '
        'btn_Salir
        '
        Me.btn_Salir.Image = Global.Convenios_New.My.Resources.Resources.salir
        Me.btn_Salir.Location = New System.Drawing.Point(1114, 2)
        Me.btn_Salir.Name = "btn_Salir"
        Me.btn_Salir.Size = New System.Drawing.Size(43, 40)
        Me.btn_Salir.TabIndex = 0
        Me.btn_Salir.UseVisualStyleBackColor = True
        '
        'tabReajuste
        '
        Me.tabReajuste.Controls.Add(Me.TabPage1)
        Me.tabReajuste.Controls.Add(Me.TabPage2)
        Me.tabReajuste.Controls.Add(Me.TabPage4)
        Me.tabReajuste.Location = New System.Drawing.Point(8, 66)
        Me.tabReajuste.Name = "tabReajuste"
        Me.tabReajuste.SelectedIndex = 0
        Me.tabReajuste.Size = New System.Drawing.Size(1169, 590)
        Me.tabReajuste.TabIndex = 27
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.SystemColors.Control
        Me.TabPage1.Controls.Add(Me.txtmgant)
        Me.TabPage1.Controls.Add(Me.txtmgdif)
        Me.TabPage1.Controls.Add(Me.txtsumaant)
        Me.TabPage1.Controls.Add(Me.txtsumDif)
        Me.TabPage1.Controls.Add(Me.txtrebant)
        Me.TabPage1.Controls.Add(Me.txtrebdif)
        Me.TabPage1.Controls.Add(Me.txtgasant)
        Me.TabPage1.Controls.Add(Me.txtgasdif)
        Me.TabPage1.Controls.Add(Me.txtvenant)
        Me.TabPage1.Controls.Add(Me.txtventdif)
        Me.TabPage1.Controls.Add(Me.txtmgnew)
        Me.TabPage1.Controls.Add(Me.txtsumanew)
        Me.TabPage1.Controls.Add(Me.txttranant)
        Me.TabPage1.Controls.Add(Me.txtrebnew)
        Me.TabPage1.Controls.Add(Me.txttrandif)
        Me.TabPage1.Controls.Add(Me.txtgasnew)
        Me.TabPage1.Controls.Add(Me.txtcontant)
        Me.TabPage1.Controls.Add(Me.txtvennew)
        Me.TabPage1.Controls.Add(Me.txtcontdif)
        Me.TabPage1.Controls.Add(Me.txttrannew)
        Me.TabPage1.Controls.Add(Me.txtingAnt)
        Me.TabPage1.Controls.Add(Me.txtcontnew)
        Me.TabPage1.Controls.Add(Me.txtingdif)
        Me.TabPage1.Controls.Add(Me.txtingnew)
        Me.TabPage1.Controls.Add(Me.Label59)
        Me.TabPage1.Controls.Add(Me.Label58)
        Me.TabPage1.Controls.Add(Me.Label66)
        Me.TabPage1.Controls.Add(Me.Label67)
        Me.TabPage1.Controls.Add(Me.Label65)
        Me.TabPage1.Controls.Add(Me.Label64)
        Me.TabPage1.Controls.Add(Me.Label63)
        Me.TabPage1.Controls.Add(Me.Label62)
        Me.TabPage1.Controls.Add(Me.Label61)
        Me.TabPage1.Controls.Add(Me.Label60)
        Me.TabPage1.Controls.Add(Me.Label57)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.Label3)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Controls.Add(Me.txtRazons)
        Me.TabPage1.Controls.Add(Me.txtRelCom)
        Me.TabPage1.Controls.Add(Me.txtRutcli)
        Me.TabPage1.Controls.Add(Me.dgvReajuste)
        Me.TabPage1.Location = New System.Drawing.Point(4, 23)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1161, 563)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Consulta"
        '
        'txtmgant
        '
        Me.txtmgant.BackColor = System.Drawing.SystemColors.Info
        Me.txtmgant.Location = New System.Drawing.Point(930, 378)
        Me.txtmgant.Name = "txtmgant"
        Me.txtmgant.Size = New System.Drawing.Size(100, 22)
        Me.txtmgant.TabIndex = 66
        Me.txtmgant.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtmgdif
        '
        Me.txtmgdif.BackColor = System.Drawing.SystemColors.Info
        Me.txtmgdif.Location = New System.Drawing.Point(929, 457)
        Me.txtmgdif.Name = "txtmgdif"
        Me.txtmgdif.Size = New System.Drawing.Size(100, 22)
        Me.txtmgdif.TabIndex = 51
        Me.txtmgdif.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtsumaant
        '
        Me.txtsumaant.BackColor = System.Drawing.SystemColors.Info
        Me.txtsumaant.Location = New System.Drawing.Point(817, 378)
        Me.txtsumaant.Name = "txtsumaant"
        Me.txtsumaant.Size = New System.Drawing.Size(100, 22)
        Me.txtsumaant.TabIndex = 52
        Me.txtsumaant.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtsumDif
        '
        Me.txtsumDif.BackColor = System.Drawing.SystemColors.Info
        Me.txtsumDif.Location = New System.Drawing.Point(816, 457)
        Me.txtsumDif.Name = "txtsumDif"
        Me.txtsumDif.Size = New System.Drawing.Size(100, 22)
        Me.txtsumDif.TabIndex = 53
        Me.txtsumDif.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtrebant
        '
        Me.txtrebant.BackColor = System.Drawing.SystemColors.Info
        Me.txtrebant.Location = New System.Drawing.Point(704, 378)
        Me.txtrebant.Name = "txtrebant"
        Me.txtrebant.Size = New System.Drawing.Size(100, 22)
        Me.txtrebant.TabIndex = 54
        Me.txtrebant.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtrebdif
        '
        Me.txtrebdif.BackColor = System.Drawing.SystemColors.Info
        Me.txtrebdif.Location = New System.Drawing.Point(703, 457)
        Me.txtrebdif.Name = "txtrebdif"
        Me.txtrebdif.Size = New System.Drawing.Size(100, 22)
        Me.txtrebdif.TabIndex = 55
        Me.txtrebdif.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtgasant
        '
        Me.txtgasant.BackColor = System.Drawing.SystemColors.Info
        Me.txtgasant.Location = New System.Drawing.Point(588, 378)
        Me.txtgasant.Name = "txtgasant"
        Me.txtgasant.Size = New System.Drawing.Size(100, 22)
        Me.txtgasant.TabIndex = 56
        Me.txtgasant.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtgasdif
        '
        Me.txtgasdif.BackColor = System.Drawing.SystemColors.Info
        Me.txtgasdif.Location = New System.Drawing.Point(587, 457)
        Me.txtgasdif.Name = "txtgasdif"
        Me.txtgasdif.Size = New System.Drawing.Size(100, 22)
        Me.txtgasdif.TabIndex = 58
        Me.txtgasdif.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtvenant
        '
        Me.txtvenant.BackColor = System.Drawing.SystemColors.Info
        Me.txtvenant.Location = New System.Drawing.Point(473, 378)
        Me.txtvenant.Name = "txtvenant"
        Me.txtvenant.Size = New System.Drawing.Size(100, 22)
        Me.txtvenant.TabIndex = 65
        Me.txtvenant.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtventdif
        '
        Me.txtventdif.BackColor = System.Drawing.SystemColors.Info
        Me.txtventdif.Location = New System.Drawing.Point(472, 457)
        Me.txtventdif.Name = "txtventdif"
        Me.txtventdif.Size = New System.Drawing.Size(100, 22)
        Me.txtventdif.TabIndex = 59
        Me.txtventdif.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtmgnew
        '
        Me.txtmgnew.BackColor = System.Drawing.SystemColors.Info
        Me.txtmgnew.Location = New System.Drawing.Point(930, 408)
        Me.txtmgnew.Name = "txtmgnew"
        Me.txtmgnew.Size = New System.Drawing.Size(100, 22)
        Me.txtmgnew.TabIndex = 60
        Me.txtmgnew.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtsumanew
        '
        Me.txtsumanew.BackColor = System.Drawing.SystemColors.Info
        Me.txtsumanew.Location = New System.Drawing.Point(817, 408)
        Me.txtsumanew.Name = "txtsumanew"
        Me.txtsumanew.Size = New System.Drawing.Size(100, 22)
        Me.txtsumanew.TabIndex = 61
        Me.txtsumanew.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txttranant
        '
        Me.txttranant.BackColor = System.Drawing.SystemColors.Info
        Me.txttranant.Location = New System.Drawing.Point(356, 378)
        Me.txttranant.Name = "txttranant"
        Me.txttranant.Size = New System.Drawing.Size(100, 22)
        Me.txttranant.TabIndex = 62
        Me.txttranant.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtrebnew
        '
        Me.txtrebnew.BackColor = System.Drawing.SystemColors.Info
        Me.txtrebnew.Location = New System.Drawing.Point(704, 408)
        Me.txtrebnew.Name = "txtrebnew"
        Me.txtrebnew.Size = New System.Drawing.Size(100, 22)
        Me.txtrebnew.TabIndex = 63
        Me.txtrebnew.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txttrandif
        '
        Me.txttrandif.BackColor = System.Drawing.SystemColors.Info
        Me.txttrandif.Location = New System.Drawing.Point(355, 457)
        Me.txttrandif.Name = "txttrandif"
        Me.txttrandif.Size = New System.Drawing.Size(100, 22)
        Me.txttrandif.TabIndex = 64
        Me.txttrandif.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtgasnew
        '
        Me.txtgasnew.BackColor = System.Drawing.SystemColors.Info
        Me.txtgasnew.Location = New System.Drawing.Point(588, 408)
        Me.txtgasnew.Name = "txtgasnew"
        Me.txtgasnew.Size = New System.Drawing.Size(100, 22)
        Me.txtgasnew.TabIndex = 50
        Me.txtgasnew.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtcontant
        '
        Me.txtcontant.BackColor = System.Drawing.SystemColors.Info
        Me.txtcontant.Location = New System.Drawing.Point(241, 378)
        Me.txtcontant.Name = "txtcontant"
        Me.txtcontant.Size = New System.Drawing.Size(100, 22)
        Me.txtcontant.TabIndex = 57
        Me.txtcontant.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtvennew
        '
        Me.txtvennew.BackColor = System.Drawing.SystemColors.Info
        Me.txtvennew.Location = New System.Drawing.Point(473, 408)
        Me.txtvennew.Name = "txtvennew"
        Me.txtvennew.Size = New System.Drawing.Size(100, 22)
        Me.txtvennew.TabIndex = 49
        Me.txtvennew.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtcontdif
        '
        Me.txtcontdif.BackColor = System.Drawing.SystemColors.Info
        Me.txtcontdif.Location = New System.Drawing.Point(240, 457)
        Me.txtcontdif.Name = "txtcontdif"
        Me.txtcontdif.Size = New System.Drawing.Size(100, 22)
        Me.txtcontdif.TabIndex = 43
        Me.txtcontdif.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txttrannew
        '
        Me.txttrannew.BackColor = System.Drawing.SystemColors.Info
        Me.txttrannew.Location = New System.Drawing.Point(356, 408)
        Me.txttrannew.Name = "txttrannew"
        Me.txttrannew.Size = New System.Drawing.Size(100, 22)
        Me.txttrannew.TabIndex = 48
        Me.txttrannew.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtingAnt
        '
        Me.txtingAnt.BackColor = System.Drawing.SystemColors.Info
        Me.txtingAnt.Location = New System.Drawing.Point(124, 378)
        Me.txtingAnt.Name = "txtingAnt"
        Me.txtingAnt.Size = New System.Drawing.Size(100, 22)
        Me.txtingAnt.TabIndex = 47
        Me.txtingAnt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtcontnew
        '
        Me.txtcontnew.BackColor = System.Drawing.SystemColors.Info
        Me.txtcontnew.Location = New System.Drawing.Point(241, 408)
        Me.txtcontnew.Name = "txtcontnew"
        Me.txtcontnew.Size = New System.Drawing.Size(100, 22)
        Me.txtcontnew.TabIndex = 46
        Me.txtcontnew.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtingdif
        '
        Me.txtingdif.BackColor = System.Drawing.SystemColors.Info
        Me.txtingdif.Location = New System.Drawing.Point(123, 457)
        Me.txtingdif.Name = "txtingdif"
        Me.txtingdif.Size = New System.Drawing.Size(100, 22)
        Me.txtingdif.TabIndex = 45
        Me.txtingdif.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtingnew
        '
        Me.txtingnew.BackColor = System.Drawing.SystemColors.Info
        Me.txtingnew.Location = New System.Drawing.Point(124, 408)
        Me.txtingnew.Name = "txtingnew"
        Me.txtingnew.Size = New System.Drawing.Size(100, 22)
        Me.txtingnew.TabIndex = 44
        Me.txtingnew.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Location = New System.Drawing.Point(16, 460)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(55, 14)
        Me.Label59.TabIndex = 33
        Me.Label59.Text = "Variación"
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Location = New System.Drawing.Point(16, 411)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(74, 14)
        Me.Label58.TabIndex = 37
        Me.Label58.Text = "EERR Nuevo"
        '
        'Label66
        '
        Me.Label66.AutoSize = True
        Me.Label66.Location = New System.Drawing.Point(951, 361)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(54, 14)
        Me.Label66.TabIndex = 34
        Me.Label66.Text = "Mg. Final"
        '
        'Label67
        '
        Me.Label67.AutoSize = True
        Me.Label67.Location = New System.Drawing.Point(830, 361)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(77, 14)
        Me.Label67.TabIndex = 35
        Me.Label67.Text = "Suma Gastos"
        '
        'Label65
        '
        Me.Label65.AutoSize = True
        Me.Label65.Location = New System.Drawing.Point(254, 361)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(75, 14)
        Me.Label65.TabIndex = 36
        Me.Label65.Text = "Contribución"
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.Location = New System.Drawing.Point(727, 361)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(46, 14)
        Me.Label64.TabIndex = 41
        Me.Label64.Text = "Rebate"
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.Location = New System.Drawing.Point(586, 361)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(102, 14)
        Me.Label63.TabIndex = 38
        Me.Label63.Text = "Gastos Operación"
        '
        'Label62
        '
        Me.Label62.AutoSize = True
        Me.Label62.Location = New System.Drawing.Point(500, 361)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(40, 14)
        Me.Label62.TabIndex = 39
        Me.Label62.Text = "Venta"
        '
        'Label61
        '
        Me.Label61.AutoSize = True
        Me.Label61.Location = New System.Drawing.Point(372, 361)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(67, 14)
        Me.Label61.TabIndex = 40
        Me.Label61.Text = "Transporte"
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.Location = New System.Drawing.Point(146, 361)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(53, 14)
        Me.Label60.TabIndex = 42
        Me.Label60.Text = "Ingresos"
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Location = New System.Drawing.Point(16, 381)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(83, 14)
        Me.Label57.TabIndex = 32
        Me.Label57.Text = "EERR Anterior"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(26, 45)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(73, 14)
        Me.Label2.TabIndex = 31
        Me.Label2.Text = "Razón Social"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(379, 13)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(84, 14)
        Me.Label3.TabIndex = 29
        Me.Label3.Text = "Num. Relación"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(26, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(67, 14)
        Me.Label1.TabIndex = 30
        Me.Label1.Text = "Rut Cliente"
        '
        'txtRazons
        '
        Me.txtRazons.BackColor = System.Drawing.SystemColors.Window
        Me.txtRazons.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRazons.Location = New System.Drawing.Point(122, 42)
        Me.txtRazons.Name = "txtRazons"
        Me.txtRazons.Size = New System.Drawing.Size(527, 22)
        Me.txtRazons.TabIndex = 28
        '
        'txtRelCom
        '
        Me.txtRelCom.BackColor = System.Drawing.SystemColors.Info
        Me.txtRelCom.Location = New System.Drawing.Point(492, 10)
        Me.txtRelCom.Name = "txtRelCom"
        Me.txtRelCom.Size = New System.Drawing.Size(157, 22)
        Me.txtRelCom.TabIndex = 26
        '
        'txtRutcli
        '
        Me.txtRutcli.BackColor = System.Drawing.SystemColors.Window
        Me.txtRutcli.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRutcli.Location = New System.Drawing.Point(122, 10)
        Me.txtRutcli.Name = "txtRutcli"
        Me.txtRutcli.Size = New System.Drawing.Size(157, 22)
        Me.txtRutcli.TabIndex = 27
        '
        'dgvReajuste
        '
        Me.dgvReajuste.AllowUserToAddRows = False
        Me.dgvReajuste.AllowUserToDeleteRows = False
        Me.dgvReajuste.AllowUserToResizeRows = False
        Me.dgvReajuste.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvReajuste.Location = New System.Drawing.Point(14, 178)
        Me.dgvReajuste.Name = "dgvReajuste"
        Me.dgvReajuste.ReadOnly = True
        Me.dgvReajuste.RowHeadersVisible = False
        Me.dgvReajuste.Size = New System.Drawing.Size(1015, 169)
        Me.dgvReajuste.TabIndex = 0
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.SystemColors.Control
        Me.TabPage2.Controls.Add(Me.btn_Base)
        Me.TabPage2.Controls.Add(Me.btn_Cliente)
        Me.TabPage2.Controls.Add(Me.btn_Ejecutivo)
        Me.TabPage2.Controls.Add(Me.dgvDetalle)
        Me.TabPage2.Location = New System.Drawing.Point(4, 23)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1161, 563)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Detalle Reajuste"
        '
        'btn_Base
        '
        Me.btn_Base.Image = Global.Convenios_New.My.Resources.Resources.Excel
        Me.btn_Base.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_Base.Location = New System.Drawing.Point(6, 15)
        Me.btn_Base.Name = "btn_Base"
        Me.btn_Base.Size = New System.Drawing.Size(195, 46)
        Me.btn_Base.TabIndex = 43
        Me.btn_Base.Text = "Genera Archivo Base"
        Me.btn_Base.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_Base.UseVisualStyleBackColor = True
        '
        'btn_Cliente
        '
        Me.btn_Cliente.Image = Global.Convenios_New.My.Resources.Resources.Excel
        Me.btn_Cliente.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_Cliente.Location = New System.Drawing.Point(408, 15)
        Me.btn_Cliente.Name = "btn_Cliente"
        Me.btn_Cliente.Size = New System.Drawing.Size(195, 46)
        Me.btn_Cliente.TabIndex = 43
        Me.btn_Cliente.Text = "Genera Archivo Cliente"
        Me.btn_Cliente.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_Cliente.UseVisualStyleBackColor = True
        '
        'btn_Ejecutivo
        '
        Me.btn_Ejecutivo.Image = Global.Convenios_New.My.Resources.Resources.Excel
        Me.btn_Ejecutivo.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_Ejecutivo.Location = New System.Drawing.Point(207, 15)
        Me.btn_Ejecutivo.Name = "btn_Ejecutivo"
        Me.btn_Ejecutivo.Size = New System.Drawing.Size(195, 46)
        Me.btn_Ejecutivo.TabIndex = 43
        Me.btn_Ejecutivo.Text = "Genera Archivo Ejecutivo"
        Me.btn_Ejecutivo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_Ejecutivo.UseVisualStyleBackColor = True
        '
        'dgvDetalle
        '
        Me.dgvDetalle.AllowUserToAddRows = False
        Me.dgvDetalle.AllowUserToDeleteRows = False
        Me.dgvDetalle.AllowUserToResizeRows = False
        Me.dgvDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetalle.Location = New System.Drawing.Point(6, 67)
        Me.dgvDetalle.Name = "dgvDetalle"
        Me.dgvDetalle.ReadOnly = True
        Me.dgvDetalle.RowHeadersVisible = False
        Me.dgvDetalle.Size = New System.Drawing.Size(1021, 410)
        Me.dgvDetalle.TabIndex = 42
        '
        'TabPage4
        '
        Me.TabPage4.BackColor = System.Drawing.SystemColors.Control
        Me.TabPage4.Controls.Add(Me.GroupBox1)
        Me.TabPage4.Location = New System.Drawing.Point(4, 23)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(1161, 563)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Polinomio"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chkEliminaD)
        Me.GroupBox1.Controls.Add(Me.chkEliminaC)
        Me.GroupBox1.Controls.Add(Me.chkEliminaB)
        Me.GroupBox1.Controls.Add(Me.chkEliminaA)
        Me.GroupBox1.Controls.Add(Me.btn_GrabarPolinomio)
        Me.GroupBox1.Controls.Add(Me.chkRelacionado)
        Me.GroupBox1.Controls.Add(Me.txtCelulosaD)
        Me.GroupBox1.Controls.Add(Me.txtCelulosaC)
        Me.GroupBox1.Controls.Add(Me.txtCelulosaB)
        Me.GroupBox1.Controls.Add(Me.txtCelulosaA)
        Me.GroupBox1.Controls.Add(Me.txtDolarD)
        Me.GroupBox1.Controls.Add(Me.txtDolarC)
        Me.GroupBox1.Controls.Add(Me.txtDolarB)
        Me.GroupBox1.Controls.Add(Me.txtDolarA)
        Me.GroupBox1.Controls.Add(Me.Label19)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.txtIpcD)
        Me.GroupBox1.Controls.Add(Me.txtIpcC)
        Me.GroupBox1.Controls.Add(Me.txtIpcB)
        Me.GroupBox1.Controls.Add(Me.txtIpcA)
        Me.GroupBox1.Controls.Add(Me.Label18)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.cmbLineaD)
        Me.GroupBox1.Controls.Add(Me.Label17)
        Me.GroupBox1.Controls.Add(Me.cmbLineaC)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.cmbLineaB)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.cmbLineaA)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Location = New System.Drawing.Point(6, 6)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1147, 176)
        Me.GroupBox1.TabIndex = 68
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Polinomio"
        '
        'chkEliminaD
        '
        Me.chkEliminaD.AutoSize = True
        Me.chkEliminaD.Location = New System.Drawing.Point(860, 140)
        Me.chkEliminaD.Name = "chkEliminaD"
        Me.chkEliminaD.Size = New System.Drawing.Size(74, 18)
        Me.chkEliminaD.TabIndex = 5
        Me.chkEliminaD.Text = "¿Elimina?"
        Me.chkEliminaD.UseVisualStyleBackColor = True
        '
        'chkEliminaC
        '
        Me.chkEliminaC.AutoSize = True
        Me.chkEliminaC.Location = New System.Drawing.Point(584, 141)
        Me.chkEliminaC.Name = "chkEliminaC"
        Me.chkEliminaC.Size = New System.Drawing.Size(74, 18)
        Me.chkEliminaC.TabIndex = 5
        Me.chkEliminaC.Text = "¿Elimina?"
        Me.chkEliminaC.UseVisualStyleBackColor = True
        '
        'chkEliminaB
        '
        Me.chkEliminaB.AutoSize = True
        Me.chkEliminaB.Location = New System.Drawing.Point(321, 141)
        Me.chkEliminaB.Name = "chkEliminaB"
        Me.chkEliminaB.Size = New System.Drawing.Size(74, 18)
        Me.chkEliminaB.TabIndex = 5
        Me.chkEliminaB.Text = "¿Elimina?"
        Me.chkEliminaB.UseVisualStyleBackColor = True
        '
        'chkEliminaA
        '
        Me.chkEliminaA.AutoSize = True
        Me.chkEliminaA.Location = New System.Drawing.Point(62, 141)
        Me.chkEliminaA.Name = "chkEliminaA"
        Me.chkEliminaA.Size = New System.Drawing.Size(74, 18)
        Me.chkEliminaA.TabIndex = 5
        Me.chkEliminaA.Text = "¿Elimina?"
        Me.chkEliminaA.UseVisualStyleBackColor = True
        '
        'btn_GrabarPolinomio
        '
        Me.btn_GrabarPolinomio.Image = Global.Convenios_New.My.Resources.Resources.Floppy
        Me.btn_GrabarPolinomio.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_GrabarPolinomio.Location = New System.Drawing.Point(991, 115)
        Me.btn_GrabarPolinomio.Name = "btn_GrabarPolinomio"
        Me.btn_GrabarPolinomio.Size = New System.Drawing.Size(150, 55)
        Me.btn_GrabarPolinomio.TabIndex = 4
        Me.btn_GrabarPolinomio.Text = "Grabar Datos"
        Me.btn_GrabarPolinomio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_GrabarPolinomio.UseVisualStyleBackColor = True
        '
        'chkRelacionado
        '
        Me.chkRelacionado.AutoSize = True
        Me.chkRelacionado.Location = New System.Drawing.Point(991, 91)
        Me.chkRelacionado.Name = "chkRelacionado"
        Me.chkRelacionado.Size = New System.Drawing.Size(150, 18)
        Me.chkRelacionado.TabIndex = 3
        Me.chkRelacionado.Text = "¿Incluye Relacionados?"
        Me.chkRelacionado.UseVisualStyleBackColor = True
        '
        'txtCelulosaD
        '
        Me.txtCelulosaD.Location = New System.Drawing.Point(860, 112)
        Me.txtCelulosaD.Name = "txtCelulosaD"
        Me.txtCelulosaD.Size = New System.Drawing.Size(100, 22)
        Me.txtCelulosaD.TabIndex = 2
        '
        'txtCelulosaC
        '
        Me.txtCelulosaC.Location = New System.Drawing.Point(584, 112)
        Me.txtCelulosaC.Name = "txtCelulosaC"
        Me.txtCelulosaC.Size = New System.Drawing.Size(100, 22)
        Me.txtCelulosaC.TabIndex = 2
        '
        'txtCelulosaB
        '
        Me.txtCelulosaB.Location = New System.Drawing.Point(321, 112)
        Me.txtCelulosaB.Name = "txtCelulosaB"
        Me.txtCelulosaB.Size = New System.Drawing.Size(100, 22)
        Me.txtCelulosaB.TabIndex = 2
        '
        'txtCelulosaA
        '
        Me.txtCelulosaA.Location = New System.Drawing.Point(62, 112)
        Me.txtCelulosaA.Name = "txtCelulosaA"
        Me.txtCelulosaA.Size = New System.Drawing.Size(100, 22)
        Me.txtCelulosaA.TabIndex = 2
        '
        'txtDolarD
        '
        Me.txtDolarD.Location = New System.Drawing.Point(860, 84)
        Me.txtDolarD.Name = "txtDolarD"
        Me.txtDolarD.Size = New System.Drawing.Size(100, 22)
        Me.txtDolarD.TabIndex = 2
        '
        'txtDolarC
        '
        Me.txtDolarC.Location = New System.Drawing.Point(584, 84)
        Me.txtDolarC.Name = "txtDolarC"
        Me.txtDolarC.Size = New System.Drawing.Size(100, 22)
        Me.txtDolarC.TabIndex = 2
        '
        'txtDolarB
        '
        Me.txtDolarB.Location = New System.Drawing.Point(321, 84)
        Me.txtDolarB.Name = "txtDolarB"
        Me.txtDolarB.Size = New System.Drawing.Size(100, 22)
        Me.txtDolarB.TabIndex = 2
        '
        'txtDolarA
        '
        Me.txtDolarA.Location = New System.Drawing.Point(62, 84)
        Me.txtDolarA.Name = "txtDolarA"
        Me.txtDolarA.Size = New System.Drawing.Size(100, 22)
        Me.txtDolarA.TabIndex = 2
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(804, 115)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(50, 14)
        Me.Label19.TabIndex = 0
        Me.Label19.Text = "Celulosa"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(528, 115)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(50, 14)
        Me.Label15.TabIndex = 0
        Me.Label15.Text = "Celulosa"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(265, 115)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(50, 14)
        Me.Label11.TabIndex = 0
        Me.Label11.Text = "Celulosa"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(6, 115)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(50, 14)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Celulosa"
        '
        'txtIpcD
        '
        Me.txtIpcD.Location = New System.Drawing.Point(860, 56)
        Me.txtIpcD.Name = "txtIpcD"
        Me.txtIpcD.Size = New System.Drawing.Size(100, 22)
        Me.txtIpcD.TabIndex = 2
        '
        'txtIpcC
        '
        Me.txtIpcC.Location = New System.Drawing.Point(584, 56)
        Me.txtIpcC.Name = "txtIpcC"
        Me.txtIpcC.Size = New System.Drawing.Size(100, 22)
        Me.txtIpcC.TabIndex = 2
        '
        'txtIpcB
        '
        Me.txtIpcB.Location = New System.Drawing.Point(321, 56)
        Me.txtIpcB.Name = "txtIpcB"
        Me.txtIpcB.Size = New System.Drawing.Size(100, 22)
        Me.txtIpcB.TabIndex = 2
        '
        'txtIpcA
        '
        Me.txtIpcA.Location = New System.Drawing.Point(62, 56)
        Me.txtIpcA.Name = "txtIpcA"
        Me.txtIpcA.Size = New System.Drawing.Size(100, 22)
        Me.txtIpcA.TabIndex = 2
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(804, 87)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(34, 14)
        Me.Label18.TabIndex = 0
        Me.Label18.Text = "Dólar"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(528, 87)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(34, 14)
        Me.Label14.TabIndex = 0
        Me.Label14.Text = "Dólar"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(265, 87)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(34, 14)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "Dólar"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(6, 87)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(34, 14)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Dólar"
        '
        'cmbLineaD
        '
        Me.cmbLineaD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbLineaD.FormattingEnabled = True
        Me.cmbLineaD.Location = New System.Drawing.Point(860, 26)
        Me.cmbLineaD.Name = "cmbLineaD"
        Me.cmbLineaD.Size = New System.Drawing.Size(197, 22)
        Me.cmbLineaD.TabIndex = 1
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(804, 59)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(25, 14)
        Me.Label17.TabIndex = 0
        Me.Label17.Text = "IPC"
        '
        'cmbLineaC
        '
        Me.cmbLineaC.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbLineaC.FormattingEnabled = True
        Me.cmbLineaC.Location = New System.Drawing.Point(584, 26)
        Me.cmbLineaC.Name = "cmbLineaC"
        Me.cmbLineaC.Size = New System.Drawing.Size(197, 22)
        Me.cmbLineaC.TabIndex = 1
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(528, 59)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(25, 14)
        Me.Label13.TabIndex = 0
        Me.Label13.Text = "IPC"
        '
        'cmbLineaB
        '
        Me.cmbLineaB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbLineaB.FormattingEnabled = True
        Me.cmbLineaB.Location = New System.Drawing.Point(321, 26)
        Me.cmbLineaB.Name = "cmbLineaB"
        Me.cmbLineaB.Size = New System.Drawing.Size(197, 22)
        Me.cmbLineaB.TabIndex = 1
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(804, 29)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(35, 14)
        Me.Label16.TabIndex = 0
        Me.Label16.Text = "Línea"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(265, 59)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(25, 14)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "IPC"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(528, 29)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(35, 14)
        Me.Label12.TabIndex = 0
        Me.Label12.Text = "Línea"
        '
        'cmbLineaA
        '
        Me.cmbLineaA.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbLineaA.FormattingEnabled = True
        Me.cmbLineaA.Location = New System.Drawing.Point(62, 26)
        Me.cmbLineaA.Name = "cmbLineaA"
        Me.cmbLineaA.Size = New System.Drawing.Size(197, 22)
        Me.cmbLineaA.TabIndex = 1
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(265, 29)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(35, 14)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "Línea"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 59)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(25, 14)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "IPC"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 29)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(35, 14)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Línea"
        '
        'frmFichaClienteConvenio
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1188, 665)
        Me.ControlBox = False
        Me.Controls.Add(Me.tabReajuste)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmFichaClienteConvenio"
        Me.ShowIcon = False
        Me.Text = "Ficha Cliente Convenio"
        Me.Panel1.ResumeLayout(False)
        Me.tabReajuste.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.dgvReajuste, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage4.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents btn_Salir As Button
    Friend WithEvents tabReajuste As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents txtRazons As TextBox
    Friend WithEvents txtRelCom As TextBox
    Friend WithEvents txtRutcli As TextBox
    Friend WithEvents dgvReajuste As DataGridView
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents txtmgant As TextBox
    Friend WithEvents txtmgdif As TextBox
    Friend WithEvents txtsumaant As TextBox
    Friend WithEvents txtsumDif As TextBox
    Friend WithEvents txtrebant As TextBox
    Friend WithEvents txtrebdif As TextBox
    Friend WithEvents txtgasant As TextBox
    Friend WithEvents txtgasdif As TextBox
    Friend WithEvents txtvenant As TextBox
    Friend WithEvents txtventdif As TextBox
    Friend WithEvents txtmgnew As TextBox
    Friend WithEvents txtsumanew As TextBox
    Friend WithEvents txttranant As TextBox
    Friend WithEvents txtrebnew As TextBox
    Friend WithEvents txttrandif As TextBox
    Friend WithEvents txtgasnew As TextBox
    Friend WithEvents txtcontant As TextBox
    Friend WithEvents txtvennew As TextBox
    Friend WithEvents txtcontdif As TextBox
    Friend WithEvents txttrannew As TextBox
    Friend WithEvents txtingAnt As TextBox
    Friend WithEvents txtcontnew As TextBox
    Friend WithEvents txtingdif As TextBox
    Friend WithEvents txtingnew As TextBox
    Friend WithEvents Label59 As Label
    Friend WithEvents Label58 As Label
    Friend WithEvents Label66 As Label
    Friend WithEvents Label67 As Label
    Friend WithEvents Label65 As Label
    Friend WithEvents Label64 As Label
    Friend WithEvents Label63 As Label
    Friend WithEvents Label62 As Label
    Friend WithEvents Label61 As Label
    Friend WithEvents Label60 As Label
    Friend WithEvents Label57 As Label
    Friend WithEvents dgvDetalle As DataGridView
    Friend WithEvents btn_Base As Button
    Friend WithEvents btn_Cliente As Button
    Friend WithEvents btn_Ejecutivo As Button
    Friend WithEvents TabPage4 As TabPage
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents chkEliminaD As CheckBox
    Friend WithEvents chkEliminaC As CheckBox
    Friend WithEvents chkEliminaB As CheckBox
    Friend WithEvents chkEliminaA As CheckBox
    Friend WithEvents btn_GrabarPolinomio As Button
    Friend WithEvents chkRelacionado As CheckBox
    Friend WithEvents txtCelulosaD As TextBox
    Friend WithEvents txtCelulosaC As TextBox
    Friend WithEvents txtCelulosaB As TextBox
    Friend WithEvents txtCelulosaA As TextBox
    Friend WithEvents txtDolarD As TextBox
    Friend WithEvents txtDolarC As TextBox
    Friend WithEvents txtDolarB As TextBox
    Friend WithEvents txtDolarA As TextBox
    Friend WithEvents Label19 As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents txtIpcD As TextBox
    Friend WithEvents txtIpcC As TextBox
    Friend WithEvents txtIpcB As TextBox
    Friend WithEvents txtIpcA As TextBox
    Friend WithEvents Label18 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents cmbLineaD As ComboBox
    Friend WithEvents Label17 As Label
    Friend WithEvents cmbLineaC As ComboBox
    Friend WithEvents Label13 As Label
    Friend WithEvents cmbLineaB As ComboBox
    Friend WithEvents Label16 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents cmbLineaA As ComboBox
    Friend WithEvents Label8 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
End Class
