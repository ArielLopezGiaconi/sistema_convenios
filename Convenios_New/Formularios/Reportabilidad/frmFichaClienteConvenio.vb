﻿Imports System.IO
Imports System.Data.OracleClient
Imports System.Runtime.InteropServices
Imports Microsoft.Office.Interop

Public Class frmFichaClienteConvenio
    Dim bd As New Conexion
    Dim flaggrilla As Boolean
    Dim numcot As String
    Dim reajuste As String
    Dim vendedor As String
    Dim PuntoControl As New PuntoControl
    Dim flagEmpRel As Boolean
    Private Sub frmFichaClienteConvenio_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        PuntoControl.RegistroUso("217")
        Me.StartPosition = FormStartPosition.Manual
        Me.Location = New Point(0, 0)
        cargaCombo()
    End Sub


    Private Sub cargaCombo()
        Try
            bd.open_dimerc()
            Dim sql = "select codlin,deslin from ma_lineapr order by deslin"
            Dim dt = bd.sqlSelect(sql)
            Dim dt2 = dt.Copy
            Dim dt3 = dt.Copy
            Dim dt4 = dt.Copy
            cmbLineaA.DataSource = dt
            cmbLineaA.SelectedIndex = -1
            cmbLineaA.ValueMember = "codlin"
            cmbLineaA.DisplayMember = "deslin"
            cmbLineaA.DropDownStyle = ComboBoxStyle.DropDown
            cmbLineaA.Text = ""
            cmbLineaA.Refresh()

            cmbLineaB.DataSource = dt2
            cmbLineaB.SelectedIndex = -1
            cmbLineaB.ValueMember = "codlin"
            cmbLineaB.DisplayMember = "deslin"
            cmbLineaB.DropDownStyle = ComboBoxStyle.DropDown
            cmbLineaB.Text = ""
            cmbLineaB.Refresh()

            cmbLineaC.DataSource = dt3
            cmbLineaC.SelectedIndex = -1
            cmbLineaC.ValueMember = "codlin"
            cmbLineaC.DisplayMember = "deslin"
            cmbLineaC.DropDownStyle = ComboBoxStyle.DropDown
            cmbLineaC.Text = ""
            cmbLineaC.Refresh()

            cmbLineaD.DataSource = dt4
            cmbLineaD.SelectedIndex = -1
            cmbLineaD.ValueMember = "codlin"
            cmbLineaD.DisplayMember = "deslin"
            cmbLineaD.DropDownStyle = ComboBoxStyle.DropDown
            cmbLineaD.Text = ""
            cmbLineaD.Refresh()
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub
    Private Sub btn_Salir_Click(sender As Object, e As EventArgs) Handles btn_Salir.Click
        Close()
    End Sub
    Private Sub txtRutcli_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtRutcli.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            PuntoControl.RegistroUso("218")
            If Trim(txtRutcli.Text) <> "" Then
                buscaCliente()
            Else
                txtRazons.Focus()
            End If
        End If
    End Sub
    Private Sub buscapolinomio()
        Try
            bd.open_dimerc()
            Dim Sql = " select codlin,ipc,dolar,celulosa,letra from ma_polinomio where codemp=3 and rutcli=" + txtRutcli.Text
            Sql = Sql & " order by letra "
            Dim dt = bd.sqlSelect(Sql)
            If dt.Rows.Count = 0 Then
                Exit Sub
            Else
                For i = 0 To dt.Rows.Count - 1
                    If dt.Rows(i).Item("letra") = "A" Then
                        cmbLineaA.SelectedValue = dt.Rows(i).Item("codlin")
                        txtIpcA.Text = dt.Rows(i).Item("ipc")
                        txtDolarA.Text = dt.Rows(i).Item("dolar")
                        txtCelulosaA.Text = dt.Rows(i).Item("celulosa")
                        Continue For
                    End If
                    If dt.Rows(i).Item("letra") = "B" Then
                        cmbLineaB.SelectedValue = dt.Rows(i).Item("codlin")
                        txtIpcB.Text = dt.Rows(i).Item("ipc")
                        txtDolarB.Text = dt.Rows(i).Item("dolar")
                        txtCelulosaB.Text = dt.Rows(i).Item("celulosa")
                        Continue For
                    End If
                    If dt.Rows(i).Item("letra") = "C" Then
                        cmbLineaC.SelectedValue = dt.Rows(i).Item("codlin")
                        txtIpcC.Text = dt.Rows(i).Item("ipc")
                        txtDolarC.Text = dt.Rows(i).Item("dolar")
                        txtCelulosaC.Text = dt.Rows(i).Item("celulosa")
                        Continue For
                    End If
                    If dt.Rows(i).Item("letra") = "D" Then
                        cmbLineaD.SelectedValue = dt.Rows(i).Item("codlin")
                        txtIpcD.Text = dt.Rows(i).Item("ipc")
                        txtDolarD.Text = dt.Rows(i).Item("dolar")
                        txtCelulosaD.Text = dt.Rows(i).Item("celulosa")
                        Continue For
                    End If
                Next
            End If

        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub
    Private Sub limpiar()
        dgvDetalle.DataSource = Nothing
        dgvReajuste.DataSource = Nothing

        txtrebant.Text = ""
        txtrebdif.Text = ""
        txtrebnew.Text = ""
        txtvenant.Text = ""
        txtvennew.Text = ""
        txtventdif.Text = ""
        txttranant.Text = ""
        txttrandif.Text = ""
        txttrannew.Text = ""
        txtingAnt.Text = ""
        txtingdif.Text = ""
        txtingnew.Text = ""
        txtcontant.Text = ""
        txtcontdif.Text = ""
        txtcontnew.Text = ""
        txtgasant.Text = ""
        txtgasdif.Text = ""
        txtgasnew.Text = ""
        txtsumaant.Text = ""
        txtsumanew.Text = ""
        txtsumDif.Text = ""
        txtmgant.Text = ""
        txtmgdif.Text = ""
        txtmgnew.Text = ""
        limpiapolinomio()
    End Sub
    Private Sub limpiapolinomio()
        cmbLineaA.SelectedIndex = -1
        cmbLineaB.SelectedIndex = -1
        cmbLineaC.SelectedIndex = -1
        cmbLineaD.SelectedIndex = -1
        txtDolarA.Text = ""
        txtIpcA.Text = ""
        txtCelulosaA.Text = ""
        txtDolarB.Text = ""
        txtIpcB.Text = ""
        txtCelulosaB.Text = ""
        txtDolarC.Text = ""
        txtIpcC.Text = ""
        txtCelulosaC.Text = ""
        txtDolarD.Text = ""
        txtIpcD.Text = ""
        txtCelulosaD.Text = ""
        chkEliminaA.Checked = False
        chkEliminaB.Checked = False
        chkEliminaC.Checked = False
        chkEliminaD.Checked = False
    End Sub
    Private Sub txtRazons_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtRazons.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If txtRazons.Text.ToString.Trim <> "" Then

                Dim buscador As New frmBuscaCliente(txtRazons.Text)
                buscador.ShowDialog()
                If buscador.dgvDatos.CurrentRow Is Nothing Then
                    Exit Sub
                End If
                txtRutcli.Text = buscador.dgvDatos.Item("Rut", buscador.dgvDatos.CurrentRow.Index).Value.ToString
                txtRazons.Text = buscador.dgvDatos.Item("Razon Social", buscador.dgvDatos.CurrentRow.Index).Value.ToString
                buscaCoti()
                buscapolinomio()
            Else
                If txtRutcli.Text = "" Then
                    txtRutcli.Focus()
                End If
            End If
        End If
    End Sub

    Private Sub buscaCliente()
        Try
            bd.open_dimerc()
            Dim StrQuery = "select razons nombre from en_cliente "
            StrQuery = StrQuery & " where rutcli = '" & txtRutcli.Text & "'"
            StrQuery = StrQuery & "   and codemp = 3"
            Dim OdnCliente = bd.sqlSelect(StrQuery)
            If Not OdnCliente.Rows.Count = 0 Then
                txtRazons.Text = CStr(OdnCliente.Rows(0).Item("nombre"))
                buscaCoti()
            Else
                MessageBox.Show("Cliente No Existe. Debe ser Ingresado Previamente.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtRutcli.Text = ""
                txtRutcli.Focus()
                Exit Sub
            End If
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub
    Private Sub txtRelCom_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtRelCom.KeyPress
        e.Handled = True
    End Sub
    Private Sub txtmgant_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtventdif.KeyPress, txtvennew.KeyPress, txtvenant.KeyPress, txttrannew.KeyPress, txttrandif.KeyPress, txttranant.KeyPress, txtsumDif.KeyPress, txtsumanew.KeyPress, txtsumaant.KeyPress, txtrebnew.KeyPress, txtrebdif.KeyPress, txtrebant.KeyPress, txtmgnew.KeyPress, txtmgdif.KeyPress, txtmgant.KeyPress, txtingnew.KeyPress, txtingdif.KeyPress, txtingAnt.KeyPress, txtgasnew.KeyPress, txtgasdif.KeyPress, txtgasant.KeyPress, txtcontnew.KeyPress, txtcontdif.KeyPress, txtcontant.KeyPress
        e.Handled = True
    End Sub
    Private Sub buscaCoti()
        Try
            bd.open_dimerc()
            flaggrilla = False
            Dim Sql = " select a.rutcli ""Rutcli"",getrazonsocial(a.codemp,a.rutcli) ""Razón Social"",a.num_reajuste ""Num. Reajuste"", "
            Sql = Sql & "a.numcot ""Num. Cotizacion"",b.fecemi ""Desde"",b.fecven ""Vence"",GET_NOMBRE(b.codven,1) ""Vendedor"", a.observacion ""Observación"", "
            Sql = Sql & "getdominio(278,a.estado) ""Estado"",c.numrel "
            Sql = Sql & "from en_reajuste_convenio a,en_cotizac b,re_emprela c "
            Sql = Sql & "where a.rutcli= " + txtRutcli.Text
            Sql = Sql & "and a.codemp=3 "
            Sql = Sql & "and a.codemp=b.codemp "
            Sql = Sql & "and a.numcot=b.numcot "
            Sql = Sql & "and a.codemp=c.codemp "
            Sql = Sql & "and a.rutcli=c.rutcli order by a.num_reajuste desc"
            Dim dt = bd.sqlSelect(Sql)
            If dt.Rows.Count = 0 Then
                MessageBox.Show("No hay reajustes hechos para este cliente", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                limpiar()
                Exit Sub
            Else
                txtRelCom.Text = dt.Rows(0).Item("numrel").ToString
                dgvReajuste.DataSource = dt
                dgvReajuste.Columns("numrel").Visible = False
                dgvReajuste.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            End If
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub
    Private Sub buscaTotalesCoti(indice As Integer)
        Try
            bd.open_dimerc()
            Dim Sql = " select toting_old,tot_cont_old,tot_trans_old,tot_venta_old, "
            Sql += " tot_gastos_old,tot_rebate_old,suma_gastos_old,mg_final_old,  "
            Sql += " toting_new,tot_cont_new, tot_trans_new,tot_venta_new,  "
            Sql += " tot_gastos_new,tot_rebate_new,suma_gastos_new,mg_final_new  "
            Sql += " from en_reajuste_convenio "
            Sql += " where codemp=3 "
            Sql += " and numcot= " + dgvReajuste.Item("Num. Cotizacion", indice).Value.ToString
            Sql += " and num_reajuste=" + dgvReajuste.Item("Num. Reajuste", indice).Value.ToString
            Dim dt = bd.sqlSelect(Sql)
            If dt.Rows.Count > 0 Then
                txtingAnt.Text = dt.Rows(0).Item("toting_old").ToString
                txtingnew.Text = dt.Rows(0).Item("toting_new").ToString
                txtingdif.Text = txtingnew.Text - txtingAnt.Text
                txtcontant.Text = dt.Rows(0).Item("tot_cont_old").ToString
                txtcontnew.Text = dt.Rows(0).Item("tot_cont_new").ToString
                txtcontdif.Text = txtcontnew.Text - txtcontant.Text
                txttranant.Text = dt.Rows(0).Item("tot_trans_old").ToString
                txttrannew.Text = dt.Rows(0).Item("tot_trans_new").ToString
                txttrandif.Text = txttrannew.Text - txttranant.Text
                txtvenant.Text = dt.Rows(0).Item("tot_venta_old").ToString
                txtvennew.Text = dt.Rows(0).Item("tot_venta_new").ToString
                txtventdif.Text = txtvennew.Text - txtvenant.Text
                txtgasant.Text = dt.Rows(0).Item("tot_gastos_old").ToString
                txtgasnew.Text = dt.Rows(0).Item("tot_gastos_new").ToString
                txtgasdif.Text = txtgasnew.Text - txtgasant.Text
                txtrebant.Text = dt.Rows(0).Item("tot_rebate_old").ToString
                txtrebnew.Text = dt.Rows(0).Item("tot_rebate_new").ToString
                txtrebdif.Text = txtrebnew.Text - txtrebant.Text
                txtsumaant.Text = dt.Rows(0).Item("suma_gastos_old").ToString
                txtsumanew.Text = dt.Rows(0).Item("suma_gastos_new").ToString
                txtsumDif.Text = txtsumanew.Text - txtsumaant.Text
                txtmgant.Text = dt.Rows(0).Item("mg_final_old").ToString / 100
                txtmgnew.Text = dt.Rows(0).Item("mg_final_new").ToString / 100
                txtmgdif.Text = (txtmgnew.Text - txtmgant.Text)

                txtmgant.Text = FormatPercent(txtmgant.Text, 2)
                txtmgnew.Text = FormatPercent(txtmgnew.Text, 2)
                txtmgdif.Text = FormatPercent(txtmgdif.Text, 2)

                txtingAnt.Text = FormatNumber(txtingAnt.Text, 0)
                txtingnew.Text = FormatNumber(txtingnew.Text, 0)
                txtingdif.Text = FormatNumber(txtingdif.Text, 0)

                txtcontdif.Text = FormatNumber(txtcontdif.Text, 0)
                txtcontant.Text = FormatNumber(txtcontant.Text, 0)
                txtcontnew.Text = FormatNumber(txtcontnew.Text, 0)

                txttrandif.Text = FormatNumber(txttrandif.Text, 0)
                txttranant.Text = FormatNumber(txttranant.Text, 0)
                txttrannew.Text = FormatNumber(txttrannew.Text, 0)

                txtventdif.Text = FormatNumber(txtventdif.Text, 0)
                txtvenant.Text = FormatNumber(txtvenant.Text, 0)
                txtvennew.Text = FormatNumber(txtvennew.Text, 0)

                txtgasdif.Text = FormatNumber(txtgasdif.Text, 0)
                txtgasant.Text = FormatNumber(txtgasant.Text, 0)
                txtgasnew.Text = FormatNumber(txtgasnew.Text, 0)

                txtrebdif.Text = FormatNumber(txtrebdif.Text, 0)
                txtrebant.Text = FormatNumber(txtrebant.Text, 0)
                txtrebnew.Text = FormatNumber(txtrebnew.Text, 0)

                txtsumDif.Text = FormatNumber(txtsumDif.Text, 0)
                txtsumaant.Text = FormatNumber(txtsumaant.Text, 0)
                txtsumanew.Text = FormatNumber(txtsumanew.Text, 0)
            Else

                MessageBox.Show("No encontro los totales de la cotización", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            End If
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub dgvReajuste_CurrentCellChanged(sender As Object, e As EventArgs) Handles dgvReajuste.CurrentCellChanged
        If dgvReajuste.CurrentRow IsNot Nothing Then
            buscaTotalesCoti(dgvReajuste.CurrentRow.Index)
        End If
    End Sub

    Private Sub dgvReajuste_CellContentDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvReajuste.CellContentDoubleClick
        If dgvReajuste.CurrentRow IsNot Nothing Then
            buscaDetalleReajuste(dgvReajuste.CurrentRow.Index)
        End If
    End Sub
    Private Sub buscaDetalleReajuste(indice As Integer)
        Try
            bd.open_dimerc()
            Dim Sql = " select codpro,despro,mpropia ""MARCA PROPIA"",linea,marca,cantidad,precio,costoanalisis ""Costo Analisis"",coscom ""Costo Comercial"",cosprom ""Costo Promedio"", "
            Sql = Sql & " cosesp ""COSTO ESPECIAL"",cosesppm ""COSTO ESPECIAL PM"", fecini_cosesp,fecfin_cosesp,cosrebate ""Costo Rebate"", aporte,peso, "
            Sql = Sql & " mg_comercial ""Mg. Comercial"",mg_inventario ""Mg. Inventario"",mg_rebate ""Mg. Rebate"","
            Sql = Sql & " val_precio ""Valorizado Precio"",val_analisis ""Valorizado Analisis"",val_inventario ""Valorizado Inventario"",val_rebate ""Valorizado Rebate"",cont_comercial ""Contribucion Comercial"", "
            Sql = Sql & " cont_inventario ""Contrib. Inventario"",cont_rebate ""Contrib. Rebate"",cont_rebate_mp ""Contrib Rebate MP"","
            Sql = Sql & " precot ""Precio Cotizacion"",val_old ""Valorizado Anterior"",estado,pedsto,variacion,usr_creac ""USUARIO"",alternativo,original "
            Sql = Sql & " from de_reajuste_convenio where codemp=3"
            Sql = Sql & " And num_reajuste=" + dgvReajuste.Item("Num. Reajuste", indice).Value.ToString
            Sql = Sql & " And numcot=" + dgvReajuste.Item("Num. Cotizacion", indice).Value.ToString
            dgvDetalle.DataSource = bd.sqlSelect(Sql)
            reajuste = dgvReajuste.Item("Num. Reajuste", indice).Value.ToString
            numcot = dgvReajuste.Item("Num. Cotizacion", indice).Value.ToString
            vendedor = dgvReajuste.Item("Vendedor", indice).Value.ToString
            tabReajuste.SelectedIndex = 1
            formatogrillaDetalle()
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub formatogrillaDetalle()
        dgvDetalle.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
        dgvDetalle.Columns("Cantidad").DefaultCellStyle.Format = "n0"
        dgvDetalle.Columns("Cantidad").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalle.Columns("Precio").DefaultCellStyle.Format = "n0"
        dgvDetalle.Columns("Precio").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalle.Columns("Costo Analisis").DefaultCellStyle.Format = "n0"
        dgvDetalle.Columns("Costo Analisis").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalle.Columns("Costo Comercial").DefaultCellStyle.Format = "n0"
        dgvDetalle.Columns("Costo Comercial").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalle.Columns("Costo Promedio").DefaultCellStyle.Format = "n0"
        dgvDetalle.Columns("Costo Promedio").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalle.Columns("Costo Especial").DefaultCellStyle.Format = "n0"
        dgvDetalle.Columns("Costo Especial").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalle.Columns("Costo Especial PM").DefaultCellStyle.Format = "n0"
        dgvDetalle.Columns("Costo Especial PM").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalle.Columns("Costo Rebate").DefaultCellStyle.Format = "n0"
        dgvDetalle.Columns("Costo Rebate").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalle.Columns("Aporte").DefaultCellStyle.Format = "n2"
        dgvDetalle.Columns("Aporte").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalle.Columns("Peso").DefaultCellStyle.Format = "n2"
        dgvDetalle.Columns("Peso").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalle.Columns("Mg. Comercial").DefaultCellStyle.Format = "n2"
        dgvDetalle.Columns("Mg. Comercial").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalle.Columns("Mg. Inventario").DefaultCellStyle.Format = "n2"
        dgvDetalle.Columns("Mg. Inventario").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalle.Columns("Mg. Rebate").DefaultCellStyle.Format = "n2"
        dgvDetalle.Columns("Mg. Rebate").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalle.Columns("Valorizado Precio").DefaultCellStyle.Format = "n0"
        dgvDetalle.Columns("Valorizado Precio").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalle.Columns("Valorizado Analisis").DefaultCellStyle.Format = "n0"
        dgvDetalle.Columns("Valorizado Analisis").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalle.Columns("Valorizado Inventario").DefaultCellStyle.Format = "n0"
        dgvDetalle.Columns("Valorizado Inventario").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalle.Columns("Valorizado Rebate").DefaultCellStyle.Format = "n0"
        dgvDetalle.Columns("Valorizado Rebate").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalle.Columns("Contribucion Comercial").DefaultCellStyle.Format = "n0"
        dgvDetalle.Columns("Contribucion Comercial").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalle.Columns("Contrib. Inventario").DefaultCellStyle.Format = "n0"
        dgvDetalle.Columns("Contrib. Inventario").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalle.Columns("Contrib. Rebate").DefaultCellStyle.Format = "n0"
        dgvDetalle.Columns("Contrib. Rebate").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalle.Columns("Contrib Rebate MP").DefaultCellStyle.Format = "n0"
        dgvDetalle.Columns("Contrib Rebate MP").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalle.Columns("Precio Cotizacion").DefaultCellStyle.Format = "n0"
        dgvDetalle.Columns("Precio Cotizacion").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalle.Columns("Valorizado Anterior").DefaultCellStyle.Format = "n0"
        dgvDetalle.Columns("Valorizado Anterior").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalle.Columns("Variacion").DefaultCellStyle.Format = "n2"
        dgvDetalle.Columns("Variacion").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
    End Sub

    Private Sub btnaEquipo_Click(sender As Object, e As EventArgs)
        Try
            Dim openFile As New OpenFileDialog
            If openFile.ShowDialog = System.Windows.Forms.DialogResult.OK Then
                IO.File.Copy(openFile.FileName, "C:\dimerc\" + openFile.FileName)
            End If
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub exportar()
        Try
            If IO.File.Exists("C:\dimerc\propuesta " & txtRazons.Text & ".xlsx") Then
                IO.File.Delete("C:\dimerc\propuesta " & txtRazons.Text & ".xlsx")
            End If

            Dim save As New SaveFileDialog
            save.Filter = "Archivo Excel | *.xlsx"
            save.InitialDirectory = "C:\dimerc"
            save.FileName = "propuesta " & txtRazons.Text & ".xlsx"
            'xlWorkBook.SaveAs("C:\dimerc\propuesta " & txtRazons.Text & ".xlsx", Excel.XlFileFormat.xlWorkbookNormal)
            If save.ShowDialog = System.Windows.Forms.DialogResult.OK Then

                bd.open_dimerc()
                Dim sql = " select b.fecini_convenio,a.codpro, a.despro, a.marca, a.linea,a.precio, a.precot, a.mg_comercial, "
                sql += " a.cantidad, a.alternativo,decode(a.original,'',a.codpro,a.original) original,a.estado  "
                sql += " from de_reajuste_convenio a,en_reajuste_convenio b where a.codemp=3  "
                sql += " and a.num_reajuste= " + reajuste
                sql += " and a.numcot=" + numcot.ToString
                sql += " and a.codemp=b.codemp"
                sql += " and a.num_reajuste=b.num_reajuste"
                sql += " and a.numcot=b.numcot"
                sql += " order by a.original,a.alternativo "
                Dim dtvendedor = bd.sqlSelect(sql)

                sql = " select rutcli,getrazonsocial(codemp,rutcli) razons,decode(estado,1,'FINANCIERO',2,'COMERCIAL') RELACION "
                sql = sql & "FROM re_emprela  "
                sql = sql & "where numrel in(select numrel from re_emprela where rutcli=" + txtRutcli.Text + " and codemp=3)  "
                sql = sql & "and rutcli not in(select rutcli from re_emprela where rutcli=" + txtRutcli.Text + " and codemp=3)order by rutcli "
                Dim dtrelacionado = bd.sqlSelect(sql)
                Dim rutrelacionado = ""
                If dtrelacionado.Rows.Count = 0 Then
                    rutrelacionado = txtRutcli.Text
                Else
                    rutrelacionado = txtRutcli.Text
                    For i = 0 To dtrelacionado.Rows.Count - 1
                        rutrelacionado += "," + dtrelacionado.Rows(i).Item("rutcli").ToString
                    Next
                End If

                sql = " select a.codpro,despro,mpropia , "
                sql = sql & " linea,marca,cantidad,a.precio, "
                sql = sql & " costoanalisis ""Costo Analisis"", "
                sql = sql & " coscom ""Costo Comercial"", "
                sql = sql & " cosprom ""Costo Promedio"", "
                sql = sql & " cosesp ""Costo Especial"", "
                sql = sql & " cosesppm ""Costo Especial PM"", "
                sql = sql & " aporte,peso, "
                sql = sql & " precot ""Precio Cotizacion"", "
                sql = sql & " estado, "
                sql = sql & " pedsto, "
                sql = sql & " ultcom, "
                sql = sql & " cosrep, "
                sql = sql & " variacion, "
                sql = sql & " fecini_cosesp ""FecIni Costo Especial"", "
                sql = sql & " fecfin_cosesp ""FecFin Costo Especial"", "
                sql = sql & " val_old ""Valorizado Anterior"", "
                sql = sql & " alternativo, "
                sql = sql & " nvl(h.precio,0) ""Precio Min. Venta"", "
                sql = sql & " nvl(i.precio,0) ""Precio Min. Coti"",GET_TIENEPRODUCTOALTERNATIVO(a.codpro) ""Estado Alternativo"" "
                sql = sql & " from de_reajuste_convenio a, "
                sql = sql & " (select codpro,min(precio) precio from   "
                sql = sql & "   (select b.codpro,min(b.precio) precio from en_factura a,de_factura b where a.codemp=3     "
                sql = sql & "   and a.rutcli in(" + rutrelacionado + ")    "
                sql = sql & "   and a.codemp=b.codemp    "
                sql = sql & "   and a.numfac=b.numfac    "
                sql = sql & "   and a.fecemi>= add_months(sysdate, -6)  group by b.codpro  "
                sql = sql & "   union  "
                sql = sql & "   select b.codpro,min(b.precio) precio from  en_factura a,de_guiades b,re_guiafac c where a.codemp=3     "
                sql = sql & "   and a.rutcli in(" + rutrelacionado + ")    "
                sql = sql & "   and a.codemp=b.codemp    "
                sql = sql & "   and a.numfac=c.numfac  "
                sql = sql & "   and b.numgui=c.numgui    "
                sql = sql & "   and a.fecemi>= add_months(sysdate, -6)  group by b.codpro)  "
                sql = sql & "   group by codpro ) h, "
                sql = sql & " (select b.codpro,min(b.precio) precio from en_cotizac a,de_cotizac b  "
                sql = sql & " where a.codemp=3   "
                sql = sql & " and a.rutcli in(" + rutrelacionado + ")  "
                sql = sql & " and a.codemp=b.codemp  "
                sql = sql & " and a.numcot=b.numcot  "
                sql = sql & " and a.fecemi>= add_months(sysdate, -6)  "
                sql = sql & " group by b.codpro) i  "
                sql = sql & " where codemp=3  "
                sql = sql & " and num_reajuste= " + reajuste
                sql = sql & " and numcot=" + numcot.ToString
                sql = sql & " and a.codpro=i.codpro(+) "
                sql = sql & " and a.codpro=h.codpro(+) "
                sql = sql & " order by alternativo,a.codpro"
                Dim dtdetalle = bd.sqlSelect(sql)

                Dim car As New frmCargando
                car.ProgressBar1.Minimum = 0
                car.ProgressBar1.Value = 0
                car.ProgressBar1.Maximum = dtvendedor.Rows.Count * 2
                car.StartPosition = FormStartPosition.CenterScreen
                car.Show()
                Cursor.Current = Cursors.WaitCursor

                Dim xlApp As Object
                Dim xlWorkBook As Object = Nothing
                Dim xlWorkSheet As Object

                Dim datestart As Date = Date.Now
                'xlApp = New Excel.ApplicationClass
                xlApp = CreateObject("Excel.Application")
                'xlApp.Visible = True

                If IO.File.Exists("C:\Program Files\Convenios_licitaciones\base.xlsx") Then xlWorkBook = xlApp.Workbooks.Open("C:\Program Files\Convenios_licitaciones\base.xlsx")
                'xlWorkBook = xlApp.Workbooks.Open("C:\test1.xlsx")
                xlWorkSheet = xlWorkBook.Worksheets("Propuesta Original")

                xlApp.ScreenUpdating = False
                'edit the cell with new value
                xlWorkSheet.Cells(12, 4) = txtRazons.Text
                xlWorkSheet.Cells(13, 4) = txtRutcli.Text
                xlWorkSheet.Cells(14, 4) = vendedor
                xlWorkSheet.Cells(23, 2) = "Estos precios comienzan a regir desde : " + dtvendedor.Rows(0).Item("fecini_convenio")
                Dim valorizadoSinMP As Double = 0
                Dim valorizadoConMP As Double = 0

                For i = 0 To dtvendedor.Rows.Count - 1
                    If Not dtvendedor.Rows(i).Item("Estado") = "ACTIVO DIMERC" And
                    Not dtvendedor.Rows(i).Item("Estado") = "ARTICULO NUEVO" And
                    Not dtvendedor.Rows(i).Item("Estado") = "EXCLUSIVO CONVENIO" And
                    Not dtvendedor.Rows(i).Item("Estado") = "AGOTADO EN MERCADO" Then
                        xlWorkSheet.Range("B" + (i + 29).ToString + "", "J" + (i + 29).ToString + "").Interior.Color = Excel.XlRgbColor.rgbNavajoWhite
                        xlWorkSheet.Cells(i + 29, 2) = (i + 1).ToString
                        xlWorkSheet.Cells(i + 29, 3) = dtvendedor.Rows(i).Item("Codpro").ToString
                        xlWorkSheet.Cells(i + 29, 4) = dtvendedor.Rows(i).Item("Despro").ToString
                        xlWorkSheet.Cells(i + 29, 5) = dtvendedor.Rows(i).Item("Marca").ToString
                        xlWorkSheet.Cells(i + 29, 6) = 0
                        xlWorkSheet.Cells(i + 29, 7) = 0
                        xlWorkSheet.Cells(i + 29, 8) = 0
                        xlWorkSheet.Cells(i + 29, 9) = 0
                        Continue For
                    End If
                    If dtvendedor.Rows(i).Item("Alternativo") = "SI" Then
                        xlWorkSheet.Range("B" + (i + 29).ToString + "", "J" + (i + 29).ToString + "").Interior.Color = Excel.XlRgbColor.rgbLightGrey
                        xlWorkSheet.Cells(i + 29, 2) = (i + 1).ToString
                        xlWorkSheet.Cells(i + 29, 3) = dtvendedor.Rows(i).Item("Codpro").ToString
                        xlWorkSheet.Cells(i + 29, 4) = dtvendedor.Rows(i).Item("Despro").ToString
                        xlWorkSheet.Cells(i + 29, 5) = dtvendedor.Rows(i).Item("Marca").ToString
                        xlWorkSheet.Cells(i + 29, 6) = dtvendedor.Rows(i - 1).Item("Precio").ToString
                        xlWorkSheet.Cells(i + 29, 7) = dtvendedor.Rows(i).Item("Precio").ToString
                        xlWorkSheet.Cells(i + 29, 8) = dtvendedor.Rows(i).Item("Mg_Comercial") / 100
                        xlWorkSheet.Cells(i + 29, 9) = (Math.Round((dtvendedor.Rows(i).Item("Precio") - dtvendedor.Rows(i - 1).Item("Precio").ToString) / dtvendedor.Rows(i - 1).Item("Precio").ToString * 100, 2)) / 100
                        xlWorkSheet.Cells(i + 29, 10) = dtvendedor.Rows(i - 1).Item("Original").ToString
                        valorizadoConMP = valorizadoConMP - dtvendedor.Rows(i - 1).Item("Precio").ToString * dtvendedor.Rows(i - 1).Item("Cantidad").ToString
                        valorizadoConMP += dtvendedor.Rows(i - 1).Item("Cantidad").ToString * dtvendedor.Rows(i).Item("Precio").ToString
                    Else
                        xlWorkSheet.Cells(i + 29, 2) = (i + 1).ToString
                        xlWorkSheet.Cells(i + 29, 3) = dtvendedor.Rows(i).Item("Codpro").ToString
                        xlWorkSheet.Cells(i + 29, 4) = dtvendedor.Rows(i).Item("Despro").ToString
                        xlWorkSheet.Cells(i + 29, 5) = dtvendedor.Rows(i).Item("Marca").ToString
                        xlWorkSheet.Cells(i + 29, 6) = dtvendedor.Rows(i).Item("Precot").ToString
                        xlWorkSheet.Cells(i + 29, 7) = dtvendedor.Rows(i).Item("Precio").ToString
                        xlWorkSheet.Cells(i + 29, 8) = dtvendedor.Rows(i).Item("Mg_Comercial") / 100
                        xlWorkSheet.Cells(i + 29, 9) = (Math.Round((dtvendedor.Rows(i).Item("Precio") - dtvendedor.Rows(i).Item("Precot")) / dtvendedor.Rows(i).Item("Precot") * 100, 2)) / 100
                        valorizadoSinMP += dtvendedor.Rows(i).Item("Precio").ToString * dtvendedor.Rows(i).Item("Cantidad").ToString
                        valorizadoConMP += dtvendedor.Rows(i).Item("Precio").ToString * dtvendedor.Rows(i).Item("Cantidad").ToString
                    End If

                    car.ProgressBar1.Value += 1
                Next

                sql = " select sum(val_old) viejo,sum(val_precio) nuevo from de_reajuste_convenio where codemp=3 "
                sql = sql & " and num_reajuste= " + reajuste
                sql = sql & " and numcot=" + numcot.ToString
                sql += " and alternativo='NO' "

                Dim dtcabeza = bd.sqlSelect(sql)
                If dtcabeza.Rows.Count > 0 Then
                    xlWorkSheet.Cells(22, 7) = dtcabeza.Rows(0).Item("viejo").ToString
                    xlWorkSheet.Cells(22, 8) = valorizadoSinMP
                    xlWorkSheet.Cells(26, 7) = dtcabeza.Rows(0).Item("viejo").ToString
                    xlWorkSheet.Cells(26, 8) = valorizadoConMP
                End If

                xlWorkSheet = xlWorkBook.Worksheets("Data Original")

                For i = 0 To dtdetalle.Rows.Count - 1
                    xlWorkSheet.Cells(i + 2, 1) = dtdetalle.Rows(i).Item("Codpro").ToString
                    xlWorkSheet.Cells(i + 2, 2) = dtdetalle.Rows(i).Item("Despro").ToString
                    xlWorkSheet.Cells(i + 2, 3) = dtdetalle.Rows(i).Item("Mpropia").ToString
                    xlWorkSheet.Cells(i + 2, 4) = dtdetalle.Rows(i).Item("Linea").ToString
                    xlWorkSheet.Cells(i + 2, 5) = dtdetalle.Rows(i).Item("Marca").ToString
                    xlWorkSheet.Cells(i + 2, 6) = IIf(dtdetalle.Rows(i).Item("Cantidad") > 0, dtdetalle.Rows(i).Item("Cantidad").ToString, 0)
                    xlWorkSheet.Cells(i + 2, 7) = dtdetalle.Rows(i).Item("Precio").ToString
                    xlWorkSheet.Cells(i + 2, 8) = dtdetalle.Rows(i).Item("Costo Analisis").ToString
                    xlWorkSheet.Cells(i + 2, 9) = dtdetalle.Rows(i).Item("Costo Promedio").ToString
                    xlWorkSheet.Cells(i + 2, 10) = dtdetalle.Rows(i).Item("Costo Especial").ToString
                    xlWorkSheet.Cells(i + 2, 11) = dtdetalle.Rows(i).Item("Costo Especial PM").ToString
                    xlWorkSheet.Cells(i + 2, 12) = dtdetalle.Rows(i).Item("Aporte") / 100
                    xlWorkSheet.Cells(i + 2, 13) = dtdetalle.Rows(i).Item("Peso") / 100
                    'xlWorkSheet.Cells(i + 2, 14) = dgvDetalleLineaProductos.Item("Mg. Comercial", i).Value / 100
                    Dim oXLRange As Excel.Range
                    oXLRange = CType(xlWorkSheet.Cells(i + 2, 14), Excel.Range)
                    oXLRange.Formula = "=1-H" & i + 2 & "/G" & i + 2
                    'xlWorkSheet.Cells(i + 2, 15) = dgvDetalleLineaProductos.Item("Mg. Inventario", i).Value / 100

                    oXLRange = CType(xlWorkSheet.Cells(i + 2, 15), Excel.Range)
                    oXLRange.FormulaLocal = "=SI(H" & i + 2 & "<I" & i + 2 & ";1-H" & i + 2 & "/G" & i + 2 & ";1-I" & i + 2 & "/G" & i + 2 & ")"

                    'xlWorkSheet.Cells(i + 2, 16) = dgvDetalleLineaProductos.Item("Mg. Rebate", i).Value / 100
                    oXLRange = CType(xlWorkSheet.Cells(i + 2, 16), Excel.Range)
                    oXLRange.FormulaLocal = "=SI(H" & i + 2 & "<W" & i + 2 & ";1-H" & i + 2 & "/G" & i + 2 & ";1-W" & i + 2 & "/G" & i + 2 & ")"


                    'xlWorkSheet.Cells(i + 2, 17) = dgvDetalleLineaProductos.Item("Valorizado Precio", i).Value.ToString
                    oXLRange = CType(xlWorkSheet.Cells(i + 2, 17), Excel.Range)
                    oXLRange.Formula = "=G" & i + 2 & "*F" & i + 2

                    'xlWorkSheet.Cells(i + 2, 18) = dgvDetalleLineaProductos.Item("Valorizado Analisis", i).Value.ToString
                    ''CALCULAR VALORIZADO ANALISIS
                    oXLRange = CType(xlWorkSheet.Cells(i + 2, 18), Excel.Range)
                    oXLRange.Formula = "=H" & i + 2 & "*F" & i + 2

                    'CALCULAR VALORIZADO INVENTARIO
                    'xlWorkSheet.Cells(i + 2, 19) = dgvDetalleLineaProductos.Item("Valorizado Inventario", i).Value.ToString
                    oXLRange = CType(xlWorkSheet.Cells(i + 2, 19), Excel.Range)
                    oXLRange.Formula = "=I" & i + 2 & "*F" & i + 2

                    'deja con formula valorizado rebate
                    'CALCULAR VALORIZADO REBATE
                    'xlWorkSheet.Cells(i + 2, 20) = dgvDetalleLineaProductos.Item("Valorizado Rebate", i).Value.ToString
                    oXLRange = CType(xlWorkSheet.Cells(i + 2, 20), Excel.Range)
                    oXLRange.Formula = "=W" & i + 2 & "*F" & i + 2
                    xlWorkSheet.Cells(i + 2, 22) = dtdetalle.Rows(i).Item("Costo Comercial").ToString
                    'xlWorkSheet.Cells(i + 2, 23) = dgvDetalleLineaProductos.Item("Costo Rebate", i).Value.ToString
                    oXLRange = CType(xlWorkSheet.Cells(i + 2, 23), Excel.Range)
                    oXLRange.Formula = "=I" & i + 2 & "*(1-L" & i + 2 & ")"

                    'xlWorkSheet.Cells(i + 2, 28) = dgvDetalleLineaProductos.Item("Contribucion Comercial", i).Value.ToString
                    oXLRange = CType(xlWorkSheet.Cells(i + 2, 28), Excel.Range)
                    oXLRange.Formula = "=(Q" & i + 2 & "-R" & i + 2 & ")"
                    'xlWorkSheet.Cells(i + 2, 29) = dgvDetalleLineaProductos.Item("Contrib. Inventario", i).Value.ToString

                    oXLRange = CType(xlWorkSheet.Cells(i + 2, 29), Excel.Range)
                    oXLRange.FormulaLocal = "=SI(C" & i + 2 & "<>""IMPORTADO"";SI(R" & i + 2 & ">S" & i + 2 & ";(R" & i + 2 & "-S" & i + 2 & ");0);0)"
                    'xlWorkSheet.Cells(i + 2, 30) = dgvDetalleLineaProductos.Item("Contrib. Rebate", i).Value.ToString

                    oXLRange = CType(xlWorkSheet.Cells(i + 2, 30), Excel.Range)
                    oXLRange.FormulaLocal = "=SI(C" & i + 2 & "<>""IMPORTADO"";SI(S" & i + 2 & ">T" & i + 2 & ";SI(R" & i + 2 & ">S" & i + 2 & ";(S" & i + 2 & "-T" & i + 2 & ");(R" & i + 2 & "-T" & i + 2 & "));0);0)"
                    'xlWorkSheet.Cells(i + 2, 31) = dgvDetalleLineaProductos.Item("Contrib Rebate MP", i).Value.ToString

                    oXLRange = CType(xlWorkSheet.Cells(i + 2, 31), Excel.Range)
                    oXLRange.FormulaLocal = "=SI(C" & i + 2 & "=""IMPORTADO"";SI(R" & i + 2 & ">S" & i + 2 & ";(R" & i + 2 & "-S" & i + 2 & ");0);0)"
                    xlWorkSheet.Cells(i + 2, 32) = dtdetalle.Rows(i).Item("Precio Cotizacion").ToString
                    xlWorkSheet.Cells(i + 2, 39) = dtdetalle.Rows(i).Item("Estado").ToString
                    xlWorkSheet.Cells(i + 2, 40) = dtdetalle.Rows(i).Item("Pedsto").ToString
                    xlWorkSheet.Cells(i + 2, 41) = dtdetalle.Rows(i).Item("UltCom").ToString
                    xlWorkSheet.Cells(i + 2, 42) = dtdetalle.Rows(i).Item("Cosrep").ToString
                    xlWorkSheet.Cells(i + 2, 43) = dtdetalle.Rows(i).Item("Variacion")
                    xlWorkSheet.Cells(i + 2, 44) = dtdetalle.Rows(i).Item("FecIni Costo Especial")
                    xlWorkSheet.Cells(i + 2, 45) = dtdetalle.Rows(i).Item("FecFin Costo Especial")
                    xlWorkSheet.Cells(i + 2, 46) = dtdetalle.Rows(i).Item("Valorizado Anterior").ToString
                    xlWorkSheet.Cells(i + 2, 47) = dtdetalle.Rows(i).Item("Estado Alternativo").ToString
                    xlWorkSheet.Cells(i + 2, 48) = dtdetalle.Rows(i).Item("Precio Min. Venta").ToString
                    xlWorkSheet.Cells(i + 2, 49) = dtdetalle.Rows(i).Item("Precio Min. Coti").ToString
                    car.ProgressBar1.Value += 1

                    Marshal.ReleaseComObject(xlWorkSheet.Cells)
                Next

                xlWorkSheet = xlWorkBook.Worksheets("Resumen Original")

                'edit the cell with new value
                'xlWorkSheet.Cells(6, 2) = txtRazons.Text
                'xlWorkSheet.Cells(7, 2) = txtRutcli.Text
                'xlWorkSheet.Cells(10, 3) = txtTotIngresiFinal.Text
                'xlWorkSheet.Cells(11, 2) = txtPorContriFinal.Text
                'xlWorkSheet.Cells(11, 3) = txtTotContriFinal.Text

                sql = " select nvl(porc_trans_new,0) transporte, "
                sql = sql & "nvl(porc_venta_new,0) venta, "
                sql = sql & "nvl(porc_gastos_new,0) gastos, "
                sql = sql & "nvl(porc_rebate_new,0) rebate from en_reajuste_convenio  "
                sql = sql & "where num_reajuste= " + reajuste.ToString
                sql = sql & "and codemp=3 and numcot= " + numcot
                Dim dtPorcentajes = bd.sqlSelect(sql)
                If dtPorcentajes.Rows.Count > 0 Then
                    xlWorkSheet.Cells(12, 2) = dtPorcentajes.Rows(0).Item("transporte") / 100
                    xlWorkSheet.Cells(13, 2) = dtPorcentajes.Rows(0).Item("venta") / 100
                    xlWorkSheet.Cells(14, 2) = dtPorcentajes.Rows(0).Item("gastos") / 100
                    xlWorkSheet.Cells(15, 2) = dtPorcentajes.Rows(0).Item("rebate") / 100
                Else
                    xlWorkSheet.Cells(12, 2) = "0,0"
                    xlWorkSheet.Cells(13, 2) = "0,0"
                    xlWorkSheet.Cells(14, 2) = "0,0"
                    xlWorkSheet.Cells(15, 2) = "0,0"
                End If

                'xlWorkSheet.Cells(14, 3) = txtTotGastOpFinal.Text

                'xlWorkSheet.Cells(15, 3) = txtTotRebateFinal.Text
                'xlWorkSheet.Cells(16, 3) = txtTotGastoFinal.Text
                'xlWorkSheet.Cells(17, 3) = txtTotMargenFinal.Text

                xlWorkSheet.Activate()
                Dim pt As Excel.PivotTable
                pt = xlWorkSheet.PivotTables("Tabla dinámica3")
                pt.PivotCache.Refresh()

                pt = xlWorkSheet.PivotTables("Tabla dinámica1")
                pt.PivotCache.Refresh()

                xlApp.ScreenUpdating = True

                car.Close()
                xlApp.DisplayAlerts = False
                'xlWorkBook.Close(SaveChanges:=True)

                xlWorkBook.SaveAs(save.FileName)
                xlWorkBook.Close()

                xlApp.DisplayAlerts = True
                xlApp.Quit()

                'xlWorkSheet.ReleaseComObject(pt)
                Marshal.ReleaseComObject(pt)
                Marshal.ReleaseComObject(xlApp)
                Marshal.ReleaseComObject(xlWorkBook)
                Marshal.ReleaseComObject(xlWorkSheet)

                MessageBox.Show("Listo")

            End If
            Cursor.Current = Cursors.Arrow
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub btn_Ejecutivo_Click(sender As Object, e As EventArgs) Handles btn_Ejecutivo.Click
        PuntoControl.RegistroUso("220")
        preparaexcelcorreoVendedor()
    End Sub

    Private Sub preparaexcelcorreoVendedor()
        Try
            bd.open_dimerc()

            Dim SQL = " select b.fecini_convenio,a.codpro, a.despro, a.marca, a.linea,a.precot,a.precio,a.mg_comercial,"
            SQL += " a.cantidad,"
            SQL += " a.alternativo,a.original,a.estado  "
            SQL += " from de_reajuste_convenio a,en_reajuste_convenio b  where a.codemp=3 "
            SQL += " And a.num_reajuste= " + reajuste
            SQL += " And a.numcot = " + numcot
            SQL += " and a.codemp=b.codemp"
            SQL += " and a.num_reajuste=b.num_reajuste"
            SQL += " and a.numcot=b.numcot"
            SQL += " order by a.original,a.alternativo "
            Dim dtvendedor = bd.sqlSelect(SQL)

            Dim car As New frmCargando
            car.ProgressBar1.Minimum = 0
            car.ProgressBar1.Value = 0
            car.ProgressBar1.Maximum = dtvendedor.Rows.Count
            car.StartPosition = FormStartPosition.CenterScreen
            car.Show()
            Cursor.Current = Cursors.WaitCursor

            Dim xlApp As Object
            Dim xlWorkBook As Object = Nothing
            Dim xlWorkSheet As Object

            Dim datestart As Date = Date.Now
            'xlApp = New Excel.ApplicationClass
            xlApp = CreateObject("Excel.Application")
            'xlApp.Visible = True

            If IO.File.Exists("C:\Program Files\Convenios_licitaciones\base para vendedor.xlsx") Then xlWorkBook = xlApp.Workbooks.Open("C:\Program Files\Convenios_licitaciones\base para vendedor.xlsx")
            'xlWorkBook = xlApp.Workbooks.Open("C:\test1.xlsx")
            xlWorkSheet = xlWorkBook.Worksheets("Propuesta Original Ejecutivo")

            xlApp.ScreenUpdating = False
            'edit the cell with new value
            xlWorkSheet.Cells(12, 4) = txtRazons.Text
            xlWorkSheet.Cells(13, 4) = txtRutcli.Text
            xlWorkSheet.Cells(15, 2) = "Precios tienen "
            xlWorkSheet.Cells(23, 2) = "Estos precios comienzan a regir desde : " + dtvendedor.Rows(0).Item("fecini_convenio")
            xlWorkSheet.Range("B15").Font.Color = Excel.XlRgbColor.rgbDarkRed

            Dim valorizadoSinMP As Double = 0
            Dim valorizadoConMP As Double = 0

            For i = 0 To dtvendedor.Rows.Count - 1
                If Not dtvendedor.Rows(i).Item("Estado") = "ACTIVO DIMERC" And
                    Not dtvendedor.Rows(i).Item("Estado") = "ARTICULO NUEVO" And
                    Not dtvendedor.Rows(i).Item("Estado") = "EXCLUSIVO CONVENIO" And
                    Not dtvendedor.Rows(i).Item("Estado") = "AGOTADO EN MERCADO" Then
                    xlWorkSheet.Range("B" + (i + 29).ToString + "", "J" + (i + 29).ToString + "").Interior.Color = Excel.XlRgbColor.rgbNavajoWhite
                    xlWorkSheet.Cells(i + 29, 2) = (i + 1).ToString
                    xlWorkSheet.Cells(i + 29, 3) = dtvendedor.Rows(i).Item("Codpro").ToString
                    xlWorkSheet.Cells(i + 29, 4) = dtvendedor.Rows(i).Item("Despro").ToString
                    xlWorkSheet.Cells(i + 29, 5) = dtvendedor.Rows(i).Item("Marca").ToString
                    xlWorkSheet.Cells(i + 29, 6) = 0
                    xlWorkSheet.Cells(i + 29, 7) = 0
                    xlWorkSheet.Cells(i + 29, 8) = 0
                    xlWorkSheet.Cells(i + 29, 9) = 0
                    Continue For
                End If

                If dtvendedor.Rows(i).Item("Alternativo") = "SI" Then
                    xlWorkSheet.Range("B" + (i + 29).ToString + "", "J" + (i + 29).ToString + "").Interior.Color = Excel.XlRgbColor.rgbLightGrey
                    xlWorkSheet.Cells(i + 29, 2) = (i + 1).ToString
                    xlWorkSheet.Cells(i + 29, 3) = dtvendedor.Rows(i).Item("Codpro").ToString
                    xlWorkSheet.Cells(i + 29, 4) = dtvendedor.Rows(i).Item("Despro").ToString
                    xlWorkSheet.Cells(i + 29, 5) = dtvendedor.Rows(i).Item("Marca").ToString
                    xlWorkSheet.Cells(i + 29, 6) = dtvendedor.Rows(i - 1).Item("Precio").ToString
                    xlWorkSheet.Cells(i + 29, 7) = dtvendedor.Rows(i).Item("Precio").ToString
                    xlWorkSheet.Cells(i + 29, 8) = dtvendedor.Rows(i).Item("Mg_Comercial") / 100
                    xlWorkSheet.Cells(i + 29, 9) = (Math.Round((dtvendedor.Rows(i).Item("Precio") - dtvendedor.Rows(i - 1).Item("Precio").ToString) / dtvendedor.Rows(i - 1).Item("Precio").ToString * 100, 2)) / 100
                    xlWorkSheet.Cells(i + 29, 10) = dtvendedor.Rows(i).Item("Original").ToString
                    valorizadoConMP = valorizadoConMP - dtvendedor.Rows(i - 1).Item("Precio").ToString * dtvendedor.Rows(i - 1).Item("Cantidad").ToString
                    valorizadoConMP += dtvendedor.Rows(i - 1).Item("Cantidad").ToString * dtvendedor.Rows(i).Item("Precio").ToString
                Else
                    xlWorkSheet.Cells(i + 29, 2) = (i + 1).ToString
                    xlWorkSheet.Cells(i + 29, 3) = dtvendedor.Rows(i).Item("Codpro").ToString
                    xlWorkSheet.Cells(i + 29, 4) = dtvendedor.Rows(i).Item("Despro").ToString
                    xlWorkSheet.Cells(i + 29, 5) = dtvendedor.Rows(i).Item("Marca").ToString
                    xlWorkSheet.Cells(i + 29, 6) = dtvendedor.Rows(i).Item("Precot").ToString
                    xlWorkSheet.Cells(i + 29, 7) = dtvendedor.Rows(i).Item("Precio").ToString
                    xlWorkSheet.Cells(i + 29, 8) = dtvendedor.Rows(i).Item("Mg_Comercial") / 100
                    xlWorkSheet.Cells(i + 29, 9) = (Math.Round((dtvendedor.Rows(i).Item("Precio") - dtvendedor.Rows(i).Item("Precot")) / dtvendedor.Rows(i).Item("Precot") * 100, 2)) / 100
                    valorizadoSinMP += dtvendedor.Rows(i).Item("Precio").ToString * dtvendedor.Rows(i).Item("Cantidad").ToString
                    valorizadoConMP += dtvendedor.Rows(i).Item("Precio").ToString * dtvendedor.Rows(i).Item("Cantidad").ToString
                End If
                car.ProgressBar1.Value += 1

                Marshal.ReleaseComObject(xlWorkSheet.Cells)
            Next
            SQL = " select toting_old viejo,toting_new nuevo from en_reajuste_convenio where codemp = 3 "
            SQL += " And num_reajuste= " + reajuste.ToString
            SQL += " And numcot = " + numcot
            Dim dtcabeza = bd.sqlSelect(SQL)
            If dtcabeza.Rows.Count > 0 Then
                xlWorkSheet.Cells(22, 7) = dtcabeza.Rows(0).Item("viejo").ToString
                xlWorkSheet.Cells(22, 8) = valorizadoSinMP
                xlWorkSheet.Cells(26, 7) = dtcabeza.Rows(0).Item("viejo").ToString
                xlWorkSheet.Cells(26, 8) = valorizadoConMP
            End If

            xlWorkSheet = xlWorkBook.Worksheets("Resumen Linea")
            xlWorkSheet.Cells(12, 4) = txtRazons.Text
            xlWorkSheet.Cells(13, 4) = txtRutcli.Text
            xlWorkSheet.Cells(15, 2) = "Precios tienen "
            xlWorkSheet.Range("B15").Font.Color = Excel.XlRgbColor.rgbDarkRed
            SQL = " select linea, sum(val_precio) valorizado,  "
            SQL = SQL & " round((sum(Val_Precio)-sum(Val_Analisis))/ sum(Val_Precio) * 100,2) mg_lista from de_reajuste_convenio  "
            SQL = SQL & "where codemp=3 and precio>0  "
            SQL = SQL & "And num_reajuste= " + reajuste.ToString
            SQL = SQL & " And numcot = " + numcot
            SQL = SQL & "group by linea  "
            SQL = SQL & "union   "
            SQL = SQL & "select 'TOTAL' linea,sum(val_precio) valorizado,  "
            SQL = SQL & "round((sum(Val_Precio)-sum(Val_Analisis))/ sum(Val_Precio) * 100,2) mg_lista from de_reajuste_convenio  "
            SQL = SQL & "where codemp=3  "
            SQL = SQL & "and num_reajuste= " + reajuste.ToString
            SQL = SQL & " and numcot = " + numcot

            Dim dtTotales = bd.sqlSelect(SQL)

            'Aca se agregan las cabeceras de nuestro datagrid.

            'Aca se ingresa el detalle recorrera la tabla celda por celda
            For i = 0 To dtTotales.Rows.Count - 1
                If dtTotales.Rows.Count - 1 = i Then
                    xlWorkSheet.Cells(i + 24, 4).Font.FontStyle = "Bold"
                    xlWorkSheet.Range("D" + (i + 24).ToString + "", "F" + (i + 24).ToString + "").Interior.Color = Excel.XlRgbColor.rgbLightGrey
                    xlWorkSheet.Cells(i + 24, 5).Font.FontStyle = "Bold"
                    xlWorkSheet.Cells(i + 24, 6).Font.FontStyle = "Bold"
                End If
                xlWorkSheet.Cells(i + 24, 4) = dtTotales.Rows(i).Item("Linea")
                xlWorkSheet.Cells(i + 24, 5) = dtTotales.Rows(i).Item("Valorizado")
                xlWorkSheet.Cells(i + 24, 6) = dtTotales.Rows(i).Item("Mg_Lista") / 100
            Next

            xlWorkSheet.Activate()

            xlApp.ScreenUpdating = True

            car.Close()
            xlApp.DisplayAlerts = False
            'xlWorkBook.Close(SaveChanges:=True)

            xlWorkBook.SaveAs("C:\dimerc\propuesta " & txtRazons.Text & " vendedor.xlsx")
            xlWorkBook.Close()

            xlApp.DisplayAlerts = True
            xlApp.Quit()

            'xlWorkSheet.ReleaseComObject(pt)
            Marshal.ReleaseComObject(xlApp)
            Marshal.ReleaseComObject(xlWorkBook)
            Marshal.ReleaseComObject(xlWorkSheet)
            Dim dateEnd As Date = Date.Now
            End_Excel_App(datestart, dateEnd)
            Cursor.Current = Cursors.Arrow

            MessageBox.Show("Archivo creado y guardado en la ruta C:\dimerc\propuesta " & txtRazons.Text & " vendedor.xlsx")
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub
    Private Sub End_Excel_App(datestart As Date, dateEnd As Date)
        Dim xlp() As Process = Process.GetProcessesByName("EXCEL")
        For Each Process As Process In xlp
            If Process.StartTime >= datestart And Process.StartTime <= dateEnd Then
                Process.Kill()
                Exit For
            End If
        Next
    End Sub

    Private Sub btn_Cliente_Click(sender As Object, e As EventArgs) Handles btn_Cliente.Click
        PuntoControl.RegistroUso("221")
        preparaexcelcorreoCliente()
    End Sub
    Private Sub preparaexcelcorreoCliente()
        Try
            bd.open_dimerc()

            Dim SQL = " select b.tipoconsumo,b.fecini_convenio,a.codpro, a.despro, a.marca,a.precio,a.cantidad, a.precot,nvl(a.variacion,0) variacion,a.alternativo,a.original,a.estado  "
            SQL += " from de_reajuste_convenio a,en_reajuste_convenio b where a.codemp=3 "
            SQL += " And a.num_reajuste= " + reajuste
            SQL += " and a.numcot = " + numcot
            SQL += " and a.codemp=b.codemp"
            SQL += " and a.num_reajuste=b.num_reajuste"
            SQL += " and a.numcot=b.numcot"
            SQL += " order by a.original,a.alternativo "
            Dim dtvendedor = bd.sqlSelect(SQL)

            Dim car As New frmCargando
            car.ProgressBar1.Minimum = 0
            car.ProgressBar1.Value = 0
            car.ProgressBar1.Maximum = dtvendedor.Rows.Count
            car.StartPosition = FormStartPosition.CenterScreen
            car.Show()
            Cursor.Current = Cursors.WaitCursor

            Dim xlApp As Object
            Dim xlWorkBook As Object = Nothing
            Dim xlWorkSheet As Object

            Dim datestart As Date = Date.Now
            'xlApp = New Excel.ApplicationClass
            xlApp = CreateObject("Excel.Application")
            'xlApp.Visible = True

            If IO.File.Exists("C:\Program Files\Convenios_licitaciones\base para cliente.xlsx") Then
                xlWorkBook = xlApp.Workbooks.Open("C:\Program Files\Convenios_licitaciones\base para cliente.xlsx")
            End If
            'xlWorkBook = xlApp.Workbooks.Open("C:\test1.xlsx")
            xlWorkSheet = xlWorkBook.Worksheets("Propuesta Original Cliente")

            xlApp.ScreenUpdating = False
            'edit the cell with new value
            xlWorkSheet.Cells(12, 4) = txtRazons.Text
            xlWorkSheet.Cells(13, 4) = txtRutcli.Text
            xlWorkSheet.Cells(15, 2) = "Precios tienen "
            xlWorkSheet.cells(28, 6) = "Consumo " + dtvendedor.Rows(0).Item("tipoconsumo").ToString
            xlWorkSheet.Cells(23, 2) = "Estos precios comienzan a regir desde : " + dtvendedor.Rows(0).Item("fecini_convenio")
            xlWorkSheet.Range("B15").Font.Color = Excel.XlRgbColor.rgbDarkRed
            Dim valorizadoSinMP As Double = 0
            Dim valorizadoConMP As Double = 0
            For i = 0 To dtvendedor.Rows.Count - 1
                If Not dtvendedor.Rows(i).Item("Estado") = "ACTIVO DIMERC" And
                    Not dtvendedor.Rows(i).Item("Estado") = "ARTICULO NUEVO" And
                    Not dtvendedor.Rows(i).Item("Estado") = "EXCLUSIVO CONVENIO" And
                    Not dtvendedor.Rows(i).Item("Estado") = "AGOTADO EN MERCADO" Then
                    xlWorkSheet.Range("B" + (i + 29).ToString + "", "J" + (i + 29).ToString + "").Interior.Color = Excel.XlRgbColor.rgbNavajoWhite
                    xlWorkSheet.Cells(i + 29, 2) = (i + 1).ToString
                    xlWorkSheet.Cells(i + 29, 3) = dtvendedor.Rows(i).Item("Codpro").ToString
                    xlWorkSheet.Cells(i + 29, 4) = dtvendedor.Rows(i).Item("Despro").ToString
                    xlWorkSheet.Cells(i + 29, 5) = dtvendedor.Rows(i).Item("Marca").ToString
                    xlWorkSheet.Cells(i + 29, 6) = 0
                    xlWorkSheet.Cells(i + 29, 7) = 0
                    xlWorkSheet.Cells(i + 29, 8) = 0
                    xlWorkSheet.Cells(i + 29, 9) = 0
                    Continue For
                End If

                If dtvendedor.Rows(i).Item("Alternativo") = "SI" Then
                    xlWorkSheet.Range("B" + (i + 29).ToString + "", "J" + (i + 29).ToString + "").Interior.Color = Excel.XlRgbColor.rgbLightGrey
                    xlWorkSheet.Cells(i + 29, 2) = (i + 1).ToString
                    xlWorkSheet.Cells(i + 29, 3) = dtvendedor.Rows(i).Item("Codpro").ToString
                    xlWorkSheet.Cells(i + 29, 4) = dtvendedor.Rows(i).Item("Despro").ToString
                    xlWorkSheet.Cells(i + 29, 5) = dtvendedor.Rows(i).Item("Marca").ToString
                    xlWorkSheet.Cells(i + 29, 6) = dtvendedor.Rows(i - 1).Item("Cantidad").ToString
                    xlWorkSheet.Cells(i + 29, 7) = dtvendedor.Rows(i - 1).Item("Precio").ToString
                    xlWorkSheet.Cells(i + 29, 8) = dtvendedor.Rows(i).Item("Precio").ToString
                    xlWorkSheet.Cells(i + 29, 9) = (Math.Round((dtvendedor.Rows(i).Item("Precio") - dtvendedor.Rows(i - 1).Item("Precio").ToString) / dtvendedor.Rows(i - 1).Item("Precio").ToString * 100, 2)) / 100
                    xlWorkSheet.Cells(i + 29, 10) = dtvendedor.Rows(i).Item("Original").ToString
                    valorizadoConMP = valorizadoConMP - dtvendedor.Rows(i - 1).Item("Precio").ToString * dtvendedor.Rows(i - 1).Item("Cantidad").ToString
                    valorizadoConMP += dtvendedor.Rows(i - 1).Item("Cantidad").ToString * dtvendedor.Rows(i).Item("Precio").ToString
                Else
                    xlWorkSheet.Cells(i + 29, 2) = (i + 1).ToString
                    xlWorkSheet.Cells(i + 29, 3) = dtvendedor.Rows(i).Item("Codpro").ToString
                    xlWorkSheet.Cells(i + 29, 4) = dtvendedor.Rows(i).Item("Despro").ToString
                    xlWorkSheet.Cells(i + 29, 5) = dtvendedor.Rows(i).Item("Marca").ToString
                    xlWorkSheet.Cells(i + 29, 6) = dtvendedor.Rows(i).Item("Cantidad").ToString
                    xlWorkSheet.Cells(i + 29, 7) = dtvendedor.Rows(i).Item("Precot").ToString
                    xlWorkSheet.Cells(i + 29, 8) = dtvendedor.Rows(i).Item("Precio").ToString
                    xlWorkSheet.Cells(i + 29, 9) = (Math.Round((dtvendedor.Rows(i).Item("Precio") - dtvendedor.Rows(i).Item("Precot")) / dtvendedor.Rows(i).Item("Precot") * 100, 2)) / 100
                    valorizadoSinMP += dtvendedor.Rows(i).Item("Precio").ToString * dtvendedor.Rows(i).Item("Cantidad").ToString
                    valorizadoConMP += dtvendedor.Rows(i).Item("Precio").ToString * dtvendedor.Rows(i).Item("Cantidad").ToString
                End If
                car.ProgressBar1.Value += 1
                Marshal.ReleaseComObject(xlWorkSheet.Cells)
            Next
            SQL = " select toting_old viejo,toting_new nuevo from en_reajuste_convenio where codemp = 3 "
            SQL += " And num_reajuste= " + reajuste.ToString
            SQL += " And numcot = " + numcot
            Dim dtcabeza = bd.sqlSelect(SQL)
            If dtcabeza.Rows.Count > 0 Then
                xlWorkSheet.Cells(22, 7) = dtcabeza.Rows(0).Item("viejo").ToString
                xlWorkSheet.Cells(22, 8) = valorizadoSinMP
                xlWorkSheet.Cells(26, 7) = dtcabeza.Rows(0).Item("viejo").ToString
                xlWorkSheet.Cells(26, 8) = valorizadoConMP
            End If

            xlApp.ScreenUpdating = True

            car.Close()
            xlApp.DisplayAlerts = False
            'xlWorkBook.Close(SaveChanges:=True)

            xlWorkBook.SaveAs("C:\dimerc\propuesta " & txtRazons.Text & " cliente.xlsx")
            xlWorkBook.Close()

            xlApp.DisplayAlerts = True
            xlApp.Quit()

            'xlWorkSheet.ReleaseComObject(pt)
            Marshal.ReleaseComObject(xlApp)
            Marshal.ReleaseComObject(xlWorkBook)
            Marshal.ReleaseComObject(xlWorkSheet)
            Dim dateEnd As Date = Date.Now
            End_Excel_App(datestart, dateEnd)

            Cursor.Current = Cursors.Arrow
            MessageBox.Show("Archivo creado y guardado en la ruta C:\dimerc\propuesta " & txtRazons.Text & " cliente.xlsx")
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles btn_GrabarPolinomio.Click
        Using conn = New OracleConnection(Conexion.retornaConexion)
            conn.Open()
            Dim comando As OracleCommand
            Dim dataadapter As OracleDataAdapter
            Dim sql As String
            PuntoControl.RegistroUso("222")
            Using transaccion = conn.BeginTransaction
                Try
                    'PRIMERO AL RUT DEL CLIENTE
                    If cmbLineaA.SelectedIndex > 0 Then
                        Dim dt As New DataTable
                        sql = "select 1 from ma_polinomio where codemp=3 and rutcli=" + txtRutcli.Text + " and letra='A'"
                        comando = New OracleCommand(sql, conn)
                        comando.Transaction = transaccion
                        dataadapter = New OracleDataAdapter(comando)
                        dataadapter.Fill(dt)
                        If dt.Rows.Count > 0 Then
                            sql = "update ma_polinomio set "
                            sql += " codlin =" + cmbLineaA.SelectedValue.ToString
                            sql += " ,ipc = " + txtIpcA.Text.Replace(",", ".")
                            sql += " ,dolar = " + txtDolarA.Text.Replace(",", ".")
                            sql += " ,celulosa = " + txtCelulosaA.Text.Replace(",", ".")
                            sql += " where codemp=3 and rutcli=" + txtRutcli.Text + " and letra ='A'"
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()
                        Else
                            sql = " insert into ma_polinomio(codemp,rutcli,codlin,letra,ipc,dolar,celulosa) "
                            sql = sql & "values(3," + txtRutcli.Text + ","
                            sql += cmbLineaA.SelectedValue.ToString + ","
                            sql += "'A',"
                            sql += txtIpcA.Text.Replace(",", ".") + ","
                            sql += txtDolarA.Text.Replace(",", ".") + ","
                            sql += txtCelulosaA.Text.Replace(",", ".") + ""
                            sql += ") "
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()
                        End If
                    End If
                    If cmbLineaB.SelectedIndex > 0 Then
                        Dim dt As New DataTable
                        sql = "select 1 from ma_polinomio where codemp=3 and rutcli=" + txtRutcli.Text + " and letra='B'"
                        comando = New OracleCommand(sql, conn)
                        comando.Transaction = transaccion
                        dataadapter = New OracleDataAdapter(comando)
                        dataadapter.Fill(dt)
                        If dt.Rows.Count > 0 Then
                            sql = "update ma_polinomio set "
                            sql += " codlin =" + cmbLineaB.SelectedValue.ToString
                            sql += " ,ipc = " + txtIpcB.Text.Replace(",", ".")
                            sql += " ,dolar = " + txtDolarB.Text.Replace(",", ".")
                            sql += " ,celulosa = " + txtCelulosaB.Text.Replace(",", ".")
                            sql += " where codemp=3 and rutcli=" + txtRutcli.Text + " and letra ='B'"
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()
                        Else
                            sql = " insert into ma_polinomio(codemp,rutcli,codlin,letra,ipc,dolar,celulosa) "
                            sql = sql & "values(3," + txtRutcli.Text + ","
                            sql += cmbLineaB.SelectedValue.ToString + ","
                            sql += "'B',"
                            sql += txtIpcB.Text.Replace(",", ".") + ","
                            sql += txtDolarB.Text.Replace(",", ".") + ","
                            sql += txtCelulosaB.Text.Replace(",", ".") + ""
                            sql += ") "
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()
                        End If
                    End If
                    If cmbLineaC.SelectedIndex > 0 Then
                        Dim dt As New DataTable
                        sql = "select 1 from ma_polinomio where codemp=3 and rutcli=" + txtRutcli.Text + " and letra='C'"
                        comando = New OracleCommand(sql, conn)
                        comando.Transaction = transaccion
                        dataadapter = New OracleDataAdapter(comando)
                        dataadapter.Fill(dt)
                        If dt.Rows.Count > 0 Then
                            sql = "update ma_polinomio set "
                            sql += " codlin =" + cmbLineaC.SelectedValue.ToString
                            sql += " ,ipc = " + txtIpcC.Text.Replace(",", ".")
                            sql += " ,dolar = " + txtDolarC.Text.Replace(",", ".")
                            sql += " ,celulosa = " + txtCelulosaC.Text.Replace(",", ".")
                            sql += " where codemp=3 and rutcli=" + txtRutcli.Text + " and letra ='C'"
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()
                        Else
                            sql = " insert into ma_polinomio(codemp,rutcli,codlin,letra,ipc,dolar,celulosa) "
                            sql = sql & "values(3," + txtRutcli.Text + ","
                            sql += cmbLineaC.SelectedValue.ToString + ","
                            sql += "'C',"
                            sql += txtIpcC.Text.Replace(",", ".") + ","
                            sql += txtDolarC.Text.Replace(",", ".") + ","
                            sql += txtCelulosaC.Text.Replace(",", ".") + ""
                            sql += ") "
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()
                        End If
                    End If
                    If cmbLineaD.SelectedIndex > 0 Then
                        Dim dt As New DataTable
                        sql = "select 1 from ma_polinomio where codemp=3 and rutcli=" + txtRutcli.Text + " and letra='D'"
                        comando = New OracleCommand(sql, conn)
                        comando.Transaction = transaccion
                        dataadapter = New OracleDataAdapter(comando)
                        dataadapter.Fill(dt)
                        If dt.Rows.Count > 0 Then
                            sql = "update ma_polinomio set "
                            sql += " codlin =" + cmbLineaD.SelectedValue.ToString
                            sql += " ,ipc = " + txtIpcD.Text.Replace(",", ".")
                            sql += " ,dolar = " + txtDolarD.Text.Replace(",", ".")
                            sql += " ,celulosa = " + txtCelulosaD.Text.Replace(",", ".")
                            sql += " where codemp=3 and rutcli=" + txtRutcli.Text + " and letra ='D'"
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()
                        Else
                            sql = " insert into ma_polinomio(codemp,rutcli,codlin,letra,ipc,dolar,celulosa) "
                            sql = sql & "values(3," + txtRutcli.Text + ","
                            sql += cmbLineaD.SelectedValue.ToString + ","
                            sql += "'D',"
                            sql += txtIpcD.Text.Replace(",", ".") + ","
                            sql += txtDolarD.Text.Replace(",", ".") + ","
                            sql += txtCelulosaD.Text.Replace(",", ".") + ""
                            sql += ") "
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()
                        End If
                    End If
                    If chkEliminaA.Checked = True Then
                        sql = "delete from ma_polinomio where codemp=3 and rutcli=" + txtRutcli.Text + " and letra='A'"
                        comando = New OracleCommand(sql, conn)
                        comando.Transaction = transaccion
                        comando.ExecuteNonQuery()
                    End If
                    If chkEliminaB.Checked = True Then
                        sql = "delete from ma_polinomio where codemp=3 and rutcli=" + txtRutcli.Text + " and letra='B'"
                        comando = New OracleCommand(sql, conn)
                        comando.Transaction = transaccion
                        comando.ExecuteNonQuery()
                    End If
                    If chkEliminaC.Checked = True Then
                        sql = "delete from ma_polinomio where codemp=3 and rutcli=" + txtRutcli.Text + " and letra='C'"
                        comando = New OracleCommand(sql, conn)
                        comando.Transaction = transaccion
                        comando.ExecuteNonQuery()
                    End If
                    If chkEliminaD.Checked = True Then
                        sql = "delete from ma_polinomio where codemp=3 and rutcli=" + txtRutcli.Text + " and letra='D'"
                        comando = New OracleCommand(sql, conn)
                        comando.Transaction = transaccion
                        comando.ExecuteNonQuery()
                    End If
                    If chkRelacionado.Checked = True Then
                        'AHORA REVISO SI TIENE RELACIONADOS Y LE HAGO LO MISMO
                        sql = " select rutcli,getrazonsocial(codemp,rutcli) razons "
                        sql = sql & "FROM re_emprela  "
                        sql = sql & "where numrel in(select numrel from re_emprela where rutcli= " + txtRutcli.Text + " and codemp=3)   "
                        sql = sql & "and rutcli not in(select rutcli from re_emprela where rutcli= " + txtRutcli.Text + "  and codemp=3)order by rutcli "
                        Dim relacionado = bd.sqlSelect(sql)

                        If relacionado.Rows.Count > 0 Then
                            For i = 0 To relacionado.Rows.Count - 1
                                Dim rutrelacionado = relacionado.Rows(i).Item("rutcli").ToString

                                If cmbLineaA.SelectedIndex > 0 Then
                                    Dim dt As New DataTable
                                    sql = "select 1 from ma_polinomio where codemp=3 and rutcli=" + rutrelacionado + " and letra='A'"
                                    comando = New OracleCommand(sql, conn)
                                    comando.Transaction = transaccion
                                    dataadapter = New OracleDataAdapter(comando)
                                    dataadapter.Fill(dt)
                                    If dt.Rows.Count > 0 Then
                                        sql = "update ma_polinomio set "
                                        sql += " codlin =" + cmbLineaA.SelectedValue.ToString
                                        sql += " ,ipc = " + txtIpcA.Text.Replace(",", ".")
                                        sql += " ,dolar = " + txtDolarA.Text.Replace(",", ".")
                                        sql += " ,celulosa = " + txtCelulosaA.Text.Replace(",", ".")
                                        sql += " where codemp=3 and rutcli=" + rutrelacionado + " and letra ='A'"
                                        comando = New OracleCommand(sql, conn)
                                        comando.Transaction = transaccion
                                        comando.ExecuteNonQuery()
                                    Else
                                        sql = " insert into ma_polinomio(codemp,rutcli,codlin,letra,ipc,dolar,celulosa) "
                                        sql = sql & "values(3," + rutrelacionado + ","
                                        sql += cmbLineaA.SelectedValue.ToString + ","
                                        sql += "'A',"
                                        sql += txtIpcA.Text.Replace(",", ".") + ","
                                        sql += txtDolarA.Text.Replace(",", ".") + ","
                                        sql += txtCelulosaA.Text.Replace(",", ".") + ""
                                        sql += ") "
                                        comando = New OracleCommand(sql, conn)
                                        comando.Transaction = transaccion
                                        comando.ExecuteNonQuery()
                                    End If
                                End If
                                If cmbLineaB.SelectedIndex > 0 Then
                                    Dim dt As New DataTable
                                    sql = "select 1 from ma_polinomio where codemp=3 and rutcli=" + rutrelacionado + " and letra='B'"
                                    comando = New OracleCommand(sql, conn)
                                    comando.Transaction = transaccion
                                    dataadapter = New OracleDataAdapter(comando)
                                    dataadapter.Fill(dt)
                                    If dt.Rows.Count > 0 Then
                                        sql = "update ma_polinomio set "
                                        sql += " codlin =" + cmbLineaB.SelectedValue.ToString
                                        sql += " ,ipc = " + txtIpcB.Text.Replace(",", ".")
                                        sql += " ,dolar = " + txtDolarB.Text.Replace(",", ".")
                                        sql += " ,celulosa = " + txtCelulosaB.Text.Replace(",", ".")
                                        sql += " where codemp=3 and rutcli=" + rutrelacionado + " and letra ='B'"
                                        comando = New OracleCommand(sql, conn)
                                        comando.Transaction = transaccion
                                        comando.ExecuteNonQuery()
                                    Else
                                        sql = " insert into ma_polinomio(codemp,rutcli,codlin,letra,ipc,dolar,celulosa) "
                                        sql = sql & "values(3," + rutrelacionado + ","
                                        sql += cmbLineaB.SelectedValue.ToString + ","
                                        sql += "'B',"
                                        sql += txtIpcB.Text.Replace(",", ".") + ","
                                        sql += txtDolarB.Text.Replace(",", ".") + ","
                                        sql += txtCelulosaB.Text.Replace(",", ".") + ""
                                        sql += ") "
                                        comando = New OracleCommand(sql, conn)
                                        comando.Transaction = transaccion
                                        comando.ExecuteNonQuery()
                                    End If
                                End If
                                If cmbLineaC.SelectedIndex > 0 Then
                                    Dim dt As New DataTable
                                    sql = "select 1 from ma_polinomio where codemp=3 and rutcli=" + rutrelacionado + " and letra='C'"
                                    comando = New OracleCommand(sql, conn)
                                    comando.Transaction = transaccion
                                    dataadapter = New OracleDataAdapter(comando)
                                    dataadapter.Fill(dt)
                                    If dt.Rows.Count > 0 Then
                                        sql = "update ma_polinomio set "
                                        sql += " codlin =" + cmbLineaC.SelectedValue.ToString
                                        sql += " ,ipc = " + txtIpcC.Text.Replace(",", ".")
                                        sql += " ,dolar = " + txtDolarC.Text.Replace(",", ".")
                                        sql += " ,celulosa = " + txtCelulosaC.Text.Replace(",", ".")
                                        sql += " where codemp=3 and rutcli=" + rutrelacionado + " and letra ='C'"
                                        comando = New OracleCommand(sql, conn)
                                        comando.Transaction = transaccion
                                        comando.ExecuteNonQuery()
                                    Else
                                        sql = " insert into ma_polinomio(codemp,rutcli,codlin,letra,ipc,dolar,celulosa) "
                                        sql = sql & "values(3," + rutrelacionado + ","
                                        sql += cmbLineaC.SelectedValue.ToString + ","
                                        sql += "'C',"
                                        sql += txtIpcC.Text.Replace(",", ".") + ","
                                        sql += txtDolarC.Text.Replace(",", ".") + ","
                                        sql += txtCelulosaC.Text.Replace(",", ".") + ""
                                        sql += ") "
                                        comando = New OracleCommand(sql, conn)
                                        comando.Transaction = transaccion
                                        comando.ExecuteNonQuery()
                                    End If
                                End If
                                If cmbLineaD.SelectedIndex > 0 Then
                                    Dim dt As New DataTable
                                    sql = "select 1 from ma_polinomio where codemp=3 and rutcli=" + rutrelacionado + " and letra='D'"
                                    comando = New OracleCommand(sql, conn)
                                    comando.Transaction = transaccion
                                    dataadapter = New OracleDataAdapter(comando)
                                    dataadapter.Fill(dt)
                                    If dt.Rows.Count > 0 Then
                                        sql = "update ma_polinomio set "
                                        sql += " codlin =" + cmbLineaD.SelectedValue.ToString
                                        sql += " ,ipc = " + txtIpcD.Text.Replace(",", ".")
                                        sql += " ,dolar = " + txtDolarD.Text.Replace(",", ".")
                                        sql += " ,celulosa = " + txtCelulosaD.Text.Replace(",", ".")
                                        sql += " where codemp=3 and rutcli=" + rutrelacionado + " and letra ='D'"
                                        comando = New OracleCommand(sql, conn)
                                        comando.Transaction = transaccion
                                        comando.ExecuteNonQuery()
                                    Else
                                        sql = " insert into ma_polinomio(codemp,rutcli,codlin,letra,ipc,dolar,celulosa) "
                                        sql = sql & "values(3," + rutrelacionado + ","
                                        sql += cmbLineaD.SelectedValue.ToString + ","
                                        sql += "'D',"
                                        sql += txtIpcD.Text.Replace(",", ".") + ","
                                        sql += txtDolarD.Text.Replace(",", ".") + ","
                                        sql += txtCelulosaD.Text.Replace(",", ".") + ""
                                        sql += ") "
                                        comando = New OracleCommand(sql, conn)
                                        comando.Transaction = transaccion
                                        comando.ExecuteNonQuery()
                                    End If
                                End If
                                If chkEliminaA.Checked = True Then
                                    sql = "delete from ma_polinomio where codemp=3 and rutcli=" + rutrelacionado + " and letra='A'"
                                    comando = New OracleCommand(sql, conn)
                                    comando.Transaction = transaccion
                                    comando.ExecuteNonQuery()
                                End If
                                If chkEliminaB.Checked = True Then
                                    sql = "delete from ma_polinomio where codemp=3 and rutcli=" + rutrelacionado + " and letra='B'"
                                    comando = New OracleCommand(sql, conn)
                                    comando.Transaction = transaccion
                                    comando.ExecuteNonQuery()
                                End If
                                If chkEliminaC.Checked = True Then
                                    sql = "delete from ma_polinomio where codemp=3 and rutcli=" + rutrelacionado + " and letra='C'"
                                    comando = New OracleCommand(sql, conn)
                                    comando.Transaction = transaccion
                                    comando.ExecuteNonQuery()
                                End If
                                If chkEliminaD.Checked = True Then
                                    sql = "delete from ma_polinomio where codemp=3 and rutcli=" + rutrelacionado + " and letra='D'"
                                    comando = New OracleCommand(sql, conn)
                                    comando.Transaction = transaccion
                                    comando.ExecuteNonQuery()
                                End If
                            Next
                        End If
                    End If

                    transaccion.Commit()
                    MessageBox.Show("Cambios de polinomio hechos con éxito")
                    limpiapolinomio()
                    buscapolinomio()

                Catch ex As Exception
                    If Not transaccion.Equals(Nothing) Then
                        transaccion.Rollback()
                    End If
                    PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                    MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Finally
                    bd.close()
                End Try
            End Using
        End Using
    End Sub

    Private Sub txtIpcA_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtIpcA.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            txtDolarA.Focus()
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtDolarA_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDolarA.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            txtCelulosaA.Focus()
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtCelulosaA_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCelulosaA.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            cmbLineaB.Focus()
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub cmbLineaA_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbLineaA.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            txtIpcA.Focus()
        End If
    End Sub

    Private Sub cmbLineaB_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbLineaB.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            txtIpcB.Focus()
        End If
    End Sub

    Private Sub cmbLineaC_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbLineaC.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            txtIpcC.Focus()
        End If
    End Sub

    Private Sub cmbLineaD_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbLineaD.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            txtIpcD.Focus()
        End If
    End Sub

    Private Sub txtIpcB_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtIpcB.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            txtDolarB.Focus()
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtDolarB_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDolarB.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            txtCelulosaB.Focus()
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtCelulosaB_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCelulosaB.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            cmbLineaC.Focus()
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtIpcC_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtIpcC.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            txtDolarC.Focus()
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtDolarC_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDolarC.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            txtCelulosaC.Focus()
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtCelulosaC_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCelulosaC.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            cmbLineaD.Focus()
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtIpcD_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtIpcD.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            txtDolarD.Focus()
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtDolarD_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDolarD.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            txtCelulosaD.Focus()
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtCelulosaD_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCelulosaD.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            btn_GrabarPolinomio.Focus()
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtRazons_Click(sender As Object, e As EventArgs) Handles txtRazons.Click
        txtRazons.SelectAll()
        limpiar()
    End Sub

    Private Sub txtRutcli_Click(sender As Object, e As EventArgs) Handles txtRutcli.Click
        txtRutcli.SelectAll()
        limpiar()
    End Sub

    Private Sub btn_Base_Click(sender As Object, e As EventArgs) Handles btn_Base.Click
        PuntoControl.RegistroUso("219")
        exportar()
    End Sub

End Class