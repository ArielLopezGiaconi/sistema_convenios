﻿Public Class frmConsultaReajustesporDia
    Dim bd As New Conexion
    Dim PuntoControl As New PuntoControl
    Private Sub btn_Salir_Click(sender As Object, e As EventArgs) Handles btn_Salir.Click
        Close()
    End Sub

    Private Sub frmConsultaReajustesporDia_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        PuntoControl.RegistroUso("210")
        Me.StartPosition = FormStartPosition.Manual
        Me.Location = New Point(0, 0)
        cargacombo()
    End Sub

    Private Sub btn_Excel_Click(sender As Object, e As EventArgs) Handles btn_Excel.Click
        PuntoControl.RegistroUso("212")
        Dim save As New SaveFileDialog
        If dgvConsulta.RowCount = 0 Then
            MessageBox.Show("No hay datos consultados en la grilla, procese datos antes de exportar", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        Else
            save.Filter = "Archivo Excel | *.xlsx"
            If save.ShowDialog = System.Windows.Forms.DialogResult.OK Then
                Globales.exporta(save.FileName, dgvConsulta)
            End If
        End If
    End Sub
    Private Sub cargacombo()
        Try
            bd.open_dimerc()
            Dim sql = " select '<<<TODOS>>>' usr_creac from dual "
            sql += " union select distinct usr_creac from en_reajuste_convenio"
            Dim dt = bd.sqlSelect(sql)

            cmbUsuario.DataSource = dt
            cmbUsuario.SelectedIndex = -1
            cmbUsuario.ValueMember = "usr_creac"
            cmbUsuario.DisplayMember = "usr_creac"
            cmbUsuario.Refresh()


        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub
    Private Sub btn_Buscar_Click(sender As Object, e As EventArgs) Handles btn_Buscar.Click
        PuntoControl.RegistroUso("211")
        Try
            bd.open_dimerc()
            Dim Sql = " select a.rutcli ""Rutcli"",getrazonsocial(a.codemp,a.rutcli) ""Razon Social"",a.num_reajuste ""Num. Reajuste"", "
            Sql = Sql & " a.numcot ""Num. Cotizacion"",GET_NOMBRE(b.codven,1) ""Vendedor"",a.usr_creac ""Usuario"","
            Sql = Sql & " a.fecha_creac ""Fecha Creacion"",a.fecini_convenio ""Desde"",a.fecfin_convenio ""Vence"", a.observacion ""Observacion"", "
            Sql = Sql & " getdominio(278,a.estado) ""Estado"",nvl(c.numrel,0) numrel,b.cencos "
            Sql = Sql & " from en_reajuste_convenio a,en_cotizac b,re_emprela c "
            Sql = Sql & "where a.codemp=3  "
            If Not cmbUsuario.SelectedValue.ToString = "<<<TODOS>>>" Then
                Sql = Sql & "and a.usr_creac='" + cmbUsuario.SelectedValue.ToString + "'"
            End If
            Sql = Sql & " and a.fecha_creac between to_date('" + dtpFechaIni.Value.Date + " 00:00:00','dd/mm/yyyy HH24:mi:ss')  "
            Sql = Sql & " and to_date('" + dtpFechaFin.Value.Date + " 23:59:59','dd/mm/yyyy HH24:mi:ss') "
            Sql = Sql & "And a.codemp=b.codemp "
            Sql = Sql & "And a.numcot=b.numcot "
            Sql = Sql & "And a.codemp=c.codemp(+) "
            Sql = Sql & "And a.rutcli=c.rutcli(+) order by a.num_reajuste desc"
            Dim dt = bd.sqlSelect(Sql)
            If dt.Rows.Count = 0 Then
                MessageBox.Show("No hay reajustes hechos para esta fecha", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            Else
                dgvConsulta.DataSource = dt
                dgvConsulta.Columns("numrel").Visible = False
                dgvConsulta.Columns("cencos").Visible = False
                dgvConsulta.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells

            End If
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub dtpFechaLineaReajuste_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFechaIni.KeyPress, dtpFechaFin.KeyPress
        btn_Buscar.PerformClick()
    End Sub

    Private Sub cmbUsuario_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbUsuario.KeyPress
        btn_Buscar.PerformClick()
    End Sub
End Class