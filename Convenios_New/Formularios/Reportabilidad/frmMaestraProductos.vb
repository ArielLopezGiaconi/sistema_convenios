﻿Public Class frmMaestraProductos
    Private Sub btnCargar_Click(sender As Object, e As EventArgs) Handles btnCargar.Click
        If cmbEstpro.SelectedIndex >= 0 Then
            PuntoControl.RegistroUso("224")
            Dim datos As New Conexion
            datos.open_dimerc()
            Dim sql As String
            sql = "SELECT A.CODPRO, A.DESPRO, E.DESCRIP_ALTER DESPRO_ALT,
                    SAP_GET_DOMINIO(20, ESTPRO) ESTADO, C.DESLIN LINEA,
                    D.DESMAR MARCA, SAP_GET_ROTACION(3,A.CODPRO) ROTACION,
                    A.DESGRU GRUPO, F.DESCRIPCION SUBGRUPO, SAP_GET_PROVEEDOR(A.CODPRO) PROVEEDOR,
                    SAP_GET_DOMINIO(246,G.CODCLASIFICA) CATEGORIA_PRICING,
                    SAP_GET_DOMINIO(358,A.TIPOPROD) TIPO, H.DESCRIPCION PROYECTO,
                    SAP_GET_DOMINIO(861, I.VALOR) CM, A.PALETA_DC,
                    SAP_GET_DOMINIO(1002, A.CODNEG) NEGOCIO,
                    DECODE(NVL(J.MPROPIA,0),1,'S','N') MARCA_PROPIA,
                    GET_MPROPIA_INVENTARIO_DIMERC(a.codpro) MARCA_PROPIA_INVENTARIO,
                    DECODE(A.PEDSTO,'S','S','P') STOCK_PEDIDO, A.COSPROM, A.COSTO COSCOM,
                    CASE WHEN K.CODTIP IS NOT NULL THEN
                        DECODE(K.CODTIP,'P','%','$')
                    END TIPO_DELTA, K.VALOR VALOR_DELTA,
                    CASE WHEN A.COSTO> 0 THEN
                        ROUND(((A.COSTO-A.COSPROM)/A.COSTO) * 100,2)
                    END PORC_CM, E.PORC_APORTE, A.XDELTA,
                    nvl(to_char(GETULTIMACOMPRA(3,'B000429','F')),'') F_ULTCOMPRA,
                    nvl(GETULTIMACOMPRA(3,'B000429','V'),0) ULTCOMPRA,
                    NVL(L.STOCKS,0) STOCK_MULTIBODEGAS, NVL(M.STOCKS,0) STOCK_ANTOFAGASTA,
                    A.PESO,A.VOLUMEN,E.TEXTO_LIBRE, B.CODPRO_ALT CODIGO_COMPETENCIA, B.DESCRIPCION DESCRIPCION_COMPETENCIA,B.RUTCOMPETIDOR
                    FROM MA_PRODUCT A, RE_PRODUCTO_NEGOCIO B, MA_LINEAPR C, MA_MARCAPR D,
                    MA_PRODUCT_NEGOCIO E, MA_NEGOCIO_SUBGRUPO F, RE_CLAPROD G, MA_NEGOCIO_PRICING H,
                    RE_PRODUCTO_CM I, DM_VENTAS.QV_MARCA_PROPIA J, MA_COSTO_COMERCIAL K, RE_BODPROD_DIMERC L, RE_BODPROD_DIMERC M
                    WHERE A.CODPRO = B.CODPRO(+)
                    AND A.CODLIN = C.CODLIN(+)
                    AND A.CODMAR = D.CODMAR(+)
                    AND A.CODPRO = E.CODPRO(+)
                    AND E.ID_SUBGRUPO = F.ID(+)
                    AND A.CODPRO = G.CODPRO(+)
                    AND G.CODGEN(+) = 246
                    AND E.ID_NEGOCIO = H.ID(+)
                    AND A.CODPRO = I.CODPRO(+)
                    AND I.CODEMP(+) = 3
                    AND A.CODPRO = J.CODDIM(+)
                    AND J.CODEMP(+) = 3
                    AND A.CODPRO = K.CODPRO(+)
                    AND K.CODEMP(+) = 3
                    AND A.CODPRO = L.CODPRO(+)
                    AND L.CODBOD(+) = 1
                    AND L.CODEMP(+) = 3
                    AND A.CODPRO = M.CODPRO(+)
                    AND M.CODBOD(+) = 66
                    AND M.CODEMP(+) = 3
                    AND B.TIPO(+) = 'CT'
                    AND A.ESTPRO = " & cmbEstpro.SelectedValue
            Dim tabla = datos.sqlSelect(sql)
            dgvProds.DataSource = tabla
        Else
            MsgBox("Seleccione estado")
        End If
    End Sub

    Private Sub frmMaestraProductos_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        Dim datos As New Conexion
        datos.open_dimerc()
        Dim tabla = datos.sqlSelect("SELECT DESVAL, CODVAL
                                     FROM DE_DOMINIO
                                     WHERE CODDOM = 20")
        cmbEstpro.DataSource = tabla
        cmbEstpro.ValueMember = "CODVAL"
        cmbEstpro.DisplayMember = "DESVAL"
        cmbEstpro.SelectedIndex = -1
        cmbEstpro.Refresh()
    End Sub

    Private Sub btnExportar_Click(sender As Object, e As EventArgs) Handles btnExportar.Click
        If dgvProds.Rows.Count > 0 Then
            PuntoControl.RegistroUso("225")
            Globales.DataTableToExcel(CType(dgvProds.DataSource, DataTable).Copy(), dgvProds) 'exporta(dialogo.FileName, dgvProds)
            'Dim dialogo = New SaveFileDialog
            'dialogo.Filter = "Excel|*.xls"
            'Dim ruta As DialogResult = dialogo.ShowDialog()
            'If ruta = DialogResult.OK Then
            '    If dialogo.FileName <> "" Then
            '        Globales.DataTableToExcel(CType(dgvProds.DataSource, DataTable).Copy(), dgvProds) 'exporta(dialogo.FileName, dgvProds)
            '    Else
            '        MsgBox("Ingrese nombre de archivo")
            '    End If
            'End If
        Else
            MsgBox("Cargar Productos antes de exportar")
        End If
    End Sub

    Private Sub frmMaestraProductos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        PuntoControl.RegistroUso("223")
        Me.Location = New Point(0, 0)
    End Sub
End Class