﻿Public Class frmConsultaRutRelacionado
    Dim bd As New Conexion
    Dim PuntoControl As New PuntoControl
    Public Sub New()

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub
    Public Sub New(rut As String, razons As String)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()
        txtRut.Text = rut
        txtRazons.Text = razons

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub
    Private Sub frmConsultaRutRelacionado_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        PuntoControl.RegistroUso("213")
        Me.StartPosition = FormStartPosition.Manual
        Me.Location = New Point(0, 0)
        If txtRut.Text <> "" And txtRazons.Text <> "" Then
            buscarrelaciones()
        End If
    End Sub

    Private Sub btn_Salir_Click(sender As Object, e As EventArgs) Handles btn_Salir.Click
        Me.Close()
    End Sub

    Private Sub txtRut_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtRut.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            PuntoControl.RegistroUso("214")
            buscarrelaciones()
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub
    Private Sub buscarrelaciones()
        Try
            bd.open_dimerc()
            Dim sql = "select razons from en_cliente where rutcli='" & txtRut.Text & "' and codemp='3'"
            Dim dt = bd.sqlSelect(sql)
            Dim tip As String
            If dt.Rows.Count > 0 Then
                txtRazons.Text = dt.Rows(0).Item("razons").ToString
                If chkComercial.Checked = True And chkFinanciera.Checked = True Then
                    tip = "('1','2')"
                Else
                    If chkFinanciera.Checked = True Then
                        tip = "('1')"
                    Else
                        tip = "('2')"
                    End If
                End If

                sql = "select " &
                "a.codemp,a.numrel,a.rutcli,a.tipo_relacion,a.empresa,a.razons,a.userid," &
                "a.neto,nvl(b.numcot,'0') as numcot,nvl(b.cencos,'0') as cencos,b.fecemi , b.fecven," &
                "case " &
                "when b.fecven>=sysdate then 'SI' " &
                "Else    'NO' " &
                "end As activo " &
                "From " &
                "(" &
                    "select " &
                    "codemp , numrel, rutcli, tipo_relacion, empresa," &
                    "razons , userid, neto " &
                    "From " &
                    "(" &
                        "select " &
                        "a.codemp,a.numrel,a.rutcli," &
                        "decode(a.estado,'2','Relacion Comecial','Relacion comercial') as tipo_relacion," &
                        "decode(a.codemp,'3','DIMERC','OFIMARKET') as empresa,b.razons,c.userid," &
                        "decode(Sum(neto), Null, 0, Sum(neto)) As neto " &
                        "from re_emprela a Left Join " &
                        "(" &
                            "select rutcli,codemp,sum(neto) as neto " &
                            "From " &
                            "("
                sql = sql & "select " &
                                "rutcli,codemp,tipo,decode(tipo,'N',sum(neto)*-1,sum(neto)) as neto " &
                                "From qv_control_margen " &
                                "where fecha<to_date('01/'||to_char(sysdate,'mm/yyyy'),'dd/mm/yyyy')" &
                                "and fecha>=add_months(to_date('01/'||to_char(sysdate,'mm/yyyy'),'dd/mm/yyyy'),-3)" &
                                "and rutcli in " &
                                "(select rutcli from re_emprela where numrel in " &
                                "(select numrel from re_emprela where rutcli='" & txtRut.Text & "' and estado in " & tip & ")) " &
                                "group by rutcli,codemp,tipo" &
                            ")" &
                        "group by rutcli,codemp " &
                        ") d " &
                    "on a.rutcli=d.rutcli and a.codemp=d.codemp,en_cliente b,ma_usuario c " &
                    "where a.numrel in " &
                    "(select numrel from re_emprela where rutcli='" & txtRut.Text & "' and estado in " & tip & ") " &
                    "and a.rutcli=b.rutcli and a.codemp=b.codemp and b.codven=c.rutusu " &
                    "group by a.numrel,a.rutcli,a.estado,a.codemp,b.razons,c.userid " &
                    ")" &
                ") a " &
                "left outer join " &
            "(" &
                "select " &
                "a.codemp , a.rutcli, a.cencos, max(a.numcot) as numcot," &
                "max(a.fecemi) as fecemi, max(a.fecven) as fecven " &
                "from en_conveni a,"
                sql = sql & "(" &
                        "select codemp,rutcli " &
                        "from re_emprela " &
                        "where numrel in (select numrel from re_emprela where rutcli='" & txtRut.Text & "' and estado in " & tip & ") " &
                    ") b " &
                "Where a.rutcli = b.rutcli And a.codemp = b.codemp " &
                "group by a.codemp,a.rutcli,a.cencos " &
                ") b on a.rutcli=b.rutcli and a.codemp=b.codemp order by neto desc "
                Dim dtDatos = bd.sqlSelect(sql)
                If dtDatos.Rows.Count > 0 Then
                    dgvDetalle.DataSource = dtDatos
                    dgvDetalle.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
                    dgvDetalle.Columns("Neto").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvDetalle.Columns("Neto").DefaultCellStyle.Format = "n0"
                Else
                    MessageBox.Show("Cliente sin la relación indicada", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Exit Sub
                End If
            Else
                MessageBox.Show("Rut inexistente, verificar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            End If
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub btn_Excel_Click(sender As Object, e As EventArgs) Handles btn_Excel.Click
        Dim save As New SaveFileDialog
        PuntoControl.RegistroUso("215")
        If dgvDetalle.RowCount = 0 Then
            MessageBox.Show("No hay datos consultados en la grilla, procese datos antes de exportar", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        Else
            save.Filter = "Archivo Excel | *.xlsx"
            If save.ShowDialog = System.Windows.Forms.DialogResult.OK Then
                Globales.exporta(save.FileName, dgvDetalle)
            End If
        End If
    End Sub
End Class