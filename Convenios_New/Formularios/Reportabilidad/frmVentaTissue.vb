﻿Public Class frmVentaTissue
    Private Sub btnCargar_Click(sender As Object, e As EventArgs) Handles btnCargar.Click
        PuntoControl.RegistroUso("227")
        Dim datos As New Conexion
        Dim sql As String
        'sql = "select z1.rutcli, z1.codpro, z1.cantid,
        'Trim(to_char(z1.totnet, '999G999G999', 'NLS_NUMERIC_CHARACTERS='',.''')) costo, 
        '        b.peso_gr peso_kg, trim(to_char(trunc(z1.cantid * b.peso_gr,2), '999G999G990D99', 'NLS_NUMERIC_CHARACTERS='',.''')) venta_kg,
        '        trim(to_char(trunc((1000 * (z1.totnet/z1.cantid))/b.peso_gr,2), '999G999G999D99', 'NLS_NUMERIC_CHARACTERS='',.''')) valor_tonelada from
        '        (select a.rutcli,a.codpro, sum(decode(tipo,'N',cantid*-1,cantid)) cantid, sum(decode(tipo,'N',costo*-1,costo)) totnet from qv_control_margen a
        '        where a.fecha between '" & dtFecini.Value.ToShortDateString & "' and '" & dtFecfin.Value.ToShortDateString & "'
        '        and tipo in('F','N')
        '        group by a.rutcli, a.codpro) z1, ma_producto_atrib b
        '        where z1.codpro = b.codpro
        '        and z1.codpro = '" & cmbCodpro.SelectedItem.ToString & "'"
        sql = "select z1.rutcli, getrazonsocial(3,z1.rutcli) razons, sap_get_negocio_eerr(3,z1.rutcli) UDN_EERR,
                sap_get_numrel(3,z1.rutcli) relacion, 
                z1.codpro,sap_get_descripcion(z1.codpro) descripcion, sap_get_marca(z1.codpro) MARCA,
                b.peso_gr ""PESO UNIDAD"",
                trim(to_char(trunc((1000 * (decode(z1.costocte,0,z1.cosprom,z1.costocte)/z1.cantid))/b.peso_gr,2), '999G999G999', 'NLS_NUMERIC_CHARACTERS='',.''')) valor_tonelada,
                trim(to_char(trunc((1000 * (z1.totnet/z1.cantid))/b.peso_gr,2), '999G999G999', 'NLS_NUMERIC_CHARACTERS='',.''')) valor_tonelada_neto,
                trim(to_char(decode(z1.costocte,0,z1.cosprom,z1.costocte), '999G999G999', 'NLS_NUMERIC_CHARACTERS='',.''')) ""COSTO DE VENTA"",
                trim(to_char(z1.cosprom, '999G999G999', 'NLS_NUMERIC_CHARACTERS='',.''')) cosprom,
                trim(to_char(z1.totnet, '999G999G999', 'NLS_NUMERIC_CHARACTERS='',.''')) ""TOTAL NETO"",
                z1.cantid,
                trim(to_char(trunc(z1.cantid * b.peso_gr,2), '999G999G990D99', 'NLS_NUMERIC_CHARACTERS='',.''')) ""VENTA KILOS"",
                z2.codpro_alt codpro_alter, sap_get_descripcion(z2.codpro_alt) descrip_alter, sap_get_marca(z2.codpro) marca_alter,
                trim(to_char(trunc((1000 * (z2.cosprom/1))/z2.peso_gr,2), '999G999G999', 'NLS_NUMERIC_CHARACTERS='',.''')) valor_tonelada_alter,
                trunc(((((1000 * (decode(z1.costocte,0,z1.cosprom,z1.costocte)/z1.cantid))/b.peso_gr) - ((1000 * (z2.cosprom/1))/z2.peso_gr))/ ((1000 * (decode(z1.costocte,0,z1.cosprom,z1.costocte)/z1.cantid))/b.peso_gr) * 100),2) variacion
                from
                (select a.rutcli,a.codpro,
                 sum(decode(tipo,'N',cantid*-1,cantid)) cantid, 
                 sum(decode(tipo,'N',costo*-1,costo)) costo,
                 sum(decode(tipo,'N',neto*-1,neto)) totnet,
                 sum(decode(tipo,'N',cosprom*-1,cosprom)) cosprom,
                 sum(decode(tipo,'N',ctocte*-1,ctocte)) costocte
                 from qv_control_margen a, ma_producto_atrib b
                where a.codpro = b.codpro
                and a.fecha between '" & dtFecini.Value.ToShortDateString & "' and '" & dtFecfin.Value.ToShortDateString & "'
                and tipo in('F','N')
                group by a.rutcli, a.codpro) z1,
                (select c.codpro,codpro_alt,sap_get_costopromedio(3,'2001',codpro_alt,'10') cosprom,e.PESO_GR
                 from re_producto_tissue c, ma_producto_atrib e
                 where c.codpro = e.codpro
                and c.tipo = 1) z2, ma_producto_atrib b
                where z1.codpro = b.codpro
                and z1.cantid > 0
                and b.codpro = z2.codpro(+)"
        If Not chkTodosProd.Checked Then
            sql = sql & vbCrLf & "and z1.codpro = '" & cmbCodpro.Text & "'"
        Else
            'no se para que mierda es esto!!
        End If

        datos.open_dimerc()
        dgvVentas.DataSource = datos.sqlSelect(sql)
        datos.close()
    End Sub

    Private Sub frmVentaTissue_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        Dim datos As New Conexion
        Dim sql As String
        sql = "SELECT CODPRO FROM MA_PRODUCTO_ATRIB"
        datos.open_dimerc()
        Dim prods = datos.sqlSelect(sql)
        For Each prod In prods.Rows
            cmbCodpro.Items.Add(prod(0).ToString())
        Next
        datos.close()
    End Sub

    Private Sub btnLimpiar_Click(sender As Object, e As EventArgs) Handles btnLimpiar.Click
        PuntoControl.RegistroUso("228")
        limpiar()
    End Sub
    Private Sub limpiar()
        dgvVentas.DataSource = Nothing
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Globales.exporta("tissue.xls", dgvVentas)
        PuntoControl.RegistroUso("229")
    End Sub

    Private Sub frmVentaTissue_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        PuntoControl.RegistroUso("226")
        Me.Location = New Point(0, 0)
    End Sub
End Class