﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmContratos
    Inherits MetroFramework.Forms.MetroForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cmbDiscos = New System.Windows.Forms.ComboBox()
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtRazons = New System.Windows.Forms.TextBox()
        Me.txtRutcli = New System.Windows.Forms.TextBox()
        Me.btn_Salir = New System.Windows.Forms.Button()
        Me.btnaEquipo = New System.Windows.Forms.Button()
        Me.btnaServidor = New System.Windows.Forms.Button()
        Me.btnRuta = New MetroFramework.Controls.MetroButton()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmbDiscos
        '
        Me.cmbDiscos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbDiscos.FormattingEnabled = True
        Me.cmbDiscos.Location = New System.Drawing.Point(12, 81)
        Me.cmbDiscos.Name = "cmbDiscos"
        Me.cmbDiscos.Size = New System.Drawing.Size(260, 22)
        Me.cmbDiscos.TabIndex = 0
        '
        'TreeView1
        '
        Me.TreeView1.Location = New System.Drawing.Point(12, 109)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.Size = New System.Drawing.Size(260, 281)
        Me.TreeView1.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 62)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(40, 14)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Discos"
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.txtRazons)
        Me.Panel1.Controls.Add(Me.txtRutcli)
        Me.Panel1.Controls.Add(Me.btn_Salir)
        Me.Panel1.Location = New System.Drawing.Point(9, 9)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(927, 50)
        Me.Panel1.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(199, 17)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(73, 14)
        Me.Label2.TabIndex = 45
        Me.Label2.Text = "Razón Social"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(16, 17)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(67, 14)
        Me.Label3.TabIndex = 44
        Me.Label3.Text = "Rut Cliente"
        '
        'txtRazons
        '
        Me.txtRazons.BackColor = System.Drawing.SystemColors.Info
        Me.txtRazons.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRazons.Location = New System.Drawing.Point(275, 14)
        Me.txtRazons.Name = "txtRazons"
        Me.txtRazons.Size = New System.Drawing.Size(582, 22)
        Me.txtRazons.TabIndex = 43
        '
        'txtRutcli
        '
        Me.txtRutcli.BackColor = System.Drawing.SystemColors.Info
        Me.txtRutcli.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRutcli.Location = New System.Drawing.Point(89, 14)
        Me.txtRutcli.Name = "txtRutcli"
        Me.txtRutcli.Size = New System.Drawing.Size(94, 22)
        Me.txtRutcli.TabIndex = 42
        '
        'btn_Salir
        '
        Me.btn_Salir.Image = Global.Convenios_New.My.Resources.Resources.salir
        Me.btn_Salir.Location = New System.Drawing.Point(863, 4)
        Me.btn_Salir.Name = "btn_Salir"
        Me.btn_Salir.Size = New System.Drawing.Size(57, 41)
        Me.btn_Salir.TabIndex = 1
        Me.btn_Salir.UseVisualStyleBackColor = True
        '
        'btnaEquipo
        '
        Me.btnaEquipo.Location = New System.Drawing.Point(285, 109)
        Me.btnaEquipo.Name = "btnaEquipo"
        Me.btnaEquipo.Size = New System.Drawing.Size(119, 48)
        Me.btnaEquipo.TabIndex = 4
        Me.btnaEquipo.Text = "Copiar a Equipo"
        Me.btnaEquipo.UseVisualStyleBackColor = True
        '
        'btnaServidor
        '
        Me.btnaServidor.Location = New System.Drawing.Point(427, 109)
        Me.btnaServidor.Name = "btnaServidor"
        Me.btnaServidor.Size = New System.Drawing.Size(119, 48)
        Me.btnaServidor.TabIndex = 4
        Me.btnaServidor.Text = "Copiar a Servidor"
        Me.btnaServidor.UseVisualStyleBackColor = True
        '
        'btnRuta
        '
        Me.btnRuta.Location = New System.Drawing.Point(861, 362)
        Me.btnRuta.Name = "btnRuta"
        Me.btnRuta.Size = New System.Drawing.Size(75, 23)
        Me.btnRuta.TabIndex = 5
        Me.btnRuta.Text = "Abrir Ruta"
        Me.btnRuta.UseSelectable = True
        '
        'frmContratos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(948, 401)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnRuta)
        Me.Controls.Add(Me.btnaServidor)
        Me.Controls.Add(Me.btnaEquipo)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TreeView1)
        Me.Controls.Add(Me.cmbDiscos)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmContratos"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.Text = "Contratos del Cliente"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents cmbDiscos As ComboBox
    Friend WithEvents TreeView1 As TreeView
    Friend WithEvents Label1 As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents txtRazons As TextBox
    Friend WithEvents txtRutcli As TextBox
    Friend WithEvents btn_Salir As Button
    Friend WithEvents btnaEquipo As Button
    Friend WithEvents btnaServidor As Button
    Friend WithEvents btnRuta As MetroFramework.Controls.MetroButton
End Class
