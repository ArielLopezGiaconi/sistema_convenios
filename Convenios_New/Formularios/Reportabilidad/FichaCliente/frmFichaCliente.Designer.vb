﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmFichaCliente
    Inherits MetroFramework.Forms.MetroForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btn_Limpiar = New System.Windows.Forms.Button()
        Me.btn_Salir = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtRazons = New System.Windows.Forms.TextBox()
        Me.txtRelCom = New System.Windows.Forms.TextBox()
        Me.txtRutcli = New System.Windows.Forms.TextBox()
        Me.txtUDN = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtSegmento = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtVendedor = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtNumcot = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtMontoCot = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtEstadoConvenio = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtFeciniConvenio = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtFecfinConvenio = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtFecUltMod = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtObserv = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.btn_VerRelacionado = New System.Windows.Forms.Button()
        Me.btn_VerConvenio = New System.Windows.Forms.Button()
        Me.btn_Contratos = New System.Windows.Forms.Button()
        Me.btn_Polinomio = New System.Windows.Forms.Button()
        Me.btn_Historia = New System.Windows.Forms.Button()
        Me.txtAnalistaConvenio = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.btn_Reajustes = New System.Windows.Forms.Button()
        Me.txtVentaConConvenio = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtVentaSinConvenio = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.cbxMulta = New System.Windows.Forms.CheckBox()
        Me.txtOPL = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtRebate = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.txtObsRebate = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txtBolGarantia = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.txtUniBoleta = New System.Windows.Forms.TextBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.txtObsMulta = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.cbxClienteProtegido = New System.Windows.Forms.CheckBox()
        Me.dtpFechaContrato = New System.Windows.Forms.DateTimePicker()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.txtDolar = New System.Windows.Forms.TextBox()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.btn_Limpiar)
        Me.Panel1.Controls.Add(Me.btn_Salir)
        Me.Panel1.Location = New System.Drawing.Point(12, 62)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(850, 47)
        Me.Panel1.TabIndex = 0
        '
        'btn_Limpiar
        '
        Me.btn_Limpiar.Image = Global.Convenios_New.My.Resources.Resources.limpiar
        Me.btn_Limpiar.Location = New System.Drawing.Point(3, 4)
        Me.btn_Limpiar.Name = "btn_Limpiar"
        Me.btn_Limpiar.Size = New System.Drawing.Size(42, 38)
        Me.btn_Limpiar.TabIndex = 39
        Me.btn_Limpiar.UseVisualStyleBackColor = True
        '
        'btn_Salir
        '
        Me.btn_Salir.Image = Global.Convenios_New.My.Resources.Resources.salir
        Me.btn_Salir.Location = New System.Drawing.Point(793, 4)
        Me.btn_Salir.Name = "btn_Salir"
        Me.btn_Salir.Size = New System.Drawing.Size(49, 38)
        Me.btn_Salir.TabIndex = 0
        Me.btn_Salir.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 150)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(73, 14)
        Me.Label2.TabIndex = 37
        Me.Label2.Text = "Razón Social"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(449, 124)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(84, 14)
        Me.Label3.TabIndex = 35
        Me.Label3.Text = "Num. Relación"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 124)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(67, 14)
        Me.Label1.TabIndex = 36
        Me.Label1.Text = "Rut Cliente"
        '
        'txtRazons
        '
        Me.txtRazons.BackColor = System.Drawing.SystemColors.Window
        Me.txtRazons.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRazons.Location = New System.Drawing.Point(166, 147)
        Me.txtRazons.Name = "txtRazons"
        Me.txtRazons.Size = New System.Drawing.Size(543, 22)
        Me.txtRazons.TabIndex = 34
        '
        'txtRelCom
        '
        Me.txtRelCom.BackColor = System.Drawing.SystemColors.Window
        Me.txtRelCom.Location = New System.Drawing.Point(552, 121)
        Me.txtRelCom.Name = "txtRelCom"
        Me.txtRelCom.Size = New System.Drawing.Size(157, 22)
        Me.txtRelCom.TabIndex = 32
        '
        'txtRutcli
        '
        Me.txtRutcli.BackColor = System.Drawing.SystemColors.Window
        Me.txtRutcli.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRutcli.Location = New System.Drawing.Point(166, 121)
        Me.txtRutcli.Name = "txtRutcli"
        Me.txtRutcli.Size = New System.Drawing.Size(178, 22)
        Me.txtRutcli.TabIndex = 33
        '
        'txtUDN
        '
        Me.txtUDN.BackColor = System.Drawing.SystemColors.Info
        Me.txtUDN.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUDN.Location = New System.Drawing.Point(166, 173)
        Me.txtUDN.Name = "txtUDN"
        Me.txtUDN.Size = New System.Drawing.Size(543, 22)
        Me.txtUDN.TabIndex = 34
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(13, 173)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(31, 14)
        Me.Label4.TabIndex = 37
        Me.Label4.Text = "UDN"
        '
        'txtSegmento
        '
        Me.txtSegmento.BackColor = System.Drawing.SystemColors.Info
        Me.txtSegmento.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSegmento.Location = New System.Drawing.Point(166, 199)
        Me.txtSegmento.Name = "txtSegmento"
        Me.txtSegmento.Size = New System.Drawing.Size(543, 22)
        Me.txtSegmento.TabIndex = 34
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(12, 202)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(64, 14)
        Me.Label5.TabIndex = 37
        Me.Label5.Text = "Segmento"
        '
        'txtVendedor
        '
        Me.txtVendedor.BackColor = System.Drawing.SystemColors.Info
        Me.txtVendedor.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtVendedor.Location = New System.Drawing.Point(166, 225)
        Me.txtVendedor.Name = "txtVendedor"
        Me.txtVendedor.Size = New System.Drawing.Size(543, 22)
        Me.txtVendedor.TabIndex = 34
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(12, 228)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(61, 14)
        Me.Label6.TabIndex = 37
        Me.Label6.Text = "Vendedor"
        '
        'txtNumcot
        '
        Me.txtNumcot.BackColor = System.Drawing.SystemColors.Info
        Me.txtNumcot.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumcot.Location = New System.Drawing.Point(166, 390)
        Me.txtNumcot.Name = "txtNumcot"
        Me.txtNumcot.Size = New System.Drawing.Size(157, 22)
        Me.txtNumcot.TabIndex = 34
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(12, 393)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(148, 14)
        Me.Label7.TabIndex = 37
        Me.Label7.Text = "Núm. Cotización Convenio"
        '
        'txtMontoCot
        '
        Me.txtMontoCot.BackColor = System.Drawing.SystemColors.Info
        Me.txtMontoCot.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMontoCot.Location = New System.Drawing.Point(552, 390)
        Me.txtMontoCot.Name = "txtMontoCot"
        Me.txtMontoCot.Size = New System.Drawing.Size(157, 22)
        Me.txtMontoCot.TabIndex = 34
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(407, 393)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(126, 14)
        Me.Label8.TabIndex = 37
        Me.Label8.Text = "$ Cotización Convenio"
        '
        'txtEstadoConvenio
        '
        Me.txtEstadoConvenio.BackColor = System.Drawing.SystemColors.Info
        Me.txtEstadoConvenio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEstadoConvenio.Location = New System.Drawing.Point(166, 443)
        Me.txtEstadoConvenio.Name = "txtEstadoConvenio"
        Me.txtEstadoConvenio.Size = New System.Drawing.Size(157, 22)
        Me.txtEstadoConvenio.TabIndex = 34
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(12, 446)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(136, 14)
        Me.Label9.TabIndex = 37
        Me.Label9.Text = "Estado Ultimo Convenio"
        '
        'txtFeciniConvenio
        '
        Me.txtFeciniConvenio.BackColor = System.Drawing.SystemColors.Info
        Me.txtFeciniConvenio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFeciniConvenio.Location = New System.Drawing.Point(166, 469)
        Me.txtFeciniConvenio.Name = "txtFeciniConvenio"
        Me.txtFeciniConvenio.Size = New System.Drawing.Size(157, 22)
        Me.txtFeciniConvenio.TabIndex = 34
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(12, 472)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(125, 14)
        Me.Label10.TabIndex = 37
        Me.Label10.Text = "Fecha Inicio Convenio"
        '
        'txtFecfinConvenio
        '
        Me.txtFecfinConvenio.BackColor = System.Drawing.SystemColors.Info
        Me.txtFecfinConvenio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFecfinConvenio.Location = New System.Drawing.Point(166, 495)
        Me.txtFecfinConvenio.Name = "txtFecfinConvenio"
        Me.txtFecfinConvenio.Size = New System.Drawing.Size(157, 22)
        Me.txtFecfinConvenio.TabIndex = 34
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(12, 498)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(112, 14)
        Me.Label11.TabIndex = 37
        Me.Label11.Text = "Fecha Fin Convenio"
        '
        'txtFecUltMod
        '
        Me.txtFecUltMod.BackColor = System.Drawing.SystemColors.Info
        Me.txtFecUltMod.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFecUltMod.Location = New System.Drawing.Point(166, 522)
        Me.txtFecUltMod.Name = "txtFecUltMod"
        Me.txtFecUltMod.Size = New System.Drawing.Size(157, 22)
        Me.txtFecUltMod.TabIndex = 34
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(12, 525)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(145, 14)
        Me.Label12.TabIndex = 37
        Me.Label12.Text = "Fecha Ultima Modificación"
        '
        'txtObserv
        '
        Me.txtObserv.BackColor = System.Drawing.SystemColors.Window
        Me.txtObserv.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObserv.Location = New System.Drawing.Point(166, 549)
        Me.txtObserv.Multiline = True
        Me.txtObserv.Name = "txtObserv"
        Me.txtObserv.Size = New System.Drawing.Size(543, 37)
        Me.txtObserv.TabIndex = 34
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(13, 549)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(73, 14)
        Me.Label13.TabIndex = 37
        Me.Label13.Text = "Observación"
        '
        'btn_VerRelacionado
        '
        Me.btn_VerRelacionado.Location = New System.Drawing.Point(750, 114)
        Me.btn_VerRelacionado.Name = "btn_VerRelacionado"
        Me.btn_VerRelacionado.Size = New System.Drawing.Size(112, 42)
        Me.btn_VerRelacionado.TabIndex = 38
        Me.btn_VerRelacionado.Text = "Ver Relacionados"
        Me.btn_VerRelacionado.UseVisualStyleBackColor = True
        '
        'btn_VerConvenio
        '
        Me.btn_VerConvenio.Location = New System.Drawing.Point(750, 161)
        Me.btn_VerConvenio.Name = "btn_VerConvenio"
        Me.btn_VerConvenio.Size = New System.Drawing.Size(112, 42)
        Me.btn_VerConvenio.TabIndex = 38
        Me.btn_VerConvenio.Text = "Ver Convenio"
        Me.btn_VerConvenio.UseVisualStyleBackColor = True
        '
        'btn_Contratos
        '
        Me.btn_Contratos.Location = New System.Drawing.Point(750, 257)
        Me.btn_Contratos.Name = "btn_Contratos"
        Me.btn_Contratos.Size = New System.Drawing.Size(112, 42)
        Me.btn_Contratos.TabIndex = 38
        Me.btn_Contratos.Text = "Ver Contrato"
        Me.btn_Contratos.UseVisualStyleBackColor = True
        '
        'btn_Polinomio
        '
        Me.btn_Polinomio.Location = New System.Drawing.Point(750, 209)
        Me.btn_Polinomio.Name = "btn_Polinomio"
        Me.btn_Polinomio.Size = New System.Drawing.Size(112, 42)
        Me.btn_Polinomio.TabIndex = 38
        Me.btn_Polinomio.Text = "Polinomio"
        Me.btn_Polinomio.UseVisualStyleBackColor = True
        '
        'btn_Historia
        '
        Me.btn_Historia.Location = New System.Drawing.Point(750, 305)
        Me.btn_Historia.Name = "btn_Historia"
        Me.btn_Historia.Size = New System.Drawing.Size(112, 42)
        Me.btn_Historia.TabIndex = 38
        Me.btn_Historia.Text = "Historia"
        Me.btn_Historia.UseVisualStyleBackColor = True
        '
        'txtAnalistaConvenio
        '
        Me.txtAnalistaConvenio.BackColor = System.Drawing.SystemColors.Window
        Me.txtAnalistaConvenio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAnalistaConvenio.Location = New System.Drawing.Point(166, 416)
        Me.txtAnalistaConvenio.Name = "txtAnalistaConvenio"
        Me.txtAnalistaConvenio.Size = New System.Drawing.Size(543, 22)
        Me.txtAnalistaConvenio.TabIndex = 34
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(12, 419)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(102, 14)
        Me.Label14.TabIndex = 37
        Me.Label14.Text = "Analista Convenio"
        '
        'btn_Reajustes
        '
        Me.btn_Reajustes.Location = New System.Drawing.Point(750, 353)
        Me.btn_Reajustes.Name = "btn_Reajustes"
        Me.btn_Reajustes.Size = New System.Drawing.Size(112, 42)
        Me.btn_Reajustes.TabIndex = 38
        Me.btn_Reajustes.Text = "Reajustes"
        Me.btn_Reajustes.UseVisualStyleBackColor = True
        '
        'txtVentaConConvenio
        '
        Me.txtVentaConConvenio.BackColor = System.Drawing.SystemColors.Info
        Me.txtVentaConConvenio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtVentaConConvenio.Location = New System.Drawing.Point(552, 443)
        Me.txtVentaConConvenio.Name = "txtVentaConConvenio"
        Me.txtVentaConConvenio.Size = New System.Drawing.Size(157, 22)
        Me.txtVentaConConvenio.TabIndex = 34
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(384, 446)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(162, 14)
        Me.Label15.TabIndex = 37
        Me.Label15.Text = "Venta Periodo con convenio"
        '
        'txtVentaSinConvenio
        '
        Me.txtVentaSinConvenio.BackColor = System.Drawing.SystemColors.Info
        Me.txtVentaSinConvenio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtVentaSinConvenio.Location = New System.Drawing.Point(552, 469)
        Me.txtVentaSinConvenio.Name = "txtVentaSinConvenio"
        Me.txtVentaSinConvenio.Size = New System.Drawing.Size(157, 22)
        Me.txtVentaSinConvenio.TabIndex = 34
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(384, 472)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(156, 14)
        Me.Label16.TabIndex = 37
        Me.Label16.Text = "Venta Periodo sin convenio"
        '
        'cbxMulta
        '
        Me.cbxMulta.AutoSize = True
        Me.cbxMulta.Location = New System.Drawing.Point(515, 309)
        Me.cbxMulta.Name = "cbxMulta"
        Me.cbxMulta.Size = New System.Drawing.Size(154, 18)
        Me.cbxMulta.TabIndex = 39
        Me.cbxMulta.Text = "Multa por Cumplimiento"
        Me.cbxMulta.UseVisualStyleBackColor = True
        '
        'txtOPL
        '
        Me.txtOPL.BackColor = System.Drawing.SystemColors.Window
        Me.txtOPL.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtOPL.Location = New System.Drawing.Point(166, 251)
        Me.txtOPL.Name = "txtOPL"
        Me.txtOPL.Size = New System.Drawing.Size(125, 22)
        Me.txtOPL.TabIndex = 40
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(12, 254)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(29, 14)
        Me.Label17.TabIndex = 41
        Me.Label17.Text = "OPL"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(298, 254)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(46, 14)
        Me.Label18.TabIndex = 43
        Me.Label18.Text = "Rebate"
        '
        'txtRebate
        '
        Me.txtRebate.BackColor = System.Drawing.SystemColors.Window
        Me.txtRebate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRebate.Location = New System.Drawing.Point(350, 251)
        Me.txtRebate.Name = "txtRebate"
        Me.txtRebate.Size = New System.Drawing.Size(125, 22)
        Me.txtRebate.TabIndex = 42
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(12, 280)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(75, 14)
        Me.Label19.TabIndex = 45
        Me.Label19.Text = "Obs. Rebate"
        '
        'txtObsRebate
        '
        Me.txtObsRebate.BackColor = System.Drawing.SystemColors.Window
        Me.txtObsRebate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObsRebate.Location = New System.Drawing.Point(166, 277)
        Me.txtObsRebate.Multiline = True
        Me.txtObsRebate.Name = "txtObsRebate"
        Me.txtObsRebate.Size = New System.Drawing.Size(543, 25)
        Me.txtObsRebate.TabIndex = 44
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(12, 310)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(107, 14)
        Me.Label20.TabIndex = 47
        Me.Label20.Text = "Boleta de Garantia"
        '
        'txtBolGarantia
        '
        Me.txtBolGarantia.BackColor = System.Drawing.SystemColors.Window
        Me.txtBolGarantia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBolGarantia.Location = New System.Drawing.Point(166, 305)
        Me.txtBolGarantia.Name = "txtBolGarantia"
        Me.txtBolGarantia.Size = New System.Drawing.Size(125, 22)
        Me.txtBolGarantia.TabIndex = 46
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(298, 310)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(66, 14)
        Me.Label21.TabIndex = 49
        Me.Label21.Text = "Uni. Boleta"
        '
        'txtUniBoleta
        '
        Me.txtUniBoleta.BackColor = System.Drawing.SystemColors.Window
        Me.txtUniBoleta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUniBoleta.Location = New System.Drawing.Point(370, 307)
        Me.txtUniBoleta.Name = "txtUniBoleta"
        Me.txtUniBoleta.Size = New System.Drawing.Size(125, 22)
        Me.txtUniBoleta.TabIndex = 48
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(12, 336)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(65, 14)
        Me.Label22.TabIndex = 51
        Me.Label22.Text = "Obs. Multa"
        '
        'txtObsMulta
        '
        Me.txtObsMulta.BackColor = System.Drawing.SystemColors.Window
        Me.txtObsMulta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObsMulta.Enabled = False
        Me.txtObsMulta.Location = New System.Drawing.Point(166, 333)
        Me.txtObsMulta.Multiline = True
        Me.txtObsMulta.Name = "txtObsMulta"
        Me.txtObsMulta.Size = New System.Drawing.Size(543, 25)
        Me.txtObsMulta.TabIndex = 50
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(12, 366)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(91, 14)
        Me.Label23.TabIndex = 53
        Me.Label23.Text = "Fecha Contrato"
        '
        'cbxClienteProtegido
        '
        Me.cbxClienteProtegido.AutoSize = True
        Me.cbxClienteProtegido.Location = New System.Drawing.Point(301, 366)
        Me.cbxClienteProtegido.Name = "cbxClienteProtegido"
        Me.cbxClienteProtegido.Size = New System.Drawing.Size(179, 18)
        Me.cbxClienteProtegido.TabIndex = 54
        Me.cbxClienteProtegido.Text = "Cliente con Costo Protegido"
        Me.cbxClienteProtegido.UseVisualStyleBackColor = True
        '
        'dtpFechaContrato
        '
        Me.dtpFechaContrato.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaContrato.Location = New System.Drawing.Point(166, 364)
        Me.dtpFechaContrato.Name = "dtpFechaContrato"
        Me.dtpFechaContrato.Size = New System.Drawing.Size(125, 22)
        Me.dtpFechaContrato.TabIndex = 55
        '
        'btnGuardar
        '
        Me.btnGuardar.Image = Global.Convenios_New.My.Resources.Resources.Floppy
        Me.btnGuardar.Location = New System.Drawing.Point(806, 404)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(56, 56)
        Me.btnGuardar.TabIndex = 56
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(501, 500)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(45, 14)
        Me.Label24.TabIndex = 58
        Me.Label24.Text = "Dolar $"
        '
        'txtDolar
        '
        Me.txtDolar.BackColor = System.Drawing.SystemColors.Info
        Me.txtDolar.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDolar.Location = New System.Drawing.Point(552, 497)
        Me.txtDolar.Name = "txtDolar"
        Me.txtDolar.ReadOnly = True
        Me.txtDolar.Size = New System.Drawing.Size(157, 22)
        Me.txtDolar.TabIndex = 57
        '
        'frmFichaCliente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(872, 606)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label24)
        Me.Controls.Add(Me.txtDolar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.dtpFechaContrato)
        Me.Controls.Add(Me.cbxClienteProtegido)
        Me.Controls.Add(Me.Label23)
        Me.Controls.Add(Me.Label22)
        Me.Controls.Add(Me.txtObsMulta)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.txtUniBoleta)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.txtBolGarantia)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.txtObsRebate)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.txtRebate)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.txtOPL)
        Me.Controls.Add(Me.cbxMulta)
        Me.Controls.Add(Me.btn_Polinomio)
        Me.Controls.Add(Me.btn_Reajustes)
        Me.Controls.Add(Me.btn_Historia)
        Me.Controls.Add(Me.btn_Contratos)
        Me.Controls.Add(Me.btn_VerConvenio)
        Me.Controls.Add(Me.btn_VerRelacionado)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtVentaSinConvenio)
        Me.Controls.Add(Me.txtVentaConConvenio)
        Me.Controls.Add(Me.txtMontoCot)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtObserv)
        Me.Controls.Add(Me.txtFecUltMod)
        Me.Controls.Add(Me.txtFecfinConvenio)
        Me.Controls.Add(Me.txtFeciniConvenio)
        Me.Controls.Add(Me.txtAnalistaConvenio)
        Me.Controls.Add(Me.txtEstadoConvenio)
        Me.Controls.Add(Me.txtNumcot)
        Me.Controls.Add(Me.txtVendedor)
        Me.Controls.Add(Me.txtSegmento)
        Me.Controls.Add(Me.txtUDN)
        Me.Controls.Add(Me.txtRazons)
        Me.Controls.Add(Me.txtRelCom)
        Me.Controls.Add(Me.txtRutcli)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmFichaCliente"
        Me.ShowIcon = False
        Me.Text = "Ficha de Cliente"
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents btn_Salir As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents txtRazons As TextBox
    Friend WithEvents txtRelCom As TextBox
    Friend WithEvents txtRutcli As TextBox
    Friend WithEvents txtUDN As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents txtSegmento As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents txtVendedor As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents txtNumcot As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents txtMontoCot As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents txtEstadoConvenio As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents txtFeciniConvenio As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents txtFecfinConvenio As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents txtFecUltMod As TextBox
    Friend WithEvents Label12 As Label
    Friend WithEvents txtObserv As TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents btn_VerRelacionado As Button
    Friend WithEvents btn_VerConvenio As Button
    Friend WithEvents btn_Contratos As Button
    Friend WithEvents btn_Polinomio As Button
    Friend WithEvents btn_Historia As Button
    Friend WithEvents txtAnalistaConvenio As TextBox
    Friend WithEvents Label14 As Label
    Friend WithEvents btn_Limpiar As Button
    Friend WithEvents btn_Reajustes As Button
    Friend WithEvents txtVentaConConvenio As TextBox
    Friend WithEvents Label15 As Label
    Friend WithEvents txtVentaSinConvenio As TextBox
    Friend WithEvents Label16 As Label
    Friend WithEvents cbxMulta As CheckBox
    Friend WithEvents txtOPL As TextBox
    Friend WithEvents Label17 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents txtRebate As TextBox
    Friend WithEvents Label19 As Label
    Friend WithEvents txtObsRebate As TextBox
    Friend WithEvents Label20 As Label
    Friend WithEvents txtBolGarantia As TextBox
    Friend WithEvents Label21 As Label
    Friend WithEvents txtUniBoleta As TextBox
    Friend WithEvents Label22 As Label
    Friend WithEvents txtObsMulta As TextBox
    Friend WithEvents Label23 As Label
    Friend WithEvents cbxClienteProtegido As CheckBox
    Friend WithEvents dtpFechaContrato As DateTimePicker
    Friend WithEvents btnGuardar As Button
    Friend WithEvents Label24 As Label
    Friend WithEvents txtDolar As TextBox
End Class
