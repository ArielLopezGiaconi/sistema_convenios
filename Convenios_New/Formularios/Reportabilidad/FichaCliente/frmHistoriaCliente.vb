﻿Imports ClosedXML.Excel

Public Class frmHistoriaCliente

    Dim bd As New Conexion
    Dim rutcli As String
    Dim razons As String
    Dim numrel As String
    Dim PuntoControl As New PuntoControl
    Dim rutrelacionado As String
    Private Sub btn_Salir_Click(sender As Object, e As EventArgs) Handles btn_Salir.Click
        Close()
    End Sub

    Private Sub frmDetalleConvenio_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        PuntoControl.RegistroUso("199")
        Me.Location = New Point(0, 0)
        StartPosition = FormStartPosition.CenterParent
    End Sub
    Public Sub New()

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub
    Public Sub New(rutcli_ As String, razons_ As String, numrel_ As String, rutrelacionado_ As String)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        rutcli = rutcli_
        razons = razons_
        numrel = numrel_
        rutrelacionado = rutrelacionado_
        txtRutcli.Text = rutcli
        txtRazons.Text = razons
        txtNumRel.Text = numrel
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        buscaHistoria()
    End Sub
    Private Sub buscaHistoria()
        Try
            bd.open_dimerc()
            Dim SQL = " select a.rutcli,getrazonsocial(3,a.rutcli) ""Razón Social"",a.numcot ""Num. Cotizacion"",a.cencos ""Centro Costo"",a.fecemi  "
            SQL = SQL & "  ""Fecha Inicio"",a.fecven ""Fecha Vencimiento"", get_nombre(a.codven,1) ""Vendedor"", "
            SQL = SQL & "  a.subtot ""Subtotal"",a.totnet ""Total Neto"",a.totiva ""Total IVA"",a.totgen ""Total General"", "
            SQL = SQL & "  a.margen ""Margen"",a.usr_creac,get_nombre(a.codusu,1) ""NOMBRE USUARIO"", a.observ,b.numcot, "
            SQL = SQL & "  case when sysdate between b.fecemi and b.fecven then 'SI' else 'NO' end activo "
            SQL = SQL & "  from en_conveni_historias a,en_conveni b "
            SQL = SQL & "  where a.rutcli in (" + rutrelacionado + ")"
            SQL = SQL & "  and a.codemp=b.codemp(+) "
            SQL = SQL & "  and a.numcot=b.numcot(+) "
            SQL = SQL & "  and a.rutcli=b.rutcli(+) "
            SQL = SQL & "  and a.cencos=b.cencos(+) "

            dgvDetalle.DataSource = bd.sqlSelect(SQL)
            dgvDetalle.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            dgvDetalle.Columns("Subtotal").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvDetalle.Columns("Subtotal").DefaultCellStyle.Format = "n0"
            dgvDetalle.Columns("Total Neto").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvDetalle.Columns("Total Neto").DefaultCellStyle.Format = "n0"
            dgvDetalle.Columns("Total IVA").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvDetalle.Columns("Total IVA").DefaultCellStyle.Format = "n0"
            dgvDetalle.Columns("Total General").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvDetalle.Columns("Total General").DefaultCellStyle.Format = "n0"
            dgvDetalle.Columns("Margen").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvDetalle.Columns("Margen").DefaultCellStyle.Format = "n2"

        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub txtRutcli_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtRutcli.KeyPress, txtRazons.KeyPress, txtNumRel.KeyPress
        e.Handled = True
    End Sub

    Private Sub dgvDetalle_CellContentDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvDetalle.CellContentDoubleClick
        buscaDetalleConvenio(dgvDetalle.Item("Num. Cotizacion", dgvDetalle.CurrentRow.Index).Value.ToString)
    End Sub
    Private Sub buscaDetalleConvenio(numcot As String)
        Try
            bd.open_dimerc()
            Dim SQL = " select codpro,getdescripcion(codpro) descripcion,cantid cantidad, "
            SQL = SQL & "precio,costos costo,totnet neto,margen,getlinea(codpro) linea,fecemi,fecven  "
            SQL = SQL & "from de_cotizac where numcot= " + numcot
            Dim dt = bd.sqlSelect(SQL)
            dgvDetalleCotizacion.DataSource = dt
            dgvDetalleCotizacion.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            dgvDetalleCotizacion.Columns("cantidad").DefaultCellStyle.Format = "n0"
            dgvDetalleCotizacion.Columns("cantidad").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvDetalleCotizacion.Columns("precio").DefaultCellStyle.Format = "n0"
            dgvDetalleCotizacion.Columns("precio").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvDetalleCotizacion.Columns("costo").DefaultCellStyle.Format = "n0"
            dgvDetalleCotizacion.Columns("costo").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvDetalleCotizacion.Columns("neto").DefaultCellStyle.Format = "n0"
            dgvDetalleCotizacion.Columns("neto").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvDetalleCotizacion.Columns("margen").DefaultCellStyle.Format = "n2"
            dgvDetalleCotizacion.Columns("margen").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub btn_ExcelHistria_Click(sender As Object, e As EventArgs) Handles btn_ExcelHistria.Click
        PuntoControl.RegistroUso("200")
        Dim save As New SaveFileDialog
        If dgvDetalle.RowCount = 0 Then
            MessageBox.Show("No hay datos consultados en la grilla, procese datos antes de exportar", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        Else
            save.Filter = "Archivo Excel | *.xlsx"
            If save.ShowDialog = System.Windows.Forms.DialogResult.OK Then
                Dim dt As DataTable = dgvDetalle.DataSource
                ExportarDgvExcel(dt, save.FileName)
                'Globales.exporta(save.FileName, dgvDetalle)
            End If
        End If
    End Sub

    Public Sub ExportarDgvExcel(dt As DataTable, direccion As String)
        Try
            Dim WorkBook As New XLWorkbook
            dt.TableName = "DATOS"
            WorkBook.Worksheets.Add(dt)
            WorkBook.SaveAs(direccion)
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub btn_ExcelDetalle_Click(sender As Object, e As EventArgs) Handles btn_ExcelDetalle.Click
        PuntoControl.RegistroUso("201")
        Dim save As New SaveFileDialog
        If dgvDetalleCotizacion.RowCount = 0 Then
            MessageBox.Show("No hay datos consultados en la grilla, procese datos antes de exportar", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        Else
            save.Filter = "Archivo Excel | *.xlsx"
            If save.ShowDialog = System.Windows.Forms.DialogResult.OK Then
                Dim dt As DataTable = dgvDetalleCotizacion.DataSource
                ExportarDgvExcel(dt, save.FileName)
                'Globales.exporta(save.FileName, dgvDetalleCotizacion)
            End If
        End If
    End Sub
End Class