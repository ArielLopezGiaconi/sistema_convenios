﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPolinomioCliente
    Inherits MetroFramework.Forms.MetroForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btn_Salir = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkEliminaD = New System.Windows.Forms.CheckBox()
        Me.chkEliminaC = New System.Windows.Forms.CheckBox()
        Me.chkEliminaB = New System.Windows.Forms.CheckBox()
        Me.chkEliminaA = New System.Windows.Forms.CheckBox()
        Me.btn_GrabarPolinomio = New System.Windows.Forms.Button()
        Me.chkRelacionado = New System.Windows.Forms.CheckBox()
        Me.txtCelulosaD = New System.Windows.Forms.TextBox()
        Me.txtCelulosaC = New System.Windows.Forms.TextBox()
        Me.txtCelulosaB = New System.Windows.Forms.TextBox()
        Me.txtCelulosaA = New System.Windows.Forms.TextBox()
        Me.txtDolarD = New System.Windows.Forms.TextBox()
        Me.txtDolarC = New System.Windows.Forms.TextBox()
        Me.txtDolarB = New System.Windows.Forms.TextBox()
        Me.txtDolarA = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtIpcD = New System.Windows.Forms.TextBox()
        Me.txtIpcC = New System.Windows.Forms.TextBox()
        Me.txtIpcB = New System.Windows.Forms.TextBox()
        Me.txtIpcA = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cmbLineaD = New System.Windows.Forms.ComboBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.cmbLineaC = New System.Windows.Forms.ComboBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.cmbLineaB = New System.Windows.Forms.ComboBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.cmbLineaA = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtRazons = New System.Windows.Forms.TextBox()
        Me.txtRutcli = New System.Windows.Forms.TextBox()
        Me.Panel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.txtRazons)
        Me.Panel1.Controls.Add(Me.txtRutcli)
        Me.Panel1.Controls.Add(Me.btn_Salir)
        Me.Panel1.Location = New System.Drawing.Point(14, 13)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1144, 50)
        Me.Panel1.TabIndex = 2
        '
        'btn_Salir
        '
        Me.btn_Salir.Image = Global.Convenios_New.My.Resources.Resources.salir
        Me.btn_Salir.Location = New System.Drawing.Point(1082, 4)
        Me.btn_Salir.Name = "btn_Salir"
        Me.btn_Salir.Size = New System.Drawing.Size(57, 41)
        Me.btn_Salir.TabIndex = 1
        Me.btn_Salir.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chkEliminaD)
        Me.GroupBox1.Controls.Add(Me.chkEliminaC)
        Me.GroupBox1.Controls.Add(Me.chkEliminaB)
        Me.GroupBox1.Controls.Add(Me.chkEliminaA)
        Me.GroupBox1.Controls.Add(Me.btn_GrabarPolinomio)
        Me.GroupBox1.Controls.Add(Me.chkRelacionado)
        Me.GroupBox1.Controls.Add(Me.txtCelulosaD)
        Me.GroupBox1.Controls.Add(Me.txtCelulosaC)
        Me.GroupBox1.Controls.Add(Me.txtCelulosaB)
        Me.GroupBox1.Controls.Add(Me.txtCelulosaA)
        Me.GroupBox1.Controls.Add(Me.txtDolarD)
        Me.GroupBox1.Controls.Add(Me.txtDolarC)
        Me.GroupBox1.Controls.Add(Me.txtDolarB)
        Me.GroupBox1.Controls.Add(Me.txtDolarA)
        Me.GroupBox1.Controls.Add(Me.Label19)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.txtIpcD)
        Me.GroupBox1.Controls.Add(Me.txtIpcC)
        Me.GroupBox1.Controls.Add(Me.txtIpcB)
        Me.GroupBox1.Controls.Add(Me.txtIpcA)
        Me.GroupBox1.Controls.Add(Me.Label18)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.cmbLineaD)
        Me.GroupBox1.Controls.Add(Me.Label17)
        Me.GroupBox1.Controls.Add(Me.cmbLineaC)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.cmbLineaB)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.cmbLineaA)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Location = New System.Drawing.Point(14, 69)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1147, 176)
        Me.GroupBox1.TabIndex = 69
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Polinomio"
        '
        'chkEliminaD
        '
        Me.chkEliminaD.AutoSize = True
        Me.chkEliminaD.Location = New System.Drawing.Point(860, 140)
        Me.chkEliminaD.Name = "chkEliminaD"
        Me.chkEliminaD.Size = New System.Drawing.Size(74, 18)
        Me.chkEliminaD.TabIndex = 5
        Me.chkEliminaD.Text = "¿Elimina?"
        Me.chkEliminaD.UseVisualStyleBackColor = True
        '
        'chkEliminaC
        '
        Me.chkEliminaC.AutoSize = True
        Me.chkEliminaC.Location = New System.Drawing.Point(584, 141)
        Me.chkEliminaC.Name = "chkEliminaC"
        Me.chkEliminaC.Size = New System.Drawing.Size(74, 18)
        Me.chkEliminaC.TabIndex = 5
        Me.chkEliminaC.Text = "¿Elimina?"
        Me.chkEliminaC.UseVisualStyleBackColor = True
        '
        'chkEliminaB
        '
        Me.chkEliminaB.AutoSize = True
        Me.chkEliminaB.Location = New System.Drawing.Point(321, 141)
        Me.chkEliminaB.Name = "chkEliminaB"
        Me.chkEliminaB.Size = New System.Drawing.Size(74, 18)
        Me.chkEliminaB.TabIndex = 5
        Me.chkEliminaB.Text = "¿Elimina?"
        Me.chkEliminaB.UseVisualStyleBackColor = True
        '
        'chkEliminaA
        '
        Me.chkEliminaA.AutoSize = True
        Me.chkEliminaA.Location = New System.Drawing.Point(62, 141)
        Me.chkEliminaA.Name = "chkEliminaA"
        Me.chkEliminaA.Size = New System.Drawing.Size(74, 18)
        Me.chkEliminaA.TabIndex = 5
        Me.chkEliminaA.Text = "¿Elimina?"
        Me.chkEliminaA.UseVisualStyleBackColor = True
        '
        'btn_GrabarPolinomio
        '
        Me.btn_GrabarPolinomio.Image = Global.Convenios_New.My.Resources.Resources.Floppy
        Me.btn_GrabarPolinomio.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_GrabarPolinomio.Location = New System.Drawing.Point(991, 115)
        Me.btn_GrabarPolinomio.Name = "btn_GrabarPolinomio"
        Me.btn_GrabarPolinomio.Size = New System.Drawing.Size(150, 55)
        Me.btn_GrabarPolinomio.TabIndex = 4
        Me.btn_GrabarPolinomio.Text = "Grabar Datos"
        Me.btn_GrabarPolinomio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_GrabarPolinomio.UseVisualStyleBackColor = True
        '
        'chkRelacionado
        '
        Me.chkRelacionado.AutoSize = True
        Me.chkRelacionado.Location = New System.Drawing.Point(991, 91)
        Me.chkRelacionado.Name = "chkRelacionado"
        Me.chkRelacionado.Size = New System.Drawing.Size(150, 18)
        Me.chkRelacionado.TabIndex = 3
        Me.chkRelacionado.Text = "¿Incluye Relacionados?"
        Me.chkRelacionado.UseVisualStyleBackColor = True
        '
        'txtCelulosaD
        '
        Me.txtCelulosaD.Location = New System.Drawing.Point(860, 112)
        Me.txtCelulosaD.Name = "txtCelulosaD"
        Me.txtCelulosaD.Size = New System.Drawing.Size(100, 22)
        Me.txtCelulosaD.TabIndex = 2
        '
        'txtCelulosaC
        '
        Me.txtCelulosaC.Location = New System.Drawing.Point(584, 112)
        Me.txtCelulosaC.Name = "txtCelulosaC"
        Me.txtCelulosaC.Size = New System.Drawing.Size(100, 22)
        Me.txtCelulosaC.TabIndex = 2
        '
        'txtCelulosaB
        '
        Me.txtCelulosaB.Location = New System.Drawing.Point(321, 112)
        Me.txtCelulosaB.Name = "txtCelulosaB"
        Me.txtCelulosaB.Size = New System.Drawing.Size(100, 22)
        Me.txtCelulosaB.TabIndex = 2
        '
        'txtCelulosaA
        '
        Me.txtCelulosaA.Location = New System.Drawing.Point(62, 112)
        Me.txtCelulosaA.Name = "txtCelulosaA"
        Me.txtCelulosaA.Size = New System.Drawing.Size(100, 22)
        Me.txtCelulosaA.TabIndex = 2
        '
        'txtDolarD
        '
        Me.txtDolarD.Location = New System.Drawing.Point(860, 84)
        Me.txtDolarD.Name = "txtDolarD"
        Me.txtDolarD.Size = New System.Drawing.Size(100, 22)
        Me.txtDolarD.TabIndex = 2
        '
        'txtDolarC
        '
        Me.txtDolarC.Location = New System.Drawing.Point(584, 84)
        Me.txtDolarC.Name = "txtDolarC"
        Me.txtDolarC.Size = New System.Drawing.Size(100, 22)
        Me.txtDolarC.TabIndex = 2
        '
        'txtDolarB
        '
        Me.txtDolarB.Location = New System.Drawing.Point(321, 84)
        Me.txtDolarB.Name = "txtDolarB"
        Me.txtDolarB.Size = New System.Drawing.Size(100, 22)
        Me.txtDolarB.TabIndex = 2
        '
        'txtDolarA
        '
        Me.txtDolarA.Location = New System.Drawing.Point(62, 84)
        Me.txtDolarA.Name = "txtDolarA"
        Me.txtDolarA.Size = New System.Drawing.Size(100, 22)
        Me.txtDolarA.TabIndex = 2
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(804, 115)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(50, 14)
        Me.Label19.TabIndex = 0
        Me.Label19.Text = "Celulosa"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(528, 115)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(50, 14)
        Me.Label15.TabIndex = 0
        Me.Label15.Text = "Celulosa"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(265, 115)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(50, 14)
        Me.Label11.TabIndex = 0
        Me.Label11.Text = "Celulosa"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(6, 115)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(50, 14)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Celulosa"
        '
        'txtIpcD
        '
        Me.txtIpcD.Location = New System.Drawing.Point(860, 56)
        Me.txtIpcD.Name = "txtIpcD"
        Me.txtIpcD.Size = New System.Drawing.Size(100, 22)
        Me.txtIpcD.TabIndex = 2
        '
        'txtIpcC
        '
        Me.txtIpcC.Location = New System.Drawing.Point(584, 56)
        Me.txtIpcC.Name = "txtIpcC"
        Me.txtIpcC.Size = New System.Drawing.Size(100, 22)
        Me.txtIpcC.TabIndex = 2
        '
        'txtIpcB
        '
        Me.txtIpcB.Location = New System.Drawing.Point(321, 56)
        Me.txtIpcB.Name = "txtIpcB"
        Me.txtIpcB.Size = New System.Drawing.Size(100, 22)
        Me.txtIpcB.TabIndex = 2
        '
        'txtIpcA
        '
        Me.txtIpcA.Location = New System.Drawing.Point(62, 56)
        Me.txtIpcA.Name = "txtIpcA"
        Me.txtIpcA.Size = New System.Drawing.Size(100, 22)
        Me.txtIpcA.TabIndex = 2
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(804, 87)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(34, 14)
        Me.Label18.TabIndex = 0
        Me.Label18.Text = "Dólar"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(528, 87)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(34, 14)
        Me.Label14.TabIndex = 0
        Me.Label14.Text = "Dólar"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(265, 87)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(34, 14)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "Dólar"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(6, 87)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(34, 14)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Dólar"
        '
        'cmbLineaD
        '
        Me.cmbLineaD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbLineaD.FormattingEnabled = True
        Me.cmbLineaD.Location = New System.Drawing.Point(860, 26)
        Me.cmbLineaD.Name = "cmbLineaD"
        Me.cmbLineaD.Size = New System.Drawing.Size(197, 22)
        Me.cmbLineaD.TabIndex = 1
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(804, 59)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(25, 14)
        Me.Label17.TabIndex = 0
        Me.Label17.Text = "IPC"
        '
        'cmbLineaC
        '
        Me.cmbLineaC.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbLineaC.FormattingEnabled = True
        Me.cmbLineaC.Location = New System.Drawing.Point(584, 26)
        Me.cmbLineaC.Name = "cmbLineaC"
        Me.cmbLineaC.Size = New System.Drawing.Size(197, 22)
        Me.cmbLineaC.TabIndex = 1
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(528, 59)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(25, 14)
        Me.Label13.TabIndex = 0
        Me.Label13.Text = "IPC"
        '
        'cmbLineaB
        '
        Me.cmbLineaB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbLineaB.FormattingEnabled = True
        Me.cmbLineaB.Location = New System.Drawing.Point(321, 26)
        Me.cmbLineaB.Name = "cmbLineaB"
        Me.cmbLineaB.Size = New System.Drawing.Size(197, 22)
        Me.cmbLineaB.TabIndex = 1
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(804, 29)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(35, 14)
        Me.Label16.TabIndex = 0
        Me.Label16.Text = "Línea"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(265, 59)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(25, 14)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "IPC"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(528, 29)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(35, 14)
        Me.Label12.TabIndex = 0
        Me.Label12.Text = "Línea"
        '
        'cmbLineaA
        '
        Me.cmbLineaA.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbLineaA.FormattingEnabled = True
        Me.cmbLineaA.Location = New System.Drawing.Point(62, 26)
        Me.cmbLineaA.Name = "cmbLineaA"
        Me.cmbLineaA.Size = New System.Drawing.Size(197, 22)
        Me.cmbLineaA.TabIndex = 1
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(265, 29)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(35, 14)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "Línea"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 59)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(25, 14)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "IPC"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 29)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(35, 14)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Línea"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(199, 17)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(73, 14)
        Me.Label2.TabIndex = 45
        Me.Label2.Text = "Razón Social"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(16, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(67, 14)
        Me.Label1.TabIndex = 44
        Me.Label1.Text = "Rut Cliente"
        '
        'txtRazons
        '
        Me.txtRazons.BackColor = System.Drawing.SystemColors.Info
        Me.txtRazons.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRazons.Location = New System.Drawing.Point(275, 14)
        Me.txtRazons.Name = "txtRazons"
        Me.txtRazons.Size = New System.Drawing.Size(770, 22)
        Me.txtRazons.TabIndex = 43
        '
        'txtRutcli
        '
        Me.txtRutcli.BackColor = System.Drawing.SystemColors.Info
        Me.txtRutcli.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRutcli.Location = New System.Drawing.Point(89, 14)
        Me.txtRutcli.Name = "txtRutcli"
        Me.txtRutcli.Size = New System.Drawing.Size(94, 22)
        Me.txtRutcli.TabIndex = 42
        '
        'frmPolinomioCliente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1170, 369)
        Me.ControlBox = False
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmPolinomioCliente"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.Text = "Polinomio del Cliente"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents btn_Salir As Button
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents chkEliminaD As CheckBox
    Friend WithEvents chkEliminaC As CheckBox
    Friend WithEvents chkEliminaB As CheckBox
    Friend WithEvents chkEliminaA As CheckBox
    Friend WithEvents btn_GrabarPolinomio As Button
    Friend WithEvents chkRelacionado As CheckBox
    Friend WithEvents txtCelulosaD As TextBox
    Friend WithEvents txtCelulosaC As TextBox
    Friend WithEvents txtCelulosaB As TextBox
    Friend WithEvents txtCelulosaA As TextBox
    Friend WithEvents txtDolarD As TextBox
    Friend WithEvents txtDolarC As TextBox
    Friend WithEvents txtDolarB As TextBox
    Friend WithEvents txtDolarA As TextBox
    Friend WithEvents Label19 As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents txtIpcD As TextBox
    Friend WithEvents txtIpcC As TextBox
    Friend WithEvents txtIpcB As TextBox
    Friend WithEvents txtIpcA As TextBox
    Friend WithEvents Label18 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents cmbLineaD As ComboBox
    Friend WithEvents Label17 As Label
    Friend WithEvents cmbLineaC As ComboBox
    Friend WithEvents Label13 As Label
    Friend WithEvents cmbLineaB As ComboBox
    Friend WithEvents Label16 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents cmbLineaA As ComboBox
    Friend WithEvents Label8 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents txtRazons As TextBox
    Friend WithEvents txtRutcli As TextBox
End Class
