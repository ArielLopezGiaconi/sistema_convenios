﻿Imports System.IO
Imports System.Data.OracleClient

Public Class frmContratos
    Dim bd As New Conexion
    Dim rutcli, razons, numrel As String
    Dim PuntoControl As New PuntoControl
    Public Sub New()

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub
    Public Sub New(rutcli_ As String, razons_ As String, numrel_ As String)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        rutcli = rutcli_
        razons = razons_
        numrel = numrel_
        txtRutcli.Text = rutcli
        txtRazons.Text = razons
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub
    Private Sub frmContratos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        PuntoControl.RegistroUso("186")
        Me.Location = New Point(0, 0)
        StartPosition = FormStartPosition.CenterParent
        Dim drives As DriveInfo() = DriveInfo.GetDrives()
        For i As Integer = 0 To drives.Length - 1
            cmbDiscos.Items.Add(drives(i).Name)
            cmbDiscos.SelectedIndex = -1
        Next
    End Sub

    Private Sub btn_Salir_Click(sender As Object, e As EventArgs) Handles btn_Salir.Click
        Close()
    End Sub

    Private Sub TreeView1_BeforeExpand(sender As Object, e As TreeViewCancelEventArgs) Handles TreeView1.BeforeExpand
        Try

            Dim courentNode As TreeNode = e.Node
            Dim folderPaht As New DirectoryInfo(cmbDiscos.SelectedItem + courentNode.FullPath)

            For Each dir As DirectoryInfo In folderPaht.GetDirectories()
                Dim fileName As String = dir.Name
                Dim node As TreeNode = courentNode.Nodes.Add(fileName)
                node.Nodes.Add(" ")
            Next

            For Each file As FileInfo In folderPaht.GetFiles()
                Dim ext As String = file.Extension
                If ext.ToLower() = ".pdf" Then
                    Dim newNode As TreeNode = courentNode.Nodes.Add(file.Name)

                End If
            Next
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try

    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbDiscos.SelectedIndexChanged
        TreeView1.Nodes.Clear()
        Dim path As New DirectoryInfo(cmbDiscos.SelectedItem.ToString())
        Try
            For Each dir As DirectoryInfo In path.GetDirectories()
                Dim node As TreeNode = TreeView1.Nodes.Add(dir.Name)
                node.Nodes.Add(" ")
            Next

            For Each file As FileInfo In path.GetFiles()
                If file.Extension.ToLower() = ".pdf" Then
                    Dim node As TreeNode = TreeView1.Nodes.Add(file.Name)
                End If
            Next
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub btnRuta_Click(sender As Object, e As EventArgs) Handles btnRuta.Click
        PuntoControl.RegistroUso("189")
        Dim ruta = "\\10.10.20.222\Sistema\Pricing\"
        Dim openFile As New OpenFileDialog

        openFile.InitialDirectory = ruta

        If openFile.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            Dim Proceso As New Process

            Proceso.StartInfo.FileName = ruta & openFile.SafeFileName
            Proceso.StartInfo.Arguments = ""
            Proceso.Start()

        End If
    End Sub

    Private Sub Panel1_Paint(sender As Object, e As PaintEventArgs) Handles Panel1.Paint

    End Sub

    Private Sub btnaEquipo_Click(sender As Object, e As EventArgs) Handles btnaEquipo.Click
        Try
            PuntoControl.RegistroUso("187")
            Dim openFile As New OpenFileDialog
            openFile.InitialDirectory = "\\10.10.20.222\Sistema\Pricing"
            If openFile.ShowDialog = System.Windows.Forms.DialogResult.OK Then
                If File.Exists("C:\dimerc\" + openFile.SafeFileName) Then
                    File.Delete("C:\dimerc\" + openFile.SafeFileName)
                End If
                File.Copy(openFile.FileName, "C:\dimerc\" + openFile.SafeFileName)
                MessageBox.Show("Archivo copiado con exito a la ruta " + "C:\dimerc\" + openFile.SafeFileName)
            End If
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnaServidor.Click
        Try
            Dim openFile As New OpenFileDialog
            openFile.InitialDirectory = "C:\"
            If openFile.ShowDialog = System.Windows.Forms.DialogResult.OK Then
                If File.Exists("\\10.10.20.222\Sistema\Pricing\" + openFile.SafeFileName) Then
                    File.Delete("\\10.10.20.222\Sistema\Pricing\" + openFile.SafeFileName)
                End If
                File.Copy(openFile.FileName, "\\10.10.20.222\Sistema\Pricing\" + openFile.SafeFileName)
                Using conn = New OracleConnection(Conexion.retornaConexion)
                    conn.Open()
                    Dim comando As OracleCommand
                    Using transaccion = conn.BeginTransaction
                        Try
                            Dim SQL = " insert into ma_contratos_convenio (codemp,rutcli,numrel,ruta) "
                            SQL = SQL & "values(3,"
                            SQL = SQL & txtRutcli.Text + ","
                            SQL = SQL & numrel + ","
                            SQL = SQL & "'" + "\\10.10.20.222\Sistema\Pricing\" + openFile.SafeFileName + "'"
                            SQL = SQL & ") "

                            comando = New OracleCommand(SQL, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()

                            transaccion.Commit()

                            MessageBox.Show("Archivo copiado con exito a la ruta " + "\\10.10.20.222\Sistema\Pricing\" + openFile.SafeFileName)
                        Catch ex As Exception
                            If Not transaccion.Equals(Nothing) Then
                                transaccion.Rollback()
                            End If
                            MessageBox.Show(ex.ToString, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Finally
                            conn.Close()
                        End Try
                    End Using
                End Using
            End If
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
End Class