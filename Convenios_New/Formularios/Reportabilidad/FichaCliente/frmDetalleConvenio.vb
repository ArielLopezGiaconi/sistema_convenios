﻿Public Class frmDetalleConvenio
    Dim bd As New Conexion
    Dim numcot As String
    Dim rutcli As String
    Dim razons As String
    Dim PuntoControl As New PuntoControl

    Private Sub btn_Salir_Click(sender As Object, e As EventArgs) Handles btn_Salir.Click
        Close()
    End Sub

    Private Sub frmDetalleConvenio_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        PuntoControl.RegistroUso("190")
        Me.Location = New Point(0, 0)
        StartPosition = FormStartPosition.CenterParent
    End Sub
    Public Sub New()

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub
    Public Sub New(rutcli_ As String, numcot_ As String, razons_ As String)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        rutcli = rutcli_
        numcot = numcot_
        razons = razons_
        txtRutcli.Text = rutcli
        txtRazons.Text = razons
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        buscadetalle()
    End Sub
    Private Sub buscadetalle()
        Try
            bd.open_dimerc()
            Dim SQL = " select codpro,getdescripcion(codpro) descripcion,cantid cantidad, "
            SQL = SQL & "precio,costos,totnet,margen,getlinea(codpro) linea,fecemi,fecven  "
            SQL = SQL & "from de_conveni where numcot= " + numcot
            Dim dt = bd.sqlSelect(SQL)
            dgvDetalle.DataSource = dt
            dgvDetalle.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            dgvDetalle.Columns("cantidad").DefaultCellStyle.Format = "n0"
            dgvDetalle.Columns("cantidad").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvDetalle.Columns("precio").DefaultCellStyle.Format = "n0"
            dgvDetalle.Columns("precio").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvDetalle.Columns("costos").DefaultCellStyle.Format = "n0"
            dgvDetalle.Columns("costos").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvDetalle.Columns("totnet").DefaultCellStyle.Format = "n0"
            dgvDetalle.Columns("totnet").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvDetalle.Columns("margen").DefaultCellStyle.Format = "n2"
            dgvDetalle.Columns("margen").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close
        End Try
    End Sub

    Private Sub txtRutcli_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtRutcli.KeyPress, txtRazons.KeyPress
        e.Handled = True
    End Sub

    Private Sub btn_Excel_Click(sender As Object, e As EventArgs) Handles btn_Excel.Click
        Dim save As New SaveFileDialog
        If dgvDetalle.RowCount = 0 Then
            MessageBox.Show("No hay datos consultados en la grilla, procese datos antes de exportar", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        Else
            save.Filter = "Archivo Excel | *.xlsx"
            If save.ShowDialog = System.Windows.Forms.DialogResult.OK Then
                Globales.exporta(save.FileName, dgvDetalle)
            End If
        End If
    End Sub
End Class