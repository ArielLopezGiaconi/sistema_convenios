﻿Imports System.Data.OracleClient

Public Class frmFichaCliente
    Dim bd As New Conexion
    Dim rutrelacionado As String
    Dim PuntoControl As New PuntoControl
    Private Sub btn_Salir_Click(sender As Object, e As EventArgs) Handles btn_Salir.Click
        Close()
    End Sub

    Private Sub frmFichaCliente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        PuntoControl.RegistroUso("191")
        Me.Location = New Point(0, 0)
        StartPosition = FormStartPosition.Manual
        Location = New Point(0, 100)
    End Sub

    Private Sub txtRutcli_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtRutcli.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If Trim(txtRutcli.Text) <> "" Then
                buscaCliente()
            Else
                txtRazons.Focus()
            End If
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtRazons_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtRazons.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If txtRazons.Text.ToString.Trim <> "" Then

                Dim buscador As New frmBuscaCliente(txtRazons.Text)
                buscador.ShowDialog()
                If buscador.dgvDatos.CurrentRow Is Nothing Then
                    Exit Sub
                End If
                txtRutcli.Text = buscador.dgvDatos.Item("Rut", buscador.dgvDatos.CurrentRow.Index).Value.ToString
                txtRazons.Text = buscador.dgvDatos.Item("Razon Social", buscador.dgvDatos.CurrentRow.Index).Value.ToString
                buscaDatosCliente()

            Else
                If txtRutcli.Text = "" Then
                    txtRutcli.Focus()
                End If
            End If
        End If
    End Sub


    Private Sub txtRelCom_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtRelCom.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If txtRelCom.Text.Trim <> "" Then

                Dim buscador As New frmBuscarelacion(txtRelCom.Text)
                buscador.ShowDialog()
                If buscador.dgvDatos.CurrentRow Is Nothing Then
                    Exit Sub
                End If
                txtRutcli.Text = buscador.dgvDatos.Item("Rut", buscador.dgvDatos.CurrentRow.Index).Value.ToString
                txtRazons.Text = buscador.dgvDatos.Item("Razon Social", buscador.dgvDatos.CurrentRow.Index).Value.ToString
                buscaDatosCliente()

            Else
                If txtRutcli.Text = "" Then
                    txtRutcli.Focus()
                End If
            End If
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub
    Private Sub buscaCliente()
        Try
            bd.open_dimerc()
            Dim StrQuery = "select razons nombre from en_cliente "
            StrQuery = StrQuery & " where rutcli = '" & txtRutcli.Text & "'"
            StrQuery = StrQuery & "   and codemp = 3"
            Dim OdnCliente = bd.sqlSelect(StrQuery)
            If Not OdnCliente.Rows.Count = 0 Then
                txtRazons.Text = CStr(OdnCliente.Rows(0).Item("nombre"))
                buscaDatosCliente()
            Else
                MessageBox.Show("Cliente No Existe. Debe ser Ingresado Previamente.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtRutcli.Text = ""
                txtRutcli.Focus()
                Exit Sub
            End If
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub
    Private Sub buscaDatosCliente()
        Try
            Dim dt1 As New DataTable
            bd.open_dimerc()
            Dim SQL = "    select a.rutcli,getrazonsocial(a.codemp,a.rutcli) razons,nvl(Get_Numrel(a.codemp,a.rutcli),0) numrel,   "
            SQL = SQL & "   GET_SEGMENTO_CLIENTE(a.codemp,a.rutcli) segmento,GET_UNINEGOCIO(a.codemp,a.rutcli) UDN,   "
            SQL = SQL & "   get_nombre(GET_VENDEDOR(a.codemp,a.rutcli),2) vendedor,  nvl(b.numcot,0) numcot, "
            SQL = SQL & "   nvl(b.totnet,0) totnet,nvl(b.usr_creac,'SIN CONVENIO ACTIVO') analista,  'ESTADO' estado,  "
            SQL = SQL & "    nvl(to_char(b.fecemi,'dd/mm/yyyy'),'Sin convenio') fecemi,   "
            SQL = SQL & "    nvl(to_char(b.fecven,'dd/mm/yyyy'),'Sin convenio') fecven,   "
            SQL = SQL & "    get_ultimafechareajuste(a.rutcli) fecultmod, b.observ,nvl(c.ventaneta,0) ventaconvenio,nvl(d.ventaneta,0) ventasinconvenio   "
            SQL = SQL & "    from en_cliente a, en_conveni b, "
            SQL = SQL & "    (select a.rutcli,sum(decode(greatest(nvl(decode(b.tipo,'N',b.neto*-1,b.neto),0),0),0,1,  nvl(decode(b.tipo,'N',b.neto*-1,b.neto),0))) ventaNeta "
            SQL = SQL & " from en_conveni a,qv_control_margen b  "
            SQL = SQL & " where sysdate between a.fecemi and a.fecven  "
            SQL = SQL & " and a.codemp=b.codemp  "
            SQL = SQL & " and a.rutcli=b.rutcli  "
            'SQL = SQL & " and a.cencos=b.cencos  "
            SQL = SQL & " and b.fecha between a.fecemi and a.fecven  "
            SQL = SQL & " and b.prodconvenio='S'  "
            SQL = SQL & " group by a.rutcli) c,  "
            SQL = SQL & " (select a.rutcli,sum(decode(greatest(nvl(decode(b.tipo,'N',b.neto*-1,b.neto),0),0),0,1, nvl(decode(b.tipo,'N',b.neto*-1,b.neto),0))) ventaNeta  "
            SQL = SQL & " from en_conveni a,qv_control_margen b  "
            SQL = SQL & " where sysdate between a.fecemi and a.fecven  "
            SQL = SQL & " and a.codemp=b.codemp  "
            SQL = SQL & " and a.rutcli=b.rutcli  "
            'SQL = SQL & " and a.cencos=b.cencos  "
            SQL = SQL & " and b.fecha between a.fecemi and a.fecven  "
            SQL = SQL & " and b.prodconvenio='N'  "
            SQL = SQL & " group by a.rutcli) d    "
            SQL = SQL & " where a.codemp=3  "
            SQL = SQL & " and a.rutcli= " + txtRutcli.Text
            SQL = SQL & " and a.codemp=b.codemp(+)   "
            SQL = SQL & " and a.rutcli=b.rutcli(+)  "
            SQL = SQL & " and a.rutcli=c.rutcli(+) "
            SQL = SQL & " and a.rutcli=d.rutcli(+)   "
            SQL = SQL & "    "

            If cbxMulta.Checked = True Then
                txtObsMulta.Enabled = True
            Else
                txtObsMulta.Enabled = False
            End If

            Dim dt = bd.sqlSelect(SQL)

            If dt.Rows.Count = 0 Then
                MessageBox.Show("Error al consutar el cliente, faltan datos en su ficha", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            Else
                txtRutcli.Text = dt.Rows(0).Item("rutcli").ToString
                txtRazons.Text = dt.Rows(0).Item("razons").ToString
                txtRelCom.Text = dt.Rows(0).Item("numrel").ToString
                txtSegmento.Text = dt.Rows(0).Item("segmento").ToString
                txtUDN.Text = dt.Rows(0).Item("UDN").ToString
                txtVendedor.Text = dt.Rows(0).Item("vendedor").ToString
                txtNumcot.Text = dt.Rows(0).Item("numcot").ToString
                txtMontoCot.Text = dt.Rows(0).Item("totnet").ToString
                txtMontoCot.Text = FormatNumber(txtMontoCot.Text, 0)
                txtAnalistaConvenio.Text = dt.Rows(0).Item("analista").ToString
                txtEstadoConvenio.Text = dt.Rows(0).Item("estado").ToString
                txtFeciniConvenio.Text = dt.Rows(0).Item("fecemi").ToString
                txtFecfinConvenio.Text = dt.Rows(0).Item("fecven").ToString
                txtVentaConConvenio.Text = dt.Rows(0).Item("ventaconvenio")
                txtVentaConConvenio.Text = FormatNumber(txtVentaConConvenio.Text, 0)
                txtVentaSinConvenio.Text = dt.Rows(0).Item("ventasinconvenio")
                txtVentaSinConvenio.Text = FormatNumber(txtVentaSinConvenio.Text, 0)
                txtFecUltMod.Text = dt.Rows(0).Item("fecultmod").ToString
                txtObserv.Text = dt.Rows(0).Item("observ").ToString
                btn_VerConvenio.Enabled = True
                btn_VerRelacionado.Enabled = True
                btn_Polinomio.Enabled = True
                btn_Historia.Enabled = True
                btn_Contratos.Enabled = True
                If txtNumcot.Text = "0" Then
                    btn_VerConvenio.Enabled = False
                End If

                If dt.Rows(0).Item("fecemi").ToString <> "Sin convenio" Then
                    SQL = "select valpar from ma_paridad
                       WHERE CODMON = 1
                         AND TRUNC(FECPAR) = '" & dt.Rows(0).Item("fecemi").ToString & "'"
                    dt1 = bd.sqlSelect(SQL)

                    If txtDolar.Text.ToString <> "" Then
                        txtDolar.Text = dt1.Rows(0).Item("valpar").ToString
                    End If
                End If

                SQL = " select opl, rebate, obsrebate, bolgaran, unibole, mulcum, obsmul, feccon, cosprot from ma_clientes_convenios
                        where rutcli = " & txtRutcli.Text
                dt = bd.sqlSelect(SQL)

                If dt.Rows.Count > 0 Then
                    txtOPL.Text = dt.Rows(0).Item("opl").ToString
                    txtRebate.Text = dt.Rows(0).Item("rebate").ToString
                    txtObsRebate.Text = dt.Rows(0).Item("obsrebate").ToString
                    txtBolGarantia.Text = dt.Rows(0).Item("bolgaran").ToString
                    txtUniBoleta.Text = dt.Rows(0).Item("unibole").ToString
                    If dt.Rows(0).Item("mulcum").ToString = "S" Then
                        cbxMulta.Checked = True
                    Else
                        cbxMulta.Checked = False
                    End If
                    txtObsMulta.Text = dt.Rows(0).Item("obsmul").ToString
                    dtpFechaContrato.Value = dt.Rows(0).Item("feccon").ToString
                    If dt.Rows(0).Item("cosprot").ToString = "S" Then
                        cbxClienteProtegido.Checked = True
                    Else
                        cbxClienteProtegido.Checked = False
                    End If
                End If

                SQL = " select distinct rutcli  "
                SQL = SQL & " FROM re_emprela   "
                SQL = SQL & " where numrel in(" + txtRelCom.Text + ")   "
                SQL = SQL & " order by rutcli "
                Dim dtClientes = bd.sqlSelect(SQL)
                If dtClientes.Rows.Count = 0 Then
                    rutrelacionado = txtRutcli.Text
                Else
                    rutrelacionado = txtRutcli.Text
                    For i = 0 To dtClientes.Rows.Count - 1
                        rutrelacionado += "," + dtClientes.Rows(i).Item("rutcli").ToString
                    Next
                End If
            End If

        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub

    Private Sub txtUDN_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtVendedor.KeyPress, txtUDN.KeyPress, txtSegmento.KeyPress, txtNumcot.KeyPress, txtMontoCot.KeyPress, txtVentaSinConvenio.KeyPress, txtVentaConConvenio.KeyPress
        e.Handled = True
    End Sub

    Private Sub btn_VerRelacionado_Click(sender As Object, e As EventArgs) Handles btn_VerRelacionado.Click
        PuntoControl.RegistroUso("192")
        Dim form As New frmConsultaRutRelacionado(txtRutcli.Text, txtRazons.Text)
        form.ShowDialog()
    End Sub

    Private Sub btn_VerConvenio_Click(sender As Object, e As EventArgs) Handles btn_VerConvenio.Click
        PuntoControl.RegistroUso("193")
        Dim form As New frmDetalleConvenio(txtRutcli.Text, txtNumcot.Text, txtRazons.Text)
        form.ShowDialog()
    End Sub

    Private Sub btn_Limpiar_Click(sender As Object, e As EventArgs) Handles btn_Limpiar.Click
        rutrelacionado = ""
        btn_VerConvenio.Enabled = False
        btn_VerRelacionado.Enabled = False
        btn_Polinomio.Enabled = False
        btn_Historia.Enabled = False
        btn_Contratos.Enabled = False
        txtObserv.Text = ""
        txtFecfinConvenio.Text = ""
        txtFeciniConvenio.Text = ""
        txtFecUltMod.Text = ""
        txtAnalistaConvenio.Text = ""
        txtEstadoConvenio.Text = ""
        txtMontoCot.Text = ""
        txtNumcot.Text = ""
        txtRazons.Text = ""
        txtRelCom.Text = ""
        txtRutcli.Text = ""
        txtVendedor.Text = ""
        txtSegmento.Text = ""
        txtUDN.Text = ""
    End Sub

    Private Sub btn_Polinomio_Click(sender As Object, e As EventArgs) Handles btn_Polinomio.Click
        PuntoControl.RegistroUso("194")
        Dim form As New frmPolinomioCliente(txtRutcli.Text, txtRazons.Text)
        form.ShowDialog()
    End Sub

    Private Sub btn_Historia_Click(sender As Object, e As EventArgs) Handles btn_Historia.Click
        PuntoControl.RegistroUso("196")
        Dim form As New frmHistoriaCliente(txtRutcli.Text, txtRazons.Text, txtRelCom.Text, rutrelacionado)
        form.ShowDialog()
    End Sub

    Private Sub btn_Contratos_Click(sender As Object, e As EventArgs) Handles btn_Contratos.Click
        PuntoControl.RegistroUso("195")
        Dim form As New frmContratos(txtRutcli.Text, txtRazons.Text, txtRelCom.Text)
        form.ShowDialog()
    End Sub

    Private Sub txtRutcli_Click(sender As Object, e As EventArgs) Handles txtRutcli.Click
        txtRutcli.SelectAll()
    End Sub

    Private Sub txtRutcli_DoubleClick(sender As Object, e As EventArgs) Handles txtRutcli.DoubleClick
        btn_Limpiar.PerformClick()
    End Sub

    Private Sub btn_Reajustes_Click(sender As Object, e As EventArgs) Handles btn_Reajustes.Click
        PuntoControl.RegistroUso("197")
        Dim form As New frmReajustes(txtRutcli.Text, txtRazons.Text, txtRelCom.Text, rutrelacionado)
        form.ShowDialog()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Using conn = New OracleConnection(Conexion.retornaConexion)
            Dim comando As OracleCommand
            Dim dataAdapter As OracleDataAdapter
            Dim sql As String
            Dim dt As New DataTable
            conn.Open()
            Using transaccion = conn.BeginTransaction
                Try
                    PuntoControl.RegistroUso("198")
                    sql = "set role rol_aplicacion"
                    comando = New OracleCommand(Sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()
                    'inserto cotizac

                    Sql = "select 1 from ma_clientes_convenios where rutcli = " & txtRutcli.Text
                    dt = bd.sqlSelect(sql)

                    If dt.Rows.Count = 0 Then
                        sql = "insert into ma_clientes_convenios (rutcli, rsocial, opl, rebate, obsrebate, bolgaran, unibole, mulcum, obsmul, feccon, cosprot, obsexclu)
                                                        values(" & txtRutcli.Text & ", '" &
                                                        txtRazons.Text & "', " &
                                                        IIf(txtOPL.Text = "", "0", txtOPL.Text) & ", " &
                                                        IIf(txtRebate.Text = "", "0", txtRebate.Text) & ", '" &
                                                        IIf(txtObsRebate.Text = "", "Sin observación", txtObsRebate.Text) & "', " &
                                                        IIf(txtBolGarantia.Text = "", "0", txtBolGarantia.Text) & ", " &
                                                        "'" & txtUniBoleta.Text & "', "
                        If cbxMulta.Checked = True Then
                            sql = sql & " 'S', '" & txtObsMulta.Text & "', "
                        Else
                            sql = sql & " 'N', '', "
                        End If
                        sql = sql & " to_date('" & dtpFechaContrato.Value.ToShortDateString & "','dd/mm/yyyy') " & ", "
                        If cbxClienteProtegido.Checked = True Then
                            sql = sql & "'S', '')"
                        Else
                            sql = sql & "'N', '') "
                        End If
                        'sql = sql & "'" & txtObsMulta.Text & "')"
                        comando = New OracleCommand(sql, conn)
                        comando.Transaction = transaccion
                        comando.ExecuteNonQuery()

                    Else
                        sql = "Update ma_clientes_convenios 
                                  set opl = " & IIf(txtOPL.Text = "", "0", txtOPL.Text) & ",
                                  rebate = " & IIf(txtRebate.Text = "", "0", txtRebate.Text) & ",
                                  obsrebate = '" & IIf(txtObsRebate.Text = "", "Sin observación", txtObsRebate.Text) & "',
                                  bolgaran = " & IIf(txtBolGarantia.Text = "", "0", txtBolGarantia.Text) & ", 
                                  unibole = '" & txtUniBoleta.Text & "', "
                        If cbxMulta.Checked = True Then
                            sql = sql & " mulcum = 'S', obsmul = '" & txtObsRebate.Text & "', "
                        Else
                            sql = sql & " mulcum = 'N', obsmul = '', "
                        End If
                        sql = sql & "feccon = to_date('" & dtpFechaContrato.Value.ToShortDateString & "','dd/mm/yyyy'), "
                        If cbxClienteProtegido.Checked = True Then
                            sql = sql & "cosprot = 'S', "
                        Else
                            sql = sql & "cosprot = 'N', "
                        End If
                        sql = sql & "obsexclu = '" & txtObsMulta.Text & "'
                          where rutcli = " & txtRutcli.Text
                        comando = New OracleCommand(sql, conn)
                        comando.Transaction = transaccion
                        comando.ExecuteNonQuery()
                    End If

                    transaccion.Commit()

                    MessageBox.Show("Datos Guardados correctamente.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)

                Catch ex As Exception
                    PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                    MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    transaccion.Rollback()
                Finally
                    btnGuardar.Enabled = True
                    conn.Close()
                End Try
            End Using
        End Using
    End Sub

    Private Sub txtOPL_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtOPL.KeyPress
        Globales.soloNumeros(sender, e)
    End Sub

    Private Sub txtRebate_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtRebate.KeyPress
        Globales.soloNumeros(sender, e)
    End Sub

    Private Sub txtBolGarantia_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBolGarantia.KeyPress
        Globales.soloNumeros(sender, e)
    End Sub

    Private Sub cbxMulta_Click(sender As Object, e As EventArgs) Handles cbxMulta.Click
        If cbxMulta.Checked = True Then
            txtObsMulta.Enabled = True
        Else
            txtObsMulta.Enabled = False
        End If
    End Sub
End Class