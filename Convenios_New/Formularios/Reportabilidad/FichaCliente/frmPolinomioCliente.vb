﻿Imports System.Data.OracleClient
Public Class frmPolinomioCliente
    Dim bd As New Conexion
    Dim rutcli As String
    Dim razons As String
    Dim PuntoControl As New PuntoControl
    Private Sub btn_Salir_Click(sender As Object, e As EventArgs) Handles btn_Salir.Click
        Close()
    End Sub

    Private Sub frmPolinomioCliente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        PuntoControl.RegistroUso("202")
        Me.Location = New Point(0, 0)
        StartPosition = FormStartPosition.CenterParent
        cargaCombo()
    End Sub
    Public Sub New()

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub
    Public Sub New(rutcli_ As String, razons_ As String)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        rutcli = rutcli_
        razons = razons_
        txtRutcli.Text = rutcli
        txtRazons.Text = razons
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        buscapolinomio()
    End Sub
    Private Sub cargaCombo()
        Try
            bd.open_dimerc()
            Dim sql = "select codlin,deslin from ma_lineapr order by deslin"
            Dim dt = bd.sqlSelect(sql)
            Dim dt2 = dt.Copy
            Dim dt3 = dt.Copy
            Dim dt4 = dt.Copy
            cmbLineaA.DataSource = dt
            cmbLineaA.SelectedIndex = -1
            cmbLineaA.ValueMember = "codlin"
            cmbLineaA.DisplayMember = "deslin"
            cmbLineaA.DropDownStyle = ComboBoxStyle.DropDown
            cmbLineaA.Text = ""
            cmbLineaA.Refresh()

            cmbLineaB.DataSource = dt2
            cmbLineaB.SelectedIndex = -1
            cmbLineaB.ValueMember = "codlin"
            cmbLineaB.DisplayMember = "deslin"
            cmbLineaB.DropDownStyle = ComboBoxStyle.DropDown
            cmbLineaB.Text = ""
            cmbLineaB.Refresh()

            cmbLineaC.DataSource = dt3
            cmbLineaC.SelectedIndex = -1
            cmbLineaC.ValueMember = "codlin"
            cmbLineaC.DisplayMember = "deslin"
            cmbLineaC.DropDownStyle = ComboBoxStyle.DropDown
            cmbLineaC.Text = ""
            cmbLineaC.Refresh()

            cmbLineaD.DataSource = dt4
            cmbLineaD.SelectedIndex = -1
            cmbLineaD.ValueMember = "codlin"
            cmbLineaD.DisplayMember = "deslin"
            cmbLineaD.DropDownStyle = ComboBoxStyle.DropDown
            cmbLineaD.Text = ""
            cmbLineaD.Refresh()
        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub
    Private Sub buscapolinomio()
        Try
            bd.open_dimerc()
            Dim Sql = " select codlin,ipc,dolar,celulosa,letra from ma_polinomio where codemp=3 and rutcli=" + rutcli
            Sql = Sql & " order by letra "
            Dim dt = bd.sqlSelect(Sql)
            If dt.Rows.Count = 0 Then
                Exit Sub
            Else
                For i = 0 To dt.Rows.Count - 1
                    If dt.Rows(i).Item("letra") = "A" Then
                        cmbLineaA.SelectedValue = dt.Rows(i).Item("codlin")
                        txtIpcA.Text = dt.Rows(i).Item("ipc")
                        txtDolarA.Text = dt.Rows(i).Item("dolar")
                        txtCelulosaA.Text = dt.Rows(i).Item("celulosa")
                        Continue For
                    End If
                    If dt.Rows(i).Item("letra") = "B" Then
                        cmbLineaB.SelectedValue = dt.Rows(i).Item("codlin")
                        txtIpcB.Text = dt.Rows(i).Item("ipc")
                        txtDolarB.Text = dt.Rows(i).Item("dolar")
                        txtCelulosaB.Text = dt.Rows(i).Item("celulosa")
                        Continue For
                    End If
                    If dt.Rows(i).Item("letra") = "C" Then
                        cmbLineaC.SelectedValue = dt.Rows(i).Item("codlin")
                        txtIpcC.Text = dt.Rows(i).Item("ipc")
                        txtDolarC.Text = dt.Rows(i).Item("dolar")
                        txtCelulosaC.Text = dt.Rows(i).Item("celulosa")
                        Continue For
                    End If
                    If dt.Rows(i).Item("letra") = "D" Then
                        cmbLineaD.SelectedValue = dt.Rows(i).Item("codlin")
                        txtIpcD.Text = dt.Rows(i).Item("ipc")
                        txtDolarD.Text = dt.Rows(i).Item("dolar")
                        txtCelulosaD.Text = dt.Rows(i).Item("celulosa")
                        Continue For
                    End If
                Next
            End If

        Catch ex As Exception
            PuntoControl.PutError(Me.Name & " // " & ex.ToString)
            MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            bd.close()
        End Try
    End Sub
    Private Sub limpiapolinomio()
        cmbLineaA.SelectedIndex = -1
        cmbLineaB.SelectedIndex = -1
        cmbLineaC.SelectedIndex = -1
        cmbLineaD.SelectedIndex = -1
        txtDolarA.Text = ""
        txtIpcA.Text = ""
        txtCelulosaA.Text = ""
        txtDolarB.Text = ""
        txtIpcB.Text = ""
        txtCelulosaB.Text = ""
        txtDolarC.Text = ""
        txtIpcC.Text = ""
        txtCelulosaC.Text = ""
        txtDolarD.Text = ""
        txtIpcD.Text = ""
        txtCelulosaD.Text = ""
        chkEliminaA.Checked = False
        chkEliminaB.Checked = False
        chkEliminaC.Checked = False
        chkEliminaD.Checked = False
    End Sub
    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles btn_GrabarPolinomio.Click
        Using conn = New OracleConnection(Conexion.retornaConexion)
            conn.Open()
            Dim comando As OracleCommand
            Dim dataadapter As OracleDataAdapter
            Dim sql As String
            PuntoControl.RegistroUso("203")
            Using transaccion = conn.BeginTransaction
                Try

                    'PRIMERO AL RUT DEL CLIENTE
                    If cmbLineaA.SelectedIndex > 0 Then
                        Dim dt As New DataTable
                        sql = "select 1 from ma_polinomio where codemp=3 and rutcli=" + rutcli + " and letra='A'"
                        comando = New OracleCommand(sql, conn)
                        comando.Transaction = transaccion
                        dataadapter = New OracleDataAdapter(comando)
                        dataadapter.Fill(dt)
                        If dt.Rows.Count > 0 Then
                            sql = "update ma_polinomio set "
                            sql += " codlin =" + cmbLineaA.SelectedValue.ToString
                            sql += " ,ipc = " + txtIpcA.Text.Replace(",", ".")
                            sql += " ,dolar = " + txtDolarA.Text.Replace(",", ".")
                            sql += " ,celulosa = " + txtCelulosaA.Text.Replace(",", ".")
                            sql += " where codemp=3 and rutcli=" + rutcli + " and letra ='A'"
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()
                        Else
                            sql = " insert into ma_polinomio(codemp,rutcli,codlin,letra,ipc,dolar,celulosa) "
                            sql = sql & "values(3," + rutcli + ","
                            sql += cmbLineaA.SelectedValue.ToString + ","
                            sql += "'A',"
                            sql += txtIpcA.Text.Replace(",", ".") + ","
                            sql += txtDolarA.Text.Replace(",", ".") + ","
                            sql += txtCelulosaA.Text.Replace(",", ".") + ""
                            sql += ") "
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()
                        End If
                    End If
                    If cmbLineaB.SelectedIndex > 0 Then
                        Dim dt As New DataTable
                        sql = "select 1 from ma_polinomio where codemp=3 and rutcli=" + rutcli + " and letra='B'"
                        comando = New OracleCommand(sql, conn)
                        comando.Transaction = transaccion
                        dataadapter = New OracleDataAdapter(comando)
                        dataadapter.Fill(dt)
                        If dt.Rows.Count > 0 Then
                            sql = "update ma_polinomio set "
                            sql += " codlin =" + cmbLineaB.SelectedValue.ToString
                            sql += " ,ipc = " + txtIpcB.Text.Replace(",", ".")
                            sql += " ,dolar = " + txtDolarB.Text.Replace(",", ".")
                            sql += " ,celulosa = " + txtCelulosaB.Text.Replace(",", ".")
                            sql += " where codemp=3 and rutcli=" + rutcli + " and letra ='B'"
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()
                        Else
                            sql = " insert into ma_polinomio(codemp,rutcli,codlin,letra,ipc,dolar,celulosa) "
                            sql = sql & "values(3," + rutcli + ","
                            sql += cmbLineaB.SelectedValue.ToString + ","
                            sql += "'B',"
                            sql += txtIpcB.Text.Replace(",", ".") + ","
                            sql += txtDolarB.Text.Replace(",", ".") + ","
                            sql += txtCelulosaB.Text.Replace(",", ".") + ""
                            sql += ") "
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()
                        End If
                    End If
                    If cmbLineaC.SelectedIndex > 0 Then
                        Dim dt As New DataTable
                        sql = "select 1 from ma_polinomio where codemp=3 and rutcli=" + rutcli + " and letra='C'"
                        comando = New OracleCommand(sql, conn)
                        comando.Transaction = transaccion
                        dataadapter = New OracleDataAdapter(comando)
                        dataadapter.Fill(dt)
                        If dt.Rows.Count > 0 Then
                            sql = "update ma_polinomio set "
                            sql += " codlin =" + cmbLineaC.SelectedValue.ToString
                            sql += " ,ipc = " + txtIpcC.Text.Replace(",", ".")
                            sql += " ,dolar = " + txtDolarC.Text.Replace(",", ".")
                            sql += " ,celulosa = " + txtCelulosaC.Text.Replace(",", ".")
                            sql += " where codemp=3 and rutcli=" + rutcli + " and letra ='C'"
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()
                        Else
                            sql = " insert into ma_polinomio(codemp,rutcli,codlin,letra,ipc,dolar,celulosa) "
                            sql = sql & "values(3," + rutcli + ","
                            sql += cmbLineaC.SelectedValue.ToString + ","
                            sql += "'C',"
                            sql += txtIpcC.Text.Replace(",", ".") + ","
                            sql += txtDolarC.Text.Replace(",", ".") + ","
                            sql += txtCelulosaC.Text.Replace(",", ".") + ""
                            sql += ") "
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()
                        End If
                    End If
                    If cmbLineaD.SelectedIndex > 0 Then
                        Dim dt As New DataTable
                        sql = "select 1 from ma_polinomio where codemp=3 and rutcli=" + rutcli + " and letra='D'"
                        comando = New OracleCommand(sql, conn)
                        comando.Transaction = transaccion
                        dataadapter = New OracleDataAdapter(comando)
                        dataadapter.Fill(dt)
                        If dt.Rows.Count > 0 Then
                            sql = "update ma_polinomio set "
                            sql += " codlin =" + cmbLineaD.SelectedValue.ToString
                            sql += " ,ipc = " + txtIpcD.Text.Replace(",", ".")
                            sql += " ,dolar = " + txtDolarD.Text.Replace(",", ".")
                            sql += " ,celulosa = " + txtCelulosaD.Text.Replace(",", ".")
                            sql += " where codemp=3 and rutcli=" + rutcli + " and letra ='D'"
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()
                        Else
                            sql = " insert into ma_polinomio(codemp,rutcli,codlin,letra,ipc,dolar,celulosa) "
                            sql = sql & "values(3," + rutcli + ","
                            sql += cmbLineaD.SelectedValue.ToString + ","
                            sql += "'D',"
                            sql += txtIpcD.Text.Replace(",", ".") + ","
                            sql += txtDolarD.Text.Replace(",", ".") + ","
                            sql += txtCelulosaD.Text.Replace(",", ".") + ""
                            sql += ") "
                            comando = New OracleCommand(sql, conn)
                            comando.Transaction = transaccion
                            comando.ExecuteNonQuery()
                        End If
                    End If
                    If chkEliminaA.Checked = True Then
                        sql = "delete from ma_polinomio where codemp=3 and rutcli=" + rutcli + " and letra='A'"
                        comando = New OracleCommand(sql, conn)
                        comando.Transaction = transaccion
                        comando.ExecuteNonQuery()
                    End If
                    If chkEliminaB.Checked = True Then
                        sql = "delete from ma_polinomio where codemp=3 and rutcli=" + rutcli + " and letra='B'"
                        comando = New OracleCommand(sql, conn)
                        comando.Transaction = transaccion
                        comando.ExecuteNonQuery()
                    End If
                    If chkEliminaC.Checked = True Then
                        sql = "delete from ma_polinomio where codemp=3 and rutcli=" + rutcli + " and letra='C'"
                        comando = New OracleCommand(sql, conn)
                        comando.Transaction = transaccion
                        comando.ExecuteNonQuery()
                    End If
                    If chkEliminaD.Checked = True Then
                        sql = "delete from ma_polinomio where codemp=3 and rutcli=" + rutcli + " and letra='D'"
                        comando = New OracleCommand(sql, conn)
                        comando.Transaction = transaccion
                        comando.ExecuteNonQuery()
                    End If
                    If chkRelacionado.Checked = True Then
                        'AHORA REVISO SI TIENE RELACIONADOS Y LE HAGO LO MISMO
                        sql = " select rutcli,getrazonsocial(codemp,rutcli) razons "
                        sql = sql & "FROM re_emprela  "
                        sql = sql & "where numrel in(select numrel from re_emprela where rutcli= " + rutcli + " and codemp=3)   "
                        sql = sql & "and rutcli not in(select rutcli from re_emprela where rutcli= " + rutcli + "  and codemp=3)order by rutcli "
                        Dim relacionado = bd.sqlSelect(sql)

                        If relacionado.Rows.Count > 0 Then
                            For i = 0 To relacionado.Rows.Count - 1
                                Dim rutrelacionado = relacionado.Rows(i).Item("rutcli").ToString

                                If cmbLineaA.SelectedIndex > 0 Then
                                    Dim dt As New DataTable
                                    sql = "select 1 from ma_polinomio where codemp=3 and rutcli=" + rutrelacionado + " and letra='A'"
                                    comando = New OracleCommand(sql, conn)
                                    comando.Transaction = transaccion
                                    dataadapter = New OracleDataAdapter(comando)
                                    dataadapter.Fill(dt)
                                    If dt.Rows.Count > 0 Then
                                        sql = "update ma_polinomio set "
                                        sql += " codlin =" + cmbLineaA.SelectedValue.ToString
                                        sql += " ,ipc = " + txtIpcA.Text.Replace(",", ".")
                                        sql += " ,dolar = " + txtDolarA.Text.Replace(",", ".")
                                        sql += " ,celulosa = " + txtCelulosaA.Text.Replace(",", ".")
                                        sql += " where codemp=3 and rutcli=" + rutrelacionado + " and letra ='A'"
                                        comando = New OracleCommand(sql, conn)
                                        comando.Transaction = transaccion
                                        comando.ExecuteNonQuery()
                                    Else
                                        sql = " insert into ma_polinomio(codemp,rutcli,codlin,letra,ipc,dolar,celulosa) "
                                        sql = sql & "values(3," + rutrelacionado + ","
                                        sql += cmbLineaA.SelectedValue.ToString + ","
                                        sql += "'A',"
                                        sql += txtIpcA.Text.Replace(",", ".") + ","
                                        sql += txtDolarA.Text.Replace(",", ".") + ","
                                        sql += txtCelulosaA.Text.Replace(",", ".") + ""
                                        sql += ") "
                                        comando = New OracleCommand(sql, conn)
                                        comando.Transaction = transaccion
                                        comando.ExecuteNonQuery()
                                    End If
                                End If
                                If cmbLineaB.SelectedIndex > 0 Then
                                    Dim dt As New DataTable
                                    sql = "select 1 from ma_polinomio where codemp=3 and rutcli=" + rutrelacionado + " and letra='B'"
                                    comando = New OracleCommand(sql, conn)
                                    comando.Transaction = transaccion
                                    dataadapter = New OracleDataAdapter(comando)
                                    dataadapter.Fill(dt)
                                    If dt.Rows.Count > 0 Then
                                        sql = "update ma_polinomio set "
                                        sql += " codlin =" + cmbLineaB.SelectedValue.ToString
                                        sql += " ,ipc = " + txtIpcB.Text.Replace(",", ".")
                                        sql += " ,dolar = " + txtDolarB.Text.Replace(",", ".")
                                        sql += " ,celulosa = " + txtCelulosaB.Text.Replace(",", ".")
                                        sql += " where codemp=3 and rutcli=" + rutrelacionado + " and letra ='B'"
                                        comando = New OracleCommand(sql, conn)
                                        comando.Transaction = transaccion
                                        comando.ExecuteNonQuery()
                                    Else
                                        sql = " insert into ma_polinomio(codemp,rutcli,codlin,letra,ipc,dolar,celulosa) "
                                        sql = sql & "values(3," + rutrelacionado + ","
                                        sql += cmbLineaB.SelectedValue.ToString + ","
                                        sql += "'B',"
                                        sql += txtIpcB.Text.Replace(",", ".") + ","
                                        sql += txtDolarB.Text.Replace(",", ".") + ","
                                        sql += txtCelulosaB.Text.Replace(",", ".") + ""
                                        sql += ") "
                                        comando = New OracleCommand(sql, conn)
                                        comando.Transaction = transaccion
                                        comando.ExecuteNonQuery()
                                    End If
                                End If
                                If cmbLineaC.SelectedIndex > 0 Then
                                    Dim dt As New DataTable
                                    sql = "select 1 from ma_polinomio where codemp=3 and rutcli=" + rutrelacionado + " and letra='C'"
                                    comando = New OracleCommand(sql, conn)
                                    comando.Transaction = transaccion
                                    dataadapter = New OracleDataAdapter(comando)
                                    dataadapter.Fill(dt)
                                    If dt.Rows.Count > 0 Then
                                        sql = "update ma_polinomio set "
                                        sql += " codlin =" + cmbLineaC.SelectedValue.ToString
                                        sql += " ,ipc = " + txtIpcC.Text.Replace(",", ".")
                                        sql += " ,dolar = " + txtDolarC.Text.Replace(",", ".")
                                        sql += " ,celulosa = " + txtCelulosaC.Text.Replace(",", ".")
                                        sql += " where codemp=3 and rutcli=" + rutrelacionado + " and letra ='C'"
                                        comando = New OracleCommand(sql, conn)
                                        comando.Transaction = transaccion
                                        comando.ExecuteNonQuery()
                                    Else
                                        sql = " insert into ma_polinomio(codemp,rutcli,codlin,letra,ipc,dolar,celulosa) "
                                        sql = sql & "values(3," + rutrelacionado + ","
                                        sql += cmbLineaC.SelectedValue.ToString + ","
                                        sql += "'C',"
                                        sql += txtIpcC.Text.Replace(",", ".") + ","
                                        sql += txtDolarC.Text.Replace(",", ".") + ","
                                        sql += txtCelulosaC.Text.Replace(",", ".") + ""
                                        sql += ") "
                                        comando = New OracleCommand(sql, conn)
                                        comando.Transaction = transaccion
                                        comando.ExecuteNonQuery()
                                    End If
                                End If
                                If cmbLineaD.SelectedIndex > 0 Then
                                    Dim dt As New DataTable
                                    sql = "select 1 from ma_polinomio where codemp=3 and rutcli=" + rutrelacionado + " and letra='D'"
                                    comando = New OracleCommand(sql, conn)
                                    comando.Transaction = transaccion
                                    dataadapter = New OracleDataAdapter(comando)
                                    dataadapter.Fill(dt)
                                    If dt.Rows.Count > 0 Then
                                        sql = "update ma_polinomio set "
                                        sql += " codlin =" + cmbLineaD.SelectedValue.ToString
                                        sql += " ,ipc = " + txtIpcD.Text.Replace(",", ".")
                                        sql += " ,dolar = " + txtDolarD.Text.Replace(",", ".")
                                        sql += " ,celulosa = " + txtCelulosaD.Text.Replace(",", ".")
                                        sql += " where codemp=3 and rutcli=" + rutrelacionado + " and letra ='D'"
                                        comando = New OracleCommand(sql, conn)
                                        comando.Transaction = transaccion
                                        comando.ExecuteNonQuery()
                                    Else
                                        sql = " insert into ma_polinomio(codemp,rutcli,codlin,letra,ipc,dolar,celulosa) "
                                        sql = sql & "values(3," + rutrelacionado + ","
                                        sql += cmbLineaD.SelectedValue.ToString + ","
                                        sql += "'D',"
                                        sql += txtIpcD.Text.Replace(",", ".") + ","
                                        sql += txtDolarD.Text.Replace(",", ".") + ","
                                        sql += txtCelulosaD.Text.Replace(",", ".") + ""
                                        sql += ") "
                                        comando = New OracleCommand(sql, conn)
                                        comando.Transaction = transaccion
                                        comando.ExecuteNonQuery()
                                    End If
                                End If
                                If chkEliminaA.Checked = True Then
                                    sql = "delete from ma_polinomio where codemp=3 and rutcli=" + rutrelacionado + " and letra='A'"
                                    comando = New OracleCommand(sql, conn)
                                    comando.Transaction = transaccion
                                    comando.ExecuteNonQuery()
                                End If
                                If chkEliminaB.Checked = True Then
                                    sql = "delete from ma_polinomio where codemp=3 and rutcli=" + rutrelacionado + " and letra='B'"
                                    comando = New OracleCommand(sql, conn)
                                    comando.Transaction = transaccion
                                    comando.ExecuteNonQuery()
                                End If
                                If chkEliminaC.Checked = True Then
                                    sql = "delete from ma_polinomio where codemp=3 and rutcli=" + rutrelacionado + " and letra='C'"
                                    comando = New OracleCommand(sql, conn)
                                    comando.Transaction = transaccion
                                    comando.ExecuteNonQuery()
                                End If
                                If chkEliminaD.Checked = True Then
                                    sql = "delete from ma_polinomio where codemp=3 and rutcli=" + rutrelacionado + " and letra='D'"
                                    comando = New OracleCommand(sql, conn)
                                    comando.Transaction = transaccion
                                    comando.ExecuteNonQuery()
                                End If
                            Next
                        End If
                    End If

                    transaccion.Commit()
                    MessageBox.Show("Cambios de polinomio hechos con éxito")
                    limpiapolinomio()
                    buscapolinomio()
                Catch ex As Exception
                    If Not transaccion.Equals(Nothing) Then
                        transaccion.Rollback()
                    End If
                    PuntoControl.PutError(Me.Name & " // " & ex.ToString)
                    MessageBox.Show("Ha ocurrido un error.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Finally
                    bd.close()
                End Try
            End Using
        End Using
    End Sub

    Private Sub txtIpcA_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtIpcA.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            txtDolarA.Focus()
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtDolarA_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDolarA.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            txtCelulosaA.Focus()
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtCelulosaA_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCelulosaA.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            cmbLineaB.Focus()
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub cmbLineaA_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbLineaA.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            txtIpcA.Focus()
        End If
    End Sub

    Private Sub cmbLineaB_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbLineaB.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            txtIpcB.Focus()
        End If
    End Sub

    Private Sub cmbLineaC_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbLineaC.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            txtIpcC.Focus()
        End If
    End Sub

    Private Sub cmbLineaD_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbLineaD.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            txtIpcD.Focus()
        End If
    End Sub

    Private Sub txtIpcB_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtIpcB.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            txtDolarB.Focus()
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtDolarB_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDolarB.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            txtCelulosaB.Focus()
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtCelulosaB_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCelulosaB.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            cmbLineaC.Focus()
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtIpcC_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtIpcC.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            txtDolarC.Focus()
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtDolarC_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDolarC.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            txtCelulosaC.Focus()
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtCelulosaC_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCelulosaC.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            cmbLineaD.Focus()
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtIpcD_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtIpcD.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            txtDolarD.Focus()
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtDolarD_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDolarD.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            txtCelulosaD.Focus()
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtCelulosaD_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCelulosaD.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            btn_GrabarPolinomio.Focus()
        ElseIf e.KeyChar = Convert.ToChar(Asc(".")) Or e.KeyChar = Convert.ToChar(Asc(",")) Then
            e.KeyChar = Convert.ToChar(Asc(","))
        Else
            Globales.soloNumeros(sender, e)
        End If
    End Sub

    Private Sub txtRutcli_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtRutcli.KeyPress, txtRazons.KeyPress
        e.Handled = True
    End Sub
End Class