﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReajustes
    Inherits MetroFramework.Forms.MetroForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dgvDetalleReajustes = New System.Windows.Forms.DataGridView()
        Me.dgvReajustes = New System.Windows.Forms.DataGridView()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtRazons = New System.Windows.Forms.TextBox()
        Me.txtNumRel = New System.Windows.Forms.TextBox()
        Me.txtRutcli = New System.Windows.Forms.TextBox()
        Me.btn_Salir = New System.Windows.Forms.Button()
        Me.btn_ExcelDetalle = New System.Windows.Forms.Button()
        Me.btn_ExcelHistria = New System.Windows.Forms.Button()
        Me.txtmgant = New System.Windows.Forms.TextBox()
        Me.txtmgdif = New System.Windows.Forms.TextBox()
        Me.txtsumaant = New System.Windows.Forms.TextBox()
        Me.txtsumDif = New System.Windows.Forms.TextBox()
        Me.txtrebant = New System.Windows.Forms.TextBox()
        Me.txtrebdif = New System.Windows.Forms.TextBox()
        Me.txtgasant = New System.Windows.Forms.TextBox()
        Me.txtgasdif = New System.Windows.Forms.TextBox()
        Me.txtvenant = New System.Windows.Forms.TextBox()
        Me.txtventdif = New System.Windows.Forms.TextBox()
        Me.txtmgnew = New System.Windows.Forms.TextBox()
        Me.txtsumanew = New System.Windows.Forms.TextBox()
        Me.txttranant = New System.Windows.Forms.TextBox()
        Me.txtrebnew = New System.Windows.Forms.TextBox()
        Me.txttrandif = New System.Windows.Forms.TextBox()
        Me.txtgasnew = New System.Windows.Forms.TextBox()
        Me.txtcontant = New System.Windows.Forms.TextBox()
        Me.txtvennew = New System.Windows.Forms.TextBox()
        Me.txtcontdif = New System.Windows.Forms.TextBox()
        Me.txttrannew = New System.Windows.Forms.TextBox()
        Me.txtingAnt = New System.Windows.Forms.TextBox()
        Me.txtcontnew = New System.Windows.Forms.TextBox()
        Me.txtingdif = New System.Windows.Forms.TextBox()
        Me.txtingnew = New System.Windows.Forms.TextBox()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.Label66 = New System.Windows.Forms.Label()
        Me.Label67 = New System.Windows.Forms.Label()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.Label61 = New System.Windows.Forms.Label()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.btn_Base = New System.Windows.Forms.Button()
        Me.btn_Cliente = New System.Windows.Forms.Button()
        Me.btn_Ejecutivo = New System.Windows.Forms.Button()
        CType(Me.dgvDetalleReajustes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvReajustes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvDetalleReajustes
        '
        Me.dgvDetalleReajustes.AllowUserToAddRows = False
        Me.dgvDetalleReajustes.AllowUserToDeleteRows = False
        Me.dgvDetalleReajustes.AllowUserToResizeRows = False
        Me.dgvDetalleReajustes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetalleReajustes.Location = New System.Drawing.Point(12, 243)
        Me.dgvDetalleReajustes.Name = "dgvDetalleReajustes"
        Me.dgvDetalleReajustes.ReadOnly = True
        Me.dgvDetalleReajustes.RowHeadersVisible = False
        Me.dgvDetalleReajustes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvDetalleReajustes.Size = New System.Drawing.Size(1112, 187)
        Me.dgvDetalleReajustes.TabIndex = 48
        '
        'dgvReajustes
        '
        Me.dgvReajustes.AllowUserToAddRows = False
        Me.dgvReajustes.AllowUserToDeleteRows = False
        Me.dgvReajustes.AllowUserToResizeRows = False
        Me.dgvReajustes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvReajustes.Location = New System.Drawing.Point(12, 66)
        Me.dgvReajustes.Name = "dgvReajustes"
        Me.dgvReajustes.ReadOnly = True
        Me.dgvReajustes.RowHeadersVisible = False
        Me.dgvReajustes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvReajustes.Size = New System.Drawing.Size(1112, 171)
        Me.dgvReajustes.TabIndex = 47
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.txtRazons)
        Me.Panel1.Controls.Add(Me.txtNumRel)
        Me.Panel1.Controls.Add(Me.txtRutcli)
        Me.Panel1.Controls.Add(Me.btn_Salir)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1112, 48)
        Me.Panel1.TabIndex = 46
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(186, 15)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(73, 14)
        Me.Label2.TabIndex = 41
        Me.Label2.Text = "Razón Social"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(871, 15)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(81, 14)
        Me.Label3.TabIndex = 40
        Me.Label3.Text = "Num. relación"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(67, 14)
        Me.Label1.TabIndex = 40
        Me.Label1.Text = "Rut Cliente"
        '
        'txtRazons
        '
        Me.txtRazons.BackColor = System.Drawing.SystemColors.Info
        Me.txtRazons.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRazons.Location = New System.Drawing.Point(262, 12)
        Me.txtRazons.Name = "txtRazons"
        Me.txtRazons.Size = New System.Drawing.Size(603, 22)
        Me.txtRazons.TabIndex = 39
        '
        'txtNumRel
        '
        Me.txtNumRel.BackColor = System.Drawing.SystemColors.Info
        Me.txtNumRel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumRel.Location = New System.Drawing.Point(958, 12)
        Me.txtNumRel.Name = "txtNumRel"
        Me.txtNumRel.Size = New System.Drawing.Size(94, 22)
        Me.txtNumRel.TabIndex = 38
        '
        'txtRutcli
        '
        Me.txtRutcli.BackColor = System.Drawing.SystemColors.Info
        Me.txtRutcli.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRutcli.Location = New System.Drawing.Point(76, 12)
        Me.txtRutcli.Name = "txtRutcli"
        Me.txtRutcli.Size = New System.Drawing.Size(94, 22)
        Me.txtRutcli.TabIndex = 38
        '
        'btn_Salir
        '
        Me.btn_Salir.Image = Global.Convenios_New.My.Resources.Resources.salir
        Me.btn_Salir.Location = New System.Drawing.Point(1058, 5)
        Me.btn_Salir.Name = "btn_Salir"
        Me.btn_Salir.Size = New System.Drawing.Size(49, 38)
        Me.btn_Salir.TabIndex = 1
        Me.btn_Salir.UseVisualStyleBackColor = True
        '
        'btn_ExcelDetalle
        '
        Me.btn_ExcelDetalle.Image = Global.Convenios_New.My.Resources.Resources.Excel
        Me.btn_ExcelDetalle.Location = New System.Drawing.Point(1130, 243)
        Me.btn_ExcelDetalle.Name = "btn_ExcelDetalle"
        Me.btn_ExcelDetalle.Size = New System.Drawing.Size(49, 42)
        Me.btn_ExcelDetalle.TabIndex = 49
        Me.btn_ExcelDetalle.UseVisualStyleBackColor = True
        '
        'btn_ExcelHistria
        '
        Me.btn_ExcelHistria.Image = Global.Convenios_New.My.Resources.Resources.Excel
        Me.btn_ExcelHistria.Location = New System.Drawing.Point(1130, 66)
        Me.btn_ExcelHistria.Name = "btn_ExcelHistria"
        Me.btn_ExcelHistria.Size = New System.Drawing.Size(49, 47)
        Me.btn_ExcelHistria.TabIndex = 50
        Me.btn_ExcelHistria.UseVisualStyleBackColor = True
        '
        'txtmgant
        '
        Me.txtmgant.BackColor = System.Drawing.SystemColors.Info
        Me.txtmgant.Location = New System.Drawing.Point(930, 467)
        Me.txtmgant.Name = "txtmgant"
        Me.txtmgant.Size = New System.Drawing.Size(100, 22)
        Me.txtmgant.TabIndex = 101
        Me.txtmgant.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtmgdif
        '
        Me.txtmgdif.BackColor = System.Drawing.SystemColors.Info
        Me.txtmgdif.Location = New System.Drawing.Point(929, 546)
        Me.txtmgdif.Name = "txtmgdif"
        Me.txtmgdif.Size = New System.Drawing.Size(100, 22)
        Me.txtmgdif.TabIndex = 86
        Me.txtmgdif.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtsumaant
        '
        Me.txtsumaant.BackColor = System.Drawing.SystemColors.Info
        Me.txtsumaant.Location = New System.Drawing.Point(817, 467)
        Me.txtsumaant.Name = "txtsumaant"
        Me.txtsumaant.Size = New System.Drawing.Size(100, 22)
        Me.txtsumaant.TabIndex = 87
        Me.txtsumaant.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtsumDif
        '
        Me.txtsumDif.BackColor = System.Drawing.SystemColors.Info
        Me.txtsumDif.Location = New System.Drawing.Point(816, 546)
        Me.txtsumDif.Name = "txtsumDif"
        Me.txtsumDif.Size = New System.Drawing.Size(100, 22)
        Me.txtsumDif.TabIndex = 88
        Me.txtsumDif.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtrebant
        '
        Me.txtrebant.BackColor = System.Drawing.SystemColors.Info
        Me.txtrebant.Location = New System.Drawing.Point(704, 467)
        Me.txtrebant.Name = "txtrebant"
        Me.txtrebant.Size = New System.Drawing.Size(100, 22)
        Me.txtrebant.TabIndex = 89
        Me.txtrebant.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtrebdif
        '
        Me.txtrebdif.BackColor = System.Drawing.SystemColors.Info
        Me.txtrebdif.Location = New System.Drawing.Point(703, 546)
        Me.txtrebdif.Name = "txtrebdif"
        Me.txtrebdif.Size = New System.Drawing.Size(100, 22)
        Me.txtrebdif.TabIndex = 90
        Me.txtrebdif.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtgasant
        '
        Me.txtgasant.BackColor = System.Drawing.SystemColors.Info
        Me.txtgasant.Location = New System.Drawing.Point(588, 467)
        Me.txtgasant.Name = "txtgasant"
        Me.txtgasant.Size = New System.Drawing.Size(100, 22)
        Me.txtgasant.TabIndex = 91
        Me.txtgasant.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtgasdif
        '
        Me.txtgasdif.BackColor = System.Drawing.SystemColors.Info
        Me.txtgasdif.Location = New System.Drawing.Point(587, 546)
        Me.txtgasdif.Name = "txtgasdif"
        Me.txtgasdif.Size = New System.Drawing.Size(100, 22)
        Me.txtgasdif.TabIndex = 93
        Me.txtgasdif.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtvenant
        '
        Me.txtvenant.BackColor = System.Drawing.SystemColors.Info
        Me.txtvenant.Location = New System.Drawing.Point(473, 467)
        Me.txtvenant.Name = "txtvenant"
        Me.txtvenant.Size = New System.Drawing.Size(100, 22)
        Me.txtvenant.TabIndex = 100
        Me.txtvenant.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtventdif
        '
        Me.txtventdif.BackColor = System.Drawing.SystemColors.Info
        Me.txtventdif.Location = New System.Drawing.Point(472, 546)
        Me.txtventdif.Name = "txtventdif"
        Me.txtventdif.Size = New System.Drawing.Size(100, 22)
        Me.txtventdif.TabIndex = 94
        Me.txtventdif.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtmgnew
        '
        Me.txtmgnew.BackColor = System.Drawing.SystemColors.Info
        Me.txtmgnew.Location = New System.Drawing.Point(930, 497)
        Me.txtmgnew.Name = "txtmgnew"
        Me.txtmgnew.Size = New System.Drawing.Size(100, 22)
        Me.txtmgnew.TabIndex = 95
        Me.txtmgnew.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtsumanew
        '
        Me.txtsumanew.BackColor = System.Drawing.SystemColors.Info
        Me.txtsumanew.Location = New System.Drawing.Point(817, 497)
        Me.txtsumanew.Name = "txtsumanew"
        Me.txtsumanew.Size = New System.Drawing.Size(100, 22)
        Me.txtsumanew.TabIndex = 96
        Me.txtsumanew.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txttranant
        '
        Me.txttranant.BackColor = System.Drawing.SystemColors.Info
        Me.txttranant.Location = New System.Drawing.Point(356, 467)
        Me.txttranant.Name = "txttranant"
        Me.txttranant.Size = New System.Drawing.Size(100, 22)
        Me.txttranant.TabIndex = 97
        Me.txttranant.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtrebnew
        '
        Me.txtrebnew.BackColor = System.Drawing.SystemColors.Info
        Me.txtrebnew.Location = New System.Drawing.Point(704, 497)
        Me.txtrebnew.Name = "txtrebnew"
        Me.txtrebnew.Size = New System.Drawing.Size(100, 22)
        Me.txtrebnew.TabIndex = 98
        Me.txtrebnew.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txttrandif
        '
        Me.txttrandif.BackColor = System.Drawing.SystemColors.Info
        Me.txttrandif.Location = New System.Drawing.Point(355, 546)
        Me.txttrandif.Name = "txttrandif"
        Me.txttrandif.Size = New System.Drawing.Size(100, 22)
        Me.txttrandif.TabIndex = 99
        Me.txttrandif.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtgasnew
        '
        Me.txtgasnew.BackColor = System.Drawing.SystemColors.Info
        Me.txtgasnew.Location = New System.Drawing.Point(588, 497)
        Me.txtgasnew.Name = "txtgasnew"
        Me.txtgasnew.Size = New System.Drawing.Size(100, 22)
        Me.txtgasnew.TabIndex = 85
        Me.txtgasnew.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtcontant
        '
        Me.txtcontant.BackColor = System.Drawing.SystemColors.Info
        Me.txtcontant.Location = New System.Drawing.Point(241, 467)
        Me.txtcontant.Name = "txtcontant"
        Me.txtcontant.Size = New System.Drawing.Size(100, 22)
        Me.txtcontant.TabIndex = 92
        Me.txtcontant.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtvennew
        '
        Me.txtvennew.BackColor = System.Drawing.SystemColors.Info
        Me.txtvennew.Location = New System.Drawing.Point(473, 497)
        Me.txtvennew.Name = "txtvennew"
        Me.txtvennew.Size = New System.Drawing.Size(100, 22)
        Me.txtvennew.TabIndex = 84
        Me.txtvennew.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtcontdif
        '
        Me.txtcontdif.BackColor = System.Drawing.SystemColors.Info
        Me.txtcontdif.Location = New System.Drawing.Point(240, 546)
        Me.txtcontdif.Name = "txtcontdif"
        Me.txtcontdif.Size = New System.Drawing.Size(100, 22)
        Me.txtcontdif.TabIndex = 78
        Me.txtcontdif.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txttrannew
        '
        Me.txttrannew.BackColor = System.Drawing.SystemColors.Info
        Me.txttrannew.Location = New System.Drawing.Point(356, 497)
        Me.txttrannew.Name = "txttrannew"
        Me.txttrannew.Size = New System.Drawing.Size(100, 22)
        Me.txttrannew.TabIndex = 83
        Me.txttrannew.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtingAnt
        '
        Me.txtingAnt.BackColor = System.Drawing.SystemColors.Info
        Me.txtingAnt.Location = New System.Drawing.Point(124, 467)
        Me.txtingAnt.Name = "txtingAnt"
        Me.txtingAnt.Size = New System.Drawing.Size(100, 22)
        Me.txtingAnt.TabIndex = 82
        Me.txtingAnt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtcontnew
        '
        Me.txtcontnew.BackColor = System.Drawing.SystemColors.Info
        Me.txtcontnew.Location = New System.Drawing.Point(241, 497)
        Me.txtcontnew.Name = "txtcontnew"
        Me.txtcontnew.Size = New System.Drawing.Size(100, 22)
        Me.txtcontnew.TabIndex = 81
        Me.txtcontnew.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtingdif
        '
        Me.txtingdif.BackColor = System.Drawing.SystemColors.Info
        Me.txtingdif.Location = New System.Drawing.Point(123, 546)
        Me.txtingdif.Name = "txtingdif"
        Me.txtingdif.Size = New System.Drawing.Size(100, 22)
        Me.txtingdif.TabIndex = 80
        Me.txtingdif.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtingnew
        '
        Me.txtingnew.BackColor = System.Drawing.SystemColors.Info
        Me.txtingnew.Location = New System.Drawing.Point(124, 497)
        Me.txtingnew.Name = "txtingnew"
        Me.txtingnew.Size = New System.Drawing.Size(100, 22)
        Me.txtingnew.TabIndex = 79
        Me.txtingnew.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Location = New System.Drawing.Point(16, 549)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(55, 14)
        Me.Label59.TabIndex = 68
        Me.Label59.Text = "Variación"
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Location = New System.Drawing.Point(16, 500)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(74, 14)
        Me.Label58.TabIndex = 72
        Me.Label58.Text = "EERR Nuevo"
        '
        'Label66
        '
        Me.Label66.AutoSize = True
        Me.Label66.Location = New System.Drawing.Point(951, 450)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(54, 14)
        Me.Label66.TabIndex = 69
        Me.Label66.Text = "Mg. Final"
        '
        'Label67
        '
        Me.Label67.AutoSize = True
        Me.Label67.Location = New System.Drawing.Point(830, 450)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(77, 14)
        Me.Label67.TabIndex = 70
        Me.Label67.Text = "Suma Gastos"
        '
        'Label65
        '
        Me.Label65.AutoSize = True
        Me.Label65.Location = New System.Drawing.Point(254, 450)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(75, 14)
        Me.Label65.TabIndex = 71
        Me.Label65.Text = "Contribución"
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.Location = New System.Drawing.Point(727, 450)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(46, 14)
        Me.Label64.TabIndex = 76
        Me.Label64.Text = "Rebate"
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.Location = New System.Drawing.Point(586, 450)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(102, 14)
        Me.Label63.TabIndex = 73
        Me.Label63.Text = "Gastos Operación"
        '
        'Label62
        '
        Me.Label62.AutoSize = True
        Me.Label62.Location = New System.Drawing.Point(500, 450)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(40, 14)
        Me.Label62.TabIndex = 74
        Me.Label62.Text = "Venta"
        '
        'Label61
        '
        Me.Label61.AutoSize = True
        Me.Label61.Location = New System.Drawing.Point(372, 450)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(67, 14)
        Me.Label61.TabIndex = 75
        Me.Label61.Text = "Transporte"
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.Location = New System.Drawing.Point(146, 450)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(53, 14)
        Me.Label60.TabIndex = 77
        Me.Label60.Text = "Ingresos"
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Location = New System.Drawing.Point(16, 470)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(83, 14)
        Me.Label57.TabIndex = 67
        Me.Label57.Text = "EERR Anterior"
        '
        'btn_Base
        '
        Me.btn_Base.Image = Global.Convenios_New.My.Resources.Resources.Excel
        Me.btn_Base.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_Base.Location = New System.Drawing.Point(1046, 438)
        Me.btn_Base.Name = "btn_Base"
        Me.btn_Base.Size = New System.Drawing.Size(182, 46)
        Me.btn_Base.TabIndex = 102
        Me.btn_Base.Text = "Genera Archivo Base"
        Me.btn_Base.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_Base.UseVisualStyleBackColor = True
        '
        'btn_Cliente
        '
        Me.btn_Cliente.Image = Global.Convenios_New.My.Resources.Resources.Excel
        Me.btn_Cliente.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_Cliente.Location = New System.Drawing.Point(1046, 536)
        Me.btn_Cliente.Name = "btn_Cliente"
        Me.btn_Cliente.Size = New System.Drawing.Size(182, 46)
        Me.btn_Cliente.TabIndex = 103
        Me.btn_Cliente.Text = "Genera Archivo Cliente"
        Me.btn_Cliente.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_Cliente.UseVisualStyleBackColor = True
        '
        'btn_Ejecutivo
        '
        Me.btn_Ejecutivo.Image = Global.Convenios_New.My.Resources.Resources.Excel
        Me.btn_Ejecutivo.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_Ejecutivo.Location = New System.Drawing.Point(1046, 488)
        Me.btn_Ejecutivo.Name = "btn_Ejecutivo"
        Me.btn_Ejecutivo.Size = New System.Drawing.Size(182, 46)
        Me.btn_Ejecutivo.TabIndex = 104
        Me.btn_Ejecutivo.Text = "Genera Archivo Ejecutivo"
        Me.btn_Ejecutivo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_Ejecutivo.UseVisualStyleBackColor = True
        '
        'frmReajustes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1236, 587)
        Me.ControlBox = False
        Me.Controls.Add(Me.btn_Base)
        Me.Controls.Add(Me.btn_Cliente)
        Me.Controls.Add(Me.btn_Ejecutivo)
        Me.Controls.Add(Me.txtmgant)
        Me.Controls.Add(Me.txtmgdif)
        Me.Controls.Add(Me.txtsumaant)
        Me.Controls.Add(Me.txtsumDif)
        Me.Controls.Add(Me.txtrebant)
        Me.Controls.Add(Me.txtrebdif)
        Me.Controls.Add(Me.txtgasant)
        Me.Controls.Add(Me.txtgasdif)
        Me.Controls.Add(Me.txtvenant)
        Me.Controls.Add(Me.txtventdif)
        Me.Controls.Add(Me.txtmgnew)
        Me.Controls.Add(Me.txtsumanew)
        Me.Controls.Add(Me.txttranant)
        Me.Controls.Add(Me.txtrebnew)
        Me.Controls.Add(Me.txttrandif)
        Me.Controls.Add(Me.txtgasnew)
        Me.Controls.Add(Me.txtcontant)
        Me.Controls.Add(Me.txtvennew)
        Me.Controls.Add(Me.txtcontdif)
        Me.Controls.Add(Me.txttrannew)
        Me.Controls.Add(Me.txtingAnt)
        Me.Controls.Add(Me.txtcontnew)
        Me.Controls.Add(Me.txtingdif)
        Me.Controls.Add(Me.txtingnew)
        Me.Controls.Add(Me.Label59)
        Me.Controls.Add(Me.Label58)
        Me.Controls.Add(Me.Label66)
        Me.Controls.Add(Me.Label67)
        Me.Controls.Add(Me.Label65)
        Me.Controls.Add(Me.Label64)
        Me.Controls.Add(Me.Label63)
        Me.Controls.Add(Me.Label62)
        Me.Controls.Add(Me.Label61)
        Me.Controls.Add(Me.Label60)
        Me.Controls.Add(Me.Label57)
        Me.Controls.Add(Me.btn_ExcelDetalle)
        Me.Controls.Add(Me.btn_ExcelHistria)
        Me.Controls.Add(Me.dgvDetalleReajustes)
        Me.Controls.Add(Me.dgvReajustes)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmReajustes"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.Text = "Historia de Reajustes del Cliente"
        CType(Me.dgvDetalleReajustes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvReajustes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents dgvDetalleReajustes As DataGridView
    Friend WithEvents dgvReajustes As DataGridView
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents txtRazons As TextBox
    Friend WithEvents txtNumRel As TextBox
    Friend WithEvents txtRutcli As TextBox
    Friend WithEvents btn_Salir As Button
    Friend WithEvents btn_ExcelDetalle As Button
    Friend WithEvents btn_ExcelHistria As Button
    Friend WithEvents txtmgant As TextBox
    Friend WithEvents txtmgdif As TextBox
    Friend WithEvents txtsumaant As TextBox
    Friend WithEvents txtsumDif As TextBox
    Friend WithEvents txtrebant As TextBox
    Friend WithEvents txtrebdif As TextBox
    Friend WithEvents txtgasant As TextBox
    Friend WithEvents txtgasdif As TextBox
    Friend WithEvents txtvenant As TextBox
    Friend WithEvents txtventdif As TextBox
    Friend WithEvents txtmgnew As TextBox
    Friend WithEvents txtsumanew As TextBox
    Friend WithEvents txttranant As TextBox
    Friend WithEvents txtrebnew As TextBox
    Friend WithEvents txttrandif As TextBox
    Friend WithEvents txtgasnew As TextBox
    Friend WithEvents txtcontant As TextBox
    Friend WithEvents txtvennew As TextBox
    Friend WithEvents txtcontdif As TextBox
    Friend WithEvents txttrannew As TextBox
    Friend WithEvents txtingAnt As TextBox
    Friend WithEvents txtcontnew As TextBox
    Friend WithEvents txtingdif As TextBox
    Friend WithEvents txtingnew As TextBox
    Friend WithEvents Label59 As Label
    Friend WithEvents Label58 As Label
    Friend WithEvents Label66 As Label
    Friend WithEvents Label67 As Label
    Friend WithEvents Label65 As Label
    Friend WithEvents Label64 As Label
    Friend WithEvents Label63 As Label
    Friend WithEvents Label62 As Label
    Friend WithEvents Label61 As Label
    Friend WithEvents Label60 As Label
    Friend WithEvents Label57 As Label
    Friend WithEvents btn_Base As Button
    Friend WithEvents btn_Cliente As Button
    Friend WithEvents btn_Ejecutivo As Button
End Class
