﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmHistoriaCliente
    Inherits MetroFramework.Forms.MetroForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dgvDetalle = New System.Windows.Forms.DataGridView()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtRazons = New System.Windows.Forms.TextBox()
        Me.txtNumRel = New System.Windows.Forms.TextBox()
        Me.txtRutcli = New System.Windows.Forms.TextBox()
        Me.btn_Salir = New System.Windows.Forms.Button()
        Me.dgvDetalleCotizacion = New System.Windows.Forms.DataGridView()
        Me.btn_ExcelHistria = New System.Windows.Forms.Button()
        Me.btn_ExcelDetalle = New System.Windows.Forms.Button()
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvDetalleCotizacion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvDetalle
        '
        Me.dgvDetalle.AllowUserToAddRows = False
        Me.dgvDetalle.AllowUserToDeleteRows = False
        Me.dgvDetalle.AllowUserToResizeRows = False
        Me.dgvDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetalle.Location = New System.Drawing.Point(10, 107)
        Me.dgvDetalle.Name = "dgvDetalle"
        Me.dgvDetalle.ReadOnly = True
        Me.dgvDetalle.RowHeadersVisible = False
        Me.dgvDetalle.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvDetalle.Size = New System.Drawing.Size(1112, 247)
        Me.dgvDetalle.TabIndex = 45
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.txtRazons)
        Me.Panel1.Controls.Add(Me.txtNumRel)
        Me.Panel1.Controls.Add(Me.txtRutcli)
        Me.Panel1.Controls.Add(Me.btn_Salir)
        Me.Panel1.Location = New System.Drawing.Point(10, 53)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1112, 48)
        Me.Panel1.TabIndex = 44
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(186, 15)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(73, 14)
        Me.Label2.TabIndex = 41
        Me.Label2.Text = "Razón Social"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(871, 15)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(81, 14)
        Me.Label3.TabIndex = 40
        Me.Label3.Text = "Num. relación"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(67, 14)
        Me.Label1.TabIndex = 40
        Me.Label1.Text = "Rut Cliente"
        '
        'txtRazons
        '
        Me.txtRazons.BackColor = System.Drawing.SystemColors.Info
        Me.txtRazons.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRazons.Location = New System.Drawing.Point(262, 12)
        Me.txtRazons.Name = "txtRazons"
        Me.txtRazons.Size = New System.Drawing.Size(603, 22)
        Me.txtRazons.TabIndex = 39
        '
        'txtNumRel
        '
        Me.txtNumRel.BackColor = System.Drawing.SystemColors.Info
        Me.txtNumRel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumRel.Location = New System.Drawing.Point(958, 12)
        Me.txtNumRel.Name = "txtNumRel"
        Me.txtNumRel.Size = New System.Drawing.Size(94, 22)
        Me.txtNumRel.TabIndex = 38
        '
        'txtRutcli
        '
        Me.txtRutcli.BackColor = System.Drawing.SystemColors.Info
        Me.txtRutcli.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRutcli.Location = New System.Drawing.Point(76, 12)
        Me.txtRutcli.Name = "txtRutcli"
        Me.txtRutcli.Size = New System.Drawing.Size(94, 22)
        Me.txtRutcli.TabIndex = 38
        '
        'btn_Salir
        '
        Me.btn_Salir.Image = Global.Convenios_New.My.Resources.Resources.salir
        Me.btn_Salir.Location = New System.Drawing.Point(1058, 5)
        Me.btn_Salir.Name = "btn_Salir"
        Me.btn_Salir.Size = New System.Drawing.Size(49, 38)
        Me.btn_Salir.TabIndex = 1
        Me.btn_Salir.UseVisualStyleBackColor = True
        '
        'dgvDetalleCotizacion
        '
        Me.dgvDetalleCotizacion.AllowUserToAddRows = False
        Me.dgvDetalleCotizacion.AllowUserToDeleteRows = False
        Me.dgvDetalleCotizacion.AllowUserToResizeRows = False
        Me.dgvDetalleCotizacion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetalleCotizacion.Location = New System.Drawing.Point(10, 364)
        Me.dgvDetalleCotizacion.Name = "dgvDetalleCotizacion"
        Me.dgvDetalleCotizacion.ReadOnly = True
        Me.dgvDetalleCotizacion.RowHeadersVisible = False
        Me.dgvDetalleCotizacion.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvDetalleCotizacion.Size = New System.Drawing.Size(1112, 247)
        Me.dgvDetalleCotizacion.TabIndex = 45
        '
        'btn_ExcelHistria
        '
        Me.btn_ExcelHistria.Image = Global.Convenios_New.My.Resources.Resources.Excel
        Me.btn_ExcelHistria.Location = New System.Drawing.Point(1128, 107)
        Me.btn_ExcelHistria.Name = "btn_ExcelHistria"
        Me.btn_ExcelHistria.Size = New System.Drawing.Size(49, 47)
        Me.btn_ExcelHistria.TabIndex = 1
        Me.btn_ExcelHistria.UseVisualStyleBackColor = True
        '
        'btn_ExcelDetalle
        '
        Me.btn_ExcelDetalle.Image = Global.Convenios_New.My.Resources.Resources.Excel
        Me.btn_ExcelDetalle.Location = New System.Drawing.Point(1130, 364)
        Me.btn_ExcelDetalle.Name = "btn_ExcelDetalle"
        Me.btn_ExcelDetalle.Size = New System.Drawing.Size(49, 42)
        Me.btn_ExcelDetalle.TabIndex = 1
        Me.btn_ExcelDetalle.UseVisualStyleBackColor = True
        '
        'frmHistoriaCliente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1187, 620)
        Me.ControlBox = False
        Me.Controls.Add(Me.dgvDetalleCotizacion)
        Me.Controls.Add(Me.dgvDetalle)
        Me.Controls.Add(Me.btn_ExcelDetalle)
        Me.Controls.Add(Me.btn_ExcelHistria)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmHistoriaCliente"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.Text = "Historial de Gestion del cliente"
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.dgvDetalleCotizacion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents dgvDetalle As DataGridView
    Friend WithEvents Panel1 As Panel
    Friend WithEvents btn_Salir As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents txtRazons As TextBox
    Friend WithEvents txtRutcli As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txtNumRel As TextBox
    Friend WithEvents dgvDetalleCotizacion As DataGridView
    Friend WithEvents btn_ExcelHistria As Button
    Friend WithEvents btn_ExcelDetalle As Button
End Class
