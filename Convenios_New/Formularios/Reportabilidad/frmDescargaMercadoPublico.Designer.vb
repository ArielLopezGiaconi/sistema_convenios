﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDescargaMercadoPublico
    Inherits MetroFramework.Forms.MetroForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cbxLinea1 = New MetroFramework.Controls.MetroCheckBox()
        Me.cbxLinea2 = New MetroFramework.Controls.MetroCheckBox()
        Me.cbxLinea4 = New MetroFramework.Controls.MetroCheckBox()
        Me.cbxLinea3 = New MetroFramework.Controls.MetroCheckBox()
        Me.cbxLinea6 = New MetroFramework.Controls.MetroCheckBox()
        Me.cbxLinea5 = New MetroFramework.Controls.MetroCheckBox()
        Me.cbxLinea8 = New MetroFramework.Controls.MetroCheckBox()
        Me.cbxLinea7 = New MetroFramework.Controls.MetroCheckBox()
        Me.cbxLinea10 = New MetroFramework.Controls.MetroCheckBox()
        Me.cbxLinea9 = New MetroFramework.Controls.MetroCheckBox()
        Me.cbxLinea12 = New MetroFramework.Controls.MetroCheckBox()
        Me.cbxLinea11 = New MetroFramework.Controls.MetroCheckBox()
        Me.btn_Grabar = New System.Windows.Forms.Button()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.SuspendLayout()
        '
        'cbxLinea1
        '
        Me.cbxLinea1.AutoSize = True
        Me.cbxLinea1.Location = New System.Drawing.Point(38, 80)
        Me.cbxLinea1.Name = "cbxLinea1"
        Me.cbxLinea1.Size = New System.Drawing.Size(199, 15)
        Me.cbxLinea1.TabIndex = 0
        Me.cbxLinea1.Text = "Menaje, Aseo y Cuidado Personal"
        Me.cbxLinea1.UseSelectable = True
        '
        'cbxLinea2
        '
        Me.cbxLinea2.AutoSize = True
        Me.cbxLinea2.Location = New System.Drawing.Point(38, 101)
        Me.cbxLinea2.Name = "cbxLinea2"
        Me.cbxLinea2.Size = New System.Drawing.Size(389, 15)
        Me.cbxLinea2.TabIndex = 1
        Me.cbxLinea2.Text = "Articulos de Ferreteria, Materiales para la Construccion y Electrohogar"
        Me.cbxLinea2.UseSelectable = True
        '
        'cbxLinea4
        '
        Me.cbxLinea4.AutoSize = True
        Me.cbxLinea4.Location = New System.Drawing.Point(38, 143)
        Me.cbxLinea4.Name = "cbxLinea4"
        Me.cbxLinea4.Size = New System.Drawing.Size(466, 15)
        Me.cbxLinea4.TabIndex = 3
        Me.cbxLinea4.Text = "Convenio Marco de HardWare Licencias de Software y Recursos educativos Digitales"
        Me.cbxLinea4.UseSelectable = True
        '
        'cbxLinea3
        '
        Me.cbxLinea3.AutoSize = True
        Me.cbxLinea3.Location = New System.Drawing.Point(38, 122)
        Me.cbxLinea3.Name = "cbxLinea3"
        Me.cbxLinea3.Size = New System.Drawing.Size(365, 15)
        Me.cbxLinea3.TabIndex = 2
        Me.cbxLinea3.Text = "Convenio Marco de Venta, Arriendo y Suministros de Impresoras."
        Me.cbxLinea3.UseSelectable = True
        '
        'cbxLinea6
        '
        Me.cbxLinea6.AutoSize = True
        Me.cbxLinea6.Location = New System.Drawing.Point(38, 185)
        Me.cbxLinea6.Name = "cbxLinea6"
        Me.cbxLinea6.Size = New System.Drawing.Size(220, 15)
        Me.cbxLinea6.TabIndex = 5
        Me.cbxLinea6.Text = "CM Articulos de Escritorio y Papeleria"
        Me.cbxLinea6.UseSelectable = True
        '
        'cbxLinea5
        '
        Me.cbxLinea5.AutoSize = True
        Me.cbxLinea5.Location = New System.Drawing.Point(38, 164)
        Me.cbxLinea5.Name = "cbxLinea5"
        Me.cbxLinea5.Size = New System.Drawing.Size(297, 15)
        Me.cbxLinea5.TabIndex = 4
        Me.cbxLinea5.Text = "CM Mobiliario gral, Oficina, Escolar, Clinico, Urbano"
        Me.cbxLinea5.UseSelectable = True
        '
        'cbxLinea8
        '
        Me.cbxLinea8.AutoSize = True
        Me.cbxLinea8.Location = New System.Drawing.Point(38, 227)
        Me.cbxLinea8.Name = "cbxLinea8"
        Me.cbxLinea8.Size = New System.Drawing.Size(285, 15)
        Me.cbxLinea8.TabIndex = 7
        Me.cbxLinea8.Text = "Ortesis, Protesis, Endoprotesis e Insumos de Salud"
        Me.cbxLinea8.UseSelectable = True
        '
        'cbxLinea7
        '
        Me.cbxLinea7.AutoSize = True
        Me.cbxLinea7.Location = New System.Drawing.Point(38, 206)
        Me.cbxLinea7.Name = "cbxLinea7"
        Me.cbxLinea7.Size = New System.Drawing.Size(452, 15)
        Me.cbxLinea7.TabIndex = 6
        Me.cbxLinea7.Text = "Neumaticos, Lubricantes, Accesorios para Vehiculos y Servicios Complementarios"
        Me.cbxLinea7.UseSelectable = True
        '
        'cbxLinea10
        '
        Me.cbxLinea10.AutoSize = True
        Me.cbxLinea10.BackColor = System.Drawing.Color.Gray
        Me.cbxLinea10.Enabled = False
        Me.cbxLinea10.Location = New System.Drawing.Point(38, 269)
        Me.cbxLinea10.Name = "cbxLinea10"
        Me.cbxLinea10.Size = New System.Drawing.Size(422, 15)
        Me.cbxLinea10.TabIndex = 9
        Me.cbxLinea10.Text = "Productos y Servicios Para Emergencia - 2016 (Emergencias Hidrologicas)  X"
        Me.cbxLinea10.UseSelectable = True
        '
        'cbxLinea9
        '
        Me.cbxLinea9.AutoSize = True
        Me.cbxLinea9.Location = New System.Drawing.Point(38, 248)
        Me.cbxLinea9.Name = "cbxLinea9"
        Me.cbxLinea9.Size = New System.Drawing.Size(423, 15)
        Me.cbxLinea9.TabIndex = 8
        Me.cbxLinea9.Text = "Productos y Servicios para Emergencias - 2016 (Emergencia de Primer Nivel)"
        Me.cbxLinea9.UseSelectable = True
        '
        'cbxLinea12
        '
        Me.cbxLinea12.AutoSize = True
        Me.cbxLinea12.Location = New System.Drawing.Point(38, 311)
        Me.cbxLinea12.Name = "cbxLinea12"
        Me.cbxLinea12.Size = New System.Drawing.Size(77, 15)
        Me.cbxLinea12.TabIndex = 11
        Me.cbxLinea12.Text = "Alimentos"
        Me.cbxLinea12.UseSelectable = True
        '
        'cbxLinea11
        '
        Me.cbxLinea11.AutoSize = True
        Me.cbxLinea11.Location = New System.Drawing.Point(38, 290)
        Me.cbxLinea11.Name = "cbxLinea11"
        Me.cbxLinea11.Size = New System.Drawing.Size(349, 15)
        Me.cbxLinea11.TabIndex = 10
        Me.cbxLinea11.Text = "Productos y Servicios para Emergencia - 2016 (Ropa de Cama)"
        Me.cbxLinea11.UseSelectable = True
        '
        'btn_Grabar
        '
        Me.btn_Grabar.Image = Global.Convenios_New.My.Resources.Resources.Floppy
        Me.btn_Grabar.Location = New System.Drawing.Point(637, 280)
        Me.btn_Grabar.Name = "btn_Grabar"
        Me.btn_Grabar.Size = New System.Drawing.Size(44, 46)
        Me.btn_Grabar.TabIndex = 12
        Me.btn_Grabar.Text = "" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btn_Grabar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_Grabar.UseVisualStyleBackColor = True
        '
        'frmDescargaMercadoPublico
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(711, 353)
        Me.Controls.Add(Me.btn_Grabar)
        Me.Controls.Add(Me.cbxLinea12)
        Me.Controls.Add(Me.cbxLinea11)
        Me.Controls.Add(Me.cbxLinea10)
        Me.Controls.Add(Me.cbxLinea9)
        Me.Controls.Add(Me.cbxLinea8)
        Me.Controls.Add(Me.cbxLinea7)
        Me.Controls.Add(Me.cbxLinea6)
        Me.Controls.Add(Me.cbxLinea5)
        Me.Controls.Add(Me.cbxLinea4)
        Me.Controls.Add(Me.cbxLinea3)
        Me.Controls.Add(Me.cbxLinea2)
        Me.Controls.Add(Me.cbxLinea1)
        Me.Name = "frmDescargaMercadoPublico"
        Me.Text = "Descarga de productos Mercado Publico"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents cbxLinea1 As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents cbxLinea2 As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents cbxLinea4 As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents cbxLinea3 As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents cbxLinea6 As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents cbxLinea5 As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents cbxLinea8 As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents cbxLinea7 As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents cbxLinea10 As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents cbxLinea9 As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents cbxLinea12 As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents cbxLinea11 As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents btn_Grabar As Button
    Friend WithEvents SaveFileDialog1 As SaveFileDialog
End Class
