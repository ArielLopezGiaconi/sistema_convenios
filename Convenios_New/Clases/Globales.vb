﻿Imports System.Data.OleDb
Imports System.IO

Public Class Globales
    Public Shared strMonto As String
    Public Shared I, JJ1 As Integer
    Public Shared N1, N2 As Integer
    Public Shared N3, X As Integer
    Public Shared X3 As Integer
    Public Shared PP, XX As String
    Public Shared JJ As String
    Public Shared rutEmpresa As String
    Public Shared rutEmpresaConDigito As String
    Public Shared rutUsuario As String
    Public Shared actualizaFicha As Boolean
    Public Shared basePrueba As Boolean = False
    Public Shared slogan As String
    Public Shared empresa As Integer
    Public Shared emisor As String
    Public Shared nom_empresa As String
    Public Shared revision As String = "1.7"
    Public Shared mes As String
    Public Shared año As String
    Public Shared user As String
    Public Shared iva As Integer = 19
    Public Shared vendedorGlobal As String
    Public Shared modulo As String = "5" 'El modulo es el identificador del sistema en la tabla ma_revision_sistemas 
    Public Shared EmpresaValoresNetos As String
    Public Shared Excel03ConString As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties='Excel 8.0;HDR={1};IMEX=1'"
    Public Shared Excel07ConString As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 12.0;HDR={1};IMEX=1'"
    Public Shared aux As Int32 = 0
    Public Shared tema As Int32 = 1

    Public Shared Function Login(ByVal user As String, ByVal password As String) As Boolean
        Conexion.username = user
        Conexion.password = password
        Dim bd As New Conexion
        Try
            If bd.open_dimerc = 1 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString())
            Return False
        Finally
            bd.close()
        End Try
    End Function

    Public Shared Function Fin_del_Mes(fecha As Date) As Date
        Try
            If IsDate(fecha) Then
                Fin_del_Mes = DateAdd("m", 1, fecha)
                Fin_del_Mes = DateSerial(Year(Fin_del_Mes), Month(Fin_del_Mes), 1)
                Fin_del_Mes = DateAdd("d", -1, Fin_del_Mes)
            Else
                Return ""
            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return ""
        End Try
    End Function
    Public Shared Sub soloNumeros(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)

        If Not IsNumeric(e.KeyChar) And Not e.KeyChar = Convert.ToChar(Keys.Back) Then
            e.Handled = True
        End If

    End Sub

    Public Shared Function soloNumeros(ByVal cadena As String) As Boolean
        Dim caracteresValidos = 0
        Dim caracteres = cadena.ToCharArray()
        For Each caracter As Char In caracteres
            If Char.IsNumber(caracter) Then
                caracteresValidos = caracteresValidos + 1
            End If
        Next
        If caracteresValidos = cadena.Length Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Shared Function soloLetras(ByVal cadena As String) As Boolean
        Dim caracteresValidos = 0
        Dim caracteres = cadena.ToCharArray()
        For Each caracter As Char In caracteres
            If Char.IsLetter(caracter) Then
                caracteresValidos = caracteresValidos + 1
            End If
        Next
        If caracteresValidos = cadena.Length Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Shared Sub soloLetras(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If IsNumeric(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Public Shared Sub solonumerosyEnter(ByVal e As KeyPressEventArgs)
        Dim num As Char
        num = e.KeyChar
        If Char.IsDigit(num) Or num = Convert.ToChar(Keys.Enter) Then
            e.Handled = False
        ElseIf Char.IsControl(num) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    Public Shared Function CalculaDv(Strrut As String) As String
        Dim intSuma As Integer
        Dim strPaso As String

        strPaso = (Space$(9 - Len(Strrut))) & Strrut
        '   intSuma = Val(Left$(strPaso, 1)) * 3
        intSuma = Val(Mid$(strPaso, Len(strPaso) - 7, 1)) * 3
        intSuma = intSuma + Val(Mid$(strPaso, Len(strPaso) - 6, 1)) * 2
        intSuma = intSuma + Val(Mid$(strPaso, Len(strPaso) - 5, 1)) * 7
        intSuma = intSuma + Val(Mid$(strPaso, Len(strPaso) - 4, 1)) * 6
        intSuma = intSuma + Val(Mid$(strPaso, Len(strPaso) - 3, 1)) * 5
        intSuma = intSuma + Val(Mid$(strPaso, Len(strPaso) - 2, 1)) * 4
        intSuma = intSuma + Val(Mid$(strPaso, Len(strPaso) - 1, 1)) * 3
        intSuma = intSuma + Val(Mid$(strPaso, Len(strPaso), 1)) * 2
        intSuma = 11 - (intSuma Mod 11)

        If intSuma = 10 Then
            CalculaDv = "K"
        Else
            If intSuma > 10 Then
                CalculaDv = "0"
            Else
                CalculaDv = CStr(intSuma)
            End If
        End If

    End Function

    Public Shared Function EsNulo(Valor As Object) As String
        ' Esta función retorna un blanco si el objeto es nulo, de lo contrario
        ' regresa el valor del objeto
        If Valor Is Nothing Or String.IsNullOrEmpty(Valor.ToString) Then
            EsNulo = ""
        Else
            EsNulo = Valor
        End If
    End Function

    Public Shared Function EsNuloConCero(Valor As Object) As String
        ' Esta función retorna un 0 si el objeto es nulo, de lo contrario
        ' regresa el valor del objeto
        If Valor Is Nothing Or String.IsNullOrEmpty(Valor.ToString) Then
            EsNuloConCero = "0"
        Else
            EsNuloConCero = Valor
        End If
    End Function

    '    Protected void ImportExcel(HttpPostedFileBase httpPostedfile)
    '{

    'String basePath = String.Format("{0}/{1}", AppDomain.CurrentDomain.BaseDirectory, "FileRepository/ImportExport/Excel");
    'String path = String.Format("{0}/{1}", basePath, httpPostedfile.FileName);
    'If (!Directory.Exists(basePath))
    '{
    'Directory.CreateDirectory(basePath);
    '}
    'httpPostedfile.SaveAs(path);
    'Using (SpreadsheetDocument doc = SpreadsheetDocument.Open(path, false))
    '{
    '//Read the first Sheet from Excel file.
    'Sheet sheet = doc.WorkbookPart.Workbook.Sheets.GetFirstChild&amp;amp;lt;Sheet&amp;amp;gt;();
    '//Get the Worksheet instance.
    'Worksheet worksheet = (doc.WorkbookPart.GetPartById(sheet.Id.Value) as WorksheetPart).Worksheet;
    '//Fetch all the rows present in the Worksheet.
    'IEnumerable<Row> rows = worksheet.GetFirstChild<SheetData>().Descendants<Row>();
    'DataTable dt = New DataTable();
    '//Loop through the Worksheet rows.
    'foreach (Row row in rows)
    '{
    '//Use the first row to add columns to DataTable.
    'If (row.RowIndex.Value == 1)
    '{
    'foreach (Cell cell in row.Descendants<Cell>())
    '{
    'dt.Columns.Add(GetValue(doc, cell));
    '}
    '}
    'Else
    '{
    '//Add rows to DataTable.
    'dt.Rows.Add();
    'int i = 0;
    'foreach (Cell cell in row.Descendants<Cell>())
    '{
    'dt.Rows[dt.Rows.Count - 1][i] = GetValue(doc, cell);
    'i++;
    '}
    '}
    '}
    '}
    '}

    'Protected Sub ImportExcel(ByVal httpPostedfile As HttpPostedFileBase)
    '    Dim basePath As String = String.Format("{0}/{1}", AppDomain.CurrentDomain.BaseDirectory, "FileRepository/ImportExport/Excel")
    '    Dim path As String = String.Format("{0}/{1}", basePath, httpPostedfile.FileName)

    '    If Not Directory.Exists(basePath) Then
    '        Directory.CreateDirectory(basePath)
    '    End If

    '    httpPostedfile.SaveAs(path)

    '    Using doc As SpreadsheetDocument = SpreadsheetDocument.Open(path, False)
    '        Dim sheet As Sheet = doc.WorkbookPart.Workbook.Sheets.GetFirstChild And amp
    '        amp
    '        lt
    '        sheet And amp
    '    amp
    '        gt
    '    (_)
    '    Dim worksheet As Worksheet = (TryCast(doc.WorkbookPart.GetPartById(sheet.Id.Value), WorksheetPart)).Worksheet
    '        Dim rows As IEnumerable(Of Row) = worksheet.GetFirstChild(Of SheetData)().Descendants(Of Row)()
    '        Dim dt As DataTable = New DataTable()

    '        For Each row As Row In rows

    '            If row.RowIndex.Value = 1 Then

    '                For Each cell As Cell In row.Descendants(Of Cell)()
    '                    dt.Columns.Add(GetValue(doc, cell))
    '                Next
    '            Else
    '                dt.Rows.Add()
    '                Dim i As Integer = 0

    '                For Each cell As Cell In row.Descendants(Of Cell)()
    '                    dt.Rows(dt.Rows.Count - 1)(i) = GetValue(doc, cell)
    '                    i += 1
    '                Next
    '            End If
    '        Next
    '    End Using
    'End Sub

    Public Shared Sub ImportarHojaEspecifica(ByVal grilla As DataGridView, ruta As String)
        Try
            Cursor.Current = Cursors.WaitCursor
            Dim dt2 As DataTable
            Dim extension As String = Path.GetExtension(ruta)
            Dim header As String = "YES"
            Dim conStr As String, sheetName As String
            conStr = String.Empty
            Select Case extension

                Case ".xls"
                    'Excel 97-03
                    conStr = String.Format(Excel03ConString, ruta, header)
                    Exit Select

                Case ".xlsx"
                    'Excel 07 en adelante
                    conStr = String.Format(Excel07ConString, ruta, header)
                    Exit Select
            End Select

            'Get the name of the First Sheet.
            Using con As New OleDbConnection(conStr)
                Using cmd As New OleDbCommand()
                    cmd.Connection = con
                    con.Open()
                    Dim dtExcelSchema As DataTable = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
                    If dtExcelSchema.Rows.Count > 1 Then
                        sheetName = dtExcelSchema.Rows(1)("TABLE_NAME").ToString()
                        If Not sheetName = "'Carga Input$'" Then
                            sheetName = dtExcelSchema.Rows(0)("TABLE_NAME").ToString()
                        End If
                    Else
                        sheetName = dtExcelSchema.Rows(0)("TABLE_NAME").ToString()
                    End If
                    con.Close()
                End Using
            End Using

            'Read Data from the First Sheet.
            Using con As New OleDbConnection(conStr)
                Using cmd As New OleDbCommand()
                    Using oda As New OleDbDataAdapter()
                        Dim dt As New DataTable()
                        cmd.CommandText = (Convert.ToString("SELECT * From [") & sheetName) + "]"
                        cmd.Connection = con
                        con.Open()
                        oda.SelectCommand = cmd
                        oda.Fill(dt)
                        con.Close()
                        If (dt.Rows.Count = 0) Then
                            MessageBox.Show("El archivo seleccionado no contiene datos", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                            Exit Sub
                        End If
                        'Llenar DataGridView.
                        dt2 = dt.Copy
                        grilla.DataSource = dt
                        'Label1.Text = DataGridExcel.RowCount

                        Cursor.Current = Cursors.Arrow

                    End Using
                End Using
            End Using

        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Public Shared Sub Importar(ByVal grilla As DataGridView, ruta As String)
        Try
            Cursor.Current = Cursors.WaitCursor
            Dim dt2 As DataTable
            Dim extension As String = Path.GetExtension(ruta)
            Dim header As String = "YES"
            Dim conStr As String, sheetName As String

            conStr = String.Empty
            Select Case extension

                Case ".xls"
                    'Excel 97-03
                    conStr = String.Format(Excel03ConString, ruta, header)
                    Exit Select

                Case ".xlsx"
                    'Excel 07 en adelante
                    conStr = String.Format(Excel07ConString, ruta, header)
                    Exit Select
            End Select

            'Get the name of the First Sheet.
            Using con As New OleDbConnection(conStr)
                Using cmd As New OleDbCommand()
                    cmd.Connection = con
                    con.Open()
                    Dim dtExcelSchema As DataTable = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
                    sheetName = dtExcelSchema.Rows(0)("TABLE_NAME").ToString()
                    con.Close()
                End Using
            End Using

            'Read Data from the First Sheet.
            Using con As New OleDbConnection(conStr)
                Using cmd As New OleDbCommand()
                    Using oda As New OleDbDataAdapter()
                        Dim dt As New DataTable()
                        cmd.CommandText = (Convert.ToString("SELECT * From [") & sheetName) + "]"
                        cmd.Connection = con
                        con.Open()
                        oda.SelectCommand = cmd
                        oda.Fill(dt)
                        con.Close()
                        If (dt.Rows.Count = 0) Then
                            MessageBox.Show("El archivo seleccionado no contiene datos", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                            Exit Sub
                        End If
                        'Llenar DataGridView.
                        dt2 = dt.Copy
                        grilla.DataSource = dt
                        'Label1.Text = DataGridExcel.RowCount

                        Cursor.Current = Cursors.Arrow

                    End Using
                End Using
            End Using

        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub


    Public Shared Sub exporta(ByVal path As String, ByVal grilla As DataGridView)
        Try
            Dim car As New frmCargando

            car.ProgressBar1.Minimum = 0
            car.ProgressBar1.Value = 0
            car.ProgressBar1.Maximum = grilla.RowCount
            car.StartPosition = FormStartPosition.CenterScreen
            car.Show()
            Cursor.Current = Cursors.WaitCursor
            Dim app As Microsoft.Office.Interop.Excel._Application = New Microsoft.Office.Interop.Excel.Application()
            Dim workbook As Microsoft.Office.Interop.Excel._Workbook = app.Workbooks.Add(Type.Missing)
            Dim worksheet As Microsoft.Office.Interop.Excel._Worksheet = Nothing
            worksheet = workbook.Sheets("Hoja1")
            worksheet = workbook.ActiveSheet

            'Aca se agregan las cabeceras de nuestro datagrid.
            For i As Integer = 1 To grilla.Columns.Count
                worksheet.Cells(1, i) = grilla.Columns(i - 1).HeaderText
            Next

            'Aca se ingresa el detalle recorrera la tabla celda por celda
            For I = 0 To grilla.Rows.Count - 1
                For j = 0 To grilla.Columns.Count - 1
                    aux = j
                    If grilla.Columns.Item(j).Name <> "SEL" Then
                        If grilla.Rows(I).Cells(j).Value.ToString Is Nothing Then
                            worksheet.Cells(I + 2, j + 1) = "sin dato"
                        Else
                            worksheet.Cells(I + 2, j + 1) = grilla.Rows(I).Cells(j).Value.ToString
                        End If
                    End If
                Next
                car.ProgressBar1.Value += 1
            Next

            'Aca le damos el formato a nuestro excel
            worksheet.SaveAs(path)
            worksheet.Rows.Item(1).Font.Bold = 1
            worksheet.Rows.Item(1).HorizontalAlignment = 3
            worksheet.Columns.AutoFit()
            worksheet.Columns.HorizontalAlignment = 2

            app.Visible = True
            app = Nothing
            workbook = Nothing
            worksheet = Nothing
            FileClose(1)
            GC.Collect()
            car.Close()
            Cursor.Current = Cursors.Arrow
        Catch ex As Exception
            MessageBox.Show(ex.ToString())
            Exit Sub
        End Try

    End Sub

    Public Shared Function detDerUsu(ByVal usuario As String, derecho As Integer) As Boolean
        Dim bd As New Conexion
        bd.open_dimerc()
        Dim sql As String = "select 1 from re_usuader where codusu = '" + usuario + "' and codder = " + derecho.ToString + " "
        If (bd.sqlSelect(sql).Rows.Count > 0) Then
            bd.close()
            Return True
        Else
            bd.close()
            Return False
        End If

    End Function

    Public Shared Function BuscaCodigoDimerc(VCodpro As String) As String
        Dim bd As New Conexion
        Try
            bd.open_dimerc()
            Dim SQL As String
            Dim dt, dt1, dt2 As New DataTable

            ' Buscar por codigo de barra, devuelve codigo dimerc
            SQL = "select codpro  from re_codebar where codbar = '" & VCodpro & "'"
            dt = bd.sqlSelect(SQL)
            If dt.Rows.Count = 0 Then
                ' Buscar por codigo dimerc, devuelve codigo dimerc
                SQL = "select despro from ma_product where codpro = '" & VCodpro & "'"
                dt.Clear()
                dt = bd.sqlSelect(SQL)
                If dt.Rows.Count = 0 Then
                    ' Buscar Con Codigo_Unificado, devuelve Código Dimerc
                    SQL = "select codpro from ma_product where codigo_uni = '" & CStr(VCodpro) & "'"
                    dt1.Clear()
                    dt1 = bd.sqlSelect(SQL)
                    If Not dt.Rows.Count = 0 Then
                        BuscaCodigoDimerc = CStr(dt.Rows(0).Item("codpro"))
                    Else
                        'Buscar Con Codigo de Proveedor en RE_PROVPRO, devuelve Código Dimerc
                        SQL = "select codpro from re_provpro where coprpv = '" & CStr(VCodpro) & "'"
                        dt1.Clear()
                        dt1 = bd.sqlSelect(SQL)
                        If dt.Rows.Count = 0 Then
                            ' Busca Por Cualquier otro código, rescata el unificado y con el busca el Cod. Dimerc
                            SQL = " SELECT pror_prod_id FROM cat_producto_organizacion@ora92 "
                            SQL = SQL & "  WHERE pror_codigo = '" & VCodpro & "' "
                            dt2.Clear()
                            dt2 = bd.sqlSelect(SQL)
                            If Not dt2.Rows.Count = 0 Then
                                SQL = "select codpro from ma_product where codigo_uni = '" & CStr(dt2.Rows(0).Item("pror_prod_id")) & "'"
                                dt1.Clear()
                                dt1 = bd.sqlSelect(SQL)
                                If Not dt1.Rows.Count = 0 Then
                                    BuscaCodigoDimerc = CStr(dt1.Rows(0).Item("codpro"))
                                Else
                                    Return VCodpro
                                End If
                            Else
                                Return VCodpro
                            End If
                        Else
                            BuscaCodigoDimerc = CStr(dt1.Rows(0).Item("codpro"))
                        End If
                    End If
                Else
                    BuscaCodigoDimerc = CStr(VCodpro)
                End If
            Else
                If VCodpro = "0000" Then
                    BuscaCodigoDimerc = VCodpro
                Else
                    BuscaCodigoDimerc = CStr(dt.Rows(0).Item("codpro"))
                End If

            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return ""
        Finally
            bd.close()
        End Try
    End Function

    Public Shared Function validaRevision() As Boolean
        Try
            Dim bd As New Conexion

            Dim sql As String = ""
            Dim revisionSistema As String
            bd.open_dimerc()
            sql = "select revision from "
            sql += " ma_revision_sistemas "
            sql += " where codemp = 2"
            sql += " and modulo = " + Globales.modulo
            revisionSistema = bd.sqlSelectUnico(sql, "revision")
            If Trim(revisionSistema) = "" Then
                MessageBox.Show("ATENCIÓN!!!!! El módulo no esta registrado para esta empresa." + Chr(13) + "Por favor comunicarse con Informática",
                                "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return False
            Else
                If Trim(revisionSistema) <> Trim(Globales.revision) Then
                    MessageBox.Show("Existe una nueva revisión para el sistema de Compras_Dimerc_Chile (" + revisionSistema + ")." + Chr(13) + "Por favor actualizar sistema.",
                                    "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    bd.close()
                    Return False
                End If
            End If
            bd.close()
            Return True
        Catch ex As Exception
            MessageBox.Show(ex.ToString, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        End Try

    End Function

    Public Shared Function strNumero(vcampo As String, vlargo As Double) As String
        strNumero = Space$(vlargo - Len(vcampo)) & vcampo
        'strNumero = Space(50) + vcampo
    End Function

    Public Shared Function strCaracter(vcampo As String, vlargo As Integer) As String
        If Len(vcampo.Trim) >= vlargo Then
            strCaracter = Left((vcampo.Trim), vlargo)
        Else
            strCaracter = vcampo.Trim & Space$(vlargo - Len(vcampo.Trim))
            'strCaracter =.toString.trim$(vcampo) + Space(50)
        End If
    End Function
    Public Shared Function UppercaseFirstLetter(ByVal val As String) As String
        ' Test for nothing or empty.
        If String.IsNullOrEmpty(val) Then
            Return val
        End If

        ' Convert to character array.
        Dim array() As Char = val.ToCharArray

        ' Uppercase first character.
        array(0) = Char.ToUpper(array(0))

        ' Return new string.
        Return New String(array)
    End Function
    Public Shared Function esEERR(ByVal rut As String) As Boolean
        Try
            If rut = "76307759" Or rut = "91826000" Or rut = "96670840" Or rut = "96690560" Or rut = "96803690" Or rut = "96931250" Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return Nothing
        End Try
    End Function
    Public Shared Sub DataTableToExcel(ByVal pDataTable As DataTable, grilla As DataGridView)
        Try
            Dim vFileName As String = Path.GetTempFileName()
            FileOpen(1, vFileName, OpenMode.Output)
            Dim sb As String = ""
            Dim dc As DataColumn
            For Each dc In pDataTable.Columns
                sb &= dc.Caption & Microsoft.VisualBasic.ControlChars.Tab
            Next
            PrintLine(1, sb)
            Dim i As Integer = 0
            Dim dr As DataRow
            For Each dr In pDataTable.Rows
                i = 0 : sb = ""
                For Each dc In pDataTable.Columns
                    If Not IsDBNull(dr(i)) Then
                        sb &= CStr(dr(i)) & Microsoft.VisualBasic.ControlChars.Tab
                    Else
                        sb &= Microsoft.VisualBasic.ControlChars.Tab
                    End If
                    i += 1
                Next
                PrintLine(1, sb)
            Next
            FileClose(1)
            TextToExcel(vFileName, grilla)
        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Public Shared Sub TextToExcel(ByVal pFileName As String, grilla As DataGridView)
        Try
            GC.Collect()
            'Dim vFormato As Microsoft.Office.Interop.Excel.XlRangeAutoFormat
            Dim vCultura As System.Globalization.CultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CurrentCulture
            Dim Exc As Microsoft.Office.Interop.Excel.Application = New Microsoft.Office.Interop.Excel.Application
            Exc.Workbooks.OpenText(pFileName, , , , Microsoft.Office.Interop.Excel.XlTextQualifier.xlTextQualifierNone, , True)
            Dim Wb As Microsoft.Office.Interop.Excel.Workbook = Exc.ActiveWorkbook
            Dim Ws As Microsoft.Office.Interop.Excel.Worksheet = Wb.ActiveSheet
            'En el ejemplo vienen otros formatos posibles
            'vFormato = Microsoft.Office.Interop.Excel.XlRangeAutoFormat.xlRangeAutoFormat3DEffects2
            'para la cabecera
            'Ws.Range(Ws.Cells(1, 1), Ws.Cells(Ws.UsedRange.Rows.Count, Ws.UsedRange.Columns.Count)).AutoFormat(vFormato)
            'para las celdas en general alinenando a la izquierda.
            'Ws.Range(Ws.Cells(2, 1), Ws.Cells(Ws.UsedRange.Rows.Count, Ws.UsedRange.Columns.Count)).HorizontalAlignment = 2
            pFileName = Path.GetTempFileName.Replace("tmp", "xls")
            File.Delete(pFileName)
            Exc.ActiveWorkbook.SaveAs(pFileName, Microsoft.Office.Interop.Excel.XlTextQualifier.xlTextQualifierNone - 1)
            Exc.Quit()
            Ws = Nothing
            Wb = Nothing
            Exc = Nothing
            GC.Collect()
            If grilla.RowCount > -1 Then
                Dim p As System.Diagnostics.Process = New System.Diagnostics.Process
                p.EnableRaisingEvents = False
                Process.Start("Excel.exe", pFileName)
            End If
            System.Threading.Thread.CurrentThread.CurrentCulture = vCultura
        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub
End Class
