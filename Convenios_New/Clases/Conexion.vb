﻿
Imports System.Data.OracleClient
Imports System.Data.Odbc
Imports System.Data.SqlClient

Public Class Conexion
    Public Shared username As String
    Public Shared password As String
    Public Shared conn_dimerc As String = "DATA SOURCE=(DESCRIPTION =(ADDRESS_LIST =(ADDRESS = (PROTOCOL = TCP)(HOST = 10.10.20.1)(PORT = 1521)))(CONNECT_DATA =(SERVICE_NAME = prod)));User Id=dm_ventas;Password=dimerc;"
    Public Shared dimerc_Prueba As String = "DATA SOURCE=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=10.10.180.152)(PORT=1521))(CONNECT_DATA=(SID=DESA01)));User Id=dm_ventas;Password=dimerc;"
    Public Shared conn_unificado As String = "DATA SOURCE =(DESCRIPTION =(ADDRESS_LIST =(ADDRESS = (PROTOCOL = TCP)(HOST = 10.10.40.210)(PORT = 1521)))(CONNECT_DATA =(SERVICE_NAME = ora92)));User Id=HOLDING;Password=SYSADM;"
    Public Shared sac As String = "SAC =(DESCRIPTION = (ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = 10.10.10.3)(PORT = 1521)))(CONNECT_DATA = (SERVICE_NAME = orcl)));User Id=SYSADM;Password=flfnqz16;"
    Public Shared conn_mysql As String = "DRIVER= {MySQL ODBC 3.51 Driver};SERVER=192.168.10.51;DATABASE=dashboard;UID=root;password=d1mySQL20_18;PORT=3306;"
    Public sqlConn As OracleConnection
    Public mysqlconn As OdbcConnection
    Public xComand As OracleCommand
    Public mysqlcomand As OdbcCommand

    Function mysqlSelect(ByVal conex As String) As DataTable
        Dim xtab As DataTable = New DataTable("xTabla")
        mysqlcomand = New OdbcCommand(conex, mysqlconn)
        Dim xad As OdbcDataAdapter = New OdbcDataAdapter(mysqlcomand)
        xad.Fill(xtab)
        mysqlconn.Close()
        Return xtab
    End Function
    Public Function open_mysql() As Integer
        Try
            'CAMBIAR PARA SELECCIONAR BASE DE PRUEBA O PRODUCCION
            mysqlconn = New OdbcConnection(conn_mysql)
            mysqlconn.Open()
            Return 1
            ' Insert code to process data.
        Catch oex As OdbcException
            Return 2
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
            Return 0
        End Try
    End Function

    Public Sub closemysql()
        mysqlconn.Close()
    End Sub

    Public Function open_dimerc() As Integer
        Try
            'CAMBIAR PARA SELECCIONAR BASE DE PRUEBA O PRODUCCION
            Dim conexString = ""

            If Globales.basePrueba = True Then
                conexString = dimerc_Prueba.Replace("dm_ventas", username)
            Else
                conexString = conn_dimerc.Replace("dm_ventas", username)
            End If
            conexString = conexString.Replace("dimerc", password)
            sqlConn = New OracleConnection(conexString)
            sqlConn.Open()
            xComand = New OracleCommand("Alter session set nls_date_format = 'dd/mm/yyyy'", sqlConn)
            xComand.ExecuteNonQuery()
            xComand = New OracleCommand("Alter session set nls_numeric_characters = '.,'", sqlConn)
            xComand.ExecuteNonQuery()
            xComand = New OracleCommand("Set Role Rol_Aplicacion", sqlConn)
            xComand.ExecuteNonQuery()
            xComand = New OracleCommand("COMMIT", sqlConn)
            xComand.ExecuteNonQuery()
            Return 1
            ' Insert code to process data.
        Catch oex As OracleException
            If oex.ToString().Contains("ORA-01017") Then
                Return -1
            Else
                Return 0
            End If

        Catch ex As Exception
            MessageBox.Show(ex.ToString)
            Return 0
        End Try
    End Function

    Public Function open_dimercFull() As Integer
        Try
            'CAMBIAR PARA SELECCIONAR BASE DE PRUEBA O PRODUCCION
            Dim conexString = ""

            If Globales.basePrueba = True Then
                conexString = dimerc_Prueba.Replace("dm_ventas", username)
            Else
                conexString = conn_dimerc.Replace("dm_ventas", username)
            End If
            conexString = conexString.Replace("dimerc", password)
            sqlConn = New OracleConnection(conexString)
            sqlConn.Open()
            xComand = New OracleCommand("Alter session set nls_date_format = 'dd/mm/yyyy'", sqlConn)
            xComand.ExecuteNonQuery()
            xComand = New OracleCommand("Alter session set nls_numeric_characters = '.,'", sqlConn)
            xComand.ExecuteNonQuery()
            xComand = New OracleCommand("Set Role Rol_Aplicacion", sqlConn)
            xComand.ExecuteNonQuery()
            xComand = New OracleCommand("COMMIT", sqlConn)
            xComand.ExecuteNonQuery()
            Return 1
            ' Insert code to process data.
        Catch oex As OracleException
            If oex.ToString().Contains("ORA-01017") Then
                Return -1
            Else
                Return 0
            End If

        Catch ex As Exception
            MessageBox.Show(ex.ToString)
            Return 0
        End Try
    End Function

    Public Function open_unificado() As Integer
        Try

            Dim conexString = ""

            conexString = conn_unificado
            sqlConn = New OracleConnection(conexString)
            sqlConn.Open()
            xComand = New OracleCommand("Alter session set nls_date_format = 'dd/mm/yyyy'", sqlConn)
            xComand.ExecuteNonQuery()
            xComand = New OracleCommand("Alter session set nls_numeric_characters = '.,'", sqlConn)
            xComand.ExecuteNonQuery()
            xComand = New OracleCommand("Set Role Rol_Aplicacion", sqlConn)
            xComand.ExecuteNonQuery()
            xComand = New OracleCommand("COMMIT", sqlConn)
            xComand.ExecuteNonQuery()
            Return 1
            ' Insert code to process data.
        Catch oex As OracleException
            If oex.ToString().Contains("ORA-01017") Then
                Return -1
            Else
                Return 0
            End If

        Catch ex As Exception
            MessageBox.Show(ex.ToString)
            Return 0
        End Try
    End Function
    Public Function opensac() As Integer
        Try

            Dim conexString = ""

            conexString = conn_unificado
            sqlConn = New OracleConnection(conexString)
            sqlConn.Open()
            xComand = New OracleCommand("Alter session set nls_date_format = 'dd/mm/yyyy'", sqlConn)
            xComand.ExecuteNonQuery()
            xComand = New OracleCommand("Alter session set nls_numeric_characters = '.,'", sqlConn)
            xComand.ExecuteNonQuery()
            xComand = New OracleCommand("Set Role Rol_Aplicacion", sqlConn)
            xComand.ExecuteNonQuery()
            xComand = New OracleCommand("COMMIT", sqlConn)
            xComand.ExecuteNonQuery()
            Return 1
            ' Insert code to process data.
        Catch oex As OracleException
            If oex.ToString().Contains("ORA-01017") Then
                Return -1
            Else
                Return 0
            End If

        Catch ex As Exception
            MessageBox.Show(ex.ToString)
            Return 0
        End Try
    End Function
    Public Shared Function retornaConexion()
        Try
            Dim conexString = ""

            If Globales.basePrueba = True Then
                conexString = dimerc_Prueba.Replace("dm_ventas", username)
            Else
                conexString = conn_dimerc.Replace("dm_ventas", username)
            End If
            conexString = conexString.Replace("dimerc", password)
            Return conexString
        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return ""
        End Try
    End Function
    Public Shared Function retornaConexionmysql()
        Try
            Return conn_mysql
        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return ""
        End Try
    End Function
    'Public Sub open_unificado()
    '    Try
    '        sqlConn = New OracleConnection(conn_unificado)
    '        sqlConn.Open()
    '        ' Insert code to process data.
    '    Catch ex As Exception
    '        MessageBox.Show(ex.ToString)
    '    End Try
    'End Sub
    Public Sub close()

        sqlConn.Close()

    End Sub
    Public Sub ejecutarMYSQL(sql As String)
        Try
            mysqlcomand = New OdbcCommand(sql, mysqlconn)
            mysqlcomand.ExecuteNonQuery()
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
            Exit Sub
        End Try

    End Sub

    Public Sub ejecutar(sql)
        Try
            xComand = New OracleCommand(sql, sqlConn)
            xComand.ExecuteNonQuery()
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
            Exit Sub
        End Try
    End Sub
    Function sqlSelect(ByVal conex As String) As DataTable
        Dim xtab As DataTable = New DataTable("xTabla")
        xComand = New OracleCommand(conex, sqlConn)
        Dim xad As OracleDataAdapter = New OracleDataAdapter(xComand)
        xad.Fill(xtab)
        sqlConn.Close()
        Return xtab
    End Function
    Function sqlSelectUnico(ByVal conex As String, item As String) As String

        Dim xtab As DataTable = New DataTable("xTabla")
        xComand = New OracleCommand(conex, sqlConn)
        Dim xad As OracleDataAdapter = New OracleDataAdapter(xComand)
        xad.Fill(xtab)
        If xtab.Rows.Count = 0 Then
            sqlConn.Close()
            Return ""
        Else
            sqlConn.Close()
            Return xtab.Rows(0).Item(item).ToString
        End If
    End Function

    Function sqlCheck(ByVal conex As String) As Integer
        xComand = New OracleCommand(conex, sqlConn)
        Dim xad As OracleDataAdapter = New OracleDataAdapter(xComand)
        Dim ds As DataSet = New DataSet()
        xad.Fill(ds)
        Dim cant As Integer = ds.Tables.Count
        cant = ds.Tables(0).Rows.Count
        Return cant
    End Function

    Function SelectMYSQL(ByVal conex As String) As DataTable
        Dim xtab As DataTable = New DataTable("xTabla")
        mysqlcomand = New OdbcCommand(conex, mysqlconn)
        Dim xad As OdbcDataAdapter = New OdbcDataAdapter(mysqlcomand)
        xad.Fill(xtab)
        mysqlconn.Close()
        Return xtab
    End Function

End Class
