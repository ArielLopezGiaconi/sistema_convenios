﻿Imports System.Data.OleDb
Imports System.IO
Imports ClosedXML.Excel
Imports mshtml
Imports SHDocVw
Public Class Bot1
    Public textoProceso As String = ""
    Public Excel03ConString As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties='Excel 8.0;HDR={1};IMEX=1'"
    Public Excel07ConString As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 12.0;HDR={1};IMEX=1'"
    Public maximoLinea As Int64
    Public maximoProducto As Int64
    Public valorLinea As Int64
    Public valorProducto As Int64
    Dim navigator As New InternetExplorer
    Dim naviDocment As HTMLDocument
    Dim naviElement As IHTMLElement
    Dim ListaNodos As HTMLElementCollection
    Public tablaMaestra, tablaTrabajo, tablaErrores As New DataTable
    Dim estado As String
    Public visible As Boolean

    Sub Main()

    End Sub
    Public Function inicioBot() As Boolean

        Dim rut As String = "17.837.880-5"
        Dim contraseña As String = "Dimerc6536"
        Dim tt, cambio As String
        Dim indice, rows1, rows2 As Int64
        Dim cambioStock As Boolean = False

        cambio = ""
        Try
            If tablaErrores.Columns.Count = 0 Then
                tablaErrores.Columns.Add("ID")
                tablaErrores.Columns.Add("PRODUCTO")
                tablaErrores.Columns.Add("MOTIVO")
            End If

            navigator.Navigate("https://www.mercadopublico.cl/Home/Autenticacion?esNuevaHome=true")
            navigator.Visible = True
            navigator.FullScreen = True

            System.Threading.Thread.Sleep(5000)
            'System.Threading.Thread.Sleep(5000)

            naviDocment = navigator.Document

            naviElement = AccionNavegador(1, "liRUT", 0, naviDocment)
            naviElement.click()

            textoProceso = "Conectando"

            naviElement = AccionNavegador(1, "txtRUT", 0, naviDocment)
            naviElement.value = rut

            naviElement = AccionNavegador(1, "txtPass", 0, naviDocment)
            naviElement.value = contraseña

            naviElement = AccionNavegador(1, "btnAccesoRUT", 0, naviDocment)
            naviElement.click()

            textoProceso = "accediendo al Portal..."

            System.Threading.Thread.Sleep(900)
            'System.Threading.Thread.Sleep(5000)

            naviElement = AccionNavegador(3, "a", 12, naviDocment)
            naviElement.click()

            'tablaMaestra = ImportarHojaEspecifica(tablaMaestra, "C:\MercadoPublico\LicitacionesNew.xlsx")
            'tablaTrabajo = ImportarHojaEspecifica(tablaTrabajo, "C:\MercadoPublico\Productos.xlsx")
            tablaMaestra = ImportarHojaEspecifica(tablaMaestra, "\\10.10.20.222\sistema\ArielLopez\MercadoPublico\LicitacionesNew.xlsx")
            'tablaTrabajo = ImportarHojaEspecifica(tablaTrabajo, "\\10.10.20.222\sistema\ArielLopez\MercadoPublico\Productos.xlsx")
            textoProceso = "Cargando Datos para trabajar..."
            tablaTrabajo.DefaultView.Sort = "idlinea asc"

            maximoLinea = tablaMaestra.Rows.Count
            maximoProducto = tablaTrabajo.Rows.Count
            rows1 = 0
            rows2 = 0

            'MODIFICACIÓN DE STOCK
            textoProceso = ("Actualizando Stock...")
            cambio = "stock"
repetir1:   For i = rows1 To tablaMaestra.Rows.Count - 1
                valorLinea = (i + 1)
                rows1 = i
                For a = rows2 To tablaTrabajo.Rows.Count - 1
                    navigator.Visible = visible
                    If tablaTrabajo.Rows(a).Item("stock").ToString.ToUpper <> "X" Then
                        If tablaMaestra.Rows(i).Item("idlinea") = tablaTrabajo.Rows(a).Item("idlinea") Then
                            'System.Threading.Thread.Sleep(900)
                            System.Threading.Thread.Sleep(2000)

                            valorProducto = (a + 1)
                            rows2 = a
                            navigator.Navigate(tablaMaestra.Rows(i).Item("rutaStock"))
                            textoProceso = ("(" & tablaTrabajo.Rows(a).Item("idproducto") & ") Ingresando a Convenio")

                            System.Threading.Thread.Sleep(900)

                            naviElement = AccionNavegador(1, "imgMod_DescontinuarStock", 0, naviDocment)
                            naviElement.click()

                            textoProceso = ("(" & tablaTrabajo.Rows(a).Item("idproducto") & ") Entrando a modificar stock")
                            System.Threading.Thread.Sleep(900)

                            naviElement = AccionNavegador(1, "txtIdProducto", 0, naviDocment)
                            naviElement.value = tablaTrabajo.Rows(a).Item("idproducto")

                            textoProceso = ("(" & tablaTrabajo.Rows(a).Item("idproducto") & ") Digitando Producto")
                            System.Threading.Thread.Sleep(900)

                            naviElement = AccionNavegador(1, "btnFiltrar", 0, naviDocment)
                            naviElement.click()

                            textoProceso = ("(" & tablaTrabajo.Rows(a).Item("idproducto") & ") Buscando Producto")
                            System.Threading.Thread.Sleep(2000)
                            System.Threading.Thread.Sleep(900)

                            System.Threading.Thread.Sleep(500)
                            naviElement = AccionNavegador(1, "liResultado", 0, naviDocment)
                            tt = naviElement.getAttribute("innerText").ToString
                            System.Threading.Thread.Sleep(500)
                            If tt = "No existen productos asociados a tu búsqueda:" Then
                                tablaErrores.Rows.Add(tablaMaestra.Rows(i).Item("Linea"), tablaTrabajo.Rows(a).Item("idproducto"), "PRODUCTO NO EXISTE EN CONVENIO")
                                Continue For
                            End If

                            naviElement = AccionNavegador(1, "tdStock" & tablaTrabajo.Rows(a).Item("idproducto"), 0, naviDocment)
                            tt = naviElement.getAttribute("innertext")

                            textoProceso = ("(" & tablaTrabajo.Rows(a).Item("idproducto") & ") Validando estado")
                            If tt.Trim = "SIN STOCK" And tablaTrabajo.Rows(a).Item("stock").ToString.ToUpper = "SI" Then
                                naviElement = AccionNavegador(1, "productoStock" & tablaTrabajo.Rows(a).Item("idproducto"), 0, naviDocment)
                                naviElement.click()

                                naviElement = AccionNavegador(3, "button", 6, naviDocment)
                                naviElement.click()
                                textoProceso = ("(" & tablaTrabajo.Rows(a).Item("idproducto") & ") Cambio a 'CON STOCK'")
                            ElseIf tt.Trim = "CON STOCK" And tablaTrabajo.Rows(a).Item("stock").ToString.ToUpper = "NO" Then
                                naviElement = AccionNavegador(1, "productoStock" & tablaTrabajo.Rows(a).Item("idproducto"), 0, naviDocment)
                                naviElement.click()

                                naviElement = AccionNavegador(3, "button", 6, naviDocment)
                                naviElement.click()
                                textoProceso = ("(" & tablaTrabajo.Rows(a).Item("idproducto") & ") Cambio a 'SIN STOCK'")
                            Else
                                textoProceso = ("(" & tablaTrabajo.Rows(a).Item("idproducto") & ") No se realizaron cambios.")
                            End If

                            System.Threading.Thread.Sleep(900)
                        End If
                    End If
                Next
            Next

            textoProceso = ("Fin Cambio Stock...")
            textoProceso = ("-----------------------------------------------")
            textoProceso = ("-----------------------------------------------")
            textoProceso = ("Actualizando precio")

            rows1 = 0
            rows2 = 0

            'MODIFICACION DE PRECIO
            cambio = "precio"
repetir2:   For i = rows1 To tablaMaestra.Rows.Count - 1
                valorLinea = (i + 1)
                rows1 = i
                System.Threading.Thread.Sleep(500)
                For a = rows2 To tablaTrabajo.Rows.Count - 1
                    navigator.Visible = visible
                    If tablaTrabajo.Rows(a).Item("precio").ToString.ToUpper <> "X" Then
                        If tablaMaestra.Rows(i).Item("idlinea") = tablaTrabajo.Rows(a).Item("idlinea") Then
                            System.Threading.Thread.Sleep(2000)
                            valorProducto = (a + 1)
                            rows2 = a
                            indice = 0
                            navigator.Navigate(tablaMaestra.Rows(i).Item("rutaStock"))
                            textoProceso = ("(" & tablaTrabajo.Rows(a).Item("idproducto") & ") Ingresando a Convenio")

                            System.Threading.Thread.Sleep(1500)
                            'ReadyAction(navigator, 700)
                            naviElement = AccionNavegador(3, "span", 3, naviDocment)
                            If Not naviElement.className Is Nothing Then
                                Continue For
                            End If
                            naviElement.click()

                            textoProceso = ("(" & tablaTrabajo.Rows(a).Item("idproducto") & ") Ingresando a Modificar Precio")

                            System.Threading.Thread.Sleep(2000)
                            'ReadyAction(navigator, 700)

                            naviElement = AccionNavegador(1, "txtIdProducto", 0, naviDocment)
                            naviElement.value = tablaTrabajo.Rows(a).Item("idproducto")
                            textoProceso = ("(" & tablaTrabajo.Rows(a).Item("idproducto") & ") Digitando Producto")

                            naviElement = AccionNavegador(1, "btnFiltrar", 0, naviDocment)
                            naviElement.click()
                            textoProceso = ("(" & tablaTrabajo.Rows(a).Item("idproducto") & ") Buscando Producto")

                            System.Threading.Thread.Sleep(2000)
                            'ReadyAction(navigator, 700)

                            System.Threading.Thread.Sleep(1000)
                            naviElement = AccionNavegador(1, "liResultado", 0, naviDocment)
                            tt = naviElement.getAttribute("innerText").ToString
                            System.Threading.Thread.Sleep(500)
                            If tt = "No existen productos asociados a tu búsqueda:" Then
                                tablaErrores.Rows.Add(tablaMaestra.Rows(i).Item("Linea"), tablaTrabajo.Rows(a).Item("idproducto"), "PRODUCTO NO EXISTE EN CONVENIO")
                                Continue For
                            End If

                            textoProceso = ("(" & tablaTrabajo.Rows(a).Item("idproducto") & ") Validando Estado")
                            'ReadyAction(navigator, 700)
                            System.Threading.Thread.Sleep(900)

                            naviElement = AccionNavegador(1, "spnEstado" & tablaTrabajo.Rows(a).Item("idproducto"), 0, naviDocment)
                            tt = naviElement.getAttribute("innerText")
                            tt = InStr(1, tt, "Sin stock")
                            If tt >= 1 Then
                                textoProceso = ("(" & tablaTrabajo.Rows(a).Item("idproducto") & ") Habilitando Stock...")
                                habilitaStock(tablaMaestra.Rows(i).Item("rutaStock"), tablaTrabajo.Rows(a).Item("idproducto"))
                                cambioStock = True

                                System.Threading.Thread.Sleep(900)
                                'ReadyAction(navigator, 700)

                                navigator.Navigate(tablaMaestra.Rows(i).Item("rutaStock"))
                                textoProceso = ("(" & tablaTrabajo.Rows(a).Item("idproducto") & ") Ingresando a Modificar Precio")

                                System.Threading.Thread.Sleep(900)
                                'ReadyAction(navigator, 700)

                                naviElement = AccionNavegador(3, "span", 3, naviDocment)
                                naviElement.click()

                                System.Threading.Thread.Sleep(900)
                                'ReadyAction(navigator, 700)

                                textoProceso = ("(" & tablaTrabajo.Rows(a).Item("idproducto") & ") Digitando Producto")
                                naviElement = AccionNavegador(1, "txtIdProducto", 0, naviDocment)
                                naviElement.value = tablaTrabajo.Rows(a).Item("idproducto")


                                naviElement = AccionNavegador(1, "btnFiltrar", 0, naviDocment)
                                naviElement.click()
                                textoProceso = ("(" & tablaTrabajo.Rows(a).Item("idproducto") & ") Buscando Producto")
                            End If

                            System.Threading.Thread.Sleep(900)
                            'ReadyAction(navigator, 700)

                            naviElement = AccionNavegador(1, "spnEstado" & tablaTrabajo.Rows(a).Item("idproducto"), 0, naviDocment)
                            tt = naviElement.getAttribute("innerText")
                            tt = InStr(1, tt, "Deshabilitado")
                            System.Threading.Thread.Sleep(500)
                            textoProceso = ("(" & tablaTrabajo.Rows(a).Item("idproducto") & ") Validando Estado")
                            If tt >= 1 Then
                                tt = naviElement.getAttribute("innerText")
                                tt = InStr(1, tt, "Precio - Con Stock")
                                If tt = 0 Then
                                    tablaErrores.Rows.Add(tablaMaestra.Rows(i).Item("Linea"), tablaTrabajo.Rows(a).Item("idproducto"), "PRODUCTO DESHABILITADO")
                                    Continue For
                                Else
                                    'SI EL PRODUCTO NO SE ENCUENTRA DESHABILITADO
                                    While tt <> "Editar campos"
                                        naviElement = AccionNavegador(3, "span", indice, naviDocment)
                                        tt = naviElement.getAttribute("innerText")
                                        System.Threading.Thread.Sleep(900)
                                        If tt = "Editar campos" Then
                                            naviElement.click()
                                        Else
                                            indice = indice + 1
                                        End If
                                    End While

                                    textoProceso = ("(" & tablaTrabajo.Rows(a).Item("idproducto") & ") Validando Precio")
                                    System.Threading.Thread.Sleep(900)
                                    naviElement = AccionNavegador(1, "txtPrecioActual" & tablaTrabajo.Rows(a).Item("idproducto"), 0, naviDocment)
                                    tt = naviElement.value.ToString.Replace(".", "")

                                    If tt <> tablaTrabajo.Rows(a).Item("precio") Then
                                        indice = 1

                                        naviElement.value = tablaTrabajo.Rows(a).Item("precio")
                                        textoProceso = ("(" & tablaTrabajo.Rows(a).Item("idproducto") & ") Digitando Precio")

                                        While tt <> "Guardar"
                                            naviElement = AccionNavegador(3, "a", indice, naviDocment)
                                            tt = naviElement.getAttribute("innerText")
                                            If tt = "Guardar" Then
                                                naviElement.click()
                                            Else
                                                indice = indice + 1
                                            End If
                                        End While

                                        textoProceso = ("precio Antes: " & tt & " / precio ahora: " & tablaTrabajo.Rows(a).Item("precio"))
                                    ElseIf tt = tablaTrabajo.Rows(a).Item("precio") Then
                                        indice = 1
                                        While tt <> "Volver"
                                            naviElement = AccionNavegador(3, "button", indice, naviDocment)
                                            tt = naviElement.getAttribute("innerText")
                                            If tt = "Volver" Then
                                                naviElement.click()
                                            Else
                                                indice = indice + 1
                                            End If
                                        End While

                                        textoProceso = ("Mismo precio, el precio no cambia (" & tt & ").")
                                    End If
                                End If
                            Else
                                System.Threading.Thread.Sleep(900)
                                'SI EL PRODUCTO ESTA HABILITADO
                                indice = 1
                                While tt <> "Editar campos"
                                    naviElement = AccionNavegador(3, "span", indice, naviDocment)
                                    tt = naviElement.getAttribute("innerText")
                                    If tt = "Editar campos" Then
                                        naviElement.click()
                                    Else
                                        indice = indice + 1
                                    End If
                                End While

                                System.Threading.Thread.Sleep(500)
                                naviElement = AccionNavegador(1, "txtPrecioActual" & tablaTrabajo.Rows(a).Item("idproducto"), 0, naviDocment)
                                tt = naviElement.value.ToString.Replace(".", "")

                                textoProceso = ("(" & tablaTrabajo.Rows(a).Item("idproducto") & ") Validadndo Precio")
                                If tt <> tablaTrabajo.Rows(a).Item("precio") Then
                                    textoProceso = ("(" & tablaTrabajo.Rows(a).Item("idproducto") & ") Digitando Precio")
                                    naviElement.value = tablaTrabajo.Rows(a).Item("precio")
                                    naviElement = AccionNavegador(1, "btnGuarda_" & tablaTrabajo.Rows(a).Item("idproducto"), 0, naviDocment)
                                    naviElement.click()

                                    textoProceso = ("precio Antes: " & tt & " / precio ahora: " & tablaTrabajo.Rows(a).Item("precio"))
                                    System.Threading.Thread.Sleep(900)
                                ElseIf tt = tablaTrabajo.Rows(a).Item("precio") Then
                                    naviElement = AccionNavegador(3, "button", 2, naviDocment)
                                    naviElement.click()

                                    textoProceso = ("Mismo precio, el precio no cambia (" & tt & ").")
                                    System.Threading.Thread.Sleep(900)
                                End If
                            End If

                            If cambioStock = True Then
                                cambioStock = False
                                textoProceso = ("inhabilitando Stock...")
                                DeshabilitaStock(tablaMaestra.Rows(i).Item("rutaStock"), tablaTrabajo.Rows(a).Item("idproducto"))
                                textoProceso = ("Actualizando precio")
                            End If

                            'ESTO ES PARA AGREGAR LA RELACIÓN DE PRODUCTO DIMERC
                            '----------------------------------------------------------------------------------------------------------------------------------------------
                            'naviElement = AccionNavegador(1, "txtCodProducto" & tablaTrabajo.Rows(a).Item("idproducto"), 0, naviDocment)
                            'tt = naviElement.value
                            'If tt <> tablaTrabajo.Rows(a).Item("precio") Then
                            '    naviElement.value = tablaTrabajo.Rows(a).Item("precio")

                            '    naviElement = AccionNavegador(1, "btnGuarda_" & tablaTrabajo.Rows(a).Item("idproducto"), 0, naviDocment)
                            '    naviElement.click()
                            'Else
                            '    naviElement = AccionNavegador(3, "button", 2, naviDocment)
                            '    naviElement.click()

                            '    textoProceso = textoProceso & vbCrLf & ("Mismo codigo, no cambia (" & tt & ").")
                            'End If

                            'ReadyAction(navigator, 500)

                            'textoProceso = textoProceso & vbCrLf & ("Codigo agregado: " & tablaTrabajo.Rows(a).Item("precio"))
                            '-----------------------------------------------------------------------------------------------------------------------------------------------
                        End If
                    End If
                Next
            Next

            textoProceso = ("Termino de cambio de precio...")
            If tablaErrores.Rows.Count > 0 Then
                ExportarDgvExcel()
            End If

            navigator.Quit()

            textoProceso = ("Proceso Finalizado...")

            CerrarIE()

            Return True
        Catch ex As Exception
            'navigator.Quit()
            If cambio = ("stock") Then
                GoTo repetir1
            ElseIf cambio = ("precio") Then
                If ex.ToString = "No se puede convertir el objeto COM del tipo 'System.__ComObject' al tipo de interfaz 'SHDocVw.IWebBrowser2'. Ocurrió un error de operación debido a que la llamada QueryInterface en el componente COM para la interfaz con IID '{D30C1661-CDAF-11D0-8A3E-00C04FC9E26E}' generó el siguiente error: El servidor RPC no está disponible. (Excepción de HRESULT: 0x800706BA)." Then
                    CerrarIE()
                End If
                navigator.Stop()
                navigator.Refresh2()
                System.Threading.Thread.Sleep(1500)
                GoTo repetir2
            End If

            Return False
        End Try
    End Function

    Private Sub CerrarIE()
        Dim p As Process

        For Each p In Process.GetProcesses()
            If Not p Is Nothing Then
                If p.ProcessName = "iexplore" Or p.ProcessName = "Internet Explorer" Then
                    p.Kill()
                    Exit Sub
                End If
            End If
        Next
    End Sub

    Public Sub ExportarDgvExcel()
        Try
            Dim WorkBook As New XLWorkbook
            tablaErrores.TableName = "DATOS"
            WorkBook.Worksheets.Add(tablaErrores)
            WorkBook.SaveAs("\\10.10.20.222\sistema\ArielLopez\MercadoPublico\ErroresOferton.xlsx")
        Catch ex As Exception
            'MessageBox.Show(ex.ToString, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Public Function ReadyAction(ByVal tool As InternetExplorer, time As Int64) As Boolean
        While (tool.Busy) ' Or tool.ReadyState <> tagREADYSTATE.READYSTATE_COMPLETE)
            System.Threading.Thread.Sleep(time)
            'tool.Stop()
            'tool.Refresh()
            textoProceso = textoProceso & vbCrLf & ("Estado: " & tool.ReadyState.ToString)
            textoProceso = textoProceso & vbCrLf & ("Cargando: " & tool.Busy)
        End While
        Return True
    End Function

    Public Function ImportarHojaEspecifica(ByVal tabla As DataTable, ruta As String) As DataTable
        Try
            'Cursor.Current = Cursors.WaitCursor

            Dim dt2 As DataTable
            Dim extension As String = Path.GetExtension(ruta)
            Dim header As String = "YES"
            Dim conStr As String, sheetName As String
            conStr = String.Empty
            Select Case extension

                Case ".xls"
                    'Excel 97-03
                    conStr = String.Format(Excel03ConString, ruta, header)
                    Exit Select

                Case ".xlsx"
                    'Excel 07 en adelante
                    conStr = String.Format(Excel07ConString, ruta, header)
                    Exit Select
            End Select

            'Get the name of the First Sheet.
            Using con As New OleDbConnection(conStr)
                Using cmd As New OleDbCommand()
                    cmd.Connection = con
                    con.Open()
                    Dim dtExcelSchema As DataTable = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
                    If dtExcelSchema.Rows.Count > 1 Then
                        sheetName = dtExcelSchema.Rows(1)("TABLE_NAME").ToString()
                        If Not sheetName = "'Carga Input$'" Then
                            sheetName = dtExcelSchema.Rows(0)("TABLE_NAME").ToString()
                        End If
                    Else
                        sheetName = dtExcelSchema.Rows(0)("TABLE_NAME").ToString()
                    End If
                    con.Close()
                End Using
            End Using

            'Read Data from the First Sheet.
            Using con As New OleDbConnection(conStr)
                Using cmd As New OleDbCommand()
                    Using oda As New OleDbDataAdapter()
                        Dim dt As New DataTable()
                        cmd.CommandText = (Convert.ToString("SELECT * From [") & sheetName) + "]"
                        cmd.Connection = con
                        con.Open()
                        oda.SelectCommand = cmd
                        oda.Fill(dt)
                        con.Close()
                        If (dt.Rows.Count = 0) Then
                            'MessageBox.Show("El archivo seleccionado no contiene datos", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                            Exit Function
                        End If
                        'Llenar DataGridView.
                        dt2 = dt.Copy
                        Return dt
                        'grilla.DataSource = dt
                        'Label1.Text = DataGridExcel.RowCount

                        'Cursor.Current = Cursors.Arrow

                    End Using
                End Using
            End Using

        Catch ex As Exception
            'MessageBox.Show(ex.ToString, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Function

    Public Function AccionNavegador(tipo As Int64, nombre As String, indice As Int64, navidocument As HTMLDocument) As IHTMLElement
        Dim naviElement As IHTMLElement
        Dim repite As Boolean = True

        While repite
            ReadyAction(navigator, 500)
            If tipo = 1 Then
                naviElement = navidocument.getElementById(nombre)
                If naviElement Is Nothing Then
                    repite = True
                Else
                    Return naviElement
                End If
            ElseIf tipo = 2 Then
                naviElement = navidocument.getElementsByName(nombre)
                If naviElement Is Nothing Then
                    repite = True
                Else
                    Return naviElement
                End If
            ElseIf tipo = 3 Then
                naviElement = navidocument.getElementsByTagName(nombre)(indice)
                If naviElement Is Nothing Then
                    repite = True
                Else
                    Return naviElement
                End If
            End If
        End While
    End Function

    Private Sub habilitaStock(ruta As String, sku As String)
        Dim tt As String
        Dim tiempo As Int64 = 0
        tiempo = (tiempo + 500)
Repeticion: navigator.Navigate(ruta)

        ReadyAction(navigator, tiempo)
        System.Threading.Thread.Sleep(5000)

        naviElement = AccionNavegador(1, "imgMod_DescontinuarStock", 0, naviDocment)
        naviElement.click()
        textoProceso = ("Habilitando Stock")

        System.Threading.Thread.Sleep(1500)
        ReadyAction(navigator, tiempo)

        textoProceso = ("Habilitando Stock.")
        naviElement = AccionNavegador(1, "txtIdProducto", 0, naviDocment)
        naviElement.value = sku

        System.Threading.Thread.Sleep(500)
        textoProceso = ("Habilitando Stock..")
        ReadyAction(navigator, 700)

        naviElement = AccionNavegador(1, "btnFiltrar", 0, naviDocment)
        naviElement.click()

        textoProceso = ("Habilitando Stock...")
        System.Threading.Thread.Sleep(2000)
        ReadyAction(navigator, tiempo)

        naviElement = AccionNavegador(1, "tdStock" & sku, 0, naviDocment)
        tt = naviElement.getAttribute("innertext")

        System.Threading.Thread.Sleep(2000)
        textoProceso = ("Habilitando Stock")
        ReadyAction(navigator, 700)

        If tt.Trim = "SIN STOCK" Then
            textoProceso = ("Habilitando Stock.")
            naviElement = AccionNavegador(1, "productoStock" & sku, 0, naviDocment)
            naviElement.click()

            ReadyAction(navigator, 700)
            textoProceso = ("Habilitando Stock..")

            naviElement = AccionNavegador(3, "button", 6, naviDocment)
            naviElement.click()

            textoProceso = ("Antes: SIN STOCK / Ahora: CON STOCK")
        Else
            textoProceso = ("Habilitando Stock...")
            GoTo Repeticion
        End If

    End Sub

    Private Sub DeshabilitaStock(ruta As String, sku As String)
        Dim tt As String
        navigator.Navigate(ruta)

        ReadyAction(navigator, 5000)

        naviElement = AccionNavegador(1, "imgMod_DescontinuarStock", 0, naviDocment)
        naviElement.click()

        ReadyAction(navigator, 3000)

        textoProceso = ("inhabilitando Stock")
        naviElement = AccionNavegador(1, "txtIdProducto", 0, naviDocment)
        naviElement.value = sku

        textoProceso = ("inhabilitando Stock.")
        ReadyAction(navigator, 700)

        naviElement = AccionNavegador(1, "btnFiltrar", 0, naviDocment)
        naviElement.click()

        textoProceso = ("inhabilitando Stock..")
        System.Threading.Thread.Sleep(2000)
        ReadyAction(navigator, 700)

        textoProceso = ("inhabilitando Stock...")
        naviElement = AccionNavegador(1, "tdStock" & sku, 0, naviDocment)
        tt = naviElement.getAttribute("innertext")

        textoProceso = ("inhabilitando Stock")
        If tt.Trim = "CON STOCK" Then
            textoProceso = ("inhabilitando Stock.")
            naviElement = AccionNavegador(1, "productoStock" & sku, 0, naviDocment)
            naviElement.click()

            textoProceso = ("inhabilitando Stock...")
            naviElement = AccionNavegador(3, "button", 6, naviDocment)
            naviElement.click()

            textoProceso = ("Antes: SIN STOCK / Ahora: CON STOCK")
        End If

        ReadyAction(navigator, 700)

    End Sub
End Class

