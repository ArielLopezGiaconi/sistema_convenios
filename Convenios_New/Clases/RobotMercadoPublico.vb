﻿Imports System.Data.OleDb
Imports ClosedXML.Excel
Imports System.IO
Imports mshtml
Imports SHDocVw
Imports OpenQA.Selenium
Imports OpenQA.Selenium.Chrome
Imports OpenQA.Selenium.Interactions
Public Class RobotMercadoPublico
    Public textoProceso As String = ""
    Public Excel03ConString As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties='Excel 8.0;HDR={1};IMEX=1'"
    Public Excel07ConString As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 12.0;HDR={1};IMEX=1'"
    Public tablaMaestra, tablaTrabajo, tablaErrores As New DataTable
    Public maximoLinea, maximoProducto, valorLinea, valorProducto, indice, rows1, rows2 As Int64

    Public Function inicioBot()
        Try
            Dim rut As String = "178378805"
            Dim contraseña As String = "emma6536"
            Dim tt, cambio As String
            'Dim precio As Int64
            Dim cambioStock As Boolean = False

            Dim servicio As ChromeDriverService = ChromeDriverService.CreateDefaultService()
            servicio.HideCommandPromptWindow = True
            Dim lista As Object
            Dim opciones As New ChromeOptions
            'opciones.AddArgument("headless")
            Dim chrome As New ChromeDriver(servicio, opciones)
            Dim Action = New Actions(chrome)
            Dim naviElement As Object

            tablaErrores.Columns.Add("Producto")
            tablaErrores.Columns.Add("Motivo")
            tablaErrores.Columns.Add("Rows")

            'navigator.Navigate("https://www.mercadopublico.cl/Home/Autenticacion?esNuevaHome=true")
            chrome.Navigate.GoToUrl("https://www.mercadopublico.cl/Home/Autenticacion?esNuevaHome=true")
            'navigator.Visible = True
            'navigator.FullScreen = True

            'ReadyAction(navigator, 500)
            System.Threading.Thread.Sleep(5000)

            'naviDocment = navigator.Document

            naviElement = chrome.FindElementById("liRUT")
            naviElement.click()
            System.Threading.Thread.Sleep(500)

            naviElement = chrome.FindElementById("txtRUT")
            'naviElement = naviDocment.getElementById("txtRUT")                  'INGRERSO USUARIOS
            'naviElement.value = rut
            naviElement.Clear()
            naviElement.sendKeys("1") '178378805
            System.Threading.Thread.Sleep(500)
            naviElement.sendKeys("7")
            System.Threading.Thread.Sleep(500)
            naviElement.sendKeys("8")
            System.Threading.Thread.Sleep(500)
            naviElement.sendKeys("3")
            System.Threading.Thread.Sleep(500)
            naviElement.sendKeys("7")
            System.Threading.Thread.Sleep(500)
            naviElement.sendKeys("8")
            System.Threading.Thread.Sleep(500)
            naviElement.sendKeys("8")
            System.Threading.Thread.Sleep(500)
            naviElement.sendKeys("0")
            System.Threading.Thread.Sleep(500)
            naviElement.sendKeys("5")
            System.Threading.Thread.Sleep(500)

            naviElement = chrome.FindElementById("txtPass")
            'naviElement = naviDocment.getElementById("txtPass")                 'INGRESO CLAVE
            'naviElement.value = contraseña
            naviElement.Clear()
            naviElement.sendKeys(contraseña)

            naviElement = chrome.FindElementById("btnAccesoRUT")
            naviElement.click()
            'naviDocment.getElementById("btnAccesoRUT").click()                  'CLICK AL BOTON DE INGRESAR

            'ReadyAction(navigator, 500)
            System.Threading.Thread.Sleep(5000)

            'naviDocment.getElementsByTagName("a")(12).click()

            lista = chrome.FindElementsByTagName("a")
            For i = 0 To lista.count - 1
                If i = 10 Then
                    naviElement = lista(i)
                    naviElement.click()
                    Exit For
                End If
            Next

            'tablaMaestra = ImportarHojaEspecifica(tablaMaestra, "C:\MercadoPublico\LicitacionesNew.xlsx")
            'tablaTrabajo = ImportarHojaEspecifica(tablaTrabajo, "C:\MercadoPublico\Productos.xlsx")
            tablaMaestra = ImportarHojaEspecifica(tablaMaestra, "\\10.10.20.222\sistema\ArielLopez\MercadoPublico\LicitacionesNew.xlsx")
            tablaTrabajo = ImportarHojaEspecifica(tablaTrabajo, "\\10.10.20.222\sistema\ArielLopez\MercadoPublico\Productos.xlsx")
            textoProceso = "Cargando Datos para trabajar..."
            tablaTrabajo.DefaultView.Sort = "idlinea asc"

            maximoLinea = tablaMaestra.Rows.Count
            maximoProducto = tablaTrabajo.Rows.Count
            rows1 = 0
            rows2 = 0

            'MODIFICACIÓN DE STOCK
            '            textoProceso = ("Actualizando Stock...")
            '            cambio = "stock"
            'repetir1:   For i = rows1 To tablaMaestra.Rows.Count - 1
            '                valorLinea = (i + 1)
            '                rows1 = i
            '                For a = rows2 To tablaTrabajo.Rows.Count - 1
            '                    If tablaTrabajo.Rows(a).Item("stock").ToString.ToUpper <> "X" Then
            '                        If tablaMaestra.Rows(i).Item("idlinea") = tablaTrabajo.Rows(a).Item("idlinea") Then
            '                            System.Threading.Thread.Sleep(1000)

            '                            valorProducto = (a + 1)
            '                            rows2 = a
            '                            chrome.Navigate.GoToUrl(tablaMaestra.Rows(i).Item("rutaStock").ToString)
            '                            textoProceso = ("(" & tablaTrabajo.Rows(a).Item("idproducto").ToString & ") Ingresando a Convenio")

            '                            System.Threading.Thread.Sleep(1000)

            '                            naviElement = chrome.FindElementById("imgMod_DescontinuarStock")
            '                            'naviElement = AccionNavegador(1, "imgMod_DescontinuarStock", 0, naviDocment)
            '                            naviElement.click()

            '                            textoProceso = ("(" & tablaTrabajo.Rows(a).Item("idproducto").ToString & ") Entrando a modificar stock")
            '                            System.Threading.Thread.Sleep(1000)

            '                            naviElement = chrome.FindElementById("txtIdProducto")
            '                            'naviElement = AccionNavegador(1, "txtIdProducto", 0, naviDocment)
            '                            'naviElement.value = tablaTrabajo.Rows(a).Item("idproducto")
            '                            naviElement.sendKeys(tablaTrabajo.Rows(a).Item("idproducto").ToString)

            '                            textoProceso = ("(" & tablaTrabajo.Rows(a).Item("idproducto") & ") Digitando Producto")
            '                            System.Threading.Thread.Sleep(1000)

            '                            naviElement = chrome.FindElementById("btnFiltrar")
            '                            'naviElement = AccionNavegador(1, "btnFiltrar", 0, naviDocment)
            '                            naviElement.click()

            '                            textoProceso = ("(" & tablaTrabajo.Rows(a).Item("idproducto") & ") Buscando Producto")
            '                            System.Threading.Thread.Sleep(1000)

            '                            System.Threading.Thread.Sleep(500)
            '                            tt = chrome.FindElementById("liResultado").Text
            '                            'naviElement = AccionNavegador(1, "liResultado", 0, naviDocment)
            '                            'tt = naviElement.getAttribute("innerText").ToString
            '                            System.Threading.Thread.Sleep(500)
            '                            If tt = "No existen productos asociados a tu búsqueda:" Then
            '                                tablaErrores.Rows.Add(tablaMaestra.Rows(i).Item("Linea"), tablaTrabajo.Rows(a).Item("idproducto"), "PRODUCTO NO EXISTE EN CONVENIO")
            '                                Continue For
            '                            End If

            '                            tt = chrome.FindElementById("tdStock" & tablaTrabajo.Rows(a).Item("idproducto")).Text
            '                            'naviElement = AccionNavegador(1, "tdStock" & tablaTrabajo.Rows(a).Item("idproducto"), 0, naviDocment)
            '                            'tt = naviElement.getAttribute("innertext")

            '                            textoProceso = ("(" & tablaTrabajo.Rows(a).Item("idproducto").ToString & ") Validando estado")
            '                            If tt.Trim = "SIN STOCK" And tablaTrabajo.Rows(a).Item("stock").ToString.ToUpper = "SI" Then
            '                                naviElement = chrome.FindElementById("productoStock" & tablaTrabajo.Rows(a).Item("idproducto"))
            '                                'naviElement = AccionNavegador(1, "productoStock" & tablaTrabajo.Rows(a).Item("idproducto"), 0, naviDocment)
            '                                naviElement.click()

            '                                'naviElement = AccionNavegador(3, "button", 6, naviDocment)
            '                                lista = chrome.FindElementsByTagName("button")
            '                                For x = 0 To lista.count - 1
            '                                    If x = 6 Then
            '                                        naviElement = lista(x)
            '                                        naviElement.click()
            '                                        Exit For
            '                                    End If
            '                                Next

            '                                naviElement.click()
            '                                textoProceso = ("(" & tablaTrabajo.Rows(a).Item("idproducto").ToString & ") Cambio a 'CON STOCK'")

            '                            ElseIf tt.Trim = "CON STOCK" And tablaTrabajo.Rows(a).Item("stock").ToString.ToUpper = "NO" Then
            '                                naviElement = chrome.FindElementById("productoStock" & tablaTrabajo.Rows(a).Item("idproducto"))
            '                                'naviElement = AccionNavegador(1, "productoStock" & tablaTrabajo.Rows(a).Item("idproducto"), 0, naviDocment)

            '                                naviElement.click()

            '                                'naviElement = AccionNavegador(3, "button", 6, naviDocment)
            '                                lista = chrome.FindElementsByTagName("button")
            '                                For x = 0 To lista.count - 1
            '                                    If x = 6 Then
            '                                        naviElement = lista(x)
            '                                        naviElement.click()
            '                                        Exit For
            '                                    End If
            '                                Next

            '                                naviElement.click()
            '                                textoProceso = ("(" & tablaTrabajo.Rows(a).Item("idproducto") & ") Cambio a 'SIN STOCK'")
            '                            Else
            '                                textoProceso = ("(" & tablaTrabajo.Rows(a).Item("idproducto") & ") No se realizaron cambios.")
            '                            End If

            '                            System.Threading.Thread.Sleep(1000)
            '                        End If
            '                    End If
            '                Next
            '            Next

            textoProceso = ("Fin Cambio Stock...")
            textoProceso = ("-----------------------------------------------")
            textoProceso = ("-----------------------------------------------")
            textoProceso = ("Actualizando precio")

            rows1 = 0
            rows2 = 0

            'MODIFICACION DE PRECIO
            cambio = "precio"
repetir2:   For i = rows1 To tablaMaestra.Rows.Count - 1
                valorLinea = (i + 1)
                rows1 = i
                System.Threading.Thread.Sleep(500)
                For a = rows2 To tablaTrabajo.Rows.Count - 1
                    'navigator.Visible = visible
                    If tablaTrabajo.Rows(a).Item("precio").ToString.ToUpper <> "X" Then
                        If tablaMaestra.Rows(i).Item("idlinea") = tablaTrabajo.Rows(a).Item("idlinea") Then
                            valorProducto = (a + 1)
                            rows2 = a
                            indice = 0
                            chrome.Navigate.GoToUrl(tablaMaestra.Rows(i).Item("rutaStock").ToString)
                            'navigator.Navigate(tablaMaestra.Rows(i).Item("rutaStock"))
                            textoProceso = ("(" & tablaTrabajo.Rows(a).Item("idproducto") & ") Ingresando a Convenio")
                            System.Threading.Thread.Sleep(500)

                            System.Threading.Thread.Sleep(500)
                            'ReadyAction(navigator, 700)

                            'naviElement = AccionNavegador(3, "span", 3, naviDocment)
                            lista = chrome.FindElementsByTagName("span")
                            For x = 0 To lista.count - 1
                                If x = 3 Then
                                    naviElement = lista(x)
                                    naviElement.click()
                                    Exit For
                                End If
                            Next

                            'naviElement.click()
                            textoProceso = ("(" & tablaTrabajo.Rows(a).Item("idproducto") & ") Ingresando a Modificar Precio")

                            System.Threading.Thread.Sleep(500)
                            'ReadyAction(navigator, 700)

                            'naviElement = AccionNavegador(1, "txtIdProducto", 0, naviDocment)
                            naviElement = chrome.FindElementById("txtIdProducto")
                            'naviElement.value = tablaTrabajo.Rows(a).Item("idproducto")
                            naviElement.sendKeys(tablaTrabajo.Rows(a).Item("idproducto").ToString)
                            textoProceso = ("(" & tablaTrabajo.Rows(a).Item("idproducto") & ") Digitando PRoducto")

                            naviElement = chrome.FindElementById("btnFiltrar")
                            'naviElement = AccionNavegador(1, "btnFiltrar", 0, naviDocment)
                            naviElement.click()
                            textoProceso = ("(" & tablaTrabajo.Rows(a).Item("idproducto") & ") Buscando Producto")

                            System.Threading.Thread.Sleep(2000)

                            tt = chrome.FindElementById("liResultado").Text
                            'naviElement = AccionNavegador(1, "liResultado", 0, naviDocment)
                            'tt = naviElement.getAttribute("innerText").ToString
                            System.Threading.Thread.Sleep(500)
                            If tt = "No existen productos asociados a tu búsqueda:" Then
                                tablaErrores.Rows.Add(tablaMaestra.Rows(i).Item("Linea"), tablaTrabajo.Rows(a).Item("idproducto"), "PRODUCTO NO EXISTE EN CONVENIO")
                                Continue For
                            End If

                            textoProceso = ("(" & tablaTrabajo.Rows(a).Item("idproducto") & ") Validadndo Estado")
                            System.Threading.Thread.Sleep(500)

                            tt = chrome.FindElementById("spnEstado" & tablaTrabajo.Rows(a).Item("idproducto").ToString).Text
                            'naviElement = AccionNavegador(1, "spnEstado" & tablaTrabajo.Rows(a).Item("idproducto"), 0, naviDocment)
                            'tt = naviElement.getAttribute("innerText")
                            tt = InStr(1, tt, "Sin stock")
                            If tt >= 1 Then
                                textoProceso = ("(" & tablaTrabajo.Rows(a).Item("idproducto") & ") Habilitando Stock...")
                                habilitaStock(tablaMaestra.Rows(i).Item("rutaStock"), tablaTrabajo.Rows(a).Item("idproducto"))
                                cambioStock = True

                                System.Threading.Thread.Sleep(500)
                                'ReadyAction(navigator, 700)

                                'navigator.Navigate(tablaMaestra.Rows(i).Item("rutaStock"))
                                chrome.Navigate.GoToUrl(tablaMaestra.Rows(i).Item("rutaStock").ToString)
                                textoProceso = ("(" & tablaTrabajo.Rows(a).Item("idproducto") & ") Ingresando a Modificar Precio")

                                System.Threading.Thread.Sleep(500)
                                'ReadyAction(navigator, 700)

                                lista = chrome.FindElementsByTagName("span")
                                For x = 0 To lista.count - 1
                                    If x = 3 Then
                                        naviElement = lista(x)
                                        naviElement.click()
                                        Exit For
                                    End If
                                Next
                                'naviElement = AccionNavegador(3, "span", 3, naviDocment)
                                'naviElement.click()

                                System.Threading.Thread.Sleep(500)
                                'ReadyAction(navigator, 700)

                                textoProceso = ("(" & tablaTrabajo.Rows(a).Item("idproducto") & ") Digitando Producto")
                                naviElement = chrome.FindElementById("txtIdProducto")
                                'naviElement = AccionNavegador(1, "txtIdProducto", 0, naviDocment)
                                naviElement.sendkeys(tablaTrabajo.Rows(a).Item("idproducto").ToString)

                                naviElement = chrome.FindElementById("btnFiltrar")
                                'naviElement = AccionNavegador(1, "btnFiltrar", 0, naviDocment)
                                naviElement.click()
                                textoProceso = ("(" & tablaTrabajo.Rows(a).Item("idproducto") & ") Buscando Producto")
                            End If

                            System.Threading.Thread.Sleep(500)
                            'ReadyAction(navigator, 700)

                            tt = chrome.FindElementById("spnEstado" & tablaTrabajo.Rows(a).Item("idproducto")).Text
                            'naviElement = AccionNavegador(1, "spnEstado" & tablaTrabajo.Rows(a).Item("idproducto"), 0, naviDocment)
                            'tt = naviElement.getAttribute("innerText")
                            tt = InStr(1, tt, "Deshabilitado")
                            System.Threading.Thread.Sleep(500)
                            textoProceso = ("(" & tablaTrabajo.Rows(a).Item("idproducto") & ") Validando Estado")
                            If tt >= 1 Then
                                'tt = naviElement.getAttribute("innerText")
                                tt = InStr(1, tt, "Precio - Con Stock")
                                If tt = 0 Then
                                    tablaErrores.Rows.Add(tablaMaestra.Rows(i).Item("Linea"), tablaTrabajo.Rows(a).Item("idproducto"), "PRODUCTO DESHABILITADO")
                                    Continue For
                                Else
                                    'SI EL PRODUCTO NO SE ENCUENTRA DESHABILITADO
                                    lista = chrome.FindElementsByTagName("span")
                                    For x = 0 To lista.count - 1
                                        naviElement = lista(x).text
                                        If tt = "Editar campos" Then
                                            naviElement = lista(x)
                                            naviElement.click()
                                            Exit For
                                        End If
                                    Next

                                    textoProceso = ("(" & tablaTrabajo.Rows(a).Item("idproducto") & ") Validando Precio")
                                    System.Threading.Thread.Sleep(500)
                                    tt = chrome.FindElementById("spnEstado" & tablaTrabajo.Rows(a).Item("idproducto").ToString).Text
                                    'naviElement = AccionNavegador(1, "txtPrecioActual" & tablaTrabajo.Rows(a).Item("idproducto"), 0, naviDocment)
                                    tt = naviElement.value.ToString.Replace(".", "")

                                    If tt <> tablaTrabajo.Rows(a).Item("precio") Then
                                        indice = 1

                                        naviElement.sendkeys(tablaTrabajo.Rows(a).Item("precio"))
                                        textoProceso = ("(" & tablaTrabajo.Rows(a).Item("idproducto") & ") Digitando Precio")

                                        lista = chrome.FindElementsByTagName("a")
                                        For x = 0 To lista.count - 1
                                            naviElement = lista(x).text
                                            If tt = "Guardar" Then
                                                naviElement = lista(x)
                                                naviElement.click()
                                                Exit For
                                            End If
                                        Next

                                        'While tt <> "Guardar"
                                        '    naviElement = AccionNavegador(3, "a", indice, naviDocment)
                                        '    tt = naviElement.getAttribute("innerText")
                                        '    If tt = "Guardar" Then
                                        '        naviElement.click()
                                        '    Else
                                        '        indice = indice + 1
                                        '    End If
                                        'End While

                                        textoProceso = ("precio Antes: " & tt & " / precio ahora: " & tablaTrabajo.Rows(a).Item("precio"))

                                    ElseIf tt = tablaTrabajo.Rows(a).Item("precio") Then
                                        lista = chrome.FindElementsByTagName("button")
                                        For x = 0 To lista.count - 1
                                            naviElement = lista(x).text
                                            If tt = "Volver" Then
                                                naviElement = lista(x)
                                                naviElement.click()
                                                Exit For
                                            End If
                                        Next

                                        'indice = 1
                                        'While tt <> "Volver"
                                        '    naviElement = AccionNavegador(3, "button", indice, naviDocment)
                                        '    tt = naviElement.getAttribute("innerText")
                                        '    If tt = "Volver" Then
                                        '        naviElement.click()
                                        '    Else
                                        '        indice = indice + 1
                                        '    End If
                                        'End While

                                        textoProceso = ("Mismo precio, el precio no cambia (" & tt & ").")
                                    End If
                                End If
                            Else
                                System.Threading.Thread.Sleep(500)
                                'SI EL PRODUCTO ESTA DESHABILITADO
                                naviElement = chrome.FindElementById("liModalEditarCampos1455544")
                                naviElement.click()

                                'lista = chrome.FindElementsByTagName("span")
                                'For x = 0 To lista.count - 1
                                '    tt = lista(x).text
                                '    If tt = "Editar campos" Then
                                '        naviElement = lista(x)
                                '        naviElement.click()
                                '        Exit For
                                '    End If
                                'Next

                                'indice = 1
                                'While tt <> "Editar campos"
                                '    naviElement = AccionNavegador(3, "span", indice, naviDocment)
                                '    tt = naviElement.getAttribute("innerText")
                                '    If tt = "Editar campos" Then
                                '        naviElement.click()
                                '    Else
                                '        indice = indice + 1
                                '    End If
                                'End While

                                System.Threading.Thread.Sleep(500)
                                naviElement = chrome.FindElementById("txtPrecioActual" & tablaTrabajo.Rows(a).Item("idproducto")).GetAttribute("value")
                                'naviElement = AccionNavegador(1, "txtPrecioActual" & tablaTrabajo.Rows(a).Item("idproducto"), 0, naviDocment)
                                tt = naviElement.ToString.Replace(".", "")

                                textoProceso = ("(" & tablaTrabajo.Rows(a).Item("idproducto") & ") Validadndo Precio")
                                If tt <> tablaTrabajo.Rows(a).Item("precio") Then
                                    textoProceso = ("(" & tablaTrabajo.Rows(a).Item("idproducto") & ") Digitando Precio")
                                    naviElement = chrome.FindElementById("txtPrecioActual" & tablaTrabajo.Rows(a).Item("idproducto"))
                                    naviElement.sendleys(tablaTrabajo.Rows(a).Item("precio").ToString)

                                    'naviElement = AccionNavegador(1, "btnGuarda_" & tablaTrabajo.Rows(a).Item("idproducto"), 0, naviDocment)
                                    naviElement = chrome.FindElementById("btnGuarda_" & tablaTrabajo.Rows(a).Item("idproducto").ToString)
                                    naviElement.click()

                                    textoProceso = ("precio Antes: " & tt & " / precio ahora: " & tablaTrabajo.Rows(a).Item("precio"))
                                    System.Threading.Thread.Sleep(500)
                                ElseIf tt = tablaTrabajo.Rows(a).Item("precio") Then
                                    lista = chrome.FindElementsByTagName("button")
                                    For x = 0 To lista.count - 1
                                        If x = 2 Then
                                            naviElement = lista(x)
                                            naviElement.click()
                                            Exit For
                                        End If
                                    Next

                                    'naviElement = AccionNavegador(3, "button", 2, naviDocment)
                                    'naviElement.click()

                                    textoProceso = ("Mismo precio, el precio no cambia (" & tt & ").")
                                    System.Threading.Thread.Sleep(500)
                                End If
                            End If

                            If cambioStock = True Then
                                cambioStock = False
                                textoProceso = ("inhabilitando Stock...")
                                DeshabilitaStock(tablaMaestra.Rows(i).Item("rutaStock"), tablaTrabajo.Rows(a).Item("idproducto"))
                                textoProceso = ("Actualizando precio")
                            End If

                            'ESTO ES PARA AGREGAR LA RELACIÓN DE PRODUCTO DIMERC
                            '----------------------------------------------------------------------------------------------------------------------------------------------
                            'naviElement = AccionNavegador(1, "txtCodProducto" & tablaTrabajo.Rows(a).Item("idproducto"), 0, naviDocment)
                            'tt = naviElement.value
                            'If tt <> tablaTrabajo.Rows(a).Item("precio") Then
                            '    naviElement.value = tablaTrabajo.Rows(a).Item("precio")

                            '    naviElement = AccionNavegador(1, "btnGuarda_" & tablaTrabajo.Rows(a).Item("idproducto"), 0, naviDocment)
                            '    naviElement.click()
                            'Else
                            '    naviElement = AccionNavegador(3, "button", 2, naviDocment)
                            '    naviElement.click()

                            '    textoProceso = textoProceso & vbCrLf & ("Mismo codigo, no cambia (" & tt & ").")
                            'End If

                            'ReadyAction(navigator, 500)

                            'textoProceso = textoProceso & vbCrLf & ("Codigo agregado: " & tablaTrabajo.Rows(a).Item("precio"))
                            '-----------------------------------------------------------------------------------------------------------------------------------------------
                        End If
                    End If
                Next
            Next

        Catch ex As Exception

        End Try
    End Function

    Private Sub DeshabilitaStock(ruta As String, sku As String)
        Dim tt As String
        Dim servicio As ChromeDriverService = ChromeDriverService.CreateDefaultService()
        servicio.HideCommandPromptWindow = True
        Dim lista As Object
        Dim opciones As New ChromeOptions
        'opciones.AddArgument("headless")
        Dim chrome As New ChromeDriver(servicio, opciones)
        Dim Action = New Actions(chrome)
        Dim naviElement As Object

        chrome.Navigate.GoToUrl(ruta)
        'navigator.Navigate(ruta)

        'ReadyAction(navigator, 1200)
        System.Threading.Thread.Sleep(500)

        naviElement = chrome.FindElementById("imgMod_DescontinuarStock")
        'naviElement = AccionNavegador(1, "imgMod_DescontinuarStock", 0, naviDocment)
        naviElement.click()

        'ReadyAction(navigator, 700)
        System.Threading.Thread.Sleep(500)

        textoProceso = ("inhabilitando Stock")
        naviElement = chrome.FindElementById("txtIdProducto")
        'naviElement = AccionNavegador(1, "txtIdProducto", 0, naviDocment)
        naviElement.sendkeys(sku)

        textoProceso = ("inhabilitando Stock.")
        'ReadyAction(navigator, 700)
        System.Threading.Thread.Sleep(500)

        naviElement = chrome.FindElementById("btnFiltrar")
        'naviElement = AccionNavegador(1, "btnFiltrar", 0, naviDocment)
        naviElement.click()

        textoProceso = ("inhabilitando Stock..")
        System.Threading.Thread.Sleep(2000)
        'ReadyAction(navigator, 700)

        textoProceso = ("inhabilitando Stock...")
        tt = chrome.FindElementById("tdStock" & sku).Text
        'naviElement = AccionNavegador(1, "tdStock" & sku, 0, naviDocment)
        'tt = naviElement.getAttribute("innertext")

        textoProceso = ("inhabilitando Stock")
        If tt.Trim = "CON STOCK" Then
            textoProceso = ("inhabilitando Stock.")
            'naviElement = AccionNavegador(1, "productoStock" & sku, 0, naviDocment)
            naviElement = chrome.FindElementById("productoStock" & sku)
            naviElement.click()

            lista = chrome.FindElementsByTagName("button")
            For x = 0 To lista.count - 1
                If x = 6 Then
                    naviElement = lista(6)
                    naviElement.click()
                    Exit For
                End If
            Next

            textoProceso = ("inhabilitando Stock...")
            'naviElement = AccionNavegador(3, "button", 6, naviDocment)
            'naviElement.click()

            textoProceso = ("Antes: SIN STOCK / Ahora: CON STOCK")
        End If

        'ReadyAction(navigator, 700)
        System.Threading.Thread.Sleep(500)

    End Sub

    Private Sub habilitaStock(ruta As String, sku As String)
        Dim tt As String
        Dim tiempo As Int64 = 0
        Dim servicio As ChromeDriverService = ChromeDriverService.CreateDefaultService()
        servicio.HideCommandPromptWindow = True
        Dim lista As Object
        Dim opciones As New ChromeOptions
        'opciones.AddArgument("headless")
        Dim chrome As New ChromeDriver(servicio, opciones)
        Dim Action = New Actions(chrome)
        Dim naviElement As Object

        tiempo = (tiempo + 500)
        chrome.Navigate.GoToUrl(ruta)

        'ReadyAction(navigator, tiempo)
        System.Threading.Thread.Sleep(500)

        naviElement = chrome.FindElementById("imgMod_DescontinuarStock")
        'naviElement = AccionNavegador(1, "imgMod_DescontinuarStock", 0, naviDocment)
        naviElement.click()
        textoProceso = ("Habilitando Stock")

        System.Threading.Thread.Sleep(500)
        'ReadyAction(navigator, tiempo)

        naviElement = chrome.FindElementById("txtIdProducto")
        textoProceso = ("Habilitando Stock.")
        'naviElement = AccionNavegador(1, "txtIdProducto", 0, naviDocment)
        naviElement.sendKeys(sku)

        System.Threading.Thread.Sleep(500)
        textoProceso = ("Habilitando Stock..")
        'ReadyAction(navigator, 700)

        naviElement = chrome.FindElementById("btnFiltrar")
        'naviElement = AccionNavegador(1, "btnFiltrar", 0, naviDocment)
        naviElement.click()

        textoProceso = ("Habilitando Stock...")
        System.Threading.Thread.Sleep(2000)
        'ReadyAction(navigator, tiempo)

        tt = chrome.FindElementById("tdStock" & sku).Text
        'naviElement = AccionNavegador(1, "tdStock" & sku, 0, naviDocment)
        'tt = naviElement.getAttribute("innertext")

        System.Threading.Thread.Sleep(2000)
        textoProceso = ("Habilitando Stock")
        'ReadyAction(navigator, 700)

        If tt.Trim = "SIN STOCK" Then
            textoProceso = ("Habilitando Stock.")
            naviElement = chrome.FindElementById("productoStock" & sku).Text
            'naviElement = AccionNavegador(1, "productoStock" & sku, 0, naviDocment)
            naviElement.click()

            System.Threading.Thread.Sleep(1000)
            textoProceso = ("Habilitando Stock..")

            lista = chrome.FindElementsByTagName("button")
            For x = 0 To lista.count - 1
                If x = 6 Then
                    naviElement = lista(6)
                    naviElement.click()
                    Exit For
                End If
            Next

            'naviElement = AccionNavegador(3, "button", 6, naviDocment)
            'naviElement.click()

            textoProceso = ("Antes: SIN STOCK / Ahora: CON STOCK")
        Else
            textoProceso = ("Habilitando Stock...")
        End If

    End Sub

    Public Sub ExportarDgvExcel()
        Try
            Dim WorkBook As New XLWorkbook
            tablaErrores.TableName = "DATOS"
            WorkBook.Worksheets.Add(tablaErrores)
            WorkBook.SaveAs("\\10.10.20.222\sistema\ArielLopez\MercadoPublico\ErroresOferton.xlsx")
        Catch ex As Exception
            'MessageBox.Show(ex.ToString, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    'Public Function ReadyAction(ByVal tool As InternetExplorer, time As Int64) As Boolean
    '    While (tool.Busy Or tool.ReadyState <> tagREADYSTATE.READYSTATE_COMPLETE)
    '        System.Threading.Thread.Sleep(time)
    '        'Console.WriteLine("Estado: " & tool.ReadyState.ToString)
    '        'Console.WriteLine("Navegador: " & tool.Busy)
    '    End While
    '    Return True
    'End Function

    Public Function ImportarHojaEspecifica(ByVal tabla As DataTable, ruta As String) As DataTable
        Try
            'Cursor.Current = Cursors.WaitCursor

            Dim dt2 As DataTable
            Dim extension As String = Path.GetExtension(ruta)
            Dim header As String = "YES"
            Dim conStr As String, sheetName As String
            conStr = String.Empty
            Select Case extension

                Case ".xls"
                    'Excel 97-03
                    conStr = String.Format(Excel03ConString, ruta, header)
                    Exit Select

                Case ".xlsx"
                    'Excel 07 en adelante
                    conStr = String.Format(Excel07ConString, ruta, header)
                    Exit Select
            End Select

            'Get the name of the First Sheet.
            Using con As New OleDbConnection(conStr)
                Using cmd As New OleDbCommand()
                    cmd.Connection = con
                    con.Open()
                    Dim dtExcelSchema As DataTable = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
                    If dtExcelSchema.Rows.Count > 1 Then
                        sheetName = dtExcelSchema.Rows(1)("TABLE_NAME").ToString()
                        If Not sheetName = "'Carga Input$'" Then
                            sheetName = dtExcelSchema.Rows(0)("TABLE_NAME").ToString()
                        End If
                    Else
                        sheetName = dtExcelSchema.Rows(0)("TABLE_NAME").ToString()
                    End If
                    con.Close()
                End Using
            End Using

            'Read Data from the First Sheet.
            Using con As New OleDbConnection(conStr)
                Using cmd As New OleDbCommand()
                    Using oda As New OleDbDataAdapter()
                        Dim dt As New DataTable()
                        cmd.CommandText = (Convert.ToString("SELECT * From [") & sheetName) + "]"
                        cmd.Connection = con
                        con.Open()
                        oda.SelectCommand = cmd
                        oda.Fill(dt)
                        con.Close()
                        If (dt.Rows.Count = 0) Then
                            'MessageBox.Show("El archivo seleccionado no contiene datos", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                            Exit Function
                        End If
                        'Llenar DataGridView.
                        dt2 = dt.Copy
                        Return dt
                        'grilla.DataSource = dt
                        'Label1.Text = DataGridExcel.RowCount

                        'Cursor.Current = Cursors.Arrow

                    End Using
                End Using
            End Using

        Catch ex As Exception
            'MessageBox.Show(ex.ToString, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Function

End Class
