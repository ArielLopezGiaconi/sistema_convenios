﻿Imports System.Data.OracleClient
Imports MySql.Data.MySqlClient

Public Class PuntoControl

    Dim bd As New Conexion
    Dim sql As String

    Public Function PutError(elerror As String)

        Using conn = New OracleConnection(Conexion.retornaConexion)
            Dim comando As OracleCommand
            conn.Open()
            Using transaccion = conn.BeginTransaction
                Try

                    sql = "insert into tab_errores (errores, sistema, usuario, fecha)
                           values('" & elerror.Substring(0, 250) & "//" & elerror.Substring(elerror.Length - 101, 100) & "', 'CONVENIOS_NEW','" & Globales.user & "', SYSDATE)"
                    comando = New OracleCommand(sql, conn)
                    comando.Transaction = transaccion
                    comando.ExecuteNonQuery()

                    transaccion.Commit()
                    Return "1"

                Catch ex As Exception
                    If Not transaccion.Equals(Nothing) Then
                        transaccion.Rollback()
                    End If
                    Return "0"
                Finally
                    conn.Close()
                End Try
            End Using
        End Using

    End Function

    Public Shared Function RegistroUso(id As String)
        Dim cone As New MySqlConnection
        cone.ConnectionString = "server=192.168.10.51;userid=root;password=d1mySQL20_18;database=dashboard;"
        Dim coman As New MySqlCommand
        Try

            If cone.State = ConnectionState.Open Then
                cone.Close()
            End If

            cone.Open()

            Dim sql As String
            Dim dt As New DataTable

            coman = cone.CreateCommand()
            coman.CommandType = CommandType.Text
            coman.CommandText = "select modulo from sistema_convenios_opciones where opcion = " & id
            coman.ExecuteNonQuery()
            Dim da As New MySqlDataAdapter(coman)
            da.Fill(dt)

            If dt.Rows.Count > 0 Then
                sql = "insert into sistema_convenios_registros (usuario, opcion, modulo, fecha) 
                       values('" & Globales.user & "', " & id & ", " & dt.Rows(0).Item("modulo") & ", " & "now())"
                coman.CommandText = sql
                coman.ExecuteNonQuery()
                da = New MySqlDataAdapter(coman)
            End If

            Return "1"

        Catch ex As Exception
            Return "0"
        Finally
            cone.Close()
        End Try
    End Function

End Class
