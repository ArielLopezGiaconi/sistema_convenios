﻿Imports System.Data.Odbc

Public Class ConexionMySQL
    Public mysqlcomand As OdbcCommand
    Public mysqlconn As OdbcConnection
    Public Shared conn_competencias As String = "DRIVER= {MySQL ODBC 3.51 Driver};SERVER=192.168.10.51;DATABASE=competencia;UID=root;password=d1mySQL20_18;PORT=3306;"
    Public Shared conn_DashBoard As String = "DRIVER= {MySQL ODBC 3.51 Driver};SERVER=192.168.10.51;DATABASE=dashboard;UID=root;password=d1mySQL20_18;PORT=3306;"

    Public Function open(conex As String) As Integer
        Try
            'CAMBIAR PARA SELECCIONAR BASE DE PRUEBA O PRODUCCION
            mysqlconn = New OdbcConnection(conex)
            mysqlconn.Open()
            Return 1
            ' Insert code to process data.
        Catch oex As OdbcException
            Return 2
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
            Return 0
        End Try
    End Function

    Public Sub close()
        mysqlconn.Close()
    End Sub

    Function sqlSelect(ByVal conex As String) As DataTable
        Dim xtab As DataTable = New DataTable("xTabla")
        mysqlcomand = New OdbcCommand(conex, mysqlconn)
        Dim xad As OdbcDataAdapter = New OdbcDataAdapter(mysqlcomand)
        xad.Fill(xtab)
        mysqlconn.Close()
        Return xtab
    End Function

    Public Sub ejecutar(sql As String)
        Try
            mysqlcomand = New OdbcCommand(sql, mysqlconn)
            mysqlcomand.ExecuteNonQuery()
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
            Exit Sub
        End Try

    End Sub
End Class
