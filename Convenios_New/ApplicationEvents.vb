﻿Imports System.Deployment.Application
Imports System.IO
Imports System.Net
Imports System.Reflection
Imports System.Security.Cryptography


Namespace My
    ' Los siguientes eventos están disponibles para MyApplication:
    ' Inicio: se desencadena cuando se inicia la aplicación, antes de que se cree el formulario de inicio.
    ' Apagado: generado después de cerrar todos los formularios de la aplicación. Este evento no se genera si la aplicación termina de forma anómala.
    ' UnhandledException: generado si la aplicación detecta una excepción no controlada.
    ' StartupNextInstance: se desencadena cuando se inicia una aplicación de instancia única y la aplicación ya está activa. 
    ' NetworkAvailabilityChanged: se desencadena cuando la conexión de red está conectada o desconectada.
    'algo
    Partial Friend Class MyApplication
        Private Sub Form1_Startup(ByVal sender As Object, ByVal e As Microsoft.VisualBasic.ApplicationServices.StartupEventArgs) Handles Me.Startup
            AddHandler AppDomain.CurrentDomain.AssemblyResolve, AddressOf CurrentDomain_AssemblyResolve
            'CheckForShortcut()
            'checkOracleDlls()
        End Sub

        Private Shared Sub CheckForShortcut()
            If (System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed) Then
                Dim ad As ApplicationDeployment = ApplicationDeployment.CurrentDeployment
                If (ad.IsFirstRun) Then 'first time user has run the app since installation or update
                    Dim code As Assembly = Assembly.GetExecutingAssembly()
                    Dim company As String = String.Empty
                    Dim description As String = String.Empty
                    If (Attribute.IsDefined(code, GetType(AssemblyCompanyAttribute))) Then
                        Dim ascompany As AssemblyCompanyAttribute =
                                CType(Attribute.GetCustomAttribute(code,
                                GetType(AssemblyCompanyAttribute)), AssemblyCompanyAttribute)
                        company = ascompany.Company
                    End If
                    If (Attribute.IsDefined(code, GetType(AssemblyDescriptionAttribute))) Then
                        Dim asdescription As AssemblyDescriptionAttribute =
                                CType(Attribute.GetCustomAttribute(code,
                                GetType(AssemblyDescriptionAttribute)), AssemblyDescriptionAttribute)
                        description = asdescription.Description
                    End If
                    If (company.Length > 0 AndAlso description.Length > 0) Then
                        Dim desktopPath As String = String.Empty
                        desktopPath = String.Concat(
                                Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
                                "\", description, ".appref-ms")
                        Dim shortcutName As String = String.Empty
                        shortcutName = String.Concat(
                                Environment.GetFolderPath(Environment.SpecialFolder.Programs),
                                "\", company, "\", description, ".appref-ms")
                        System.IO.File.Copy(shortcutName, desktopPath, True)
                    End If
                End If
            End If
        End Sub

        Private Sub checkOracleDlls()
            Dim P As String = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase)
            P = New Uri(P).LocalPath
            Dim mustDownload As Boolean

            For Each fichero As String In getListDlls()
                fichero = Path.Combine(P, fichero)
                If Not File.Exists(fichero) Then
                    mustDownload = True
                End If
            Next

            If mustDownload Then
                downloadDlls()
            Else
                If Not checkShaDlls() Then
                    downloadDlls()
                End If
            End If

        End Sub

        Private Sub downloadDlls()
            Dim P As String = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase)
            P = New Uri(P).LocalPath

            For Each fichero As String In getListDlls()
                fichero = Path.Combine(P, fichero)
                File.Delete(fichero)
            Next

            Dim downloader As New WebClient()
            For Each fichero As String In getListDlls()
                downloader.DownloadFile(New Uri("http://10.10.230.25/oracle_client/" & fichero), Path.Combine(P, fichero))
            Next
        End Sub

        Private Function checkShaDlls() As Boolean
            Dim P As String = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase)
            P = New Uri(P).LocalPath

            Dim shaLocal = ""
            For Each fichero As String In getListDlls()
                fichero = Path.Combine(P, fichero)
                shaLocal = shaLocal + getSha(fichero)
            Next
            If shaLocal = Resources.ShaDllOracle Then
                Return True
            Else
                Return False
            End If
        End Function

        Private Function getSha(ByVal filename As String) As String
            Try
                Using sha1 As New SHA1CryptoServiceProvider()
                    Dim bb As Byte() = File.ReadAllBytes(filename)
                    Return BitConverter.ToString(sha1.ComputeHash(bb)).Replace("-", String.Empty)
                End Using
            Catch ex As Exception
                Return "0"
            End Try
        End Function

        Private Function getListDlls() As String()
            Dim listDlls = My.Resources.RequiredDlls.Split(vbLf)
            Dim listReturn As New ArrayList
            For Each dllName As String In listDlls
                Dim dllFiltrado = ""
                dllFiltrado = dllName.Trim
                dllFiltrado = dllFiltrado.Replace(vbLf, "")
                listReturn.Add(dllFiltrado)
            Next
            Return listReturn.ToArray(GetType(String))
        End Function

        Private Shared Function CurrentDomain_AssemblyResolve(ByVal sender As Object, ByVal args As ResolveEventArgs) As Assembly
            Return Load()
        End Function

        Public Shared Function Load() As Assembly
            Try
                ' Get the byte[] of the DLL
                Dim ba As Byte() = Nothing
                Dim resource As String = "Convenios_New.Microsoft.Office.Interop.Excel.dll"
                Dim curAsm As Assembly = Assembly.GetExecutingAssembly()
                Using stm As Stream = curAsm.GetManifestResourceStream(resource)
                    ba = New Byte(CInt(stm.Length) - 1) {}
                    stm.Read(ba, 0, CInt(stm.Length))
                End Using

                Dim fileOk As Boolean = False
                Dim tempFile As String = ""

                Using sha1 As New SHA1CryptoServiceProvider()
                    ' Get the hash value of the Embedded DLL
                    Dim fileHash As String = BitConverter.ToString(sha1.ComputeHash(ba)).Replace("-", String.Empty)

                    ' The full path of the DLL that will be saved
                    tempFile = Path.GetTempPath() + "Microsoft.Office.Interop.Excel.dll"

                    ' Check if the DLL is already existed or not?
                    If File.Exists(tempFile) Then
                        ' Get the file hash value of the existed DLL
                        Dim bb As Byte() = File.ReadAllBytes(tempFile)
                        Dim fileHash2 As String = BitConverter.ToString(sha1.ComputeHash(bb)).Replace("-", String.Empty)

                        ' Compare the existed DLL with the Embedded DLL
                        If fileHash = fileHash2 Then
                            ' Same file
                            fileOk = True
                        Else
                            ' Not same
                            fileOk = False
                        End If
                    Else
                        ' The DLL is not existed yet
                        fileOk = False
                    End If
                End Using

                ' Create the file on disk
                If Not fileOk Then
                    System.IO.File.WriteAllBytes(tempFile, ba)
                End If

                ' Load it into memory    
                Return Assembly.LoadFile(tempFile)
            Catch ex As Exception
                MessageBox.Show(ex.ToString())
                Return Assembly.LoadFile("")
            End Try
        End Function
    End Class
End Namespace
